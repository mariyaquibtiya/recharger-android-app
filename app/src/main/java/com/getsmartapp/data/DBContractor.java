package com.getsmartapp.data;

import android.provider.BaseColumns;

/**
 * @author Nitesh.Verma on 05-05-2015.
 */
public class DBContractor {

    public static final class RecentRechargeEntry implements BaseColumns {
        public static final String TABLE_NAME = "recent_recharge";
        public static final String COLUMN_OPERATOR = "operator";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_NUMBER = "number";
        public static final String COLUMN_IS_POSTPAID = "is_postpaid";
        public static final String COLUMN_TIMESTAMP = "timestamp";
        public static final String COLUMN_OPERATOR_ID = "operator_id";
        public static final String COLUMN_PLAN_ID = "plan_id";
        public static final String COLUMN_CIRCLE_ID = "circle_id";
        public static final String COLUMN_CATAGORY_ID = "category_id";
        public static final String COLUMN_VALIDITY = "validity";
        public static final String COLUMN_DESC = "desc";
    }

    public static final class CardDataEntry implements BaseColumns {
        public static final String TABLE_NAME = "card_data";
        public static final String VIEW_NAME = "view_card";
        public static final String TABLE_ALIAS = "CA";
        public static final String COLUMN_CARD_ID = "card_id";
        public static final String COLUMN_CARD_NAME = "card_name";
        public static final String COLUMN_VAL_RECENCY_FALLBACK = "val_recency_fallback";
        public static final String COLUMN_VAL_CONTEXT_FALLBACK = "val_context_fallback";
        public static final String COLUMN_COEFF_CONTEXT = "coeff_context";
        public static final String COLUMN_POSITION_FIXED = "pos_fixed";
        public static final String COLUMN_PARENT_CARD_ID = "parent_card_id";
        public static final String COLUMN_CARD_VISIBILITY = "card_visibility";
        public static final String COLUMN_UPDATE_TIMESTAMP = "update_timestamp";
        public static final String COLUMN_PRIORITY_FALLBACK = "priority_fallback";
        public static final String COLUMN_SCORE = "score";
    }
}
