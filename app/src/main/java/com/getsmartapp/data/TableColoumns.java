package com.getsmartapp.data;

import android.provider.BaseColumns;

/**
 * @author Shalakha.Gupta on 13-05-2015.
 */
public class TableColoumns {

    public static final class OrderSummaryEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "order_summary";
        public static final String COLUMN_UID = "user_id";
        public static final String COLUMN_ORDER_ID="order_id";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_MOBILE_NO= "mobile_no";
        public static final String COLUMN_NETWORK = "network";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_AMOUNT = "amount";
    }
    public static final class AddCardEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "add_card";
        public static final String COLUMN_SAVED_TIMESTAMP = "saved_time";
        public static final String COLUMN_USED_TIMESTAMP = "used_time";
        public static final String COLUMN_UID = "user_id";
        public static final String COLUMN_CARD_NO="card_no";
        public static final String COLUMN_CARD_TYPE = "card_type";
        public static final String COLUMN_EXPIRY_MONTH= "expiry_month";
        public static final String COLUMN_EXPIRY_YEAR= "expiry_year";
        public static final String COLUMN_CARD_BANK_NAME = "bank_name";
        public static final String COLUMN_IS_DEFAULT = "is_default";
        public static final String COLUMN_CARD_HOLDER_NAME= "holder_name";
        public static final String COLUMN_PAYMENT_MODE= "payment_mode";
        public static final String COLUMN_ISSUER= "issuer";
        public static final String COLUMN_CARD_TOKEN= "card_token";
        public static final String COLUMN_CARD_BIN= "card_bin";
        public static final String COLUMN_ISEXPIRED= "is_expired";
    }

    public static final class AddGroupEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "add_group";
        public static final String COLUMN_UID = "user_id";
        public static final String COLUMN_GROUP_NAME = "group_name";
        public static final String COLUMN_MEMBER_NAME = "member_name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_NUMBER = "number";
    }

    public static final class RegistrationEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "registration";
        public static final String COLUMN_UID = "user_id";
        public static final String COLUMN_FIRST_NAME="first_name";
        public static final String COLUMN_LAST_NAME="last_name";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_MOBILE_NO= "mobile_no";
    }
}
