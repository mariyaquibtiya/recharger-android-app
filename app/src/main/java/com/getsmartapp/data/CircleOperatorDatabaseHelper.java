package com.getsmartapp.data;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * @author nitesh on 20/5/15.
 */
public class CircleOperatorDatabaseHelper extends SQLiteAssetHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "circle_operator.sqlite";
    private static final String TAG = SQLiteAssetHelper.class.getSimpleName();


    public CircleOperatorDatabaseHelper(Context context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    public interface TABLES {
        String TABLE_NAME = "operator_cicrle";
    }

    public interface COLUMNS {
        String COLUMN_COUNTRY_CODE = "mcc";
        String COLUMN_NETWORK_CODE = "mnc";
        String COLUMN_CIRCLE_NAME = "circle";
        String COLUMN_OPERATOR_ID = "operator";
    }
}
