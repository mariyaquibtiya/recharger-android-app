package com.getsmartapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.CardDataModel;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.util.CardUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;


public class DBHelper extends SdkDbHelper {

    private static DBHelper sInstance;

    public static synchronized DBHelper getInstance(Context context) {
        if (sInstance == null)
            sInstance = new DBHelper(context.getApplicationContext());
        return sInstance;
    }

    private DBHelper(Context context) {
        super(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);

        final String CREATE_TABLE_CART_ITEMS = "CREATE TABLE " + DBContractor.CartItemsEntry.TABLE_NAME + " (" +
                DBContractor.CartItemsEntry.COLUMN_OPERATOR + " TEXT NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_AMOUNT + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_PLAN_ID + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_CIRCLE_ID + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_SP_ID + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_DESC + " TEXT NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_MOBILE + " TEXT , " +
                DBContractor.CartItemsEntry.COLUMN_CAT_ID + " INTEGER NOT NULL, " +
                "UNIQUE (" + DBContractor.CartItemsEntry.COLUMN_AMOUNT + ", " +
                DBContractor.CartItemsEntry.COLUMN_OPERATOR + ")" +
                ");";

        final String CREATE_TABLE_RECENT_RECHARGE = "CREATE TABLE " + DBContractor.RecentRechargeEntry.TABLE_NAME + " (" +
                DBContractor.RecentRechargeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBContractor.RecentRechargeEntry.COLUMN_NUMBER + " TEXT NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_OPERATOR + " TEXT NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_AMOUNT + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_IS_POSTPAID + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.RecentRechargeEntry.COLUMN_OPERATOR_ID + " INTEGER DEFAULT 0, " +
                DBContractor.RecentRechargeEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_PLAN_ID + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_CIRCLE_ID + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_CATAGORY_ID + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_DESC + " TEXT NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_VALIDITY + " TEXT NOT NULL, " +
                "UNIQUE (" + DBContractor.RecentRechargeEntry.COLUMN_NUMBER + ", " +
                DBContractor.RecentRechargeEntry.COLUMN_AMOUNT + ") ON CONFLICT IGNORE" +
                ")";


        final String CREATE_GROUP_RECHARGE_TABLE = "CREATE TABLE " + DBContractor.UserGroupsEntry.TABLE_NAME + " (" +
                DBContractor.UserGroupsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DBContractor.UserGroupsEntry.COLUMN_MEMBER_NAME + " TEXT NOT NULL, " +
                DBContractor.UserGroupsEntry.COLUMN_GROUP_NAME + " TEXT NOT NULL, " +
                "UNIQUE (" + DBContractor.UserGroupsEntry.COLUMN_MEMBER_NAME + ", " + DBContractor.UserGroupsEntry.COLUMN_GROUP_NAME + ")" +
                ")";


        final String CREATE_CARD_DATA_TABLE = "CREATE TABLE " + DBContractor.CardDataEntry.TABLE_NAME + " (" +
                DBContractor.CardDataEntry.COLUMN_CARD_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardDataEntry.COLUMN_CARD_NAME + " TEXT NOT NULL UNIQUE, " +
                DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_PRIORITY_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE + " TEXT)";

        final String CREATE_CARD_CATEGORY_TABLE = "CREATE TABLE " + DBContractor.CardCategoryEntry.TABLE_NAME + " (" +
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + " TEXT)";

        final String CREATE_CARD_CATEGORY_MAPPING = "CREATE TABLE " + DBContractor.CardCategoryMapping.TABLE_NAME + " (" +
                DBContractor.CardCategoryMapping.COLUMN_CARD_ID + " INTEGER NOT NULL, " +
                DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " INTEGER NOT NULL)";

        final String CREATE_CARD_DATA_VIEW = "CREATE VIEW " + DBContractor.CardDataEntry.VIEW_NAME + " AS SELECT " +
                DataStorageConstants.ALIAS + ".* FROM (SELECT " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_NAME + ", " +
                DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + ", " +
                "MAX((SELECT (0.3*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ")+(0.7*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + "*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + "))) AS " + DBContractor.CardDataEntry.COLUMN_SCORE + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE +
                " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " " + DBContractor.CardDataEntry.TABLE_ALIAS +
                " JOIN " + DBContractor.CardCategoryMapping.TABLE_NAME + " " + DBContractor.CardCategoryMapping.TABLE_ALIAS +
                " ON " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CARD_ID +
                " JOIN " + DBContractor.CardCategoryEntry.TABLE_NAME + " " + DBContractor.CardCategoryEntry.TABLE_ALIAS +
                " ON " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " = " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " WHERE " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " = 1" +
                " GROUP BY " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME +
                " HAVING " + DBContractor.CardDataEntry.COLUMN_SCORE + " > 0" +
                " ORDER BY " + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC" +
                ") " + DataStorageConstants.ALIAS + " GROUP BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID +
                " ORDER BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC";

        final String CREATE_USSD_CODE_DETAIL_TABLE = "CREATE TABLE " + DBContractor.USSDCodeDetails.TABLE_NAME + " (" +
                DBContractor.USSDCodeDetails.COLUMN_TITLE + " TEXT NOT NULL, " +
                DBContractor.USSDCodeDetails.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                DBContractor.USSDCodeDetails.COLUMN_CODE_TYPE + " INTEGER NOT NULL, " +
                DBContractor.USSDCodeDetails.COLUMN_DEFAULT_DIAL_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_UPDATED_DIAL_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_DIAL_INSTRUCTION + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_DEFAULT_SMS_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_UPDATED_SMS_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_SMS_TEXT + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_SMS_INSTRUCTION + " TEXT)";

        try {
            db.execSQL(CREATE_TABLE_CART_ITEMS);
            db.execSQL(CREATE_TABLE_RECENT_RECHARGE);

            db.execSQL(CREATE_GROUP_RECHARGE_TABLE);
            db.execSQL(CREATE_CARD_DATA_TABLE);
            db.execSQL(CREATE_CARD_CATEGORY_TABLE);
            db.execSQL(CREATE_CARD_CATEGORY_MAPPING);
            insertCardData(db, Constants.INSERT);
            insertCardCategoryData(db);
            insertCardCategoryMapping(db);
            updateExcludeCardString(db);

            db.execSQL(CREATE_CARD_DATA_VIEW);
            db.execSQL(CREATE_USSD_CODE_DETAIL_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        super.onUpgrade(db, oldVersion, newVersion);

        final String DROP_TABLE_ORDER_TABLE = "DROP TABLE IF EXISTS order_summary";
        final String DROP_TABLE_ADD_CARD = "DROP TABLE IF EXISTS add_card" ;
        final String DROP_TABLE_REGISTRATION = "DROP TABLE IF EXISTS registration" ;
        final String DROP_TABLE_GROUP = "DROP TABLE IF EXISTS add_group";

        final String DROP_TABLE_CARD_DATA = "DROP TABLE IF EXISTS " + DBContractor.CardDataEntry.TABLE_NAME;

        final String DROP_TABLE_CARD_CATEGORY = "DROP TABLE IF EXISTS " + DBContractor.CardCategoryEntry.TABLE_NAME;

        final String DROP_TABLE_CARD_CATEGORY_MAPPING = "DROP TABLE IF EXISTS " + DBContractor.CardCategoryMapping.TABLE_NAME;

        final String DROP_VIEW_CARD = "DROP VIEW IF EXISTS " + DBContractor.CardDataEntry.VIEW_NAME;


        final String CREATE_CARD_DATA_TABLE = "CREATE TABLE " + DBContractor.CardDataEntry.TABLE_NAME + " (" +
                DBContractor.CardDataEntry.COLUMN_CARD_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardDataEntry.COLUMN_CARD_NAME + " TEXT NOT NULL UNIQUE, " +
                DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_PRIORITY_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE + " TEXT)";

        final String CREATE_CARD_CATEGORY_TABLE = "CREATE TABLE " + DBContractor.CardCategoryEntry.TABLE_NAME + " (" +
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + " TEXT)";

        final String CREATE_CARD_CATEGORY_MAPPING = "CREATE TABLE " + DBContractor.CardCategoryMapping.TABLE_NAME + " (" +
                DBContractor.CardCategoryMapping.COLUMN_CARD_ID + " INTEGER NOT NULL, " +
                DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " INTEGER NOT NULL)";

        final String CREATE_CARD_DATA_VIEW = "CREATE VIEW " + DBContractor.CardDataEntry.VIEW_NAME + " AS SELECT " +
                DataStorageConstants.ALIAS + ".* FROM (SELECT " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_NAME + ", " +
                DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + ", " +
                "MAX((SELECT (0.3*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ")+(0.7*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + "*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + "))) AS " + DBContractor.CardDataEntry.COLUMN_SCORE + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE +
                " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " " + DBContractor.CardDataEntry.TABLE_ALIAS +
                " JOIN " + DBContractor.CardCategoryMapping.TABLE_NAME + " " + DBContractor.CardCategoryMapping.TABLE_ALIAS +
                " ON " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CARD_ID +
                " JOIN " + DBContractor.CardCategoryEntry.TABLE_NAME + " " + DBContractor.CardCategoryEntry.TABLE_ALIAS +
                " ON " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " = " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " WHERE " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " = 1" +
                " GROUP BY " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME +
                " HAVING " + DBContractor.CardDataEntry.COLUMN_SCORE + " > 0" +
                " ORDER BY " + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC" +
                ") " + DataStorageConstants.ALIAS + " GROUP BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID +
                " ORDER BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC";

        final String CREATE_USSD_CODE_DETAIL_TABLE = "CREATE TABLE IF NOT EXISTS " + DBContractor.USSDCodeDetails.TABLE_NAME + " (" +
                DBContractor.USSDCodeDetails.COLUMN_TITLE + " TEXT NOT NULL, " +
                DBContractor.USSDCodeDetails.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                DBContractor.USSDCodeDetails.COLUMN_CODE_TYPE + " INTEGER NOT NULL, " +
                DBContractor.USSDCodeDetails.COLUMN_DEFAULT_DIAL_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_UPDATED_DIAL_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_DIAL_INSTRUCTION + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_DEFAULT_SMS_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_UPDATED_SMS_CODE + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_SMS_TEXT + " TEXT, " +
                DBContractor.USSDCodeDetails.COLUMN_SMS_INSTRUCTION + " TEXT)";

        try {

            db.execSQL(DROP_VIEW_CARD);
            db.execSQL(DROP_TABLE_CARD_DATA);
            db.execSQL(DROP_TABLE_CARD_CATEGORY);
            db.execSQL(DROP_TABLE_CARD_CATEGORY_MAPPING);
            db.execSQL(CREATE_CARD_DATA_TABLE);
            db.execSQL(CREATE_CARD_CATEGORY_TABLE);
            db.execSQL(CREATE_CARD_CATEGORY_MAPPING);
            db.execSQL(DROP_TABLE_ORDER_TABLE);
            db.execSQL(DROP_TABLE_ADD_CARD);
            db.execSQL(DROP_TABLE_REGISTRATION);
            db.execSQL(DROP_TABLE_GROUP);
            insertCardData(db, Constants.INSERT);
            insertCardCategoryData(db);
            insertCardCategoryMapping(db);
            updateExcludeCardString(db);
            db.execSQL(CREATE_CARD_DATA_VIEW);
            db.execSQL(CREATE_USSD_CODE_DETAIL_TABLE);
            SharedPrefManager pref = new SharedPrefManager(mContext);
            pref.setBooleanValue(Constants.IS_QUICKHELP_FETCHED, Boolean.FALSE);
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("PeeyushKS","ON UPGRADE: DBHELPER 21");
        }

    }

    public Cursor getDataCursor(SQLiteDatabase mDatabase, String tablename) {
        return mDatabase.query(tablename, null, null, null, null, null, null);
    }

    public Cursor getUserPhoneDetailCursor(SQLiteDatabase mDatabase) {
        return mDatabase.rawQuery("select * from " + DBContractor.UserPhoneEntry.TABLE_NAME, null);
    }

    private void insertCardData(SQLiteDatabase db, int type) {
        ContentValues values = new ContentValues();

        Iterator itrCardList = CardUtils.getCardList(mContext, type).listIterator();
        while (itrCardList.hasNext()) {
            CardDataModel card = (CardDataModel) itrCardList.next();
            if (type == Constants.INSERT) {
                values.put(DBContractor.CardDataEntry.COLUMN_CARD_ID, card.getCard_id());
                values.put(DBContractor.CardDataEntry.COLUMN_CARD_NAME, card.getCard_name());
            }
            values.put(DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, card.getVal_recency_fallback());
            values.put(DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, card.getVal_context_fallback());
            values.put(DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT, card.getCoeff_context());
            values.put(DBContractor.CardDataEntry.COLUMN_POSITION_FIXED, card.getPos_fixed());
            values.put(DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID, card.getParent_card_id());
            values.put(DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY, card.getCard_visibility());
            values.put(DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP, new Date().getTime());
            values.put(DBContractor.CardDataEntry.COLUMN_PRIORITY_FALLBACK, (0.3 * card.getVal_recency_fallback()) + (0.7 * card.getVal_context_fallback() * card.getCoeff_context()));
            values.put(DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE, "");

            if (type == Constants.INSERT) {
                db.insert(DBContractor.CardDataEntry.TABLE_NAME, null, values);
            } else if (type == Constants.UPDATE) {
                db.update(DBContractor.CardDataEntry.TABLE_NAME, values, DBContractor.CardDataEntry.COLUMN_CARD_ID + "=" + card.getCard_id(), null);
            }
        }
    }

    private void insertCardCategoryData(SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        Iterator itrCardCategoryList = CardUtils.getCardCategoryList(mContext).entrySet().iterator();
        while (itrCardCategoryList.hasNext()) {
            HashMap.Entry cardEntry = (HashMap.Entry) itrCardCategoryList.next();
            values.put(DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID, (Integer) cardEntry.getKey());
            values.put(DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME, (String) cardEntry.getValue());

            db.insert(DBContractor.CardCategoryEntry.TABLE_NAME, null, values);
        }
    }

    private void insertCardCategoryMapping(SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        Iterator itrMappingList = CardUtils.getCardMappingList(mContext).listIterator();
        while (itrMappingList.hasNext()) {
            Integer[] mappingEntry = (Integer[]) itrMappingList.next();
            values.put(DBContractor.CardCategoryMapping.COLUMN_CARD_ID, mappingEntry[0]);
            values.put(DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID, mappingEntry[1]);

            db.insert(DBContractor.CardCategoryMapping.TABLE_NAME, null, values);
        }
    }

    private void updateExcludeCardString(SQLiteDatabase db) {
        Cursor cardCursor = null;

        String rawQuery = "SELECT DISTINCT(" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + ") " + DBContractor.CardDataEntry.COLUMN_CARD_ID +
                " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " " + DBContractor.CardDataEntry.TABLE_ALIAS +
                " JOIN " + DBContractor.CardCategoryMapping.TABLE_NAME + " " + DBContractor.CardCategoryMapping.TABLE_ALIAS + " ON " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CARD_ID +
                " JOIN " + DBContractor.CardCategoryEntry.TABLE_NAME + " " + DBContractor.CardCategoryEntry.TABLE_ALIAS + " ON " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " = " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " WHERE " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID + " IN " +
                "(SELECT " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " " + DBContractor.CardDataEntry.TABLE_ALIAS +
                " JOIN " + DBContractor.CardCategoryMapping.TABLE_NAME + " " + DBContractor.CardCategoryMapping.TABLE_ALIAS + " ON " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CARD_ID +
                " JOIN " + DBContractor.CardCategoryEntry.TABLE_NAME + " " + DBContractor.CardCategoryEntry.TABLE_ALIAS + " ON " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " = " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " WHERE " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = ?)";

        for(int i = 1; i <= 33; i++) {
            cardCursor = db.rawQuery(rawQuery, new String[]{String.valueOf(i)});

            if (cardCursor != null && cardCursor.moveToFirst()) {
                String cardString = "";
                do {
                    String currentCardId = cardCursor.getString(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_CARD_ID));
                    if(!currentCardId.equals(String.valueOf(i))) {
                        cardString += "," + currentCardId;
                    }
                } while(cardCursor.moveToNext());

                if(!cardString.isEmpty()) {
                    cardString = cardString.substring(1);

                    ContentValues values = new ContentValues();
                    values.put(DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE, cardString);

                    db.update(DBContractor.CardDataEntry.TABLE_NAME, values, DBContractor.CardDataEntry.COLUMN_CARD_ID + "=?", new String[]{String.valueOf(i)});

                }
            }
        }

        if(cardCursor != null && !cardCursor.isClosed()) {
            cardCursor.close();
        }
    }

    public Cursor getRecentRechargeCursor(SQLiteDatabase db, String number) {

        return db.rawQuery("select * from " + DBContractor.RecentRechargeEntry.TABLE_NAME + " where " + DBContractor.RecentRechargeEntry.COLUMN_NUMBER + "=" + number, null);

    }

}