package com.getsmartapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.lib.model.RecentRechargeItem;
import com.getsmartapp.util.SProvider;

import java.util.List;

/**
 * @author shalakha.gupta on 17/09/15.
 */
public class SaveRecentRechargesAdapter extends BaseAdapter {

    private LayoutInflater inflater = null;
    Context mContext;
    List<RecentRechargeItem> rrList;
    SProvider sProvider;

    public SaveRecentRechargesAdapter(Context mContext, List<RecentRechargeItem> rrList) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rrList = rrList;
        sProvider = new SProvider();
    }

    @Override
    public int getCount() {
        if (rrList != null && rrList.size() > 0)
            return rrList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = inflater.inflate(R.layout.recent_recharge_item_row, null);
            holder = new ViewHolder();
            holder.numberTxt = (TextView) view.findViewById(R.id.tv_rr_number);
            holder.amountTxt = (TextView) view.findViewById(R.id.tv_rr_amount);
            holder.sprovideImg = (ImageView) view.findViewById(R.id.iv_rr_operator);

            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        holder.sprovideImg.setImageResource(sProvider.getOperatorImgRes(rrList.get(position).getProvider_name()));
        holder.numberTxt.setText(rrList.get(position).getNumber());
        holder.amountTxt.setText(mContext.getString(R.string.rs) + rrList.get(position).getRecamount());
        view.setId(position);
        return view;
    }

    public static class ViewHolder {

        public TextView numberTxt;
        public TextView amountTxt;
        public ImageView sprovideImg;
    }
}
