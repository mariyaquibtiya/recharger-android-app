package com.getsmartapp.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.apsalar.sdk.Apsalar;
import com.getsmartapp.RechargerApplication;
import com.getsmartapp.activity.SplashActivity;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.tagmanager.TagManager;

/**
 * @author Peeyush.Singh on 03-05-2016.
 */
public class PersistenetNotificationBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(!RechargerApplication.getGlobalInstance().isApplicationInForeground())
        {
            DataLayer mDataLayer = TagManager.getInstance(context).getDataLayer();
            mDataLayer.push(DataLayer.mapOf("event", "GAEvent", "eventAct", "Today", "eventCat", "Widget", "eventLbl", "Open", "eventVal", 1));

            Apsalar.event("WidgetClicked", "eventAct", "Today", "eventCat", "Widget", "eventLbl", "Open", "eventVal", 1);

            Intent intent1 = new Intent(context, SplashActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent1);
        }
    }
}
