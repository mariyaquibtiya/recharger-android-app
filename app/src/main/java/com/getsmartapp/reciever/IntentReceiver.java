package com.getsmartapp.reciever;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.getsmartapp.activity.SplashActivity;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.util.CustomNotificationFactory;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;
import com.urbanairship.util.UAStringUtil;

/**
 * @author bishwajeet.kumar on 08/09/15.
 */
public class IntentReceiver extends BaseIntentReceiver {

    private static final String TAG = "IntentReceiver";

    @Override
    protected void onChannelRegistrationSucceeded(Context context, String channelId) {
    }

    @Override
    protected void onChannelRegistrationFailed(Context context) {
    }

    @Override
    protected void onPushReceived(Context context, PushMessage message, int notificationId) {
        Log.e(TAG, "Received push notification. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);

    }

    @Override
    protected void onBackgroundPushReceived(Context context, PushMessage message) {
    }

    @Override
    protected boolean onNotificationOpened(Context context, PushMessage message, int notificationId) {
        Log.e(TAG, "User clicked notification. Alert: " + message.getAlert());
        String pushtype = UAStringUtil.isEmpty(CustomNotificationFactory.getMessageData(message.getAlert(), "PUSHTYPE"))?"": CustomNotificationFactory.getMessageData(message.getAlert(), "PUSHTYPE");
        if(pushtype.equalsIgnoreCase("silentAlert")) {
            SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
            Intent i = new Intent(context, SplashActivity.class);
            i.setData(Uri.parse(sharedPrefManager.getStringValue(Constants.CUSTOM_DEEPLINK)));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sharedPrefManager.setStringValue(Constants.CUSTOM_DEEPLINK, "");
            context.startActivity(i);
        } else {
            return true;
        }
        // Return false to let UA handle launching the launch activity
        return true;
    }

    @Override
    protected boolean onNotificationActionOpened(Context context, PushMessage message, int notificationId, String buttonId, boolean isForeground) {
        Log.e(TAG, "User clicked notification button. Button ID: " + buttonId + " Alert: " + message.getAlert());
        String pushtype = UAStringUtil.isEmpty(CustomNotificationFactory.getMessageData(message.getAlert(), "PUSHTYPE"))?"": CustomNotificationFactory.getMessageData(message.getAlert(), "PUSHTYPE");
        if(pushtype.equalsIgnoreCase("silentAlert")) {
            SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
            Intent i = new Intent(context, SplashActivity.class);
            i.setData(Uri.parse(sharedPrefManager.getStringValue(Constants.CUSTOM_DEEPLINK)));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sharedPrefManager.setStringValue(Constants.CUSTOM_DEEPLINK, "");
            context.startActivity(i);
        } else {
            return true;
        }
        // Return false to let UA handle launching the launch activity
        return true;
    }

    @Override
    protected void onNotificationDismissed(Context context, PushMessage message, int notificationId) {
        Log.e(TAG, "Notification dismissed. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
    }
}
