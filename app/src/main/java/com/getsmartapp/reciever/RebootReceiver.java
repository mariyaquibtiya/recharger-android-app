package com.getsmartapp.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.services.LocationService;
import com.getsmartapp.util.PersistentNotification;

/**
 * @author Peeyush.Singh on 23-02-2016.
 */
public class RebootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        InternetDataUsageUtil.getInstance(context).handleBroadCastAction(context, intent);
        PersistentNotification.getInstance(context).updateDataNotification(context);

        Intent il = new Intent(context, LocationService.class);
        context.startService(il);
    }
}
