package com.getsmartapp.interfaces;

import com.getsmartapp.model.WalletHistoryModel;

/**
 * @author shalakha.gupta on 02/02/16.
 */
public interface WalletResponseCallback {
    void onWalletSuccess(WalletHistoryModel model);
    void onWalletFailure(String error);
}
