package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 27/07/15.
 */
public interface DrawerItemClickListener {

    void OnDrawerItemClick(int pos);
}
