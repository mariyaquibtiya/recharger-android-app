package com.getsmartapp.interfaces;

/**
 * @author Shalakha.Gupta on 25-05-2015.
 */
public interface IncomingSmsListner {

   void onSmsReceived(String sendNo, String message);
}
