package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 20/06/16.
 */
public interface MaterialKeyBackListener {

    public void onBackKeyPress();
}
