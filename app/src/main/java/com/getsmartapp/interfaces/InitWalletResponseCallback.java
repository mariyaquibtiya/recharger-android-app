package com.getsmartapp.interfaces;

import com.getsmartapp.lib.model.InitRechargeModel;

/**
 * @author shalakha.gupta on 03/02/16.
 */
public interface InitWalletResponseCallback {
    void onInitWalletSuccess(InitRechargeModel initModel);
    void onInitWalletFailure(String error);
}
