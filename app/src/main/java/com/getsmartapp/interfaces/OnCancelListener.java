package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 10/11/15.
 */
public interface OnCancelListener {
    void OnCancel();
}
