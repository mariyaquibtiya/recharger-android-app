package com.getsmartapp.interfaces;

import com.getsmartapp.lib.model.PromoCodeModel1;

/**
 * @author shalakha.gupta on 29/02/16.
 */
public interface ApplyGcCallBack {

    void onGCSuccess(PromoCodeModel1 response);
    void onGCFailure(String error);
}
