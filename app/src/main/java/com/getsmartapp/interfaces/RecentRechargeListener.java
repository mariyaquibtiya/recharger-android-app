package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 26/10/15.
 */
public interface RecentRechargeListener {
    void onRecentRechargeItemClick(int pos);
}
