package com.getsmartapp.interfaces;

/**
 * @author Jayant.B on 8/11/2015.
 */
public interface OnCompleteLocation {
    void OnComplete(int pos);
}
