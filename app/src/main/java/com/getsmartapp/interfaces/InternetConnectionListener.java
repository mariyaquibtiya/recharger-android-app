package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 09/09/15.
 */
public interface InternetConnectionListener {
    void onRetryClick();
    void onGoToSettingsClick();
}
