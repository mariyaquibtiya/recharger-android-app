package com.getsmartapp.interfaces;

/**
 * @author Shalakha.Gupta on 25-05-2015.
 */
public interface DialogOkClickListner {

     void onOKClick(String mobileNo);
}
