package com.getsmartapp.interfaces;

import android.os.Bundle;

import java.util.HashMap;

/**
 * @author shalakha.gupta on 23/07/15.
 */
public interface OnBoardingInterface {

    void onSave(HashMap<String, String> data, String from);
    void onClick(Bundle bundle);
}
