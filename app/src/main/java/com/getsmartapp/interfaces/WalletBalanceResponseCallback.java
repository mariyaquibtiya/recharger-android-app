package com.getsmartapp.interfaces;

import com.getsmartapp.model.GetWalletBalanceModel;

/**
 * @author shalakha.gupta on 02/02/16.
 */
public interface WalletBalanceResponseCallback {
    void onWalletSuccess(GetWalletBalanceModel model);
    void onWalletFailure(String error);
}
