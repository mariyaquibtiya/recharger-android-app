package com.getsmartapp.interfaces;

import android.widget.TextView;

/**
 * @author shalakha.gupta on 04/12/15.
 */
public interface UpdateRequestText {
    void updateRequestText(TextView messageText, String message, long remainSecs);
}
