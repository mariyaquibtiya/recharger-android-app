package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 29/04/16.
 */
public interface DeleteCardDialogListener {

    void onDeleteCardOKClick(String mobileNo, int oneTapEnable);
}
