package com.getsmartapp.interfaces;

import com.getsmartapp.model.RefundToBankModel;

/**
 * @author shalakha.gupta on 04/02/16.
 */
public interface RefundToBankResponseCallback {

    void onRefundSuccess(RefundToBankModel.BodyEntity.DataEntity dataEntity);
    void onRefundFailure(String error);
}
