package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 22/01/16.
 */
public interface MakePaymentBtnClickListner {
    void onMakePaymentClick(String paymentVia, boolean isPaymentThroughGC);
}
