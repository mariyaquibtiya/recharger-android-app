package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 03/02/16.
 */
public interface WalletGiftCouponResponseCallback {

    void onGiftCouponSuccess(double gcAmt);
    void onGiftCouponFailure(String error);
}
