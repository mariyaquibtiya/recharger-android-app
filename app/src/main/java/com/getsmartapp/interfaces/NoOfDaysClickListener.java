package com.getsmartapp.interfaces;

/**
 * @author Shalakha.Gupta on 17-06-2015.
 */
public interface NoOfDaysClickListener {

    public void onOKClick(String mobileNo);
    public void onCancelClick();
}
