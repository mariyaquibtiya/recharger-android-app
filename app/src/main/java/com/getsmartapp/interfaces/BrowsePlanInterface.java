package com.getsmartapp.interfaces;

/**
 * @author shalakha.gupta on 23/07/15.
 */
public interface BrowsePlanInterface {

    void addRemoveItemInCart(boolean isShow, Object object);
    void showHideCart(boolean isShow);
    void viewCartDetails();
    boolean checkPlanExistInCart(int planId);
}
