package com.getsmartapp.interfaces;

/**
 * @author Shalakha.Gupta on 02-06-2015.
 */
public interface RechargeTypeListener {
    public void onSingleRecharge();
    public void onMultipleRecharge(boolean checked, boolean checked1, boolean checked2, boolean checked3);
}
