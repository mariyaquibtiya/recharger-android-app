package com.getsmartapp.fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getsmartapp.R;

public class BaseAccountFragment extends BaseFragment {

    public BaseAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base_account, container, false);
        LinearLayout itemsContainer = (LinearLayout) view.findViewById(R.id.items_container);
        inflateItems(itemsContainer);

        return view;
    }

    private void inflateItems(LinearLayout itemsContainer) {

        String[] accountItems = getActivity().getResources().getStringArray(R.array.account_main_items);
        TypedArray imageArray = getResources().obtainTypedArray(R.array.account_items_drawable);

        for (int i=0; i<imageArray.length(); i++) {

            if (i==4) {
                View subHeading = View.inflate(getActivity(), R.layout.account_sub_heading, null);
                itemsContainer.addView(subHeading);
            }

            View itemView = View.inflate(getActivity(), R.layout.account_list_item, null);
            TextView itemType = (TextView) itemView.findViewById(R.id.item_type_tv);
            itemType.setCompoundDrawablesWithIntrinsicBounds(imageArray.getResourceId(i, -1), 0, R.drawable.top_arrow, 0);
            itemType.setText(accountItems[i]);

            itemsContainer.addView(itemView);
        }

        imageArray.recycle();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
