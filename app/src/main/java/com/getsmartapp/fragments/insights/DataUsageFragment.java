package com.getsmartapp.fragments.insights;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getsmartapp.R;
import com.getsmartapp.cards.DataSummary;
import com.getsmartapp.fragments.BaseFragment;
import com.getsmartapp.lib.constants.DataStorageConstants;

public class DataUsageFragment extends BaseFragment {


    public DataUsageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_data_usage, container, false);
        DataSummary dataSummary = new DataSummary(getActivity(), DataStorageConstants.CARD_DATA_TOP_APPS);
        dataSummary.inflateDataUsage(view);

        return view;
    }

}
