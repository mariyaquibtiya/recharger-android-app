package com.getsmartapp.fragments.insights;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getsmartapp.R;
import com.getsmartapp.cards.WifiSummary;
import com.getsmartapp.fragments.BaseFragment;
import com.getsmartapp.lib.constants.DataStorageConstants;

public class WifiUsageFragment extends BaseFragment {


    public WifiUsageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wifi_usage, container, false);
        WifiSummary wifiSummary = new WifiSummary(getActivity(), DataStorageConstants.CARD_WIFI_USAGE);
        wifiSummary.inflateWifiData(view);

        return view;
    }

}
