package com.getsmartapp.fragments.insights;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getsmartapp.R;
import com.getsmartapp.cards.CallingSummary;
import com.getsmartapp.fragments.BaseFragment;
import com.getsmartapp.lib.constants.DataStorageConstants;

public class CallUsageFragment extends BaseFragment {


    public CallUsageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_call_usage, container, false);
        CallingSummary callingSummary = new CallingSummary(getActivity(), DataStorageConstants.CARD_CALLING_SUMMARY);
        callingSummary.inflateCallsData(rootView);

        return rootView;
    }

}
