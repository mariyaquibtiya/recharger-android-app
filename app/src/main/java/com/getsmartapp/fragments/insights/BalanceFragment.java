package com.getsmartapp.fragments.insights;


import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.getsmartapp.R;
import com.getsmartapp.fragments.BaseFragment;

public class BalanceFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private SwipeRefreshLayout mMainBalanceLayout, mDataBalanceLayout;
    private TextView dataBalanceValue, mainBalanceValue, mainLastUpdated, dataLastUpdated, mainExpiry, dataExpiry;

    public BalanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_balance, container, false);

        mMainBalanceLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.main_balance_layout);
        Drawable corner_rect_smartsave_bg = getResources().getDrawable(R.drawable.corner_rect_smartsave_bg);
        mMainBalanceLayout.findViewById(R.id.main_balance_content_layout).setBackgroundDrawable(corner_rect_smartsave_bg);

        mDataBalanceLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.data_balance_layout);
        Drawable corner_rect_enable_smartsave_bg = getResources().getDrawable(R.drawable.corner_rect_enable_smartsave_bg);
        mDataBalanceLayout.findViewById(R.id.main_balance_content_layout).setBackgroundDrawable(corner_rect_enable_smartsave_bg);

        initializeBalanceLayouts();

        mMainBalanceLayout.setOnRefreshListener(this);
        mMainBalanceLayout.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                Color.RED, Color.CYAN);
        mMainBalanceLayout.setDistanceToTriggerSync(20);// in dips
        mMainBalanceLayout.setSize(SwipeRefreshLayout.DEFAULT);// LARGE also can be used


        mDataBalanceLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mDataBalanceLayout.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        mDataBalanceLayout.setRefreshing(true);
                        handler.sendEmptyMessage(0);
                    }
                }, 1000);
            }
        });
        mDataBalanceLayout.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                Color.RED, Color.CYAN);
        mDataBalanceLayout.setDistanceToTriggerSync(20);// in dips
        mDataBalanceLayout.setSize(SwipeRefreshLayout.DEFAULT);// LARGE also can be used

        return rootView;
    }

    private void initializeBalanceLayouts() {

//        View darkMainView = mMainBalanceLayout.findViewById(R.id.darker_layout);
//        GradientDrawable bg = (GradientDrawable) darkMainView.getBackground();
//        bg.setColor(getResources().getColor(R.color.light_blue_dark));

//        View lightMainView = mMainBalanceLayout.findViewById(R.id.lighter_layout);
//        GradientDrawable bgLight = (GradientDrawable) lightMainView.getBackground();
//        bgLight.setColor(getResources().getColor(R.color.enable_smartsave_bg));

//        View darkDataView = mDataBalanceLayout.findViewById(R.id.darker_layout);
//        GradientDrawable bgdata = (GradientDrawable) darkDataView.getBackground();
//        bgdata.setColor(getResources().getColor(R.color.purple_dark));

//        View lightDataView = mDataBalanceLayout.findViewById(R.id.lighter_layout);
//        GradientDrawable bgdataLight = (GradientDrawable) lightDataView.getBackground();
//        bgdataLight.setColor(getResources().getColor(R.color.purple_light));

        TextView main_tv = (TextView) mMainBalanceLayout.findViewById(R.id.balance_type);
        main_tv.setText(getString(R.string.main_balance));

        TextView balance_tv = (TextView) mDataBalanceLayout.findViewById(R.id.balance_type);
        balance_tv.setText(getString(R.string.data_balance));

        dataBalanceValue = (TextView) mDataBalanceLayout.findViewById(R.id.balance_type_value);
        dataBalanceValue.setText("250 MB");

        mainBalanceValue = (TextView) mMainBalanceLayout.findViewById(R.id.balance_type_value);

        dataLastUpdated = (TextView) mDataBalanceLayout.findViewById(R.id.last_updated_value);
        dataLastUpdated.setText("12 April 2016");

        mainLastUpdated = (TextView) mMainBalanceLayout.findViewById(R.id.last_updated_value);

        dataExpiry = (TextView) mDataBalanceLayout.findViewById(R.id.expires_on_value);
        dataExpiry.setText("20 April 2016");

        mainExpiry = (TextView) mMainBalanceLayout.findViewById(R.id.expires_on_value);
    }

    @Override
    public void onRefresh() {

        mMainBalanceLayout.postDelayed(new Runnable() {

            @Override
            public void run() {
                mMainBalanceLayout.setRefreshing(true);
                handler.sendEmptyMessage(0);
            }
        }, 1000);
    }

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            if (mMainBalanceLayout.isRefreshing()) {
                mMainBalanceLayout.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getActivity(),
                                "main balance refreshed", Toast.LENGTH_SHORT).show();
                        mMainBalanceLayout.setRefreshing(false);
                    }
                }, 1000);
            }
            if (mDataBalanceLayout.isRefreshing()) {
                mDataBalanceLayout.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getActivity(),
                                "data balance refreshed", Toast.LENGTH_SHORT).show();
                        mDataBalanceLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        }
    };

}
