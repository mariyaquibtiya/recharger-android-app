package com.getsmartapp.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.activity.CallDetailsActivity;
import com.getsmartapp.activity.HomeActivity;
import com.getsmartapp.constants.Constants;
import com.getsmartapp.customviews.circleindicator.CirclePageIndicator;
import com.getsmartapp.fragments.insights.BalanceFragment;
import com.getsmartapp.fragments.insights.CallUsageFragment;
import com.getsmartapp.fragments.insights.DataUsageFragment;
import com.getsmartapp.fragments.insights.WifiUsageFragment;

import java.util.ArrayList;
import java.util.List;

public class BaseInsightsFragment extends BaseFragment implements ViewPager.OnPageChangeListener{

    private int mShortAnimationDuration;
    private ViewGroup mBottomLayout;
    private ViewPager mInsightsViewPager;
    private Handler mHandler;
    private CirclePageIndicator mCirclePagerIndicator;

    public BaseInsightsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_base_insight, container, false);

        final View lockedView = rootView.findViewById(R.id.locked_view);
        mInsightsViewPager = (ViewPager) rootView.findViewById(R.id.insights_view_pager);
        mCirclePagerIndicator = (CirclePageIndicator) rootView.findViewById(R.id.circle_page_indicator);
        mBottomLayout = (ViewGroup) rootView.findViewById(R.id.bottom_layout);
        mShortAnimationDuration = getResources().getInteger(R.integer.fadeAnimTime);

        final SharedPreferences sharedPreferences = ((HomeActivity) getActivity()).getSharedPreferences();

        if (sharedPreferences.getBoolean(Constants.ENABLED_SMARTSAVE, false)) {

            if (lockedView.getVisibility() == View.VISIBLE) {
                lockedView.setVisibility(View.GONE);
                mInsightsViewPager.setVisibility(View.VISIBLE);
                mBottomLayout.setVisibility(View.VISIBLE);
            }

            initializeViewPagerAdapter();

        } else {
            lockedView.setVisibility(View.VISIBLE);
            mInsightsViewPager.setVisibility(View.INVISIBLE);
            mBottomLayout.setVisibility(View.INVISIBLE);

            lockedView.findViewById(R.id.enable_smartsave_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(Constants.ENABLED_SMARTSAVE, true);
                    editor.apply();

                    lockedView.setVisibility(View.GONE);
                    mInsightsViewPager.setVisibility(View.VISIBLE);
                    mBottomLayout.setVisibility(View.VISIBLE);

                    initializeViewPagerAdapter();
                }
            });
        }

        return rootView;
    }

    private void initializeViewPagerAdapter() {
        ArrayList<String> fragmentNames = new ArrayList<>();
        fragmentNames.add(0, BalanceFragment.class.getName());
        fragmentNames.add(1, DataUsageFragment.class.getName());
        fragmentNames.add(2, CallUsageFragment.class.getName());
        fragmentNames.add(3, WifiUsageFragment.class.getName());

        InsightsPagerAdapter fragmentPagerAdapter = new InsightsPagerAdapter(getActivity(), getChildFragmentManager(), fragmentNames);
        mInsightsViewPager.setAdapter(fragmentPagerAdapter);
        mInsightsViewPager.addOnPageChangeListener(this);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                updatePagerBottomLayout(mInsightsViewPager.getCurrentItem());
            }
        });
        mCirclePagerIndicator.setViewPager(mInsightsViewPager);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(final int position) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                updatePagerBottomLayout(position);
            }
        });
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void updatePagerBottomLayout(final int position) {

        // Animate the "hide" view to 0% opacity. After the animation ends, set its visibility
        // to GONE as an optimization step (it won't participate in layout passes, etc.)
        mBottomLayout.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                        updateBottomLayoutValues(position);
                        mBottomLayout.animate()
                                .alpha(1f)
                                .setDuration(mShortAnimationDuration)
                                .setListener(null);
                        //hideView.setVisibility(View.INVISIBLE);
                    }
                });


    }

    private String[] titles = { "Recent Deductions", "Top Apps", "Top Contacts", "Top Wifis" };

    private void updateBottomLayoutValues(int pagePosition) {

        TextView title = (TextView) mBottomLayout.findViewById(R.id.bottom_lay_title);
        title.setText(titles[pagePosition]);

        switch (pagePosition) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
        LinearLayout detailsLayout = (LinearLayout) mBottomLayout.findViewById(R.id.details_layout);
        detailsLayout.removeAllViews();

        if (getActivity() != null) {
            for (int i = 0; i < 5; i++) {
                View view = View.inflate(getActivity(), R.layout.details_list_item, null);
                detailsLayout.addView(view);
            }
        }

        detailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CallDetailsActivity.class);
                startActivity(intent);
            }
        });
    }

    class InsightsPagerAdapter extends FragmentPagerAdapter {

        Context mCtx;
        List<String> mFragmentNames;

        public InsightsPagerAdapter(Context context, FragmentManager fm, ArrayList<String> fragmentNames) {
            super(fm);
            mCtx = context;
            mFragmentNames = fragmentNames;
        }

        @Override
        public Fragment getItem(int position) {

            return Fragment.instantiate(mCtx, mFragmentNames.get(position));
        }

        @Override
        public int getCount() {
            return mFragmentNames.size();
        }

    }
}
