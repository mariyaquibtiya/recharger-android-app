package com.getsmartapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.getsmartapp.data.DBHelper;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.utils.ApplicationContextUtil;
import com.getsmartapp.sharedPreference.PushNotificationPrefManager;
import com.getsmartapp.util.AppUtils;
import com.getsmartapp.util.BranchAndParseUtils;
import com.getsmartapp.util.CustomNotificationFactory;
import com.getsmartapp.util.PersistentNotification;
import com.urbanairship.UAirship;
import com.urbanairship.push.notifications.NotificationActionButton;
import com.urbanairship.push.notifications.NotificationActionButtonGroup;

import io.branch.referral.Branch;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class RechargerApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks{


    public static RechargerApplication sApplicationController;
    private static boolean sIsAppInForeground = false;


    public static RechargerApplication getGlobalInstance() {

        return sApplicationController;
    }

    public boolean isApplicationInForeground()
    {
        return sIsAppInForeground;
    }

    public void setApplicationForegroundState(boolean foregroundState) {
        sIsAppInForeground = foregroundState;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {

        super.onCreate();
        Branch.getAutoInstance(this);
        PersistentNotification.getInstance(this);
        ApplicationContextUtil.getInstance().setApplicationContext(this);
        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        DBHelper dbHelper = DBHelper.getInstance(this);//new DBHelper(this);
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();

        //TODO:UNCOMMENT_LATER
        //Fabric.with(this, new Crashlytics());

        registerActivityLifecycleCallbacks(this);
        sApplicationController = this;
        sharedPrefManager.setBooleanValue(Constants.SENDING_DATA,false);
        initVariables();

        //UA Initiation
        UAirship.takeOff(this, new UAirship.OnReadyCallback() {
            @Override
            public void onAirshipReady(UAirship airship) {
                CustomNotificationFactory notificationFactory;
                notificationFactory = new CustomNotificationFactory(getApplicationContext());
                // Custom notification icon
                notificationFactory.setSmallIconId(R.drawable.notification);
                // The accent color for Android Lollipop+
                notificationFactory.setColor(getResources().getColor(R.color.notification_bg));
                // Set the factory on the PushManager
                airship.getPushManager().setNotificationFactory(notificationFactory);
            }
        });

        UAirship.shared().getPushManager().setUserNotificationsEnabled(true);

        initializeCustomNotificationButtonGroups();

        PushNotificationPrefManager pushNotificationPrefManager = PushNotificationPrefManager.getInstance(getApplicationContext());
        if (!pushNotificationPrefManager.getIsLaunched() || sharedPrefManager.getIntValue("pushenabled") == 0) {
            sharedPrefManager.setIntValue(Constants.pushPromotionalPref, 1);
            sharedPrefManager.setIntValue(Constants.pushTransPref, 1);
            sharedPrefManager.setIntValue(Constants.pushWifiPref, 1);
            BranchAndParseUtils.subscribeToPushTag(getApplicationContext(), Constants.pushPromotionalPref);
            BranchAndParseUtils.subscribeToPushTag(getApplicationContext(), Constants.pushTransPref);
            pushNotificationPrefManager.setIsLaunched(Boolean.TRUE);
            sharedPrefManager.setIntValue("pushenabled", 1);
        }

        /*  Calligraphy Custom Fonts   */
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Bariol_Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        if(!sharedPrefManager.getBooleanValue(Constants.IS_REFERRAL_IMAGE_SAVED, Boolean.FALSE)) {
            if(AppUtils.saveReferralImage(this)) {
                sharedPrefManager.setBooleanValue(Constants.IS_REFERRAL_IMAGE_SAVED, Boolean.TRUE);
            }
        }
    }

    //TODO:UNCOMMENT_LATER
    /*public native String[] stringFromMethod();
    static {
        try {
            // Load necessary libraries.
            System.loadLibrary("recharger");
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
        }
    }
*/

    private void initVariables() {
        //TODO:UNCOMMENT_LATER
        /*String[] variables = stringFromMethod();

        if(variables.length >= 3)
            Constants.encyptionSalt = variables[2];*/
    }

    private void initializeCustomNotificationButtonGroups() {

        //Initializing Custom UA Buttons
        NotificationActionButton updateNpwButtonAction = new NotificationActionButton.Builder("UpdateNow")
                .setLabel(R.string.updateNowbtn)
                .setIcon(R.drawable.cua_update)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton learnMoreButtonAction = new NotificationActionButton.Builder("LearnMore")
                .setLabel(R.string.learnMoreBtn)
                .setIcon(R.drawable.cua_learn)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton tryItButtonAction = new NotificationActionButton.Builder("TryIt")
                .setLabel(R.string.tryItBtn)
                .setIcon(R.drawable.cua_try)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton knowMoreButtonAction = new NotificationActionButton.Builder("KnowMore")
                .setLabel(R.string.knowMoreBtn)
                .setIcon(R.drawable.cua_learn)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton reviewPreferenceButtonAction = new NotificationActionButton.Builder("ReviewPreferences")
                .setLabel(R.string.reviewPreferencesBtn)
                .setIcon(R.drawable.cua_pref)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton loveItButtonAction = new NotificationActionButton.Builder("LoveIt")
                .setLabel(R.string.loveItBtn)
                .setIcon(R.drawable.cua_love)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton hateItButtonAction = new NotificationActionButton.Builder("HateIt")
                .setLabel(R.string.hateItBtn)
                .setIcon(R.drawable.cua_hate)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton addCardButtonAction = new NotificationActionButton.Builder("AddCard")
                .setLabel(R.string.addCardBtn)
                .setIcon(R.drawable.cua_card)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton reviewPermissionButtonAction = new NotificationActionButton.Builder("ReviewPermission")
                .setLabel(R.string.reviewPermissionBtn)
                .setIcon(R.drawable.cua_permissions)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton answerQuestionsButtonAction = new NotificationActionButton.Builder("AnswerQuestions")
                .setLabel(R.string.answerQuestionsBtn)
                .setIcon(R.drawable.cua_answer)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton exploreComboButtonAction = new NotificationActionButton.Builder("ExploreCombo")
                .setLabel(R.string.exploreComboBtn)
                .setIcon(R.drawable.explore_combo)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton exploreNowButtonAction = new NotificationActionButton.Builder("ExploreNow")
                .setLabel(R.string.exploreNowBtn)
                .setIcon(R.drawable.explore_now)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton exploreOptionsButtonAction = new NotificationActionButton.Builder("ExploreOptions")
                .setLabel(R.string.exploreOptionsBtn)
                .setIcon(R.drawable.explore_options)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton findPlansButtonAction = new NotificationActionButton.Builder("FindPlans")
                .setLabel(R.string.findPlansBtn)
                .setIcon(R.drawable.find_plan)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton getReferredButtonAction = new NotificationActionButton.Builder("GetReferred")
                .setLabel(R.string.getReferredBtn)
                .setIcon(R.drawable.get_referred)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton inviteNowButtonAction = new NotificationActionButton.Builder("InviteNow")
                .setLabel(R.string.inviteNowBtn)
                .setIcon(R.drawable.invite_now)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton letsRetryButtonAction = new NotificationActionButton.Builder("LetsRetry")
                .setLabel(R.string.letsRetryBtn)
                .setIcon(R.drawable.lets_retry)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton letsTryButtonAction = new NotificationActionButton.Builder("LetsTry")
                .setLabel(R.string.letsTryBtn)
                .setIcon(R.drawable.lets_try)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton payBillButtonAction = new NotificationActionButton.Builder("PayBill")
                .setLabel(R.string.payBillBtn)
                .setIcon(R.drawable.pay_bill)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton rechargeNowButtonAction = new NotificationActionButton.Builder("RechargeNow")
                .setLabel(R.string.rechargeNowBtn)
                .setIcon(R.drawable.recharge_now)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton redeemDiscountButtonAction = new NotificationActionButton.Builder("RedeemDiscount")
                .setLabel(R.string.redeemDiscountBtn)
                .setIcon(R.drawable.redeem_discount)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton redeemCashbackButtonAction = new NotificationActionButton.Builder("RedeemCashback")
                .setLabel(R.string.redeemCashbackBtn)
                .setIcon(R.drawable.redeem_discount)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton showDetailsButtonAction = new NotificationActionButton.Builder("ShowDetails")
                .setLabel(R.string.showDetailsBtn)
                .setIcon(R.drawable.show_details)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton showPlansButtonAction = new NotificationActionButton.Builder("ShowPlans")
                .setLabel(R.string.showPlansBtn)
                .setIcon(R.drawable.show_plans)
                .setPerformsInForeground(true)
                .build();

        NotificationActionButton showWalletButtonAction = new NotificationActionButton.Builder("ShowWallet")
                .setLabel(R.string.showWalletBtn)
                .setIcon(R.drawable.show_wallet)
                .setPerformsInForeground(true)
                .build();

        //Creating Button Groups
        //New Version Button Group
        NotificationActionButtonGroup newVersionButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(updateNpwButtonAction)
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        //New Feature Button Group
        NotificationActionButtonGroup newFeatureButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(tryItButtonAction)
                .addNotificationActionButton(knowMoreButtonAction)
                .build();

        //Review Preferences Button Group
        NotificationActionButtonGroup reviewPreferencesButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(reviewPreferenceButtonAction)
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        //Feedback Button Group
        NotificationActionButtonGroup feedbackButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(loveItButtonAction)
                .addNotificationActionButton(hateItButtonAction)
                .build();

        //Add Card Button Group
        NotificationActionButtonGroup addCardButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(addCardButtonAction)
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        //News Alert Button Group
        NotificationActionButtonGroup newsAlertButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        //Review Permission Button Group
        NotificationActionButtonGroup reviewPermissionButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(reviewPermissionButtonAction)
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        //Customer Survey Button Group
        NotificationActionButtonGroup customerSurveyButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(answerQuestionsButtonAction)
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        //Contextual Usage Button Group
        NotificationActionButtonGroup contextualUsageButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(learnMoreButtonAction)
                .build();

        NotificationActionButtonGroup exploreComboButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(exploreComboButtonAction)
                .build();

        NotificationActionButtonGroup exploreNowButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(exploreNowButtonAction)
                .build();

        NotificationActionButtonGroup exploreOptionsButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(exploreOptionsButtonAction)
                .build();

        NotificationActionButtonGroup findPlansButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(findPlansButtonAction)
                .build();

        NotificationActionButtonGroup getReferredButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(getReferredButtonAction)
                .build();

        NotificationActionButtonGroup inviteNowButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(inviteNowButtonAction)
                .build();

        NotificationActionButtonGroup letsRetryButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(letsRetryButtonAction)
                .build();

        NotificationActionButtonGroup letsTryButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(letsTryButtonAction)
                .build();

        NotificationActionButtonGroup payBillButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(payBillButtonAction)
                .build();

        NotificationActionButtonGroup rechargeNowButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(rechargeNowButtonAction)
                .build();

        NotificationActionButtonGroup redeemDiscountButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(redeemDiscountButtonAction)
                .build();

        NotificationActionButtonGroup redeemCashbackButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(redeemCashbackButtonAction)
                .build();

        NotificationActionButtonGroup showDetailsButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(showDetailsButtonAction)
                .build();

        NotificationActionButtonGroup showPlansButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(showPlansButtonAction)
                .build();

        NotificationActionButtonGroup showWalletButtonGroup = new NotificationActionButtonGroup.Builder()
                .addNotificationActionButton(showWalletButtonAction)
                .build();

        UAirship.shared().getPushManager().addNotificationActionButtonGroup("NewAppVersionGroup", newVersionButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("NewFeatureGroup", newFeatureButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ReviewPreferenceGroup", reviewPreferencesButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("FeedbackGroup", feedbackButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("AddCardGroup", addCardButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("NewsAlertGroup", newsAlertButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ReviewPermissionGroup", reviewPermissionButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("CustomerSurveyGroup", customerSurveyButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ContextualUsageGroup", contextualUsageButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ExploreComboGroup", exploreComboButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ExploreNowGroup", exploreNowButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ExploreOptionsGroup", exploreOptionsButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("FindPlansGroup", findPlansButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("GetReferredGroup", getReferredButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("InviteNowGroup", inviteNowButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("LetsRetryGroup", letsRetryButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("LetsTryGroup", letsTryButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("PayBillGroup", payBillButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("RechargeNowGroup", rechargeNowButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("RedeemDiscountGroup", redeemDiscountButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("RedeenCashbackGroup", redeemCashbackButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ShowDetailsGroup", showDetailsButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ShowPlansGroup", showPlansButtonGroup);
        UAirship.shared().getPushManager().addNotificationActionButtonGroup("ShowWalletGroup", showWalletButtonGroup);
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}