package com.getsmartapp.managers;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.getsmartapp.R;
import com.getsmartapp.activity.HomeActivity;
import com.getsmartapp.activity.SplashActivity;
import com.getsmartapp.data.DBHelper;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.sharedPreference.BranchPrefManager;
import com.getsmartapp.sharedPreference.PushNotificationPrefManager;
import com.getsmartapp.util.AppUtils;
import com.getsmartapp.util.BranchAndParseUtils;
import com.getsmartapp.util.CardUtils;
import com.getsmartapp.util.Lg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

//TODO:UNCOMMENT_LATER
//import com.freshdesk.mobihelp.Mobihelp;

/**
 * @author bishwajeet.kumar on 08/09/15.
 */
public class ParseDeepLinkManager {
    private static final String TAG = "ParseDeepLinkActivity";
    public static final String RECHARGE_DEEP_LINK = "/recharge";
    public static final String REFERRAL_DEEP_LINK = "/referral";
    public static final String TRANSACTIONAL_DEEP_LINK = "/ordersummary";
    public static final String GETREFERRED_DEEP_LINK = "/getreferred";
    public static final String SMARTCOMBO_DEEP_LINK = "/smartcombo";
    public static final String INSTREAM_BROWSEPLANS_DEEP_LINK = "/instream";
    public static final String MESSAGE_INBOX_DEEP_LINK = "/messageinbox";
    public static final String SETTINGS_DEEP_LINK = "/settings";
    public static final String MANAGE_CARDS_DEEP_LINK = "/managecards";
    public static final String STD_CALLING_DEEP_LINK = "/stdcalling";
    public static final String CALLING_USAGE_DEEP_LINK = "/callingUsage";
    public static final String DATA_USAGE_DEEP_LINK = "/dataUsage";
    public static final String WIFI_USAGE_DEEP_LINK = "/wifiusage";
    public static final String RECENT_CONTACTS_DEEP_LINK = "/recentContacts";
    public static final String ROAMING_USAGE_DEEP_LINK = "/roamingUsage";
    public static final String WALLET_DEEP_LINK = "/wallet";
    public static final String CONTACT_US_DEEP_LINK = "/contactus";
    public static final String QUICK_HELP_DEEP_LINK = "/quickhelp";
    public static final String WIFI_HOTSPOT_DEEP_LINK = "/hotspot";


    static ParseDeepLinkManager parseDeepLinkManager;
    SplashActivity mContext;
    SharedPrefManager mSharedPreferences;
    private SQLiteDatabase sqLiteDatabase;
    private Intent mOnBoardingIntent;

    public static ParseDeepLinkManager getInstance(SplashActivity context) {
        if (parseDeepLinkManager == null)
            parseDeepLinkManager = new ParseDeepLinkManager(context);
        return parseDeepLinkManager;
    }

    ParseDeepLinkManager(SplashActivity context) {
        mContext = context;
        mSharedPreferences = new SharedPrefManager(mContext);
        DBHelper dbHelper = DBHelper.getInstance(mContext);//new DBHelper(SplashActivity.this);
        sqLiteDatabase = dbHelper.getReadableDatabase();
    }

    public boolean isHavingDeepLink(Intent intent) {
        // Parse deep link URI

        if (mSharedPreferences.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER).trim().length() == 0 || mSharedPreferences.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) != 1) {
            return false;
        }

        String deepLink = "";

        if (intent != null && intent.getData() != null) {
            Log.e("PeeyushKS", "Deep Link 3");

            deepLink = intent.getData().getPath();
        } else {
            Log.e("PeeyushKS", "Deep Link 4 : " + intent.getData());
            return false;
        }

        Log.e("PeeyushKS", "Deep Link: " + intent.getData());

        String dataString = intent.getDataString();

        Log.e("PeeyushKS", "Deep Link: " + intent.getData().getHost());
        Log.e("PeeyushKS", "Deep Link: " + intent.getData().getScheme());
        Log.e("PeeyushKS", "Deep Link String: " + intent.getDataString());

        //Branch handling
        if (!TextUtils.isEmpty(dataString) && (dataString.contains("com.getsmartapp://open") || dataString.contains("com.getsmartapp://smartapp"))) {
            branchHandling(intent);
            return true;
        }

        if (TextUtils.isEmpty(deepLink)) {
            return false;
        }

        Intent launchActivity = new Intent();
        try {
            PushNotificationPrefManager pushNotificationPrefManager = PushNotificationPrefManager.getInstance(mContext);
            pushNotificationPrefManager.setIsDeeplinkPush(Boolean.TRUE);
            doThisTaskBeforHomeIntent();
            switch (deepLink) {
                //TODO:UNCOMMENT_LATER
                /*case RECHARGE_DEEP_LINK:
                    if (HomeActivity.getInstance() != null) {
                        HomeActivity homeScreen = HomeActivity.getInstance();
                        homeScreen.OnDrawerItemClick(1);
                        mContext.finish();
                    } else {
                        AppUtils.updateCards(mContext);
                        //This method is called to hit all plans api before home launch
                        launchActivity.setClass(mContext, HomeActivity.class);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchActivity.putExtra("selected_pos", 1);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                        mContext.finish();
                    }
                    break;
                case REFERRAL_DEEP_LINK:
                    if (HomeActivity.getInstance() != null) {
                        Lg.e("DEEPLINK", "creating new instance");
                        HomeActivity homeScreen = HomeActivity.getInstance();
                        homeScreen.OnDrawerItemClick(3);
                        mContext.finish();
                    } else {
                        Lg.e("DEEPLINK", "creating new intent");
                        AppUtils.updateCards(mContext);
                        //This method is called to hit all plans api before home launch
                        launchActivity.setClass(mContext, HomeActivity.class);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchActivity.putExtra("selected_pos", 3);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                        mContext.finish();
                    }
                    break;
                case TRANSACTIONAL_DEEP_LINK:
                    launchActivity.setClass(mContext, StatusActivity.class);
                    launchActivity.putExtra("from", "PushNotification");
                    launchActivity.putExtra("transId", pushNotificationPrefManager.getOrderIDfromPush());
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case GETREFERRED_DEEP_LINK:
                    launchActivity.setClass(mContext, ReferralApplyCodeActivity.class);
                    launchActivity.putExtra(BundleConstants.FROM, "Push");
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case SMARTCOMBO_DEEP_LINK:
                    String mComboPlanInnerCardString = mSharedPreferences.getStringValue(BundleConstants.SMARTCOMBOPUSH);
                    if ((mComboPlanInnerCardString != null) && (!mComboPlanInnerCardString.equals("")) && (!mComboPlanInnerCardString.contains("We could not find a combo to cover your usage"))) {
                        launchActivity.setClass(mContext, ComboRecommendation.class);
                        launchActivity.putExtra(BundleConstants.COMBOPLAN, mComboPlanInnerCardString);
                        mContext.startActivity(launchActivity);
                    } else {
                        Lg.e(TAG, "Unknown deep link: " + deepLink + ". Falling back to main activity.");
                        launchActivity.setClass(mContext, SplashActivity.class);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                    }
                    mContext.finish();
                    break;
                case INSTREAM_BROWSEPLANS_DEEP_LINK:
                    String phoneNo = mSharedPreferences.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER);
                    String sTypeId = mSharedPreferences.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
                    int providerId = mSharedPreferences.getIntValue(Constants.ON_BOARDING_PROVIDER_ID);
                    String providerName = mSharedPreferences.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
                    int circleId = mSharedPreferences.getIntValue(Constants.ON_BOARDING_CIRCLE_ID);
                    int sTypeID = 1;
                    if (sTypeId.equalsIgnoreCase("postpaid"))
                        sTypeID = 2;

                    String instreamType = "special";
                    launchActivity.setClass(mContext, BrowsePlan_New.class);
                    launchActivity.putExtra(Constants.NUMBER, phoneNo);
                    launchActivity.putExtra(Constants.PROVIDER_NAME, providerName);
                    launchActivity.putExtra(BundleConstants.PROVIDER_ID, providerId);
                    launchActivity.putExtra(BundleConstants.CIRCLE_ID, circleId);
                    launchActivity.putExtra(BundleConstants.FROM, HomeActivity.class.getName());
                    launchActivity.putExtra(BundleConstants.TYPEID, sTypeID);
                    if ("data".equals(instreamType)) {
                        launchActivity.putExtra(BundleConstants.LANDING, BundleConstants.TOO_MUCH_DATA);
                    } else if ("special".equals(instreamType)) {
                        launchActivity.putExtra(BundleConstants.LANDING, BundleConstants.SLASH_N_DASH);
                    }
                    launchActivity.putExtra(Constants.CATEGORYID, 4);
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case MESSAGE_INBOX_DEEP_LINK:
                    if (HomeActivity.getInstance() != null) {
                        Lg.e("DEEPLINK", "creating new instance");
                        HomeActivity homeScreen = HomeActivity.getInstance();
                        homeScreen.OnDrawerItemClick(5);
                        mContext.finish();
                    } else {
                        Lg.e("DEEPLINK", "creating new intent");
                        AppUtils.updateCards(mContext);
                        //This method is called to hit all plans api before home launch
                        launchActivity.setClass(mContext, HomeActivity.class);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchActivity.putExtra("selected_pos", 5);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                        mContext.finish();
                    }
                    break;
                case SETTINGS_DEEP_LINK:
                    if (HomeActivity.getInstance() != null) {
                        Lg.e("DEEPLINK", "creating new instance");
                        HomeActivity homeScreen = HomeActivity.getInstance();
                        homeScreen.OnDrawerItemClick(8);
                        mContext.finish();
                    } else {
                        Lg.e("DEEPLINK", "creating new intent");
                        AppUtils.updateCards(mContext);
                        //This method is called to hit all plans api before home launch
                        launchActivity.setClass(mContext, HomeActivity.class);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchActivity.putExtra("selected_pos", 8);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                        mContext.finish();
                    }
                    break;
                case MANAGE_CARDS_DEEP_LINK:
                    if (AppUtils.isLoggedIn(mContext)) {
                        launchActivity.setClass(mContext, ManageCardsActivity.class);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivityForResult(launchActivity, Constants.FRAG_BACK_REQUEST);
                        mContext.finish();
                    }
                    break;
                case STD_CALLING_DEEP_LINK:
                    launchActivity.setClass(mContext, STDUsage.class);
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case CALLING_USAGE_DEEP_LINK:
                    launchActivity.setClass(mContext, CallingUsages.class);
                    launchActivity.putExtra(BundleConstants.FROM, MessageActivity.class.getName());
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case DATA_USAGE_DEEP_LINK:
                    launchActivity.setClass(mContext, DataUsage1.class);
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case ROAMING_USAGE_DEEP_LINK:
                    launchActivity.setClass(mContext, RoamingUsages.class);
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case RECENT_CONTACTS_DEEP_LINK:
                    Map<String, String> recentContact = InAppContextualUsageUtil.recentContactDetail(mContext);
                    launchActivity.setClass(mContext, RecentContactDataUsageActivity.class);
                    launchActivity.putExtra(BundleConstants.PHONE, recentContact.get("phone"));
                    launchActivity.putExtra(BundleConstants.CONTACT_NAME, recentContact.get("name"));
                    mContext.startActivity(launchActivity);
                    mContext.finish();
                    break;
                case WALLET_DEEP_LINK:
                    if (HomeActivity.getInstance() != null) {
                        Lg.e("DEEPLINK", "creating new instance");
                        HomeActivity homeScreen = HomeActivity.getInstance();
                        homeScreen.OnDrawerItemClick(2);
                        mContext.finish();
                    } else {
                        Lg.e("DEEPLINK", "creating new intent");
                        AppUtils.updateCards(mContext);
                        //This method is called to hit all plans api before home launch
                        launchActivity.setClass(mContext, HomeActivity.class);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchActivity.putExtra("selected_pos", 2);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                        mContext.finish();
                    }
                    break;
                case CONTACT_US_DEEP_LINK:
                    Mobihelp.clearCustomData(mContext);
                    Mobihelp.showConversations(mContext);
                    mContext.finish();
                    break;
                case QUICK_HELP_DEEP_LINK:
                    if (HomeActivity.getInstance() != null) {
                        Lg.e("DEEPLINK", "creating new instance");
                        HomeActivity homeScreen = HomeActivity.getInstance();
                        homeScreen.OnDrawerItemClick(6);
                        mContext.finish();
                    } else {
                        Lg.e("DEEPLINK", "creating new intent");
                        AppUtils.updateCards(mContext);
                        //This method is called to hit all plans api before home launch
                        launchActivity.setClass(mContext, HomeActivity.class);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        launchActivity.putExtra("selected_pos", 6);
                        launchActivity.setAction(Long.toString(Calendar.getInstance().getTimeInMillis()));
                        mContext.startActivity(launchActivity);
                        mContext.finish();
                    }
                    break;

                case WIFI_HOTSPOT_DEEP_LINK:
                    launchActivity.setClass(mContext, ShowWifiList.class);
                    launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY
                            | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    mContext.startActivity(launchActivity);
                    mContext.finish();

                    break;*/
                default:
                    Lg.e(TAG, "Unknown deep link: " + deepLink + ". Falling back to main activity.");
                    return false;
            }
        } catch (Exception e) {
            Lg.e(TAG, "Unknown deep link: " + deepLink + ". Falling back to main activity.");
            return false;
        }
        //TODO:UNCOMMENT_LATER
        // return true;
    }

    private void branchHandling(Intent intent) {
        if (intent != null && intent.getData() != null) {

            Branch.getInstance(mContext.getApplicationContext()).initSession(new Branch.BranchReferralInitListener() {
                @Override
                public void onInitFinished(JSONObject referringParams, BranchError branchError) {
                    BranchPrefManager prefs = BranchPrefManager.getInstance(mContext.getApplicationContext());
                    int isUserOnBoardedSuccessfully = mSharedPreferences.getIntValue(Constants
                            .USER_ON_BOARDED_SUCCESSFULLY);
                    if (isUserOnBoardedSuccessfully == 1) {
                        Cursor cursor = null;
                        try {
                            cursor = sqLiteDatabase.rawQuery("SELECT * from " + DBContractor.UserPhoneEntry.TABLE_NAME, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                mOnBoardingIntent = new Intent(mContext, HomeActivity.class);
                                mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                SharedPrefManager sharedPrefManager = new SharedPrefManager(mContext);
                                sharedPrefManager.setStringValue(Constants.CHECK_HIJACK, "YES");
                            }
                        } finally {
                            assert cursor != null;
                            cursor.close();
                        }
                    }
                    if (mOnBoardingIntent != null) {
                        mOnBoardingIntent.putExtra("pos", 0);
                        if (branchError == null) {
                            try {
                                if (referringParams.has(ApiConstants.CURRENT_BRANCH_USER_NAME) && prefs.getReferralBranchUserIdentity() == null) {
                                    prefs.setReferralBranchUserName(referringParams.getString(ApiConstants.CURRENT_BRANCH_USER_NAME));
                                    prefs.setReferralBranchUserIdentity(referringParams.getString(ApiConstants.CURRENT_BRANCH_USER_IDENTITY));
                                    prefs.setReferralUserImageURL(referringParams.getString(ApiConstants.CURRENT_USER_IMAGE_URL));
                                    prefs.setReferralDeviceID(referringParams.getString(ApiConstants.CURRENT_DEVICE_ID));
                                    prefs.setReferralChannel(referringParams.getString(ApiConstants.CURRENT_USER_CHANNEL));
                                    prefs.setCurrentDeviceID(AppUtils.getDeviceIDForSSO(mContext));
                                    prefs.setIsReferredUser(Boolean.TRUE);
                                    prefs.setShowWelcomeMessage(Boolean.TRUE);
                                    BranchAndParseUtils.subscribeAndEventTrackForUA(mContext.getApplicationContext(), "user_getreferred");
                                }
                                if (referringParams.has(ApiConstants.BRANCH_EMAIL_CTA)) {
                                    switch (referringParams.getString(ApiConstants.BRANCH_EMAIL_CTA)) {
                                        //TODO:UNCOMMENT_LATER
                                        /*case "signup":
                                            mOnBoardingIntent.setClass(mContext, LoginSignUpMainActivity.class);
                                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                            break;
                                        case "recharge":
                                            mOnBoardingIntent.setClass(mContext, HomeActivity.class);
                                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            mOnBoardingIntent.putExtra("selected_pos", 1);
                                            AppUtils.updateCards(mContext.getApplicationContext());
                                            break;
                                        case "referral":
                                            mOnBoardingIntent.setClass(mContext, HomeActivity.class);
                                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            mOnBoardingIntent.putExtra("selected_pos", 3);
                                            AppUtils.updateCards(mContext.getApplicationContext());
                                            break;
                                        case "smartcombo":
                                            String mComboPlanInnerCardString = mSharedPreferences.getStringValue(BundleConstants.SMARTCOMBOPUSH);
                                            if ((mComboPlanInnerCardString != null) && (!mComboPlanInnerCardString.equals("")) && (!mComboPlanInnerCardString.contains("We could not find a combo to cover your usage"))) {
                                                mOnBoardingIntent.setClass(mContext.getApplicationContext(), ComboRecommendation.class);
                                                mOnBoardingIntent.putExtra(BundleConstants.COMBOPLAN, mComboPlanInnerCardString);
                                                AppUtils.updateCards(mContext.getApplicationContext());
                                            } else {
                                                mOnBoardingIntent.setClass(mContext, HomeActivity.class);
                                                mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                mOnBoardingIntent.putExtra("selected_pos", 1);
                                                AppUtils.updateCards(mContext.getApplicationContext());
                                            }
                                            break;
                                        case "browseplansdata":
                                            String phoneNo = mSharedPreferences.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER);
                                            String sTypeId = mSharedPreferences.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
                                            int providerId = mSharedPreferences.getIntValue(Constants.ON_BOARDING_PROVIDER_ID);
                                            String providerName = mSharedPreferences.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
                                            int circleId = mSharedPreferences.getIntValue(Constants.ON_BOARDING_CIRCLE_ID);
                                            int sTypeID = 1;
                                            if (sTypeId.equalsIgnoreCase("postpaid"))
                                                sTypeID = 2;

                                            mOnBoardingIntent.setClass(mContext.getApplicationContext(), BrowsePlan_New.class);
                                            mOnBoardingIntent.putExtra(Constants.NUMBER, phoneNo);
                                            mOnBoardingIntent.putExtra(Constants.PROVIDER_NAME, providerName);
                                            mOnBoardingIntent.putExtra(BundleConstants.PROVIDER_ID, providerId);
                                            mOnBoardingIntent.putExtra(BundleConstants.CIRCLE_ID, circleId);
                                            mOnBoardingIntent.putExtra(BundleConstants.FROM, HomeActivity.class.getName());
                                            mOnBoardingIntent.putExtra(BundleConstants.TYPEID, sTypeID);
                                            mOnBoardingIntent.putExtra(BundleConstants.LANDING, BundleConstants.TOO_MUCH_DATA);
                                            mOnBoardingIntent.putExtra(Constants.CATEGORYID, 4);
                                            AppUtils.updateCards(mContext.getApplicationContext());
                                            break;
                                        case "browseplansspecial":
                                            String phoneNoSpl = mSharedPreferences.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER);
                                            String sTypeIdSpl = mSharedPreferences.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
                                            int providerIdSpl = mSharedPreferences.getIntValue(Constants.ON_BOARDING_PROVIDER_ID);
                                            String providerNameSpl = mSharedPreferences.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
                                            int circleIdSpl = mSharedPreferences.getIntValue(Constants.ON_BOARDING_CIRCLE_ID);
                                            int sTypeIDSpl = 1;
                                            if (sTypeIdSpl.equalsIgnoreCase("postpaid"))
                                                sTypeIDSpl = 2;

                                            mOnBoardingIntent.setClass(mContext.getApplicationContext(), BrowsePlan_New.class);
                                            mOnBoardingIntent.putExtra(Constants.NUMBER, phoneNoSpl);
                                            mOnBoardingIntent.putExtra(Constants.PROVIDER_NAME, providerNameSpl);
                                            mOnBoardingIntent.putExtra(BundleConstants.PROVIDER_ID, providerIdSpl);
                                            mOnBoardingIntent.putExtra(BundleConstants.CIRCLE_ID, circleIdSpl);
                                            mOnBoardingIntent.putExtra(BundleConstants.FROM, HomeActivity.class.getName());
                                            mOnBoardingIntent.putExtra(BundleConstants.TYPEID, sTypeIDSpl);
                                            mOnBoardingIntent.putExtra(BundleConstants.LANDING, BundleConstants.SLASH_N_DASH);
                                            mOnBoardingIntent.putExtra(Constants.CATEGORYID, 4);
                                            AppUtils.updateCards(mContext.getApplicationContext());
                                            break;
                                        case "callingusage":
                                            mOnBoardingIntent.setClass(mContext, CallingUsages.class);
                                            mOnBoardingIntent.putExtra(BundleConstants.FROM, HomeActivity.class.getName());
                                            AppUtils.updateCards(mContext.getApplicationContext());
                                            break;
                                        case "datausage":
                                            mOnBoardingIntent.setClass(mContext, DataUsage1.class);
                                            AppUtils.updateCards(mContext.getApplicationContext());
                                            break;*/
                                        default:
                                            break;
                                    }
                                }
                            } catch (JSONException e) {
                                prefs.setShowWelcomeMessage(Boolean.FALSE);
                            }
                        } else {
                            prefs.setShowWelcomeMessage(Boolean.FALSE);
                        }

                        mOnBoardingIntent.putExtra("FROM", SplashActivity.class.getName());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mContext.startActivity(mOnBoardingIntent);
                                mContext.finish();
                                AppUtils.startActivity(mContext);
                            }
                        }, 2000);
                    }

                }
            }, intent.getData(), mContext);
        }
    }

    private String getDeepLink(Intent intent) {
        if (intent != null && intent.getData() != null) {
            return intent.getData().getPath();

        }
        return null;
    }

    private String getDeepLinkQueryParameter(String key) {
        Intent intent = mContext.getIntent();
        if (intent != null && intent.getData() != null) {
            return intent.getData().getQueryParameter(key);
        }

        return null;
    }

    String comboKey;

    private void doThisTaskBeforHomeIntent() {
        SharedPrefManager mSharedPref = new SharedPrefManager(mContext);
        String mPrefferedDataType = mSharedPreferences.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
        if (mPrefferedDataType.equalsIgnoreCase(mContext.getString(R.string.lte))) {
            mSharedPreferences.setStringValue(Constants.ON_BOARDING_PREFERRED_TYPE, mContext.getString(R.string.fourg));
            mPrefferedDataType = mContext.getString(R.string.fourg);
        }

        if (!mSharedPreferences.getBooleanValue(Constants.APP_STARTED_FIRST_TIME) && AppUtils.isConnectingToInternet(mContext)) {

            try {
                String CATEGORYID = "1";
                if (mPrefferedDataType.equalsIgnoreCase("2G")) {
                    CATEGORYID = "1";
                    mPrefferedDataType = "2g";
                } else if (mPrefferedDataType.equalsIgnoreCase("3G") || mPrefferedDataType.equalsIgnoreCase(mContext.getString(R.string.fourg)) || mPrefferedDataType.equalsIgnoreCase("4G/LTE")) {
                    CATEGORYID = "2";
                    mPrefferedDataType = "3g";
                }
                DBHelper dbHelper = DBHelper.getInstance(mContext);
                SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
                String cardJson = CardUtils.getCardRankings(mContext, sqLiteDatabase);
                parseCardsJson(cardJson);
                if (!AppUtils.isStringNullEmpty(comboKey)) {
                    CATEGORYID = "0," + CATEGORYID;
                }


                if (!DateUtil.nDaysGoneSinceOnBoard(mContext, 3)) {
                    HomeScreenConnectionManager.getInstance(mContext).postPlanApiDetailsWebService(mContext, comboKey, CATEGORYID, mPrefferedDataType);
                } else {
                    if (DateUtil.sixHourBefore(mSharedPreferences.getLongValue(Constants.LAST_RECO_CALL_TIMESTAMP))) {
                        HomeScreenConnectionManager.getInstance(mContext).getPlanApiDetailsWebService(mContext, CATEGORYID, mPrefferedDataType);
                    } else {
                        if (mSharedPreferences.getIntValue(Constants.FETCH_FRESH_RECOMMENDATION) == 1) {
                            HomeScreenConnectionManager.getInstance(mContext).getPlanApiDetailsWebService(mContext, CATEGORYID, mPrefferedDataType);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void parseCardsJson(String cardsStr) {
        try {
            JSONObject json = new JSONObject(cardsStr);
            Iterator<String> iter = json.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                if (json.has(key)) {
                    JSONArray jArr = json.getJSONArray(key);
                    if (jArr != null && jArr.length() > 0) {
                        String value = (String) jArr.get(0);
                        if (value.equalsIgnoreCase(DataStorageConstants.CARD_RECOMMEND_COMBO)) {
                            comboKey = key;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
