package com.getsmartapp.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.lotame.android.CrowdControl;
import com.lotame.android.CrowdControl.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class LotameManager {

    private static final String SHARED_PREFERENCES_LOTAME_APP_MANAGE = "SHARED_PREFERENCES_LOTAME_APP_MANAGE";
    private static final String LOTAME_UPDATED_TIME = "LOTAME_UPDATED_TIME";

    private static final String LOTAME_AUDIENCE_ABBR_KEY = "LOTAME_AUDIENCE_ABBR_KEY";
    private static final int LOTAME_BASE_CLIENT_ID = 7633;
    private static int mCounter = 0;
    private static LotameManager mLotameManager = null;
    private static String[] mAudienceInfoArray = null;

    private final int LOTAME_TIMEOUT_MILLIS = 5000;
    private CrowdControl mCrowdControlHttp = null;
    private CrowdControl mCrowdControlHttpBase = null;
    private SharedPreferences mAppPreference;
    private SharedPreferences.Editor mEditor = null;
    public static final int COUNTRY_SESSION_TIME_HRS = 24;

    public static LotameManager getInstance() {
        if (null == mLotameManager) {
            mLotameManager = new LotameManager();
        }
        return mLotameManager;
    }

    /**
     * LotameManager.getInstance().onCreate(context, ClientId);
     *
     * @param context
     * @param lotameClientId
     */
    public void onCreate(Context context, int lotameClientId) {
        // Because no protocol is specified, HTTP will be used.
        if (null == mCrowdControlHttp) {
            mCrowdControlHttp = new CrowdControl(context, lotameClientId);
        }
        if (null == mCrowdControlHttpBase) {
            mCrowdControlHttpBase = new CrowdControl(context, LOTAME_BASE_CLIENT_ID);
        }
    }

    /**
     * LotameManager.getInstance().onCreate(context);
     * @param context
     */
    public void onCreate(Context context) {
        // Because no protocol is specified, HTTP will be used.
        if (null == mCrowdControlHttp) {
            mCrowdControlHttp = new CrowdControl(context, LOTAME_BASE_CLIENT_ID);
        }
        if (null == mCrowdControlHttpBase) {
            mCrowdControlHttpBase = new CrowdControl(context, LOTAME_BASE_CLIENT_ID);
        }
    }


    /**
     * LotameManager.getInstance().onCreate(context, ClientId, protocol);
     *
     * @param context
     * @param lotameClientId
     * @param protocol
     */
    public void onCreate(Context context, int lotameClientId, Protocol protocol) {
        // Instantiating a CrowdControl instance configured for HTTPS
        if (null == mCrowdControlHttp) {
            mCrowdControlHttp = new CrowdControl(context, lotameClientId, protocol);
        }
        if (null == mCrowdControlHttpBase) {
            mCrowdControlHttpBase = new CrowdControl(context, LOTAME_BASE_CLIENT_ID, protocol);
        }
    }

    /**
     * Start Session by calling this method in onStart of your baseActivity
     * LotameManager.getInstance().onStart();
     */
    public void onStart() {
        if (null != mCrowdControlHttp) {
            mCrowdControlHttp.startSession();
        }
        if (null != mCrowdControlHttpBase) {
            mCrowdControlHttpBase.startSession();
        }
    }

    /**
     * LotameManager.getInstance().sendLotameEvents(prefix, eventName);
     * eventName event name separated by colon e.g. Home:Discover
     * Examples-
     *
     * @param prefix    - app.android.gaana:Ver-4.0:
     * @param eventName - home:discover
     */
    public void sendLotameEvents(String prefix, String eventName) {
        collectAndSendSomethingInteresting(prefix + eventName);
    }

    /**
     * LotameManager.getInstance().sendLotameEvents(finalEventName);
     *
     * @param finalEventName Included with the eventNames
     */
    public void sendLotameEvents(String finalEventName) {
        collectAndSendSomethingInteresting(finalEventName);
    }

    /**
     * @param eventName - Final Events used to send to the Lotame
     */
    private void collectAndSendSomethingInteresting(String eventName) {


        // Add data points to collect.  This can be called any number of times
        if (null != mCrowdControlHttp) {
            mCrowdControlHttp.add("int", eventName);
            mCounter++;

            if (mCounter == 1) {
                mCounter = 0;
                sendLotameEventsTask();
            }
        }
    }

    /**
     * Thread that will send the collected Lotame Events to the Server
     */
    private void sendLotameEventsTask() {
        if (mCrowdControlHttp.isInitialized()) {
            try {
                mCrowdControlHttp.bcpAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Boolean hasSessionExpired(String timeThen) {
        long secondThen = Long.parseLong(timeThen);
        long secondNow = new Date().getTime();
        long diff = (secondNow - secondThen) / 1000;

        int diff_int = (int) diff;
        if (diff_int > (COUNTRY_SESSION_TIME_HRS) * 60 * 60) {
            return true;
        } else {
            return false;
        }
    }

    public String[] getAudienceInfoArray(Context context) {

        if (null == mAudienceInfoArray) {
            mAppPreference = context.getSharedPreferences(SHARED_PREFERENCES_LOTAME_APP_MANAGE, Context.MODE_PRIVATE);
            String abbr = mAppPreference.getString(LOTAME_AUDIENCE_ABBR_KEY, "all");

            if (!TextUtils.isEmpty(abbr)) {

                String lastUpdatdTime = mAppPreference.getString(LOTAME_UPDATED_TIME, null);

                if(TextUtils.isEmpty(lastUpdatdTime) || hasSessionExpired(lastUpdatdTime))
                    getAudienceInfo(context);

                mAudienceInfoArray = abbr.split("#");

                return mAudienceInfoArray;
            }

            getAudienceInfo(context);

        }
        return mAudienceInfoArray;
    }

    /**
     * Method to provide the Audience Information/Profile
     *
     * @return
     */
    private void getAudienceInfo(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String jsonData = null;
                if (null != mCrowdControlHttpBase) {
                    try {
                        jsonData = mCrowdControlHttpBase.getAudienceJSON(LOTAME_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

                        if (!TextUtils.isEmpty(jsonData)) {
                            String audienceAbbr = "";

                            JSONObject jObj = new JSONObject(jsonData);
                            JSONArray jArrayAudiences = jObj.getJSONObject("Profile").getJSONObject("Audiences").getJSONArray("Audience");
                            if (null != jArrayAudiences && jArrayAudiences.length() > 0) {
                                for (int i = 0; i < jArrayAudiences.length(); i++) {
                                    if (null != jArrayAudiences.getJSONObject(i) && jArrayAudiences.getJSONObject(i).has("abbr")) {
                                        if (i == 0) {
                                            audienceAbbr = jArrayAudiences.getJSONObject(i).getString("abbr");
                                        } else {
                                            audienceAbbr = audienceAbbr + "#" + jArrayAudiences.getJSONObject(i).getString("abbr");
                                        }
                                    }
                                }

                                mAppPreference = context.getSharedPreferences(SHARED_PREFERENCES_LOTAME_APP_MANAGE, Context.MODE_PRIVATE);
                                mEditor = mAppPreference.edit();
                                if (!TextUtils.isEmpty(audienceAbbr) && !audienceAbbr.equalsIgnoreCase(mAppPreference.getString(LOTAME_AUDIENCE_ABBR_KEY, "all"))) {
                                    mEditor.putString(LOTAME_AUDIENCE_ABBR_KEY, audienceAbbr);
                                    mEditor.putString(LOTAME_UPDATED_TIME, String.valueOf(System.currentTimeMillis()));
                                }
                                mEditor.apply();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}