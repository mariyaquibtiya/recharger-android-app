package com.getsmartapp.managers;

import android.content.Context;
import android.net.TrafficStats;
import android.os.SystemClock;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.getsmartapp.activity.HomeActivity;
import com.getsmartapp.lib.dataAggregation.DataAggregationUtils;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.HomeAllInstreamModel;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.util.AppUtils;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Peeyush.Singh on 25-11-2015.
 */
public class HomeScreenConnectionManager {

    public static HomeAllInstreamModel homeAllInstreamModel;
    private Map<String, String> volleyMap;
    private HomeActivity mHomeScreen;
    private boolean IS_CALLED_VIA_POST;
    private SharedPrefManager sharedPrefManager;
    private boolean IS_CALL_FAILED = false;

    public static HomeAllInstreamModel getHomeAllInstreamModel() {
        return homeAllInstreamModel;
    }

    private static HomeScreenConnectionManager mHomeScreenConnectionManager;

    private HomeScreenConnectionManager(Context context) {
        sharedPrefManager = new SharedPrefManager(context);
    }

    public static HomeScreenConnectionManager getInstance(Context context) {
        if (mHomeScreenConnectionManager == null)
            mHomeScreenConnectionManager = new HomeScreenConnectionManager(context);

        return mHomeScreenConnectionManager;
    }

    public void postPlanApiDetailsWebService(Context context, String comboKey, String CATEGORYID, String mPrefferedDataType) {

        try {
            IS_CALLED_VIA_POST = true;
            IS_CALL_FAILED = false;
            DataAggregationUtils dgu = new DataAggregationUtils(context,0);


            int num_of_days = sharedPrefManager.getIntValue(Constants
                    .NUMBER_OF_DAYS);

            RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());

            volleyMap = new HashMap<>();

            volleyMap.put(ApiConstants.CIRCLEIDCOMBO, sharedPrefManager.getIntValue(Constants.ON_BOARDING_CIRCLE_ID) + "");
            volleyMap.put("categoryIds", CATEGORYID + ",9,7");//+sprecial category

            volleyMap.put(ApiConstants.SPID, sharedPrefManager.getIntValue(Constants
                    .ON_BOARDING_PROVIDER_ID) + "");
            volleyMap.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
            volleyMap.put(ApiConstants.NO_OF_DAYS, ((num_of_days == 0) ? "28" : (num_of_days + "")));

            volleyMap.put(ApiConstants.JSON_DEVICEID, sharedPrefManager.getStringValue(Constants.DEVICE_ID));

            volleyMap.put(ApiConstants.DATAPREFERENCES, sharedPrefManager.getStringValue
                    (Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());


            if (SystemClock.elapsedRealtime() > 259200000 && TrafficStats.getMobileRxBytes() > 1000) {
                String dataexitsString = sharedPrefManager.getStringValue(Constants.DATA_EXISTS_FLAG);
                if (!AppUtils.isStringNullEmpty(dataexitsString) && dataexitsString.equalsIgnoreCase(ApiConstants.DATA_NOT_AVAILABLE)) {
                    volleyMap.put(ApiConstants.NETWORKUSAGES, dgu.formNetworkData(sharedPrefManager.getStringValue
                            (Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase()));
                }
            }

            if (!DateUtil.nDaysGoneSinceOnBoard(context, 1)) {
                volleyMap.put(ApiConstants.CALLINGUSAGES, dgu.formCallSmsData(0,0));
            }


            String url = ApiConstants.HOME_DEVICE_URL;

            StringRequest myReq = new StringRequest(Request.Method.POST,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Gson gson = new Gson();
                            homeAllInstreamModel = gson.fromJson(response, HomeAllInstreamModel.class);

                            if (mHomeScreen != null) {
                                mHomeScreen.OnSuccess(homeAllInstreamModel, null);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            IS_CALL_FAILED = true;
                            if (mHomeScreen != null) {
                                mHomeScreen.OnFailure(null);
                            }
                        }
                    }) {

                protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                    return volleyMap;
                }

                ;
            };


            myReq.setRetryPolicy(new DefaultRetryPolicy(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(myReq);
        } catch (Exception e) {
        }
    }

    public void setHomeScreen(HomeActivity homeScreen) {
        mHomeScreen = homeScreen;

        if (IS_CALLED_VIA_POST) {
            if (SystemClock.elapsedRealtime() > 259200000 && TrafficStats.getMobileRxBytes() > 1000) { //REC 931, REC 778
                String dataexitsString = sharedPrefManager.getStringValue(Constants.DATA_EXISTS_FLAG);
                if (!AppUtils.isStringNullEmpty(dataexitsString) &&
                        dataexitsString.equalsIgnoreCase(ApiConstants.DATA_NOT_AVAILABLE)) {
                } else {
                    homeScreen.setEstimatingData(true);
                }
            } else {
                homeScreen.setEstimatingData(true);
            }
        }

        if (homeAllInstreamModel != null) {
            mHomeScreen.OnSuccess(homeAllInstreamModel, null);
        }

        if(IS_CALL_FAILED)
            mHomeScreen.OnFailure(null);
    }

    public void getPlanApiDetailsWebService(Context context, String dataCategoryId,
                                            String dataPreference) {
        volleyMap = new HashMap<>();
        IS_CALLED_VIA_POST = true;
        try {
            IS_CALL_FAILED = false;
            final SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
            int num_of_days = mSharedPrefManager.getIntValue(Constants
                    .NUMBER_OF_DAYS);
            volleyMap.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants.ON_BOARDING_CIRCLE_ID) + "");
            volleyMap.put("categoryIds", dataCategoryId + ",9,7");//+sprecial category
            volleyMap.put(ApiConstants.SPID, mSharedPrefManager.getIntValue(Constants
                    .ON_BOARDING_PROVIDER_ID) + "");
            volleyMap.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
            volleyMap.put(ApiConstants.NO_OF_DAYS, ((num_of_days == 0) ? "28" : (num_of_days + "")));
            volleyMap.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));

            volleyMap.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue
                    (Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());

            String url = ApiConstants.HOME_URL;//"https://getsmartapp.com/recharger-api-1.1/recommend/all/plans";
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            StringBuilder stringBuilder = new StringBuilder(url);
            Iterator<Map.Entry<String, String>> iterator = volleyMap.entrySet().iterator();
            int i = 1;
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (i == 1) {
                    stringBuilder.append("?" + entry.getKey() + "=" + entry.getValue());
                } else {
                    stringBuilder.append("&" + entry.getKey() + "=" + entry.getValue());
                }
                iterator.remove(); // avoids a ConcurrentModificationException
                i++;
            }
            url = stringBuilder.toString();


            StringRequest myReq = new StringRequest(Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Gson gson = new Gson();
                            homeAllInstreamModel = gson.fromJson(response, HomeAllInstreamModel.class);
                            mSharedPrefManager.setStringValue(Constants.GET_PLAN_API_DETAILS, new Gson().toJson(homeAllInstreamModel));
                            mSharedPrefManager.setLongValue(Constants.LAST_RECO_CALL_TIMESTAMP, System.currentTimeMillis());
                            if (mHomeScreen != null) {
                                mHomeScreen.OnSuccess(homeAllInstreamModel, null);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            IS_CALL_FAILED = true;
                            if (mHomeScreen != null) {
                                mHomeScreen.OnFailure(null);
                            }
                        }
                    });
            myReq.setRetryPolicy(new DefaultRetryPolicy(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(myReq);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}