package com.getsmartapp.sharedPreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.getsmartapp.constants.Constants;
import com.getsmartapp.lib.constants.ApiConstants;

/**
 * @author bishwajeet.kumar on 15/07/15.
 */
public class BranchPrefManager {

    private static final String SHARED_PREF_FILE = "branch_pref";

    private static BranchPrefManager prefHelper_;
    private SharedPreferences appSharedPrefs_;
    private SharedPreferences.Editor prefsEditor_;

    private BranchPrefManager(Context context) {
        this.appSharedPrefs_ = context.getSharedPreferences(SHARED_PREF_FILE,Context.MODE_PRIVATE);
        this.prefsEditor_ = this.appSharedPrefs_.edit();
    }

    public static BranchPrefManager getInstance(Context context) {
        if(prefHelper_ == null) {
            prefHelper_ = new BranchPrefManager(context);
        }
        return prefHelper_;
    }

    public void setCurrentBranchUserName(String name) {
        this.writeStringToPrefs(ApiConstants.CURRENT_BRANCH_USER_NAME, name);
    }

    public String getCurrentBranchUserName() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_BRANCH_USER_NAME);
    }

    public void setReferralBranchUserName(String name) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_BRANCH_USER_NAME, name);
    }

    public String getReferralBranchUserName() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_BRANCH_USER_NAME);
    }

    public void setCurrentBranchUserIdentity(String identity) {
        this.writeStringToPrefs(ApiConstants.CURRENT_BRANCH_USER_IDENTITY, identity);
    }

    public String getCurrentBranchUserIdentity() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_BRANCH_USER_IDENTITY);
    }

    public void setReferralBranchUserIdentity(String identity) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_BRANCH_USER_IDENTITY, identity);
    }

    public String getReferralBranchUserIdentity() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_BRANCH_USER_IDENTITY);
    }

    public void setCurrentUserReferralLink(String refLink) {
        this.writeStringToPrefs(ApiConstants.CURRENT_USER_REFERRAL_LINK, refLink);
    }

    public String getCurrentUserReferralLink() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_USER_REFERRAL_LINK);
    }

    public void setCurrentUserReferralCode(String refCode) {
        this.writeStringToPrefs(ApiConstants.CURRENT_USER_REFERRAL_CODE, refCode);
    }

    public String getCurrentUserReferralCode() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_USER_REFERRAL_CODE);
    }

    public void setCurrentUserGCCode(String gcCode) {
        this.writeStringToPrefs(ApiConstants.CURRENT_USER_GC_CODE, gcCode);
    }

    public String getCurrentUserGCCode() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_USER_GC_CODE);
    }

    public void setIsActiveInviteeGC(Boolean isActiveInviteeGC) {
        this.writeBooleanToPrefs(ApiConstants.IS_ACTIVE_INVITEE_GC, isActiveInviteeGC);
    }

    public Boolean getIsActiveInviteeGC() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.IS_ACTIVE_INVITEE_GC);
    }

    public void setCurrentUserImageUrl(String imageURL) {
        this.writeStringToPrefs(ApiConstants.CURRENT_USER_IMAGE_URL, imageURL);
    }

    public String getCurrentUserImageURL() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_USER_IMAGE_URL);
    }

    public void setReferralUserImageURL(String imageURL) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_USER_IMAGE_URL, imageURL);
    }

    public String getReferralUserImageURL() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_USER_IMAGE_URL);
    }

    public void setIsReferredUser(Boolean isReferredUser) {
        this.writeBooleanToPrefs(ApiConstants.IS_REFERRED_USER, isReferredUser);
    }

    public Boolean getIsReferredUser() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.IS_REFERRED_USER);
    }

    public void setCurrentDeviceID(String deviceID) {
        this.writeStringToPrefs(ApiConstants.CURRENT_DEVICE_ID, deviceID);
    }

    public String getCurrentDeviceID() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_DEVICE_ID);
    }

    public void setReferralDeviceID(String deviceID) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_DEVICE_ID, deviceID);
    }

    public String getReferralDeviceID() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_DEVICE_ID);
    }

    public void setReferralChannel(String channel) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_CHANNEL, channel);
    }

    public String getReferralChannel() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_CHANNEL);
    }

    public void setCurrentUserChannel(String channel) {
        this.writeStringToPrefs(ApiConstants.CURRENT_USER_CHANNEL, channel);
    }

    public String getCurrentUserChannel() {
        return (String)this.readStringFromPrefs(ApiConstants.CURRENT_USER_CHANNEL);
    }

    public void setReferralEarningAmount(String earningAmount) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_EARNING_AMT, earningAmount);
    }

    public String getReferralEarningAmount() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_EARNING_AMT);
    }

    public void setReferralRedeemableAmount(String redeemableAmount) {
        this.writeStringToPrefs(ApiConstants.REFERRAL_REDEEMABLE_AMT, redeemableAmount);
    }

    public String getReferralRedeemableAmount() {
        return (String)this.readStringFromPrefs(ApiConstants.REFERRAL_REDEEMABLE_AMT);
    }

    public void setHasMadeFirstTransaction(Integer hasDoneFirstTransaction) {
        this.writeBooleanToPrefs(ApiConstants.HAS_MADE_FIRST_TRANSACTION, hasDoneFirstTransaction == 1 ? Boolean.TRUE : Boolean.FALSE);
    }

    public Boolean getHasMadeFirstTransaction() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.HAS_MADE_FIRST_TRANSACTION);
    }

    public void setShowWelcomeMessage(Boolean showWelcomeMessage) {
        this.writeBooleanToPrefs(ApiConstants.SHOW_WELCOME_MSG, showWelcomeMessage);
    }

    public Boolean getShowWelcomeMessage() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.SHOW_WELCOME_MSG);
    }

    public void setTransactionalPushState(Boolean tPush) {
        this.writeBooleanToPrefs(ApiConstants.T_PUSH_STATE, tPush);
    }

    public Boolean getTransactionalPushState() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.T_PUSH_STATE);
    }

    public void setPromotionalPushState(Boolean pPush) {
        this.writeBooleanToPrefs(ApiConstants.P_PUSH_STATE, pPush);
    }

    public Boolean getPromotionalPushState() {
        return (Boolean)readBooleanFromPrefs(ApiConstants.P_PUSH_STATE);
    }

    public void setEmailNotificationState(Boolean emailState) {
        this.writeBooleanToPrefs(ApiConstants.EMAIL_STATE, emailState);
    }

    public Boolean getEmailNotificationState() {
        return (Boolean)readBooleanFromPrefs(ApiConstants.EMAIL_STATE);
    }

    public void setSMSNotificationState(Boolean smsState) {
        this.writeBooleanToPrefs(ApiConstants.SMS_STATE, smsState);
    }

    public Boolean getSMSNotificationState() {
        return (Boolean)readBooleanFromPrefs(ApiConstants.SMS_STATE);
    }

    public void setIsNewDevice(Integer isNewDevice) {
        this.writeBooleanToPrefs(ApiConstants.IS_NEW_DEVICE, isNewDevice == 1 ? Boolean.TRUE : Boolean.FALSE);
    }

    public Boolean getIsNewDevice() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.IS_NEW_DEVICE);
    }

    public void setIsReferredDevice(Integer isReferredDevice) {
        this.writeBooleanToPrefs(ApiConstants.IS_REFERRED_DEVICE, isReferredDevice == 1 ? Boolean.TRUE : Boolean.FALSE);
    }

    public Boolean getIsReferredDevice() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.IS_REFERRED_DEVICE);
    }

    public void setIsReferredNumber(Integer isReferredNumber) {
        this.writeBooleanToPrefs(ApiConstants.IS_REFERRED_NUMBER, isReferredNumber == 1 ? Boolean.TRUE : Boolean.FALSE);
    }

    public Boolean getIsReferredNumber() {
        return (Boolean)this.readBooleanFromPrefs(ApiConstants.IS_REFERRED_NUMBER);
    }

    public void setIsReferSkipClicked(Boolean isReferSkipClicked) {
        this.writeBooleanToPrefs(Constants.ISREFERSKIPCLICKED, isReferSkipClicked);
    }

    public Boolean getIsReferSkipClicked() {
        return (Boolean)this.readBooleanFromPrefs(Constants.ISREFERSKIPCLICKED);
    }

    private void writeStringToPrefs(String key, String value) {
        prefHelper_.prefsEditor_.putString(key, value);
        prefHelper_.prefsEditor_.apply();
    }

    private Object readStringFromPrefs(String key) {
        return prefHelper_.appSharedPrefs_.getString(key, null);
    }

    private void writeBooleanToPrefs(String key, Boolean value) {
        prefHelper_.prefsEditor_.putBoolean(key, value);
        prefHelper_.prefsEditor_.apply();
    }

    private Object readBooleanFromPrefs(String key) {
        return prefHelper_.appSharedPrefs_.getBoolean(key, Boolean.FALSE);
    }

    public void clearSharedPrefs() {
        setCurrentBranchUserName("");
        setCurrentBranchUserIdentity("");
        setCurrentDeviceID("");
        setCurrentUserChannel("");
        setCurrentUserReferralCode("");
        setCurrentUserReferralLink("");
        setCurrentUserGCCode("");
        setCurrentUserImageUrl("");
        setReferralRedeemableAmount("");
        setReferralEarningAmount("");
        setIsActiveInviteeGC(Boolean.FALSE);
        setReferralBranchUserName("");
        setReferralBranchUserIdentity("");
        setReferralChannel("");
        setReferralDeviceID("");
        setReferralUserImageURL("");
        setIsReferredUser(Boolean.FALSE);
        setHasMadeFirstTransaction(0);
        setIsNewDevice(0);
    }
}
