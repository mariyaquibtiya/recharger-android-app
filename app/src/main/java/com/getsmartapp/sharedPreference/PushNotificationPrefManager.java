package com.getsmartapp.sharedPreference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author bishwajeet.kumar on 18/07/15.
 */
public class PushNotificationPrefManager {
    private static final String SHARED_PREF_FILE = "push_notify_pref";

    private static PushNotificationPrefManager prefHelper_;
    private SharedPreferences appSharedPrefs_;
    private SharedPreferences.Editor prefsEditor_;

    public PushNotificationPrefManager(Context context) {
        this.appSharedPrefs_ = context.getSharedPreferences(SHARED_PREF_FILE,Context.MODE_PRIVATE);
        this.prefsEditor_ = this.appSharedPrefs_.edit();
    }

    public static PushNotificationPrefManager getInstance(Context context) {
        if(prefHelper_ == null) {
            prefHelper_ = new PushNotificationPrefManager(context);
        }
        return prefHelper_;
    }

    public void setHasUserLogged(Boolean hasUserLogged) {
        this.writeBooleanToPrefs("has_user_logged", hasUserLogged);
    }

    public Boolean getHasUserLogged() {
        return (Boolean)this.readBooleanFromPrefs("has_user_logged");
    }

    public void setOrderIDfromPush(long orderID) {
        this.writeLongToPrefs("pushOrderId", orderID);
    }

    public long getOrderIDfromPush() {
        return (long)this.readLongFromPrefs("pushOrderId");
    }

    public void setIsDeeplinkPush(Boolean isDeeplinkPush) {
        this.writeBooleanToPrefs("isDeeplinkPush", isDeeplinkPush);
    }

    public Boolean isDeeplinkPush() {
        return (Boolean)this.readBooleanFromPrefs("isDeeplinkPush");
    }

    public void setIsLaunched(Boolean isLaunch) {
        this.writeBooleanToPrefs("isLaunch", isLaunch);
    }

    public Boolean getIsLaunched() {
        return (Boolean)this.readBooleanFromPrefs("isLaunch");
    }

    public void setShowHomeSnackBar(Boolean showHomeSnackBar) {
        this.writeBooleanToPrefs("showHomeSnackBar", showHomeSnackBar);
    }

    public Boolean getShowHomeSnackBar() {
        return (Boolean)this.readBooleanFromPrefs("showHomeSnackBar");
    }

    public void setSnackBarCoupon(String couponCode) {
        this.writeStringToPrefs("snackBarCode", couponCode);
    }

    public String getSnackBarCoupon() {
        return (String)this.readStringFromPrefs("snackBarCode");
    }

    private void writeStringToPrefs(String key, String value) {
        prefHelper_.prefsEditor_.putString(key, value);
        prefHelper_.prefsEditor_.commit();
    }

    private Object readStringFromPrefs(String key) {
        return prefHelper_.appSharedPrefs_.getString(key, null);
    }

    private void writeBooleanToPrefs(String key, Boolean value) {
        prefHelper_.prefsEditor_.putBoolean(key, value);
        prefHelper_.prefsEditor_.commit();
    }

    private Object readBooleanFromPrefs(String key) {
        return prefHelper_.appSharedPrefs_.getBoolean(key, Boolean.FALSE);
    }

    private void writeLongToPrefs(String key, long orderId) {
        prefHelper_.prefsEditor_.putLong(key, orderId);
        prefHelper_.prefsEditor_.commit();
    }

    private Object readLongFromPrefs(String key) {
        return prefHelper_.appSharedPrefs_.getLong(key, Long.MIN_VALUE);
    }
}
