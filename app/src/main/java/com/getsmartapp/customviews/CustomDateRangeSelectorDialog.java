package com.getsmartapp.customviews;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;

import com.getsmartapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author maria.quibtiya on 19/07/16.
 */
public class CustomDateRangeSelectorDialog extends Dialog implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private Context mContext;
    private Calendar mCalendar = Calendar.getInstance();
    private boolean isStartDate = true;
    private TextView startDatePicker, endDatePicker;

    public CustomDateRangeSelectorDialog(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public CustomDateRangeSelectorDialog(Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        initView();
    }

    protected CustomDateRangeSelectorDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        mContext = context;
        initView();
    }

    private void initView() {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = View.inflate(mContext, R.layout.select_date_range_layout, null) ;
        startDatePicker = (TextView) view.findViewById(R.id.start_date_picker);
        endDatePicker = (TextView) view.findViewById(R.id.end_date_picker);

        startDatePicker.setOnClickListener(this);
        endDatePicker.setOnClickListener(this);

        view.findViewById(R.id.confirm_btn).setSelected(true);

        setContentView(view);
    }

    @Override
    public void onClick(View view) {

        int viewClicked = view.getId();
        if (viewClicked == R.id.start_date_picker)
            isStartDate = true;
        else if (viewClicked == R.id.end_date_picker)
            isStartDate = false;

        new DatePickerDialog(mContext, this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, monthOfYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateLabel();
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        String dateToSet = sdf.format(mCalendar.getTime());
        if (isStartDate)
            startDatePicker.setText(dateToSet);
        else endDatePicker.setText(dateToSet);
    }
}
