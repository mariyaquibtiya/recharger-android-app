package com.getsmartapp.customviews.bottombar;

import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;

/**
 * @author maria.quibtiya on 14/07/16.
 */
public class TabIndicator {
    /**
     * Creates a new rectangle indicator for tab background.
     *
     * @param width  the width of the indicator
     * @param height  the height of the indicator
     * @param color the color of the indicator
     * @return a nice and adorable indicator.
     */
    protected static ShapeDrawable make(int width, int height, int color) {
        ShapeDrawable indicator = new ShapeDrawable(new RectShape());
        indicator.setIntrinsicWidth(width);
        indicator.setIntrinsicHeight(height);
        indicator.getPaint().setColor(color);
        return indicator;
    }
}
