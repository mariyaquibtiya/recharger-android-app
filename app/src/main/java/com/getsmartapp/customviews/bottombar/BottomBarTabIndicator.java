package com.getsmartapp.customviews.bottombar;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

/**
 * @author maria.quibtiya on 14/07/16.
 */
public class BottomBarTabIndicator extends View{

    //private boolean isVisible = false;
    private long animationDuration = 150;
    private Context mContext;

    public void translateTo(View tabToAddTo) {
        ViewCompat.animate(this)
                .setDuration(animationDuration)
                .translationX(tabToAddTo.getX())
                .translationY(0)
                .start();
    }

    protected BottomBarTabIndicator(Context context, int position, final View tabToAddTo, // Rhyming accidentally! That's a Smoove Move!
                             int backgroundColor) {
        super(context);
        mContext = context;

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                MiscUtils.dpToPixel(context, 100), MiscUtils.dpToPixel(context, 2));

        setLayoutParams(params);
        /*setGravity(Gravity.CENTER);
        MiscUtils.setTextAppearance(this,
                R.style.BB_BottomBarBadge_Text);

        int three = MiscUtils.dpToPixel(context, 3);*/
        ShapeDrawable background = TabIndicator.make(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, backgroundColor);
        //setPadding(three, three, three, three);
        setBackgroundCompat(background);

        final FrameLayout container = new FrameLayout(context);
        container.setLayoutParams(params);

        ViewGroup parent = (ViewGroup) tabToAddTo.getParent();
        parent.removeView(tabToAddTo);

        container.setTag(tabToAddTo.getTag());
        tabToAddTo.setTag(null);
        container.addView(tabToAddTo);
        container.addView(this);

        parent.addView(container, position);

        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                container.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                adjustPosition(tabToAddTo);
            }
        });
    }

    protected void adjustPosition(View tabToAddTo) {
        setX((float) (tabToAddTo.getX() /*+ (tabToAddTo.getWidth() / 1.75)*/));
        setY(tabToAddTo.getY() + tabToAddTo.getHeight() - MiscUtils.dpToPixel(mContext, 5));
    }

    private void adjustPositionAndSize(View tabToAddTo) {
        adjustPosition(tabToAddTo);
        //setTranslationY(0);

        int size = Math.max(getWidth(), getHeight());

        /*ViewGroup.LayoutParams params = getLayoutParams();

        if (params.width != size || params.height != size) {
            params.width = size;
            params.height = size;
            setLayoutParams(params);
        }*/
    }

    @SuppressWarnings("deprecation")
    private void setBackgroundCompat(Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(background);
        } else {
            setBackgroundDrawable(background);
        }
    }








    public BottomBarTabIndicator(Context context) {
        super(context);
    }

    public BottomBarTabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BottomBarTabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BottomBarTabIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
