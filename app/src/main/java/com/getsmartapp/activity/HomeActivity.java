package com.getsmartapp.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.getsmartapp.R;
import com.getsmartapp.customviews.bottombar.BottomBar;
import com.getsmartapp.customviews.bottombar.BottomBarTab;
import com.getsmartapp.customviews.bottombar.OnTabClickListener;
import com.getsmartapp.fragments.BaseAccountFragment;
import com.getsmartapp.fragments.BaseFragment;
import com.getsmartapp.fragments.BaseInsightsFragment;
import com.getsmartapp.fragments.BaseRechargeFragment;
import com.getsmartapp.lib.model.HomeAllInstreamModel;
import com.getsmartapp.util.ApiUtility;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends BaseActivity implements OnTabClickListener,
        BaseFragment.OnFragmentInteractionListener, ApiUtility.ApiCallback, AdapterView.OnItemClickListener {

    public static final String POSITION = "POSITION";
    private final int TAB_COUNT = 3;

    public FrameLayout mFragmentContainer;
    //private TabLayout mTabLayout;

    private String tabTitles[];
    private int[] imageSelectedResId = {
            R.drawable.tab_insights,
            R.drawable.tab_recharge,
            R.drawable.tab_account
    };
    private int[] imageUnselectedResId = {
            R.drawable.settings,
            R.drawable.settings,
            R.drawable.settings
    };

    private Toolbar mToolbar;

    private BottomBar mBottomBar;

    private static HomeActivity sInstance;
    private boolean estimating_data;

    public static HomeActivity getInstance() {
        return sInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_base);

        mBottomBar = BottomBar.attachShy((CoordinatorLayout) findViewById(R.id.main_content),
                findViewById(R.id.nested_scroll_container), savedInstanceState);

        sInstance = this;

        tabTitles = new String[] { getString(R.string.insights), getString(R.string.recharge), getString(R.string.account) };

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        initializeToolbar();

        initializeCalendarPopup();

       // setSupportActionBar(mToolbar);

        mFragmentContainer = (FrameLayout) findViewById(R.id.container);

        initializeTabLayout();

    }

    private void initializeToolbar() {

        Spinner topSpinner = (Spinner) mToolbar.findViewById(R.id.actionbar_spinner);
        ArrayList<String> mobileNumersWithOperator = new ArrayList<>();
        mobileNumersWithOperator.add("Airtel "+ getString(R.string.mid_dot) + " 8527337738");
        mobileNumersWithOperator.add("Telenor "+ getString(R.string.mid_dot) + " 9453995160");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>
                (this, R.layout.text_view_spinner,mobileNumersWithOperator);

        dataAdapter.setDropDownViewResource
                (R.layout.text_view_spinner);

        topSpinner.setAdapter(dataAdapter);
    }

    private void initializeCalendarPopup() {
        final ImageView calendar = (ImageView) mToolbar.findViewById(R.id.right_image);
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ListPopupWindow listPopupWindow = new ListPopupWindow(HomeActivity.this);
                listPopupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_rect_white));
                listPopupWindow.setWidth(getResources().getDimensionPixelSize(R.dimen.calendar_popup_width));
                listPopupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);

                listPopupWindow.setDropDownGravity(Gravity.RIGHT);
                listPopupWindow.setVerticalOffset(getResources().getDimensionPixelSize(R.dimen.margin_5));

                ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(HomeActivity.this, R.array.calendar_options,
                        R.layout.popup_tv_layout);
                listPopupWindow.setAdapter(arrayAdapter);
                listPopupWindow.setAnchorView(calendar);
                listPopupWindow.show();

                listPopupWindow.setOnItemClickListener(HomeActivity.this);

            }
        });
    }

    private void initializeTabLayout() {
        mBottomBar.setItems(new BottomBarTab(imageSelectedResId[0], tabTitles[0]),
                new BottomBarTab(imageSelectedResId[1], tabTitles[1]),
                new BottomBarTab(imageSelectedResId[2], tabTitles[2]));

        mBottomBar.setActiveTabColor(getResources().getColor(R.color.tv_color_selected));

        ViewGroup bottomBar = (ViewGroup) mBottomBar.getTabContainer();
        int childCount = bottomBar.getChildCount();
        for (int i=0; i<childCount; i++) {
            bottomBar.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (isSmartSaveEnabled()) {
                        removeAllTouchListeners();
                        return false;
                    }
                    else {
                        Snackbar.make(findViewById(R.id.container), "Please enable Smartsave", Snackbar.LENGTH_SHORT).show();
                        return true;
                    }
                }
            });
        }

        mBottomBar.setOnTabClickListener(this);

    }

    private void removeAllTouchListeners() {
        /*ViewGroup bottomBar = (ViewGroup) mBottomBar.getTabContainer();
        int childCount = bottomBar.getChildCount();
        for (int i=0; i<childCount; i++) {
            bottomBar.getChildAt(i).setOnTouchListener(null);
        }*/
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mBottomBar.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        /*if (mTabLayout != null && mTabLayout.getVisibility() == View.VISIBLE)
            mTabLayout.getTabAt(savedInstanceState.getInt(POSITION)).select();*/
    }

    private void replaceFragmentAtTab(int tabPosition) {

        Fragment fragmentToReplace = null;

        switch (tabPosition) {
            case 0:
                fragmentToReplace = Fragment.instantiate(this, BaseInsightsFragment.class.getName());//new BaseInsightsFragment();
                break;
            case 1:
                fragmentToReplace = Fragment.instantiate(this, BaseRechargeFragment.class.getName());//new BaseRechargeFragment();
                break;
            case 2:
                fragmentToReplace = Fragment.instantiate(this, BaseAccountFragment.class.getName());//new BaseAccountFragment();
                break;
            /*default:
                fragmentToReplace = Fragment.instantiate(this, BaseInsightsFragment.class.getName());//new BaseInsightsFragment();*/

        }
        if (fragmentToReplace != null)
            replaceFragment(mFragmentContainer.getId(), fragmentToReplace, true);
    }


    public void setEstimatingData(boolean value) {
        estimating_data = value;
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        popAllFragments();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void OnSuccess(Object planDetailModel, Response response) {

        if (planDetailModel instanceof HomeAllInstreamModel) {
            if (((HomeAllInstreamModel) planDetailModel).getBody() != null) {
                HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity comboRecomms =
                        ((HomeAllInstreamModel) planDetailModel).getBody().getComboRecommendation();

                if (comboRecomms != null) {
                    List<HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity> comboSets = comboRecomms.getComboSets();
                    if (comboSets != null && comboSets.size() > 0) {
                        for (int i = 0; i < comboSets.size(); i++) {
                            HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity comboset = comboSets.get(i);
                            List<HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity.PlansEntity> plansList = comboset.getPlans();
                            HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity.ComboSubsetsEntity onecombo =
                                    comboset.getComboSubsets().get(0);
                            int cost = onecombo.getComboCost().getComboCost();
                            double saving = onecombo.getComboCost().getSaving();
                            String sav = String.valueOf(Math.round(saving));
                            String no_of_plans = String.valueOf(onecombo.getPlanIds().size());

                            //TODO:UNCOMMENT_LATER
                            /*if (!AppUtils.isStringNullEmpty(comboKey)) {
                                HomeCardsModel model = new HomeCardsModel();
                                model.setCardName(DataStorageConstants.CARD_RECOMMEND_COMBO);
                                model.setScore(Integer.parseInt(comboKey));
                                model.seteCoeff(comboCoeff);
                                addDataInCardList(model);
                                if (cardsAdapter != null) {
                                    cardsAdapter.setComboAmountandSaving(cost, sav, no_of_plans, comboRecomms, plansList, estimating_data);
                                }
                                break;
                            }*/
                        }
                    }
                }

                List<HomeAllInstreamModel.BodyEntity.InStreamRecommendationsEntity> instreamRecomms =
                        ((HomeAllInstreamModel) planDetailModel).getBody().getInStreamRecommendations();

                if (instreamRecomms != null && instreamRecomms.size() > 0) {
                    for (int i = 0; i < instreamRecomms.size(); i++) {
                        int categoryId = instreamRecomms.get(i).getCategoryId();

                        //TODO:UNCOMMENT_LATER
                        /*if (categoryId == 9) {

                            List<HomeAllInstreamModel.BodyEntity.InStreamRecommendationsEntity.PlansEntity>
                                    instreamPlans = instreamRecomms.get(i).getPlans();
                            if (!AppUtils.isStringNullEmpty(instreamSprecialKey)) {
                                HomeCardsModel model = new HomeCardsModel();
                                model.setCardName(DataStorageConstants.CARD_RECOMMEND_SPL);
                                model.setScore(Integer.parseInt(instreamSprecialKey));
                                model.seteCoeff(instreamSprecialCoeff);
                                addDataInCardList(model);
                                if (cardsAdapter != null)
                                    cardsAdapter.setInstreamSlashDashPlans(instreamPlans);
                            }
                        } else if (categoryId == 1 || categoryId == 2) {//2g/3g
                            if (!AppUtils.isStringNullEmpty(instreamDataKey)) {
                                List<HomeAllInstreamModel.BodyEntity.InStreamRecommendationsEntity.PlansEntity>
                                        instreamPlans = instreamRecomms.get(i).getPlans();
                                HomeCardsModel model = new HomeCardsModel();
                                model.setCardName(DataStorageConstants.CARD_RECOMMEND_DATA);
                                model.setScore(Integer.parseInt(instreamDataKey));
                                model.seteCoeff(instreamDataCoeff);
                                addDataInCardList(model);
                                if (cardsAdapter != null)
                                    cardsAdapter.setInstreamTooMuchPlans(instreamPlans);
                            }
                        }*/

                    }
                }
                //TODO:UNCOMMENT_LATER
                        //callOrderStatusCard();//load order cards after instream calls
            } else {
                //TODO:UNCOMMENT_LATER
                        //callOrderStatusCard();//load order cards if no instream available
            }
        }
    }

    @Override
    public void OnFailure(RetrofitError error) {

        //TODO:UNCOMMENT_LATER
        /*apiUtility.getPlanApiDetailsSharedPrefs(this);
        setCardsListAdpater();
        dismissProgessDialog();*/
    }

    @Override
    public void onTabSelected(int position) {
        Log.d("Mariya","onTabSelected");

        replaceFragmentAtTab(position);
    }

    @Override
    public void onTabReSelected(int position) {

        Log.d("Mariya","onTabReSelected");

        if (getBackstackEntryCount() == 0)
            replaceFragmentAtTab(position);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Log.d("Mariya","onItemClick");
    }
}
