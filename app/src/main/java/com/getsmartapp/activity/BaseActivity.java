package com.getsmartapp.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.getsmartapp.R;
import com.getsmartapp.RechargerApplication;
import com.getsmartapp.constants.Constants;

import java.util.Calendar;

public abstract class BaseActivity extends AppCompatActivity {


    //private SharedPrefManager mSharedPrefManager;
    private long bill_cycle_start_time_in_millis;
    private long bill_cycle_end_time_in_millis;

    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //mSharedPrefManager = new SharedPrefManager(this);
        updateBillCycle();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onResume() {
        RechargerApplication.getGlobalInstance().setApplicationForegroundState(true);
        updateBillCycle();
        super.onResume();

    }

    @Override
    protected void onPause() {
        RechargerApplication.getGlobalInstance().setApplicationForegroundState(false);
        super.onPause();

    }

    private void updateBillCycle()
    {
        String sTypeId = "";//mSharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
        if(sTypeId.equalsIgnoreCase("Postpaid")) {

            bill_cycle_start_time_in_millis = 0;//mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_START_DATE_TIME_MILLIS);
            bill_cycle_end_time_in_millis = 0;//mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_END_DATE_TIME_MILLIS);
            if (bill_cycle_start_time_in_millis == 0) {

                Calendar calendar = Calendar.getInstance();
                if (bill_cycle_start_time_in_millis == 0) {
                    bill_cycle_start_time_in_millis = calendar.getTimeInMillis();

                    int mNextSelectedDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                    calendar.set(Calendar.DATE, mNextSelectedDay);
                    bill_cycle_end_time_in_millis = calendar.getTimeInMillis();
                }

                //mSharedPrefManager.setLongValue(Constants.ON_BOARDING_BILL_START_DATE_TIME_MILLIS, bill_cycle_start_time_in_millis);
                //mSharedPrefManager.setLongValue(Constants.ON_BOARDING_BILL_END_DATE_TIME_MILLIS, bill_cycle_end_time_in_millis);

            } else if (Calendar.getInstance().getTimeInMillis() > bill_cycle_end_time_in_millis) {

                long single_day = 24 * 3600 * 1000;
                long difference_in_millis = Calendar.getInstance().getTimeInMillis() - bill_cycle_end_time_in_millis;
                int days_diff = (int) (difference_in_millis / single_day);

                int factor = (days_diff /30)  + 1;

                bill_cycle_start_time_in_millis = bill_cycle_start_time_in_millis + factor*30*single_day;
                bill_cycle_end_time_in_millis = bill_cycle_end_time_in_millis + factor*30*single_day;

                //mSharedPrefManager.setLongValue(Constants.ON_BOARDING_BILL_START_DATE_TIME_MILLIS, bill_cycle_start_time_in_millis);
                //mSharedPrefManager.setLongValue(Constants.ON_BOARDING_BILL_END_DATE_TIME_MILLIS, bill_cycle_end_time_in_millis);
            } else if(Calendar.getInstance().getTimeInMillis() < bill_cycle_start_time_in_millis)
            {

                long single_day = 24 * 3600 * 1000;

                long difference_in_millis = bill_cycle_start_time_in_millis - Calendar.getInstance().getTimeInMillis();
                int days_diff = (int) (difference_in_millis / single_day);

                int factor = (days_diff /30)  + 1;


                bill_cycle_start_time_in_millis = bill_cycle_start_time_in_millis - factor*30*single_day;
                bill_cycle_end_time_in_millis = bill_cycle_end_time_in_millis - factor*30*single_day;

                //mSharedPrefManager.setLongValue(Constants.ON_BOARDING_BILL_START_DATE_TIME_MILLIS, bill_cycle_start_time_in_millis);
                //mSharedPrefManager.setLongValue(Constants.ON_BOARDING_BILL_END_DATE_TIME_MILLIS, bill_cycle_end_time_in_millis);

            }

        }
    }

    public SharedPreferences getSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.shared_prefs_name), MODE_PRIVATE);
        return sharedPreferences;
    }

    public boolean isSmartSaveEnabled() {
        return getSharedPreferences().getBoolean(Constants.ENABLED_SMARTSAVE, false);
    }

    public int replaceFragment(int containerViewId, Fragment fragmentToReplace, boolean addToBackstack) {
        if (mFragmentManager == null)
            mFragmentManager = getSupportFragmentManager();

        int replacedFrgmnt;
        if (addToBackstack)
            replacedFrgmnt = mFragmentManager.beginTransaction().replace(containerViewId, fragmentToReplace).
                addToBackStack(fragmentToReplace.getClass().getSimpleName()).commit();
        else
            replacedFrgmnt = mFragmentManager.beginTransaction().replace(containerViewId, fragmentToReplace).commit();

        return replacedFrgmnt;
    }

    public int getBackstackEntryCount() {
        if (mFragmentManager == null)
            mFragmentManager = getSupportFragmentManager();

        return mFragmentManager.getBackStackEntryCount();
    }

    public void popOutFragment(Fragment fragment) {
        if (mFragmentManager == null)
            mFragmentManager = getSupportFragmentManager();

        mFragmentManager.popBackStack(fragment.getClass().getSimpleName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void popAllFragments() {
        if (mFragmentManager == null)
            mFragmentManager = getSupportFragmentManager();

        if (mFragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = mFragmentManager.getBackStackEntryAt(0);
            mFragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}
