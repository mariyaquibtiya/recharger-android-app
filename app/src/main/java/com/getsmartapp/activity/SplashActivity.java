package com.getsmartapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.TrafficStats;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apsalar.sdk.Apsalar;
import com.getsmartapp.R;
import com.getsmartapp.constants.Constants;
import com.getsmartapp.data.DBHelper;
import com.getsmartapp.lib.SmartSDK;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.dataAggregation.DataAggregationUtils;
import com.getsmartapp.lib.database.SdkAppDatabaseHelper;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.App;
import com.getsmartapp.lib.model.MobileDataVal;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.services.WelcomeEmailService;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.managers.HomeScreenConnectionManager;
import com.getsmartapp.managers.ParseDeepLinkManager;
import com.getsmartapp.services.GetWalletBalanceTask;
import com.getsmartapp.services.LocationService;
import com.getsmartapp.util.AppUtils;
import com.getsmartapp.util.BranchAndParseUtils;
import com.getsmartapp.util.CardUtils;
import com.getsmartapp.util.ContainerHolderSingleton;
import com.getsmartapp.util.CustomDialogUtil;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tagmanager.Container;
import com.google.android.gms.tagmanager.ContainerHolder;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.tagmanager.TagManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SplashActivity extends Activity {
    //    private static final long TIMEOUT_FOR_CONTAINER_OPEN_MILLISECONDS = 1500;
    private static final long TIMEOUT_FOR_CONTAINER_OPEN_MILLISECONDS = 100;

    private static final String CONTAINER_ID = "GTM-P64ZVV";
    private ImageView imageView;
    private SdkAppDatabaseHelper mSdkAppDatabaseHelper;
    private int mTotalCallDuration;

    private List<com.getsmartapp.lib.model.CallLog> mCallLogList;
    private Intent mOnBoardingIntent;
    private int mTotalNightCallCount;
    private int mTotalNightCallMinute;
    private int mTotalCallLogSize;
    private SharedPrefManager mSharedPref;
    private SQLiteDatabase sqLiteDatabase;
    private String instreamDataKey = "", instreamSprecialKey = "", orderStatusKey = "", comboKey = "";
    private Double instreamDataCoeff = 0.0, instreamSprecialCoeff = 0.0, orderStatusCoeff = 0.0, comboCoeff = 0.0;
    private String installedAppVersion;
    private String forceUpdate = "0";
    private String ignoreUpdate = "1";
    private String updateJSON;
    private boolean appVersionNotFoundOnServer = true;
    static Uri APP_URI = Uri.parse("android-app://com.getsmartapp/smartapp/home");
    static Uri WEB_URL = Uri.parse("http://com.getsmartapp/smartapp");
    private GoogleApiClient mClient;
    private String mTitle;
    private int appOpenCount;
    private String cardJson;
    private DataLayer mDataLayer;

    private void disableNotification()
    {
        mSharedPref.setBooleanValue(Constants.SHARED_PREFERENCE_SHOW_NOTIFICATION, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_main);
        mSharedPref = new SharedPrefManager(this);

        SmartSDK.startAllServices(this);
        Intent intent1 = new Intent(this, LocationService.class);
        startService(intent1);

        imageView = (ImageView) findViewById(R.id.imageView);

        Animation anim = AnimationUtils.loadAnimation(this, R.anim
                .rotate_animation);
        findViewById(R.id.loader_img).startAnimation(anim);

        APP_URI = Uri.parse("android-app://"+getPackageName()+"/smartapp/home");
        WEB_URL = Uri.parse("http://"+getPackageName()+"/smartapp");

        if(TextUtils.isEmpty(mSharedPref.getStringValue(InternetDataUsageUtil.SHARED_PREFERENCE_LAST_WIFI_CONNECTION))) {
            try {
                WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                mSharedPref.setStringValue(InternetDataUsageUtil.SHARED_PREFERENCE_LAST_WIFI_CONNECTION, wifiManager.getConnectionInfo().getSSID().replace("\"", ""));
            } catch (Exception e){}
        }
        try {

            InternetDataUsageUtil.getInstance(this).setAppName(getString(R.string.app_name));
            InternetDataUsageUtil.getInstance(getApplicationContext()).initialiseDataCapture(getApplicationContext());

            if(mSharedPref.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE,-1)==-1)
            {
                if(InternetDataUsageUtil.isNetworkConnected(this))
                {
                    int connectionType;
                    if(InternetDataUsageUtil.isMobileNetworkConnected(getApplicationContext()))
                    {
                        connectionType = InternetDataUsageUtil.NETWORK_CONNECTION.MOBILE_NETWORK.ordinal();
                    }else
                    {
                        connectionType = InternetDataUsageUtil.NETWORK_CONNECTION.WIFI_NETWORK.ordinal();
                    }
                    mSharedPref.setIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE, connectionType);
                }
            }
            InternetDataUsageUtil.getInstance(getApplicationContext()).initialiseDataCapture(getApplicationContext());

        }catch (Exception e){}


        DBHelper dbHelper = DBHelper.getInstance(SplashActivity.this);//new DBHelper(SplashActivity.this);
        sqLiteDatabase = dbHelper.getReadableDatabase();

        mDataLayer = TagManager.getInstance(this).getDataLayer();

        new GetWalletBalanceTask(getApplicationContext(),null).execute();

        updateJSON = mSharedPref.getStringValue(Constants.APP_UPDATE_URL_RESPONSE);

        mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        mTitle = "smartapp";

        BranchAndParseUtils.subscribeAndEventTrackForUA(this, "user_opened_app");
        mDataLayer.push(DataLayer.mapOf("event", "GAEvent", "eventAct", "App Open", "eventCat", "App Open", "eventLbl", "App Open", "eventVal", 1));
        appOpenCount = mSharedPref.getIntValue(Constants.APP_OPEN_COUNT);
        if(4 >= appOpenCount) {
            appOpenCount += 1;
            mSharedPref.setIntValue(Constants.APP_OPEN_COUNT, appOpenCount);
        } else if(5 == appOpenCount) {
            BranchAndParseUtils.subscribeToPushTag(this, "appOpen5");
        }
        BranchAndParseUtils.subscribeToLatestAppVersion(this);

        long timeDifference = Calendar.getInstance().getTimeInMillis() - mSharedPref.getLongValue(Constants.APP_UDPATE_CHECK_TIME);

        if (AppUtils.isConnectingToInternet(this) && (timeDifference > 0) && (timeDifference > 24 * 60 * 60 * 1000)) {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest myReq = new StringRequest(Request.Method.GET,
                    ApiConstants.APP_UPDATE_URL + "?version=" + Calendar.getInstance().getTimeInMillis(),
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            mSharedPref.setStringValue(Constants.APP_UPDATE_URL_RESPONSE, response);
                            updateJSON = response;

                            mSharedPref.setLongValue(Constants.APP_UDPATE_CHECK_TIME, Calendar.getInstance().getTimeInMillis());
                            preInit();
                        }
                    },
                    new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            preInit();
                        }
                    });
            myReq.setRetryPolicy(new DefaultRetryPolicy(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(myReq);
        } else {
            preInit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void preInit() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            installedAppVersion = pInfo.versionName;

            JSONArray jsonArray = new JSONArray(updateJSON);
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).getString("app_version").equalsIgnoreCase(installedAppVersion)) {
                    forceUpdate = jsonArray.getJSONObject(i).getInt("force_update") + "";
                    ignoreUpdate = jsonArray.getJSONObject(i).getInt("ignore") + "";
                    appVersionNotFoundOnServer = false;
                    long timeDifference = Calendar.getInstance().getTimeInMillis() - mSharedPref.getLongValue(Constants.APP_UPDATE_SOFT_UPDATE_DIALOG_SHOWN_LAST_TIME);

                    if ((forceUpdate.equalsIgnoreCase("0") && ignoreUpdate.equalsIgnoreCase("0") && timeDifference > 2 * 24 * 60 * 60 * 1000) || (forceUpdate.equalsIgnoreCase("1")))
                        CustomDialogUtil.showCustomDialog(this, "Update Your App", onClickListener, (forceUpdate.equalsIgnoreCase("1") ? true : false));
                    else
                        init();

                    break;
                }
            }

            if (appVersionNotFoundOnServer) {
                init();
            }

        } catch (PackageManager.NameNotFoundException e) {
            init();

            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
            init();

        }catch (Exception e){
            init();
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.yes_btn:
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri
                            .parse("market://details?id=" + getPackageName())));
                    break;
                case R.id.no_btn:
                    init();
                    break;
                default:
            }
        }
    };


    //TODO:UNCOMMENT_LATER
    private void init() {
        TagManager tagManager = TagManager.getInstance(this);
        mSdkAppDatabaseHelper = new SdkAppDatabaseHelper(this);

        if (mSharedPref.getStringValue(Constants.ISBOARDINGTUTORIALSHOWN).equals("1")) {
            if (mSharedPref.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) == 1) {
                mOnBoardingIntent = new Intent(SplashActivity.this, HomeActivity.class);
                mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mOnBoardingIntent.putExtra("selected_pos", 1);
                AppUtils.updateCards(getApplicationContext());
            } else {
               // mOnBoardingIntent = new Intent(SplashActivity.this, OnBoardingMobileNumberActivity.class);
            }
        } else if (mSharedPref.getBooleanValue(Constants.APP_STARTED_FIRST_TIME)) {
           // mOnBoardingIntent = new Intent(SplashActivity.this, OnBoardingTutorialActivity.class);
            mOnBoardingIntent.putExtra("screen1_title", getString(R.string.onboarding_tutorial1_title));
            mOnBoardingIntent.putExtra("screen1_subtitle", getString(R.string.onboarding_tutorial1_subtitle));
            mOnBoardingIntent.putExtra("screen2_title", getString(R.string
                    .onboarding_tutorial2_title));
            mOnBoardingIntent.putExtra("screen2_subtitle", getString(R.string
                    .onboarding_tutorial2_subtitle));
            mOnBoardingIntent.putExtra("screen3_title", getString(R.string
                    .onboarding_tutorial3_title));
            mOnBoardingIntent.putExtra("screen3_subtitle", getString(R.string
                    .onboarding_tutorial3_subtitle));
            new OnboardingBackgroundTask().execute();
        }

        tagManager.setVerboseLoggingEnabled(true);

        PendingResult<ContainerHolder> pending =
                tagManager.loadContainerPreferNonDefault(CONTAINER_ID, R.raw.gtm_p64zvv);

        pending.setResultCallback(new ResultCallback<ContainerHolder>() {
            @Override
            public void onResult(ContainerHolder containerHolder) {
                ContainerHolderSingleton.setContainerHolder(containerHolder);
                Container container = containerHolder.getContainer();

                ContainerHolderSingleton.setContainerHolder(containerHolder);
                ContainerLoadedCallback.registerCallbacksForContainer(container);
                containerHolder.setContainerAvailableListener(new ContainerLoadedCallback());

            }
        }, TIMEOUT_FOR_CONTAINER_OPEN_MILLISECONDS, TimeUnit.MILLISECONDS);
        String dataexitsflag = mSharedPref.getStringValue(Constants.DATA_EXISTS_FLAG);
        if (!AppUtils.isStringNullEmpty(dataexitsflag) && !dataexitsflag.equalsIgnoreCase(ApiConstants.DATA_AVAILABLE)) {
            checkifDataExists();
        }

        if (mSharedPref.getIntValue("is_email_send") == 0) {
            Intent welcomeEmailServiceIntent = new Intent(this, WelcomeEmailService.class);
            startService(welcomeEmailServiceIntent);
        }
        Apsalar.setFBAppId(getString(R.string.facebook_app_id));
        Apsalar.startSession(getApplicationContext(), getString(R.string.apsalar_api_key), getString(R.string.apsalar_secret));
        Apsalar.event("App Open", "eventAct", "App opened", "eventVal", 1);

        String mPrefferedDataType = mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
        if (mPrefferedDataType.equalsIgnoreCase(getString(R.string.lte))) {
            mSharedPref.setStringValue(Constants.ON_BOARDING_PREFERRED_TYPE, getString(R.string.fourg));
            mPrefferedDataType = getString(R.string.fourg);
        }

        if (!mSharedPref.getBooleanValue(Constants.APP_STARTED_FIRST_TIME) && AppUtils.isConnectingToInternet(this)) {

            try {
                String CATEGORYID = "1";
                if (mPrefferedDataType.equalsIgnoreCase("2G")) {
                    CATEGORYID = "1";
                    mPrefferedDataType = "2g";
                } else if (mPrefferedDataType.equalsIgnoreCase("3G") || mPrefferedDataType.equalsIgnoreCase(getString(R.string.fourg)) || mPrefferedDataType.equalsIgnoreCase("4G/LTE")) {
                    CATEGORYID = "2";
                    mPrefferedDataType = "3g";
                }
                cardJson = CardUtils.getCardRankings(this, sqLiteDatabase);
                parseCardsJson(cardJson);
                if (!AppUtils.isStringNullEmpty(comboKey)) {
                    CATEGORYID = "0," + CATEGORYID;
                }


                if (!DateUtil.nDaysGoneSinceOnBoard(this, 3)) {
                    HomeScreenConnectionManager.getInstance(getApplicationContext()).postPlanApiDetailsWebService(getApplicationContext(), comboKey, CATEGORYID, mPrefferedDataType);
                } else {
                    if (DateUtil.sixHourBefore(mSharedPref.getLongValue(Constants.LAST_RECO_CALL_TIMESTAMP))) {

                        HomeScreenConnectionManager.getInstance(getApplicationContext()).getPlanApiDetailsWebService(this, CATEGORYID, mPrefferedDataType);
                    } else {
                        if (mSharedPref.getIntValue(Constants.FETCH_FRESH_RECOMMENDATION) == 1) {

                            HomeScreenConnectionManager.getInstance(getApplicationContext()).getPlanApiDetailsWebService(this, CATEGORYID, mPrefferedDataType);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        launchHome();

    }

    private void parseCardsJson(String cardsStr) {
        orderStatusKey = "";
        try {
            JSONObject json = new JSONObject(cardsStr);
            Iterator<String> iter = json.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                if (json.has(key)) {
                    JSONArray jArr = json.getJSONArray(key);
                    if (jArr != null && jArr.length() > 0) {
                        String value = (String) jArr.get(0);
                        Double e = (Double) jArr.get(1);
                        if (value.equalsIgnoreCase(DataStorageConstants.CARD_RECOMMEND_DATA)) {
                            instreamDataKey = key;
                            instreamDataCoeff = e;
                        } else if (value.equalsIgnoreCase(DataStorageConstants.CARD_RECOMMEND_SPL)) {
                            instreamSprecialKey = key;
                            instreamSprecialCoeff = e;
                        } else if (value.equalsIgnoreCase(DataStorageConstants.CARD_ORDER_STATUS)) {
                            orderStatusKey = key;
                            orderStatusCoeff = e;
                        } else if (value.equalsIgnoreCase(DataStorageConstants.CARD_RECOMMEND_COMBO)) {
                            comboKey = key;
                            comboCoeff = e;
                        } else {
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void launchHome() {

        if(!ParseDeepLinkManager.getInstance(this).isHavingDeepLink(getIntent()))
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    //TODO:UNCOMMENT_LATER
                    if (mSharedPref.getStringValue(Constants.ISBOARDINGTUTORIALSHOWN).equals("1")) {

                        if (mSharedPref.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER).trim().length()!=0&&mSharedPref.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) == 1) {
                            mOnBoardingIntent = new Intent(SplashActivity.this, HomeActivity.class);
                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mOnBoardingIntent.putExtra("selected_pos", 1);
                            AppUtils.updateCards(getApplicationContext());
                        } else {
                          //  mOnBoardingIntent = new Intent(SplashActivity.this, OnBoardingMobileNumberActivity.class);
                        }
                    } else if (mSharedPref.getBooleanValue(Constants.APP_STARTED_FIRST_TIME)) {

                       // mOnBoardingIntent = new Intent(SplashActivity.this, OnBoardingTutorialActivity.class);
                        mOnBoardingIntent.putExtra("screen1_title", getString(R.string.onboarding_tutorial1_title));
                        mOnBoardingIntent.putExtra("screen1_subtitle", getString(R.string.onboarding_tutorial1_subtitle));
                        mOnBoardingIntent.putExtra("screen2_title", getString(R.string
                                .onboarding_tutorial2_title));
                        mOnBoardingIntent.putExtra("screen2_subtitle", getString(R.string
                                .onboarding_tutorial2_subtitle));
                        mOnBoardingIntent.putExtra("screen3_title", getString(R.string
                                .onboarding_tutorial3_title));
                        mOnBoardingIntent.putExtra("screen3_subtitle", getString(R.string
                                .onboarding_tutorial3_subtitle));
                        new OnboardingBackgroundTask().execute();
                    } else {

                        mOnBoardingIntent = new Intent(SplashActivity.this, HomeActivity.class);
                        mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mOnBoardingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mOnBoardingIntent.putExtra("refreshHomeScreen", true);
                        mOnBoardingIntent.putExtra("selected_pos", 1);

                        AppUtils.updateCards(getApplicationContext());
                    }

                    SharedPrefManager sharedPrefManager = new SharedPrefManager(SplashActivity.this);
                    sharedPrefManager.setStringValue(Constants.CHECK_HIJACK, "YES");
                    startActivity(mOnBoardingIntent);
                    finish();
                    AppUtils.startActivity(SplashActivity.this);
                }
            }, 2000);
        }else
        {
            finish();
        }
    }


    private class OnboardingBackgroundTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            saveCallLogData();
            setOnBoardingTutorialTitleScreen1();
            setOnBoardingTutorialTitleScreen2();

            return null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mClient != null) {
            if (!mClient.isConnected()) {
                mClient.connect();
            }
            AppIndex.AppIndexApi.start(mClient, getAction());
        }
        onNewIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
        String action = intent.getAction();
        String data = intent.getDataString();
        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            Log.e("AppIndexing", "AppIndexing" + data);
        }
    }

    private void splashAnimation() {
        Animation animationScaleDown = AnimationUtils.loadAnimation(this, R.anim.pop_in_splash);
        AnimationSet growShrink = new AnimationSet(true);
        growShrink.addAnimation(animationScaleDown);
        imageView.startAnimation(growShrink);
    }

    private void saveCallLogData() {
        Uri allCalls = CallLog.Calls.CONTENT_URI;
        String phone_number, person_name;
        int call_duration_in_sec, call_duration_in_min;
        mCallLogList = new ArrayList<>();
        try {
            long date = new Date(Calendar.getInstance().getTimeInMillis() - 30L * 24 * 3600 * 1000).getTime();
            Cursor cursor = getContentResolver().query(allCalls, null, "date" + ">?", new
                    String[]{"" + date}, null);
            if (cursor != null && cursor.moveToFirst()) {
                mTotalCallLogSize = cursor.getCount();
                do {
                    phone_number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                    phone_number = phone_number.replaceAll("\\s", "");
                    if (phone_number.startsWith("+91")) {
                        phone_number = phone_number.substring(3);
                    } else if (phone_number.startsWith("0")) {
                        phone_number = phone_number.substring(1);
                    }
                    if (phone_number.length() > 4) {
                        int call_int = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
                        if (call_int == CallLog.Calls.OUTGOING_TYPE) {
                            person_name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                            call_duration_in_sec = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.DURATION));

                            call_duration_in_min = AppUtils.getMinCallDuration(call_duration_in_sec);
                            long start_time = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
                            int is_night = AppUtils.getIsNightValue(start_time);
                            if (is_night == 1) {
                                mTotalNightCallCount += 1;
                                mTotalNightCallMinute += call_duration_in_min;
                            }
                            mTotalCallDuration += call_duration_in_min;
                            String operator_name = fillCircleAndOperator(phone_number.substring
                                    (0, 5));
                            boolean mAlreadyExist = false;
                            for (int i = 0; i < mCallLogList.size(); i++) {
                                com.getsmartapp.lib.model.CallLog callLog = mCallLogList.get(i);
                                if (callLog.getmPhoneNumber().equals(phone_number)) {
                                    callLog.setmDurationInMinute(callLog.getmDurationInMinute() +
                                            call_duration_in_min);
                                    mCallLogList.set(i, callLog);
                                    mAlreadyExist = true;
                                    break;
                                }
                            }
                            if (!mAlreadyExist) {
                                com.getsmartapp.lib.model.CallLog mCallLog = new com.getsmartapp.lib.model
                                        .CallLog(phone_number, operator_name, person_name,
                                        call_duration_in_min);
                                mCallLogList.add(mCallLog);
                            }
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnBoardingTutorialTitleScreen1() {
        boolean mBoardingTutorialFirstTitle = false;
        for (int i = 0; i < mCallLogList.size(); i++) {
            com.getsmartapp.lib.model.CallLog callLog = mCallLogList.get(i);
            double call_rate = ((double) callLog.getmDurationInMinute() /
                    (double) mTotalCallDuration);
            if ((call_rate >= .3) && callLog.getmDurationInMinute() > 50) {
                mBoardingTutorialFirstTitle = true;
                String person_name = callLog.getmPersonName();
                if (!AppUtils.isStringNullEmpty(person_name)) {
                    if (person_name.contains(" ")) {
                        person_name = person_name.substring(0, person_name.indexOf(" "));
                    }
                    String title = "Cheaper calls to " + person_name + "!";
                    String subtitle = ((int) (call_rate * 100)) + "% of your calls are made to " + person_name + ". Use our intelligent recommendations to cut costs by upto 70%.";
                    mOnBoardingIntent.putExtra("screen1_title", title);
                    mOnBoardingIntent.putExtra("screen1_subtitle", subtitle);
                    break;
                }
            }
        }
        if (!mBoardingTutorialFirstTitle) {
            HashMap<String, Integer> mMostCalledOperator = mostCalled(mCallLogList);
            Set<String> keys = mMostCalledOperator.keySet();
            for (String key : keys) {
                double call_rate = ((double) mMostCalledOperator.get(key) /
                        (double) mTotalCallDuration);

                if (((call_rate >= .3) && (mMostCalledOperator.get(key) > 50))) {
                    if ((key != null) && (!key.equals("null")) && (!key.equals(""))) {
                        String title = "Recharge only what you use!";
                        String subtitle = ((int) (call_rate * 100)) + "% of your calls are made to " + key + " network. Use our intelligent recommendations to cut costs by upto 70%.";
                        mOnBoardingIntent.putExtra("screen1_title", title);
                        mOnBoardingIntent.putExtra("screen1_subtitle", subtitle);
                        break;
                    }
                }
            }
        }
    }

    public void setOnBoardingTutorialTitleScreen2() {
        boolean mBoardingTutorialFirstTitle = false;
        if ((mTotalCallLogSize > 40) && (((mTotalCallDuration * 60) / mTotalCallLogSize) < 50)) {
            mBoardingTutorialFirstTitle = true;
            String title = "Plans made for you!";
            String subtitle = "You keep your calls brief. We recommend using a per second plan. Using it already? Our algorithms can help you save further.";
            mOnBoardingIntent.putExtra("screen2_title", title);
            mOnBoardingIntent.putExtra("screen2_subtitle", subtitle);
        }
        if ((!mBoardingTutorialFirstTitle) && (mTotalNightCallCount > 20) &&
                (mTotalNightCallMinute > 120)) {
            String title = "Plans made for you!";
            String subtitle = "Calls you take in the day are different from your hour long calls at night. We’ll suggest better night tarrifs, helping you maximize on savings.";
            mOnBoardingIntent.putExtra("screen2_title", title);
            mOnBoardingIntent.putExtra("screen2_subtitle", subtitle);
        }
    }


    private void setOnBoardingTutorialTitleScreen3() {
        MobileDataVal mobileDataVal = AppUtils.giveMobileDataVal();
        int total_data = (int) mobileDataVal.getMobile_data() / (1024 * 1024);
        if (total_data >= 50) {
            try {
                List<App> applist = appDataHashmap();
                App topapp = findTopApp(applist);
                float topappdata = topapp.getTotal_dataUsed();
                if (topappdata > total_data * 0.2) {
                    mOnBoardingIntent.putExtra("screen3_title", "Keep track of your usage!");
                    mOnBoardingIntent.putExtra("screen3_subtitle", topapp + " uses more data than any other app on your phone . We'll keep you updated with statistics that matter to you.");
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


        }

    }

    private App findTopApp(List<App> applist) {
        float data = 0;
        String topapp = "";
        HashMap<String, Float> map = new HashMap();
        for (App app : applist) {
            if (app.getTotal_dataUsed() > data && !app.isSys_app()) {
                data = app.getTotal_dataUsed();
                topapp = app.getApp_name();
            }

        }

        return new App(topapp, data);
    }

    private String fillCircleAndOperator(String phone) {

        Cursor c = mSdkAppDatabaseHelper.getCircleAndOperator(phone);
        String operator_name = "";
        if (c.moveToFirst()) {
            operator_name = c.getString(c.getColumnIndex("service_provider_name"));
        }
        c.close();
        return operator_name;
    }

    private HashMap<String, Integer> mostCalled(List<com.getsmartapp.lib.model.CallLog> callLogs) {
        HashMap<String, Integer> map = new HashMap<>();
        for (final com.getsmartapp.lib.model.CallLog callLog : callLogs) {
            Integer count = map.get(callLog.getmOperatorName());
            map.put(callLog.getmOperatorName(), count == null ? callLog.getmDurationInMinute() : count + callLog.getmDurationInMinute());
        }
        return map;
    }

    private List<App> appDataHashmap() throws PackageManager.NameNotFoundException {
        List<App> applist = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        List<ApplicationInfo> packages = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            ApplicationInfo info = packageManager.getApplicationInfo(packageInfo.packageName, 0);
            String installedApps = (String) packageManager.getApplicationLabel(info);
            int UID = packageInfo.uid;
            PackageInfo pi = getPackageManager().getPackageInfo(packageInfo.packageName, 0);
            long txdata = TrafficStats.getUidTxBytes(UID) > 0 ? TrafficStats.getUidTxBytes(UID) : 0;
            long rxdata = TrafficStats.getUidRxBytes(UID) > 0 ? TrafficStats.getUidRxBytes(UID) : 0;
            float totaldata = (txdata + rxdata) / (1024f * 1024f);
            boolean is_system_app = (info.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
            applist.add(new App(installedApps, totaldata, is_system_app));

        }
        return applist;
    }

    private static class ContainerLoadedCallback implements ContainerHolder.ContainerAvailableListener {
        @Override
        public void onContainerAvailable(ContainerHolder containerHolder,
                                         String containerVersion) {
            // Load each container when it becomes available
            Container container = containerHolder.getContainer();
            registerCallbacksForContainer(container);
        }

        public static void registerCallbacksForContainer(Container container) {
        }
    }

    private void checkifDataExists() {
        DataAggregationUtils dau = new DataAggregationUtils(this,0);
        HashMap<String, String> map = new HashMap<>();
        if (AppUtils.isConnectingToInternet(this)) {
            map.put(ApiConstants.DEVICE_ID, dau.getDeviceID());
            map.put(ApiConstants.NOOFDAYS, "30");
            map.put(ApiConstants.JSON_DATA_TYPE, "4");
            RestClient restClient = new RestClient(ApiConstants.AGGREGATION_BASE_URL, null);
            restClient.getApiService().getPreviousAggregatedData(map, new Callback<Response>() {
                @Override
                public void success(Response result, Response response) {
                    if (result != null) {
                        //Try to get response body
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {

                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            String line;

                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        String res = sb.toString();
                        try {
                            JSONArray arr = new JSONArray(res);
                            JSONObject obj = arr.getJSONObject(0);

                            Date last_updated = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a").parse(obj.getString("last_updated_date"));
                            int no_of_days = obj.getInt("daysCount");
                            if (!DateUtil.dataSentBeforeSevenDays(last_updated) && no_of_days > 3) {
                                mSharedPref.setStringValue(Constants.DATA_EXISTS_FLAG, ApiConstants.DATA_AVAILABLE);
                            } else {
                                mSharedPref.setStringValue(Constants.DATA_EXISTS_FLAG, ApiConstants.DATA_NOT_AVAILABLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    public Action getAction() {
        return Action.newAction(Action.TYPE_VIEW, mTitle, WEB_URL, APP_URI);
    }

    @Override
    public void onStop() {
        if (mClient != null) {
            AppIndex.AppIndexApi.end(mClient, getAction());
            mClient.disconnect();
        }
        super.onStop();
    }


}
