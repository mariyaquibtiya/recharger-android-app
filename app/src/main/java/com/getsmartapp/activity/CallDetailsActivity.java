package com.getsmartapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.getsmartapp.R;
import com.getsmartapp.model.CallDetailsModel;

import java.util.ArrayList;

public class CallDetailsActivity extends BaseActivity {

    private ArrayList<CallDetailsModel> mCallDetailsList = new ArrayList<>(6);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            //mCallDetailsList = getIntent().getParcelableArrayListExtra(Constants.EXTRA_CALL_DETAILS);
        }

        setContentView(R.layout.call_deatils_screen);

        ViewPager viewPager = (ViewPager) findViewById(R.id.swipe_view_pager);
        //viewPager.setClipToPadding(false);
        //viewPager.setPadding(left,0,right,0);
        //viewPager.setPageMargin(-100);
        viewPager.setAdapter(new CustomAdapter(this, mCallDetailsList));

    }

    class CustomAdapter extends PagerAdapter {

        private Context mContext;
        private ArrayList<CallDetailsModel> mArrayList;

        public CustomAdapter(Context context, ArrayList<CallDetailsModel> callDetailsList) {
            mContext = context;
            mArrayList = callDetailsList;
        }

        @Override
        public int getCount() {
            return /*mArrayList.size()*/6;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {

            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = View.inflate(mContext, R.layout.call_usage_details_layout, null);
            container.addView(view);

            //inflate with CallDetailsModel
            bindDataWithView();

            return view;
        }

        private void bindDataWithView() {

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}
