package com.getsmartapp.cards;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.managers.RealmDBManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.realmObjects.CallObject;
import com.getsmartapp.util.AppUtils;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author maria.quibtiya on 15/07/16.
 */
public class CallingSummary {

    private String mCardName;
    private Context mContext;
    private SharedPrefManager mSharedPrefManager;
    private RealmDBManager mRealmDBManager;
    //private final int[] mCallingPieColors;
    private List<Map.Entry<CallObject, Integer>> mLastCallDetails;

    public CallingSummary(Context context, String cardName) {
        mCardName = cardName;
        mContext = context;
        mSharedPrefManager = new SharedPrefManager(mContext);
        mRealmDBManager = RealmDBManager.getInstance(context);

        AppUtils.updateOrgCardList(cardName, 1);//for tapped state
        /*mCallingPieColors = new int[]{ContextCompat.getColor(mContext, R.color.pie_blue),
                ContextCompat.getColor(mContext, R.color.pie_green), ContextCompat.getColor(mContext, R.color.pie_yellow)};*/

    }

    public void inflateCallsData(View rootView) {

        View pieChartMainLayout = rootView.findViewById(R.id.piechart_main_layout);

        int incomingColor = mContext.getResources().getColor(R.color.pie_green);
        int outgoingColor = mContext.getResources().getColor(R.color.pie_blue);
        int stdColor = mContext.getResources().getColor(R.color.pie_yellow);

        FitChart fitChart = (FitChart)pieChartMainLayout.findViewById(R.id.data_usage_piechart);
        fitChart.setMinValue(0f);
        fitChart.setMaxValue(100f);

        TextView usageType = (TextView) pieChartMainLayout.findViewById(R.id.usage_type);
        TextView usageTypeValue = (TextView) pieChartMainLayout.findViewById(R.id.usage_type_value);
        usageTypeValue.setTextColor(incomingColor);
        usageTypeValue.setText("90");
        usageType.setText(mContext.getString(R.string.total_calls));

        View usageLayout = rootView.findViewById(R.id.data_value_main_layout);

        TextView dataType1 = (TextView) usageLayout.findViewById(R.id.data_type1);
        TextView dataValueType1 = (TextView) usageLayout.findViewById(R.id.data_value1);
        TextView dataType2 = (TextView) usageLayout.findViewById(R.id.data_type2);
        TextView dataValueType2 = (TextView) usageLayout.findViewById(R.id.data_value2);
        TextView dataType3 = (TextView) usageLayout.findViewById(R.id.data_type3);
        TextView dataValueType3 = (TextView) usageLayout.findViewById(R.id.data_value3);

        dataType1.setTextColor(mContext.getResources().getColor(R.color.white));
        dataType2.setTextColor(mContext.getResources().getColor(R.color.white));
        dataType3.setTextColor(mContext.getResources().getColor(R.color.white));

        dataValueType1.setTextColor(incomingColor);
        dataValueType2.setTextColor(outgoingColor);
        dataValueType3.setTextColor(stdColor);

        dataType1.setText(mContext.getString(R.string.incoming));
        dataType2.setText(mContext.getString(R.string.outgoing));
        dataType3.setText(mContext.getString(R.string.std));

        TextView showingDataTv = (TextView) rootView.findViewById(R.id.data_show_tv);
        showingDataTv.setText(mContext.getString(R.string.calling_usage_text));





        int total_outgoing_min = 0, total_incoming_min = 0;
        int total_outgoing_std_min=0, total_incoming_std_min=0,total_std_duration=0;

        if (mCardName.equalsIgnoreCase(DataStorageConstants.CARD_CALLING_SUMMARY)) {
            int callDays = mSharedPrefManager.getIntValue(Constants.CALLINGUSAGE);

            if(callDays ==0 )
                mSharedPrefManager.setIntValue(Constants.CALLINGUSAGE, 30);
            callDays = mSharedPrefManager.getIntValue(Constants.CALLINGUSAGE);

            if(callDays == -1)
            {
                int initDay = Integer.parseInt(mSharedPrefManager.getStringValue(Constants.ON_BOARDING_BILL_DATE));

                int currentDay = Calendar.getInstance().get(Calendar.DATE);

                int dayDiff = currentDay - initDay + 1;
                if(mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS) > 0)
                {
                    dayDiff = (int) mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS);
                }
                mSharedPrefManager.setIntValue(Constants.CALLINGUSAGE, -1);

                total_outgoing_min = mRealmDBManager.getTotalIncomingMins(dayDiff);//outgoingModal.getInternational_call() + outgoingModal.getLocal_call() + outgoingModal.getNational_call();
                total_incoming_min = mRealmDBManager.getTotalOutgoingMins(dayDiff);//incomingModal.getInternational_call() + incomingModal.getLocal_call() + incomingModal.getNational_call();
            }else if ( total_incoming_min==0 && total_outgoing_min==0) {

                total_incoming_min = mRealmDBManager.getTotalIncomingMins(callDays);//incomingMap.get(com.getsmartapp.lib.sdkconst.DataStorageConstants.MINS);
                total_outgoing_min = mRealmDBManager.getTotalOutgoingMins(callDays);//outgoingMap.get(com.getsmartapp.lib.sdkconst.DataStorageConstants.MINS);
            }

            String duration;
            switch (callDays) {
                case 7:
                    duration = "week";
                    break;
                case 30:
                    duration = "month";
                    break;
                case -1:
                    duration = "bill cycle";
                    break;
                default:
                    duration = "month";
                    break;
            }
            dataValueType2.setText(total_outgoing_min + " min");
            dataValueType1.setText(total_incoming_min + " min");
            
            if ((total_outgoing_min + total_incoming_min) > 0) {
                /*if (total_incoming_min > total_outgoing_min) {
                    subtitle.setText("This " + duration + ", your friends called you more often than you did.");
                } else {
                    subtitle.setText("You made more calls than you received this " + duration + ".");
                }*/
            }


        }



        /*else if (mCardName.equalsIgnoreCase(DataStorageConstants.CARD_STD_SUMMARY))*/ {
            int stdCallDays = mSharedPrefManager.getIntValue(Constants.STDUSAGE);
            if(stdCallDays == 0)
                mSharedPrefManager.setIntValue(Constants.STDUSAGE, 30);
            stdCallDays = mSharedPrefManager.getIntValue(Constants.STDUSAGE);


            if (total_std_duration==0) {

                if(stdCallDays == -1)
                {
                    int initDay = Integer.parseInt(mSharedPrefManager.getStringValue(Constants.ON_BOARDING_BILL_DATE));

                    int currentDay = Calendar.getInstance().get(Calendar.DATE);

                    int dayDiff = currentDay - initDay + 1;
                    if(mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS) > 0)
                    {
                        dayDiff = (int) mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS);
                    }

                    total_outgoing_std_min = mRealmDBManager.getTotalSTDOutgoingMins(dayDiff);//outgoingModal.getInternational_call() + outgoingModal.getLocal_call() + outgoingModal.getNational_call();
                    total_incoming_std_min = mRealmDBManager.getTotalSTDIncomingMins(dayDiff);//incomingModal.getInternational_call() + incomingModal.getLocal_call() + incomingModal.getNational_call();
                }
                else {
                    total_incoming_std_min = mRealmDBManager.getTotalSTDIncomingMins(stdCallDays);//stdInMap.get(com.getsmartapp.lib.sdkconst.DataStorageConstants.MINS);
                    total_outgoing_std_min = mRealmDBManager.getTotalSTDOutgoingMins(stdCallDays);//stdOutMap.get(com.getsmartapp.lib.sdkconst.DataStorageConstants.MINS);
                }
            }

            total_std_duration = total_incoming_std_min + total_outgoing_std_min;
            /*dataValueType2.setText(total_outgoing_std_min + " min");
            dataValueType1.setText(total_incoming_std_min + " min");*/
            String duration;
            switch (stdCallDays) {
                case 7:
                    duration = "week";
                    break;
                case 30:
                    duration = "month";
                    break;
                case -1:
                    duration = "bill cycle";
                    break;
                default:
                    duration = "month";
                    break;
            }

            if (total_std_duration > 0) {
                float percentageout = (float) (total_outgoing_std_min * 100) / total_std_duration;
                if (percentageout > 60) {
                    //subtitle.setText("You made more STD calls than you received this " + duration + ".");
                } else if (percentageout < 40) {
                   // subtitle.setText("Your STD contacts called you more often than you called them this " + duration + ".");
                } else {
                   // subtitle.setText("STD minutes summary for this " + duration + ".");
                }
            } else {
                //subtitle.setText("STD minutes summary for this " + duration + ".");
            }
        }

        int totalCallsForPieChart = total_incoming_min + total_outgoing_min + total_std_duration;
        float percentIncoming = (total_incoming_min * 1.0f) / totalCallsForPieChart;
        float percentOutgoing = (total_outgoing_min * 1.0f) / totalCallsForPieChart;
        float percentStd = (total_std_duration * 1.0f) / totalCallsForPieChart;

        Collection<FitChartValue> values = new ArrayList<>();
        values.add(new FitChartValue(30f, incomingColor));
        values.add(new FitChartValue(20f, outgoingColor));
        values.add(new FitChartValue(15f, stdColor));

        fitChart.setValues(values);


        /*else */{ // top contact
            int total_in_min = 0;
            int total_out_min = 0;
            int total = 0;
            if ((mLastCallDetails == null))
                mLastCallDetails = mRealmDBManager.getTopContacts(30);

            if (mLastCallDetails != null && mLastCallDetails.size() > 0) {


                CallObject callObject = mLastCallDetails.get(0).getKey();
                final String phone = callObject.getContact_number();////lastCallDetails.get(0).getPhone();
                String name = callObject.getContact_name();//lastCallDetails.get(0).getName();
                if (TextUtils.isEmpty(name)) {
                    name = AppUtils.getContactName(mContext, phone);
                }
                final String nameorphone = (!TextUtils.isEmpty(name)) ? name : phone;
                {
                    total_in_min = mRealmDBManager.getTotalIncomingCallByNumber(30, phone);//incomingCalls.get(DataStorageConstants.MINS);
                    total_out_min = mRealmDBManager.getTotalOutgoingCallByNumber(30, phone);//outgoingCalls.get(DataStorageConstants.MINS);
                }
                //title.setText(nameorphone + " and you");
                total = total_in_min + total_out_min;
                if (total > 0) {
                    int incoming_percent = (total_in_min * 100) / total;
                    int totalcalls = mRealmDBManager.getTotalCallsByNumberDays(30, phone);//total_in_min+ total_out_min; //outgoingCalls.get(DataStorageConstants.COUNT)+incomingCalls.get(DataStorageConstants.COUNT);
                    int callDaysCount = 30;
                    if (incoming_percent >= 60) {
                       // subtitle.setText("Of the " + totalcalls + (totalcalls == 1 ? " call" : " calls") + " in the past " + callDaysCount + " days, most calls were made by " + nameorphone + ".");
                    } else if ((100 - incoming_percent) >= 60) {
                       // subtitle.setText("Of the " + totalcalls + (totalcalls == 1 ? " call" : " calls") + " in the past " + callDaysCount + " days, most calls were made by you.");
                    } else {
                       // subtitle.setText("You have spoken " + totalcalls + " times to " + nameorphone + " in the past " + callDaysCount + " days.");
                    }
                } else {
                   // subtitle.setText(messgText);
                }

                /*dataValueType2.setText(total_out_min + (total_out_min == 1 ? " minute" : " minutes"));
                dataValueType1.setText(total_in_min + (total_in_min == 1 ? " minute" : " minutes"));*/

            }
        }
    }

    public void updateDataForDuration(String duration) {
        //get and set details for the specified duration
    }
}
