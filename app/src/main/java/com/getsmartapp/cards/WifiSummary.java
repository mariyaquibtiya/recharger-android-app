package com.getsmartapp.cards;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.lib.managers.RealmDBManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.util.AppUtils;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author maria.quibtiya on 15/07/16.
 */
public class WifiSummary {

    private final Context mContext;
    private final String mCardName;
    private final RealmDBManager mRealmDBManager;
    private final SharedPrefManager mSharedPrefManager;

    public WifiSummary(Context context, String cardName) {
        mContext = context;
        mCardName = cardName;

        AppUtils.updateOrgCardList(cardName, 1);//for tapped state
        mSharedPrefManager = new SharedPrefManager(mContext);
        mRealmDBManager = RealmDBManager.getInstance(mContext);
    }

    public void inflateWifiData(View rootView) {

        View pieChartMainLayout = rootView.findViewById(R.id.piechart_main_layout);

        int pieGreenColor = mContext.getResources().getColor(R.color.pie_green);
        int pieBlueColor = mContext.getResources().getColor(R.color.pie_blue);
        int pieYellowColor = mContext.getResources().getColor(R.color.pie_yellow);

        FitChart fitChart = (FitChart)pieChartMainLayout.findViewById(R.id.data_usage_piechart);
        fitChart.setMinValue(0f);
        fitChart.setMaxValue(100f);

        TextView usageType = (TextView) pieChartMainLayout.findViewById(R.id.usage_type);
        TextView usageTypeValue = (TextView) pieChartMainLayout.findViewById(R.id.usage_type_value);
        usageTypeValue.setText("500 MB");
        usageType.setText(mContext.getString(R.string.wifi_data));

        View usageLayout = rootView.findViewById(R.id.data_value_main_layout);

        TextView dataType1 = (TextView) usageLayout.findViewById(R.id.data_type1);
        TextView dataValueType1 = (TextView) usageLayout.findViewById(R.id.data_value1);
        TextView dataType2 = (TextView) usageLayout.findViewById(R.id.data_type2);
        TextView dataValueType2 = (TextView) usageLayout.findViewById(R.id.data_value2);
        TextView dataType3 = (TextView) usageLayout.findViewById(R.id.data_type3);
        TextView dataValueType3 = (TextView) usageLayout.findViewById(R.id.data_value3);

        dataType1.setTextColor(mContext.getResources().getColor(R.color.white));
        dataType2.setTextColor(mContext.getResources().getColor(R.color.white));
        dataType3.setTextColor(mContext.getResources().getColor(R.color.white));

        dataValueType1.setTextColor(pieGreenColor);
        dataValueType2.setTextColor(pieBlueColor);
        dataValueType3.setTextColor(pieYellowColor);

        dataType1.setText("Wifi-1");
        dataType2.setText("Wifi-2");
        dataType3.setText(mContext.getString(R.string.others));

        TextView showingDataTv = (TextView) rootView.findViewById(R.id.data_show_tv);
        showingDataTv.setText(mContext.getString(R.string.data_usage_text));


        Collection<FitChartValue> values = new ArrayList<>();
        values.add(new FitChartValue(20f, pieGreenColor));
        values.add(new FitChartValue(40f, pieBlueColor));
        values.add(new FitChartValue(25f, pieYellowColor));

        fitChart.setValues(values);
    }
}
