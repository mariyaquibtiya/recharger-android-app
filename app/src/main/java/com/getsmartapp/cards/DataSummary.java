package com.getsmartapp.cards;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.RealmDBManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.DataModel;
import com.getsmartapp.lib.realmObjects.AppObject;
import com.getsmartapp.util.AppUtils;
import com.getsmartapp.util.FormatNumber;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import io.realm.RealmList;

/**
 * @author maria.quibtiya on 15/07/16.
 */
public class DataSummary {

    private String mCardName;
    private Context mContext;
    private SharedPrefManager mSharedPrefManager;
    private RealmDBManager mRealmDBManager;
    private DataModel mDataModel;
    private RealmList<AppObject> mAppList;

    public DataSummary (Context context, String cardName) {
        mCardName = cardName;
        mContext = context;

        AppUtils.updateOrgCardList(cardName, 1);//for tapped state
        mSharedPrefManager = new SharedPrefManager(mContext);
        mRealmDBManager = RealmDBManager.getInstance(mContext);

    }

    public void inflateDataUsage(View rootView) {

        View pieChartMainLayout = rootView.findViewById(R.id.piechart_main_layout);

        int twoGColor = mContext.getResources().getColor(R.color.pie_green);
        int threeGColor = mContext.getResources().getColor(R.color.pie_blue);
        int fourGColor = mContext.getResources().getColor(R.color.pie_yellow);

        FitChart fitChart = (FitChart)pieChartMainLayout.findViewById(R.id.data_usage_piechart);
        fitChart.setMinValue(0f);
        fitChart.setMaxValue(100f);

        TextView usageType = (TextView) pieChartMainLayout.findViewById(R.id.usage_type);
        TextView usageTypeValue = (TextView) pieChartMainLayout.findViewById(R.id.usage_type_value);
        usageTypeValue.setText("500 MB");
        usageType.setText(mContext.getString(R.string.mobile_data));

        View usageLayout = rootView.findViewById(R.id.data_value_main_layout);

        TextView dataType1 = (TextView) usageLayout.findViewById(R.id.data_type1);
        TextView dataValueType1 = (TextView) usageLayout.findViewById(R.id.data_value1);
        TextView dataType2 = (TextView) usageLayout.findViewById(R.id.data_type2);
        TextView dataValueType2 = (TextView) usageLayout.findViewById(R.id.data_value2);
        TextView dataType3 = (TextView) usageLayout.findViewById(R.id.data_type3);
        TextView dataValueType3 = (TextView) usageLayout.findViewById(R.id.data_value3);

        dataType1.setTextColor(mContext.getResources().getColor(R.color.white));
        dataType2.setTextColor(mContext.getResources().getColor(R.color.white));
        dataType3.setTextColor(mContext.getResources().getColor(R.color.white));

        dataValueType1.setTextColor(twoGColor);
        dataValueType2.setTextColor(threeGColor);
        dataValueType3.setTextColor(fourGColor);

        dataType1.setText(mContext.getString(R.string.two_g_data));
        dataType2.setText(mContext.getString(R.string.three_g_data));
        dataType3.setText(mContext.getString(R.string.four_g_data));

        TextView showingDataTv = (TextView) rootView.findViewById(R.id.data_show_tv);
        showingDataTv.setText(mContext.getString(R.string.data_usage_text));


        if (mCardName.equalsIgnoreCase(DataStorageConstants.CARD_DATA_TOP_APPS)) {
            //ArrayList<Map.Entry> yVals1 = new ArrayList<>();
            ArrayList<String> xVals = new ArrayList<>();
            int dataUsageDays = mSharedPrefManager.getIntValue(Constants.DATAUSAGE);
            if (dataUsageDays == 0) {
                dataUsageDays = 30;
                mSharedPrefManager.setIntValue(Constants.DATAUSAGE, 30);
            }

            if (mDataModel == null) {
                mDataModel = AppUtils.getDataUsageDetails(dataUsageDays, AppUtils.getEnumValue(dataUsageDays), mContext);
            }
            float total_data_used = mDataModel.getThreeG() + mDataModel.getTwoG() + mDataModel.getFourG();
            float app_data = 0;

            long totalDataUsed = 0;


            if (AppUtils.getEnumValue(dataUsageDays) == DataStorageConstants.SHOW_CATEGORY_TYPE.BILL_CYCLE_TYPE) {

                int initDay = Integer.parseInt(mSharedPrefManager.getStringValue(Constants.ON_BOARDING_BILL_DATE));
                int currentDay = Calendar.getInstance().get(Calendar.DATE);
                int dayDiff = currentDay - initDay + 1;
                if (mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS) > 0) {
                    dayDiff = (int) mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS);
                }
                mAppList = InternetDataUsageUtil.getInstance(mContext).getAppWiseTotalDataUsageForLastNumberOfDays1(mContext, dayDiff);//, true);
                totalDataUsed = InternetDataUsageUtil.getInstance(mContext).getTotalMobileDataUsageForLastNumberOfDays1(mContext, dayDiff);
            } else {
                mAppList = InternetDataUsageUtil.getInstance(mContext).getAppWiseTotalDataUsageForLastNumberOfDays1(mContext, dataUsageDays);//, true);
                totalDataUsed = InternetDataUsageUtil.getInstance(mContext).getTotalMobileDataUsageForLastNumberOfDays1(mContext, dataUsageDays);
            }

            int appSize = mAppList.size();

            if (mAppList.size() >= 3) {
//                mAppList = mAppList.subList(0, 3);
                appSize = 3;//mAppList.size();
            }

            String timeperiod = "";
            total_data_used = totalDataUsed;

            if (AppUtils.getEnumValue(dataUsageDays) != DataStorageConstants.SHOW_CATEGORY_TYPE.BILL_CYCLE_TYPE) {
                switch (dataUsageDays) {
                    case 7:
                        timeperiod = "week";
                        break;
                    case 30:
                        timeperiod = "month";
                        break;
                    default:
                        timeperiod = "month";
                        break;
                }
            } else
                timeperiod = "bill cycle";
            int count = appSize;

            if (appSize > 0) {
                for (int i = 0; i < ((appSize >= 3) ? 3 : appSize); i++) {

                    AppObject appObject = mAppList.get(i);
                    app_data += appObject.getApp_mobile_data_usage();
                  //  yVals1.add(new Map.Entry(appObject.getApp_mobile_data_usage(), i));

                    xVals.add(appObject.getApp_name() + " - " + InternetDataUsageUtil.humanReadableByteCount((long) appObject.getApp_mobile_data_usage()));
                    String display_name = appObject.getApp_name();
                    String display_txt = "\u25CF   " + display_name;
                    String display_data_usage = "";
                    if (FormatNumber.formatedNumberWithoutUnit(appObject.getApp_mobile_data_usage()) > 0) {

                        display_data_usage = " \u2022 " + InternetDataUsageUtil.humanReadableByteCount((long) appObject.getApp_mobile_data_usage());
                    } else
                        display_data_usage = " \u2022 1 KB";

                    Spannable wordtoSpan = new SpannableString(display_txt);
                    /*try {
                        mChartColor[i] = appchartColor[i];
                        wordtoSpan.setSpan(new ForegroundColorSpan(appchartColor[i]), display_txt.indexOf("\u25CF   "), (display_txt.indexOf("\u25CF   ") + ("\u25CF   ").length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }catch (Exception e){}
                    labels[i].setText(wordtoSpan);
                    labels_data_view[i].setText(display_data_usage);
                    labels[i].setVisibility(View.VISIBLE);
                    labels_data_view[i].setVisibility(View.VISIBLE);*/
                }
//                total_data_used = InternetDataUsageUtil.getInstance(context).getTodaysMobileDataUsed();
                if (total_data_used < app_data) {
                    total_data_used = app_data;
                }
                float total_data_other = total_data_used - app_data;
                if (total_data_other > 0) {
                    count += 1;
                  //  yVals1.add(new Map.Entry(total_data_other, appSize));
                    xVals.add("Others");
                    String other_data_usage = " \u2022 " + InternetDataUsageUtil.humanReadableByteCount((long) total_data_other);
                    Spannable wordtoSpan = new SpannableString("\u25CF   Others");
                    /*mChartColor[appSize] = appchartColor[3];
                    try {
                        wordtoSpan.setSpan(new ForegroundColorSpan(appchartColor[3]), 0, ("\u25CF   Others".indexOf("Others")), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }catch (Exception e){}
                    labels[appSize].setText(wordtoSpan);
                    labels_data_view[appSize].setText(other_data_usage);
                    labels[appSize].setVisibility(View.VISIBLE);
                    labels_data_view[appSize].setVisibility(View.VISIBLE);
                }
                for (int i = count; i < 4; i++) {
                    labels[i].setVisibility(View.GONE);
                    labels_data_view[i].setVisibility(View.GONE);
                }*/
                }

            }
        }

        Collection<FitChartValue> values = new ArrayList<>();
        values.add(new FitChartValue(20f, twoGColor));
        values.add(new FitChartValue(40f, threeGColor));
        values.add(new FitChartValue(25f, fourGColor));

        fitChart.setValues(values);
    }
}
