package com.getsmartapp.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.getsmartapp.R;
import com.getsmartapp.data.DBHelper;
import com.getsmartapp.lib.SmartSDK;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.CallModel;
import com.getsmartapp.lib.model.CardDataModel;
import com.getsmartapp.lib.model.DataModel;
import com.getsmartapp.lib.model.HomeCardsModel;
import com.getsmartapp.lib.model.QueryResult;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.sharedPreference.BranchPrefManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * @author bishwajeet.kumar on 14/08/15.
 */
public class CardUtils {
    private static BranchPrefManager branchPrefManager;
    private static Gson gson;
    private static BufferedReader reader;

    //Read card data from JSON
    public static List<CardDataModel> getCardList(Context context, int type) {
        List<CardDataModel> cardList = new ArrayList<>();
        gson = new Gson();
        try {
            if (type == Constants.INSERT) {
                reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.carddata)));
            } else if (type == Constants.UPDATE) {
                reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.carddata_update)));
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            cardList = gson.fromJson(sb.toString(), new TypeToken<List<CardDataModel>>() {
            }.getType());
        } catch (Exception e) {
        }
        return cardList;
    }

    //Read card category from JSON
    public static Map<Integer, String> getCardCategoryList(Context context) {
        Map<Integer, String> categoryMap = new HashMap<>();
        gson = new Gson();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.cardcategory)));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            categoryMap = gson.fromJson(sb.toString(), new TypeToken<Map<Integer, String>>() {
            }.getType());
        } catch (Exception e) {
        }
        return categoryMap;
    }

    //Read card-category mapping from JSON
    public static List<Integer[]> getCardMappingList(Context context) {
        List<Integer[]> mappingList = new ArrayList<>();
        gson = new Gson();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.mapping)));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            mappingList = gson.fromJson(sb.toString(), new TypeToken<ArrayList<Integer[]>>() {
            }.getType());
        } catch (Exception e) {
        }
        return mappingList;
    }

    //Get raw card list from DB
    public static String getCardRankings(Context context, SQLiteDatabase msqliteDatabase) {
        gson = new Gson();
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        Map<Integer, CardDataModel> initialCardMap = new LinkedHashMap<>();
        Map<Integer, CardDataModel> finalCardMap = new LinkedHashMap<>();
        Map<Integer, Object[]> simpleCardMap;
        List<Integer> emptyPositionList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        LinkedHashMap<Integer, Integer> allCardIdList = new LinkedHashMap<>();
        LinkedHashSet<Integer> cardToExcludeSet = new LinkedHashSet<>();
        Cursor cardCursor = null;
        String cardJson = "";
        int cardPosition = 1;
        try {
            cardCursor = getCardCursor(msqliteDatabase);
            if (cardCursor != null && cardCursor.moveToFirst()) {
                CardDataModel card;
                do {
                    card = new CardDataModel();
                    card.setCard_id(cardCursor.getInt(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_CARD_ID)));
                    card.setCard_name(cardCursor.getString(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_CARD_NAME)));
                    card.setCategory_name(cardCursor.getString(cardCursor.getColumnIndex(DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME)));
                    card.setParent_card_id(cardCursor.getInt(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID)));
                    card.setPos_fixed(cardCursor.getInt(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_POSITION_FIXED)));
                    card.setCoeff_context(cardCursor.getFloat(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT)));
                    card.setVal_recency_fallback(cardCursor.getInt(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK)));
                    card.setVal_context_fallback(cardCursor.getInt(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK)));
                    card.setScore(cardCursor.getFloat(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_SCORE)));
                    card.setExclude_card(cardCursor.getString(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE)));

                    if(card.getExclude_card() != null && !card.getExclude_card().isEmpty()) {
                        cardToExcludeSet = addExcludedCards(cardToExcludeSet, card.getExclude_card());
                    }
                    allCardIdList.put(card.getCard_id(), cardPosition);
                    initialCardMap.put(cardPosition, card);
                    cardPosition++;
                }
                while (cardCursor.moveToNext());
                cardCursor.close();

                excludeCards(initialCardMap, cardToExcludeSet, allCardIdList);
                handleLinkedCardPair(initialCardMap);
                insertFixedCards(initialCardMap, finalCardMap, emptyPositionList);
                insertRemainingCards(initialCardMap, finalCardMap, emptyPositionList);
                simpleCardMap = getSimpleCardRankMap(serializeJSON(finalCardMap));

                cardJson = gson.toJson(simpleCardMap);
//                cardJson = "{\"1\":[\"recommend_special\",1.0],\"2\":[\"recommend_combo\",1.0],\"3\":[\"std_summary\",1.0],\"4\":[\"roaming\",1.0],\"5\":[\"calling_summary\",1.0],\"6\":[\"data_top_apps\",1.0],\"7\":[\"recommend_data\",1.0],\"8\":[\"usage_wifi\",1.0]}";
                Log.e("cardjson", cardJson);
                sharedPrefManager.setStringValue(Constants.CARD_RANK_JSON, cardJson);
            }
        } catch (Exception e) {
        } finally {
            if (cardCursor != null) {
                cardCursor.close();
            }
        }
        return cardJson;
    }

    //Get Card cursor
    private static Cursor getCardCursor(SQLiteDatabase msqliteDatabase) {
        String[] colSelect = new String[]{
                DBContractor.CardDataEntry.COLUMN_CARD_ID,
                DBContractor.CardDataEntry.COLUMN_CARD_NAME,
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME,
                DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID,
                DBContractor.CardDataEntry.COLUMN_POSITION_FIXED,
                DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT,
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK,
                DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK,
                DBContractor.CardDataEntry.COLUMN_SCORE,
                DBContractor.CardDataEntry.COLUMN_CARDS_TO_EXCLUDE};

        return msqliteDatabase.query(DBContractor.CardDataEntry.VIEW_NAME, colSelect, null, null, null, null, null);
    }

    //Insert CardIds in Set
    private static LinkedHashSet<Integer> addExcludedCards(LinkedHashSet<Integer> cardToExcludeSet, String exclude_card) {
        String[] cards = exclude_card.split(",");
        for(String s : cards) {
            cardToExcludeSet.add(Integer.parseInt(s));
        }
        return cardToExcludeSet;
    }

    //Remove Cards of the common category
    private static void excludeCards(Map<Integer, CardDataModel> initialCardMap, LinkedHashSet<Integer> cardToExcludeSet, LinkedHashMap<Integer, Integer> allCardIdList) {
        try {
            Integer[] cardIds = allCardIdList.keySet().toArray(new Integer[allCardIdList.keySet().size()]);
            for(int i = cardIds.length - 1; i >= 0; i--) {
                if(cardToExcludeSet.contains(cardIds[i])) {
                    CardDataModel card = initialCardMap.get(allCardIdList.get(cardIds[i]));
                    for(String s : card.getExclude_card().split(",")) {
                        if(!s.isEmpty() && cardToExcludeSet.contains(Integer.parseInt(s))) {
                            cardToExcludeSet.remove(Integer.parseInt(s));
                        }
                    }
                    initialCardMap.remove(allCardIdList.get(cardIds[i]));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Insert Cards with fixed locations
    private static void insertFixedCards(Map<Integer, CardDataModel> initialCardMap, Map<Integer, CardDataModel> finalCardMap, List<Integer> emptyPositionList) {
        try {
            Iterator cardMapIterator = initialCardMap.values().iterator();
            while (cardMapIterator.hasNext()) {
                CardDataModel card = (CardDataModel) cardMapIterator.next();
                if (card.getPos_fixed() != 0) {
                    finalCardMap.put(card.getPos_fixed(), card);
                    emptyPositionList.remove(emptyPositionList.indexOf(card.getPos_fixed()));
                    cardMapIterator.remove();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Insert Cards at empty locations
    private static void insertRemainingCards(Map<Integer, CardDataModel> initialCardMap, Map<Integer, CardDataModel> finalCardMap, List<Integer> emptyPositionList) {
        try {
            Iterator cardMapIterator = initialCardMap.entrySet().iterator();
            while (cardMapIterator.hasNext()) {
                HashMap.Entry cardEntry = (HashMap.Entry) cardMapIterator.next();
                CardDataModel card = (CardDataModel) cardEntry.getValue();
                finalCardMap.put(emptyPositionList.get(0), card);
                emptyPositionList.remove(0);
                cardMapIterator.remove();

            }
        } catch (Exception e) {
        }
    }

    //Handles Linked Cards
    private static void handleLinkedCardPair(Map<Integer, CardDataModel> initialCardMap) {
        try {
            Iterator cardMapIterator = initialCardMap.entrySet().iterator();
            while (cardMapIterator.hasNext()) {
                HashMap.Entry cardEntry = (HashMap.Entry) cardMapIterator.next();
                CardDataModel card = (CardDataModel) cardEntry.getValue();
                if (card.getParent_card_id() != 0) {
                    getCardByID(card.getParent_card_id(), initialCardMap).setChild_card(card);
                    cardMapIterator.remove();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Serialize Card Json
    private static Map<Integer, CardDataModel> serializeJSON(Map<Integer, CardDataModel> finalCardMap) {
        Map<Integer, CardDataModel> serializedCardMap = new TreeMap<>();
        Iterator cardIterator = finalCardMap.entrySet().iterator();
        try {
            while (cardIterator.hasNext()) {
                HashMap.Entry cardEntry = (HashMap.Entry) cardIterator.next();
                CardDataModel card = (CardDataModel) cardEntry.getValue();
                serializedCardMap.put((Integer) cardEntry.getKey(), card);
            }
        } catch (Exception e) {
        }
        return serializedCardMap;
    }

    //Returns a simple json to be sent for rendering
    private static Map<Integer, Object[]> getSimpleCardRankMap(Map<Integer, CardDataModel> finalCardMap) {
        Map<Integer, Object[]> simpleCardMap = new TreeMap<>();
        Iterator cardIterator = finalCardMap.entrySet().iterator();
        int cardRank = 1;
        try {
            while (cardIterator.hasNext()) {
                HashMap.Entry cardEntry = (HashMap.Entry) cardIterator.next();
                CardDataModel card = (CardDataModel) cardEntry.getValue();
                if (card.getChild_card() != null) {
                    simpleCardMap.put(cardRank++, new Object[]{card.getChild_card().getCard_name(), card.getCoeff_context()});
                    simpleCardMap.put(cardRank++, new Object[]{card.getCard_name(), card.getCoeff_context()});
                } else {
                    simpleCardMap.put(cardRank++, new Object[]{card.getCard_name(), card.getCoeff_context()});
                }
            }
        } catch (Exception e) {
        }
        return simpleCardMap;
    }

    //Get CardDataModel by CardID
    private static CardDataModel getCardByID(int card_id, Map<Integer, CardDataModel> cardMap) {
        Iterator cardMapIterator = cardMap.entrySet().iterator();
        CardDataModel card = new CardDataModel();
        try {
            while (cardMapIterator.hasNext()) {
                HashMap.Entry cardEntry = (HashMap.Entry) cardMapIterator.next();
                card = (CardDataModel) cardEntry.getValue();
                if (card.getCard_id() == card_id) {
                    return card;
                }
            }
        } catch (Exception e) {
        }
        return card;
    }

    //Update Card parameters
    public static void updateCard(Context context, SQLiteDatabase msqliteDatabase) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);

        try {


            updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_TODAY_USAGE, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100 * (-1), DataStorageConstants.HIDE_CARD);

            //Card : referral
            if (branchPrefManager.getShowWelcomeMessage() && (AppUtils.getLoggedInUser(context) == null)) {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_REFERRAL, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 95, DataStorageConstants.SHOW_CARD);
            } else {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_REFERRAL, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100 * (-1), DataStorageConstants.HIDE_CARD);
            }

            //Card: get_referred, launch_offer
            if (!branchPrefManager.getHasMadeFirstTransaction() && !branchPrefManager.getIsReferredUser() && (AppUtils.getLoggedInUser(context) != null) && !branchPrefManager.getIsReferredDevice() && !branchPrefManager.getIsReferredNumber()) {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_GET_REFERRED, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 50, DataStorageConstants.SHOW_CARD);
            } else {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_GET_REFERRED, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100 * (-1), DataStorageConstants.HIDE_CARD);
            }

            //Card : referral_discount
            if (branchPrefManager.getIsActiveInviteeGC() && branchPrefManager.getCurrentUserGCCode() != null && !branchPrefManager.getCurrentUserGCCode().isEmpty() && (AppUtils.getLoggedInUser(context) != null)) {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_REFERRAL_DISCOUNT, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100, DataStorageConstants.SHOW_CARD);
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_REFERRAL, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100 * (-1), DataStorageConstants.HIDE_CARD);
            } else {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_REFERRAL_DISCOUNT, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100 * (-1), DataStorageConstants.HIDE_CARD);
            }

            //Card: Recommendation for Postpaid, Prepaid
            if ("Postpaid".equalsIgnoreCase(sharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE))) {
                hideShowCard(DataStorageConstants.CARD_RECOMMEND_COMBO, DataStorageConstants.HIDE_CARD, context);
                hideShowCard(DataStorageConstants.CARD_RECOMMEND_SPL, DataStorageConstants.HIDE_CARD, context);
                hideShowCard(DataStorageConstants.CARD_RECOMMEND_DATA, DataStorageConstants.HIDE_CARD, context);
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_COMING_SOON, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 50, DataStorageConstants.SHOW_CARD);
            } else if ("Prepaid".equalsIgnoreCase(sharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE))) {
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_RECOMMEND_COMBO, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
                hideShowCard(DataStorageConstants.CARD_RECOMMEND_SPL, DataStorageConstants.SHOW_CARD, context);
                hideShowCard(DataStorageConstants.CARD_RECOMMEND_DATA, DataStorageConstants.SHOW_CARD, context);
                updateCardParameters(msqliteDatabase, DataStorageConstants.CARD_COMING_SOON, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }
        } catch (Exception e) {
        }
    }

    //Update UnTapped Cards
    public static void updateUntappedCard(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        gson = new Gson();

        try {
            if (!sharedPrefManager.getStringValue(Constants.CARD_JSON_FOR_TAPPED).equals("")) {
                JsonParser parser = new JsonParser();
                JsonElement cardString = parser.parse(sharedPrefManager.getStringValue(Constants.CARD_JSON_FOR_TAPPED));
                JsonArray cardArray = cardString.getAsJsonArray();

                Iterator cardIterator = cardArray.iterator();
                while (cardIterator.hasNext()) {
                    JsonObject card = (JsonObject) cardIterator.next();
                    if (Integer.parseInt(card.get("2").toString()) == 1) {
                        Double cardCoeff = CardUtils.getCardCoefficient(sqLiteDatabase, card.get("0").toString());
                        CardUtils.updateCardContextCoefficient(sqLiteDatabase, card.get("0").getAsString(), Math.max(.1, cardCoeff * .6), DataStorageConstants.SHOW_CARD);
                        cardIterator.remove();
                    }
                }
                sharedPrefManager.setStringValue(Constants.CARD_JSON_FOR_TAPPED, "");
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    //Can be used for updating Recency, Context and Visibility
    public static void updateCardParameters(SQLiteDatabase msqliteDatabase, String cardName, String parameter, Integer value, Integer isVisible) {
        try {
            ContentValues values = new ContentValues();
            values.put(parameter, value);
            values.put(DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY, isVisible);
            values.put(DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP, new Date().getTime());

            String selection = DBContractor.CardDataEntry.COLUMN_CARD_NAME + " LIKE ?";
            String[] selectionArgs = {String.valueOf(cardName)};

            msqliteDatabase.update(
                    DBContractor.CardDataEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Can be used for updating Context Coefficient -- User Feedback
    public static void updateCardContextCoefficient(SQLiteDatabase msqliteDatabase, String cardName, Double value, Integer isVisible) {
        try {
            ContentValues values = new ContentValues();
            values.put(DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT, value);
            values.put(DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY, isVisible);
            values.put(DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP, new Date().getTime());

            String selection = DBContractor.CardDataEntry.COLUMN_CARD_NAME + " LIKE ?";
            String[] selectionArgs = {String.valueOf(cardName)};

            msqliteDatabase.update(
                    DBContractor.CardDataEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);

        } catch (SQLiteDatabaseLockedException e) {
            e.printStackTrace();
        }
    }

    public static void updateCardContextCoeffOnUndo(String cardName, double e, Context context) {
        SQLiteDatabase mSQLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();//new DBHelper(mContext).getReadableDatabase();
        double eCoefficient = Math.min(1.0, e * 5);
        if (cardName.equalsIgnoreCase(DataStorageConstants.CARD_FEEDBACK)) {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, cardName, eCoefficient, 1);
        } else if (cardName.equalsIgnoreCase(DataStorageConstants.CARD_ROAMING_ALERT)) {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, cardName, eCoefficient, 1);
        } else if (cardName.equalsIgnoreCase(DataStorageConstants.CARD_COMING_SOON)) {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, cardName, e, 1);
        } else {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, cardName, eCoefficient, 0);
        }
    }

    public static void updateCardContextCoefficientOnDismiss(String mCardName, double e, Context mContext) {
        SQLiteDatabase mSQLiteDatabase = DBHelper.getInstance(mContext).getWritableDatabase();//new DBHelper(mContext).getReadableDatabase();
        double eCoefficient = Math.max(0.1, e * .2);
        if (mCardName.equalsIgnoreCase(DataStorageConstants.CARD_FEEDBACK)) {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, mCardName, eCoefficient, 0);
        } else if (mCardName.equalsIgnoreCase(DataStorageConstants.CARD_ROAMING_ALERT)) {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, mCardName, eCoefficient, 0);
        } else if (mCardName.equalsIgnoreCase(DataStorageConstants.CARD_COMING_SOON)) {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, mCardName, e, 0);
        } else {
            CardUtils.updateCardContextCoefficient(mSQLiteDatabase, mCardName, eCoefficient, 1);
        }
    }

    public static void hideShowCard(String cardName, Integer isVisible, Context context) {
        SQLiteDatabase mSQLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY, isVisible);

            String selection = DBContractor.CardDataEntry.COLUMN_CARD_NAME + " LIKE ?";
            String[] selectionArgs = {String.valueOf(cardName)};

            int count = mSQLiteDatabase.update(
                    DBContractor.CardDataEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);
            Log.e("hideShowCard", cardName + ", " + isVisible);

        } catch (SQLiteDatabaseLockedException e) {
            e.printStackTrace();
        }
    }

    public static void stdCallsPerWeek(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        CallModel outsoingStdCall = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase, 0, Constants.ConnType.STD, Constants.NetworkType.ALL, null, null, Constants.Service.OUTGOING, Constants.THIS_WEEK_START, 0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        int count = outsoingStdCall.getCall_count();

        if (count <= 0) {
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase,DataStorageConstants.CARD_STD_SUMMARY, com.getsmartapp.data.DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK,0,DataStorageConstants.HIDE_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase,DataStorageConstants.CARD_STD_TOP_CONTACTS, com.getsmartapp.data.DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK,0,DataStorageConstants.HIDE_CARD);
            hideShowCard(DataStorageConstants.CARD_STD_TOP_CONTACTS,DataStorageConstants.HIDE_CARD,context);
            hideShowCard(DataStorageConstants.CARD_STD_SUMMARY,DataStorageConstants.HIDE_CARD,context);
            return;
        }
        if (count > 100) {
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
        } else if (count > 40 && count < 100) {
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
        } else {
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 20, DataStorageConstants.SHOW_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 20, DataStorageConstants.SHOW_CARD);
        }
    }

    public static void lastCallTime(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        try {

            List<QueryResult> lastcall = SmartSDK.getInstance(context).getRecentData(Constants.Action.CALL, sqLiteDatabase, 0, null, null, null, null, null, Constants.THIS_MONTH_START, 0, null, "1", Constants.DATA_FOR.LIVE_APP);
            if (lastcall != null && lastcall.size() > 0) {
                QueryResult res = lastcall.get(0);
                long startTimestamp = res.getTimestamp();
                String lastContact = res.getPhone();
                long mins = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTimestamp);

                Integer maxIntVal = (int) Math.max(100 * (1 - (mins / 360)), 0);

                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_CALLING_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, DataStorageConstants.SHOW_CARD);
                List<QueryResult> topContactList = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL, sqLiteDatabase, 0, null, null, null, null, Constants.Service.OUTGOING, Constants.THIS_WEEK_START, 0, null, "2", Constants.DATA_FOR.LIVE_APP);

                if (topContactList != null && topContactList.size() > 0) {
                    for (QueryResult result : topContactList) {
                        String number = result.getPhone();

                        if (lastContact.equalsIgnoreCase(number)) {
                            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_CALLING_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, DataStorageConstants.SHOW_CARD);
                            break;
                        } else {
                            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_CALLING_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, DataStorageConstants.HIDE_CARD);
                        }
                    }
                } else {
                    CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_CALLING_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, DataStorageConstants.HIDE_CARD);
                }


                List<QueryResult> recentOutList = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL, sqLiteDatabase, 0, null, null, null, null, Constants.Service.OUTGOING, Constants.THIS_WEEK_START, 0, null, "8", Constants.DATA_FOR.LIVE_APP);
                if (recentOutList != null && recentOutList.size() > 0) {
                    int contactCount = 1;
                    for (QueryResult topres :
                            recentOutList) {
                        final String number = topres.getPhone();

                        if (lastContact.equalsIgnoreCase(number)) {
                            long time = topres.getTimestamp();
                            long minscon = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - time);
                            Integer maxval = (int) Math.max(100 * (1 - (minscon / 360)), 0);
                            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_CALLS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxval, DataStorageConstants.SHOW_CARD);
                            if (contactCount <= 3) {
                                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_CALLS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
                                break;
                            } else if (contactCount > 3 && contactCount <= 5) {
                                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_CALLS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
                                break;
                            } else if (contactCount > 5 && contactCount <= 8) {
                                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_CALLS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 20, DataStorageConstants.SHOW_CARD);
                                break;
                            }
                        } else {
                            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_CALLS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
                        }
                        contactCount++;
                    }
                }
            } else {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_CALLING_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_CALLS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_CALLING_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }


            List<QueryResult> topStdList = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL, sqLiteDatabase, 0, Constants.ConnType.STD, null, null, null, null, Constants.THIS_MONTH_START, 0, null, "2", Constants.DATA_FOR.LIVE_APP);
            if (topStdList != null && topStdList.size() > 0) {
                for (QueryResult topcon : topStdList) {
                    String number = topcon.getPhone();

                    List<QueryResult> lastStd = SmartSDK.getInstance(context).getRecentData(Constants.Action.CALL, sqLiteDatabase, 0, Constants.ConnType.STD, null, null, null, null, Constants.THIS_MONTH_START, 0, number, "1", Constants.DATA_FOR.LIVE_APP);
                    List<QueryResult> lastoutCall = SmartSDK.getInstance(context).getRecentData(Constants.Action.CALL,sqLiteDatabase,0, Constants.ConnType.STD,null,null,null, Constants.Service.OUTGOING,Constants.THIS_MONTH_START,0,number,null, Constants.DATA_FOR.LIVE_APP);
                    if (lastStd != null && lastStd.size() > 0 && lastoutCall!=null && lastoutCall.size()>0) {
                        QueryResult res = lastStd.get(0);
                        long time = res.getTimestamp();
                        long mins = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - time);

                        Integer maxIntVal = (int) Math.max(100 * (1 - (mins / 360)), 0);
                        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, DataStorageConstants.SHOW_CARD);
                        break;
                    } else {
                        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
                    }

                }
            } else {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_TOP_CONTACTS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }

            List<QueryResult> lastSTDcall = SmartSDK.getInstance(context).getRecentData(Constants.Action.CALL, sqLiteDatabase, 0, Constants.ConnType.STD, null, null, null, null, Constants.THIS_MONTH_START, 0, null, "1", Constants.DATA_FOR.LIVE_APP);
            if (lastSTDcall != null && lastSTDcall.size() > 0) {
                QueryResult res = lastSTDcall.get(0);
                long startTimestamp = res.getTimestamp();
                long mins = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTimestamp);
                Integer maxIntVal = (int) Math.max(100 * (1 - (mins / 360)), 0);
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, DataStorageConstants.SHOW_CARD);
            } else {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_STD_SUMMARY, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }



        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public static void lastSmsTime(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        List<QueryResult> topContact = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL, sqLiteDatabase, 0, null, null, null, null, Constants.Service.OUTGOING, Constants.THIS_WEEK_START, 0, null, "5", Constants.DATA_FOR.LIVE_APP);

        List<QueryResult> recentSmsContacts = SmartSDK.getInstance(context).getRecentData(Constants.Action.SMS, sqLiteDatabase, 0, null, null, null, null, null, Constants.THIS_MONTH_START, 0, null, null, Constants.DATA_FOR.LIVE_APP);
        SharedPrefManager prefs = new SharedPrefManager(context);
        long timestamp = prefs.getLongValue(Constants.ON_BOARDING_TIME);
        long smsTimestamp = 0;
        try {
            if (topContact != null && topContact.size() > 0) {
                for (int i = 0; i < topContact.size(); i++) {
                    String num = topContact.get(i).getPhone();
                    for (int j = 0; j < recentSmsContacts.size(); j++) {
                        String smsnum = recentSmsContacts.get(j).getPhone();
                        if (!TextUtils.isEmpty(smsnum) && smsnum.equalsIgnoreCase(num)) {
                            if (timestamp < smsTimestamp) {
                                timestamp = smsTimestamp;
                            }
                            break;
                        }
                    }
                }
                long mins = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - timestamp);

                Integer maxIntVal = (int) Math.max(100 * (1 - (mins / 360)), 0);

                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, maxIntVal, maxIntVal == 0 ? DataStorageConstants.HIDE_CARD : DataStorageConstants.SHOW_CARD);

                List<QueryResult> topCalledNum = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL, sqLiteDatabase, 0, null, null, null, null, null, Constants.THIS_WEEK_START, 0, null, "1", Constants.DATA_FOR.LIVE_APP);
                if (topCalledNum != null && topCalledNum.size() > 0) {

                    QueryResult out = topCalledNum.get(0);
                    String num = out.getPhone();
                    List<QueryResult> smsmadeByContact = SmartSDK.getInstance(context).getRecentData(Constants.Action.SMS, sqLiteDatabase, 0, null, null, null, null, null, Constants.THIS_WEEK_START, 0, num, null, Constants.DATA_FOR.LIVE_APP);
                    int contacount = smsmadeByContact.size();
                    if (contacount > 20) {
                        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
                    } else if (contacount >= 10 && contacount < 20) {
                        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
                    } else if (contacount >= 5 && contacount < 10) {
                        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 20, DataStorageConstants.SHOW_CARD);
                    } else {
                        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
                    }
                } else {
                    CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
                }

            } else {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECENT_CONTACT_SMS, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.SHOW_CARD);
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public static void appTime(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        JSONArray jsonArray = InternetDataUsageUtil.getInstance(context).getAllData();


        try {

            if (jsonArray.length() > 5) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_DATA_TOP_APPS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
            } else {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_DATA_TOP_APPS, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public static void wifiCard(Context context)
    {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_WIFI_USAGE, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, -1000, DataStorageConstants.HIDE_CARD);


    }

    public static void instreamRecoCard(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        DataModel dataModel = AppUtils.getDataUsageDetails(30, AppUtils.getEnumValue(30), context);

        CallModel outgoingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase, 0, null, null, null, null, Constants.Service.OUTGOING, AppUtils.getStartDuration(30), 0, AppUtils.getEnumValue(30));

        int totalDataMB = Math.round(Float.parseFloat(dataModel.getTotal()));
        int totalCallMins = outgoingModal.getInternational_call() + outgoingModal.getNational_call() + outgoingModal.getLocal_call();

        if (totalDataMB >= (5 * totalCallMins)) {
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECOMMEND_DATA, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECOMMEND_SPL, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
        } else {
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECOMMEND_SPL, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_RECOMMEND_DATA, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
        }
    }

    public static void roamingCards(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);

        Integer daysSinceLastRoaming = Integer.MIN_VALUE;
        Cursor lastRoamingSession = getLastRoamingLocationTime(sqLiteDatabase);
        if (lastRoamingSession != null && lastRoamingSession.moveToFirst()) {
            do {
                final long start_time = lastRoamingSession.getLong(lastRoamingSession.getColumnIndex(DBContractor
                        .CallDataEntry.COLUMN_STARTTIME));
                Lg.w("start time: ", start_time + "");
                daysSinceLastRoaming = Integer.parseInt(DateUtil.getHoursFromTimestamp(start_time)) / 24;

            } while (lastRoamingSession.moveToNext());
        }

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Boolean isRoaming = telephonyManager.isNetworkRoaming();

        Integer roamingCallsToday = callsOnRoaming(sqLiteDatabase, context);

        if (isRoaming) {
            //Handling Roaming Alert Card
            if (DateUtil.twoDaysBefore(sharedPrefManager.getLongValue(Constants.LAST_ROAMING_ALERT_TIMESTAMP))) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING_ALERT, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 100, DataStorageConstants.SHOW_CARD);
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING_ALERT, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 100, DataStorageConstants.SHOW_CARD);
                sharedPrefManager.setLongValue(Constants.LAST_ROAMING_ALERT_TIMESTAMP, System.currentTimeMillis());
            }

            //Handling Roaming Details Card
            //Recency Value
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 100, DataStorageConstants.SHOW_CARD);

            //Context Value
            if (roamingCallsToday > 15) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 80, DataStorageConstants.SHOW_CARD);
            } else if (roamingCallsToday <= 15 && roamingCallsToday > 5) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 60, DataStorageConstants.SHOW_CARD);
            } else if (roamingCallsToday <= 5 && roamingCallsToday >= 1) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 40, DataStorageConstants.SHOW_CARD);
            } else if (roamingCallsToday == 0) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }

            long end_time = new Date().getTime();
            long start_time = end_time - (1000L * 60L * 60L * 24L) * 7L;

            if (getRoamingCallCursor(start_time, end_time, sqLiteDatabase)) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }

        } else {
            //Handling Roaming Alert Card
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING_ALERT, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING_ALERT, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, 0, DataStorageConstants.HIDE_CARD);

            //Handling Roaming Details Card
            //Recency Value
            Integer tempVal = Math.max(100 * (1 - (daysSinceLastRoaming / 14)), 0);
            CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK, tempVal, DataStorageConstants.SHOW_CARD);

            //Context Value
            if (roamingCallsToday > 10) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 70, DataStorageConstants.SHOW_CARD);
            } else if (roamingCallsToday <= 10 && roamingCallsToday > 5) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 50, DataStorageConstants.SHOW_CARD);
            } else if (roamingCallsToday <= 5 && roamingCallsToday >= 1) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 30, DataStorageConstants.SHOW_CARD);
            } else if (roamingCallsToday == 0) {
                CardUtils.updateCardParameters(sqLiteDatabase, DataStorageConstants.CARD_ROAMING, DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, 0, DataStorageConstants.HIDE_CARD);
            }
        }
    }

    private static boolean getRoamingCallCursor(long start_time, long end_time, SQLiteDatabase msqliteDatabase) {
        boolean result = true;
        Cursor cursor = null;
        try {
            if (end_time == 0) {
                cursor = msqliteDatabase.query(DBContractor.CallDataEntry.TABLE_NAME, null,
                        DBContractor.CallDataEntry.COLUMN_STARTTIME + " > " + String.valueOf(start_time), null, null, null, null);
            } else {
                cursor = msqliteDatabase.query(DBContractor.CallDataEntry.TABLE_NAME, null,
                        DBContractor.CallDataEntry.COLUMN_STARTTIME + " > " + String.valueOf(start_time) + " and " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " < " + String.valueOf(end_time), null, null, null, null);
            }
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int isRoaming = cursor.getInt(cursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_IS_ROAMING));
                    if (isRoaming == 0) {
                        result = false;
                        break;
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return result;
    }

    public static Cursor getLastRoamingLocationTime(SQLiteDatabase sqLiteDatabase) {
        return sqLiteDatabase.rawQuery("SELECT " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + " = 1 group by " + DBContractor.CallDataEntry
                .COLUMN_LOCATION_STATE + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " DESC " +
                "LIMIT 1", null);
    }

    public static int callsOnRoaming(SQLiteDatabase sqLiteDatabase, Context context) {

        CallModel incomingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase, 0, null, null, Constants.Roaming.YES, null, Constants.Service.INCOMING, Constants.TODAY_START, 0, DataStorageConstants.SHOW_CATEGORY_TYPE.TODAY_TYPE);
        CallModel outgoingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase, 0, null, null, Constants.Roaming.YES, null, Constants.Service.OUTGOING, Constants.TODAY_START, 0, DataStorageConstants.SHOW_CATEGORY_TYPE.TODAY_TYPE);
        int total_outgoing_min = outgoingModal.getInternational_call() + outgoingModal.getLocal_call() + outgoingModal.getNational_call();
        int total_incoming_min = incomingModal.getInternational_call() + incomingModal.getLocal_call() + incomingModal.getNational_call();

        return total_incoming_min + total_outgoing_min;
    }

    private static Double getCardCoefficient(SQLiteDatabase msqliteDatabase, String cardName) {
        Double cardCoeff = 0.0;
        String query = "SELECT " + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " WHERE " + DBContractor.CardDataEntry.COLUMN_CARD_NAME + " = " + cardName + ";";
        Cursor card = null;

        try {
            card = msqliteDatabase.rawQuery(query, null);

            if (card != null && card.moveToFirst()) {
                cardCoeff = card.getDouble(card.getColumnIndex(DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT));
            }
        } catch (Exception e) {
            if (card != null && !card.isClosed()) {
                card.close();
            }
        } finally {
            if (card != null && !card.isClosed()) {
                card.close();
            }
        }
        return cardCoeff;
    }

    public static void parseCardsJsonOrg() {
        List<String> strList = Arrays.asList(DataStorageConstants.CARDS_ARRAY);
        List<HomeCardsModel> cardsList = new ArrayList<>();

        for (String cardName : strList) {
            HomeCardsModel model = new HomeCardsModel();
            model.setCardName(cardName);
            model.setIsTapped(0);
            cardsList.add(model);
        }
        AppUtils.setOrgCardList(cardsList);
    }

    public static void resetContextCoefficient(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        Cursor cardCursor = null;

        try {
            String[] colSelect = new String[]{DBContractor.CardDataEntry.COLUMN_CARD_NAME, DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY};

            long oneDay = 1000L * 60L * 60L * 24L;
            long lastDayTimestamp = new Date().getTime() - oneDay;
            long secLastDayTimestamp = new Date().getTime() - (2L * oneDay);
            String[] selectionArgs = {String.valueOf(secLastDayTimestamp), String.valueOf(lastDayTimestamp)};

            cardCursor = sqLiteDatabase.query(DBContractor.CardDataEntry.TABLE_NAME, colSelect, DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP + " BETWEEN ? AND ? ", selectionArgs, null, null, null);

            if (cardCursor != null && cardCursor.moveToFirst()) {
                do {
                    String cardName = cardCursor.getString(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_CARD_NAME));
                    Integer cardVisibility = cardCursor.getInt(cardCursor.getColumnIndex(DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY));
                    CardUtils.updateCardContextCoefficient(sqLiteDatabase, cardName, 1.0, cardVisibility);
                }
                while (cardCursor.moveToNext());
                cardCursor.close();
            }
        } catch (Exception e) {
        } finally {
            if (cardCursor != null) {
                cardCursor.close();
            }
        }
    }

}
