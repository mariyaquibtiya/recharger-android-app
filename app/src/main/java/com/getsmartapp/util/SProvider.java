package com.getsmartapp.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.getsmartapp.R;
import com.getsmartapp.lib.constants.Constants;

/**
 * @author Shalakha.Gupta on 31-05-2015.
 */
public class SProvider {

    public SProvider() {
    }

    public Drawable getSproviderDrawable(Context context,String name){
        if(name.equalsIgnoreCase("Vodafone")){
            return context.getResources().getDrawable(R.drawable.vodafone);
        }
        else  if(name.equalsIgnoreCase("Airtel")){
            return context.getResources().getDrawable(R.drawable.airtel);
        }
        else  if(name.equalsIgnoreCase("Aircel")){
            return context.getResources().getDrawable(R.drawable.aircel);
        }
        else  if(name.equalsIgnoreCase("bsnl")){
            return context.getResources().getDrawable(R.drawable.bsnl);
        }
        else  if(name.equalsIgnoreCase("tata Docomo")){
            return context.getResources().getDrawable(R.drawable.docomo);
        }
        else  if(name.equalsIgnoreCase("idea")){
            return context.getResources().getDrawable(R.drawable.idea);
        }
        else  if(name.equalsIgnoreCase("Tata Indicom")){
            return context.getResources().getDrawable(R.drawable.indicom);
        }
        else  if(name.equalsIgnoreCase("mtnl")){
            return context.getResources().getDrawable(R.drawable.mtnl);
        } else  if(name.equalsIgnoreCase("mts")){
            return context.getResources().getDrawable(R.drawable.mts);
        }
        else  if(name.contains("jio")) {
            return context.getResources().getDrawable(R.drawable.jio);
        }
        else  if(name.contains("Reliance")){
            return context.getResources().getDrawable(R.drawable.reliance);
        }
        else  if(name.equalsIgnoreCase("t24")){
            return context.getResources().getDrawable(R.drawable.t24);
        }
        else  if(name.equalsIgnoreCase("uninor")){
            return context.getResources().getDrawable(R.drawable.uninor);
        }
        else  if(name.equalsIgnoreCase("videocon")){
            return context.getResources().getDrawable(R.drawable.videocon);
        }
        else  if(name.equalsIgnoreCase("virgin")){
            return context.getResources().getDrawable(R.drawable.virgin);
        }
        else  if(name.contains("etisalat")){
            return context.getResources().getDrawable(R.drawable.etisalat);
        }
        else  if(name.equalsIgnoreCase("loop")){
            return context.getResources().getDrawable(R.drawable.loop);
        }
        else  if(name.contains("S Tel")) {
            return context.getResources().getDrawable(R.drawable.stel);
        }
        else  if(name.contains("jio")) {
            return context.getResources().getDrawable(R.drawable.jio);
        }
        else{
            return context.getResources().getDrawable(R.drawable.default_sim);
        }
    }

    public Drawable getOrderStatusIcon(Context context,String orderStatus){
        if(orderStatus.equalsIgnoreCase(Constants.ORDER_SUCCESSFUL)){
            return context.getResources().getDrawable(R.drawable.success);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_FAILED)){
            return context.getResources().getDrawable(R.drawable.failed);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_INIT)){
            return context.getResources().getDrawable(R.drawable.process);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PARTIALLY_SUCCESSFUL)){
            return context.getResources().getDrawable(R.drawable.partly);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PENDING)){
            return context.getResources().getDrawable(R.drawable.process);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PAYMENT_REFUNDED)){
            return context.getResources().getDrawable(R.drawable.partly);
        }
        else{
            return null;
        }
    }

    public int getOrderStatusTextColor(Context context,String orderStatus){
        if(orderStatus.equalsIgnoreCase(Constants.ORDER_SUCCESSFUL)){
            return context.getResources().getColor(R.color.order_success);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_FAILED)){
            return context.getResources().getColor(R.color.order_failure);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_INIT)){
            return context.getResources().getColor(R.color.order_pending);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PARTIALLY_SUCCESSFUL)){
            return context.getResources().getColor(R.color.order_partly_success);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PENDING)){
            return context.getResources().getColor(R.color.order_pending);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PAYMENT_REFUNDED)){
            return context.getResources().getColor(R.color.order_partly_success);
        }
        else{
            return 0;
        }
    }

    public String getOrderStatusMessage(Context context,String orderStatus){
        if(orderStatus.equalsIgnoreCase(Constants.ORDER_SUCCESSFUL)){
            return context.getString(R.string.order_success);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_FAILED)){
            return context.getString(R.string.order_failed);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_INIT)){
            return context.getString(R.string.order_pending);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PARTIALLY_SUCCESSFUL)){
            return context.getString(R.string.order_partly_success);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PENDING)){
            return context.getString(R.string.order_pending);
        }
        else  if(orderStatus.equalsIgnoreCase(Constants.ORDER_PAYMENT_REFUNDED)){
            return context.getString(R.string.order_refunded);
        }
            return "";
    }

    public int getOperatorImgRes(String operator)
    {
        switch (operator.toLowerCase())
        {
            case "airtel" : return R.drawable.airtel;
            case "vodafone" :return R.drawable.vodafone;
            case "aircel" : return R.drawable.aircel;
            case "videocon" :return R.drawable.videocon;
            case "reliance cdma" :return R.drawable.reliance;
            case "reliance gsm" :return R.drawable.reliance;
            case "tata docomo" : return R.drawable.docomo;
            case "bsnl" :return R.drawable.bsnl;
            case "mtnl" :return R.drawable.mtnl;
            case "uninor" :return R.drawable.uninor;
            case "virgin cdma" : return R.drawable.virgin;
            case "virgin gsm" : return R.drawable.virgin;
            case "tata indicom" : return R.drawable.indicom;
            case "idea" : return R.drawable.idea;
            case "mts" : return R.drawable.mts;
            case "t24" : return R.drawable.t24;
            case "loop" : return R.drawable.loop;
            case "s tel" : return R.drawable.stel;
        }
        return R.drawable.default_sim;
    }

}
