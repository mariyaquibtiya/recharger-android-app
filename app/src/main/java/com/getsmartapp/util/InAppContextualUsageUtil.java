package com.getsmartapp.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.getsmartapp.data.DBHelper;
import com.getsmartapp.lib.SmartSDK;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.model.App;
import com.getsmartapp.lib.model.CallModel;
import com.getsmartapp.lib.model.DataModel;
import com.getsmartapp.lib.model.QueryResult;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author bishwajeet.kumar on 25/01/16.
 */
public class InAppContextualUsageUtil {

    public static String stdCalling(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        HashMap<String,Integer> outgoingStd = SmartSDK.getInstance(context).getCalltotalData(sqLiteDatabase, 0, Constants.ConnType.STD, null, null, null, Constants.Service.OUTGOING, Constants.THIS_WEEK_START, 0, null, null);
        HashMap<String,Integer> incomingstd = SmartSDK.getInstance(context).getCalltotalData(sqLiteDatabase, 0, Constants.ConnType.STD, null, null, null, Constants.Service.INCOMING, Constants.THIS_WEEK_START, 0, null, null);

        int countOut = outgoingStd.get(com.getsmartapp.lib.constants.DataStorageConstants.COUNT);
        int countIn = incomingstd.get(com.getsmartapp.lib.constants.DataStorageConstants.COUNT);
        String text = "";

        if(countIn != 0 && countOut !=0) {
            if(countIn == countOut) {
                text = "You received equal number of STD calls as you made! ";
            }
            else if(countIn > countOut) {
                text = "You received " + ((countIn-countOut)*100/countOut) + "% more STD calls than you made this week. ";
            }
            else if(countOut > countIn) {
                text = "You made " + ((countOut-countIn)*100/countIn) + "% more STD calls than you received this week. ";
            }
        }
        return text + "Preview STD calling habits with Insights!";
    }

    public static String callingUsage(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        HashMap<String,Integer> outmap = SmartSDK.getInstance(context).getCalltotalData(sqLiteDatabase,0,null,null,null,null, Constants.Service.OUTGOING, com.getsmartapp.lib.constants.Constants.THIS_WEEK_START,0,null,null);
        HashMap<String,Integer> inmap = SmartSDK.getInstance(context).getCalltotalData(sqLiteDatabase,0,null,null,null,null, Constants.Service.INCOMING, com.getsmartapp.lib.constants.Constants.THIS_WEEK_START,0,null,null);
        int countIn = inmap.get(com.getsmartapp.lib.constants.DataStorageConstants.COUNT);
        int countOut = outmap.get(com.getsmartapp.lib.constants.DataStorageConstants.COUNT);
        String text = "";

        if(countIn != 0 && countOut !=0) {
            if(countIn == countOut) {
                text = "You received equal number of calls as you made! ";
            }
            else if(countIn > countOut) {
                text = "You received " + ((countIn-countOut)*100/countOut) + "% more calls than you made this week. ";
            }
            else if(countOut > countIn) {
                text = "You made " + ((countOut-countIn)*100/countIn) + "% more calls than you received this week. ";
            }
        }
        return text + "Learn more from calling habits via Insights.";
    }

    public static String dataUsage(Context context) {
        List<App> mAppList = InternetDataUsageUtil.getInstance(context.getApplicationContext()).getAppWiseTotalDataUsageForLastNumberOfDays(7, false);//AppUtils.calculateMobileDataByApp(context, AppUtils.getStartDuration(7), 0);
        String text = "";
        if (mAppList.size() > 0) {
            App currentApp = mAppList.get(0);
            text = currentApp.getApp_name() + " consumed the most data this week. ";
        }
        return text + "Get precise data usage patterns via insights";
    }

    public static String recentContact(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        String text = "";

        List<QueryResult> topCallinglist = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL, sqLiteDatabase, 0, null, null, null, null, null, com.getsmartapp.lib.constants.Constants.THIS_WEEK_START, 0, null, "1", Constants.DATA_FOR.LIVE_APP);
        try {

            if (topCallinglist != null && topCallinglist.size()>0) {
                QueryResult res = topCallinglist.get(0);
                String name = res.getName();
                String num = res.getPhone();

                if (TextUtils.isEmpty(name)) {
                    name = AppUtils.getContactName(context, num);
                }

                text = (!TextUtils.isEmpty(name)) ? name : (num != null) ? num : "";
                if(!text.equalsIgnoreCase("")) {
                    text = text + " has talked to you the most out of your friends! ";
                }
            }
        }
        catch(Exception e) {

        }
        return text + "Learn more about them via Insights!";
    }

    public static Map<String, String> recentContactDetail(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        Map<String, String> recentContact = new HashMap<>();
        Cursor topContactCursor = null;

        try {
            List<QueryResult> res = SmartSDK.getInstance(context).getTopData(Constants.Action.CALL,sqLiteDatabase,0,null,null,null,null,null, com.getsmartapp.lib.constants.Constants.THIS_WEEK_START,0,null,"1", Constants.DATA_FOR.LIVE_APP);

            if (res != null && res.size()>0) {
                recentContact.put("name", res.get(0).getName());
                recentContact.put("phone", res.get(0).getPhone());
            }
        } catch(Exception e) {
        }
        return recentContact;
    }

    public static String roamingUsage(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        String text = "";
        HashMap<String,Integer> outRoamingmap = SmartSDK.getInstance(context).getCalltotalData(sqLiteDatabase, 0, null, null, Constants.Roaming.YES, null, Constants.Service.OUTGOING, Constants.THIS_WEEK_START, 0, null, null);

        int countOut = outRoamingmap.get(com.getsmartapp.lib.constants.DataStorageConstants.COUNT);
        if(countOut > 0) {
            text = "You made " + countOut + " mins of outgoing calls on roaming over the last week. ";
        }
        return text + "Learn about the roaming usage via Insights.";
    }

    public static String callingUsageForSMS(Context context) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getWritableDatabase();
        String text = "Your Weekly Call Summary: ";

        CallModel totalIncomingCallModel = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase,0,null,null,null,null, Constants.Service.INCOMING,Constants.THIS_WEEK_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        CallModel totalOutgoingCallModel = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase,0,null,null,null,null, Constants.Service.OUTGOING,Constants.THIS_WEEK_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        CallModel totalSTDOutgoingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase,0, Constants.ConnType.STD,null,null,null, Constants.Service.OUTGOING,Constants.THIS_WEEK_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        CallModel totalSTDIncomingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase,0, Constants.ConnType.STD,null,null,null, Constants.Service.OUTGOING,Constants.THIS_WEEK_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        CallModel totalRoamingIncomingCallModel = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase,0, Constants.ConnType.STD,null, Constants.Roaming.YES,null, Constants.Service.INCOMING,Constants.THIS_WEEK_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        CallModel totalRoamingOutgoingCallModel = SmartSDK.getInstance(context).getCallDataForUsagesScreen(sqLiteDatabase,0, Constants.ConnType.STD,null, Constants.Roaming.YES,null, Constants.Service.OUTGOING,Constants.THIS_WEEK_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE);
        int totalIncomingMinutes = totalIncomingCallModel.getInternational_call() + totalIncomingCallModel.getNational_call() + totalIncomingCallModel.getLocal_call();
        int totalOutgoingMinutes = totalOutgoingCallModel.getInternational_call() + totalOutgoingCallModel.getNational_call() + totalOutgoingCallModel.getLocal_call();
        int totalSTDOutgoingMinutes = totalSTDOutgoingModal.getInternational_call() + totalSTDOutgoingModal.getNational_call() + totalSTDOutgoingModal.getLocal_call();
        int totalSTDIncomingMinutes = totalSTDIncomingModal.getInternational_call() + totalSTDIncomingModal.getNational_call() + totalSTDIncomingModal.getLocal_call();
        int totalRoamingIncomingMinutes = totalRoamingIncomingCallModel.getInternational_call() + totalRoamingIncomingCallModel.getNational_call() + totalRoamingIncomingCallModel.getLocal_call();
        int totalRoamingOutgoingMinutes = totalRoamingOutgoingCallModel.getInternational_call() + totalRoamingOutgoingCallModel.getNational_call() + totalRoamingOutgoingCallModel.getLocal_call();
        int totalCallingMinutes = totalIncomingMinutes + totalOutgoingMinutes;
        int totalSTDCallingMinutes = totalSTDIncomingMinutes + totalSTDOutgoingMinutes;
        int totalRoamingCallingMinutes = totalRoamingIncomingMinutes + totalRoamingOutgoingMinutes;

        HashMap<String ,Integer> totalSMSCountMap = SmartSDK.getInstance(context).getSmsTotalData(sqLiteDatabase,0,null,null,null,null, Constants.Service.OUTGOING,Constants.THIS_WEEK_START,0,null,null);

        int total_sms_count = 0;
        if (totalSMSCountMap != null && totalSMSCountMap.size()>0) {
           total_sms_count = totalSMSCountMap.get(DataStorageConstants.COUNT);

        }

        if(totalCallingMinutes == 0) {
            return null;
        }
        if(totalCallingMinutes > 60) {
            text += "Outgoing: " + totalOutgoingMinutes + " min; Incoming: " + totalIncomingMinutes + " min; STD: " + totalSTDCallingMinutes + " min; SMS Sent: " + total_sms_count + "; Roaming: " + totalRoamingCallingMinutes + " min. Save Money on Calls. Open smartapp for more details.";
        }
        return text;
    }

    public static String dataUsageForSMS(Context context) {
        String text = "Your Weekly Data Summary: ";
        DataModel mDataModel = AppUtils.getDataUsageDetails(7, AppUtils.getEnumValue(7), context);

        float total_data_used = mDataModel.getThreeG() + mDataModel.getTwoG() + mDataModel.getFourG();

        if(total_data_used > 7) {
            text += "Data Used: " + FormatNumber.convertDataUnitsToMB(total_data_used * 1024) +
                    "; Avg Daily Use: " + FormatNumber.convertDataUnitsToMB(total_data_used * 1024 / 7) +
                    "; Wifi Used: " + FormatNumber.convertDataUnitsToMB(mDataModel.getWifi() * 1024) + ". Save data cost. Open smartapp for more details.";
        } else {
            return null;
        }
        return text;
    }

    public static String recommendPlanSMS() {
        String text;
        if(!BranchAndParseUtils.verifyTagSubscription(Constants.INSTREAM_RC)) {
            text = "We found some excellent rate cutter plans to save you around 60% over call costs. Recharge here. " + Constants.DEEPLINK_BROWSE_PLANS_SPL;
        } else if (!BranchAndParseUtils.verifyTagSubscription(Constants.INSTREAM_DATA)) {
            text = "We found some excellent data plans to save you around 60% over data costs. Recharge here. " + Constants.DEEPLINK_BROWSE_PLANS_DATA;
        } else {
            text = "We found some excellent rate cutter plans to save you around 60% over call costs. Recharge here. " + Constants.DEEPLINK_BROWSE_PLANS_SPL;
        }
        return text;
    }

    public static Map<String, String> callingUsageForPush(Context mContext, int type) {
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(mContext).getWritableDatabase();
        Map<String, String> pushData = new HashMap<>();
        String title = "";
        String body = "";
        String returns;

        switch(type) {
            case 1:

                CallModel mOutgoingLastCallModel = SmartSDK.getInstance(mContext).getCallDataForUsagesScreen(sqLiteDatabase,0, Constants.ConnType.ALL,null,null,null, Constants.Service.OUTGOING,Constants.LAST_MONTH_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.LAST_MONTH_TYPE);
                CallModel mOutgoingThisCallModel = SmartSDK.getInstance(mContext).getCallDataForUsagesScreen(sqLiteDatabase,0,null,null,null,null, Constants.Service.INCOMING,Constants.THIS_MONTH_START, 0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_MONTH_TYPE);
                int callThisMonth = mOutgoingThisCallModel.getInternational_call() + mOutgoingThisCallModel.getNational_call() + mOutgoingThisCallModel.getLocal_call();
                int callLastMonth = mOutgoingLastCallModel.getInternational_call() + mOutgoingLastCallModel.getNational_call() + mOutgoingLastCallModel.getLocal_call();

                if(callThisMonth > 60) {
                    returns = "false";
                    if(callThisMonth > callLastMonth) {
                        title = "Getting Busy?";
                        body = "Your outgoing calls have increased by " + ((callThisMonth - callLastMonth)*100)/callLastMonth + "% since last month";
                    } else {
                        title = "Much Needed Break!";
                        body = "Your outgoing calls have decreased by " + ((callLastMonth - callThisMonth)*100)/callLastMonth + "% since last month";
                    }
                } else {
                    returns = "true";
                }

                pushData = fillMap(pushData, returns, title, body, "com.getsmartapp://deeplink/callingUsage");
                break;

            case 2:

                CallModel totalIncomingCallModel = SmartSDK.getInstance(mContext).getCallDataForUsagesScreen(sqLiteDatabase,0,null,null,null,null, Constants.Service.INCOMING,Constants.THIS_MONTH_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_MONTH_TYPE);
                CallModel totalOutgoingCallModel = SmartSDK.getInstance(mContext).getCallDataForUsagesScreen(sqLiteDatabase,0,null,null,null,null, Constants.Service.OUTGOING,Constants.THIS_MONTH_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_MONTH_TYPE);
                int totalIncomingMinutes = totalIncomingCallModel.getInternational_call() + totalIncomingCallModel.getNational_call() + totalIncomingCallModel.getLocal_call();
                int totalOutgoingMinutes = totalOutgoingCallModel.getInternational_call() + totalOutgoingCallModel.getNational_call() + totalOutgoingCallModel.getLocal_call();
                int totalCallingMinutes = totalIncomingMinutes + totalOutgoingMinutes;

                try {
                    returns = "false";

                        List<QueryResult> topContactList = SmartSDK.getInstance(mContext).getTopData(Constants.Action.CALL,sqLiteDatabase,0,null,null,null,null,null,Constants.THIS_MONTH_START,0,null,"8", Constants.DATA_FOR.LIVE_APP);
                    if (topContactList != null && topContactList.size()>0) {
                        QueryResult res = topContactList.get(0);
                        String name = res.getName();
                        String num = res.getPhone();
                        int mins = res.getHashMap().get(DataStorageConstants.MINS);

                        if (name == null) {
                            name = AppUtils.getContactName(mContext, num);
                        }

                        body = (!TextUtils.isEmpty(name)) ? name : (num != null) ? num : "";
                        if(!body.equalsIgnoreCase("") && mins > 30) {
                            body = "Last month, in " + (mins*100)/totalCallingMinutes + "% of your calls, you were talking to " + body + "!";
                        } else {
                            returns = "true";
                        }
                    } else {
                        returns = "true";
                    }
                } catch(Exception e) {
                    returns = "true";

                }

                pushData = fillMap(pushData, returns, "Favourite Contact?", body, "com.getsmartapp://deeplink/recentContacts");
                break;
            case 3:

                CallModel totalSTDOutgoingModal = SmartSDK.getInstance(mContext).getCallDataForUsagesScreen(sqLiteDatabase,0, Constants.ConnType.STD,null,null,null, Constants.Service.OUTGOING,Constants.THIS_MONTH_START,0, DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_MONTH_TYPE);
                int totalSTDOutgoingMinutes = totalSTDOutgoingModal.getInternational_call() + totalSTDOutgoingModal.getNational_call() + totalSTDOutgoingModal.getLocal_call();


                try {
                    returns = "false";

                    List<QueryResult> mOutgoingCursor = SmartSDK.getInstance(mContext).getTopData(Constants.Action.CALL,sqLiteDatabase,0, Constants.ConnType.STD,null,null,null, Constants.Service.OUTGOING,AppUtils.getStartDuration(30),0,null,"1", Constants.DATA_FOR.LIVE_APP);
                    if (mOutgoingCursor != null && mOutgoingCursor.size()>0) {
                        for (QueryResult result:mOutgoingCursor)  {

                            String num = result.getPhone();
                            String name = result.getName();
                            int mins = result.getHashMap().get(DataStorageConstants.MINS);

                            if (name == null) {
                                name = AppUtils.getContactName(mContext, num);
                            }

                            body = (!TextUtils.isEmpty(name)) ? name : (num != null) ? num : "";
                            if(!body.equalsIgnoreCase("") && mins > 30) {
                                body = "Last month, in " + (mins*100)/totalSTDOutgoingMinutes + "% of your STD calls, you were talking to " + body + "!";
                            } else {
                                returns = "true";
                            }
                        }
                    } else {
                        returns = "true";
                    }
                } catch(Exception e) {
                    returns = "true";
                }

                pushData = fillMap(pushData, returns, "Time to unite!", body, "com.getsmartapp://deeplink/stdcalling");
                break;
            case 4:
                List<QueryResult> res = SmartSDK.getInstance(mContext).getGranulatedData(Constants.Action.CALL,sqLiteDatabase,0,null,null,null,null,Constants.Service.OUTGOING,Constants.THIS_MONTH_START,0,null);
                long totalWeekend = 0;
                long totalWeekday = 0;

                returns = "false";
                if(!res.isEmpty()) {
                    for(QueryResult entry : res) {
                        if(isWeekend(entry.getSdkkey())) {
                            totalWeekend += entry.getHashMap().get("mins");
                        } else {
                            totalWeekday += entry.getHashMap().get("mins");
                        }
                    }
                } else {
                    returns = "true";
                }

                if(totalWeekday >= totalWeekend && totalWeekend != 0) {
                    title = "Time to Relax!";
                    body = "On weekends, your outgoing went down by " + ((totalWeekday - totalWeekend)*100)/totalWeekday + "% this month";
                } else if (totalWeekday < totalWeekend) {
                    title = "Party Plans!";
                    body = "On weekends, your outgoing shot up by " + ((totalWeekend - totalWeekday)*100)/totalWeekday + "% this month";
                }
                pushData = fillMap(pushData, returns, title, body, "com.getsmartapp://deeplink/callingUsage");
                break;
        }
        return pushData;
    }

    public static Map<String, String> dataUsageForPush(Context mContext, int type) {
        Map<String, String> pushData = new HashMap<>();
        InternetDataUsageUtil internetDataUsageUtil = InternetDataUsageUtil.getInstance(mContext);
        String title = "";
        String body = "";
        String returns;

        switch(type) {
            case 1:
                returns = "false";
                HashMap<String, JSONObject> dataMap60Days = internetDataUsageUtil.getDataUsageForLastNumberOfDays(60);
                HashMap<String, JSONObject> dataMap30Days = internetDataUsageUtil.getDataUsageForLastNumberOfDays(30);

                long dataThisMonth = collateData(dataMap30Days);
                long dataLastMonth = collateData(dataMap60Days) - dataThisMonth;

                if(dataThisMonth/(1024*1024) >= 50 && dataLastMonth > 0) {
                    if(dataThisMonth >= dataLastMonth) {
                        title = "Watch Out!";
                        body = "Your data consumption has gone up by " + ((dataThisMonth - dataLastMonth)*100)/dataLastMonth + "% this month";

                    } else if(dataThisMonth < dataLastMonth) {
                        title = "Good Going!";
                        body = "Your data consumption has gone up by " + ((dataLastMonth - dataThisMonth)*100)/dataLastMonth + "% this month";
                    }
                } else {
                    returns = "true";
                }

                pushData = fillMap(pushData, returns, title, body, "com.getsmartapp://deeplink/dataUsage");
                break;
            case 2:
                returns = "false";
                List<App> mAppList = internetDataUsageUtil.getAppWiseTotalDataUsageForLastNumberOfDays(30, false);
                if (mAppList.size() > 0) {
                    App currentApp = mAppList.get(0);
                    if(currentApp.getmobile_dataUsed()/(1024*1024) > 30.0) {
                        body = currentApp.getApp_name() + " consumed more data than any other app this month!";
                    } else {
                        returns = "true";
                    }
                } else {
                    returns = "true";
                }
                pushData = fillMap(pushData, returns, "Favourite App?", body, "com.getsmartapp://deeplink/dataUsage");
                break;
            case 3:
                returns = "false";
                HashMap<String, JSONObject> dataMapDays = InternetDataUsageUtil.getInstance(mContext).getDataUsageForLastNumberOfDays(30);
                long totalWeekend = 0;
                long totalWeekday = 0;

                if(dataMapDays.isEmpty()) {
                    returns = "true";
                }

                Iterator iterator = dataMapDays.entrySet().iterator();
                while(iterator.hasNext()) {
                    try {
                        Map.Entry entry = (Map.Entry) iterator.next();
                        String date = (String)entry.getKey();
                        if(isWeekend(date)) {
                            JSONObject obj = (JSONObject)entry.getValue();
                            if(obj.has("data")) {
                                JSONObject data = obj.getJSONObject("data");
                                if(data.has("totalMobileData")) {
                                    totalWeekend += data.getLong("totalMobileData");
                                }
                            }
                        } else {
                            JSONObject obj = (JSONObject)entry.getValue();
                            if(obj.has("data")) {
                                JSONObject data = obj.getJSONObject("data");
                                if(data.has("totalMobileData")) {
                                    totalWeekday += data.getLong("totalMobileData");
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(totalWeekday >= totalWeekend && totalWeekend != 0) {
                    title = "Good Control!";
                    body = "On weekends, your data consumption went down by " + ((totalWeekday - totalWeekend)*100)/totalWeekday + "% this month";
                } else if (totalWeekday < totalWeekend) {
                    title = "Careful!";
                    body = "On weekends, your data consumption shot up by " + ((totalWeekend - totalWeekday)*100)/totalWeekday + "% this month";
                } else {
                    returns = "true";
                }

                pushData = fillMap(pushData, returns, title, body, "com.getsmartapp://deeplink/dataUsage");
                break;
        }
        return pushData;
    }

    public static Map<String, String> wifiUsageForPush(Context mContext, int type) {
        Map<String, String> pushData = new HashMap<>();
        InternetDataUsageUtil internetDataUsageUtil = InternetDataUsageUtil.getInstance(mContext);
        String title = "";
        String body = "";
        String returns;

        switch(type) {
            case 1:
                returns = "false";
                ArrayList<App> topAppList = internetDataUsageUtil.getAppWiseWifiDataUsageForLastNumberOfDays(30, false);
                if (topAppList.size() > 0) {
                    App currentApp = topAppList.get(0);
                    if(currentApp.getWifi_dataUsed()/(1024*1024) > 100.0) {
                        body = currentApp.getApp_name() + " consumed more wifi data than any other app this month!";
                    } else {
                        returns = "true";
                    }
                } else {
                    returns = "true";
                }
                pushData = fillMap(pushData, returns, "Favourite App?", body, "com.getsmartapp://deeplink/wifiusage");

                break;
            case 2:
                returns = "false";
                ArrayList<JSONObject> jsonObjectArrayList  = internetDataUsageUtil.getWifiListForDays(mContext, 30);

                try {
                    if(!jsonObjectArrayList.isEmpty()) {
                        String wifiSSID = jsonObjectArrayList.get(0).getString(InternetDataUsageUtil.WIFI_SSID);
                        long wifiData = jsonObjectArrayList.get(0).getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED);

                        if(wifiSSID != null && !wifiSSID.isEmpty() && (wifiData/(1024*1024)) > 100.0) {
                            body = "You consumed more wifi data in " + wifiSSID + " than any other network this month!";
                        } else {
                            returns = "true";
                        }
                    } else {
                        returns = "true";
                    }

                } catch(Exception e) {
                    returns = "true";
                    e.printStackTrace();
                }

                pushData = fillMap(pushData, returns, "Favourite Wifi Network?", body, "com.getsmartapp://deeplink/wifiusage");
                break;
        }
        return pushData;
    }

    private static Map<String, String> fillMap(Map<String, String> map, String returns, String title, String body, String deeplink) {
        map.put("return", returns);
        map.put("title", title);
        map.put("body", body);
        map.put("deeplink", deeplink);

        return map;
    }

    private static long collateData(HashMap<String, JSONObject> map) {
        Iterator iterator = map.entrySet().iterator();
        long total = 0;
        while(iterator.hasNext()) {
            try {
                Map.Entry entry = (Map.Entry) iterator.next();
                JSONObject obj = (JSONObject)entry.getValue();
                if(obj.has("data")) {
                    JSONObject data = obj.getJSONObject("data");
                    if(data.has("totalMobileData")) {
                        total += data.getLong("totalMobileData");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    private static boolean isWeekend(String date) {
        int dayOfWeek = 0;
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            cal.setTime(sdf.parse(date));

            dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return (Calendar.SUNDAY == dayOfWeek || Calendar.SATURDAY == dayOfWeek);
    }
}
