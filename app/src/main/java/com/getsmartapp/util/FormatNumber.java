package com.getsmartapp.util;

/**
 * @author nitesh on 18/5/15.
 */
public class FormatNumber {

    public static float formatedNumberWithoutUnit(float data_in_MB) {

        float formattedNumber = 0;
        if (data_in_MB <= 1.0f) {
            formattedNumber = Math.round(data_in_MB * 1024);
        } else if (1.0f < data_in_MB && data_in_MB <= 10.0f) {
            formattedNumber = Float.parseFloat(String.format("%.1f", data_in_MB));
        } else if (data_in_MB > 10.0f && data_in_MB <= 1024.0f) {
            formattedNumber = Math.round(data_in_MB);
        } else if (data_in_MB > 1024.0f) {
            formattedNumber = Float.parseFloat(String.format("%.1f", data_in_MB / 1024.0f));
        } else if (data_in_MB >= 10 * 1024.0f) {
            formattedNumber = Float.parseFloat(String.valueOf(Math.round(data_in_MB / 1024.0f)));
        }
        return formattedNumber;
    }

    public static String convertDataUnits(float data_in_KB) {

        String formattedNumber = "";
        String units = "";
        if (data_in_KB <= 900f) {
            formattedNumber = String.valueOf(Math.round(data_in_KB));
            units = "KB";
        } else if (900f < data_in_KB && (data_in_KB / 1024) <= 9.9f) {
            formattedNumber = String.format("%.1f", (data_in_KB / 1024));
            units = "MB";
        } else if ((data_in_KB / 1024) > 9.9f && (data_in_KB / 1024) <= 999) {
            formattedNumber = String.valueOf(Math.round((data_in_KB / 1024)));
            units = "MB";
        } else if ((data_in_KB / 1024) > 1000f && (data_in_KB / (1024 * 1024)) <= 9.9) {
            formattedNumber = String.format("%.1f", (data_in_KB / (1024 * 1024)));
            units = "GB";
        } else if ((data_in_KB / (1024 * 1024)) > 9.9) {
            formattedNumber = String.valueOf(Math.round((data_in_KB / (1024 * 1024))));
            units = "GB";
        }
        return formattedNumber + " " + units;
    }

    public static String convertDataUnitsToMB(float data_in_KB) {
        String formattedNumber = "0";
        String units = "MB";
        if(900f < data_in_KB) {
            formattedNumber = String.format("%.1f", (data_in_KB / 1024));
            units = "MB";
        }
        return formattedNumber + " " + units;
    }

    public static String dataUsagePieChartDataUnits(float data_in_KB) {

        String formattedNumber = "";
        String units = "";
        if (data_in_KB <= 900f) {
            formattedNumber = String.valueOf(Math.round(data_in_KB));
            units = "KILOBYTES";
        } else if (900f < data_in_KB && (data_in_KB / 1024) <= 9.9f) {
            formattedNumber = String.format("%.1f", (data_in_KB / 1024));
            units = "MEGABYTES";
        } else if ((data_in_KB / 1024) > 9.9f && (data_in_KB / 1024) <= 999) {
            formattedNumber = String.valueOf(Math.round((data_in_KB / 1024)));
            units = "MEGABYTES";
        } else if ((data_in_KB / 1024) > 1000f && (data_in_KB / (1024 * 1024)) <= 9.9) {
            formattedNumber = String.format("%.1f", (data_in_KB / (1024 * 1024)));
            units = "GIGABYTES";
        } else if ((data_in_KB / (1024 * 1024)) > 9.9) {
            formattedNumber = String.valueOf(Math.round((data_in_KB / (1024 * 1024))));
            units = "GIGABYTES";
        }
        return formattedNumber + "\n" + units;
    }
}
