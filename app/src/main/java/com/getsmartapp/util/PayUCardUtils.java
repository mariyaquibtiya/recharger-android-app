package com.getsmartapp.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.getsmartapp.R;
//import com.payu.india.Payu.PayuConstants;

/**
 * @author shalakha.gupta on 18/04/16.
 */
public class PayUCardUtils {


    public static int getMaxCardLength( String issuer) {
        /*switch (issuer) {
            case PayuConstants.VISA:
                return 16;
            case PayuConstants.LASER:
                return 18;
            case PayuConstants.DISCOVER:
                return 16;
            case PayuConstants.MAES:
                return 16;
            case PayuConstants.MAST:
                return 16;
            case PayuConstants.AMEX:
                return 15;
            case PayuConstants.DINR:
                return 14;
            case PayuConstants.JCB:
                return 16;
            case PayuConstants.SMAE:
                return 19;
            case PayuConstants.RUPAY:
                return 16;
        }*/
        return 16;
    }


    public static Drawable getIssuerDrawable(String issuer, Context context) {

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            /*switch (issuer) {
                case PayuConstants.VISA:
                    return context.getResources().getDrawable(R.drawable.visa);
                case PayuConstants.LASER:
                    return context.getResources().getDrawable(R.drawable.laser);
                case PayuConstants.DISCOVER:
                    return context.getResources().getDrawable(R.drawable.discover);
                case PayuConstants.MAES:
                    return context.getResources().getDrawable(R.drawable.maestro);
                case PayuConstants.MAST:
                    return context.getResources().getDrawable(R.drawable.master);
                case PayuConstants.AMEX:
                    return context.getResources().getDrawable(R.drawable.amex);
                case PayuConstants.DINR:
                    return context.getResources().getDrawable(R.drawable.diner);
                case PayuConstants.JCB:
                    return context.getResources().getDrawable(R.drawable.jcb);
                case PayuConstants.SMAE:
                    return context.getResources().getDrawable(R.drawable.maestro);
                case PayuConstants.RUPAY:
                    return context.getResources().getDrawable(R.drawable.rupay);
            }*/
            return null;
        } else {

            /*switch (issuer) {
                case PayuConstants.VISA:
                    return context.getResources().getDrawable(R.drawable.visa, null);
                case PayuConstants.LASER:
                    return context.getResources().getDrawable(R.drawable.laser, null);
                case PayuConstants.DISCOVER:
                    return context.getResources().getDrawable(R.drawable.discover, null);
                case PayuConstants.MAES:
                    return context.getResources().getDrawable(R.drawable.maestro, null);
                case PayuConstants.MAST:
                    return context.getResources().getDrawable(R.drawable.master, null);
                case PayuConstants.AMEX:
                    return context.getResources().getDrawable(R.drawable.amex, null);
                case PayuConstants.DINR:
                    return context.getResources().getDrawable(R.drawable.diner, null);
                case PayuConstants.JCB:
                    return context.getResources().getDrawable(R.drawable.jcb, null);
                case PayuConstants.SMAE:
                    return context.getResources().getDrawable(R.drawable.maestro, null);
                case PayuConstants.RUPAY:
                    return context.getResources().getDrawable(R.drawable.rupay, null);
            }*/
            return null;
        }
    }

    public static String getCardTypeText(String cardType) {
        /*if (cardType.startsWith(PayuConstants.VISA)) {
            return "Visa Card";
        } else if (cardType.startsWith(PayuConstants.LASER)) {
            return "Laser Card";
        } else if (cardType.startsWith(PayuConstants.DISCOVER)) {
            return "Discover Card";
        } else if (cardType.startsWith(PayuConstants.MAES)) {
            return "Maestro Card";
        } else if (cardType.startsWith(PayuConstants.MAST)) {
            return "Master Card";
        } else if (cardType.startsWith(PayuConstants.AMEX)) {
            return "Amex Card";
        } else if (cardType.startsWith(PayuConstants.DINR)) {
            return "Diner Club Card";
        } else if (cardType.startsWith(PayuConstants.JCB)) {
            return "JCB Card";
        } else if (cardType.startsWith(PayuConstants.SMAE)) {
            return "SMAE Card";
        }
        else if (cardType.startsWith(PayuConstants.RUPAY)) {
            return "Rupay Card";
        }*/
        return "";
    }

    public static int getCardTypeImage(String cardType) {
        /*if (cardType.startsWith(PayuConstants.VISA)) {
            return R.drawable.visa;
        } else if (cardType.startsWith(PayuConstants.LASER)) {
            return R.drawable.laser;
        } else if (cardType.startsWith(PayuConstants.DISCOVER)) {
            return R.drawable.discover;
        } else if (cardType.startsWith(PayuConstants.MAES)) {
            return R.drawable.maestro;
        } else if (cardType.startsWith(PayuConstants.MAST)) {
            return R.drawable.master;
        } else if (cardType.startsWith(PayuConstants.AMEX)) {
            return R.drawable.amex;
        } else if (cardType.startsWith(PayuConstants.DINR)) {
            return R.drawable.diner;
        } else if (cardType.startsWith(PayuConstants.JCB)) {
            return R.drawable.jcb;
        } else if (cardType.startsWith(PayuConstants.SMAE)) {
            return R.drawable.maestro;
        }
        else if (cardType.startsWith(PayuConstants.RUPAY)) {
            return R.drawable.rupay;
        }*/
        return R.drawable.default_card;
    }


    public static String getSpacedCardNo(String orgCardNo) {
        try {
            int interval = 4;
            char separator = ' ';

            StringBuilder sb = new StringBuilder(orgCardNo);

            for (int i = 0; i < orgCardNo.length() / interval; i++) {
                sb.insert(((i + 1) * interval) + i, separator);
            }
            return sb.toString().trim();
        } catch (Exception e) {
            return "";
        }
    }

    public static String getSpacedCardNoForAmexDiner(String orgCardNo) {
        try {
            int interval = 4;
            char separator = ' ';

            StringBuilder sb = new StringBuilder(orgCardNo);
            sb.insert(4, separator);
            sb.insert(11, separator);
            sb.replace(5, 11, "******");
            return sb.toString().replace("X", "*").trim();
        } catch (Exception e) {
            return "";
        }
    }
}
