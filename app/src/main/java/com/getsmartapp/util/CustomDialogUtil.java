package com.getsmartapp.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Spannable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.adapter.SaveRecentRechargesAdapter;
import com.getsmartapp.interfaces.DeleteCardDialogListener;
import com.getsmartapp.interfaces.DialogOkClickListner;
import com.getsmartapp.interfaces.InternetConnectionListener;
import com.getsmartapp.interfaces.RecentRechargeListener;
import com.getsmartapp.interfaces.UpdateRequestText;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.RecentRechargeItem;
import com.getsmartapp.lib.constants.Constants;

import java.util.Calendar;
import java.util.List;

/**
 * @author Shalakha.Gupta on 26-06-2015.
 */
public class CustomDialogUtil {

    private static SharedPrefManager mSharedPref;

    public static Dialog showInternetLostDialog(final Context context, final InternetConnectionListener listener) {
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return null;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);
        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText("No internet connection");
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(context.getString(R.string.no_internet));

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        yesBtn.setText(context.getString(R.string.retry_net));
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onGoToSettingsClick();
                }
            }
        });

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!AppUtils.isConnectingToInternet(context)) {
                    showInternetLostDialog(context, listener);
                } else {
                    if (listener != null) {
                        listener.onRetryClick();
                    }
                }
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onGoToSettingsClick();
                }
            }
        });
        dialog.show();
        return dialog;
    }


    public static void showAlertDialog(Context context, String title, String message, final DialogOkClickListner listener) {
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);


        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        noBtn.setVisibility(View.GONE);
        yesBtn.setText("OK");
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onOKClick("");

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public static Dialog showRecentRechargeDialog(final Context context, final RecentRechargeListener listener,
                                                  List<RecentRechargeItem> rrList) {
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return null;
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.recent_recharge);


        ListView lvrecentRecharge = (ListView) dialog.findViewById(R.id.lvrecentRecharge);
        lvrecentRecharge.setAdapter(new SaveRecentRechargesAdapter(context, rrList));
        lvrecentRecharge.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listener != null)
                    listener.onRecentRechargeItemClick(position);
                dialog.dismiss();
            }
        });
        return dialog;
    }


    public static void showCustomDialog(final Context context, final String title, Spannable message,
                                        final DialogOkClickListner listener, final String cardNo) {
        // custom dialog
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);


        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onOKClick(cardNo);

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showCustomDialog(final Context context, final String title, Spannable message,
                                        final DialogOkClickListner listener, String yesText, String noText, final String cardNo) {
        // custom dialog
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);


        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        noBtn.setText(noText);
        yesBtn.setText(yesText);

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onOKClick(cardNo);

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showCustomDialog(final Context context, String message,
                                        final View.OnClickListener listener, final boolean forceUpdate) {
        // custom dialog
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.card_deletion_layout);

        mSharedPref = new SharedPrefManager(context);

        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        if(forceUpdate) {
            titleView.setText(mContext.getResources().getString(R.string.hard_update_title));
            messageView.setText(mContext.getResources().getString(R.string.hard_update_body));
            noBtn.setVisibility(View.GONE);
        }else
        {
            titleView.setText(mContext.getResources().getString(R.string.soft_update_title));
            messageView.setText(mContext.getResources().getString(R.string.soft_update_body));

        }

        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);

        noBtn.setText("Update Later!");

        yesBtn.setText("Update Now!");

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mSharedPref.setLongValue(Constants.APP_UPDATE_SOFT_UPDATE_DIALOG_SHOWN_LAST_TIME, Calendar.getInstance().getTimeInMillis());

                listener.onClick(v);
            }
        });

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v);
            }
        });
        dialog.show();
    }


    public static Dialog showRequestSupportDialog(final Context context, final String title,
                                                  final String message, UpdateRequestText listner, final long remainSecs) {
        // custom dialog
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return null;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);


        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        final TextView messageView = (TextView) dialog.findViewById(R.id.message);
        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        noBtn.setVisibility(View.INVISIBLE);
        yesBtn.setText(context.getString(R.string.ok));
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if(listner!=null)
            listner.updateRequestText(messageView,message,remainSecs);
        return dialog;

    }


    public static void showLoadMoneyDialog(final Context context, final String title, Spannable message) {
        // custom dialog
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);


        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        noBtn.setVisibility(View.GONE);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        yesBtn.setText(context.getString(R.string.ok));

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static void showAlertDialogCancleOutside(Context context, String title, String message, final DialogOkClickListner listener) {
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return ;
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (listener != null)
                    listener.onOKClick("");

                dialog.dismiss();
            }
        });

        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        noBtn.setVisibility(View.GONE);
        yesBtn.setText("OK");
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onOKClick("");

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showAlertDialog(Context context, String title, String message, final DialogOkClickListner listener, boolean isCancelable) {
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);
        dialog.setCanceledOnTouchOutside(isCancelable);
        dialog.setCancelable(isCancelable);
        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);
        noBtn.setVisibility(View.GONE);
        yesBtn.setText("OK");
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onOKClick("");

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public static void showDeleteCardDialog(final Context context, final String title, Spannable message,
                                            final DeleteCardDialogListener listener, final String cardNo, final int oneTapEnable) {
        // custom dialog
        Activity mContext = (Activity) context;
        if ((mContext == null) || mContext.isFinishing())
            return;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.card_deletion_layout);


        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        titleView.setText(title);
        TextView messageView = (TextView) dialog.findViewById(R.id.message);
        messageView.setText(message);

        TextView noBtn = (TextView) dialog.findViewById(R.id.no_btn);
        TextView yesBtn = (TextView) dialog.findViewById(R.id.yes_btn);

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onDeleteCardOKClick(cardNo,oneTapEnable);

                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
