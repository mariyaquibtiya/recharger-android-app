package com.getsmartapp.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.model.HomeCardsModel;

import java.util.List;

/**
 * @author jayant on 24/3/15.
 */

public abstract class EndlessRecyclerOnScrollListener extends
        RecyclerView.OnScrollListener {
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    private LinearLayoutManager mLinearLayoutManager;
    List<HomeCardsModel> cardsList;

    public EndlessRecyclerOnScrollListener(
            LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    public EndlessRecyclerOnScrollListener(
            LinearLayoutManager linearLayoutManager, List<HomeCardsModel> cardsList) {
        this.mLinearLayoutManager = linearLayoutManager;
        //TODO:UNCOMMENT_LATER
        //this.cardsList= HomeActivity.getInstance().cardsList;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        onScrolled(dy);
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
        if (loading) {
            if (totalItemCount >= previousTotal) {
                loading = false;
                previousTotal = totalItemCount - 1;
            }
        }
        if ((dy > 0) && (!loading) && (totalItemCount - visibleItemCount) == (firstVisibleItem +
                                                                                      visibleThreshold)) {
            onLoadMore();
            loading = true;
        }
        if(firstVisibleItem > getRechargeCardPosition()){
            onNextCardFocus();
        }
    }

    public abstract void onLoadMore();

    public abstract void onScrolled(int dy);

    public abstract void onNextCardFocus();

    public int getRechargeCardPosition(){
        if(cardsList!=null && cardsList.size()>0){
            for (int i=0;i<cardsList.size();i++){
                String cardName = cardsList.get(i).getCardName();
                if(cardName.equalsIgnoreCase(DataStorageConstants.CARD_RECHARGE)){
                    return i;
                }
            }
        }
        return -1;
    }
}