package com.getsmartapp.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.getsmartapp.R;
import com.getsmartapp.lib.SmartSDK;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.CallModel;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.reciever.PersistenetNotificationBroadcastReceiver;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.tagmanager.TagManager;

/**
 * @author Peeyush.Singh on 02-05-2016.
 */
public class PersistentNotification implements InternetDataUsageUtil.UpdateNotificationListener {

    private SharedPrefManager mSharedPrefManager;
    private static PersistentNotification sPersistentNotification;
    private static final String SCREEN_NAME = "Today's Widget";

    public static synchronized PersistentNotification getInstance(Context context) {
        if(sPersistentNotification ==null)
            sPersistentNotification = new PersistentNotification(context);
        return sPersistentNotification;
    }

    private PersistentNotification(Context context) {
        mSharedPrefManager = new SharedPrefManager(context);
        InternetDataUsageUtil.getInstance(context).setUpdateNotificationListener(this);
    }

    private Notification mNotification;
    private int notifyID = 27111987;

    @Override
    public void updateDataNotification(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            updateNotificationLollipop(context);
        else
            updateNotification(context);

        if((System.currentTimeMillis() - mSharedPrefManager.getLongValue(Constants.LAST_PERSISTENT_EVENT)) >= 24 * 60 * 60 * 1000L) {
            DataLayer mDataLayer = TagManager.getInstance(context).getDataLayer();
            mDataLayer.push(DataLayer.mapOf("event", "openScreen", "screenName", SCREEN_NAME));
            mSharedPrefManager.setLongValue(Constants.LAST_PERSISTENT_EVENT, System.currentTimeMillis());
        }
    }

    void updateNotificationLollipop(Context context)
    {

        NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);
        if(!TextUtils.isEmpty(mSharedPrefManager.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER)) && mSharedPrefManager.getBooleanValue(Constants.SHARED_PREFERENCE_SHOW_NOTIFICATION, true)) {

            RemoteViews  contentView = new RemoteViews(context.getPackageName(), R.layout.notification_layout);

            mNotification =
                    new NotificationCompat.Builder(context)
                            .setPriority(NotificationCompat.PRIORITY_MIN).setSmallIcon(android.R.color.transparent).setAutoCancel(false).build();
            mNotification.flags = Notification.FLAG_NO_CLEAR;
            SdkDbHelper dbHelper = SdkDbHelper.getInstance(context);//new DBHelper(this);
            CallModel outgoingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(dbHelper.getReadableDatabase(), 0, null, null, null, null, Constants.Service.OUTGOING, Constants.TODAY_START, 0, DataStorageConstants.SHOW_CATEGORY_TYPE.TODAY_TYPE);
            int total_outgoing_min = outgoingModal.getInternational_call() + outgoingModal.getLocal_call() + outgoingModal.getNational_call();

            String callText = total_outgoing_min + " MIN";

            String preferredType = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE, "3G");
            if(preferredType.equalsIgnoreCase("4G/LTE"))
                preferredType = "4G";


            String mobileDataType = preferredType + " Data";

            contentView.setTextViewText(R.id.callDataText, callText);

            long data = InternetDataUsageUtil.getInstance(context).getTodaysMobileDataUsed(context);

            contentView.setTextViewText(R.id.internetDataText, convertDataUnits(data / 1024.0f));
            contentView.setTextViewText(R.id.internetDataType, mobileDataType);

            mNotification.contentView = contentView;

            Intent broadCastIntent = new Intent("data.notification");
            broadCastIntent.setClass(context, PersistenetNotificationBroadcastReceiver.class);

            mNotification.contentIntent = PendingIntent.getBroadcast(context, 11111, broadCastIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            mNotificationManager.notify(notifyID, mNotification);

        }else
        {
            if(mNotification != null)
                mNotificationManager.cancel(notifyID);
        }
    }

    void updateNotification(Context context)
    {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if(!TextUtils.isEmpty(mSharedPrefManager.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER)) && mSharedPrefManager.getBooleanValue(Constants.SHARED_PREFERENCE_SHOW_NOTIFICATION, true)) {


            RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_layout_pre_lollipop);

            mNotification =  new NotificationCompat.Builder(context).setPriority(NotificationCompat.PRIORITY_MIN).setSmallIcon(android.R.color.transparent).build();
            mNotification.flags = Notification.FLAG_NO_CLEAR;
            SdkDbHelper dbHelper = SdkDbHelper.getInstance(context);//new DBHelper(this);
            CallModel outgoingModal = SmartSDK.getInstance(context).getCallDataForUsagesScreen(dbHelper.getReadableDatabase(), 0, null, null, null, null, Constants.Service.OUTGOING, Constants.TODAY_START, 0, DataStorageConstants.SHOW_CATEGORY_TYPE.TODAY_TYPE);
            int total_outgoing_min = outgoingModal.getInternational_call() + outgoingModal.getLocal_call() + outgoingModal.getNational_call();

            String callText = total_outgoing_min +" MIN";

            String preferredType = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE, "3G");
            if(preferredType.equalsIgnoreCase("4G/LTE"))
                preferredType = "4G";

            String mobileDataType = preferredType + " Data";

            contentView.setTextViewText(R.id.callDataText, callText);

            long data = InternetDataUsageUtil.getInstance(context).getTodaysMobileDataUsed(context);

            contentView.setTextViewText(R.id.internetDataText, convertDataUnits(data / 1024.0f));
            contentView.setTextViewText(R.id.internetDataType, mobileDataType);

            mNotification.contentView = contentView;

            Intent broadCastIntent = new Intent("data.notification");
            broadCastIntent.setClass(context, PersistenetNotificationBroadcastReceiver.class);

            mNotification.contentIntent = PendingIntent.getBroadcast(context, 11111, broadCastIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            mNotificationManager.notify(notifyID, mNotification);
        } else {
            if(mNotification != null)
                mNotificationManager.cancel(notifyID);
        }
    }

    public static String convertDataUnits(float data_in_KB) {

        String formattedNumber = "";
        String units = "";
        if (data_in_KB <= 900f) {
            formattedNumber = String.valueOf(Math.round(data_in_KB));
            units = "KB";
        } else if (900f < data_in_KB && (data_in_KB / 1024) <= 9.9f) {
            formattedNumber = String.format("%.1f", (data_in_KB / 1024));
            units = "MB";
        } else if ((data_in_KB / 1024) > 9.9f && (data_in_KB / 1024) <= 999) {
            formattedNumber = String.valueOf(Math.round((data_in_KB / 1024)));
            units = "MB";
        } else if ((data_in_KB / 1024) > 1000f && (data_in_KB / (1024 * 1024)) <= 9.9) {
            formattedNumber = String.format("%.1f", (data_in_KB / (1024 * 1024)));
            units = "GB";
        } else if ((data_in_KB / (1024 * 1024)) > 9.9) {
            formattedNumber = String.valueOf(Math.round((data_in_KB / (1024 * 1024))));
            units = "GB";
        }
        return formattedNumber + " " + units;
    }

}
