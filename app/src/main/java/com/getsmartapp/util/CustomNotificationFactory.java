package com.getsmartapp.util;

import android.app.Notification;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.NotificationCompat.Style;

import com.getsmartapp.sharedPreference.PushNotificationPrefManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushMessage;
import com.urbanairship.push.notifications.NotificationFactory;
import com.urbanairship.util.NotificationIDGenerator;
import com.urbanairship.util.UAStringUtil;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @author bishwajeet.kumar on 28/10/15.
 */
public class CustomNotificationFactory extends NotificationFactory {

    private Context mContext;
    private int titleId;
    private int smallIconId;
    private int largeIcon;
    private Uri sound = null;
    private int constantNotificationId = -1;
    private int accentColor = 0;
    String body;
    private PushNotificationPrefManager pushNotificationPrefManager;

    public CustomNotificationFactory(Context context) {
        super(context);
        mContext = context;
        this.titleId = context.getApplicationInfo().labelRes;
        this.smallIconId = context.getApplicationInfo().icon;
    }

    public Notification createNotification(@NonNull PushMessage message, int notificationId) {
        if(UAStringUtil.isEmpty(message.getAlert())) {
            return null;
        } else {
            BigTextStyle defaultStyle;
            String pushType = UAStringUtil.isEmpty(getMessageData(message.getAlert(), "PUSHTYPE"))?"":getMessageData(message.getAlert(), "PUSHTYPE");
            if(pushType.equalsIgnoreCase("silentAlert")) {
                defaultStyle = (new BigTextStyle()).bigText(body);
            } else {
                defaultStyle = (new BigTextStyle()).bigText(getMessageData(message.getAlert(), "BODY"));
            }
            try {
                return this.createNotificationBuilder(message, notificationId, defaultStyle).build();
            } catch(NullPointerException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public int getNextId(@NonNull PushMessage pushMessage) {
        return this.constantNotificationId > 0?this.constantNotificationId: NotificationIDGenerator.nextID();
    }

    protected Builder createNotificationBuilder(@NonNull PushMessage message, int notificationId, @Nullable Style defaultStyle) {
        pushNotificationPrefManager = PushNotificationPrefManager.getInstance(getContext());
        Set<String> tags = UAirship.shared().getPushManager().getTags();
        String title;
        String orderId = "";
        String coupon = "";
        String pushtype;

        try {
            pushtype = UAStringUtil.isEmpty(getMessageData(message.getAlert(), "PUSHTYPE"))?"":getMessageData(message.getAlert(), "PUSHTYPE");

            if(pushtype.equalsIgnoreCase("silentAlert")) {
                AppUtils.triggerSMSWorkflow(mContext);
                Map<String, String> pushData = AppUtils.triggerUsagePushWorkflow(mContext, message);

                if((pushData.containsKey("return") && pushData.get("return").equals("true")) || pushData.isEmpty()) {
                    return null;
                } else {
                    title = pushData.get("title");
                    body = pushData.get("body");
                    pushtype = "pushPromPref";
                    if (defaultStyle != null) {
                        defaultStyle = (new BigTextStyle()).bigText(body);
                    }
                }
            } else {
                title = UAStringUtil.isEmpty(getMessageData(message.getAlert(), "TITLE"))?this.getDefaultTitle():getMessageData(message.getAlert(), "TITLE");
                body = getMessageData(message.getAlert(), "BODY");
                orderId = UAStringUtil.isEmpty(getMessageData(message.getAlert(), "ORDERID"))?"":getMessageData(message.getAlert(), "ORDERID");
                coupon = UAStringUtil.isEmpty(getMessageData(message.getAlert(), "COUPON"))?"":getMessageData(message.getAlert(), "COUPON");
            }

            if(tags.contains(pushtype)) {

                if (!orderId.isEmpty()) {
                    pushNotificationPrefManager.setOrderIDfromPush(Long.parseLong(orderId));
                }

                if (!coupon.isEmpty()) {
                    AppUtils.copyToClipboard(mContext, "COUPONCODE", coupon);
                    pushNotificationPrefManager.setSnackBarCoupon(coupon);
                    pushNotificationPrefManager.setShowHomeSnackBar(Boolean.TRUE);
                }

                Builder builder = (new Builder(this.getContext()))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSmallIcon(smallIconId)
                        .setColor(this.accentColor)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setLocalOnly(message.isLocalOnly())
                        .setPriority(message.getPriority())
                        .setCategory(message.getCategory())
                        .setVisibility(message.getVisibility());

                Notification notification = this.createPublicVersionNotification(message, this.smallIconId);

                if (notification != null) {
                    builder.setPublicVersion(notification);
                }

                int defaults = 3;
                if (this.sound != null) {
                    builder.setSound(this.sound);
                    defaults &= -2;
                }

                builder.setDefaults(defaults);
                if (this.largeIcon > 0) {
                    builder.setLargeIcon(BitmapFactory.decodeResource(this.getContext().getResources(), this.largeIcon));
                }

                if (message.getSummary() != null) {
                    builder.setSubText(message.getSummary());
                }

                Style style = null;

                try {
                    style = this.createNotificationStyle(message);
                } catch (IOException var11) {
                    Logger.error("Failed to create notification style.", var11);
                }

                if (style != null) {
                    builder.setStyle(style);
                } else if (defaultStyle != null) {
                    builder.setStyle(defaultStyle);
                }

                if (!message.isLocalOnly()) {
                    try {
                        builder.extend(this.createWearableExtender(message, notificationId));
                    } catch (IOException var10) {
                        Logger.error("Failed to create wearable extender.", var10);
                    }
                }

                builder.extend(this.createNotificationActionsExtender(message, notificationId));
                return builder;
            } else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setConstantNotificationId(int id) {
        this.constantNotificationId = id;
    }

    public int getConstantNotificationId() {
        return this.constantNotificationId;
    }

    public void setTitleId(@StringRes int titleId) {
        this.titleId = titleId;
    }

    @StringRes
    public int getTitleId() {
        return this.titleId;
    }

    public void setSmallIconId(@DrawableRes int smallIconId) {
        this.smallIconId = smallIconId;
    }

    @DrawableRes
    public int getSmallIconId() {
        return this.smallIconId;
    }

    public void setSound(Uri sound) {
        this.sound = sound;
    }

    public Uri getSound() {
        return this.sound;
    }

    public void setLargeIcon(@DrawableRes int largeIcon) {
        this.largeIcon = largeIcon;
    }

    @DrawableRes
    public int getLargeIcon() {
        return this.largeIcon;
    }

    public void setColor(@ColorInt int accentColor) {
        this.accentColor = accentColor;
    }

    @ColorInt
    public int getColor() {
        return this.accentColor;
    }

    protected String getDefaultTitle() {
        return this.getTitleId() == 0?this.getContext().getPackageManager().getApplicationLabel(this.getContext().getApplicationInfo()).toString():(this.getTitleId() > 0?this.getContext().getString(this.getTitleId()):"");
    }

    public static String getMessageData(String message, String type) {
        Gson gson = new Gson();
        JsonObject data = new JsonParser().parse(message).getAsJsonObject();

        switch (type) {
            case "TITLE":
                Lg.e("TITLE", data.get("title").getAsString());
                return data.get("title").getAsString();
            case "BODY":
                Lg.e("BODY", data.get("body").getAsString());
                return data.get("body").getAsString();
            case "ORDERID":
                if(data.get("orderid") != null && !data.get("orderid").isJsonNull()) {
                    Lg.e("ORDERID", data.get("orderid").getAsString());
                    return data.get("orderid").getAsString();
                }
                return "";
            case "COUPON":
                if(data.get("gccode") != null && !data.get("gccode").isJsonNull()) {
                    Lg.e("COUPON", data.get("gccode").getAsString());
                    return data.get("gccode").getAsString();
                }
                return "";
            case "PUSHTYPE":
                if(data.get("pushtype") != null && !data.get("pushtype").isJsonNull()) {
                    Lg.e("PUSHTYPE", data.get("pushtype").getAsString());
                    return data.get("pushtype").getAsString();
                }
            default:
                return "";
        }
    }
}
