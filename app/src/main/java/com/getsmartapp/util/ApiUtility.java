package com.getsmartapp.util;

import android.app.Activity;
import android.content.Context;
import android.net.TrafficStats;
import android.os.SystemClock;
import android.support.v4.app.Fragment;

import com.getsmartapp.activity.HomeActivity;
import com.getsmartapp.lib.dataAggregation.DataAggregationUtils;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.ComboPlan;
import com.getsmartapp.lib.model.HomeAllInstreamModel;
import com.getsmartapp.lib.model.InstreamPlanModel;
import com.getsmartapp.lib.model.PlanDetailModel;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.DateUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Jayant.B on 7/24/2015.
 */
public class ApiUtility {

    private ApiCallback mApiCallback;

    public interface ApiCallback {
        void OnSuccess(Object planDetailModel, Response
                response);

        void OnFailure(RetrofitError error);
    }

    public void getPlanApiDetailsWebService(Activity activity, String dataCategoryId,
                                            String dataPreference) {

        try {
            mApiCallback = (ApiCallback) activity;
            RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
            Map<String, String> params = new HashMap<>();
            final SharedPrefManager mSharedPrefManager = new SharedPrefManager(activity);
            int num_of_days = mSharedPrefManager.getIntValue(Constants
                    .NUMBER_OF_DAYS);
            params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants.ON_BOARDING_CIRCLE_ID) + "");
            params.put("categoryIds", dataCategoryId + ",9");//+sprecial category
            params.put(ApiConstants.SPID, mSharedPrefManager.getIntValue(Constants
                    .ON_BOARDING_PROVIDER_ID) + "");
            params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
            params.put(ApiConstants.NO_OF_DAYS, ((num_of_days == 0) ? "28" : (num_of_days + "")));
            params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
            params.put(ApiConstants.DATAPREFERENCES, dataPreference);
            restClient.getApiService().getRecommendedPlanAPIDetails(params,
                    new
                            Callback<HomeAllInstreamModel>
                                    () {
                                @Override
                                public void success(HomeAllInstreamModel planDetailModel, Response
                                        response) {
                                    Lg.w("Response: ", planDetailModel.toString());
                                    mSharedPrefManager.setStringValue(Constants.GET_PLAN_API_DETAILS, new Gson().toJson(planDetailModel));
                                    mSharedPrefManager.setLongValue(Constants.LAST_RECO_CALL_TIMESTAMP, System.currentTimeMillis());
                                    mApiCallback.OnSuccess(planDetailModel, response);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    if (error != null) {
                                        if (error.getCause() != null) {
                                            Lg.w("RetrofitError: ", error.getCause().toString());
                                        }
                                    }
                                    mApiCallback.OnFailure(error);
                                }
                            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPlanApiDetailsSharedPrefs(Activity activity) {
        try {
            mApiCallback = (ApiCallback) activity;
            final SharedPrefManager mSharedPrefManager = new SharedPrefManager(activity);
            HomeAllInstreamModel planDetailModel = new Gson().fromJson(mSharedPrefManager.getStringValue(Constants.GET_PLAN_API_DETAILS), HomeAllInstreamModel.class);
            mApiCallback.OnSuccess(planDetailModel, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPlanApiDetailsWebService(Fragment context, int start_index, int category_id,
                                            String excludeIds, int recommended_id, String mobile,
                                            String provider_id, String circle_id) {
        try {
            mApiCallback = (ApiCallback) context;
            if (AppUtils.isConnectingToInternet(context.getActivity())) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECHARGER_ADDRESS, null);
                Map<String, String> params = new HashMap<String, String>();
                SharedPrefManager mSharedPrefManager = new SharedPrefManager(context.getActivity());
                String userJsonData = mSharedPrefManager.getStringValue(Constants.USER_DATA);
                if ((userJsonData != null) && (!userJsonData.equals(""))) {
                    ProxyLoginUser.SoResponseEntity mUserLoginSSO = new Gson().fromJson(userJsonData, ProxyLoginUser.SoResponseEntity.class);
                    params.put(ApiConstants.SSOID, mUserLoginSSO.getUserId());
                } else
                    params.put(ApiConstants.SSOID, "");
                params.put(ApiConstants.TYPE, ApiConstants.MOBILE);
                params.put(ApiConstants.CIRCLEID, circle_id);
                params.put(ApiConstants.CATEGORYID, category_id + "");
                params.put(ApiConstants.OPERATORID, provider_id);
                params.put(ApiConstants.STARTINDEX, start_index + "");
                params.put(ApiConstants.LIMIT, ApiConstants.LIMIT_VALUE + "");
                params.put(ApiConstants.RECOMMEND, recommended_id + "");
                params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
                params.put(ApiConstants.NO_OF_DAYS, ApiConstants.NO_OF_DAYS_VALUE + "");
                params.put(ApiConstants.JSON_DEVICEID, AppUtils.getHashedDeviceID(AppUtils.getDeviceIDForSSO(context.getActivity()) +
                        mobile));
                params.put(ApiConstants.EXCLUDEID, excludeIds);
                params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue
                        (Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
                restClient.getApiService().getPlanAPIDetails(params,
                        new Callback<PlanDetailModel>
                                () {
                            @Override
                            public void success(PlanDetailModel planDetailModel, Response
                                    response) {
                                Lg.w("Response: ", planDetailModel.toString());
                                mApiCallback.OnSuccess(planDetailModel, response);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                if (error != null && error.getCause() != null) {
                                    Lg.w("RetrofitError: ", error.getCause().toString());
                                    mApiCallback.OnFailure(error);
                                }
                            }
                        });
            } else {
                CustomDialogUtil.showInternetLostDialog(context.getActivity(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getComboApiWebService(Context context, int category_id, int spid, int no_of_days) {
        try {
            mApiCallback = (ApiCallback) context;
            if (AppUtils.isConnectingToInternet(context)) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
                Map<String, String> params = new HashMap<String, String>();
                SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
                params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
                params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                        .ON_BOARDING_CIRCLE_ID) + "");
                params.put(ApiConstants.CATEGORYID, category_id + "");
                params.put(ApiConstants.SPID, spid + "");
                params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
                params.put(ApiConstants.NO_OF_DAYS, no_of_days + "");
                params.put(ApiConstants.TOPUPBALANCE, "0");
                params.put(ApiConstants.DATABALANCE, "0");
                params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
                restClient.getApiService().getComboAPIDetails(params,
                        new
                                Callback<ComboPlan>
                                        () {
                                    @Override
                                    public void success(ComboPlan comboPlan, Response
                                            response) {
                                        Lg.w("Response: ", comboPlan.toString());
                                        mApiCallback.OnSuccess(comboPlan, response);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Lg.w("RetrofitError: ", error.getCause().toString());
                                        mApiCallback.OnFailure(error);
                                    }
                                });
            } else {
                CustomDialogUtil.showInternetLostDialog(context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCustomComboApiWebService(Context context, int category_id, int spid, HashMap<String, Object> targetUsageMap) {
        try {
            mApiCallback = (ApiCallback) context;
            if (AppUtils.isConnectingToInternet(context)) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
                Map<String, String> params = new HashMap<String, String>();
                SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
                params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
                params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                        .ON_BOARDING_CIRCLE_ID) + "");
                params.put(ApiConstants.CATEGORYID, category_id + "");
                params.put(ApiConstants.SPID, spid + "");
                params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
                params.put(ApiConstants.TOPUPBALANCE, "0");
                params.put(ApiConstants.DATABALANCE, "0");
                params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
                if(targetUsageMap.containsKey(ApiConstants.NO_OF_DAYS)) {
                    params.put(ApiConstants.NO_OF_DAYS, String.valueOf(targetUsageMap.get(ApiConstants.NO_OF_DAYS)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_LOCAL_USAGE)) {
                    params.put(ApiConstants.TARGET_LOCAL_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_LOCAL_USAGE)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_STD_USAGE)) {
                    params.put(ApiConstants.TARGET_STD_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_STD_USAGE)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_DATA_USAGE)) {
                    params.put(ApiConstants.TARGET_DATA_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_DATA_USAGE)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_SMS_USAGE)) {
                    params.put(ApiConstants.TARGET_SMS_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_SMS_USAGE)));
                }
                restClient.getApiService().getComboAPIDetails(params,
                        new
                                Callback<ComboPlan>
                                        () {
                                    @Override
                                    public void success(ComboPlan comboPlan, Response
                                            response) {
                                        Lg.w("Response: ", String.valueOf(comboPlan));
                                        mApiCallback.OnSuccess(comboPlan, response);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Lg.w("RetrofitError: ", String.valueOf(error.getCause()));
                                        mApiCallback.OnFailure(error);
                                    }
                                });
            } else {
                CustomDialogUtil.showInternetLostDialog(context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postComboApiWebService(Context context, int category_id, int spid, int no_of_days) {
        try {
            mApiCallback = (ApiCallback) context;
            DataAggregationUtils dgu = new DataAggregationUtils(context,0);
            if (AppUtils.isConnectingToInternet(context)) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
                Map<String, String> params = new HashMap<>();
                SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
                String pref_network = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
                params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
                params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                        .ON_BOARDING_CIRCLE_ID) + "");
                params.put(ApiConstants.CATEGORYID, category_id + "");
                params.put(ApiConstants.SPID, spid + "");
                params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
                params.put(ApiConstants.NO_OF_DAYS, no_of_days + "");
                params.put(ApiConstants.TOPUPBALANCE, "0");
                params.put(ApiConstants.DATABALANCE, "0");
                params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
                if (SystemClock.elapsedRealtime() > 259200000 && TrafficStats.getMobileRxBytes() > 1000) {
                    if (mSharedPrefManager.getStringValue(Constants.DATA_EXISTS_FLAG).equalsIgnoreCase(ApiConstants.DATA_NOT_AVAILABLE)) {
                        params.put(ApiConstants.NETWORKUSAGES, dgu.formNetworkData(pref_network));
                    }
                } else {
                    if (HomeActivity.getInstance() != null) {
                        HomeActivity.getInstance().setEstimatingData(true);
                    }
                }
                if (!DateUtil.nDaysGoneSinceOnBoard(context, 1)) {
                    params.put(ApiConstants.CALLINGUSAGES, dgu.formCallSmsData(0,0));
                }
                restClient.getApiService().postComboAPIDetails(params,
                        new
                                Callback<ComboPlan>
                                        () {
                                    @Override
                                    public void success(ComboPlan comboPlan, Response
                                            response) {
                                        Lg.w("Response: ", comboPlan.toString());
                                        mApiCallback.OnSuccess(comboPlan, response);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Lg.w("RetrofitError: ", error.getCause().toString());
                                        mApiCallback.OnFailure(error);
                                    }
                                });
            } else {
                CustomDialogUtil.showInternetLostDialog(context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postCustomComboApiWebService(Context context, int category_id, int spid, HashMap<String, Object> targetUsageMap) {
        try {
            mApiCallback = (ApiCallback) context;
            DataAggregationUtils dgu = new DataAggregationUtils(context,0);
            if (AppUtils.isConnectingToInternet(context)) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
                Map<String, String> params = new HashMap<>();
                SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
                String pref_network = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
                params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
                params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                        .ON_BOARDING_CIRCLE_ID) + "");
                params.put(ApiConstants.CATEGORYID, category_id + "");
                params.put(ApiConstants.SPID, spid + "");
                params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
                params.put(ApiConstants.TOPUPBALANCE, "0");
                params.put(ApiConstants.DATABALANCE, "0");
                params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
                if (SystemClock.elapsedRealtime() > 259200000 && TrafficStats.getMobileRxBytes() > 1000) {
                    if (mSharedPrefManager.getStringValue(Constants.DATA_EXISTS_FLAG).equalsIgnoreCase(ApiConstants.DATA_NOT_AVAILABLE)) {
                        params.put(ApiConstants.NETWORKUSAGES, dgu.formNetworkData(pref_network));
                    }
                } else {
                    if (HomeActivity.getInstance() != null) {
                        HomeActivity.getInstance().setEstimatingData(true);
                    }
                }
                if (!DateUtil.nDaysGoneSinceOnBoard(context, 1)) {
                    params.put(ApiConstants.CALLINGUSAGES, dgu.formCallSmsData(0,0));
                }
                if(targetUsageMap.containsKey(ApiConstants.NO_OF_DAYS)) {
                    params.put(ApiConstants.NO_OF_DAYS, String.valueOf(targetUsageMap.get(ApiConstants.NO_OF_DAYS)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_LOCAL_USAGE)) {
                    params.put(ApiConstants.TARGET_LOCAL_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_LOCAL_USAGE)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_STD_USAGE)) {
                    params.put(ApiConstants.TARGET_STD_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_STD_USAGE)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_DATA_USAGE)) {
                    params.put(ApiConstants.TARGET_DATA_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_DATA_USAGE)));
                }
                if(targetUsageMap.containsKey(ApiConstants.TARGET_SMS_USAGE)) {
                    params.put(ApiConstants.TARGET_SMS_USAGE, String.valueOf(targetUsageMap.get(ApiConstants.TARGET_SMS_USAGE)));
                }
                restClient.getApiService().postComboAPIDetails(params,
                        new
                                Callback<ComboPlan>
                                        () {
                                    @Override
                                    public void success(ComboPlan comboPlan, Response
                                            response) {
                                        Lg.w("Response: ", comboPlan.toString());
                                        mApiCallback.OnSuccess(comboPlan, response);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Lg.w("RetrofitError: ", error.getCause().toString());
                                        mApiCallback.OnFailure(error);
                                    }
                                });
            } else {
                CustomDialogUtil.showInternetLostDialog(context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getInstreamRecommendation(Context context, String category_id, int no_of_days) {
        try {
            RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
            mApiCallback = (ApiCallback) context;
            HashMap<String, String> params = new HashMap<>();
            SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
            params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID)/*"abb619e0379d55003d6ea039f9cd2b675077e03dbe8af33df9595925911f673f"*/);
            params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                    .ON_BOARDING_CIRCLE_ID) + "");
            params.put(ApiConstants.SPID, mSharedPrefManager.getIntValue(Constants.ON_BOARDING_PROVIDER_ID) + "");
            params.put(ApiConstants.CATEGORY_ID, category_id + "");
            params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
            params.put(ApiConstants.NO_OF_DAYS, no_of_days + "");
            params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue
                    (Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
            restClient.getApiService().getInstreamPlansDetails(params, new Callback<InstreamPlanModel>() {
                @Override
                public void success(InstreamPlanModel instreamPlanModel, Response response) {
                    mApiCallback.OnSuccess(instreamPlanModel, response);
                }

                @Override
                public void failure(RetrofitError error) {
                    mApiCallback.OnFailure(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postInstreamRecommendation(Context context, String category_id, int no_of_days) {
        try {
            mApiCallback = (ApiCallback) context;
            DataAggregationUtils dgu = new DataAggregationUtils(context,0);
            if (AppUtils.isConnectingToInternet(context)) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
                Map<String, String> params = new HashMap<>();
                SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
                String pref_network = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
                int spid = mSharedPrefManager.getIntValue(Constants.ON_BOARDING_PROVIDER_ID);
                params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
                params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                        .ON_BOARDING_CIRCLE_ID) + "");
                params.put(ApiConstants.CATEGORY_ID, category_id + "");
                params.put(ApiConstants.SPID, spid + "");
                params.put(ApiConstants.RECCOUNT, ApiConstants.RECCOUNT_VALUE + "");
                params.put(ApiConstants.NO_OF_DAYS, no_of_days + "");
                params.put(ApiConstants.DATAPREFERENCES, mSharedPrefManager.getStringValue
                        (Constants.ON_BOARDING_PREFERRED_TYPE).toLowerCase());
                if (SystemClock.elapsedRealtime() > 259200000 && TrafficStats.getMobileRxBytes() > 1000) {
                    if (mSharedPrefManager.getStringValue(Constants.DATA_EXISTS_FLAG).equalsIgnoreCase(ApiConstants.DATA_NOT_AVAILABLE)) {
                        params.put(ApiConstants.NETWORKUSAGES, dgu.formNetworkData(pref_network));
                    }
                }
                if (!DateUtil.nDaysGoneSinceOnBoard(context, 1)) {
                    params.put(ApiConstants.CALLINGUSAGES, dgu.formCallSmsData(0,0));
                }
                restClient.getApiService().postInstreamPlansDetails(params,
                        new
                                Callback<InstreamPlanModel>
                                        () {
                                    @Override
                                    public void success(InstreamPlanModel comboPlan, Response
                                            response) {
                                        Lg.w("Response: ", comboPlan.toString());
                                        mApiCallback.OnSuccess(comboPlan, response);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        mApiCallback.OnFailure(error);
                                    }
                                });
            } else {
                CustomDialogUtil.showInternetLostDialog(context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getSTDPlanDetails(Context context, int no_of_days) {
        try {
            RestClient restClient = new RestClient(ApiConstants.BASE_RECOMMENDATION_ADDRESS, null);
            mApiCallback = (ApiCallback) context;
            HashMap<String, String> params = new HashMap<>();
            SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);
            params.put(ApiConstants.JSON_DEVICEID, mSharedPrefManager.getStringValue(Constants.DEVICE_ID));
            params.put(ApiConstants.CIRCLEIDCOMBO, mSharedPrefManager.getIntValue(Constants
                    .ON_BOARDING_CIRCLE_ID) + "");
            params.put(ApiConstants.SPID, mSharedPrefManager.getIntValue(Constants.ON_BOARDING_PROVIDER_ID) + "");
            params.put(ApiConstants.FILTER, ApiConstants.CALL_STD);
            params.put(ApiConstants.NO_OF_DAYS, no_of_days + "");
            params.put(ApiConstants.RECOMMEND,ApiConstants.STD_PLAN_RECOMMEND_VALUE);
            restClient.getApiService().getSTDPlansDetails(params, new Callback<InstreamPlanModel>() {
                @Override
                public void success(InstreamPlanModel instreamPlanModel, Response response) {
                    mApiCallback.OnSuccess(instreamPlanModel, response);
                }

                @Override
                public void failure(RetrofitError error) {
                    mApiCallback.OnFailure(error);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}