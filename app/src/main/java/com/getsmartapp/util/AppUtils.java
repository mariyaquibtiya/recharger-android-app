package com.getsmartapp.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.getsmartapp.R;
import com.getsmartapp.data.DBHelper;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkAppDatabaseHelper;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.CircleModel;
import com.getsmartapp.lib.model.DataModel;
import com.getsmartapp.lib.model.GoogleImgModal;
import com.getsmartapp.lib.model.HomeAllInstreamModel;
import com.getsmartapp.lib.model.HomeCardsModel;
import com.getsmartapp.lib.model.MobileDataVal;
import com.getsmartapp.lib.model.Operator;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.lib.model.USSDDetailModel;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.services.BulkAggregateDataService;
import com.getsmartapp.lib.services.DailyAggregationService;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.sharedPreference.BranchPrefManager;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.tagmanager.TagManager;
import com.google.gson.Gson;
import com.urbanairship.push.PushMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.branch.referral.Branch;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
//TODO:UNCOMMENT_LATER
//import com.freshdesk.mobihelp.Mobihelp;


/**
 * @author Shalakha.Gupta on 15-05-2015.
 */
public class AppUtils {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String getFormattedMobileNumber(@Nullable String mobile) {
        if (mobile != null && mobile.length() == 10) {
            return Constants.INDIA_CODE.concat(mobile);
        }
        return mobile;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // Checking for all possible internet providers
    public static boolean isConnectingToInternet(Context ctx) {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Boolean RegExChecker(String pattern, String matcher) {
        Pattern intsOnly = Pattern.compile(pattern);
        Matcher makeMatch = intsOnly.matcher(matcher);
        return makeMatch.matches();
    }

    public static void hide_keyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if(inputMethodManager!=null && inputMethodManager.isActive())
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void showkeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY); // show
    }


    public static void checkandSendDataToServer(Context context) {

        DBHelper dbHelper = DBHelper.getInstance(context);//new DBHelper(context);
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        SharedPrefManager shrd = new SharedPrefManager(context);
        Intent serviceIntent = null;
        long lastcalltime = AppUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.CALLSMS_DATA_TYPE);
        long lastdatatime = AppUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.INTERNET_DATA_TYPE);
        if (shrd.getIntValue(Constants.BULK_DATA_SENT_ONCE) == 1) {
            if (lastcalltime != 0 && lastdatatime != 0) {
                if (DateUtil.isToday(new Date(lastcalltime)) && DateUtil.isToday(new Date(lastdatatime))) {
                    if (DateUtil.sixHourBefore(lastcalltime) && DateUtil.sixHourBefore(lastdatatime)) {
                        serviceIntent = new Intent(context, DailyAggregationService.class);
                        context.startService(serviceIntent);
                    }
                } else {
                    if (DateUtil.sixHourBefore(lastcalltime)) {
                        serviceIntent = new Intent(context, BulkAggregateDataService.class);
                        context.startService(serviceIntent);
                    }
                }
            } else {
                serviceIntent = new Intent(context, BulkAggregateDataService.class);
                context.startService(serviceIntent);
            }
        } else {
            serviceIntent = new Intent(context, BulkAggregateDataService.class);
            context.startService(serviceIntent);
        }
    }

    public static String getDeviceIDForSSO(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * Method for opening the soft keyboard
     *
     * @param mContext, editText
     */
    public static void openSoftKeyboard(final Context mContext, final EditText editText) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(editText.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }
        }, 500);
    }

    public static AnimationSet FadeInFadeOutAnimation() {
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(500);
        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(500);
        AnimationSet as = new AnimationSet(true);
        as.addAnimation(out);
        in.setStartOffset(500);
        as.addAnimation(in);
        return as;
    }

    public static String getLoggedInUser(Context context) {
        SharedPrefManager mSharedPref = new SharedPrefManager(context);
        String value = mSharedPref.getStringValue(Constants.USER_DATA);
        if (value != null && !value.equals("")) {
            return value;
        } else {
            return null;
        }
    }

    public static ProxyLoginUser.SoResponseEntity getLoggedInSSODetails(Context context) {
        String user_data = getLoggedInUser(context);
        if (user_data != null) {
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(user_data, ProxyLoginUser.SoResponseEntity.class);
            if (user != null) {
                return user;
            }
        }
        return null;
    }


    public static String getSSOUserId(Context context) {
        String user_data = getLoggedInUser(context);
        if (user_data != null) {
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(user_data, ProxyLoginUser.SoResponseEntity.class);
            if (user != null) {
                return user.getUserId();
            }
        }
        return null;
    }

    public static void finishActivity(Activity context) {
        if (context != null) {
            context.finish();
            context.overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
        }
    }

    public static void startActivity(Activity context) {
        if (context != null)
            context.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
    }


    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void cartHideAnimation(View cartLayout) {
        TranslateAnimation animate = new TranslateAnimation(
                0, 0, 0, cartLayout.getHeight());
        animate.setDuration(200);
        animate.setFillAfter(false);
        cartLayout.startAnimation(animate);
        cartLayout.setVisibility(View.GONE);
    }

    public static void cartShowAnimation(View cartLayout) {
        TranslateAnimation animate = new TranslateAnimation(
                0, 0, cartLayout.getHeight(), 0);
        animate.setDuration(200);
        animate.setFillAfter(false);
        cartLayout.startAnimation(animate);
        cartLayout.setVisibility(View.VISIBLE);
    }

    public static void rotate(float fromdegree, float todegree, ImageView icon, int duration) {
        final RotateAnimation rotateAnim = new RotateAnimation(fromdegree, todegree,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnim.setDuration(duration);
        rotateAnim.setFillAfter(true);
        icon.startAnimation(rotateAnim);
    }

    /**
     * Convert a dp float value to pixels
     *
     * @param dp float value in dps to convert
     * @return DP value converted to pixels
     */
    public static int dp2px(Context context, float dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return Math.round(px);
    }

    public static boolean isStringNullEmpty(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("null") || value.trim().length() == 0)
            return true;
        else
            return false;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static boolean copyToClipboard(Context context, String label, String text) {
        try {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText(label, text);
                clipboard.setPrimaryClip(clip);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void getAndStoreProfilePicUrl(Context context) {
        final SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        String token = sharedPrefManager.getStringValue(Constants.GP_ACCESS_TOKEN);
        RestClient restClientGPlus = new RestClient(ApiConstants.GP_PROFILE_BASE_URL, null);
        restClientGPlus.getApiService().getGplusPicUrl(token, new Callback<GoogleImgModal>() {
            @Override
            public void success(GoogleImgModal googleImgModal, Response response) {
                if (googleImgModal != null) {
                    String imgUrl = googleImgModal.getImage().getUrl();
                    sharedPrefManager.setStringValue(Constants.GP_PIC_URL, imgUrl);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    public static boolean isLoggedIn(Context context) {
        String value = getLoggedInUser(context);
        return value != null;
    }


    public static MobileDataVal giveMobileDataVal() {
        long mob_rxbyte = TrafficStats.getMobileRxBytes();
        long mob_txbyte = TrafficStats.getMobileTxBytes();
        long mob_total = mob_rxbyte + mob_txbyte;
        long total_rxbyte = TrafficStats.getTotalRxBytes();
        long total_txbyte = TrafficStats.getTotalTxBytes();
        long total_totalData = total_rxbyte + total_txbyte;
        long total_wifi = total_totalData - mob_total;
        return new MobileDataVal(mob_total, total_totalData, total_wifi);
    }

    public static DataModel extrapoleteForDays(String dataPref) {

        long time_since_boot = SystemClock.elapsedRealtime();
        MobileDataVal mobileDataVal = giveMobileDataVal();
        float data_in_kb = mobileDataVal.getMobile_data() / 1024f;
        float wifi_in_kb = mobileDataVal.getWifi_data() / 1024f;
        float data_mob = (ApiConstants.EXTRAPOLATION_DAYS * data_in_kb * 3600 * 24 * 1000) / time_since_boot;
        float data_wifi = (ApiConstants.EXTRAPOLATION_DAYS * wifi_in_kb * 3600 * 24 * 1000) / time_since_boot;
        switch (dataPref.toLowerCase()) {
            case "2g":
                return new DataModel(0, 0, data_mob, data_wifi, "", "kilobytes", "KB");
            case "3g":
                return new DataModel(0, data_mob, 0, data_wifi, "", "kilobytes", "KB");
            case "4g":
                return new DataModel(data_mob, 0, 0, data_wifi, "", "kilobytes", "KB");
        }
        return null;
    }

    
    //Calculating Data components -------------------------------------->
    public static DataModel getDataUsageDetails(int timeDuration, DataStorageConstants.SHOW_CATEGORY_TYPE timetype,
                                                Context context) {

        LinkedHashMap<String, DataModel> hashMap = new LinkedHashMap<>();
        SharedPrefManager mSharedPref = new SharedPrefManager(context);
        InternetDataUsageUtil internetDataUsageUtil = InternetDataUsageUtil.getInstance(context);
        float threeg_d = 0;//(threeg_data / (1024f * 1024f));
        float twog_d = 0;//(twog_data / (1024f * 1024f));
        float wifi_d = 0;//(wifi_data / (1024f * 1024f));
        float fourg_d = 0;//(fourg_data) / (1024f * 1024f);

        float totalData = 0;

        ArrayList<Long> dataValuesArrayList = new ArrayList<>();
        ArrayList<Long> dateMillisArrayList = new ArrayList<>();

        dataValuesArrayList.clear();
        dateMillisArrayList.clear();

        if(timetype == DataStorageConstants.SHOW_CATEGORY_TYPE.BILL_CYCLE_TYPE)
        {
            int initDay = Integer.parseInt(mSharedPref.getStringValue(Constants.ON_BOARDING_BILL_DATE));
            int currentDay = Calendar.getInstance().get(Calendar.DATE);
            int dayDiff = currentDay - initDay + 1;
            if(mSharedPref.getLongValue(Constants.ON_BOARDING_BILL_DAYS) > 0)
            {
                dayDiff = (int) mSharedPref.getLongValue(Constants.ON_BOARDING_BILL_DAYS);
            }
            if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                threeg_d = internetDataUsageUtil.getTotalMobileDataUsageForLastNumberOfDays(dayDiff)  / (1024f * 1024f);
            } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                twog_d = internetDataUsageUtil.getTotalMobileDataUsageForLastNumberOfDays(dayDiff) / (1024f * 1024f);
            } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                fourg_d = internetDataUsageUtil.getTotalMobileDataUsageForLastNumberOfDays(dayDiff) / (1024f * 1024f);
            } else
                threeg_d = internetDataUsageUtil.getTotalMobileDataUsageForLastNumberOfDays(dayDiff)  / (1024f * 1024f);

            totalData = internetDataUsageUtil.getTotalOverallDataUsageForLastNumberOfDays(dayDiff) / (1024f * 1024f);

            dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(dayDiff));

            Collections.sort(dateMillisArrayList, Collections.reverseOrder());

            for (int i = 0; i < dateMillisArrayList.size(); i++) {
                long data = dateMillisArrayList.get(i); 
                if (data < 0)
                    data = 0;

                try {
                    dataValuesArrayList.set((i / 7), dataValuesArrayList.get(i / 7) + data);
                } catch (Exception e) {
                    dataValuesArrayList.add((i / 7), data);
                }
            }
        } else if(timetype == DataStorageConstants.SHOW_CATEGORY_TYPE.TODAY_TYPE)
        {
            if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
            } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                twog_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
            } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                fourg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
            }else
                threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);

            totalData = internetDataUsageUtil.getTodaysTotalDataUsed() / (1024f * 1024f);

            dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(1));

            Collections.sort(dateMillisArrayList, Collections.reverseOrder());

            for (int i = 0; i < dateMillisArrayList.size(); i++) {
                long data = dateMillisArrayList.get(i); 
                if (data < 0)
                    data = 0;
                dataValuesArrayList.add(data);
            }
        }
        else {
            switch (timeDuration) {
                case 1:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getTodaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(1));

                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i); 
                        if (data < 0)
                            data = 0;
                        dataValuesArrayList.add(data);
                    }
                    break;
                case 7:

                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast7DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast7DaysMobileDataUsed() / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast7DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast7DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast7DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(7));
                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());
                    int loopLimit = dateMillisArrayList.size();

                    if (loopLimit > 7)
                        loopLimit = 7;

                    for (int i = 0; i < loopLimit; i++) {
                        long data = dateMillisArrayList.get(i); 
                        if (data < 0)
                            data = 0;
                        dataValuesArrayList.add(data);
                    }
                    break;
                case 14:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast14DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast14DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast14DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast14DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast14DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(14));

                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i); 
                        if (data < 0)
                            data = 0;
                        dataValuesArrayList.add(data);
                    }

                    break;
                case 30:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast28DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(28));

                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i); 
                        if (data < 0)
                            data = 0;

                        try {
                            dataValuesArrayList.set((i / 7), dataValuesArrayList.get(i / 7) + data);
                        } catch (Exception e) {
                            dataValuesArrayList.add((i / 7), data);
                        }
                    }

                    break;
                default:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast28DaysTotalDataUsed() / (1024f * 1024f);


                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateMapData(28));
                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i); 
                        if (data < 0)
                            data = 0;
                        try {
                            dataValuesArrayList.set((i / 7), dataValuesArrayList.get(i / 7) + data);
                        } catch (Exception e) {
                            dataValuesArrayList.add((i / 7), data);
                        }
                    }
                    break;
            }
        }
        try{
            for(int i=0; i<dataValuesArrayList.size();i++)
            {
                hashMap.put(i+"", getDataModelUpdate(hashMap, "", dataValuesArrayList.get(i),
                        i+"", context));
            }
        }
        catch (Exception e){
        }


        wifi_d = totalData - (twog_d+threeg_d+fourg_d);
        float total_data_in_mb = (threeg_d + twog_d + fourg_d /*/ (1024f * 1024f)*/);

        return new DataModel(fourg_d, threeg_d, twog_d, wifi_d, total_data_in_mb + ""/*, data_usage_units, data_usages_short_units*/, hashMap);
    }


    public static DataModel getWifiDataUsageDetails(int timeDuration, DataStorageConstants.SHOW_CATEGORY_TYPE timetype,
                                                    Context context) {

        LinkedHashMap<String, DataModel> hashMap = new LinkedHashMap<>();
        SharedPrefManager mSharedPref = new SharedPrefManager(context);
        InternetDataUsageUtil internetDataUsageUtil = InternetDataUsageUtil.getInstance(context);
        float threeg_d = 0;
        float twog_d = 0;
        float wifi_d = 0;
        float fourg_d = 0;

        float totalData = 0;

        ArrayList<Long> dataValuesArrayList = new ArrayList<>();
        ArrayList<Long> dateMillisArrayList = new ArrayList<>();

        dataValuesArrayList.clear();
        dateMillisArrayList.clear();

      if(timetype == DataStorageConstants.SHOW_CATEGORY_TYPE.TODAY_TYPE)
        {
            if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
            } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                twog_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
            } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                fourg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
            }else
                threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);

            totalData = internetDataUsageUtil.getTodaysTotalDataUsed() / (1024f * 1024f);

            dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateWifiMapData(1));

            Collections.sort(dateMillisArrayList, Collections.reverseOrder());

            for (int i = 0; i < dateMillisArrayList.size(); i++) {
                long data = dateMillisArrayList.get(i); 
                if (data < 0)
                    data = 0;
                dataValuesArrayList.add(data);
            }
        }
        else {
            switch (timeDuration) {
                case 1:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getTodaysMobileDataUsed(context) / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getTodaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateWifiMapData(1));

                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i); 
                        if (data < 0)
                            data = 0;
                        dataValuesArrayList.add(data);
                    }
                    break;
                case 7:

                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast7DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast7DaysMobileDataUsed() / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast7DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast7DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast7DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateWifiMapData(7));
                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());
                    int loopLimit = dateMillisArrayList.size();

                    if (loopLimit > 7)
                        loopLimit = 7;

                    for (int i = 0; i < loopLimit; i++) {
                        long data = dateMillisArrayList.get(i);
                        if (data < 0)
                            data = 0;
                        dataValuesArrayList.add(data);
                    }
                    break;
                case 14:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast14DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast14DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast14DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast14DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast14DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateWifiMapData(14));

                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i);
                        if (data < 0)
                            data = 0;
                        dataValuesArrayList.add(data);
                    }

                    break;
                case 30:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast28DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateWifiMapData(28));

                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i);
                        if (data < 0)
                            data = 0;

                        try {
                            dataValuesArrayList.set((i / 7), dataValuesArrayList.get(i / 7) + data);
                        } catch (Exception e) {
                            dataValuesArrayList.add((i / 7), data);
                        }
                    }

                    break;
                default:
                    if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                        twog_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);
                    } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                        fourg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed() / (1024f * 1024f);
                    }else
                        threeg_d = internetDataUsageUtil.getLast28DaysMobileDataUsed()  / (1024f * 1024f);

                    totalData = internetDataUsageUtil.getLast28DaysTotalDataUsed() / (1024f * 1024f);

                    dateMillisArrayList.addAll(InternetDataUsageUtil.getInstance(context).updateWifiMapData(28));
                    Collections.sort(dateMillisArrayList, Collections.reverseOrder());

                    for (int i = 0; i < dateMillisArrayList.size(); i++) {
                        long data = dateMillisArrayList.get(i);
                        if (data < 0)
                            data = 0;
                        try {
                            dataValuesArrayList.set((i / 7), dataValuesArrayList.get(i / 7) + data);
                        } catch (Exception e) {
                            dataValuesArrayList.add((i / 7), data);
                        }
                    }
                    break;
            }
        }
        try{
            for(int i=0; i<dataValuesArrayList.size();i++)
            {
                hashMap.put(i+"", getDataModelWifiUpdate(hashMap, DataStorageConstants.WIFI_CONN, dataValuesArrayList.get(i),
                        i+"", context));
            }
        }
        catch (Exception e){
        }


        wifi_d = totalData - (twog_d+threeg_d+fourg_d);
        float total_data_in_mb = (threeg_d + twog_d + fourg_d );

        return new DataModel(fourg_d, threeg_d, twog_d, wifi_d, total_data_in_mb + "", hashMap);
    }

    private static DataModel getDataModelWifiUpdate(HashMap<String, DataModel> hashMap, String
            old_type, long totalData, String current_dayNo, Context context) {
        DataModel dataModel;

        SharedPrefManager mSharedPref = new SharedPrefManager(context);

        if (hashMap.containsKey(current_dayNo)) {
            dataModel = hashMap.get(current_dayNo);
        } else {
            dataModel = new DataModel();
        }
        if (old_type.equals(DataStorageConstants.WIFI_CONN)) {
            dataModel.setWifi(totalData / (1024f * 1024f));
        } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
            dataModel.setThreeG(totalData  / (1024f * 1024f));
        }
        else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
            dataModel.setTwoG(totalData  / (1024f * 1024f));
        }
        else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
            dataModel.setFourG(totalData  / (1024f * 1024f));
        }

        float total_data_in_mb = (dataModel.getThreeG() + dataModel.getTwoG() + dataModel.getFourG());
        String data_usage_units = "", data_usages_short_units = "";
        if (total_data_in_mb < 1.0f) {
            data_usage_units = "KILOBYTES";
            data_usages_short_units = "KB";
        } else if (total_data_in_mb > 1.0f && total_data_in_mb <= 1024.0f) {
            data_usage_units = "MEGABYTES";
            data_usages_short_units = "MB";
        } else if (total_data_in_mb > 1024.0f) {
            data_usage_units = "GIGABYTES";
            data_usages_short_units = "GB";
        }

        dataModel.setTotal(String.valueOf(total_data_in_mb));
        dataModel.setLongunit(data_usage_units);
        dataModel.setShortunit(data_usages_short_units);
        return dataModel;
    }

    private static DataModel getDataModelUpdate(HashMap<String, DataModel> hashMap, String
            old_type, long totalData, String current_dayNo, Context context) {
        DataModel dataModel;

        SharedPrefManager mSharedPref = new SharedPrefManager(context);

        if (hashMap.containsKey(current_dayNo)) {
            dataModel = hashMap.get(current_dayNo);
        } else {
            dataModel = new DataModel();
        }
        if (old_type.equals(DataStorageConstants.WIFI_CONN)) {
            dataModel.setWifi(0);
        } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.threeg))) {
                dataModel.setThreeG(totalData  / (1024f * 1024f));
            }
            else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.twog))) {
                dataModel.setTwoG(totalData  / (1024f * 1024f));
            }
            else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(context.getString(R.string.fourg))) {
                dataModel.setFourG(totalData  / (1024f * 1024f));
            }

        float total_data_in_mb = (dataModel.getThreeG() + dataModel.getTwoG() + dataModel.getFourG());
        String data_usage_units = "", data_usages_short_units = "";
        if (total_data_in_mb < 1.0f) {
            data_usage_units = "KILOBYTES";
            data_usages_short_units = "KB";
        } else if (total_data_in_mb > 1.0f && total_data_in_mb <= 1024.0f) {
            data_usage_units = "MEGABYTES";
            data_usages_short_units = "MB";
        } else if (total_data_in_mb > 1024.0f) {
            data_usage_units = "GIGABYTES";
            data_usages_short_units = "GB";
        }


        dataModel.setTotal(String.valueOf(total_data_in_mb));
        dataModel.setLongunit(data_usage_units);
        dataModel.setShortunit(data_usages_short_units);
        return dataModel;
    }

    private static DataModel getDataModelUpdate(HashMap<String, DataModel> hashMap, String
            old_type, long delta_rx, long delta_tx, String current_dayNo) {
        DataModel dataModel;
        if (hashMap.containsKey(current_dayNo)) {
            dataModel = hashMap.get(current_dayNo);
        } else {
            dataModel = new DataModel();
        }
        if (old_type.equals(DataStorageConstants.WIFI_CONN)) {
            dataModel.setWifi(dataModel.getWifi() + ((delta_rx + delta_tx) / (1024f * 1024f)));
        } else if (old_type.equals(DataStorageConstants.THREEG_CONN)) {
            dataModel.setThreeG(dataModel.getThreeG() + ((delta_rx + delta_tx) / (1024f * 1024f)));
        } else if (old_type.equals(DataStorageConstants.TWOG_CONN)) {
            dataModel.setTwoG(dataModel.getTwoG() + ((delta_rx + delta_tx) / (1024f * 1024f)));
        } else if (old_type.equals(DataStorageConstants.FOURG_CONN)) {
            dataModel.setFourG(dataModel.getFourG() + ((delta_rx + delta_tx) / (1024f * 1024f)));
        }
        float total_data_in_mb = (dataModel.getThreeG() + dataModel.getTwoG() + dataModel.getFourG());
        String data_usage_units = "", data_usages_short_units = "";
        if (total_data_in_mb < 1.0f) {
            data_usage_units = "KILOBYTES";
            data_usages_short_units = "KB";
        } else if (total_data_in_mb > 1.0f && total_data_in_mb <= 1024.0f) {
            data_usage_units = "MEGABYTES";
            data_usages_short_units = "MB";
        } else if (total_data_in_mb > 1024.0f) {
            data_usage_units = "GIGABYTES";
            data_usages_short_units = "GB";
        }


        dataModel.setTotal(String.valueOf(total_data_in_mb));
        dataModel.setLongunit(data_usage_units);
        dataModel.setShortunit(data_usages_short_units);
        return dataModel;
    }


    public static long getStartDuration(int no_of_days) {
        if ((no_of_days == 0) || (no_of_days == 30))
            return Constants.THIS_MONTH_START;
        else
            return Constants.THIS_WEEK_START;
    }

    public static String getMonthLabel(int no_of_days, Context context) {
        if ((no_of_days == 0) || (no_of_days == 30))
            return context.getString(R.string.lasting_30_days);
        else if(no_of_days == -1)
            return context.getString(R.string.bill_cycle_lbl);
        else
            return context.getString(R.string.lasting_7_days);
    }
//TODO:UNCOMMENT_LATER
    //
    /*public static int getSelectedID(int no_of_days) {
        if ((no_of_days == 0) || (no_of_days == 30))
            return R.id.this_month;
        else if(no_of_days == -1)
            return R.id.bill_cycle;
        else
            return R.id.this_week;
    }*/

    public static DataStorageConstants.SHOW_CATEGORY_TYPE getEnumValue(int no_of_days) {
        if(no_of_days == -1)
            return DataStorageConstants.SHOW_CATEGORY_TYPE.BILL_CYCLE_TYPE;
        else if ((no_of_days == 0) || (no_of_days == 30))
            return DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_MONTH_TYPE;
        else
            return DataStorageConstants.SHOW_CATEGORY_TYPE.THIS_WEEK_TYPE;
    }

    public static Uri getContactPhotoUri(Context ctx, String number) {

        String id = getContactIDFromNumber(number, ctx);
        Cursor cur = null;
        try {

            cur = ctx.getContentResolver().query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=" + id + " AND "
                            + ContactsContract.Data.MIMETYPE + "='"
                            + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
                    null);
            if (cur != null) {
                if (!cur.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
                .parseLong(id));
        return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }

    public static String getContactIDFromNumber(String contactNumber, Context context) {
        contactNumber = Uri.encode(contactNumber);
        int phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, contactNumber), new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();
        return String.valueOf(phoneContactID);
    }

    
    public static String getUserProfilePicUrl(Context context, ProxyLoginUser.SoResponseEntity user) {
        SharedPrefManager shd = new SharedPrefManager(context);
        String fb_token = shd.getStringValue(Constants.FB_ACCESS_TOKEN);
        if (user != null) {
            if (user.getSite() != null) {
                if (user.getSite().equalsIgnoreCase("facebook")) {
                    return ApiConstants.FB_PROFILE_PIC_URL + fb_token;
                } else {
                    return shd.getStringValue(Constants.GP_PIC_URL);
                }
            }
        }
        return isStringNullEmpty(shd.getStringValue(Constants.GP_PIC_URL)) ? ApiConstants.FB_PROFILE_PIC_URL + fb_token : shd.getStringValue(Constants.GP_PIC_URL);
    }

    public static List<Operator> getOperatorsList(Context context) {
        SdkAppDatabaseHelper dbHelper = new SdkAppDatabaseHelper(context);
        List<Operator> operatorList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = dbHelper.getAllOperators();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String operator = cursor.getString(cursor.getColumnIndex(DataStorageConstants.COLUMN_SERVICE_PROVIDER));
                    int operatorId = cursor.getInt(cursor.getColumnIndex(DataStorageConstants.COLUMN_SERVICE_ROVIDER_ID));
                    operatorList.add(new Operator(operatorId, operator));
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return operatorList;
    }

    public static List<CircleModel> getCircleList(Context context) {

        SdkAppDatabaseHelper dbHelper = new SdkAppDatabaseHelper(context);
        List<CircleModel> circleModels = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = dbHelper.getAllCircle();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String circle = cursor.getString(cursor.getColumnIndex(SdkAppDatabaseHelper.COLUMNS.COLUMN_CIRCLE_NAME));
                    int circle_id = cursor.getInt(cursor.getColumnIndex(SdkAppDatabaseHelper.COLUMNS.COLUMN_CIRCLE_ID));
                    circleModels.add(new CircleModel(circle_id, circle));
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return circleModels;
    }

    public static List<String> operators(List<Operator> list, int val) {
        List<String> operators = new ArrayList<>();
        if (val == 1) {
            for (Operator op : list) {
                operators.add(op.getValue());
            }
        } else {
            for (Operator op : list) {
                operators.add(String.valueOf(op.getId()));
            }
        }
        return operators;
    }

    public static List<String> circles(List<CircleModel> list, int val) {
        List<String> circle = new ArrayList<>();
        if (val == 1) {
            for (CircleModel cir : list) {
                circle.add(cir.getCircle());

            }
        } else {

            for (CircleModel cir : list) {
                circle.add(String.valueOf(cir.getCircleId()));
            }

        }
        return circle;
    }

    public static int getIdFromOperator(Context context, String operator) {
        List<Operator> operatorList = getOperatorsList(context);
        for (Operator op : operatorList) {
            if (op.getValue().equalsIgnoreCase(operator)) {
                return op.getId();
            }

        }
        return 0;
    }

    public static int getIdFromCircle(Context context, String circle) {
        List<CircleModel> circleModels = getCircleList(context);
        for (CircleModel cir : circleModels) {
            if (cir.getCircle().equalsIgnoreCase(circle)) {
                return cir.getCircleId();
            }

        }
        return 0;
    }


    public static String getCategoryName(int categoryId) {
        switch (categoryId) {
            case 1:
                return "2G Data";
            case 2:
                return "3G Data";
            case 4:
                return "Topup";
            case 5:
                return "Full Talktime";
            case 7:
                return "Roaming";
            case 8:
                return "SMS";
            case 9:
                return "Special Recharge";
            default:
                return "Topup";
        }
    }


    public static void updateCardParameters(int contextValue, String cardName, Context ctx, int shoAction) {
        SQLiteDatabase mSQLiteDatabase = DBHelper.getInstance(ctx).getWritableDatabase();
        CardUtils.updateCardParameters(mSQLiteDatabase, cardName,
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, contextValue, shoAction);
    }


    public static void updateOrderCardParameters(String orderStatus, long orderId, Context ctx, int listSize) {
        SharedPrefManager mSharedPref = new SharedPrefManager(ctx);
        mSharedPref.setLongValue(Constants.LAST_TRANSACTION_ID, orderId);
        mSharedPref.setStringValue(Constants.LAST_TRANSACTION_STATUS, orderStatus);

        int contextValue = 0;
        int likeAppContextVal = 0;
        if (orderStatus.equalsIgnoreCase(Constants.ORDER_INIT)) {
            contextValue = 60;
        } else if (orderStatus.equalsIgnoreCase(Constants.ORDER_SUCCESSFUL)) {
            contextValue = 60;
            likeAppContextVal = 40;
        } else if (orderStatus.equalsIgnoreCase(Constants.ORDER_PENDING) && listSize == 1) {//single
            contextValue = 60;
        } else if (orderStatus.equalsIgnoreCase(Constants.ORDER_PENDING) && listSize > 1
                || orderStatus.equalsIgnoreCase(Constants.ORDER_PARTIALLY_SUCCESSFUL)) {//combo
            contextValue = 100;
        } else if (orderStatus.equalsIgnoreCase(Constants.ORDER_FAILED)) {
            contextValue = 60;
            likeAppContextVal = 0;
        } else if (orderStatus.equalsIgnoreCase(Constants.ORDER_PAYMENT_REFUNDED)) {
            contextValue = 60;
        }

        SQLiteDatabase mSQLiteDatabase = DBHelper.getInstance(ctx).getWritableDatabase();//new DBHelper(ctx).getReadableDatabase();
        CardUtils.updateCardParameters(mSQLiteDatabase, DataStorageConstants.CARD_ORDER_STATUS,
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, contextValue, DataStorageConstants.SHOW_CARD);
        CardUtils.updateCardParameters(mSQLiteDatabase, DataStorageConstants.CARD_FEEDBACK,
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK, likeAppContextVal, DataStorageConstants.SHOW_CARD);
    }

    public static GradientDrawable setOnboardingEditText(Context context, int color) {
        GradientDrawable gradientDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable
                    .onboarding_edit_text_background, context.getTheme());
        } else {
            gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable
                    .onboarding_edit_text_background);
        }
        gradientDrawable.setColor(color);
        gradientDrawable.setStroke(1, color);
        return gradientDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawablePrepaidUnselect(Context context, int[] color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(color_array[2]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawablePrepaidSelect(Context context, int[] color_array, int borderColor) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(borderColor);
        final GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[1]);
        return layerDrawable;
    }

    public static StateListDrawable setStateListDrawablePrepaidRadioButton(Context context, int[] color_array, int borderColor) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setRadioButtonLayerDrawablePrepaidSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_pressed}, setRadioButtonLayerDrawablePrepaidSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_focused}, setRadioButtonLayerDrawablePrepaidSelect(context, color_array, borderColor));
        states.addState(new int[]{}, setRadioButtonLayerDrawablePrepaidUnselect(context, color_array));
        return states;
    }

    public static LayerDrawable setRadioButtonLayerDrawablePostpaidUnselect(Context context, int[] color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(color_array[2]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawablePostpaidSelect(Context context, int[] color_array, int borderColor) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(borderColor);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[1]);
        return layerDrawable;
    }

    public static StateListDrawable setStateListDrawablePostpaidRadioButton(Context context, int[] color_array, int borderColor) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setRadioButtonLayerDrawablePostpaidSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_pressed}, setRadioButtonLayerDrawablePostpaidSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_focused}, setRadioButtonLayerDrawablePostpaidSelect(context, color_array, borderColor));
        states.addState(new int[]{}, setRadioButtonLayerDrawablePostpaidUnselect(context, color_array));
        return states;
    }

    public static StateListDrawable setStateListDrawableMidButton(Context context, int[]
            color_array) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setMidRadioButtonLayerDrawableSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_pressed}, setMidRadioButtonLayerDrawableSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_focused}, setMidRadioButtonLayerDrawableSelect(context, color_array));
        states.addState(new int[]{}, setMidRadioButtonLayerDrawableUnselect(context, color_array));
        return states;
    }

    public static LayerDrawable setMidRadioButtonLayerDrawableUnselect(Context context, int[]
            color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(color_array[2]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static LayerDrawable setMidRadioButtonLayerDrawableSelect(Context context, int[]
            color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(context.getResources().getColor(R.color.white));
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[1]);
        return layerDrawable;
    }


    public static ColorStateList setRadioButtonTextDrawable(Context context, int[] color_array) {
        ColorStateList states = new ColorStateList(new int[][]{
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_checked},
                new int[]{}
        },
                new int[]{
                        color_array[0],
                        color_array[0],
                        color_array[2]
                });
        return states;
    }

    public static ColorStateList setRadioButtonTextDrawableWallet(Context context, int[] color_array) {
        ColorStateList states = new ColorStateList(new int[][]{
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_checked},
                new int[]{}
        },
                new int[]{
                        color_array[1],
                        color_array[1],
                        color_array[0]
                });
        return states;
    }

    public static GradientDrawable setButtonNormalState(Context context, int[] color_array) {
        GradientDrawable gradientDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable
                    .button_normal_drawable, context.getTheme());
        } else {
            gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable
                    .button_normal_drawable);
        }
        gradientDrawable.setColor(color_array[3]);
        gradientDrawable.setStroke(1, ContextCompat.getColor(context, R.color.black_trans));
        return gradientDrawable;
    }

    public static GradientDrawable setButtonPressedState(Context context, int[] color_array) {
        GradientDrawable gradientDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable
                    .button_pressed_drawable, context.getTheme());
        } else {
            gradientDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable
                    .button_pressed_drawable);
        }
        gradientDrawable.setColor(darkerColor(color_array[3], .5f));
        gradientDrawable.setStroke(1, ContextCompat.getColor(context, R.color.black_trans));
        return gradientDrawable;
    }

    public static StateListDrawable setStateListButtonDrawable(Context context, int[] color_array) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed}, setButtonPressedState(context, color_array));
        states.addState(new int[]{android.R.attr.state_focused}, setButtonPressedState(context, color_array));
        states.addState(new int[]{}, setButtonNormalState(context, color_array));
        return states;
    }

    public static int darkerColor(int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb(a,
                Math.max((int) (r - (r * factor)), 0),
                Math.max((int) (g - (g * factor)), 0),
                Math.max((int) (b - (b * factor)), 0));
    }

    public static int[] getOperatorTheme(int operator_id, Context context) {
        int[] mOperatorColorArray = null;
        if ((operator_id == 1) || (operator_id == 2) || (operator_id == 13)) {
            mOperatorColorArray = context.getResources().getIntArray(R.array
                    .airtel_operator_colors);
        } else if (operator_id == 6) {
            mOperatorColorArray = context.getResources().getIntArray(R.array.aircel_operator_colors);
        } else if (operator_id == 10) {
            mOperatorColorArray = context.getResources().getIntArray(R.array.loop_operator_colors);
        } else if (operator_id == 11) {
            mOperatorColorArray = context.getResources().getIntArray(R.array.docomo_operator_colors);
        } else if ((operator_id == 12) || (operator_id == 14)) {
            mOperatorColorArray = context.getResources().getIntArray(R.array.virgin_operator_colors);
        } else if ((operator_id == 23) || (operator_id == 17) || (operator_id == 7)) {
            mOperatorColorArray = context.getResources().getIntArray(R.array.videocon_operator_colors);
        } else {
            mOperatorColorArray = context.getResources().getIntArray(R.array.reliance_operator_colors);
        }
        return mOperatorColorArray;
    }

    public static String getNumberOfDaysForComboRecommendation(int days, Context context) {

        String num_of_days = "";
        switch (days) {
            case 21:
                num_of_days = context.getString(R.string.lasting_3_weeks);
                break;
            case 14:
                num_of_days = context.getString(R.string.lasting_2_weeks);
                break;
            case 45:
                num_of_days = context.getString(R.string.lasting_45_days);
                break;
            default:
                num_of_days = context.getString(R.string.lasting_1_month);
                break;
        }
        return num_of_days;
    }


    public static String getStringFromNumberOfDays(int days, Context context) {

        String num_of_days = "";
        //TODO:UNCOMMENT_LATER
        //
        /*switch (days) {
            case 21:
                num_of_days = context.getString(R.string.three_weeks);
                break;
            case 14:
                num_of_days = context.getString(R.string.two_weeks);
                break;
            case 45:
                num_of_days = context.getString(R.string.fortyfive_days);
                break;
            default:
                num_of_days = context.getString(R.string.one_month);
                break;
        }*/
        return num_of_days;
    }


    private static List<String> getApplicationPackageNameWithLauncher(Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities
                (intent, 0);
        List<String> packagename = new ArrayList<>();
        for (ResolveInfo info : resolveInfoList) {
            ApplicationInfo applicationInfo = info.activityInfo.applicationInfo;
            packagename.add(applicationInfo.packageName);
        }
        return packagename;
    }

    public static int getMinCallDuration(int call_duration_in_sec) {
        double duration_in_min = call_duration_in_sec / 60.0f;
        return (int) Math.ceil(duration_in_min);
    }


    public static List<HomeAllInstreamModel.BodyEntity.InStreamRecommendationsEntity.PlansEntity> findAndmakePlanList(Context context) {
        SharedPrefManager shrd = new SharedPrefManager(context);
        String bodyStr = "{\"inStreamRecommendations\":[{\"plans\":[{\"planDetail\":{\"categoryId\":2,\"planId\":17533,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":28,\"rechargeValue\":0,\"category\":\"Special\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"28 Days\",\"description\":\"All Local + STD calls @ 1.2p/sec\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"},\"cost\":6092.95151627704},{\"planDetail\":{\"categoryId\":3,\"planId\":17559,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":88,\"rechargeValue\":0,\"category\":\"Special\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"28 Days\",\"description\":\"All Local & STD calls @ 40p/min\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"},\"cost\":6095.263508564676}],\"categoryId\":9}],\"comboRecommendation\":{\"comboSets\":[{\"hasSms\":0,\"has4g\":0,\"plans\":[{\"categoryId\":4,\"planId\":17627,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":160,\"rechargeValue\":138.35,\"category\":\"Topup\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"NA\",\"description\":\"Talktime of Rs. 138.35\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"},{\"categoryId\":9,\"planId\":17559,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":88,\"rechargeValue\":0,\"category\":\"Special\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"28 Days\",\"description\":\"All Local & STD calls @ 40p/min\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"},{\"categoryId\":2,\"planId\":17591,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":397,\"rechargeValue\":0,\"category\":\"3G\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"28 Days\",\"description\":\"1.5 GB @3G speed. Post 1.5 GB you will be charged 4p/10kb\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"}],\"subText\":\"Plan tailored for your usage\",\"has3g\":1,\"minValidity\":0,\"has2g\":0,\"comboName\":\"Combo Pack 1\",\"comboId\":53880,\"comboSubsets\":[{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":356,\"dominatUsageComponentVal\":265,\"dominatUsageComponentType\":\"local\",\"std\":90,\"data\":1.3935695087001658,\"local\":265,\"sms\":0},\"planIds\":[17627,17559,17591],\"comboCost\":{\"totalCost\":781.4,\"usageCost\":136.4,\"comboCost\":645}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":341,\"dominatUsageComponentVal\":253,\"dominatUsageComponentType\":\"local\",\"std\":86,\"data\":0.0013335593384690575,\"local\":253,\"sms\":0},\"planIds\":[17627,17559],\"comboCost\":{\"totalCost\":6255.263508564676,\"usageCost\":6007.263508564676,\"comboCost\":248}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":54,\"call\":147,\"dominatUsageComponentVal\":111,\"dominatUsageComponentType\":\"local\",\"std\":36,\"data\":1.3335593384690583,\"local\":111,\"sms\":0},\"planIds\":[17627,17591],\"comboCost\":{\"totalCost\":880.4799927696586,\"usageCost\":323.47999276965857,\"comboCost\":557}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":2,\"call\":6,\"dominatUsageComponentVal\":5,\"dominatUsageComponentType\":\"local\",\"std\":1,\"data\":0.0302217630331973,\"local\":5,\"sms\":0},\"planIds\":[17627],\"comboCost\":{\"totalCost\":6354.343501334335,\"usageCost\":6194.343501334335,\"comboCost\":160}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":3,\"dominatUsageComponentVal\":2,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":0.013335593384690578,\"local\":2,\"sms\":0},\"planIds\":[17559,17591],\"comboCost\":{\"totalCost\":621.4,\"usageCost\":136.4,\"comboCost\":485}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":3,\"dominatUsageComponentVal\":2,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":1.3335593384690553E-10,\"local\":2,\"sms\":0},\"planIds\":[17559],\"comboCost\":{\"totalCost\":6095.263508564676,\"usageCost\":6007.263508564676,\"comboCost\":88}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":1,\"call\":2,\"dominatUsageComponentVal\":2,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":1.3335593384690583,\"local\":2,\"sms\":0},\"planIds\":[17591],\"comboCost\":{\"totalCost\":720.4799927696586,\"usageCost\":323.47999276965857,\"comboCost\":397}}],\"hasData\":1,\"spId\":2,\"circleId\":5,\"maxValidity\":28},{\"hasSms\":0,\"has4g\":0,\"plans\":[{\"categoryId\":4,\"planId\":17625,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":140,\"rechargeValue\":119.81,\"category\":\"Topup\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"NA\",\"description\":\"Talktime of Rs. 119.81\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"},{\"categoryId\":9,\"planId\":17564,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":109,\"rechargeValue\":0,\"category\":\"Special\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"28 Days\",\"description\":\"All Local & STD calls @ 35p/min\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"},{\"categoryId\":2,\"planId\":17591,\"circle\":\"Delhi NCR\",\"serviceType\":\"PREPAID_MOBILE\",\"serviceProviderId\":2,\"amount\":397,\"rechargeValue\":0,\"category\":\"3G\",\"isExpired\":false,\"canRecommend\":1,\"validity\":\"28 Days\",\"description\":\"1.5 GB @3G speed. Post 1.5 GB you will be charged 4p/10kb\",\"circleId\":5,\"serviceProvider\":\"Vodafone\"}],\"subText\":\"Plan tailored for your usage\",\"has3g\":1,\"minValidity\":0,\"has2g\":0,\"comboName\":\"Combo Pack 2\",\"comboId\":53949,\"comboSubsets\":[{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":356,\"dominatUsageComponentVal\":265,\"dominatUsageComponentType\":\"local\",\"std\":90,\"data\":1.3935695087001658,\"local\":265,\"sms\":0},\"planIds\":[17625,17564,17591],\"comboCost\":{\"totalCost\":765.35,\"usageCost\":119.35,\"comboCost\":646}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":341,\"dominatUsageComponentVal\":253,\"dominatUsageComponentType\":\"local\",\"std\":86,\"data\":1.3335593384690571E-4,\"local\":253,\"sms\":0},\"planIds\":[17625,17564],\"comboCost\":{\"totalCost\":6239.213508564677,\"usageCost\":5990.213508564677,\"comboCost\":249}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":47,\"call\":127,\"dominatUsageComponentVal\":96,\"dominatUsageComponentType\":\"local\",\"std\":31,\"data\":1.3335593384690583,\"local\":96,\"sms\":0},\"planIds\":[17625,17591],\"comboCost\":{\"totalCost\":860.4799927696586,\"usageCost\":323.47999276965857,\"comboCost\":537}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":2,\"call\":4,\"dominatUsageComponentVal\":4,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":0.025380902213641785,\"local\":4,\"sms\":0},\"planIds\":[17625],\"comboCost\":{\"totalCost\":6334.343501334335,\"usageCost\":6194.343501334335,\"comboCost\":140}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":3,\"dominatUsageComponentVal\":2,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":0.013335593384690578,\"local\":2,\"sms\":0},\"planIds\":[17564,17591],\"comboCost\":{\"totalCost\":625.35,\"usageCost\":119.35,\"comboCost\":506}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":0,\"call\":3,\"dominatUsageComponentVal\":2,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":1.3335593384690553E-10,\"local\":2,\"sms\":0},\"planIds\":[17564],\"comboCost\":{\"totalCost\":6099.213508564677,\"usageCost\":5990.213508564677,\"comboCost\":109}},{\"equivalentUsage\":{\"nightCall\":0,\"inNetworkTotalUsage\":1,\"call\":2,\"dominatUsageComponentVal\":2,\"dominatUsageComponentType\":\"local\",\"std\":0,\"data\":1.3335593384690583,\"local\":2,\"sms\":0},\"planIds\":[17591],\"comboCost\":{\"totalCost\":720.4799927696586,\"usageCost\":323.47999276965857,\"comboCost\":397}}],\"hasData\":1,\"spId\":2,\"circleId\":5,\"maxValidity\":28}],\"usage\":{\"nightCall\":1,\"inNetworkTotalUsage\":126,\"call\":341,\"dominatUsageComponentVal\":254,\"dominatUsageComponentType\":\"local\",\"std\":87,\"data\":1.3335593384690583,\"local\":254,\"sms\":0}}}";
        HomeAllInstreamModel.BodyEntity planbody = new Gson().fromJson(bodyStr, HomeAllInstreamModel.BodyEntity.class);
        return planbody.getInStreamRecommendations().get(0).getPlans();
    }

    public static int getIsNightValue(long start_time) {
        String night_start_time = "23:00:00";
        String night_end_time = "06:00:00";
        Date date = new Date(start_time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String dateo = simpleDateFormat.format(date);
        if (dateo.compareTo(night_start_time) > 0 || dateo.compareTo(night_end_time) < 0) {
            return 1;
        }
        return 0;
    }

    public static void setDataForTesting(Context context, String type, String data, String date,
                                         String status) {
        SharedPrefManager mSharedPreference = new SharedPrefManager(context);
        String jsonData = mSharedPreference.getStringValue(Constants.BACKGROUNDSERVICE);
        try {
            JSONObject newJson = new JSONObject();
            newJson.put("Service Type", type);
            newJson.put("Service Data", data);
            DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
            newJson.put("Service Time", df.format(Calendar.getInstance().getTime()));
            newJson.put("Service Status", status);
            if (!jsonData.equals("")) {
                JSONObject jsonObject = new JSONObject(jsonData);
                JSONArray jsonArray = jsonObject.optJSONArray("data");
                jsonArray.put(newJson);
                jsonObject.put("data", jsonArray);
                mSharedPreference.setStringValue(Constants.BACKGROUNDSERVICE, jsonObject.toString());
            } else {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(newJson);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("data", jsonArray);
                mSharedPreference.setStringValue(Constants.BACKGROUNDSERVICE, jsonObject.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
    }

    public static long appinstallTime(Context context) throws PackageManager.NameNotFoundException {

        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
    }

    public static String getHashedDeviceID(String deviceID) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Lg.e("DEVICEID-HASHING", e.getMessage());
            e.printStackTrace();
        }
        messageDigest.reset();
        messageDigest.update(deviceID.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        return new String(bytesToHex(resultByte)).toLowerCase();
    }

    public static String getShaEncryptedData(String data){
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            Lg.e("DEVICEID-HASHING", e.getMessage());
            e.printStackTrace();
        }
        messageDigest.reset();
        messageDigest.update(data.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        return new String(bytesToHex(resultByte)).toLowerCase();
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static String getEmijoByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = phoneNumber;
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            contactName = (!TextUtils.isEmpty(name))? name : phoneNumber;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    public static void showMessageOKCancel(Context context, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public static void instreamCall(Context context) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        String mPrefferedDataType = sharedPrefManager.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
        String CATEGORYID = "1";
        if (mPrefferedDataType.equalsIgnoreCase("2G")) {
            CATEGORYID = "1";
        } else if (mPrefferedDataType.equalsIgnoreCase("3G") || mPrefferedDataType.equalsIgnoreCase("4G") || mPrefferedDataType.equalsIgnoreCase("4G/LTE")) {
            CATEGORYID = "2";
        }
        if (AppUtils.isConnectingToInternet(context)) {
            ApiUtility apiUtility = new ApiUtility();
            try {
                if (!DateUtil.nDaysGoneSinceOnBoard(context, 3)) {
                    apiUtility.postInstreamRecommendation(context, CATEGORYID, 28);
                } else {
                    apiUtility.getInstreamRecommendation(context, CATEGORYID, 28);
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void logoutUser(Context context) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        sharedPrefManager.deleteStringKeyVal(Constants.USER_DATA);
        sharedPrefManager.deleteStringKeyVal(Constants.LAST_TRANSACTION_STATUS);
        sharedPrefManager.deleteStringKeyVal(Constants.LAST_TRANSACTION_ID);
        sharedPrefManager.setStringValue(Constants.GP_PIC_URL, "");
        BranchAndParseUtils.unsubscribeFromPushChannelOnLogout(context);
        Branch.getInstance(context).logout();
        sharedPrefManager.setStringValue(Constants.FB_ACCESS_TOKEN, "");
        sharedPrefManager.setStringValue(Constants.WS_HASH, "");
        sharedPrefManager.setStringValue(Constants.DELETE_CARD_HASH, "");
        sharedPrefManager.setStringValue(Constants.SAVE_CARD_HASH, "");
        sharedPrefManager.setStringValue(Constants.FETCH_CARD_HASH, "");
        sharedPrefManager.setStringValue(Constants.VAS_CARD_HASH, "");
        sharedPrefManager.setStringValue(Constants.DELETE_STORED_CVV_HASH,"");
        BranchPrefManager branchPrefManager = BranchPrefManager.getInstance(context);
        branchPrefManager.clearSharedPrefs();
        //TODO:UNCOMMENT_LATER
        //Mobihelp.clearUserData(context);
    }

    public static String parseTime(long milliseconds) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliseconds),
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(milliseconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }

    public static int setChartHeightCoefficient(int num_of_calls) {
        int mDivisionFactor = 1;
        if (num_of_calls <= 5) {
            mDivisionFactor = 20;
        } else if (num_of_calls <= 10) {
            mDivisionFactor = 10;
        } else if (num_of_calls <= 20) {
            mDivisionFactor = 5;
        } else if (num_of_calls <= 30) {
            mDivisionFactor = 4;
        } else if (num_of_calls <= 40) {
            mDivisionFactor = 3;
        }else if (num_of_calls <= 50) {
            mDivisionFactor = 2;
        }
        return mDivisionFactor;
    }

    public static int setChartHeightMinValue(int chart_height) {
        return (((chart_height / 100) + 1) * 12);
    }


    public static String getPrimaryEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        if (accountManager != null) {
            return getAccount(accountManager);
        } else {
            return null;
        }
    }

    private static String getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccounts();
        if (accounts != null && accounts.length > 0) {
            for (Account account : accounts) {
                if (account.type.equalsIgnoreCase("com.google")) {
                    return account.name;
                }
            }
        }
        return null;
    }

    private static String rechargeCardNumber;
    private static String rechargeCardProvider;
    private static String rechargeCardCircle;
    private static String rechargeCardAmount;

    public static void setRechargeCardNumber(String rechargeCardNumber) {
        AppUtils.rechargeCardNumber = rechargeCardNumber;
    }

    public static void setRechargeCardProvider(String rechargeCardProvider) {
        AppUtils.rechargeCardProvider = rechargeCardProvider;
    }

    public static void setRechargeCardCircle(String rechargeCardCircle) {
        AppUtils.rechargeCardCircle = rechargeCardCircle;
    }

    public static void setRechargeCardAmount(String rechargeCardAmount) {
        AppUtils.rechargeCardAmount = rechargeCardAmount;
    }

    public static String getRechargeCardNumber() {
        return rechargeCardNumber;
    }

    public static String getRechargeCardProvider() {
        return rechargeCardProvider;
    }

    public static String getRechargeCardCircle() {
        return rechargeCardCircle;
    }

    public static String getRechargeCardAmount() {
        return rechargeCardAmount;
    }

    public static int getWalletAmount(Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        return sharedPreference.getIntValue(Constants.WALLET_BALANCE);
    }

    public static void setWalletAmount(int walletAmount, Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        sharedPreference.setIntValue(Constants.WALLET_BALANCE, walletAmount);
    }


    public static int getMonthlyCreditedAmount(Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        return sharedPreference.getIntValue(Constants.WALLET_MONTHLY_CREDIT);
    }

    public static void setMonthlyCreditedAmount(int monthlyCreditedAmount, Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        sharedPreference.setIntValue(Constants.WALLET_MONTHLY_CREDIT, monthlyCreditedAmount);
    }

    public static int getMonthlyDebitedAmount(Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        return sharedPreference.getIntValue(Constants.WALLET_MONTHLY_DEBIT);
    }

    public static void setMonthlyDebitedAmount(int monthlyDebitedAmount, Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        sharedPreference.setIntValue(Constants.WALLET_MONTHLY_DEBIT, monthlyDebitedAmount);
    }

    public static int getWalletON(Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        return sharedPreference.getIntValue(Constants.WALLET_ON);
    }

    public static void setWalletON(int walletON, Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        sharedPreference.setIntValue(Constants.WALLET_ON, walletON);
    }

    public static boolean planKnown(Context context) {
        SharedPrefManager sharedPreference = new SharedPrefManager(context);
        String mSimType = sharedPreference.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
        String data = sharedPreference.getStringValue(Constants.ON_BOARDING_DATA_LIMIT);
        if (mSimType.equalsIgnoreCase("Postpaid") && data != null) {
            return true;
        } else {
            return false;
        }
    }
//TODO:UNCOMMENT_LATER
    //
    /*public static PieData setGraphData(Context context, int val1, int val2, String card_name) {
        int[] graphcolors = new int[]{ContextCompat.getColor(context, R.color.calling_primary), ContextCompat.getColor(context, R.color.graph_black)};
        int[] std_graphcolor = new int[]{ContextCompat.getColor(context, R.color.std_primary), ContextCompat.getColor(context, R.color.graph_black)};
        ArrayList<Entry> yVals1 = new ArrayList<>();
        yVals1.add(new Entry(val1, 0));
        yVals1.add(new Entry(val2, 1));
        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("outgoing");
        xVals.add("incoming");
        PieDataSet dataSet = new PieDataSet(yVals1, "Calling Usage");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        if (!card_name.equalsIgnoreCase(DataStorageConstants.CARD_STD_SUMMARY)) {
            dataSet.setColors(graphcolors);
        } else {
            dataSet.setColors(std_graphcolor);
        }
        PieData data = new PieData(xVals, dataSet);
        data.setDrawValues(false);
        return data;
    }

    public static void setPieChartCardParams(Context context, PieChart pieChart, String cardname) {
        pieChart.setUsePercentValues(true);
        pieChart.setDescription("");
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColorTransparent(false);
        pieChart.setHoleRadius(88f);
        pieChart.setDrawCenterText(true);
        pieChart.setDrawSliceText(false);
        pieChart.setTouchEnabled(false);
        pieChart.setRotationEnabled(true);
        pieChart.getLegend().setEnabled(false);
        pieChart.setCenterText("\nMINUTES");
        Typeface roboto_light = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        pieChart.setCenterTextTypeface(roboto_light);
        pieChart.setCenterTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, 12));
        if (cardname.equalsIgnoreCase(DataStorageConstants.CARD_DATA_TOP_APPS)) {
            pieChart.setCenterTextColor(ContextCompat.getColor(context, R.color.datausage_color_primary));
        } else if (!(cardname.equalsIgnoreCase(DataStorageConstants.CARD_STD_SUMMARY) || cardname.equalsIgnoreCase(DataStorageConstants.CARD_STD_TOP_CONTACTS))) {
            pieChart.setCenterTextColor(ContextCompat.getColor(context, R.color.calling_primary));
        } else {
            pieChart.setCenterTextColor(ContextCompat.getColor(context, R.color.std_primary));
        }
        pieChart.setCenterTextWordWrapEnabled(true);
    }*/

    public static int getPixels(int unit, float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(unit, size, metrics);
    }

    public static void sendBroadcast(Context context, String cardName) {
        Intent intent = new Intent("send");
        intent.putExtra("card_refresh", cardName);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static long getLastSendEntry(SQLiteDatabase db, int send_type, int data_type) {
        Cursor cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "= " + data_type + " and " + DBContractor.SendToServerEntry.COLUMN_SEND_TYPE + "= " + send_type, null, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        if (cursor != null && cursor.moveToFirst()) {
            long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_TIMESTAMP));
            return timestamp;
        } else {
            return 0;
        }
    }

    public static long getLastDataTypeEntry(SQLiteDatabase db, int data_type) {
        Cursor cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "= ?", new String[]{String.valueOf(data_type)}, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        if (cursor != null && cursor.moveToFirst()) {
            long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_TIMESTAMP));
            return timestamp;
        } else {
            return 0;
        }
    }

    public static Cursor getLastRoamingLocation(SQLiteDatabase sqLiteDatabase) {
        return sqLiteDatabase.rawQuery("SELECT " + DBContractor.CallDataEntry.COLUMN_LOCATION_STATE + ", " + DBContractor.CallDataEntry.COLUMN_LOCATION_LOCALITY + ", " + DBContractor.CallDataEntry.COLUMN_LOCATION_COUNTRY + ", " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + " = 1 group by " +
                DBContractor.CallDataEntry
                        .COLUMN_LOCATION_STATE + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " DESC " +
                "LIMIT 4", null);
    }


    public static LayerDrawable setRadioButtonLayerDrawablePaymentUnSelect(Context context, int[] color_array, int borderColor) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(borderColor);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[2]);
        return layerDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawablePaymentSelect(Context context, int[] color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(color_array[0]);
        final GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static StateListDrawable setStateListDrawablePaymentRadioButton(Context context, int[] color_array, int borderColor) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setRadioButtonLayerDrawablePaymentSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_pressed}, setRadioButtonLayerDrawablePaymentSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_focused}, setRadioButtonLayerDrawablePaymentSelect(context, color_array));
        states.addState(new int[]{}, setRadioButtonLayerDrawablePaymentUnSelect(context, color_array, borderColor));
        return states;
    }

    public static LayerDrawable setRadioButtonLayerDrawableGiftCouponUnSelect(Context context, int[] color_array, int borderColor) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(borderColor);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[2]);
        return layerDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawableGiftCouponSelect(Context context, int[] color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(color_array[0]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static StateListDrawable setStateListDrawableGiftRadioButton(Context context, int[] color_array, int borderColor) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setRadioButtonLayerDrawableGiftCouponSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_pressed}, setRadioButtonLayerDrawableGiftCouponSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_focused}, setRadioButtonLayerDrawableGiftCouponSelect(context, color_array));
        states.addState(new int[]{}, setRadioButtonLayerDrawableGiftCouponUnSelect(context, color_array, borderColor));
        return states;
    }

    public static Cursor getLastRoamingLocationTime(SQLiteDatabase sqLiteDatabase) {
        return sqLiteDatabase.rawQuery("SELECT " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + " = 1 order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " ASC " +
                "LIMIT 1", null);
    }

    private static List<HomeCardsModel> orgCardList;

    public static Set<String> renderedCards;

    public static Set<String> getRenderedCards() {
        return renderedCards;
    }

    public static void setRenderedCards(Set<String> renderedCards) {
        AppUtils.renderedCards = renderedCards;
    }

    public static List<HomeCardsModel> getOrgCardList() {
        return orgCardList;
    }

    public static void setOrgCardList(List<HomeCardsModel> orgCardList) {
        AppUtils.orgCardList = orgCardList;
    }

    public static void updateOrgCardList(String cardName, int value) {
        HomeCardsModel model = new HomeCardsModel();
        model.setCardName(cardName);
        model.setIsTapped(value);
        if (renderedCards != null && !renderedCards.contains(cardName)) {
            renderedCards.add(cardName);
        }
        if (AppUtils.orgCardList != null && AppUtils.orgCardList.size()>0) {
            for (int i = 0; i < AppUtils.orgCardList.size(); i++) {
                HomeCardsModel homeCardsModel = AppUtils.orgCardList.get(i);
                if (homeCardsModel!=null && homeCardsModel.getCardName().equalsIgnoreCase(model.getCardName())) {
                    if (value == 1) {
                        if (homeCardsModel.getIsTapped() == 0)
                            AppUtils.orgCardList.set(i, model);
                    } else
                        AppUtils.orgCardList.set(i, model);
                    break;
                }
            }
        }
    }

    public static void triggerGAEventRenderedCard(Context context) {

        try {
            DataLayer mDataLayer;
            mDataLayer = TagManager.getInstance(context).getDataLayer();

            if (!getRenderedCards().isEmpty()) {
                for (String card : getRenderedCards()) {
                    mDataLayer.push(DataLayer.mapOf("event", "GAEvent", "eventAct", card, "eventCat", "Card", "eventLbl", "Visible", "eventVal", 1));
                }
            }
        }catch (Exception e){}

    }

    public static boolean checkIfLastSendSixHoursBefore(SQLiteDatabase mDatabase) {
        long dailysend_call_type = AppUtils.getLastSendEntry(mDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.CALLSMS_DATA_TYPE);
        long dailysend_data_type = AppUtils.getLastSendEntry(mDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.INTERNET_DATA_TYPE);
        long dailysend_app_type = AppUtils.getLastSendEntry(mDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.APP_DATA_TYPE);
        if (dailysend_call_type != 0 && dailysend_data_type != 0 && dailysend_app_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_call_type)) && DateUtil.isToday(new Date(dailysend_app_type)) && DateUtil.isToday(new Date(dailysend_data_type))) {
                if (DateUtil.sixHourBefore(dailysend_app_type) && DateUtil.sixHourBefore(dailysend_call_type) && DateUtil.sixHourBefore(dailysend_data_type)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String calculateShaHash(Context context, ProxyLoginUser.SoResponseEntity
            ssoUser, String amount, long timeStamp) {
        try {
            String ssoId = ssoUser.getUserId();
            String email = ssoUser.getPrimaryEmailId();
            String phoneNumber = ssoUser.getVerifiedMobile();
            String deviceId = getDeviceIDForSSO(context);
            StringBuilder sB = new StringBuilder();
            sB.append(ssoId).append("|").append(email).append("|").append(phoneNumber).append("|")
                    .append(amount).append("|").append(timeStamp);
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(sB.toString().getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateCards(Context context) {
        CardUtils.resetContextCoefficient(context);
        CardUtils.updateUntappedCard(context);
        CardUtils.stdCallsPerWeek(context);
        CardUtils.lastCallTime(context);
        CardUtils.lastSmsTime(context);
        CardUtils.appTime(context);
        CardUtils.instreamRecoCard(context);
        CardUtils.roamingCards(context);
        CardUtils.wifiCard(context);
        CardUtils.parseCardsJsonOrg();
        AppUtils.setRenderedCards(new TreeSet<String>());
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public static StateListDrawable setStateListDrawableSavedCardRadioButton(Context context, int[] color_array, int borderColor) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setRadioButtonLayerDrawableSavedCardSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_pressed}, setRadioButtonLayerDrawableSavedCardSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_focused}, setRadioButtonLayerDrawableSavedCardSelect(context, color_array, borderColor));
        states.addState(new int[]{}, setRadioButtonLayerDrawableSavedCardUnselect(context, color_array));
        return states;
    }

    public static LayerDrawable setRadioButtonLayerDrawableSavedCardSelect(Context context, int[] color_array, int borderColor) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(color_array[0]);
        final GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawableSavedCardUnselect(Context context, int[] color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .prepaid_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(color_array[0]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[2]);
        return layerDrawable;
    }

    public static StateListDrawable setStateListDrawableNetBankRadioButton(Context context, int[] color_array, int borderColor) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setRadioButtonLayerDrawableNetBankSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_pressed}, setRadioButtonLayerDrawableNetBankSelect(context, color_array, borderColor));
        states.addState(new int[]{android.R.attr.state_focused}, setRadioButtonLayerDrawableNetBankSelect(context, color_array, borderColor));
        states.addState(new int[]{}, setRadioButtonLayerDrawableNetBankUnselect(context, color_array));
        return states;
    }

    public static LayerDrawable setRadioButtonLayerDrawableNetBankUnselect(Context context, int[] color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(color_array[0]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[2]);
        return layerDrawable;
    }

    public static LayerDrawable setRadioButtonLayerDrawableNetBankSelect(Context context, int[] color_array, int borderColor) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .postpaid_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(color_array[0]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static StateListDrawable setStateListDrawableCCDCButton(Context context, int[]
            color_array) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                setMidRadioButtonLayerDrawableSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_pressed}, setCCDCRadioButtonLayerDrawableSelect(context, color_array));
        states.addState(new int[]{android.R.attr.state_focused}, setCCDCRadioButtonLayerDrawableSelect(context, color_array));
        states.addState(new int[]{}, setCCDCRadioButtonLayerDrawableUnselect(context, color_array));
        return states;
    }

    public static LayerDrawable setCCDCRadioButtonLayerDrawableUnselect(Context context, int[]
            color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_unselected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_unselected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_border);
        gradientDrawableBorder.setColor(color_array[0]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.unselect_background);
        gradientDrawableBackground.setColor(color_array[2]);
        return layerDrawable;
    }

    public static LayerDrawable setCCDCRadioButtonLayerDrawableSelect(Context context, int[]
            color_array) {
        LayerDrawable layerDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_selected, context.getTheme());
        } else {
            layerDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable
                    .midbutton_sim_type_selected);
        }
        GradientDrawable gradientDrawableBorder = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_border);
        gradientDrawableBorder.setColor(color_array[0]);
        GradientDrawable gradientDrawableBackground = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(R.id.select_background);
        gradientDrawableBackground.setColor(color_array[0]);
        return layerDrawable;
    }

    public static String getAppVersionName(Context context) {
        PackageInfo pInfo;
        String version = "";
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }


    public static String loadJSONFromAsset(Context ctc) {
        String json = null;
        try {
            InputStream is = ctc.getAssets().open("test_json.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static void showSnackbar(View view , String message, int length){
        Snackbar snackbar = Snackbar
                .make(view, message, length);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(3);
        snackbar.show();
    }

    public static void insertUSSDDetail(Context context, List<USSDDetailModel> ussdList) {
        SQLiteDatabase msqliteDatabase = DBHelper.getInstance(context).getWritableDatabase();

        int deleteRowCount = msqliteDatabase.delete(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.TABLE_NAME, "1", null);

        ContentValues values = new ContentValues();

        Iterator itrCardCategoryList = ussdList.iterator();
        while (itrCardCategoryList.hasNext()) {
            USSDDetailModel model = (USSDDetailModel)itrCardCategoryList.next();
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_TITLE, model.getTitle());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DESCRIPTION, model.getDescription());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_CODE_TYPE, model.getType());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DEFAULT_DIAL_CODE, model.getDefaultDialCode());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_UPDATED_DIAL_CODE, model.getUpdatedDialCode());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DIAL_INSTRUCTION, model.getDialInstruction());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DEFAULT_SMS_CODE, model.getDefaultSMSCode());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_UPDATED_SMS_CODE, model.getUpdatedSMSCode());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_SMS_TEXT, model.getSmsMessage());
            values.put(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_SMS_INSTRUCTION, model.getSmsInstruction());

            long newRowID = msqliteDatabase.insert(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.TABLE_NAME, null, values);
        }
    }

    public static List<USSDDetailModel> fetchUSSDDetail(SQLiteDatabase msqliteDatabase, int type) {
        List<USSDDetailModel> list = new ArrayList<>();

        String[] colSelect = new String[]{
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_TITLE,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DESCRIPTION,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_CODE_TYPE,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DEFAULT_DIAL_CODE,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_UPDATED_DIAL_CODE,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DIAL_INSTRUCTION,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DEFAULT_SMS_CODE,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_UPDATED_SMS_CODE,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_SMS_TEXT,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_SMS_INSTRUCTION};

        Cursor cursor = msqliteDatabase.query(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.TABLE_NAME,
                colSelect,
                com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_CODE_TYPE + "=?", new String[]{String.valueOf(type)},
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            USSDDetailModel model;
            do {
                model = new USSDDetailModel();
                model.setTitle(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_TITLE)));
                model.setDescription(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DESCRIPTION)));
                model.setType(cursor.getInt(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_CODE_TYPE)));
                model.setDefaultDialCode(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DEFAULT_DIAL_CODE)));
                model.setUpdatedDialCode(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_UPDATED_DIAL_CODE)));
                model.setDialInstruction(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DIAL_INSTRUCTION)));
                model.setDefaultSMSCode(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_DEFAULT_SMS_CODE)));
                model.setUpdatedSMSCode(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_UPDATED_SMS_CODE)));
                model.setSmsMessage(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_SMS_TEXT)));
                model.setSmsInstruction(cursor.getString(cursor.getColumnIndex(com.getsmartapp.lib.database.DBContractor.USSDCodeDetails.COLUMN_SMS_INSTRUCTION)));

                list.add(model);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        return list;
    }

    public static void triggerSMSWorkflow(final Context context) {
        Handler handler;
        final SharedPrefManager shrd = new SharedPrefManager(context);
        try {
            if(DateUtil.nDaysBefore(shrd.getLongValue(Constants.LAST_CALL_SUMMARY_SMS), 7) && BranchAndParseUtils.verifyTagSubscription("user_onboarded")) {
                handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        sendUsageSMS("0CBldRcS1MY%3D", "SMARTP", "SMARTP-2016-03-31-1", InAppContextualUsageUtil.callingUsageForSMS(context), shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                    }
                };
                shrd.setLongValue(Constants.LAST_CALL_SUMMARY_SMS, System.currentTimeMillis());
                handler.postDelayed(runnable, getRandomTime());
            }
            if(DateUtil.nDaysBefore(shrd.getLongValue(Constants.LAST_DATA_SUMMARY_SMS), 7) && BranchAndParseUtils.verifyTagSubscription("user_onboarded")) {
                handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        sendUsageSMS("bZS%2F%2BY18j7k%3D", "SMARTP", "SMARTP-2016-03-31-2", InAppContextualUsageUtil.dataUsageForSMS(context), shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                    }
                };
                shrd.setLongValue(Constants.LAST_DATA_SUMMARY_SMS, System.currentTimeMillis());
                handler.postDelayed(runnable, getRandomTime());
            }
            if((DateUtil.nDaysBefore(shrd.getLongValue(Constants.LAST_RECOMMENDED_PLAN_SMS), 5)) && (BranchAndParseUtils.verifyTagSubscription("user_onboarded")) && (shrd.getIntValue(Constants.RECOMMENDATION_SMS_COUNT) != 1)) {
                handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        sendUsageSMS("b5RhGDoKfnE%3D", "SMARTP", "SMARTP-2016-03-31-3", InAppContextualUsageUtil.recommendPlanSMS(), shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                    }
                };
                shrd.setIntValue(Constants.RECOMMENDATION_SMS_COUNT, 1);
                handler.postDelayed(runnable, getRandomTime());
            }
            if(DateUtil.nDaysBefore(shrd.getLongValue(Constants.LAST_NOT_REGISTERED_SMS), 7) && BranchAndParseUtils.verifyTagSubscription("user_onboarded") && !BranchAndParseUtils.verifyTagSubscription("user_registered") && shrd.getIntValue(Constants.NOT_REGISTERED_SMS_COUNT) <= 6) {
                handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        sendUsageSMS("4ILrE4I%2B%2FRE%3D", "SMARTP", "SMARTP-2016-03-31-7", "Invite your friends and get free recharges through our awesome referral program. Invite friends here. " + Constants.DEEPLINK_REFERRAL, shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                    }
                };
                shrd.setIntValue(Constants.NOT_REGISTERED_SMS_COUNT, shrd.getIntValue(Constants.NOT_REGISTERED_SMS_COUNT) + 1);
                shrd.setLongValue(Constants.LAST_NOT_REGISTERED_SMS, System.currentTimeMillis());
                handler.postDelayed(runnable, getRandomTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> triggerUsagePushWorkflow(Context mContext, PushMessage message) {
        SharedPrefManager shrd = new SharedPrefManager(mContext);
        Map<String, String> pushText = new HashMap<>();
        int typeCall = 0;
        int typeData = 0;
        int typeWiFi = 0;
        try {

            if(DateUtil.nDaysBefore(shrd.getLongValue(Constants.LAST_CALL_SUMMARY_PUSH), 7) && BranchAndParseUtils.verifyTagSubscription("user_onboarded")) {

                if(shrd.getBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_1)) {
                    typeCall = 1;
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_1, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_2, Boolean.TRUE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_3, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_4, Boolean.FALSE);
                } else if(shrd.getBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_2)) {
                    typeCall = 2;
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_1, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_2, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_3, Boolean.TRUE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_4, Boolean.FALSE);
                } else if(shrd.getBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_3)) {
                    typeCall = 3;
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_1, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_2, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_3, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_4, Boolean.TRUE);
                } else if(shrd.getBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_4)) {
                    typeCall = 4;
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_1, Boolean.TRUE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_2, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_3, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.CALL_SUMMARY_PUSH_TYPE_4, Boolean.FALSE);
                }

                pushText = InAppContextualUsageUtil.callingUsageForPush(mContext, typeCall);
                shrd.setLongValue(Constants.LAST_CALL_SUMMARY_PUSH, System.currentTimeMillis());
                shrd.setStringValue(Constants.CUSTOM_DEEPLINK, pushText.containsKey("deeplink") ? pushText.get("deeplink") : "com.getsmartapp://deeplink/splash");
            }

            if(DateUtil.nDaysBefore(shrd.getLongValue(Constants.LAST_DATA_SUMMARY_PUSH), 10) && BranchAndParseUtils.verifyTagSubscription("user_onboarded")) {

                if(shrd.getBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_1)) {
                    typeData = 1;
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_1, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_2, Boolean.TRUE);
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_3, Boolean.FALSE);
                } else if(shrd.getBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_2)) {
                    typeData = 2;
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_1, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_2, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_3, Boolean.TRUE);
                } else if(shrd.getBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_3)) {
                    typeData = 3;
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_1, Boolean.TRUE);
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_2, Boolean.FALSE);
                    shrd.setBooleanValue(Constants.DATA_SUMMARY_PUSH_TYPE_3, Boolean.FALSE);
                }
                pushText = InAppContextualUsageUtil.dataUsageForPush(mContext, typeData);
                shrd.setLongValue(Constants.LAST_DATA_SUMMARY_PUSH, System.currentTimeMillis());
                shrd.setStringValue(Constants.CUSTOM_DEEPLINK, pushText.containsKey("deeplink") ? pushText.get("deeplink") : "com.getsmartapp://deeplink/splash");
            }


        } catch(Exception e) {
            e.printStackTrace();
        }
        return pushText;
    }

    public static void sendUsageSMS(String templateId, String masking, String templateName, String templateText, String recPhoneNo) {
        String csHash = getSMSChecksum(recPhoneNo + "|" + templateId + "|" + masking + "|" + templateName + "|" + templateText + "|" + Constants.encyptionSalt);

        HashMap<String, String> map = new HashMap<>();
        map.put("templateId", templateId);
        map.put("masking", masking);
        map.put("templateName", templateName);
        map.put("templateText", templateText);
        map.put("recPhoneNo", recPhoneNo);
        map.put("csHash", csHash);

        RestClient restClient = new RestClient(ApiConstants.BASE_RECHARGER_ADDRESS, null);
        restClient.getApiService().sendSMS(map, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public static String getSMSChecksum(String msg) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            Lg.e("SMS-HASHING", e.getMessage());
            e.printStackTrace();
        }
        messageDigest.reset();
        messageDigest.update(msg.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        return new String(bytesToHex(resultByte)).toLowerCase();
    }

    public static String getMerchantKey(Context mContext ){
        try {
            Bundle bundle = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), PackageManager.GET_META_DATA).metaData;
            String mMerchantKey = mContext.getString(R.string.payu_merchant_id);//bundle.getString("payu_merchant_id");
            return mMerchantKey;
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NullPointerException e) {
        }
        return null;
    }


    public static void setCardTokenNMerchantHash(Context ctx,String cardToken, String merchantHash,String ssoId) {
        //create test hashmap
        HashMap<String, String> testHashMap = new HashMap<String, String>();
        testHashMap.put(Constants.CARD_TOKEN, cardToken);
        testHashMap.put(Constants.MERCHANT_HASH, merchantHash);

        //convert to string using gson
        Gson gson = new Gson();
        String hashMapString = gson.toJson(testHashMap);

        //save in shared prefs
        SharedPrefManager sharedPreference =  new SharedPrefManager(ctx);
        sharedPreference.setStringValue(ssoId, hashMapString);

    }


    public static void removeCardTokenNMerchantHash(Context ctx,String ssoId){
        SharedPrefManager sharedPreference =  new SharedPrefManager(ctx);
        sharedPreference.deleteStringKeyVal(ssoId);
    }

    public static String getMaskedCardNumber(String cardNumber){
        String[] splitArr = cardNumber.split(" ");
        int arrLength = splitArr.length;
        StringBuilder sb = new StringBuilder();

        if(arrLength>1) {
            for (int i = 0; i < arrLength; i++) {
                if (i == 0) {
                    sb.append(splitArr[i]).append(" ");
                } else if (i == arrLength - 1) {
                    sb.append(splitArr[i]);
                } else {
                    sb.append(splitArr[i].replaceAll("\\S", "*")).append(" ");
                }

            }
            return sb.toString();
        }
        return "";
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getAlbumStorageDir(Context context, String albumName) {
        // Get the directory for the app's private pictures directory.
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("getAlbumStorageDir", "Directory not created");
        }
        return file;
    }

    public static boolean saveReferralImage(Context context) {
        Boolean isImageSaved;
        if(AppUtils.isExternalStorageWritable()) {
            try {
                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.referral_image);
                File file = new File(AppUtils.getAlbumStorageDir(context, "SmartApp"), Constants.REFERRAL_IMAGE_NAME);
                FileOutputStream outStream = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();
                isImageSaved = Boolean.TRUE;
            } catch(Exception e) {
                e.printStackTrace();
                isImageSaved = Boolean.FALSE;
            }
        } else {
            isImageSaved = Boolean.FALSE;
        }
        return isImageSaved;
    }

    public static Uri getReferralImageURI(Context context) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        File file = null;
        try {
            if(sharedPrefManager.getBooleanValue(Constants.IS_REFERRAL_IMAGE_SAVED)) {
                file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/SmartApp/" +  Constants.REFERRAL_IMAGE_NAME);
            } else {
                if(saveReferralImage(context)) {
                    sharedPrefManager.setBooleanValue(Constants.IS_REFERRAL_IMAGE_SAVED, Boolean.TRUE);
                    file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/SmartApp/" + Constants.REFERRAL_IMAGE_NAME);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
            if(saveReferralImage(context)) {
                sharedPrefManager.setBooleanValue(Constants.IS_REFERRAL_IMAGE_SAVED, Boolean.TRUE);
                file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/SmartApp/" + Constants.REFERRAL_IMAGE_NAME);
            }
        }
        return Uri.fromFile(file);
    }

    private static long getRandomTime() {
        Random r = new Random();
        long time = (long)r.nextInt(3600000);
        Log.e("delay by: ", String.valueOf(time));
        return time;
    }
}