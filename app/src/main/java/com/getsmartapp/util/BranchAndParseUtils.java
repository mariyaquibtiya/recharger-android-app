package com.getsmartapp.util;

import android.content.Context;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.lib.model.ReferrerBranchDetails;
import com.getsmartapp.lib.model.ReferrerEarnings;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.sharedPreference.BranchPrefManager;
import com.getsmartapp.sharedPreference.PushNotificationPrefManager;
import com.google.gson.Gson;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.CustomEvent;
import com.urbanairship.util.UAStringUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author bishwajeet.kumar on 17/07/15.
 */
public class BranchAndParseUtils {

    public static BranchPrefManager branchPrefManager;
    public static PushNotificationPrefManager pushPrefManager;
    private static Gson gson = new Gson();

    public static void saveBranchPrefData(Context context,ProxyLoginUser.SoResponseEntity userData) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        branchPrefManager = BranchPrefManager.getInstance(context);

        String imageUrl = AppUtils.getUserProfilePicUrl(context, userData) == null ? ApiConstants.NOIMAGEFOUND : AppUtils.getUserProfilePicUrl(context, userData);

        branchPrefManager.setCurrentBranchUserName(userData.getFirstName());
        branchPrefManager.setCurrentBranchUserIdentity(userData.getUserId());
        branchPrefManager.setCurrentUserChannel("sso" + userData.getUserId());
        branchPrefManager.setCurrentUserImageUrl(imageUrl);
        branchPrefManager.setCurrentDeviceID(AppUtils.getDeviceIDForSSO(context));

        branchPrefManager = BranchPrefManager.getInstance(context);

        //Set NamedUser on UA
        String currentNamedUser = UAStringUtil.isEmpty(branchPrefManager.getCurrentUserChannel()) ? null : branchPrefManager.getCurrentUserChannel();
        UAirship.shared().getPushManager().getNamedUser().setId(currentNamedUser);

        //Set ALIAS on UA
        String currentAlias = UAStringUtil.isEmpty(branchPrefManager.getCurrentBranchUserName()) ? null : branchPrefManager.getCurrentBranchUserName();
        UAirship.shared().getPushManager().setAlias(currentAlias);

        Branch.getInstance(context).setIdentity(branchPrefManager.getCurrentBranchUserIdentity(), new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject jsonObject, BranchError branchError) {

            }
        });

        fetchReferralEarnings(context);
    }

    public static void initiateSingUpWebHook(Context context) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        final JSONObject branchUserMetadata = new JSONObject();
        try {
            branchUserMetadata.put(ApiConstants.CURRENT_BRANCH_USER_IDENTITY, branchPrefManager.getCurrentBranchUserIdentity());
            branchUserMetadata.put(ApiConstants.CURRENT_BRANCH_USER_NAME, branchPrefManager.getCurrentBranchUserName());
            branchUserMetadata.put(ApiConstants.CURRENT_USER_CHANNEL, branchPrefManager.getCurrentUserChannel());
            branchUserMetadata.put(ApiConstants.CURRENT_USER_IMAGE_URL, branchPrefManager.getCurrentUserImageURL());
            branchUserMetadata.put(ApiConstants.CURRENT_USER_GC_CODE, branchPrefManager.getCurrentUserGCCode());
            branchUserMetadata.put(ApiConstants.CURRENT_USER_REFERRAL_CODE, branchPrefManager.getCurrentUserReferralCode());
            branchUserMetadata.put(ApiConstants.CURRENT_USER_REFERRAL_LINK, branchPrefManager.getCurrentUserReferralLink());
            branchUserMetadata.put(ApiConstants.CURRENT_DEVICE_ID, AppUtils.getDeviceIDForSSO(context));
        } catch(JSONException e) {
            e.printStackTrace();
        }
        Branch.getInstance(context).userCompletedAction("signup_success", branchUserMetadata);
    }

    public static void subscribeToPushChannelOnLogin(Context context) {
        pushPrefManager = PushNotificationPrefManager.getInstance(context);
        branchPrefManager = BranchPrefManager.getInstance(context);

        Set<String> tags = new HashSet<>();
        tags = UAirship.shared().getPushManager().getTags();

        pushPrefManager.setHasUserLogged(Boolean.TRUE);

        tags.add(branchPrefManager.getCurrentUserChannel());

        if(!branchPrefManager.getHasMadeFirstTransaction()) {
            tags.add(ApiConstants.TAG_SIGNUP_NOT_TRANSACTED);
        }

        if (!tags.contains("hasMadeFirstReferral")) {
            tags.add(ApiConstants.TAG_FIRST_REFERRAL_PENDING);
        } else {
            tags.add(ApiConstants.TAG_LAZY_REFERRER);
        }

        UAirship.shared().getPushManager().setTags(tags);
    }

    public static void subscribeToDevicePushChannel(Context context) {
        pushPrefManager = PushNotificationPrefManager.getInstance(context);

        Set<String> tags = new HashSet<>();
        tags = UAirship.shared().getPushManager().getTags();

        if(!tags.contains(AppUtils.getDeviceIDForSSO(context))) {
            tags.add(AppUtils.getDeviceIDForSSO(context));
        }

        UAirship.shared().getPushManager().setTags(tags);
    }

    public static void subscribeToPushTag(Context context, String pushTag) {
        Set<String> tags = new HashSet<>();
        tags = UAirship.shared().getPushManager().getTags();

        if(!tags.contains(pushTag)) {
            tags.add(pushTag);
        }

        UAirship.shared().getPushManager().setTags(tags);
    }

    public static void subscribeAndEventTrackForUA(Context context, String pushTag) {
        Set<String> tags = new HashSet<>();
        tags = UAirship.shared().getPushManager().getTags();

        CustomEvent event = new CustomEvent.Builder(pushTag).create();
        UAirship.shared().getAnalytics().addEvent(event);

        if(!tags.contains(pushTag)) {
            tags.add(pushTag);
        }

        UAirship.shared().getPushManager().setTags(tags);
    }

    public static void unsubscribeFromPushTag(Context context, String pushTag) {
        Set<String> tags = new HashSet<>();

        tags = UAirship.shared().getPushManager().getTags();

        try {
            if(tags.contains(pushTag)) {
                tags.remove(pushTag);
            }
            UAirship.shared().getPushManager().setTags(tags);
        } catch(Exception e) {
            Lg.e("Urban Airship", "Failed to unsubscribe from: "+ pushTag);
        }
    }

    public static void unsubscribeFromPushChannelOnLogout(Context context) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        Set<String> tags = new HashSet<>();

        UAirship.shared().getPushManager().getNamedUser().setId(null);

        tags = UAirship.shared().getPushManager().getTags();

        try {

            tags.remove(branchPrefManager.getCurrentUserChannel());

            if(!branchPrefManager.getHasMadeFirstTransaction()) {
                tags.remove(ApiConstants.TAG_SIGNUP_NOT_TRANSACTED);
            }

            UAirship.shared().getPushManager().setTags(tags);
        } catch(Exception e) {
        }
    }

    public static void subscribeToLatestAppVersion(Context context) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        String currentAppVersion = "app_version_" + AppUtils.getAppVersionName(context);
        String oldAppVersion = sharedPrefManager.getStringValue(Constants.OLD_APP_VERSION);

        if(!currentAppVersion.equals(oldAppVersion)) {
            unsubscribeFromPushTag(context, oldAppVersion);
            subscribeToPushTag(context, currentAppVersion);
            sharedPrefManager.setStringValue(Constants.OLD_APP_VERSION, currentAppVersion);
        }
    }

    public static boolean verifyTagSubscription(String tag) {
        Set<String> tags = UAirship.shared().getPushManager().getTags();
        return tags.contains(tag);
    }

    public static void processPushBeforeReferral(Context context) {
        Set<String> tags = new HashSet<>();
        tags = UAirship.shared().getPushManager().getTags();


        if(tags.contains(ApiConstants.TAG_FIRST_REFERRAL_PENDING)) {
            tags.remove(ApiConstants.TAG_FIRST_REFERRAL_PENDING);
            tags.add(ApiConstants.TAG_LAZY_REFERRER);
        }

        UAirship.shared().getPushManager().setTags(tags);
    }

    public static String getUserRegistrationData(Context context,ProxyLoginUser.SoResponseEntity userData) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        Map<String, Object> userRegistrationMap = new LinkedHashMap<>();
        Map<String, String> user = new LinkedHashMap<>();
        Map<String, String> userDevice = new LinkedHashMap<>();
        Integer isReferred;
        String referrerID;
        user.put(ApiConstants.ssoid, userData.getUserId());
        user.put(ApiConstants.FNAME, userData.getFirstName());
        user.put(ApiConstants.LNAME, userData.getLastName());
        user.put(ApiConstants.EMAIL, userData.getPrimaryEmailId());
        user.put(ApiConstants.PHONE, userData.getVerifiedMobile());
        user.put(ApiConstants.IMAGEURL, branchPrefManager.getCurrentUserImageURL());
        userDevice.put(ApiConstants.DEVICE_ID, branchPrefManager.getCurrentDeviceID());
        userDevice.put(ApiConstants.ssoid, userData.getUserId());
        userDevice.put(ApiConstants.ISCURRENTID, "y");
        isReferred = branchPrefManager.getIsReferredUser()?1:0;
        referrerID = branchPrefManager.getReferralBranchUserIdentity();
        userRegistrationMap.put("user",user);
        userRegistrationMap.put("userDevice", userDevice);
        userRegistrationMap.put(ApiConstants.ISREFERRED, isReferred);
        userRegistrationMap.put(ApiConstants.REFERREDID, referrerID);

        return gson.toJson(userRegistrationMap);
    }

    public static String getReferredData(Context context, String referralCode) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        branchPrefManager = BranchPrefManager.getInstance(context);

        Map<String, Object> userRegistrationMap = new LinkedHashMap<>();
        Map<String, String> user = new LinkedHashMap<>();

        Gson gson = new Gson();
        ProxyLoginUser.SoResponseEntity userData = gson.fromJson(sharedPrefManager.getStringValue(Constants.USER_DATA), ProxyLoginUser.SoResponseEntity.class);

        if(userData != null) {
            user.put(ApiConstants.ssoid, userData.getUserId());
            user.put(ApiConstants.email, userData.getPrimaryEmailId());

            userRegistrationMap.put("user", user);
            userRegistrationMap.put("referralCode", referralCode);
            userRegistrationMap.put("deviceId", branchPrefManager.getCurrentDeviceID());

            return gson.toJson(userRegistrationMap);
        } else {
            return null;
        }
    }

    public static void fetchReferralEarnings(final Context context) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        RestClient restClient = new RestClient(ApiConstants.BASE_REGISTRATION_ADDRESS,null);
        restClient.getApiService().getReferrerEarnings(branchPrefManager.getCurrentBranchUserIdentity(), new Callback<ReferrerEarnings>() {
            @Override
            public void success(ReferrerEarnings referrerEarnings, Response response) {
                if ("1".equals(referrerEarnings.getHeader().getStatus())) {
                    setReferralEarnings(referrerEarnings, context);
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private static void setReferralEarnings(ReferrerEarnings referralEarnings, Context context) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        branchPrefManager.setReferralRedeemableAmount(String.valueOf(referralEarnings.getBody().getAmountRedeemable()));
    }

    public static void fetchReferrerBranchDetails(final Context context) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        RestClient restClient = new RestClient(ApiConstants.BASE_REGISTRATION_ADDRESS,null);
        restClient.getApiService().getReferrerBranchDetails(branchPrefManager.getCurrentBranchUserIdentity(), new Callback<ReferrerBranchDetails>() {
            @Override
            public void success(ReferrerBranchDetails referrerBranchDetails, Response response) {
                if ("1".equals(referrerBranchDetails.getHeader().getStatus())) {
                    setReferrerBranchDetails(referrerBranchDetails, context);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Lg.e("Ref", error.getMessage());
            }
        });
    }

    private static void setReferrerBranchDetails(ReferrerBranchDetails referrerBranchDetails, Context context) {
        branchPrefManager = BranchPrefManager.getInstance(context);
        branchPrefManager.setReferralBranchUserName(String.valueOf(referrerBranchDetails.getBody().getCurrent_branch_user_name()));
        branchPrefManager.setReferralBranchUserIdentity(String.valueOf(referrerBranchDetails.getBody().getCurrent_branch_user_identity()));
        branchPrefManager.setReferralUserImageURL(String.valueOf(referrerBranchDetails.getBody().getCurrent_user_image_url()));
        branchPrefManager.setReferralChannel(String.valueOf(referrerBranchDetails.getBody().getCurrent_user_channel()));
        if(String.valueOf(referrerBranchDetails.getBody().getCurrent_branch_user_name())!=null && String.valueOf(referrerBranchDetails.getBody().getCurrent_branch_user_name())!=""){
            branchPrefManager.setIsReferredUser(Boolean.TRUE);
        }
    }
}
