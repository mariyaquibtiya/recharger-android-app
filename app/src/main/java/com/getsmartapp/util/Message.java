package com.getsmartapp.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

/**
 * @author Nitesh.Verma on 07-05-2015.
 */
public class Message {
    public static void message(View view,String message)
    {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }
    public static void toastMessage(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}