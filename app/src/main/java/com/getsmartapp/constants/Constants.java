package com.getsmartapp.constants;

/**
 * @author maria.quibtiya on 01/07/16.
 */
public class Constants {

    public static final String APP_STARTED_FIRST_TIME = "APP_STARTED_FIRST_TIME";

    public static final String ENABLED_SMARTSAVE = "EnableSmartsave";
    public static final String APP_UPDATE_SOFT_UPDATE_DIALOG_SHOWN_LAST_TIME = "APP_UPDATE_SOFT_UPDATE_DIALOG_SHOWN_LAST_TIME";
    public static final String APP_UPDATE_URL_RESPONSE = "APP_UPDATE_URL_RESPONSE";
    public static final String APP_OPEN_COUNT = "app_open_count";
    public static final int DEFAULT_TIMEOUT_IN_MILLISECONDS = 20000;
    public static final String APP_UDPATE_CHECK_TIME = "APP_UDPATE_CHECK_TIME";

    public static final String EXTRA_CALL_DETAILS = "CallDetailsList";


    public static final String ISREFERSKIPCLICKED = "isReferSkipClicked";
    public static final String SHARED_PREFERENCE_SHOW_NOTIFICATION = "SHARED_PREFERENCE_SHOW_NOTIFICATION";
    public static final String CHECK_HIJACK = "check_hijack";

    // For 1st sim
    public static final String ON_BOARDING_MOBILE_NUMBER = "onboarding_number";
    public static final String ON_BOARDING_OPERATOR_NAME = "onboarding_operator_name";
    public static final String ON_BOARDING_PROVIDER_ID = "onboarding_provider_id";
    public static final String ON_BOARDING_PREFERRED_TYPE = "onboarding_preferred_type";
    public static final String ON_BOARDING_SIM_TYPE = "onboarding_sim_type";
    public static final String ON_BOARDING_CIRCLE_ID = "onboarding_circle_id";
    public static final String ON_BOARDING_DATA_LIMIT = "onboarding_data_limit";
    public static final String ON_BOARDING_BILL_DATE = "onboarding_bill_date";
    public static final String ON_BOARDING_CIRCLE = "onboarding_circle";
    public static final String ON_BOARDING_BILL_START_DATE_TIME_MILLIS = "onboarding_bill_start_date_time_millis";
    public static final String ON_BOARDING_BILL_END_DATE_TIME_MILLIS = "onboarding_bill_end_date_time_millis";
    public static final String ON_BOARDING_BILL_DAYS = "onboarding_bill_days";


    // For 2nd sim
    public static final String ON_BOARDING_MOBILE_NUMBER_2 = "onboarding_number_2";
    public static final String ON_BOARDING_OPERATOR_NAME_2 = "onboarding_operator_name_2";
    public static final String ON_BOARDING_PROVIDER_ID_2 = "onboarding_provider_id_2";
    public static final String ON_BOARDING_PREFERRED_TYPE_2 = "onboarding_preferred_type_2";
    public static final String ON_BOARDING_SIM_TYPE_2 = "onboarding_sim_type_2";
    public static final String ON_BOARDING_CIRCLE_ID_2 = "onboarding_circle_id_2";
    public static final String ON_BOARDING_DATA_LIMIT_2 = "onboarding_data_limit_2";
    public static final String ON_BOARDING_BILL_DATE_2 = "onboarding_bill_date_2";
    public static final String ON_BOARDING_CIRCLE_2 = "onboarding_circle_2";


    public static final String SMS_SERVICE_LAUNCHED = "sms_service_launched";
    public static final String ON_BOARDING_NUMBER_CHANGED = "on_boarding_number_changed";
    public static final String ON_BOARDING_TIME = "onboarding_time";
    public static final String USER_ON_BOARDED_SUCCESSFULLY = "onboarding_successfully";
    public static final String FETCH_FRESH_RECOMMENDATION = "fetch_fresh_recommendation";
    public static final String LAST_RECO_CALL_TIMESTAMP = "last_reco_call_timestamp";
    public static final String DATA_EXISTS_FLAG = "previous_data_exists";
    public static final String ISBOARDINGTUTORIALSHOWN = "isboardingtutorialshown";
}
