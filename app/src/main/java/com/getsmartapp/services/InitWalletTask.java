package com.getsmartapp.services;

import android.content.Context;
import android.os.AsyncTask;

import com.getsmartapp.R;
import com.getsmartapp.interfaces.InitWalletResponseCallback;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.model.InitRechargeModel;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.util.AppUtils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author shalakha.gupta on 03/02/16.
 */
public class InitWalletTask extends AsyncTask<Context, Void, InitRechargeModel> {


    private Context context;
    InitWalletResponseCallback listner;
    double pgAmount;

    public InitWalletTask(Context context, InitWalletResponseCallback listner, double pgAmount) {
        this.context = context;
        this.listner = listner;
        this.pgAmount = pgAmount;
    }

    @Override
    protected InitRechargeModel doInBackground(Context... params) {
        return doBackgroungTask();
    }

    @Override
    protected void onPostExecute(InitRechargeModel initRechargeModel) {
        super.onPostExecute(initRechargeModel);
        try {
            if (initRechargeModel != null) {
                String status = initRechargeModel.getHeader().getStatus();
                if (status.equalsIgnoreCase("1")) {
                    InitRechargeModel.BodyEntity bodyObj = initRechargeModel.getBody();
                    if (bodyObj == null) {
                        if (listner != null) {
                            listner.onInitWalletFailure(context.getString(R.string.wallet_server_error));
                        }
                    } else if (listner != null) {
                        listner.onInitWalletSuccess(initRechargeModel);

                    }
                } else if (status.equalsIgnoreCase("0")) {
                    InitRechargeModel.HeaderEntity.ErrorsEntity errorList = initRechargeModel.getHeader().getErrors();
                    if (errorList != null && errorList.getErrorList() != null && errorList.getErrorList().size() > 0) {
                        String errorMsg = errorList.getErrorList().get(0).getMessage();
                        if (listner != null) {
                            listner.onInitWalletFailure(errorMsg);
                        }
                    } else {
                        if (listner != null) {
                            listner.onInitWalletFailure(context.getString(R.string.wallet_server_error));
                        }
                    }
                }
            } else {
                if (listner != null) {
                    listner.onInitWalletFailure(context.getString(R.string.wallet_server_error));
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
            if (listner != null) {
                listner.onInitWalletFailure(context.getString(R.string.wallet_server_error));
            }
        }
    }


    private InitRechargeModel doBackgroungTask() {
        String user_data = AppUtils.getLoggedInUser(context.getApplicationContext());
        if (user_data != null) { //user is logged in
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(user_data, ProxyLoginUser.SoResponseEntity.class);
            if (user != null) {
                long timestamp = System.currentTimeMillis();
                String transcionHash = AppUtils.calculateShaHash(context, user, String.valueOf
                                (pgAmount),
                        timestamp);

                String urlStr = ApiConstants.BASE_RECHARGER_ADDRESS + "/wallet/loadPGVoucher";

                String ssoId = user.getUserId();
                String email = user.getPrimaryEmailId();
                String deviceId = AppUtils.getDeviceIDForSSO(context);
                String ipAddress = AppUtils.getIPAddress(true);
                String firstName = user.getFirstName();
                String phoneNumber = user.getVerifiedMobile();

                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setReadTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);
                    httpURLConnection.setConnectTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);
                    httpURLConnection.setRequestMethod("POST");
                    String requestBody = "{\n" +
                            "                \"emailId\":\"" + email + "\",\n" +
                            "                \"ssoId\":\"" + ssoId + "\",\n" +
                            "                \"deviceId\":\"" + deviceId + "\",\n" +
                            "                \"ipAddress\":\"" + ipAddress + "\",\n" +
                            "                \"timestamp\" : \"" + timestamp + "\",\n" +
                            "                \"pgAmount\" : \"" + pgAmount + "\",\n" +
                            "                \"firstName\":\"" + firstName + "\",\n" +
                            "                \"phoneNumber\" : \"" + phoneNumber + "\"\n" +
                            "}\n";


                    httpURLConnection.addRequestProperty("transactionHash", transcionHash);
                    httpURLConnection.addRequestProperty("content-type", "application/json");
                    httpURLConnection.addRequestProperty("version", ApiConstants.backend_version);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(requestBody.getBytes());
                    outputStream.close();

                    BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
                    StringBuilder sb = new StringBuilder();
                    String output;
                    while ((output = br.readLine()) != null) {
                        sb.append(output);
                    }

                    Gson gson = new Gson();
                    InitRechargeModel model = gson.fromJson(sb.toString(), InitRechargeModel.class);
                    return model;
                } catch (MalformedURLException e) {
                    if (listner != null) {
                        listner.onInitWalletFailure(context.getString(R.string.server_error));
                    }
                } catch (IOException e) {
                    if (listner != null) {
                        listner.onInitWalletFailure(context.getString(R.string.server_error));
                    }
                }
            }
        }
        return null;
    }
}
