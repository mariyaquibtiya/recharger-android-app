package com.getsmartapp.services;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.LocationUtils;/*
import com.getsmartapp.wifimain.TrackLocSingleton;
import com.getsmartapp.wifimain.UserConstants;
import com.getsmartapp.wifimain.nearbyplaceshttp.NearbyPlacesFoursquare;*/

import java.util.Calendar;
import java.util.Date;


/**
 * @author nitesh on 25/5/15.
 */
public class LocationService extends Service {

    private LocationManager locationManager;
    private MyLocationListener listener;
    private SharedPreferences sharedPreferences;
    private SharedPrefManager sharedPrefManager;
    private long mLastUpdateTime;
    //private Location mLastLocation;
    private final long MINIMUM_TIME_FOR_LOC_UPDATE = 10 * 60 * 1000; // in msec
    //private final int MINIMUM_DISTANCE_FOR_LOC_UPDATE = /*50*/0; // in metres

    private final long WIFI_PUSH_RECEIVED_TIME_DIFF = 2 * 60 * 60 * 1000;
    private final int WIFI_NUM_OF_PUSHES = 2;

    private static boolean IS_LOCATION_ENABLED = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Mariya", "Location Service started : onCreate() called");
        sharedPrefManager = new SharedPrefManager(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(local_broadcast, new IntentFilter("update_location"));

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Log.e("stop service", "done");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(local_broadcast);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationManager.removeUpdates(listener);
        sharedPrefManager.setIntValue(Constants.LOCATION_SERVICE_RUNNING, 0);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreferences = getSharedPreferences("Location_Pref", MODE_PRIVATE);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return START_STICKY;
            }
        }

        Log.d("Mariya", "Loc Service_ onStartCmd");

        if (!IS_LOCATION_ENABLED && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MINIMUM_TIME_FOR_LOC_UPDATE,
                    0, listener);
            Log.d("Mariya", "LocationService requestLocUpdates");
            IS_LOCATION_ENABLED = true;
        }

        sharedPrefManager.setIntValue(Constants.LOCATION_SERVICE_RUNNING, 1);
        return START_STICKY;

    }

    private BroadcastReceiver local_broadcast = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (!IS_LOCATION_ENABLED && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MINIMUM_TIME_FOR_LOC_UPDATE,
                        0, listener);
                Log.d("Mariya","LocationService requestLocUpdates");
                IS_LOCATION_ENABLED = true;
            }
        }
    };

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            if (!TextUtils.isEmpty(latitude +"") && latitude != 0 && !TextUtils.isEmpty(longitude +"") && longitude != 0) {
                sharedPreferences.edit().putFloat("latitude", (float) latitude).apply();
                sharedPreferences.edit().putFloat("longitude", (float) longitude).apply();

                LocationUtils.getInstance(getApplicationContext()).setAddressComponents();

                if ((location.getTime() - mLastUpdateTime) >= MINIMUM_TIME_FOR_LOC_UPDATE) {
                    /*TrackLocSingleton obj = TrackLocSingleton.getInstance();
                    obj.setTimeDifference(location.getTime() - mLastUpdateTime);

                    obj.setCurrentLoc(location);
                    mLastUpdateTime = location.getTime();

                    boolean isUnrestrictedTime = isUnrestrictedTime();

                    if (isUnrestrictedTime && (obj.getNumOfPushes(getApplicationContext()) < WIFI_NUM_OF_PUSHES)) {
                        if (obj.getShouldStartWifiLocService(getApplicationContext())
                                && (mLastUpdateTime - obj.getNotificationReceivedTime(getApplicationContext())
                                >= WIFI_PUSH_RECEIVED_TIME_DIFF)) {

                            Intent intent = new Intent(LocationService.this, NearbyPlacesFoursquare.class);
                            intent.putExtra(UserConstants.EXTRA_LATITUDE, location.getLatitude());
                            intent.putExtra(UserConstants.EXTRA_LONGITUDE, location.getLongitude());
                            intent.putExtra(UserConstants.EXTRA_PLACE_TYPE, "establishment|airport|amusement_park|art_gallery|bar|beauty_salon|" +
                                    "book_store|bowling_alley|bus_station|cafe|car_rental|casino|library|lodging|movie_theater|" +
                                    "night_club|restaurant|shopping_mall|spa|train_station|transit_station|university");
                            intent.putExtra(UserConstants.EXTRA_RADIUS, Math.max(200, (int) (2 * location.getAccuracy())));

                            startService(intent);

                        } else obj.setLastLocation(null);
                    } else obj.setLastLocation(null);*/
                }
            }
        }


        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {
            if(s.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER))
                IS_LOCATION_ENABLED = false;

        }
    }

    private boolean isUnrestrictedTime() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Date currentTime = calendar.getTime();

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY, 22);
        calendar1.set(Calendar.MINUTE, 0);
        calendar1.set(Calendar.SECOND, 0);
        Date nightTime = calendar1.getTime();

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.HOUR_OF_DAY, 8);
        calendar2.set(Calendar.MINUTE, 0);
        calendar2.set(Calendar.SECOND, 0);
        Date mornTime = calendar2.getTime();

        if (currentTime.after(mornTime) && currentTime.before(nightTime))
            return true;
        else {
            //TrackLocSingleton.getInstance().setNumOfPushes(getApplicationContext(), 0);
            return false;
        }
    }

}
