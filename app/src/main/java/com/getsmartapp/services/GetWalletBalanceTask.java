package com.getsmartapp.services;

import android.content.Context;
import android.os.AsyncTask;

import com.getsmartapp.R;
import com.getsmartapp.interfaces.WalletBalanceResponseCallback;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.model.GetWalletBalanceModel;
import com.getsmartapp.util.AppUtils;
import com.getsmartapp.util.Lg;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author shalakha.gupta on 07/01/16.
 */
public class GetWalletBalanceTask extends AsyncTask<Context, Void, GetWalletBalanceModel> {

    private Context context;
    private WalletBalanceResponseCallback listner;
    private String errorMsg;

    public GetWalletBalanceTask(Context context, WalletBalanceResponseCallback listner) {
        this.context = context;
        this.listner = listner;
    }

    @Override
    protected GetWalletBalanceModel doInBackground(Context... params) {
        return getWalletBalance();
    }

    @Override
    protected void onPostExecute(GetWalletBalanceModel getWalletBalanceModelModel) {
        super.onPostExecute(getWalletBalanceModelModel);
        boolean isServerError=false;

        try {
            if (getWalletBalanceModelModel != null && getWalletBalanceModelModel.getHeader() != null) {
                String headerStatus = getWalletBalanceModelModel.getHeader().getStatus();
                if (!AppUtils.isStringNullEmpty(headerStatus)) {
                    if (headerStatus.equalsIgnoreCase("1")) {
                        GetWalletBalanceModel.BodyEntity body = getWalletBalanceModelModel.getBody();
                        if (body != null) {
                            if (body.getStatusCode() == 2000) {
                                GetWalletBalanceModel.BodyEntity.DataEntity dataEntity = body.getDataEntity();
                                if (dataEntity != null) {
                                    int amount = dataEntity.getBalanceAmount();
                                    AppUtils.setWalletAmount(amount, context.getApplicationContext());
                                    AppUtils.setMonthlyCreditedAmount(dataEntity.getMonthlyCreditedAmount(), context.getApplicationContext());
                                    AppUtils.setMonthlyDebitedAmount(dataEntity.getMonthlyDebitedAmount(), context.getApplicationContext());
                                    GetWalletBalanceModel.BodyEntity.WalletStatus walletStatusEnitity = body.getWalletStatus();
                                    if (walletStatusEnitity != null) {
                                        int isWalletOn = walletStatusEnitity.getStatusCode();
                                        AppUtils.setWalletON(isWalletOn, context.getApplicationContext());
                                    }
                                    if (listner != null)
                                        listner.onWalletSuccess(getWalletBalanceModelModel);
                                }
                                else{
                                    isServerError=true;
                                }
                            }
                            else{
                                isServerError=true;
                            }
                        }
                        else {
                            isServerError=true;
                        }
                    } else {
                        if (getWalletBalanceModelModel.getHeader().getErrors() != null) {
                            errorMsg = getWalletBalanceModelModel.getHeader().getErrors().getErrorList().get(0).getMessage();
                            if (listner != null)
                                listner.onWalletFailure(errorMsg);
                        }
                    }

                }
                else {
                    isServerError=true;
                }
            } else {
                isServerError=true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            isServerError=true;
        }
        if(isServerError){
            if (listner != null) {
                String errorMsg = "We are really sorry! Wallet is temporarily down. We will be back soon!";
                listner.onWalletFailure(errorMsg);
            }
        }
    }

    private GetWalletBalanceModel getWalletBalance() {
        String user_data = AppUtils.getLoggedInUser(context.getApplicationContext());
        if (user_data != null) { //user is logged in
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(user_data, ProxyLoginUser.SoResponseEntity.class);
            if (user != null) {
                long timestamp = System.currentTimeMillis();
                String transcionHash = AppUtils.calculateShaHash(context, user, "-1.0", timestamp);
                String urlStr = ApiConstants.BASE_RECHARGER_ADDRESS + "/wallet/balance";
                String ssoId = user.getUserId();
                String email = user.getPrimaryEmailId();
                String deviceId = AppUtils.getDeviceIDForSSO(context);
                String ipAddress = AppUtils.getIPAddress(true);
                String isMonthlyCredit = "True";
                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setReadTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);
                    httpURLConnection.setConnectTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);

                    httpURLConnection.setRequestMethod("POST");
                    String requestBody = "{\n" +
                            "                \"email\":\"" + email + "\",\n" +
                            "                \"isMonthlyCredit\":\"" + isMonthlyCredit + "\",\n" +
                            "                \"ssoId\":\"" + ssoId + "\",\n" +
                            "                \"deviceId\":\"" + deviceId + "\",\n" +
                            "                \"ipAddress\":\"" + ipAddress + "\",\n" +
                            "                \"timestamp\" : \"" + timestamp + "\"\n" +
                            "}\n";

                    httpURLConnection.addRequestProperty("transactionHash", transcionHash);
                    httpURLConnection.addRequestProperty("content-type", "application/json");

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(requestBody.getBytes());
                    outputStream.close();

                    BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
                    StringBuilder sb = new StringBuilder();
                    String output;
                    while ((output = br.readLine()) != null) {
                        sb.append(output);
                    }
                    Lg.w("Wallet Balance ", sb.toString());
                    Gson gson = new Gson();
                    return gson.fromJson(sb.toString(), GetWalletBalanceModel.class);
                } catch (MalformedURLException e) {
                    errorMsg = context.getString(R.string.server_error);
                } catch (IOException e) {
                    errorMsg = context.getString(R.string.server_error);
                }
            }
        }
        return null;
    }
}
