package com.getsmartapp.services;

import android.content.Context;

import com.getsmartapp.R;
import com.getsmartapp.interfaces.ApplyGcCallBack;
import com.getsmartapp.lib.model.PromoCodeModel1;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.lib.model.RechargeDetails;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
//import com.getsmartapp.screens.PayUPaymentActivity;
import com.getsmartapp.util.AppUtils;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author shalakha.gupta on 29/02/16.
 */
public class ApplyGCTask /*extends PayUPaymentActivity*/{

    private Context context;
    private ApplyGcCallBack listner;
    private String gcId;
    private ArrayList<RechargeDetails> rechargeItems;
    private ProxyLoginUser.SoResponseEntity loggedInUser;
    private boolean doGCValidationOnServer;

    public ApplyGCTask(Context context, String gcId, ArrayList<RechargeDetails> rechargeItems, boolean doGCValidationOnServer){
        this.context = context;
        this.gcId = gcId;
        this.rechargeItems = rechargeItems;
        loggedInUser = AppUtils.getLoggedInSSODetails(context);
        this.doGCValidationOnServer=doGCValidationOnServer;
    }

    public void setAppLyGcCallback(ApplyGcCallBack listner){
        this.listner=listner;
    }

    public void getURLForGC() {
        final String  orderAmt;
        StringBuilder product = new StringBuilder();// seq comma seperated
        StringBuilder sumAmt = new StringBuilder();// item payable comma seperated
        StringBuilder catalog = new StringBuilder();// 0
        StringBuilder category = new StringBuilder();//(prepaid/postpaid) comma seperated

        if (gcId.contains(" "))
            gcId = gcId.replace(" ", "");

        double totalAmount = 0.0;
        double itemDiscount = 0.0;
        for (RechargeDetails rechargeDetails : rechargeItems) {
            if (product.length() < 1)
                product = product.append(rechargeDetails.getSeqId() + 1);
            else
                product = product.append(",").append(rechargeDetails.getSeqId() + 1);

            if (category.length() < 1)
                category = category.append(rechargeDetails.getSType());
            else
                category = category.append(",").append(rechargeDetails.getSType());

            double itemPayableAmount = rechargeDetails.getItemAmount() - itemDiscount;
            totalAmount = totalAmount + itemPayableAmount;

            if (sumAmt.length() < 1)
                sumAmt = sumAmt.append(itemPayableAmount);
            else
                sumAmt = sumAmt.append(",").append(itemPayableAmount);

            if (catalog.length() < 1)
                catalog = catalog.append("1");
            else
                catalog = catalog.append(",").append("1");
        }
        orderAmt = totalAmount + "";
        String userId = loggedInUser.getPrimaryEmailId();
        String altId = loggedInUser.getVerifiedMobile();
        String orderTime = System.currentTimeMillis() + "";
        String deviceId = AppUtils.getDeviceIDForSSO(context);

//        (<userId>|<altId>|<gcId>|<gcCode>|<amount>|<orderAmount>|<salt>)


        StringBuilder sharInput = new StringBuilder();
        sharInput.append(userId).append("|");
        sharInput.append(altId).append("|");
        sharInput.append(gcId).append("|");
        sharInput.append(orderAmt).append("|");
        sharInput.append(Constants.encyptionSalt);

        String shaDisgesStr = AppUtils.getShaEncryptedData(sharInput.toString());
        RestClient restClientGC = new RestClient(ApiConstants.BASE_RECHARGER_ADDRESS,null);
        restClientGC.getApiService().verifyPromocode(userId, gcId, orderAmt, "recharger", product.toString(),
                orderTime, sumAmt.toString(), catalog.toString(), category.toString(), altId,deviceId,doGCValidationOnServer,
                shaDisgesStr,new Callback<PromoCodeModel1>() {
                    @Override
                    public void success(PromoCodeModel1 model1, Response response) {
                        boolean isSuccess=false;
                        try {
                            if (model1 != null && model1.getHeader() != null) {
                                String headerStatus = model1.getHeader().getStatus();
                                if (!AppUtils.isStringNullEmpty(headerStatus) ) {
                                    if (headerStatus.equalsIgnoreCase("1")) {
                                        PromoCodeModel1.BodyEntity body = model1.getBody();
                                        if (body != null){
                                            if ( body.getValidationResponse() != null) {
                                                PromoCodeModel1.BodyEntity.ValidationResponseEntity responseModel = body.getValidationResponse();
                                                if (responseModel != null && !AppUtils.isStringNullEmpty(responseModel.getStatus())) {
//                                                        sha512Hex(<userId>|<merchant>|<orderAmount>|<totalAmount>|<amount>|<status>|<gcMergable>|<gcPartialRedeem>|<error>|
//                                                        <errorCode>|<offer>|<userLock>|<category>|<type>|<paid>|<firstUse>|<reason>|<salt>)

                                                    if(listner!=null){
                                                        isSuccess=true;
                                                        listner.onGCSuccess(model1);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if(!isSuccess){
                            if(listner!=null){
                                listner.onGCFailure(context.getString(R.string.server_error));
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(listner!=null){
                            listner.onGCFailure(context.getString(R.string.server_error));
                        }
                    }
                });
    }

}
