package com.getsmartapp.services;

import android.content.Context;
import android.os.AsyncTask;

import com.getsmartapp.R;
import com.getsmartapp.interfaces.RefundToBankResponseCallback;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.model.RefundToBankModel;
import com.getsmartapp.util.AppUtils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author shalakha.gupta on 04/02/16.
 */
public class RefundToBankTask extends AsyncTask<Context, Void, RefundToBankModel> {

    private Context context;
    RefundToBankResponseCallback listner;
    double refundAmount;
    String walletRefNo;
    String walletOrderId;

    public RefundToBankTask(Context context, RefundToBankResponseCallback listner,
                            double refundAmount, String walletRefNo, String walletOrderId) {
        this.context = context;
        this.listner = listner;
        this.refundAmount = refundAmount;
        this.walletRefNo = walletRefNo;
        this.walletOrderId = walletOrderId;
    }

    @Override
    protected RefundToBankModel doInBackground(Context... params) {
        return getRefundResponse();
    }

    @Override
    protected void onPostExecute(RefundToBankModel gcModel) {
        super.onPostExecute(gcModel);
        boolean isServerError=false;
        try {
            if (gcModel != null && gcModel.getHeader() != null) {
                String headerStatus = gcModel.getHeader().getStatus();
                if (!AppUtils.isStringNullEmpty(headerStatus)) {
                    if (headerStatus.equalsIgnoreCase("1")) {
                        RefundToBankModel.BodyEntity body = gcModel.getBody();
                        if (body != null) {
                            if (body.getStatusCode() == 2000) {
                                RefundToBankModel.BodyEntity.DataEntity dataEntity = body.getData();
                                if (dataEntity != null) {
                                    if (listner != null)
                                        listner.onRefundSuccess(dataEntity);
                                }
                            }
                            else {
                                isServerError=true;
                            }
                        } else {
                            isServerError=true;
                        }
                    }
                    else {
                        if (gcModel.getHeader().getErrors() != null) {
                            String error = gcModel.getHeader().getErrors().getErrorList().get(0).getMessage();
                            if(!AppUtils.isStringNullEmpty(error)) {
                                if (listner != null)
                                    listner.onRefundFailure(error);
                            }
                            else {
                                isServerError =true;
                            }
                        }
                        else {
                            isServerError =true;
                        }
                    }
                }
                else{
                    isServerError =true;
                }

            }
            else {
                isServerError=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            isServerError=true;
        }

        if(isServerError){
            if (listner != null) {
                String error = context.getString(R.string.server_error);
                listner.onRefundFailure(error);
            }
        }
    }


    private RefundToBankModel getRefundResponse() {
        String user_data = AppUtils.getLoggedInUser(context.getApplicationContext());
        if (user_data != null) { //user is logged in
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(user_data, ProxyLoginUser.SoResponseEntity.class);
            if (user != null) {
                long timestamp = System.currentTimeMillis();
                String transcionHash = AppUtils.calculateShaHash(context, user, String
                        .valueOf(refundAmount), timestamp);

                String urlStr = ApiConstants.BASE_RECHARGER_ADDRESS + "/wallet/refundToBank";
                String ssoId = user.getUserId();
                String email = user.getPrimaryEmailId();
                String deviceId = AppUtils.getDeviceIDForSSO(context);
                String ipAddress = AppUtils.getIPAddress(true);
                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setReadTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);
                    httpURLConnection.setConnectTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);

                    httpURLConnection.setRequestMethod("POST");

                String requestBody = "{\n" +
                        "                \"email\":\"" + email + "\",\n" +
                        "                \"ssoId\":\"" + ssoId + "\",\n" +
                        "                \"deviceId\":\"" + deviceId + "\",\n" +
                        "                \"ipAddress\":\"" + ipAddress + "\",\n" +
                        "                \"timestamp\":\"" + timestamp + "\",\n" +
                        "                \"walletRefNo\":\"" + walletRefNo + "\",\n" +
                        "                \"orderRefNumber\":\"" + walletOrderId + "\",\n" +
                        "                \"amount\" : \"" + refundAmount + "\"\n" +
                        "}\n";

                httpURLConnection.addRequestProperty("transactionHash", transcionHash);
                httpURLConnection.addRequestProperty("content-type", "application/json");

                OutputStream outputStream = httpURLConnection.getOutputStream();
                outputStream.write(requestBody.getBytes());
                outputStream.close();

                BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
                StringBuilder sb = new StringBuilder();
                String output;
                while ((output = br.readLine()) != null) {
                    sb.append(output);
                }

                Gson gson = new Gson();
                    RefundToBankModel model = gson.fromJson(sb.toString(), RefundToBankModel.class);
                return model;
            } catch (MalformedURLException e) {
                if (listner != null) {
                    listner.onRefundFailure(context.getString(R.string.server_error));
                }
            } catch (IOException e) {
                if (listner != null) {
                    listner.onRefundFailure(context.getString(R.string.server_error));
                }
            }
        }
    }
    return null;
}

}
