package com.getsmartapp.services;

import android.content.Context;
import android.os.AsyncTask;

import com.getsmartapp.R;
import com.getsmartapp.interfaces.WalletGiftCouponResponseCallback;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.model.LoadMoneyGCModel;
import com.getsmartapp.util.AppUtils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author shalakha.gupta on 03/02/16.
 */
public class GetGiftCouponTask extends AsyncTask<Context, Void, LoadMoneyGCModel> {

    private Context context;
    private WalletGiftCouponResponseCallback listner;
    private String gcId;

    public GetGiftCouponTask(Context context, WalletGiftCouponResponseCallback listner, String gcId) {
        this.context = context;
        this.listner = listner;
        this.gcId = gcId;
    }

    @Override
    protected LoadMoneyGCModel doInBackground(Context... params) {
        return getCoupnCodeResponse();
    }

    @Override
    protected void onPostExecute(LoadMoneyGCModel gcModel) {
        super.onPostExecute(gcModel);
        boolean isServerError=false;
        try {
            if (gcModel != null && gcModel.getHeader() != null) {
                String headerStatus = gcModel.getHeader().getStatus();
                if (!AppUtils.isStringNullEmpty(headerStatus)) {
                    if (headerStatus.equalsIgnoreCase("1")) {
                        LoadMoneyGCModel.BodyEntity body = gcModel.getBody();
                        if (body != null) {
                            if (body.getStatusCode() == 2000) {
                                LoadMoneyGCModel.BodyEntity.DataEntity dataEntity = body.getDataEntity();
                                if (dataEntity != null) {
                                    try {
                                        int amount = (int) dataEntity.getBalanceAmount();
                                        AppUtils.setWalletAmount(amount, context.getApplicationContext());
                                    } catch (Exception e) {
                                        AppUtils.setWalletAmount(0, context.getApplicationContext());
                                    }
                                    if (listner != null)
                                        listner.onGiftCouponSuccess(dataEntity.getAmount());
                                }
                            }
                        }
                        else{
                            isServerError=true;
                        }
                    } else if (headerStatus.equalsIgnoreCase("0")) {
                        if (gcModel.getHeader().getErrors() != null) {
                            if (listner != null) {
                                String error = gcModel.getHeader().getErrors().getErrorList().get(0).getMessage();
                                if (AppUtils.isStringNullEmpty(error))
                                    error = context.getString(R.string.server_error);

                                listner.onGiftCouponFailure(error);
                            }
                        }
                    } else {
                        isServerError=true;
                    }
                }
                else {
                    isServerError=true;
                }
            } else {
                isServerError=true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            isServerError=true;
        }
        if(isServerError){
            if (listner != null) {
                String error = context.getString(R.string.server_error);
                listner.onGiftCouponFailure(error);
            }
        }
    }


    private LoadMoneyGCModel getCoupnCodeResponse() {
        String user_data = AppUtils.getLoggedInUser(context.getApplicationContext());
        if (user_data != null) { //user is logged in
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(user_data, ProxyLoginUser.SoResponseEntity.class);
            if (user != null) {
                long timestamp = System.currentTimeMillis();
                String transcionHash = AppUtils.calculateShaHash(context, user, "-1.0", timestamp);
//                transcionHash = "64f79eac9ba84079f98ed6f8a37c6c34768007a176ff03d345c491d7bdd21ca57d493082bc2bf8a825ac254de059fbc257d02e6149d7c5d9537e67e790ba189b";

                String urlStr = ApiConstants.BASE_RECHARGER_ADDRESS + "/wallet/loadGCVoucher";
                String ssoId = user.getUserId();
                String email = user.getPrimaryEmailId();
                String deviceId = AppUtils.getDeviceIDForSSO(context);
                String ipAddress = AppUtils.getIPAddress(true);
                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setReadTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);
                    httpURLConnection.setConnectTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS);

                    httpURLConnection.setRequestMethod("POST");
                    String requestBody = "{\n" +
                            "                \"emailId\":\"" + email + "\",\n" +
                            "                \"ssoId\":\"" + ssoId + "\",\n" +
                            "                \"deviceId\":\"" + deviceId + "\",\n" +
                            "                \"ipAddress\":\"" + ipAddress + "\",\n" +
                            "                \"timestamp\":\"" + timestamp + "\",\n" +
                            "                \"gcId\" : \"" + gcId + "\"\n" +
                            "}\n";

//                    Log.w("requestBody", requestBody);
                    httpURLConnection.addRequestProperty("transactionHash", transcionHash);
                    httpURLConnection.addRequestProperty("content-type", "application/json");

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(requestBody.getBytes());
                    outputStream.close();

                    BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
                    StringBuilder sb = new StringBuilder();
                    String output;
                    while ((output = br.readLine()) != null) {
                        sb.append(output);
                    }
//                    Log.e("PeeyushKS", httpURLConnection.getResponseCode() + " :Response: " + httpURLConnection.getResponseMessage());
//                    Log.e("PeeyushKS GC RESPONSE", "Body: " + sb.toString());

                    Gson gson = new Gson();
                    LoadMoneyGCModel model = gson.fromJson(sb.toString(), LoadMoneyGCModel.class);
                    return model;
                } catch (MalformedURLException e) {
                    if (listner != null) {
                        listner.onGiftCouponFailure(context.getString(R.string.server_error));
                    }

//                    Log.e("PeeyushKS", "MalformedURLException: " + getClass().getSimpleName());
                } catch (IOException e) {
                    if (listner != null) {
                        listner.onGiftCouponFailure(context.getString(R.string.server_error));
                    }
//                    Log.e("PeeyushKS", "IOException: " + getClass().getSimpleName());
                }
            }
        }
        return null;
    }

}
