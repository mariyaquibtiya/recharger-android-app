package com.getsmartapp.model;

/**
 * @author shalakha.gupta on 21/01/16.
 */
public class GetBalanceReqBody {

    String email;
    String ssoId;
    String deviceId;
    String ipAddress;
    String timestamp;

    public GetBalanceReqBody(){

    }

    public GetBalanceReqBody( String email,
        String ssoId,
        String deviceId,
        String ipAddress,
                              String timestamp){
        this.email=email;
        this.ssoId=ssoId;
        this.deviceId=deviceId;
        this.ipAddress =ipAddress;
        this.timestamp = timestamp;
    }

}
