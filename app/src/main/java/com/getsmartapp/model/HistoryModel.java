package com.getsmartapp.model;

import java.util.List;

/**
 * @author shalakha.gupta on 04/02/16.
 */
public class HistoryModel {


    /**
     * status : SUCCESS
     * statusCode : 2000
     * message : SUCCESS
     * description : null
     * data : [{"type":"Order","createdOn":"02/03/2016","amount":500,"walletAmount":150,"recordType":"A","paymentInfo":null,"expiryDate":null,"paymentRefNumber":null,"walletRefNumber":67,"walletTxnDetailsId":"219","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":113,"orderItems":[{"type":"Topup","createdOn":"02/03/2016","amount":175,"walletAmount":150,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":67,"walletTxnDetailsId":"219","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0},{"type":"Topup","createdOn":"02/03/2016","amount":25,"walletAmount":325,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":66,"walletTxnDetailsId":"218","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":200,"walletAmount":350,"recordType":"A","paymentInfo":"","expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":68,"walletTxnDetailsId":"217","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":100,"walletAmount":550,"recordType":"A","paymentInfo":"","expiryDate":"02/14/2016","paymentRefNumber":null,"walletRefNumber":59,"walletTxnDetailsId":"216","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0}]},{"type":"Promo","createdOn":"02/03/2016","amount":200,"walletAmount":650,"recordType":"B","paymentInfo":null,"expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":68,"walletTxnDetailsId":"215","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":112},{"type":"Refund","createdOn":"02/03/2016","amount":100,"walletAmount":100,"recordType":"B","paymentInfo":null,"expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":65,"walletTxnDetailsId":"211","remainingAmount":100,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":109,"orderItems":[{"type":"Refund","createdOn":"02/03/2016","amount":100,"walletAmount":100,"recordType":"B","paymentInfo":"","expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":65,"walletTxnDetailsId":"211","remainingAmount":100,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":0}]},{"type":"Order","createdOn":"02/03/2016","amount":550,"walletAmount":0,"recordType":"A","paymentInfo":null,"expiryDate":null,"paymentRefNumber":null,"walletRefNumber":63,"walletTxnDetailsId":"210","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":108,"orderItems":[{"type":"Topup","createdOn":"02/03/2016","amount":25,"walletAmount":0,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":63,"walletTxnDetailsId":"210","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":0},{"type":"Topup","createdOn":"02/03/2016","amount":225,"walletAmount":25,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":62,"walletTxnDetailsId":"209","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":100,"walletAmount":250,"recordType":"A","paymentInfo":"","expiryDate":"02/14/2016","paymentRefNumber":null,"walletRefNumber":59,"walletTxnDetailsId":"208","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":200,"walletAmount":350,"recordType":"A","paymentInfo":"","expiryDate":"02/05/2016","paymentRefNumber":null,"walletRefNumber":64,"walletTxnDetailsId":"207","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet3","orderDescription":"third time","createdBy":null,"uniqueId":0}]},{"type":"Promo","createdOn":"02/03/2016","amount":200,"walletAmount":550,"recordType":"B","paymentInfo":"","expiryDate":"02/05/2016","paymentRefNumber":"smartwallet3","walletRefNumber":64,"walletTxnDetailsId":"206","remainingAmount":0,"transactionStatus":"success","voucherType":"Cashback","orderRefNumber":null,"orderDescription":null,"createdBy":null,"uniqueId":107},{"type":"Order","createdOn":"02/03/2016","amount":125,"walletAmount":225,"recordType":"A","paymentInfo":null,"expiryDate":null,"paymentRefNumber":null,"walletRefNumber":62,"walletTxnDetailsId":"203","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet2","orderDescription":"second time","createdBy":null,"uniqueId":105,"orderItems":[{"type":"Topup","createdOn":"02/03/2016","amount":25,"walletAmount":225,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":62,"walletTxnDetailsId":"203","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet2","orderDescription":"second time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":100,"walletAmount":250,"recordType":"A","paymentInfo":"","expiryDate":"02/14/2016","paymentRefNumber":null,"walletRefNumber":59,"walletTxnDetailsId":"202","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet2","orderDescription":"second time","createdBy":null,"uniqueId":0}]},{"type":"Topup","createdOn":"02/03/2016","amount":250,"walletAmount":350,"recordType":"B","paymentInfo":"","expiryDate":null,"paymentRefNumber":"smartwallet2","walletRefNumber":62,"walletTxnDetailsId":"201","remainingAmount":0,"transactionStatus":"success","voucherType":"Voucher","orderRefNumber":null,"orderDescription":null,"createdBy":null,"uniqueId":104},{"type":"RefundToBank","createdOn":"02/03/2016","amount":20,"walletAmount":100,"recordType":"A","paymentInfo":null,"expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":61,"walletTxnDetailsId":"200","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":null,"orderDescription":null,"createdBy":null,"uniqueId":103},{"type":"Refund","createdOn":"02/03/2016","amount":20,"walletAmount":20,"recordType":"B","paymentInfo":null,"expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":61,"walletTxnDetailsId":"198","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":102,"orderItems":[{"type":"Refund","createdOn":"02/03/2016","amount":20,"walletAmount":20,"recordType":"B","paymentInfo":"","expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":61,"walletTxnDetailsId":"198","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":0}]},{"type":"RefundToBank","createdOn":"02/03/2016","amount":50,"walletAmount":0,"recordType":"A","paymentInfo":null,"expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":60,"walletTxnDetailsId":"197","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":null,"orderDescription":null,"createdBy":null,"uniqueId":101},{"type":"Refund","createdOn":"02/03/2016","amount":50,"walletAmount":50,"recordType":"B","paymentInfo":null,"expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":60,"walletTxnDetailsId":"195","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":99,"orderItems":[{"type":"Refund","createdOn":"02/03/2016","amount":30,"walletAmount":30,"recordType":"B","paymentInfo":"","expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":60,"walletTxnDetailsId":"195","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":0},{"type":"Refund","createdOn":"02/03/2016","amount":20,"walletAmount":50,"recordType":"B","paymentInfo":"","expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":60,"walletTxnDetailsId":"196","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":0}]},{"type":"Order","createdOn":"02/03/2016","amount":100,"walletAmount":0,"recordType":"A","paymentInfo":null,"expiryDate":"02/14/2016","paymentRefNumber":null,"walletRefNumber":59,"walletTxnDetailsId":"194","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":98,"orderItems":[{"type":"Promo","createdOn":"02/03/2016","amount":100,"walletAmount":0,"recordType":"A","paymentInfo":"","expiryDate":"02/14/2016","paymentRefNumber":null,"walletRefNumber":59,"walletTxnDetailsId":"194","remainingAmount":0,"transactionStatus":"fail","voucherType":null,"orderRefNumber":"smartwallet1","orderDescription":"first time","createdBy":null,"uniqueId":0}]},{"type":"Promo","createdOn":"02/03/2016","amount":100,"walletAmount":100,"recordType":"B","paymentInfo":"","expiryDate":"02/14/2016","paymentRefNumber":"smartwallet1","walletRefNumber":59,"walletTxnDetailsId":"193","remainingAmount":0,"transactionStatus":"success","voucherType":"Voucher","orderRefNumber":null,"orderDescription":null,"createdBy":null,"uniqueId":97}]
     * size : 13
     */

    private String status;
    private int statusCode;
    private String message;
    private Object description;
    private int size;
    /**
     * type : Order
     * createdOn : 02/03/2016
     * amount : 500
     * walletAmount : 150
     * recordType : A
     * paymentInfo : null
     * expiryDate : null
     * paymentRefNumber : null
     * walletRefNumber : 67
     * walletTxnDetailsId : 219
     * remainingAmount : 0
     * transactionStatus : success
     * voucherType : null
     * orderRefNumber : smartwallet4
     * orderDescription : fourth time
     * createdBy : null
     * uniqueId : 113
     * orderItems : [{"type":"Topup","createdOn":"02/03/2016","amount":175,"walletAmount":150,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":67,"walletTxnDetailsId":"219","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0},{"type":"Topup","createdOn":"02/03/2016","amount":25,"walletAmount":325,"recordType":"A","paymentInfo":"","expiryDate":null,"paymentRefNumber":null,"walletRefNumber":66,"walletTxnDetailsId":"218","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":200,"walletAmount":350,"recordType":"A","paymentInfo":"","expiryDate":"02/18/2016","paymentRefNumber":null,"walletRefNumber":68,"walletTxnDetailsId":"217","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0},{"type":"Promo","createdOn":"02/03/2016","amount":100,"walletAmount":550,"recordType":"A","paymentInfo":"","expiryDate":"02/14/2016","paymentRefNumber":null,"walletRefNumber":59,"walletTxnDetailsId":"216","remainingAmount":0,"transactionStatus":"success","voucherType":null,"orderRefNumber":"smartwallet4","orderDescription":"fourth time","createdBy":null,"uniqueId":0}]
     */

    private List<DataEntity> data;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Object getDescription() {
        return description;
    }

    public int getSize() {
        return size;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {
        private String type;
        private String createdOn;
        private int amount;
        private int walletAmount;
        private String recordType;
        private Object paymentInfo;
        private Object expiryDate;
        private Object paymentRefNumber;
        private int walletRefNumber;
        private String walletTxnDetailsId;
        private int remainingAmount;
        private String transactionStatus;
        private Object voucherType;
        private String orderRefNumber;
        private String orderDescription;
        private Object createdBy;
        private int uniqueId;
        /**
         * type : Topup
         * createdOn : 02/03/2016
         * amount : 175
         * walletAmount : 150
         * recordType : A
         * paymentInfo :
         * expiryDate : null
         * paymentRefNumber : null
         * walletRefNumber : 67
         * walletTxnDetailsId : 219
         * remainingAmount : 0
         * transactionStatus : success
         * voucherType : null
         * orderRefNumber : smartwallet4
         * orderDescription : fourth time
         * createdBy : null
         * uniqueId : 0
         */

        private List<OrderItemsEntity> orderItems;

        public void setType(String type) {
            this.type = type;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public void setWalletAmount(int walletAmount) {
            this.walletAmount = walletAmount;
        }

        public void setRecordType(String recordType) {
            this.recordType = recordType;
        }

        public void setPaymentInfo(Object paymentInfo) {
            this.paymentInfo = paymentInfo;
        }

        public void setExpiryDate(Object expiryDate) {
            this.expiryDate = expiryDate;
        }

        public void setPaymentRefNumber(Object paymentRefNumber) {
            this.paymentRefNumber = paymentRefNumber;
        }

        public void setWalletRefNumber(int walletRefNumber) {
            this.walletRefNumber = walletRefNumber;
        }

        public void setWalletTxnDetailsId(String walletTxnDetailsId) {
            this.walletTxnDetailsId = walletTxnDetailsId;
        }

        public void setRemainingAmount(int remainingAmount) {
            this.remainingAmount = remainingAmount;
        }

        public void setTransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        public void setVoucherType(Object voucherType) {
            this.voucherType = voucherType;
        }

        public void setOrderRefNumber(String orderRefNumber) {
            this.orderRefNumber = orderRefNumber;
        }

        public void setOrderDescription(String orderDescription) {
            this.orderDescription = orderDescription;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public void setUniqueId(int uniqueId) {
            this.uniqueId = uniqueId;
        }

        public void setOrderItems(List<OrderItemsEntity> orderItems) {
            this.orderItems = orderItems;
        }

        public String getType() {
            return type;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public int getAmount() {
            return amount;
        }

        public int getWalletAmount() {
            return walletAmount;
        }

        public String getRecordType() {
            return recordType;
        }

        public Object getPaymentInfo() {
            return paymentInfo;
        }

        public Object getExpiryDate() {
            return expiryDate;
        }

        public Object getPaymentRefNumber() {
            return paymentRefNumber;
        }

        public int getWalletRefNumber() {
            return walletRefNumber;
        }

        public String getWalletTxnDetailsId() {
            return walletTxnDetailsId;
        }

        public int getRemainingAmount() {
            return remainingAmount;
        }

        public String getTransactionStatus() {
            return transactionStatus;
        }

        public Object getVoucherType() {
            return voucherType;
        }

        public String getOrderRefNumber() {
            return orderRefNumber;
        }

        public String getOrderDescription() {
            return orderDescription;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public int getUniqueId() {
            return uniqueId;
        }

        public List<OrderItemsEntity> getOrderItems() {
            return orderItems;
        }

        public static class OrderItemsEntity {
            private String type;
            private String createdOn;
            private int amount;
            private int walletAmount;
            private String recordType;
            private String paymentInfo;
            private Object expiryDate;
            private Object paymentRefNumber;
            private int walletRefNumber;
            private String walletTxnDetailsId;
            private int remainingAmount;
            private String transactionStatus;
            private Object voucherType;
            private String orderRefNumber;
            private String orderDescription;
            private Object createdBy;
            private int uniqueId;

            public void setType(String type) {
                this.type = type;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public void setWalletAmount(int walletAmount) {
                this.walletAmount = walletAmount;
            }

            public void setRecordType(String recordType) {
                this.recordType = recordType;
            }

            public void setPaymentInfo(String paymentInfo) {
                this.paymentInfo = paymentInfo;
            }

            public void setExpiryDate(Object expiryDate) {
                this.expiryDate = expiryDate;
            }

            public void setPaymentRefNumber(Object paymentRefNumber) {
                this.paymentRefNumber = paymentRefNumber;
            }

            public void setWalletRefNumber(int walletRefNumber) {
                this.walletRefNumber = walletRefNumber;
            }

            public void setWalletTxnDetailsId(String walletTxnDetailsId) {
                this.walletTxnDetailsId = walletTxnDetailsId;
            }

            public void setRemainingAmount(int remainingAmount) {
                this.remainingAmount = remainingAmount;
            }

            public void setTransactionStatus(String transactionStatus) {
                this.transactionStatus = transactionStatus;
            }

            public void setVoucherType(Object voucherType) {
                this.voucherType = voucherType;
            }

            public void setOrderRefNumber(String orderRefNumber) {
                this.orderRefNumber = orderRefNumber;
            }

            public void setOrderDescription(String orderDescription) {
                this.orderDescription = orderDescription;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public void setUniqueId(int uniqueId) {
                this.uniqueId = uniqueId;
            }

            public String getType() {
                return type;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public int getAmount() {
                return amount;
            }

            public int getWalletAmount() {
                return walletAmount;
            }

            public String getRecordType() {
                return recordType;
            }

            public String getPaymentInfo() {
                return paymentInfo;
            }

            public Object getExpiryDate() {
                return expiryDate;
            }

            public Object getPaymentRefNumber() {
                return paymentRefNumber;
            }

            public int getWalletRefNumber() {
                return walletRefNumber;
            }

            public String getWalletTxnDetailsId() {
                return walletTxnDetailsId;
            }

            public int getRemainingAmount() {
                return remainingAmount;
            }

            public String getTransactionStatus() {
                return transactionStatus;
            }

            public Object getVoucherType() {
                return voucherType;
            }

            public String getOrderRefNumber() {
                return orderRefNumber;
            }

            public String getOrderDescription() {
                return orderDescription;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public int getUniqueId() {
                return uniqueId;
            }
        }
    }
}
