package com.getsmartapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * @author  maria.quibtiya on 10/07/16.
 */
public class CallDetailsModel implements Serializable {

    private static Parcelable.Creator<CallDetailsModel> CREATOR =
            new Parcelable.Creator<CallDetailsModel>() {
                @Override
                public CallDetailsModel createFromParcel(Parcel source) {
                    return null;
                }

                @Override
                public CallDetailsModel[] newArray(int size) {
                    return new CallDetailsModel[0];
                }
            };

}
