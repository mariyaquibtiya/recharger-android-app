package com.getsmartapp.model;

import java.util.List;

/**
 * @author shalakha.gupta on 04/02/16.
 */
public class RefundToBankModel {


    /**
     * status : 1
     */

    private HeaderEntity header;
    /**
     * status : SUCCESS
     * statusCode : 2000
     * message : SUCCESS
     * description : null
     * data : {"walletId":2,"email":"niteesh.bijalwan@gmail.com","balanceAmount":20,"latestExpiryDat7e":"2015-12-30"}
     */

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }
        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 602
             * message : Error in walletBalanceRequest API request
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }

    public static class BodyEntity {
        private String status;
        private int statusCode;
        private String message;

        private DataEntity data;

        public void setStatus(String status) {
            this.status = status;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setData(DataEntity data) {
            this.data = data;
        }

        public String getStatus() {
            return status;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getMessage() {
            return message;
        }

        public DataEntity getData() {
            return data;
        }

        public static class DataEntity {
            private int walletId;
            private String email;
            private int balanceAmount;
            private String latestExpiryDate;

            public void setWalletId(int walletId) {
                this.walletId = walletId;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public void setBalanceAmount(int balanceAmount) {
                this.balanceAmount = balanceAmount;
            }

            public void setLatestExpiryDat7e(String latestExpiryDat7e) {
                this.latestExpiryDate = latestExpiryDat7e;
            }

            public int getWalletId() {
                return walletId;
            }

            public String getEmail() {
                return email;
            }

            public int getBalanceAmount() {
                return balanceAmount;
            }

            public String getLatestExpiryDat7e() {
                return latestExpiryDate;
            }
        }
    }
}
