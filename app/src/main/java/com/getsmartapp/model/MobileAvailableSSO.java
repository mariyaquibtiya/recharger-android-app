package com.getsmartapp.model;

import com.getsmartapp.lib.model.EmailEntity;
import com.getsmartapp.lib.model.GenericParserSSO;

import java.util.List;

/**
 * @author Nitesh.Verma on 18-03-2015.
 */
public class MobileAvailableSSO extends GenericParserSSO {

    /**
     * allemails : [{"status":"1","email":"niteshghn@gmail.com","primary":"1"},{"status":"0","email":"niteshhh@grr.la","primary":"0"}]
     * available : false
     * user : ticket belong to same User
     * mobile : 8800744198
     */
    private List<EmailEntity> allemails;
    private boolean available;
    private String sameUser;
    private String mobile;

    public void setAllemails(List<EmailEntity> allemails) {
        this.allemails = allemails;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setSameUser(String user) {
        this.sameUser = user;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<EmailEntity> getAllemails() {
        return allemails;
    }

    public boolean isAvailable() {
        return available;
    }

    public String getSameUser() {
        return sameUser;
    }

    public String getMobile() {
        return mobile;
    }
}
