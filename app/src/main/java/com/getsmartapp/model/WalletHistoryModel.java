package com.getsmartapp.model;

import java.util.List;

/**
 * @author shalakha.gupta on 04/01/16.
 */
public class WalletHistoryModel {

    /**
     * status : 1
     * errors : {"errorList":[{"errorCode":602,"message":"Error in walletBalanceRequest API request"}]}
     */

    private HeaderEntity header;
    /**
     * type : Topup
     * amount : 10
     * createdOn : 01/22/2016
     * walletAmount : 160
     * recordType : B
     * paymentInfo : info
     * remainingAmount : 160.0
     * transactionStatus : success
     */

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;

        public void setStatus(String status) {
            this.status = status;
        }

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 602
             * message : Error in walletBalanceRequest API request
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }

    public static class BodyEntity {

        private String status;
        private int statusCode;
        private String message;
        private int size;
        private double walletBalance;
        private List<DataEntity> data;


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public double getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(double walletBalance) {
            this.walletBalance = walletBalance;
        }

        public List<DataEntity> getData() {
            return data;
        }

        public void setData(List<DataEntity> data) {
            this.data = data;
        }

        public class DataEntity {
            private String type;
            private int amount;
            private String createdOn;
            private int walletAmount;
            private String recordType;
            private String paymentInfo;
            private double remainingAmount;
            private String transactionStatus;
            private String paymentRefNumber;
            private String walletRefNumber;
            private String walletTxnDetailsId;
            private String voucherType;
            private String expiryDate;
            private String  orderRefNumber;
            private String orderDescription;
            private String createdBy;
            private String uniqueId;

            public void setType(String type) {
                this.type = type;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public void setWalletAmount(int walletAmount) {
                this.walletAmount = walletAmount;
            }

            public void setRecordType(String recordType) {
                this.recordType = recordType;
            }

            public void setPaymentInfo(String paymentInfo) {
                this.paymentInfo = paymentInfo;
            }

            public void setRemainingAmount(double remainingAmount) {
                this.remainingAmount = remainingAmount;
            }

            public void setTransactionStatus(String transactionStatus) {
                this.transactionStatus = transactionStatus;
            }

            public String getType() {
                return type;
            }

            public int getAmount() {
                return amount;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public int getWalletAmount() {
                return walletAmount;
            }

            public String getRecordType() {
                return recordType;
            }

            public String getPaymentInfo() {
                return paymentInfo;
            }

            public double getRemainingAmount() {
                return remainingAmount;
            }

            public String getTransactionStatus() {
                return transactionStatus;
            }

            public String getPaymentRefNumber() {
                return paymentRefNumber;
            }

            public void setPaymentRefNumber(String paymentRefNumber) {
                this.paymentRefNumber = paymentRefNumber;
            }

            public String getWalletRefNumber() {
                return walletRefNumber;
            }

            public void setWalletRefNumber(String walletRefNumber) {
                this.walletRefNumber = walletRefNumber;
            }

            public String getWalletTxnDetailsId() {
                return walletTxnDetailsId;
            }

            public void setWalletTxnDetailsId(String walletTxnDetailsId) {
                this.walletTxnDetailsId = walletTxnDetailsId;
            }

            public String getVoucherType() {
                return voucherType;
            }

            public void setVoucherType(String voucherType) {
                this.voucherType = voucherType;
            }

            public String getExpiryDate() {
                return expiryDate;
            }

            public void setExpiryDate(String expiryDate) {
                this.expiryDate = expiryDate;
            }

            public String getOrderRefNumber() {
                return orderRefNumber;
            }

            public void setOrderRefNumber(String orderRefNumber) {
                this.orderRefNumber = orderRefNumber;
            }

            public String getOrderDescription() {
                return orderDescription;
            }

            public void setOrderDescription(String orderDescription) {
                this.orderDescription = orderDescription;
            }

            public String getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }
        }
    }
}