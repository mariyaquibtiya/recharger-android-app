package com.getsmartapp.model;

import java.util.List;

/**
 * @author shalakha.gupta on 02/02/16.
 */
public class LoadMoneyGCModel {

    private HeaderEntity header;

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;
        public void setStatus(String status) {
            this.status = status;
        }
        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 602
             * message : Error in walletBalanceRequest API request
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }

    }

    public static class BodyEntity {
        String status;
        int statusCode;
        String message;
        DataEntity data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataEntity getDataEntity() {
            return data;
        }

        public void setDataEntity(DataEntity dataEntity) {
            this.data = dataEntity;
        }

        public class DataEntity{
            int walletId;
            String email;
            double amount;
            double balanceAmount;
            String latestExpiryDate;

            public int getWalletId() {
                return walletId;
            }

            public void setWalletId(int walletId) {
                this.walletId = walletId;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public double getBalanceAmount() {
                return balanceAmount;
            }

            public void setBalanceAmount(double balanceAmount) {
                this.balanceAmount = balanceAmount;
            }

            public String getLatestExpiryDate() {
                return latestExpiryDate;
            }

            public void setLatestExpiryDate(String latestExpiryDate) {
                this.latestExpiryDate = latestExpiryDate;
            }

            public double getAmount() {
                return amount;
            }

            public void setAmount(double gcAmount) {
                this.amount = gcAmount;
            }
        }
    }

}

