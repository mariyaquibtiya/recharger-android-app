package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;

/**
 * @author Peeyush.Singh on 16-06-2016.
 */
public class ServerSyncInfoObject extends RealmObject{
    String data_date;
    int json_type;
    long timestamp;
    int send_type;
    int sim_id;

    public void setData_date(String data_date) {
        this.data_date = data_date;
    }

    public void setJson_type(int json_type) {
        this.json_type = json_type;
    }

    public void setSend_type(int send_type) {
        this.send_type = send_type;
    }

    public void setSim_id(int sim_id) {
        this.sim_id = sim_id;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getJson_type() {
        return json_type;
    }

    public int getSend_type() {
        return send_type;
    }

    public int getSim_id() {
        return sim_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getData_date() {
        return data_date;
    }
}
