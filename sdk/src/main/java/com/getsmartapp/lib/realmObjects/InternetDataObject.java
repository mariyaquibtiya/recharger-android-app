package com.getsmartapp.lib.realmObjects;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * @author Peeyush.Singh on 03-06-2016.
 */
public class InternetDataObject extends RealmObject {

    @Index
    String date;
    String file_name;
    long date_in_millis;
    long mobile_data_received;
    long mobile_data_transferred;
    long total_mobile_data;
    long total_mobile_bg_data;
    long total_mobile_fg_data;
    long total_2g_data;
    long total_2g_bg_data;
    long total_2g_fg_data;
    long total_3g_data;
    long total_3g_bg_data;
    long total_3g_fg_data;
    long total_4g_data;
    long total_4g_bg_data;
    long total_4g_fg_data;
    long total_5g_data;
    long total_5g_bg_data;
    long total_5g_fg_data;
    long total_6g_data;
    long total_6g_bg_data;
    long total_6g_fg_data;
    long wifi_data_received;
    long wifi_data_transferred;
    long total_wifi_data;
    long total_wifi_bg_data;
    long total_wifi_fg_data;

    long total_data;
    long total_data_received;
    long total_data_transferred;

    RealmList<AppObject> appObjectRealmList;
    RealmList<WifiHotSpot> wifiHotSpotRealmList;


    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public void setDate_in_millis(long date_in_millis) {
        this.date_in_millis = date_in_millis;
    }

    public void setMobile_data_received(long mobile_data_received) {
        this.mobile_data_received = mobile_data_received;
    }

    public void setMobile_data_transferred(long mobile_data_transferred) {
        this.mobile_data_transferred = mobile_data_transferred;
    }

    public void setTotal_mobile_data(long total_mobile_data) {
        this.total_mobile_data = total_mobile_data;
    }

    public void setWifi_data_received(long wifi_data_received) {
        this.wifi_data_received = wifi_data_received;
    }

    public void setWifi_data_transferred(long wifi_data_transferred) {
        this.wifi_data_transferred = wifi_data_transferred;
    }

    public void setTotal_wifi_data(long total_wifi_data) {
        this.total_wifi_data = total_wifi_data;
    }

    public void setTotal_data(long total_data) {
        this.total_data = total_data;
    }

    public void setTotal_data_received(long total_data_received) {
        this.total_data_received = total_data_received;
    }

    public void setTotal_data_transferred(long total_data_transferred) {
        this.total_data_transferred = total_data_transferred;
    }

    public void setAppObjectRealmList(RealmList<AppObject> appObjectRealmList) {
        this.appObjectRealmList = appObjectRealmList;
    }

    public void setWifiHotSpotRealmList(RealmList<WifiHotSpot> wifiHotSpotRealmList) {
        this.wifiHotSpotRealmList = wifiHotSpotRealmList;
    }

    public String getFile_name() {
        return file_name;
    }

    public String getDate() {
        return date;
    }

    public long getDate_in_millis() {
        return date_in_millis;
    }

    public long getMobile_data_received() {
        return mobile_data_received;
    }

    public long getMobile_data_transferred() {
        return mobile_data_transferred;
    }

    public long getTotal_mobile_data() {
        return total_mobile_data;
    }

    public long getWifi_data_received() {
        return wifi_data_received;
    }

    public long getWifi_data_transferred() {
        return wifi_data_transferred;
    }

    public long getTotal_wifi_data() {
        return total_wifi_data;
    }

    public long getTotal_data() {
        return total_data;
    }

    public long getTotal_data_received() {
        return total_data_received;
    }

    public long getTotal_data_transferred() {
        return total_data_transferred;
    }

    public RealmList<AppObject> getAppObjectRealmList() {
        return appObjectRealmList;
    }

    public RealmList<WifiHotSpot> getWifiHotSpotRealmList() {
        return wifiHotSpotRealmList;
    }
}
