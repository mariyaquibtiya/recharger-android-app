package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;

/**
 * @author Peeyush.Singh on 20-06-2016.
 */
public class CardCategoryRealmObject extends RealmObject{
    int card_category_id;
    String card_category_name;

    public void setCard_category_id(int card_category_id) {
        this.card_category_id = card_category_id;
    }

    public void setCard_category_name(String card_category_name) {
        this.card_category_name = card_category_name;
    }
}
