package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;

/**
 * @author Peeyush.Singh on 16-06-2016.
 */
public class USSDCodeDetailsObject extends RealmObject{

    String ussd_code_details;
    String title_text;
    String description_text;
    int code_type;
    String default_dial_code;
    String updated_dial_code;
    String dial_instructions;
    String default_sms_code;
    String updated_sms_code;
    String sms_instructions;
    String sms_text;

    public void setCode_type(int code_type) {
        this.code_type = code_type;
    }

    public void setDefault_dial_code(String default_dial_code) {
        this.default_dial_code = default_dial_code;
    }

    public void setDescription_text(String description_text) {
        this.description_text = description_text;
    }

    public void setTitle_text(String title_text) {
        this.title_text = title_text;
    }

    public void setUpdated_dial_code(String updated_dial_code) {
        this.updated_dial_code = updated_dial_code;
    }

    public void setUssd_code_details(String ussd_code_details) {
        this.ussd_code_details = ussd_code_details;
    }

    public void setDefault_sms_code(String default_sms_code) {
        this.default_sms_code = default_sms_code;
    }

    public void setDial_instructions(String dial_instructions) {
        this.dial_instructions = dial_instructions;
    }

    public void setSms_instructions(String sms_instructions) {
        this.sms_instructions = sms_instructions;
    }

    public void setSms_text(String sms_text) {
        this.sms_text = sms_text;
    }

    public void setUpdated_sms_code(String updated_sms_code) {
        this.updated_sms_code = updated_sms_code;
    }

    public int getCode_type() {
        return code_type;
    }

    public String getDefault_dial_code() {
        return default_dial_code;
    }

    public String getDescription_text() {
        return description_text;
    }

    public String getTitle_text() {
        return title_text;
    }

    public String getUpdated_dial_code() {
        return updated_dial_code;
    }

    public String getUssd_code_details() {
        return ussd_code_details;
    }

    public String getDefault_sms_code() {
        return default_sms_code;
    }

    public String getDial_instructions() {
        return dial_instructions;
    }

    public String getSms_instructions() {
        return sms_instructions;
    }

    public String getSms_text() {
        return sms_text;
    }

    public String getUpdated_sms_code() {
        return updated_sms_code;
    }
}
