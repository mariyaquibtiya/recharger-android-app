package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * @author Peeyush.Singh on 24-05-2016.
 */
public class SMSObject extends RealmObject {

    String sms_id;
    @Index
    String contact_number;
    String contact_name;
    String sms_address;
    long sms_time;
    String sms_body;
    String sms_type;
    long sms_sync_time;
    int is_roaming;
    @Index
    String hash;

    boolean isOperatorSMS;
    String is_night;
    String network_type;
    String circle_id;
    String country_name;
    String sms_location_locality;
    String sms_location_state;
    String sms_location_country;
    String sms_location_feature;
    String sim_id;

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setOperatorSMS(boolean operatorSMS) {
        isOperatorSMS = operatorSMS;
    }

    public boolean isOperatorSMS() {
        return isOperatorSMS;
    }

    public void setSms_address(String sms_address) {
        this.sms_address = sms_address;
    }

    public void setNetwork_type(String network_type) {
        this.network_type = network_type;
    }

    public void setIs_roaming(int is_roaming) {
        this.is_roaming = is_roaming;
    }

    public void setIs_night(String is_night) {
        this.is_night = is_night;
    }

    public void setCircle_id(String circle_id) {
        this.circle_id = circle_id;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public void setSms_body(String sms_body) {
        this.sms_body = sms_body;
    }

    public void setSms_id(String sms_id) {
        this.sms_id = sms_id;
    }

    public void setSms_location_country(String sms_location_country) {
        this.sms_location_country = sms_location_country;
    }

    public void setSms_location_feature(String sms_location_feature) {
        this.sms_location_feature = sms_location_feature;
    }

    public void setSms_location_locality(String sms_location_locality) {
        this.sms_location_locality = sms_location_locality;
    }

    public void setSms_location_state(String sms_location_state) {
        this.sms_location_state = sms_location_state;
    }

    public void setSim_id(String sim_id) {
        this.sim_id = sim_id;
    }

    public void setSms_sync_time(long sms_sync_time) {
        this.sms_sync_time = sms_sync_time;
    }

    public void setSms_time(long sms_time) {
        this.sms_time = sms_time;
    }

    public void setSms_type(String sms_type) {
        this.sms_type = sms_type;
    }

    public String getHash() {
        return hash;
    }

    public String getSms_address() {
        return sms_address;
    }

    public String getNetwork_type() {
        return network_type;
    }

    public int getIs_roaming() {
        return is_roaming;
    }

    public long getSms_sync_time() {
        return sms_sync_time;
    }

    public long getSms_time() {
        return sms_time;
    }

    public String getCircle_id() {
        return circle_id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public String getContact_number() {
        return contact_number;
    }

    public String getCountry_name() {
        return country_name;
    }

    public String getIs_night() {
        return is_night;
    }

    public String getSms_body() {
        return sms_body;
    }

    public String getSms_id() {
        return sms_id;
    }

    public String getSms_location_country() {
        return sms_location_country;
    }

    public String getSms_location_feature() {
        return sms_location_feature;
    }

    public String getSms_location_locality() {
        return sms_location_locality;
    }

    public String getSms_location_state() {
        return sms_location_state;
    }

    public String getSms_type() {
        return sms_type;
    }

    public String getSim_id() {
        return sim_id;
    }

    public static class Constants
    {
        public static final String SMS_TYPE_SENT = "SMS_TYPE_SENT";
        public static final String SMS_TYPE_RECEIVED = "SMS_TYPE_RECEIVED";

        public static final String LOCAL_NUMBER = "LOCAL_NUMBER";
        public static final String STD_NUMBER = "STD_NUMBER";
        public static final String ISD_NUMBER = "ISD_NUMBER";
        public static final String PROMOTIONAL_NUMBER = "PROMOTIONAL_NUMBER";
    }
}
