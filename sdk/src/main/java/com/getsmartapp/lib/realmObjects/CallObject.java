package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * @author Peeyush.Singh on 21-05-2016.
 */
public class CallObject extends RealmObject {

    String call_id;
    @Index
    String contact_number;
    String contact_name;
    long call_time;
    int call_duration_in_sec;
    int call_duration_in_min;
    String call_type;
    int is_roaming;
    String number_type;
    int is_night;
    @Index
    String hash;
    String network_type;
    String circle_id;
    String country_name;
    String is_synced;
    String sync_time;
    @Index
    String call_location_locality;
    String call_location_state;
    String call_location_country;
    String call_location_feature;
    String sim_id;

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }

    public void setCall_duration_in_min(int call_duration_in_min) {
        this.call_duration_in_min = call_duration_in_min;
    }

    public void setCall_duration_in_sec(int call_duration_in_sec) {
        this.call_duration_in_sec = call_duration_in_sec;
    }

    public void setCall_id(String call_id) {
        this.call_id = call_id;
    }

    public void setCall_location_country(String call_location_country) {
        this.call_location_country = call_location_country;
    }

    public void setCall_location_feature(String call_location_feature) {
        this.call_location_feature = call_location_feature;
    }

    public void setCall_location_locality(String call_location_locality) {
        this.call_location_locality = call_location_locality;
    }

    public void setCall_location_state(String call_location_state) {
        this.call_location_state = call_location_state;
    }

    public void setSim_id(String sim_id) {
        this.sim_id = sim_id;
    }

    public void setCall_time(long call_time) {
        this.call_time = call_time;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public void setCircle_id(String circle_id) {
        this.circle_id = circle_id;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public void setIs_night(int is_night) {
        this.is_night = is_night;
    }

    public void setIs_roaming(int is_roaming) {
        this.is_roaming = is_roaming;
    }

    public void setIs_synced(String is_synced) {
        this.is_synced = is_synced;
    }

    public void setNetwork_type(String network_type) {
        this.network_type = network_type;
    }

    public void setNumber_type(String number_type) {
        this.number_type = number_type;
    }

    public void setSync_time(String sync_time) {
        this.sync_time = sync_time;
    }

    public int getCall_duration_in_min() {
        return call_duration_in_min;
    }

    public int getCall_duration_in_sec() {
        return call_duration_in_sec;
    }

    public long getCall_time() {
        return call_time;
    }

    public String getCall_id() {
        return call_id;
    }

    public String getCall_location_country() {
        return call_location_country;
    }

    public String getCall_location_feature() {
        return call_location_feature;
    }

    public String getCall_location_locality() {
        return call_location_locality;
    }

    public String getCall_location_state() {
        return call_location_state;
    }

    public String getCall_type() {
        return call_type;
    }

    public String getCircle_id() {
        return circle_id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public String getContact_number() {
        return contact_number;
    }

    public String getCountry_name() {
        return country_name;
    }

    public int getIs_night() {
        return is_night;
    }

    public int getIs_roaming() {
        return is_roaming;
    }

    public String getIs_synced() {
        return is_synced;
    }

    public String getNetwork_type() {
        return network_type;
    }

    public String getNumber_type() {
        return number_type;
    }

    public String getSync_time() {
        return sync_time;
    }

    public String getSim_id() {
        return sim_id;
    }

/*    public static class Constants
    {
        public static String INCOMING_TYPE = "INCOMING";
        public static String OUTGOING_TYPE = "OUTGOING";
        public static String MISSED_TYPE = "MISSED";


        public static final String LOCAL_NUMBER = "LOCAL_NUMBER";
        public static final String STD_NUMBER = "STD_NUMBER";
        public static final String ISD_NUMBER = "ISD_NUMBER";
        public static final String TOLLFREE_NUMBER = "TOLLFREE_NUMBER";
    }*/











}
