package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * @author Peeyush.Singh on 03-06-2016.
 */
public class WifiHotSpot extends RealmObject {

    String wifi_ssid;
    String wifi_mac_address;
    long wifi_connected_time;
    long last_wifi_connected_time;
    String date;
    long date_in_millis;
    String wifi_locations;

    @Index
    long wifi_data_consumed;

    public void setLast_wifi_connected_time(long last_wifi_connected_time) {
        this.last_wifi_connected_time = last_wifi_connected_time;
    }

    public void setWifi_connected_time(long wifi_connected_time) {
        this.wifi_connected_time = wifi_connected_time;
    }

    public void setWifi_mac_address(String wifi_mac_address) {
        this.wifi_mac_address = wifi_mac_address;
    }

    public void setWifi_ssid(String wifi_ssid) {
        this.wifi_ssid = wifi_ssid;
    }

    public void setWifi_data_consumed(long wifi_data_consumed) {
        this.wifi_data_consumed = wifi_data_consumed;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDate_in_millis(long date_in_millis) {
        this.date_in_millis = date_in_millis;
    }

    public void setWifi_locations(String wifi_locations) {
        this.wifi_locations = wifi_locations;
    }

    public long getLast_wifi_connected_time() {
        return last_wifi_connected_time;
    }

    public long getWifi_data_consumed() {
        return wifi_data_consumed;
    }

    public long getWifi_connected_time() {
        return wifi_connected_time;
    }

    public String getWifi_mac_address() {
        return wifi_mac_address;
    }

    public String getWifi_ssid() {
        return wifi_ssid;
    }

    public String getDate() {
        return date;
    }

    public long getDate_in_millis() {
        return date_in_millis;
    }

    public String getWifi_locations() {
        return wifi_locations;
    }
}
