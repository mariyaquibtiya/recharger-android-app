package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;

/**
 * @author Peeyush.Singh on 16-06-2016.
 */
public class UserPhoneEntryObject extends RealmObject {

    String mobile_number;
    String circle;
    String provider;
    String sim_type;
    String bill_start_day;
    int monthly_bill;
    int circle_id;
    String preffered_network;
    String data_limit;
    int sim_no;

    public void setBill_start_day(String bill_start_day) {
        this.bill_start_day = bill_start_day;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public void setCircle_id(int circle_id) {
        this.circle_id = circle_id;
    }

    public void setMonthly_bill(int monthly_bill) {
        this.monthly_bill = monthly_bill;
    }

    public void setPreffered_network(String preffered_network) {
        this.preffered_network = preffered_network;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setSim_type(String sim_type) {
        this.sim_type = sim_type;
    }

    public void setData_limit(String data_limit) {
        this.data_limit = data_limit;
    }

    public void setSim_no(int sim_no) {
        this.sim_no = sim_no;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public String getBill_start_day() {
        return bill_start_day;
    }

    public int getMonthly_bill() {
        return monthly_bill;
    }

    public int getCircle_id() {
        return circle_id;
    }

    public String getCircle() {
        return circle;
    }

    public String getPreffered_network() {
        return preffered_network;
    }

    public String getProvider() {
        return provider;
    }

    public String getData_limit() {
        return data_limit;
    }

    public String getSim_type() {
        return sim_type;
    }

    public int getSim_no() {
        return sim_no;
    }
}
