package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;

/**
 * @author Peeyush.Singh on 20-06-2016.
 */
public class CardObjectCategoryMapping extends RealmObject{
    int card_object_id;
    int card_category_id;

    public void setCard_category_id(int card_category_id) {
        this.card_category_id = card_category_id;
    }

    public void setCard_object_id(int card_object_id) {
        this.card_object_id = card_object_id;
    }

    public int getCard_category_id() {
        return card_category_id;
    }

    public int getCard_object_id() {
        return card_object_id;
    }
}
