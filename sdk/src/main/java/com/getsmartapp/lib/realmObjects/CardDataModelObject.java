package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * @author Peeyush.Singh on 25-05-2016.
 */
public class CardDataModelObject extends RealmObject {

    @Index
    private int card_id;
    @Index
    private String card_name;
    private String card_data;
    private String view_card;

    private int val_recency_fallback;
    private int val_context_fallback;
    private double coeff_context;
    private int pos_fixed;
    private int receny_calc_reqd;
    private int parent_card_id;

    private int card_visibility;
    private long update_timestamp;
    private int priority_fallback;
    private float score;
    private String exclude_card;

    private int context_calc_reqd;
    private CardDataModelObject child_card;
    private String category_name;

    public void setCard_data(String card_data) {
        this.card_data = card_data;
    }

    public void setPriority_fallback(int priority_fallback) {
        this.priority_fallback = priority_fallback;
    }

    public void setUpdate_timestamp(long update_timestamp) {
        this.update_timestamp = update_timestamp;
    }

    public void setView_card(String view_card) {
        this.view_card = view_card;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public void setCard_visibility(int card_visibility) {
        this.card_visibility = card_visibility;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public void setChild_card(CardDataModelObject child_card) {
        this.child_card = child_card;
    }

    public void setCoeff_context(float coeff_context) {
        this.coeff_context = coeff_context;
    }

    public void setContext_calc_reqd(int context_calc_reqd) {
        this.context_calc_reqd = context_calc_reqd;
    }

    public void setExclude_card(String exclude_card) {
        this.exclude_card = exclude_card;
    }

    public void setParent_card_id(int parent_card_id) {
        this.parent_card_id = parent_card_id;
    }

    public void setPos_fixed(int pos_fixed) {
        this.pos_fixed = pos_fixed;
    }

    public void setReceny_calc_reqd(int receny_calc_reqd) {
        this.receny_calc_reqd = receny_calc_reqd;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public void setVal_context_fallback(int val_context_fallback) {
        this.val_context_fallback = val_context_fallback;
    }

    public void setVal_recency_fallback(int val_recency_fallback) {
        this.val_recency_fallback = val_recency_fallback;
    }

    public void setCoeff_context(double coeff_context) {
        this.coeff_context = coeff_context;
    }

    public double getCoeff_context() {
        return coeff_context;
    }

    public CardDataModelObject getChild_card() {
        return child_card;
    }

    public float getScore() {
        return score;
    }

    public int getCard_id() {
        return card_id;
    }

    public int getCard_visibility() {
        return card_visibility;
    }

    public int getContext_calc_reqd() {
        return context_calc_reqd;
    }

    public int getParent_card_id() {
        return parent_card_id;
    }

    public int getPos_fixed() {
        return pos_fixed;
    }

    public int getPriority_fallback() {
        return priority_fallback;
    }

    public int getReceny_calc_reqd() {
        return receny_calc_reqd;
    }

    public int getVal_context_fallback() {
        return val_context_fallback;
    }

    public int getVal_recency_fallback() {
        return val_recency_fallback;
    }

    public long getUpdate_timestamp() {
        return update_timestamp;
    }

    public String getCard_data() {
        return card_data;
    }

    public String getCard_name() {
        return card_name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getExclude_card() {
        return exclude_card;
    }

    public String getView_card() {
        return view_card;
    }
}
