package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;

/**
 * @author Peeyush.Singh on 20-06-2016.
 */
public class CardDataRealmObject extends RealmObject {

    int card_id;
    String card_name;
    double val_recency_fallback;
    int receny_calc_reqd;
    double val_context_fallback;
    int context_calc_reqd;
    double coeff_context;
    int pos_fixed;
    int parent_card_id;
    int card_visibility;
    long update_timestamp;
    double score;


    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public void setReceny_calc_reqd(int receny_calc_reqd) {
        this.receny_calc_reqd = receny_calc_reqd;
    }

    public void setPos_fixed(int pos_fixed) {
        this.pos_fixed = pos_fixed;
    }

    public void setParent_card_id(int parent_card_id) {
        this.parent_card_id = parent_card_id;
    }

    public void setCoeff_context(double coeff_context) {
        this.coeff_context = coeff_context;
    }

    public void setContext_calc_reqd(int context_calc_reqd) {
        this.context_calc_reqd = context_calc_reqd;
    }

    public void setVal_context_fallback(double val_context_fallback) {
        this.val_context_fallback = val_context_fallback;
    }

    public void setVal_recency_fallback(double val_recency_fallback) {
        this.val_recency_fallback = val_recency_fallback;
    }

    public void setCard_visibility(int card_visibility) {
        this.card_visibility = card_visibility;
    }

    public void setUpdate_timestamp(long update_timestamp) {
        this.update_timestamp = update_timestamp;
    }

    public void setScore(double score) {
        this.score = score;
    }
    public int getCard_id() {
        return card_id;
    }

    public String getCard_name() {
        return card_name;
    }

    public int getPos_fixed() {
        return pos_fixed;
    }

    public int getParent_card_id() {
        return parent_card_id;
    }

    public double getCoeff_context() {
        return coeff_context;
    }

    public int getCard_visibility() {
        return card_visibility;
    }

    public double getVal_context_fallback() {
        return val_context_fallback;
    }

    public double getVal_recency_fallback() {
        return val_recency_fallback;
    }

    public int getContext_calc_reqd() {
        return context_calc_reqd;
    }

    public int getReceny_calc_reqd() {
        return receny_calc_reqd;
    }

    public long getUpdate_timestamp() {
        return update_timestamp;
    }

    public double getScore() {
        return score;
    }
}
