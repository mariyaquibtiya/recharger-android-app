package com.getsmartapp.lib.realmObjects;

import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * @author Peeyush.Singh on 03-06-2016.
 */
public class AppObject extends RealmObject {

    String app_name;
    @Index
    String app_package_name;
    int app_uid;
    String date;
    long date_in_millis;
    boolean is_temp_object;
    long app_mobile_data_usage;
    boolean is_installed;
    long app_mobile_bg_data_usage;
    long app_mobile_fg_data_usage;
    long app_2g_usage;
    long app_2g_bg_data_usage;
    long app_2g_fg_data_usage;
    long app_3g_usage;
    long app_3g_bg_data_usage;
    long app_3g_fg_data_usage;
    long app_4g_usage;
    long app_4g_bg_data_usage;
    long app_4g_fg_data_usage;
    long app_5g_usage;
    long app_5g_bg_data_usage;
    long app_5g_fg_data_usage;
    long app_6g_usage;
    long app_6g_bg_data_usage;
    long app_6g_fg_data_usage;
    long app_wifi_data_usage;
    long app_wifi_bg_data_usage;
    long app_wiif_fg_data_usage;
    long app_total_data_usage;


    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public void setApp_package_name(String app_package_name) {
        this.app_package_name = app_package_name;
    }

    public void setApp_uid(int app_uid) {
        this.app_uid = app_uid;
    }

    public boolean is_installed() {
        return is_installed;
    }

    public void setInstalled(boolean is_installed) {
        this.is_installed = is_installed;
    }

    public void setApp_mobile_data_usage(long app_mobile_data_usage) {
        this.app_mobile_data_usage = app_mobile_data_usage;
    }

    public void setApp_wifi_data_usage(long app_wifi_data_usage) {
        this.app_wifi_data_usage = app_wifi_data_usage;
    }

    public void setApp_total_data_usage(long app_total_data_usage) {
        this.app_total_data_usage = app_total_data_usage;
    }

    public void setTempObject(boolean tempObject) {
        is_temp_object = tempObject;
    }

    public void setApp_3g_usage(long app_3g_usage) {
        this.app_3g_usage = app_3g_usage;
    }

    public void setApp_2g_usage(long app_2g_usage) {
        this.app_2g_usage = app_2g_usage;
    }

    public void setApp_4g_usage(long app_4g_usage) {
        this.app_4g_usage = app_4g_usage;
    }

    public void setApp_5g_usage(long app_5g_usage) {
        this.app_5g_usage = app_5g_usage;
    }

    public void setApp_6g_usage(long app_6g_usage) {
        this.app_6g_usage = app_6g_usage;
    }

    public void setDate_in_millis(long date_in_millis) {
        this.date_in_millis = date_in_millis;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getApp_name() {
        return app_name;
    }

    public String getApp_package_name() {
        return app_package_name;
    }

    public int getApp_uid() {
        return app_uid;
    }

    public long getApp_mobile_data_usage() {
        return app_mobile_data_usage;
    }

    public long getApp_wifi_data_usage() {
        return app_wifi_data_usage;
    }

    public long getDate_in_millis() {
        return date_in_millis;
    }

    public String getDate() {
        return date;
    }

    public boolean isTempObject() {
        return is_temp_object;
    }

    public long getApp_total_data_usage() {
        return app_total_data_usage;
    }
}
