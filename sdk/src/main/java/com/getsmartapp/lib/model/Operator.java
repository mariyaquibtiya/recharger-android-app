package com.getsmartapp.lib.model;

/**
 * @author Bishwajeet.Kumar on 29-04-2015.
 */
public class Operator {
    private Integer _ID;
    private String _Value = "";

    public Operator() {
        _ID = 0;
        _Value = "";
    }

    public Operator(Integer ID, String Value) {
        this._ID = ID;
        this._Value = Value;
    }

    @ Override
    public String toString() {
        return _Value;
    }

    public Integer getId() {
        return _ID;
    }

    public String getValue() {
        return _Value;
    }


    public void set_ID(Integer _ID) {
        this._ID = _ID;
    }


    public void set_Value(String _Value) {
        this._Value = _Value;
    }
}

