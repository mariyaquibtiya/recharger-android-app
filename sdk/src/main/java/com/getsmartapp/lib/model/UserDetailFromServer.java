package com.getsmartapp.lib.model;

/**
 * @author nitesh.verma on 11/16/15.
 */
public class UserDetailFromServer {


    /**
     * error : User not found.
     * ssoId : 7puxfoxahu6petyvwqxzeqzod
     * fName : Prateek
     * lName : Shekhar
     * email : shekhar.prateek01@gmail.com
     * phNo : 9650360714
     * deviceId : f3f819a79110a72
     * isVerifiedPhNo : 1
     * isDisabled : 0
     * lastUpdated : Nov 16, 2015 12:19:17 AM
     * addedOn : Sep 18, 2015 12:33:41 PM
     * preferences : {"pushTransPref":1,"pushPromotionalPref":1,"emailPref":1,"smsPref":1}
     */

    private String error;
    private String ssoId;
    private String fName;
    private String lName;
    private String email;
    private String phNo;
    private String deviceId;
    private int isVerifiedPhNo;
    private int isDisabled;
    private String lastUpdated;
    private String addedOn;
    /**
     * pushTransPref : 1
     * pushPromotionalPref : 1
     * emailPref : 1
     * smsPref : 1
     */

    private PreferencesEntity preferences;

    public void setError(String error) {
        this.error = error;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public void setLName(String lName) {
        this.lName = lName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setIsVerifiedPhNo(int isVerifiedPhNo) {
        this.isVerifiedPhNo = isVerifiedPhNo;
    }

    public void setIsDisabled(int isDisabled) {
        this.isDisabled = isDisabled;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public void setPreferences(PreferencesEntity preferences) {
        this.preferences = preferences;
    }

    public String getError() {
        return error;
    }

    public String getSsoId() {
        return ssoId;
    }

    public String getFName() {
        return fName;
    }

    public String getLName() {
        return lName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhNo() {
        return phNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public int getIsVerifiedPhNo() {
        return isVerifiedPhNo;
    }

    public int getIsDisabled() {
        return isDisabled;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public PreferencesEntity getPreferences() {
        return preferences;
    }

    public static class PreferencesEntity {
        private int pushTransPref;
        private int pushPromotionalPref;
        private int emailPref;
        private int smsPref;

        public void setPushTransPref(int pushTransPref) {
            this.pushTransPref = pushTransPref;
        }

        public void setPushPromotionalPref(int pushPromotionalPref) {
            this.pushPromotionalPref = pushPromotionalPref;
        }

        public void setEmailPref(int emailPref) {
            this.emailPref = emailPref;
        }

        public void setSmsPref(int smsPref) {
            this.smsPref = smsPref;
        }

        public int getPushTransPref() {
            return pushTransPref;
        }

        public int getPushPromotionalPref() {
            return pushPromotionalPref;
        }

        public int getEmailPref() {
            return emailPref;
        }

        public int getSmsPref() {
            return smsPref;
        }
    }
}
