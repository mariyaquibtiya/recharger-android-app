package com.getsmartapp.lib.model;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * @author Jayant.B on 7/23/2015.
 */
public class DataModel {
    private float threeG = 0;
    private float twoG = 0;
    private float wifi = 0;
    private float fourG = 0;
    private String total = "";
    private String longunit = "";
    private String shortunit = "";

    @Override
    public String toString() {
        return "DataModel{" +
                "threeG=" + threeG +
                ", twoG=" + twoG +
                ", wifi=" + wifi +
                ", fourG=" + fourG +
                ", total='" + total + '\'' +
                ", longunit='" + longunit + '\'' +
                ", shortunit='" + shortunit + '\'' +
                ", hashMap=" + hashMap +
                '}';
    }

    public HashMap<String, DataModel> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, DataModel> hashMap) {
        this.hashMap = hashMap;
    }

    private HashMap<String, DataModel> hashMap;

//    public DataModel(float fourG, float threeG, float twoG, float wifi, String combined, String
//            longunit, String shortunit, HashMap hm) {
//        this.threeG = threeG;
//        this.twoG = twoG;
//        this.wifi = wifi;
//        this.total = combined;
//        this.longunit = longunit;
//        this.shortunit = shortunit;
//        this.fourG = fourG;
//        this.hashMap = hm;
//    }

    public DataModel(float fourG, float threeG, float twoG, float wifi, String combined, HashMap hm) {
        this.threeG = threeG;
        this.twoG = twoG;
        this.wifi = wifi;
        this.total = combined;
        this.fourG = fourG;
        this.hashMap = hm;
    }

    public DataModel(float fourG, float threeG, float twoG, float wifi, String combined, String
            longunit, String shortunit) {
        this.threeG = threeG;
        this.twoG = twoG;
        this.wifi = wifi;
        this.total = combined;
        this.longunit = longunit;
        this.shortunit = shortunit;
        this.fourG = fourG;
    }

    public DataModel() {
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLongunit() {
        return longunit;
    }

    public void setLongunit(String longunit) {
        this.longunit = longunit;
    }

    public float getThreeG() {
        return threeG;
    }

    public void setThreeG(float threeG) {
        this.threeG = threeG;
    }

    public float getTwoG() {
        return twoG;
    }

    public void setTwoG(float twoG) {
        this.twoG = twoG;
    }

    public float getWifi() {
        return wifi;
    }

    public void setWifi(float wifi) {
        this.wifi = wifi;
    }

    public String getShortunit() {
        return shortunit;
    }

    public void setShortunit(String shortunit) {
        this.shortunit = shortunit;
    }

    public float getFourG() {
        return fourG;
    }

    public void setFourG(float fourG) {
        this.fourG = fourG;
    }

    public void merge(Object obj, Object update) {
        if (!obj.getClass().isAssignableFrom(update.getClass())) {
            return;
        }
        Method[] methods = obj.getClass().getMethods();
        for (Method fromMethod : methods) {
            if (fromMethod.getDeclaringClass().equals(obj.getClass())
                    && fromMethod.getName().startsWith("get")) {

                String fromName = fromMethod.getName();
                String toName = fromName.replace("get", "set");
                try {
                    Method toMetod = obj.getClass().getMethod(toName, fromMethod.getReturnType());
                    Object value = fromMethod.invoke(update, (Object[]) null);
                    if (value != null) {
                        toMetod.invoke(obj, value);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
