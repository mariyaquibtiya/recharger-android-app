package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author nitesh.verma on 9/2/15.
 */
public class GoogleImgModal {


    /**
     * ageRange : {"min":21}
     * image : {"isDefault":false,"url":"https://lh3.googleusercontent.com/-3g4pGlHO3XI/AAAAAAAAAAI/AAAAAAAAAP8/fGoDbq-CE1c/photo.jpg?sz=50"}
     * occupation : Job
     * gender : male
     * kind : plus#person
     * displayName : Nitesh Verma
     * verified : false
     * language : en
     * url : https://plus.google.com/+NiteshVerma999
     * objectType : person
     * isPlusUser : true
     * skills : Java, Jquery, Web develoment, Google Maps
     * emails : [{"type":"account","value":"niteshghn@gmail.com"}]
     * cover : {"layout":"banner","coverPhoto":{"width":940,"url":"https://lh3.googleusercontent.com/-6KfkBLmK0uw/UcojJVa8ggI/AAAAAAAABj0/tm_tZF8YdrQ/s630-fcrop64=1,261b3dabe4a3dd93/%25C2%25A9%2BAravind%2BKrishnaswamy_20110306-ngorongono-5d2-5886-Edit.jpg","height":639},"coverInfo":{"topImageOffset":0,"leftImageOffset":0}}
     * placesLived : [{"value":"New Delhi"},{"value":"Gohana"}]
     * name : {"familyName":"Verma","givenName":"Nitesh"}
     * organizations : [{"name":"Indian Institute of Technology Delhi","type":"school","primary":true},{"name":"farmerzden.com","title":"Assistant Manager","type":"work","startDate":"2014","primary":true}]
     * etag : "gLJf7LwN3wOpLHXk4IeQ9ES9mEc/OaCrpy0VSfcmWrUQVuR3SRNswtM"
     * id : 102296105624446251780
     * circledByCount : 75
     */
    private AgeRangeEntity ageRange;
    private ImageEntity image;
    private String occupation;
    private String gender;
    private String kind;
    private String displayName;
    private boolean verified;
    private String language;
    private String url;
    private String objectType;
    private boolean isPlusUser;
    private String skills;
    private List<EmailsEntity> emails;
    private CoverEntity cover;
    private List<PlacesLivedEntity> placesLived;
    private NameEntity name;
    private List<OrganizationsEntity> organizations;
    private String etag;
    private String id;
    private int circledByCount;

    public void setAgeRange(AgeRangeEntity ageRange) {
        this.ageRange = ageRange;
    }

    public void setImage(ImageEntity image) {
        this.image = image;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public void setIsPlusUser(boolean isPlusUser) {
        this.isPlusUser = isPlusUser;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public void setEmails(List<EmailsEntity> emails) {
        this.emails = emails;
    }

    public void setCover(CoverEntity cover) {
        this.cover = cover;
    }

    public void setPlacesLived(List<PlacesLivedEntity> placesLived) {
        this.placesLived = placesLived;
    }

    public void setName(NameEntity name) {
        this.name = name;
    }

    public void setOrganizations(List<OrganizationsEntity> organizations) {
        this.organizations = organizations;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCircledByCount(int circledByCount) {
        this.circledByCount = circledByCount;
    }

    public AgeRangeEntity getAgeRange() {
        return ageRange;
    }

    public ImageEntity getImage() {
        return image;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getGender() {
        return gender;
    }

    public String getKind() {
        return kind;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getLanguage() {
        return language;
    }

    public String getUrl() {
        return url;
    }

    public String getObjectType() {
        return objectType;
    }

    public boolean isIsPlusUser() {
        return isPlusUser;
    }

    public String getSkills() {
        return skills;
    }

    public List<EmailsEntity> getEmails() {
        return emails;
    }

    public CoverEntity getCover() {
        return cover;
    }

    public List<PlacesLivedEntity> getPlacesLived() {
        return placesLived;
    }

    public NameEntity getName() {
        return name;
    }

    public List<OrganizationsEntity> getOrganizations() {
        return organizations;
    }

    public String getEtag() {
        return etag;
    }

    public String getId() {
        return id;
    }

    public int getCircledByCount() {
        return circledByCount;
    }

    public class AgeRangeEntity {
        /**
         * min : 21
         */
        private int min;

        public void setMin(int min) {
            this.min = min;
        }

        public int getMin() {
            return min;
        }
    }

    public class ImageEntity {
        /**
         * isDefault : false
         * url : https://lh3.googleusercontent.com/-3g4pGlHO3XI/AAAAAAAAAAI/AAAAAAAAAP8/fGoDbq-CE1c/photo.jpg?sz=50
         */
        private boolean isDefault;
        private String url;

        public void setIsDefault(boolean isDefault) {
            this.isDefault = isDefault;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isIsDefault() {
            return isDefault;
        }

        public String getUrl() {
            return url;
        }
    }

    public class EmailsEntity {
        /**
         * type : account
         * value : niteshghn@gmail.com
         */
        private String type;
        private String value;

        public void setType(String type) {
            this.type = type;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }
    }

    public class CoverEntity {
        /**
         * layout : banner
         * coverPhoto : {"width":940,"url":"https://lh3.googleusercontent.com/-6KfkBLmK0uw/UcojJVa8ggI/AAAAAAAABj0/tm_tZF8YdrQ/s630-fcrop64=1,261b3dabe4a3dd93/%25C2%25A9%2BAravind%2BKrishnaswamy_20110306-ngorongono-5d2-5886-Edit.jpg","height":639}
         * coverInfo : {"topImageOffset":0,"leftImageOffset":0}
         */
        private String layout;
        private CoverPhotoEntity coverPhoto;
        private CoverInfoEntity coverInfo;

        public void setLayout(String layout) {
            this.layout = layout;
        }

        public void setCoverPhoto(CoverPhotoEntity coverPhoto) {
            this.coverPhoto = coverPhoto;
        }

        public void setCoverInfo(CoverInfoEntity coverInfo) {
            this.coverInfo = coverInfo;
        }

        public String getLayout() {
            return layout;
        }

        public CoverPhotoEntity getCoverPhoto() {
            return coverPhoto;
        }

        public CoverInfoEntity getCoverInfo() {
            return coverInfo;
        }

        public class CoverPhotoEntity {
            /**
             * width : 940
             * url : https://lh3.googleusercontent.com/-6KfkBLmK0uw/UcojJVa8ggI/AAAAAAAABj0/tm_tZF8YdrQ/s630-fcrop64=1,261b3dabe4a3dd93/%25C2%25A9%2BAravind%2BKrishnaswamy_20110306-ngorongono-5d2-5886-Edit.jpg
             * height : 639
             */
            private int width;
            private String url;
            private int height;

            public void setWidth(int width) {
                this.width = width;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public int getWidth() {
                return width;
            }

            public String getUrl() {
                return url;
            }

            public int getHeight() {
                return height;
            }
        }

        public class CoverInfoEntity {
            /**
             * topImageOffset : 0
             * leftImageOffset : 0
             */
            private int topImageOffset;
            private int leftImageOffset;

            public void setTopImageOffset(int topImageOffset) {
                this.topImageOffset = topImageOffset;
            }

            public void setLeftImageOffset(int leftImageOffset) {
                this.leftImageOffset = leftImageOffset;
            }

            public int getTopImageOffset() {
                return topImageOffset;
            }

            public int getLeftImageOffset() {
                return leftImageOffset;
            }
        }
    }

    public class PlacesLivedEntity {
        /**
         * value : New Delhi
         */
        private String value;

        public void setValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public class NameEntity {
        /**
         * familyName : Verma
         * givenName : Nitesh
         */
        private String familyName;
        private String givenName;

        public void setFamilyName(String familyName) {
            this.familyName = familyName;
        }

        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        public String getFamilyName() {
            return familyName;
        }

        public String getGivenName() {
            return givenName;
        }
    }

    public class OrganizationsEntity {
        /**
         * name : Indian Institute of Technology Delhi
         * type : school
         * primary : true
         */
        private String name;
        private String type;
        private boolean primary;

        public void setName(String name) {
            this.name = name;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setPrimary(boolean primary) {
            this.primary = primary;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public boolean isPrimary() {
            return primary;
        }
    }
}
