package com.getsmartapp.lib.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author nitesh.verma on 3/29/16.
 */
public class CallClassificationMNP implements Serializable {


    /**
     * status : 1
     * errors : {"errorList":[{"errorCode":1401,"message":"Unable to fetch mnp data."}]}
     */

    private HeaderEntity header;
    private BodyEntity body;

    public HeaderEntity getHeader() {
        return header;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 1401
             * message : Unable to fetch mnp data.
             */

            private List<ErrorListEntity> errorList;

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public int getErrorCode() {
                    return errorCode;
                }

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public String getMessage() {
                    return message;
                }

                public void setMessage(String message) {
                    this.message = message;
                }
            }
        }
    }

    public static class BodyEntity {
        /**
         * spId : 1
         * spName : Airtel
         * circleId : 15
         * circleName : Mumbai
         * connType : 1
         * source : 0
         * key : 7715049619
         */

        private List<PhoneDataEntity> phoneData;

        public List<PhoneDataEntity> getPhoneData() {
            return phoneData;
        }

        public void setPhoneData(List<PhoneDataEntity> phoneData) {
            this.phoneData = phoneData;
        }

        public static class PhoneDataEntity {
            private String spId;
            private String spName;
            private String circleId;
            private String circleName;
            private String connType;
            private String source;
            private String phone;

            public String getSpId() {
                return spId;
            }

            public void setSpId(String spId) {
                this.spId = spId;
            }

            public String getSpName() {
                return spName;
            }

            public void setSpName(String spName) {
                this.spName = spName;
            }

            public String getCircleId() {
                return circleId;
            }

            public void setCircleId(String circleId) {
                this.circleId = circleId;
            }

            public String getCircleName() {
                return circleName;
            }

            public void setCircleName(String circleName) {
                this.circleName = circleName;
            }

            public String getConnType() {
                return connType;
            }

            public void setConnType(String connType) {
                this.connType = connType;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }
        }
    }
}
