package com.getsmartapp.lib.model;

/**
 * @author  bishwajeet.kumar on 20/07/15.
 */
public class GetReferredDetails {

    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        private String gcExpiryDate;
        private String inviteeGC;
        private String referredBy;
        private String isActiveInviteeGC;
        private String referrerId;

        public void setGcExpiryDate(String gcExpiryDate) {
            this.gcExpiryDate = gcExpiryDate;
        }

        public void setInviteeGC(String inviteeGC) {
            this.inviteeGC = inviteeGC;
        }

        public void setReferredBy(String referredBy) {
            this.referredBy = referredBy;
        }

        public void setIsActiveInviteeGC(String isActiveInviteeGC) {
            this.isActiveInviteeGC = isActiveInviteeGC;
        }

        public void setReferrerId(String referrerId) {
            this.referrerId = referrerId;
        }

        public String getGcExpiryDate() {
            return gcExpiryDate;
        }

        public String getInviteeGC() {
            return inviteeGC;
        }

        public String getReferredBy() {
            return referredBy;
        }

        public String getIsActiveInviteeGC() {
            return isActiveInviteeGC;
        }

        public String getReferrerId() {
            return referrerId;
        }
    }

    public class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
