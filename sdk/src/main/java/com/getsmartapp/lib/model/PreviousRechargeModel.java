package com.getsmartapp.lib.model;

/**
 * @author Shalakha.Gupta on 28-05-2015.
 */
public class PreviousRechargeModel {

    private String number;
    private String operator;
    private String amount;
    private int service_type;
    private int operatorId;
    private String mRechargeTitle;
    private String mSubRechargeTitle;
    public PreviousRechargeModel() {}
    public PreviousRechargeModel(String number, String operator, String amount, int service_type, int operatorId, String mRechargeTitle, String mSubRechargeTitle) {
        this.number = number;
        this.operator = operator;
        this.amount = amount;
        this.service_type = service_type;
        this.operatorId = operatorId;
        this.mRechargeTitle = mRechargeTitle;
        this.mSubRechargeTitle = mSubRechargeTitle;
    }

    public String getmRechargeTitle() {
        return mRechargeTitle;
    }

    public void setmRechargeTitle(String mRechargeTitle) {
        this.mRechargeTitle = mRechargeTitle;
    }

    public String getmSubRechargeTitle() {
        return mSubRechargeTitle;
    }

    public void setmSubRechargeTitle(String mSubRechargeTitle) {
        this.mSubRechargeTitle = mSubRechargeTitle;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getService_type() {
        return service_type;
    }

    public void setService_type(int service_type) {
        this.service_type = service_type;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }
}
