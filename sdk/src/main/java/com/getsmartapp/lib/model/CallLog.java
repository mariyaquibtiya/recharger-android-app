package com.getsmartapp.lib.model;

/**
 * @author Jayant.B on 7/23/2015.
 */
public class CallLog {
    private String mPhoneNumber;
    private String mOperatorName;
    private String mPersonName;
    private int mDurationInMinute;
    private int mFrequency;

    public int getmFrequency() {
        return mFrequency;
    }

    public void setmFrequency(int mFrequency) {
        this.mFrequency = mFrequency;
    }

    @Override
    public String toString() {
        return "CallLog{" +
                "mPhoneNumber='" + mPhoneNumber + '\'' +
                ", mOperatorName='" + mOperatorName + '\'' +
                ", mPersonName='" + mPersonName + '\'' +
                ", mDurationInMinute=" + mDurationInMinute +
                '}';
    }

    public String getmPhoneNumber() {
        return mPhoneNumber;
    }

    public void setmPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public String getmOperatorName() {
        return mOperatorName;
    }

    public void setmOperatorName(String mOperatorName) {
        this.mOperatorName = mOperatorName;
    }

    public String getmPersonName() {
        return mPersonName;
    }

    public void setmPersonName(String mPersonName) {
        this.mPersonName = mPersonName;
    }

    public int getmDurationInMinute() {
        return mDurationInMinute;
    }

    public void setmDurationInMinute(int mDurationInMinute) {
        this.mDurationInMinute = mDurationInMinute;
    }

    public CallLog(String mPhoneNumber, String mOperatorName, String mPersonName, int mDurationInMinute) {
        this.mPhoneNumber = mPhoneNumber;
        this.mOperatorName = mOperatorName;
        this.mPersonName = mPersonName;
        this.mDurationInMinute = mDurationInMinute;
    }
}
