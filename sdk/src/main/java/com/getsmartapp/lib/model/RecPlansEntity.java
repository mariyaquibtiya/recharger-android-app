package com.getsmartapp.lib.model;

/**
 * @author shalakha.gupta on 13/08/15.
 */
public class RecPlansEntity {
    /**
     * cost : 5912.276433819253
     * planDetail : {"serviceType":"PREPAID_MOBILE","amount":299,"serviceProviderId":1,"rechargeValue":0,"description":"650 Local Minutes","serviceProvider":"Airtel","planId":631,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}
     */
    private double savingsAbs;
    private double savingsPercentage;
    private double cost;
    private PlanDetailEntity planDetail;

    public double getSavingsAbs() {
        return savingsAbs;
    }

    public void setSavingsAbs(double savingsAbs) {
        this.savingsAbs = savingsAbs;
    }

    public double getSavingsPercentage() {
        return savingsPercentage;
    }

    public void setSavingsPercentage(double savingsPercentage) {
        this.savingsPercentage = savingsPercentage;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setPlanDetail(PlanDetailEntity planDetail) {
        this.planDetail = planDetail;
    }

    public double getCost() {
        return cost;
    }

    public PlanDetailEntity getPlanDetail() {
        return planDetail;
    }

    public class PlanDetailEntity {
        /**
         * serviceType : PREPAID_MOBILE
         * amount : 299
         * serviceProviderId : 1
         * rechargeValue : 0
         * description : 650 Local Minutes
         * serviceProvider : Airtel
         * planId : 631
         * circleId : 5
         * validity : 28 Days
         * circle : Delhi NCR
         * category : Discounted Tariff Recharge
         * isExpired : false
         * categoryId : 9
         */
        private String serviceType;
        private int amount;
        private int serviceProviderId;
        private int rechargeValue;
        private String description;
        private String serviceProvider;
        private int planId;
        private int circleId;
        private String validity;
        private String circle;
        private String category;
        private boolean isExpired;
        private int categoryId;

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public void setServiceProviderId(int serviceProviderId) {
            this.serviceProviderId = serviceProviderId;
        }

        public void setRechargeValue(int rechargeValue) {
            this.rechargeValue = rechargeValue;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setServiceProvider(String serviceProvider) {
            this.serviceProvider = serviceProvider;
        }

        public void setPlanId(int planId) {
            this.planId = planId;
        }

        public void setCircleId(int circleId) {
            this.circleId = circleId;
        }

        public void setValidity(String validity) {
            this.validity = validity;
        }

        public void setCircle(String circle) {
            this.circle = circle;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public void setIsExpired(boolean isExpired) {
            this.isExpired = isExpired;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getServiceType() {
            return serviceType;
        }

        public int getAmount() {
            return amount;
        }

        public int getServiceProviderId() {
            return serviceProviderId;
        }

        public int getRechargeValue() {
            return rechargeValue;
        }

        public String getDescription() {
            return description;
        }

        public String getServiceProvider() {
            return serviceProvider;
        }

        public int getPlanId() {
            return planId;
        }

        public int getCircleId() {
            return circleId;
        }

        public String getValidity() {
            return validity;
        }

        public String getCircle() {
            return circle;
        }

        public String getCategory() {
            return category;
        }

        public boolean isIsExpired() {
            return isExpired;
        }

        public int getCategoryId() {
            return categoryId;
        }
    }
}