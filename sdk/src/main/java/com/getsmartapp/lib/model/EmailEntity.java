package com.getsmartapp.lib.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Jayant.B on 7/22/2015.
 */
public class EmailEntity implements Parcelable {
    /**
     * verified : yes
     * email : amit.chouhan@timesinternet.in
     * primary : 1
     */
    private String verified;
    private String email;
    private String primary;
    private String status;

    protected EmailEntity(Parcel in) {
        verified = in.readString();
        email = in.readString();
        primary = in.readString();
        status = in.readString();
    }

    public static final Creator<EmailEntity> CREATOR = new Creator<EmailEntity>() {
        @Override
        public EmailEntity createFromParcel(Parcel in) {
            return new EmailEntity(in);
        }

        @Override
        public EmailEntity[] newArray(int size) {
            return new EmailEntity[size];
        }
    };

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getVerified() {
        return verified;
    }

    public String getEmail() {
        return email;
    }

    public String getPrimary() {
        return primary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(verified);
        dest.writeString(email);
        dest.writeString(primary);
        dest.writeString(status);
    }
}
