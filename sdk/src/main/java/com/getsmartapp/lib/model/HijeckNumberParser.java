package com.getsmartapp.lib.model;

/**
 * @author nitesh.verma on 10/27/15.
 */
public class HijeckNumberParser {

    /**
     * status : 1
     */

    private HeaderEntity header;
    /**
     * isHijacked : 1
     */

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    public static class BodyEntity {
        private int isHijacked;

        public void setIsHijacked(int isHijacked) {
            this.isHijacked = isHijacked;
        }

        public int getIsHijacked() {
            return isHijacked;
        }
    }
}
