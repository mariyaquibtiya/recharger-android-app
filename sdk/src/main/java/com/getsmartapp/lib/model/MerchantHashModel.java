package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 22/03/16.
 */
public class MerchantHashModel {

    private HeaderEntity header;

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;
        public void setStatus(String status) {
            this.status = status;
        }
        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 602
             * message : Error in walletBalanceRequest API request
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }

    }

    public static class BodyEntity {

//        {"header":{"status":"1"},"body":{"userSavedCardList":[{"uscId":1,"pgSlug":"payu","ssoId":"abcd@123","cardToken":"qwert","merchantHash":"randomhash12","createdOn":"Mar 29, 2016"}]}}

        private List<DataEntity> userSavedCardList;
         public List<DataEntity> getDataEntity() {
            return userSavedCardList;
        }

        public void setDataEntity(List<DataEntity> dataEntity) {
            this.userSavedCardList = dataEntity;
        }


        public class DataEntity{
            String cardToken,merchantHash;

            public String getCard_token() {
                return cardToken;
            }

            public void setCard_token(String card_token) {
                this.cardToken = card_token;
            }

            public String getMerchant_hash() {
                return merchantHash;
            }

            public void setMerchant_hash(String merchant_hash) {
                this.merchantHash = merchant_hash;
            }
        }


    }
}
