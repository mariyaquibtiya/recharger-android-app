package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 29/07/15.
 */
public class OrderSummaryBean {
    /**
     * body : {"message":"Some of your recharges are still in process. We will keep you informed of the status of pending items.","promoCodeDiscount":0,"orderDate":"Jun 20, 2015 1:22:03 AM","netAmountPaid":5,"totalAmount":5,"paymentStatus":"PMNT_SUCCESSFUL","orderItemStatusList":[{"amount":5,"message":"Your recharge is still in process. Operator is yet to confirm payment.","spName":"Vodafone","rechargeStatus":"RECHARGE_DELAYED_OPERATOR_SIDE","rechargeTo":"9811158771","orderDate":"20-Jun-15","mobikwikReqId":"143474354778297503613410","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"seqId":0,"orderId":9750361341}],"orderStatus":"ORDER_PENDING","orderId":9750361341}
     * header : {"errors":{"errorList":[{"message":"No Plans Found.","errorCode":"102"}]},"status":"1"}
     */
    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        /**
         * message : Some of your recharges are still in process. We will keep you informed of the status of pending items.
         * promoCodeDiscount : 0.0
         * orderDate : Jun 20, 2015 1:22:03 AM
         * netAmountPaid : 5.0
         * totalAmount : 5.0
         * paymentStatus : PMNT_SUCCESSFUL
         * orderItemStatusList : [{"amount":5,"message":"Your recharge is still in process. Operator is yet to confirm payment.","spName":"Vodafone","rechargeStatus":"RECHARGE_DELAYED_OPERATOR_SIDE","rechargeTo":"9811158771","orderDate":"20-Jun-15","mobikwikReqId":"143474354778297503613410","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"seqId":0,"orderId":9750361341}]
         * orderStatus : ORDER_PENDING
         * orderId : 9750361341
         */
        private String message;
        private double promoCodeDiscount;
        private String orderDate;
        private double netAmountPaid;
        private double totalAmount;
        private String paymentStatus;
        private List<OrderHistoryBean.BodyEntity.OrdersListEntity.OrderItemStatusListEntity> orderItemStatusList;
        private String orderStatus;
        private long orderId;
        private String promoCode;
        private String cardMessage;
        private double   walletAmount;
        private double  walletBalance;
        private double recWalletBalance;
        private String bankName;
        private String cardBrand;
        private  String paymentMode;
        private String maskedCardNo;
        private String walletPaymentStatus;

        public void setMessage(String message) {
            this.message = message;
        }

        public void setPromoCodeDiscount(double promoCodeDiscount) {
            this.promoCodeDiscount = promoCodeDiscount;
        }
        public void setPromoCode(String promoCode) {
            this.promoCode = promoCode;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public void setNetAmountPaid(double netAmountPaid) {
            this.netAmountPaid = netAmountPaid;
        }

        public void setTotalAmount(double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public void setOrderItemStatusList(List<OrderHistoryBean.BodyEntity.OrdersListEntity.OrderItemStatusListEntity> orderItemStatusList) {
            this.orderItemStatusList = orderItemStatusList;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public String getMessage() {
            return message;
        }

        public String getCardMessage() {
            return cardMessage;
        }

        public void setCardMessage(String cardMessage) {
            this.cardMessage = cardMessage;
        }

        public double getPromoCodeDiscount() {
            return promoCodeDiscount;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public double getNetAmountPaid() {
            return netAmountPaid;
        }

        public double getTotalAmount() {
            return totalAmount;
        }
        public String getPromoCode() {
            return promoCode;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public List<OrderHistoryBean.BodyEntity.OrdersListEntity.OrderItemStatusListEntity> getOrderItemStatusList() {
            return orderItemStatusList;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public long getOrderId() {
            return orderId;
        }

        public double getWalletAmount() {
            return walletAmount;
        }

        public void setWalletAmount(double walletAmount) {
            this.walletAmount = walletAmount;
        }

        public double getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(double walletAmount) {
            this.walletBalance = walletAmount;
        }


        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getCardPlatformName() {
            return cardBrand;
        }

        public void setCardPlatformName(String cardPlatformName) {
            this.cardBrand = cardBrand;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getMaskedCardNumber() {
            return maskedCardNo;
        }

        public void setMaskedCardNumber(String maskedCardNumber) {
            this.maskedCardNo = maskedCardNumber;
        }

        public double getRecWalletBalance() {
            return recWalletBalance;
        }

        public void setRecWalletBalance(double recWalletBalance) {
            this.recWalletBalance = recWalletBalance;
        }

        public String getWalletPaymentStatus() {
            return walletPaymentStatus;
        }

        public void setWalletPaymentStatus(String walletPaymentStatus) {
            this.walletPaymentStatus = walletPaymentStatus;
        }
    }

    public class HeaderEntity {
        /**
         * errors : {"errorList":[{"message":"No Plans Found.","errorCode":"102"}]}
         * status : 1
         */
        private ErrorsEntity errors;
        private String status;

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public String getStatus() {
            return status;
        }

        public class ErrorsEntity {
            /**
             * errorList : [{"message":"No Plans Found.","errorCode":"102"}]
             */
            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public class ErrorListEntity {
                /**
                 * message : No Plans Found.
                 * errorCode : 102
                 */
                private String message;
                private String errorCode;

                public void setMessage(String message) {
                    this.message = message;
                }

                public void setErrorCode(String errorCode) {
                    this.errorCode = errorCode;
                }

                public String getMessage() {
                    return message;
                }

                public String getErrorCode() {
                    return errorCode;
                }
            }
        }
    }
}
