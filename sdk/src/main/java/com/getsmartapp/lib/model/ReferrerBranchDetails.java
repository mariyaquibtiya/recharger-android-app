package com.getsmartapp.lib.model;

/**
 * @author bishwajeet.kumar on 29/07/15.
 */
public class ReferrerBranchDetails {

    /**
     * body : {"current_user_channel":"4zuahk5e88ighg3qmteeo7al3","current_branch_user_identity":"4zuahk5e88ighg3qmteeo7al3","current_branch_user_name":"abhinava","current_user_image_url":"abs"}
     * header : {"status":"1"}
     */
    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public static class BodyEntity {
        /**
         * current_user_channel : 4zuahk5e88ighg3qmteeo7al3
         * current_branch_user_identity : 4zuahk5e88ighg3qmteeo7al3
         * current_branch_user_name : abhinava
         * current_user_image_url : abs
         */
        private String current_user_channel;
        private String current_branch_user_identity;
        private String current_branch_user_name;
        private String current_user_image_url;

        public void setCurrent_user_channel(String current_user_channel) {
            this.current_user_channel = current_user_channel;
        }

        public void setCurrent_branch_user_identity(String current_branch_user_identity) {
            this.current_branch_user_identity = current_branch_user_identity;
        }

        public void setCurrent_branch_user_name(String current_branch_user_name) {
            this.current_branch_user_name = current_branch_user_name;
        }

        public void setCurrent_user_image_url(String current_user_image_url) {
            this.current_user_image_url = current_user_image_url;
        }

        public String getCurrent_user_channel() {
            return current_user_channel;
        }

        public String getCurrent_branch_user_identity() {
            return current_branch_user_identity;
        }

        public String getCurrent_branch_user_name() {
            return current_branch_user_name;
        }

        public String getCurrent_user_image_url() {
            return current_user_image_url;
        }
    }

    public static class HeaderEntity {
        /**
         * status : 1
         */
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
