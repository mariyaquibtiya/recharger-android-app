package com.getsmartapp.lib.model;

/**
 * @author Jayant.B on 7/23/2015.
 */
public class RecentContactModel implements Comparable {
    private String name = "";
    private int duration = 0;
    private String phone = "";

    public RecentContactModel(String name, String phone, int duration) {
        this.name = name;
        this.duration = duration;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getduration() {
        return duration;
    }

    public void setduration(int duration) {
        this.duration = duration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int compareTo(Object another) {
        RecentContactModel model = (RecentContactModel) another;
        return (this.duration < model.duration) ? 1 : (this.duration > model.duration) ? -1 : 0;
    }
}
