package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 02/09/15.
 */
public class InitRechargeModel {
    /**
     * body : {"wsHash":"7c59061563ea5fdaef439c86304d57d132a460cc7b4bb008b87ce3e38ebaa137535652cf23c2f22f06b65dc7f6bb1af5c0e33f199e10574e8efce3bbb8d37c87","orderId":11503206247,"paymentHash":"b50178807da46417c812eb5b48b89322f21791f86ca1cbe4a591e03dff5f355e50aad0ff5d02a92d964ab9c62329127857e47ea092236ef296eed88624d8eaeb","merchantKey":"iwhOel:c6ygfo5cqfdx7q2avci0hkovi"}
     * header : {"status":"1"}
     */
    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        /**
         * wsHash : 7c59061563ea5fdaef439c86304d57d132a460cc7b4bb008b87ce3e38ebaa137535652cf23c2f22f06b65dc7f6bb1af5c0e33f199e10574e8efce3bbb8d37c87
         * orderId : 11503206247
         * paymentHash : b50178807da46417c812eb5b48b89322f21791f86ca1cbe4a591e03dff5f355e50aad0ff5d02a92d964ab9c62329127857e47ea092236ef296eed88624d8eaeb
         * merchantKey : iwhOel:c6ygfo5cqfdx7q2avci0hkovi
         */
        private String wsHash;
        private long orderId;
        private String paymentHash;
        private String merchantKey;

        public void setWsHash(String wsHash) {
            this.wsHash = wsHash;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public void setPaymentHash(String paymentHash) {
            this.paymentHash = paymentHash;
        }

        public void setMerchantKey(String merchantKey) {
            this.merchantKey = merchantKey;
        }

        public String getWsHash() {
            return wsHash;
        }

        public long getOrderId() {
            return orderId;
        }

        public String getPaymentHash() {
            return paymentHash;
        }

        public String getMerchantKey() {
            return merchantKey;
        }
    }

//    public class HeaderEntity {
//        /**
//         * status : 1
//         */
//        private String status;
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//    }


    public class HeaderEntity {
        /**
         * errors : {"errorList":[{"message":"No Plans Found.","errorCode":"102"}]}
         * status : 1
         */
        private ErrorsEntity errors;
        private String status;

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public String getStatus() {
            return status;
        }

        public class ErrorsEntity {
            /**
             * errorList : [{"message":"No Plans Found.","errorCode":"102"}]
             */
            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public class ErrorListEntity {
                /**
                 * message : No Plans Found.
                 * errorCode : 102
                 */
                private String message;
                private String errorCode;

                public void setMessage(String message) {
                    this.message = message;
                }

                public void setErrorCode(String errorCode) {
                    this.errorCode = errorCode;
                }

                public String getMessage() {
                    return message;
                }

                public String getErrorCode() {
                    return errorCode;
                }
            }
        }
    }
}
