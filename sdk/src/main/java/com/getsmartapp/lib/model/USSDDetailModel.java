package com.getsmartapp.lib.model;

/**
 * @author bishwajeet.kumar on 16/03/16.
 */
public class USSDDetailModel {

    private String title;
    private String description;
    private int type;
    private String defaultDialCode;
    private String updatedDialCode;
    private String dialInstruction;
    private String defaultSMSCode;
    private String updatedSMSCode;
    private String smsMessage;
    private String smsInstruction;

    public USSDDetailModel(String title, String description, int type, String defaultDialCode, String updatedDialCode, String dialInstruction, String defaultSMSCode, String updatedSMSCode, String smsMessage, String smsInstruction) {
        this.title = title;
        this.description = description;
        this.type = type;
        this.defaultDialCode = defaultDialCode;
        this.updatedDialCode = updatedDialCode;
        this.dialInstruction = dialInstruction;
        this.defaultSMSCode = defaultSMSCode;
        this.updatedSMSCode = updatedSMSCode;
        this.smsMessage = smsMessage;
        this.smsInstruction = smsInstruction;
    }

    public USSDDetailModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDefaultDialCode() {
        return defaultDialCode;
    }

    public void setDefaultDialCode(String defaultDialCode) {
        this.defaultDialCode = defaultDialCode;
    }

    public String getUpdatedDialCode() {
        return updatedDialCode;
    }

    public void setUpdatedDialCode(String updatedDialCode) {
        this.updatedDialCode = updatedDialCode;
    }

    public String getDialInstruction() {
        return dialInstruction;
    }

    public void setDialInstruction(String dialInstruction) {
        this.dialInstruction = dialInstruction;
    }

    public String getDefaultSMSCode() {
        return defaultSMSCode;
    }

    public void setDefaultSMSCode(String defaultSMSCode) {
        this.defaultSMSCode = defaultSMSCode;
    }

    public String getUpdatedSMSCode() {
        return updatedSMSCode;
    }

    public void setUpdatedSMSCode(String updatedSMSCode) {
        this.updatedSMSCode = updatedSMSCode;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getSmsInstruction() {
        return smsInstruction;
    }

    public void setSmsInstruction(String smsInstruction) {
        this.smsInstruction = smsInstruction;
    }

    public String getString() {
        return title + ", " + description + ", Dial Code: " + defaultDialCode + ", SMS Code: " + defaultSMSCode;
    }
}
