package com.getsmartapp.lib.model;

/**
 * @author bishwajeet.kumar on 20/07/15.
 */
public class GenerateReferralCode {

    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        private String referralCode;
        private String referralLink;
        private String ssoId;

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }

        public void setReferralLink(String referralLink) {
            this.referralLink = referralLink;
        }

        public void setSsoId(String ssoId) {
            this.ssoId = ssoId;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public String getReferralLink() {
            return referralLink;
        }

        public String getSsoId() {
            return ssoId;
        }
    }

    public class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
