package com.getsmartapp.lib.model;

/**
 * @author nitesh.verma on 29/07/15.
 */
public class DataAggregationParser {


    /**
     * message : {"insertedCount":0,"matchedCount":0,"removedCount":0,"modifiedCount":0,"upserts":[{"index":0,"id":{"timestamp":1449731087,"machineIdentifier":1658417,"processIdentifier":3163,"counter":11744301}}]}
     */

    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
