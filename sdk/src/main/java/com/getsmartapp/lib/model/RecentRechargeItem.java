package com.getsmartapp.lib.model;

/**
 * @author nitesh.verma on 9/20/15.
 */
public class RecentRechargeItem {
    String provider_name;
    String number;
    int recamount;
    int stype;
    int catId;
    int spId;

    public RecentRechargeItem(String provider_name, String number, int recamount,int stype,int carid,int spId) {
        this.provider_name = provider_name;
        this.number = number;
        this.recamount = recamount;
        this.stype = stype;
        this.catId = carid;
        this.spId = spId;
    }

    public String getProvider_name() {
        return provider_name;
    }


    public String getNumber() {
        return number;
    }

    public int getRecamount() {
        return recamount;
    }

    public int getStype() {
        return stype;
    }

    public int getCatId() {
        return catId;
    }


    public int getSpId() {
        return spId;
    }

    public void setSpId(int spId) {
        this.spId = spId;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RecentRechargeItem)){
            return false;
        }if (o==null){
            return false;
        }
        if (((RecentRechargeItem) o).getNumber().equalsIgnoreCase(this.getNumber())
                && ((RecentRechargeItem)o).getProvider_name().equalsIgnoreCase(this.getProvider_name())
                && ((RecentRechargeItem)o).getRecamount()==this.getRecamount()
                && ((RecentRechargeItem)o).getStype()==this.getStype()
                && ((RecentRechargeItem) o).getCatId()==this.getCatId()
                && ((RecentRechargeItem) o).getSpId()==this.getSpId()){
            return true;
        }else {
            return false;
        }
    }
}
