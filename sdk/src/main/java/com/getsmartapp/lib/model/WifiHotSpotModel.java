package com.getsmartapp.lib.model;

import java.io.Serializable;

/**
 * @author Peeyush.Singh on 09-05-2016.
 */
public class WifiHotSpotModel implements Serializable {
    private String ssidName;
    private float wifi_dataUsed;

    public WifiHotSpotModel(String ssidName, float total_dataUsed) {
        this.ssidName = ssidName;
        this.wifi_dataUsed = total_dataUsed;
    }

    public String getSSIDName() {
        return ssidName;
    }

    public float getWifi_dataUsed() {
        return wifi_dataUsed;
    }


    @Override
    public boolean equals(Object o) {
        return super.equals(o);

    }
}