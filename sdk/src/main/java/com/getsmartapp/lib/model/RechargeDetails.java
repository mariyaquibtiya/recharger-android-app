package com.getsmartapp.lib.model;

import java.io.Serializable;

/**
 * @author Shalakha.Gupta on 15-05-2015.
 */
public class RechargeDetails implements Serializable{

    private int seqId;
    private int planId;
    private long operator;
    private double circle;
    private int type;
    private int qty;
    private String rechargeToNo;
    private int isSpecialRecharge;
    private double itemAmount;
    private double itemDiscount;
    private double itemPayableAmount;
    private String rechargeType;
    private int operatorId;
    private int categoryId;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getSeqId() {
        return seqId;
    }

    public void setSeqId(int seqId) {
        this.seqId = seqId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public long getOperator() {
        return operator;
    }

    public void setOperator(long operator) {
        this.operator = operator;
    }

    public double getCircle() {
        return circle;
    }

    public void setCircle(double circle) {
        this.circle = circle;
    }

    public int getSType() {
        return type;
    }

    public void setSType(int type) {
        this.type = type;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getRechargeToNo() {
        return rechargeToNo;
    }

    public void setRechargeToNo(String rechargeToNo) {
        this.rechargeToNo = rechargeToNo;
    }

    public int getIsSpecialRecharge() {
        return isSpecialRecharge;
    }

    public void setIsSpecialRecharge(int isSpecialRecharge) {
        this.isSpecialRecharge = isSpecialRecharge;
    }

    public double getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(double itemAmount) {
        this.itemAmount = itemAmount;
    }

    public double getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(double itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public double getItemPayableAmount() {
        return itemPayableAmount;
    }

    public void setItemPayableAmount(double itemPayableAmount) {
        this.itemPayableAmount = itemPayableAmount;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }
}
