package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 26/11/15.
 */
public class PromoCodeModel1 {
    /**
     * status : 1
     */

    private HeaderEntity header;
    /**
     * hasMadeFirstTxn : true
     * validationResponse : {"userId":"brucewaynebtmn69@gmail.com","altId":"8ck2yqx12e8c65cg6e1houp84","gcId":"HARSHAL123","merchant":"recharger","orderAmount":"10.0","totalAmount":"0.0","amount":"10.0","status":"SUCCESS","gcMergable":"false","gcPartialRedeem":"false","offer":"","userLock":"false","refId":"78623336","productAmount":["10.0"],"catalog":"1","category":"1","product":"1","type":"PROMOTIONAL","paid":"true","ruleEnabled":"false"}
     */

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    public static class BodyEntity {
        private boolean hasMadeFirstTxn;
        /**
         * userId : brucewaynebtmn69@gmail.com
         * altId : 8ck2yqx12e8c65cg6e1houp84
         * gcId : HARSHAL123
         * merchant : recharger
         * orderAmount : 10.0
         * totalAmount : 0.0
         * amount : 10.0
         * status : SUCCESS
         * gcMergable : false
         * gcPartialRedeem : false
         * offer :
         * userLock : false
         * refId : 78623336
         * productAmount : ["10.0"]
         * catalog : 1
         * category : 1
         * product : 1
         * type : PROMOTIONAL
         * paid : true
         * ruleEnabled : false
         */

        private ValidationResponseEntity validationResponse;

        private boolean isGCConsumed;
        public void setHasMadeFirstTxn(boolean hasMadeFirstTxn) {
            this.hasMadeFirstTxn = hasMadeFirstTxn;
        }

        public void setValidationResponse(ValidationResponseEntity validationResponse) {
            this.validationResponse = validationResponse;
        }

        public boolean isHasMadeFirstTxn() {
            return hasMadeFirstTxn;
        }

        public ValidationResponseEntity getValidationResponse() {
            return validationResponse;
        }

        public boolean isGCConsumed() {
            return isGCConsumed;
        }

        public void setGCConsumed(boolean GCConsumed) {
            isGCConsumed = GCConsumed;
        }

        public static class ValidationResponseEntity {

            private String userId="";
            private String altId="";
            private String gcId="";
            private String merchant="";
            private String orderAmount="";
            private String totalAmount="";
            private String amount="";
            private String status="";
            private String gcMergable="";
            private String gcPartialRedeem="";
            private String offer="";
            private String userLock="";
            private String refId="";
            private String catalog="";
            private String category="";
            private String product="";
            private String type="";
            private String paid="";
            private String ruleEnabled="";
            private List<String> productAmount;
            private String error="";
            private int errorCode;
            private boolean firstUse;
            private String reason="";
            private String csHash="";

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public void setAltId(String altId) {
                this.altId = altId;
            }

            public void setGcId(String gcId) {
                this.gcId = gcId;
            }

            public void setMerchant(String merchant) {
                this.merchant = merchant;
            }

            public void setOrderAmount(String orderAmount) {
                this.orderAmount = orderAmount;
            }

            public void setTotalAmount(String totalAmount) {
                this.totalAmount = totalAmount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public void setGcMergable(String gcMergable) {
                this.gcMergable = gcMergable;
            }

            public void setGcPartialRedeem(String gcPartialRedeem) {
                this.gcPartialRedeem = gcPartialRedeem;
            }

            public void setOffer(String offer) {
                this.offer = offer;
            }

            public void setUserLock(String userLock) {
                this.userLock = userLock;
            }

            public void setRefId(String refId) {
                this.refId = refId;
            }

            public void setCatalog(String catalog) {
                this.catalog = catalog;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public void setProduct(String product) {
                this.product = product;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setPaid(String paid) {
                this.paid = paid;
            }

            public void setRuleEnabled(String ruleEnabled) {
                this.ruleEnabled = ruleEnabled;
            }

            public void setProductAmount(List<String> productAmount) {
                this.productAmount = productAmount;
            }

            public String getUserId() {
                return userId;
            }

            public String getAltId() {
                return altId;
            }

            public String getGcId() {
                return gcId;
            }

            public String getMerchant() {
                return merchant;
            }

            public String getOrderAmount() {
                return orderAmount;
            }

            public String getTotalAmount() {
                return totalAmount;
            }

            public String getAmount() {
                return amount;
            }

            public String getStatus() {
                return status;
            }

            public String getGcMergable() {
                return gcMergable;
            }

            public String getGcPartialRedeem() {
                return gcPartialRedeem;
            }

            public String getOffer() {
                return offer;
            }

            public String getUserLock() {
                return userLock;
            }

            public String getRefId() {
                return refId;
            }

            public String getCatalog() {
                return catalog;
            }

            public String getCategory() {
                return category;
            }

            public String getProduct() {
                return product;
            }

            public String getType() {
                return type;
            }

            public String getPaid() {
                return paid;
            }

            public String getRuleEnabled() {
                return ruleEnabled;
            }

            public List<String> getProductAmount() {
                return productAmount;
            }

            public String getError() {
                return error;
            }

            public void setError(String error) {
                this.error = error;
            }

            public int getErrorCode() {
                return errorCode;
            }

            public void setErrorCode(int errorCode) {
                this.errorCode = errorCode;
            }

            public boolean getFirstUse() {
                return firstUse;
            }

            public void setFirstUse(boolean firstUse) {
                this.firstUse = firstUse;
            }

            public String getReason() {
                return reason;
            }

            public void setReason(String reason) {
                this.reason = reason;
            }

            public String getCsHash() {
                return csHash;
            }

            public void setCsHash(String csHash) {
                this.csHash = csHash;
            }
        }
    }
}
