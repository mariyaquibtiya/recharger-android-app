package com.getsmartapp.lib.model;

import java.io.Serializable;

/**
 * @author Peeyush.Singh on 21-03-2016.
 */
public class CallClassificationMNPModel implements Serializable {

    private Header header;
    public Header getHeader() {
        return header;
    }

    public class Header implements  Serializable
    {
        private String status;
        public String getStatus() {
            return status;
        }
    }

    private Body body;

    public Body getBody() {
        return body;
    }
    public class Body implements Serializable
    {

        private PhoneData[] phoneData;

        public PhoneData[] getPhoneData() {
            return phoneData;
        }

        public class PhoneData implements Serializable
        {
            private String spId;
            private String spName;
            private String circleId;
            private String circleName;
            private String connType;
            private String source;
            private String phone;

            public String getSpId() {
                return spId;
            }

            public String getSpName() {
                return spName;
            }

            public String getCircleId() {
                return circleId;
            }

            public String getCircleName() {
                return circleName;
            }

            public String getConnType() {
                return connType;
            }

            public String getSource() {
                return source;
            }

            public String getPhone() {
                return phone;
            }
        }
    }

}
