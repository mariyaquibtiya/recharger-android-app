package com.getsmartapp.lib.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author nitesh.verma on 2/18/16.
 */
public class QueryResult implements Serializable{

    String sdkkey;
    String sdkvalue;
    String phone;
    String name;
    HashMap<String,Integer> hashMap;
    long timestamp;

    public QueryResult(String sdkkey, String sdkvalue,String phone,String name,long timestamp, HashMap<String,Integer> map) {
        this.sdkkey = sdkkey;
        this.sdkvalue = sdkvalue;
        this.hashMap = map;
        this.phone = phone;
        this.name=name;
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSdkkey() {
        return sdkkey;
    }

    public void setSdkkey(String sdkkey) {
        this.sdkkey = sdkkey;
    }

    public String getSdkvalue() {
        return sdkvalue;
    }

    public void setSdkvalue(String sdkvalue) {
        this.sdkvalue = sdkvalue;
    }

    public HashMap<String, Integer> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, Integer> hashMap) {
        this.hashMap = hashMap;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(sdkkey);
//        dest.writeString(sdkvalue);
//        dest.writeMap(hashMap);
//    }
//
//    public QueryResult(Parcel in) {
//        sdkkey = in.readString();
//        sdkvalue = in.readString();
//        hashMap = in.readHashMap(new ClassLoader() {
//            @Override
//            public Class<?> loadClass(String className) throws ClassNotFoundException {
//                return HashMap.class;
//            }
//        });
//    }
//
//    public static final Parcelable.Creator<QueryResult> CREATOR = new Parcelable.Creator<QueryResult>()
//    {
//        public QueryResult createFromParcel(Parcel in)
//        {
//            return new QueryResult(in);
//        }
//        public QueryResult[] newArray(int size)
//        {
//            return new QueryResult[size];
//        }
//    };
}
