package com.getsmartapp.lib.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bishwajeet.kumar on 12/08/15.
 */
public class CardDataModel {

    /**
     * pos_fixed : 0
     * receny_calc_reqd : 0
     * val_context_fallback : 0
     * parent_card_id : 0
     * val_recency_fallback : 0
     * coeff_context : 1
     * card_id : 27
     * card_visibility : 0
     * card_name : data_plan_update
     * context_calc_reqd : 0
     */
    private int pos_fixed;
    private int receny_calc_reqd;
    private int val_context_fallback;
    private int parent_card_id;
    private int val_recency_fallback;
    private float coeff_context;
    private int card_id;
    private int card_visibility;
    private String card_name;
    private int context_calc_reqd;
    private float score;
    private CardDataModel child_card;
    private String category_name;
    private String exclude_card;

    public CardDataModel getChild_card() {
        return child_card;
    }

    public void setChild_card(CardDataModel child_card) {
        this.child_card = child_card;
    }

    public static List<CardDataModel> arrayCardDataModelFromData(String str) {

        Type listType = new TypeToken<ArrayList<CardDataModel>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public void setPos_fixed(int pos_fixed) {
        this.pos_fixed = pos_fixed;
    }

    public void setReceny_calc_reqd(int receny_calc_reqd) {
        this.receny_calc_reqd = receny_calc_reqd;
    }

    public void setVal_context_fallback(int val_context_fallback) {
        this.val_context_fallback = val_context_fallback;
    }

    public void setParent_card_id(int parent_card_id) {
        this.parent_card_id = parent_card_id;
    }

    public void setVal_recency_fallback(int val_recency_fallback) {
        this.val_recency_fallback = val_recency_fallback;
    }

    public void setCoeff_context(float coeff_context) {
        this.coeff_context = coeff_context;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public void setCard_visibility(int card_visibility) {
        this.card_visibility = card_visibility;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public void setContext_calc_reqd(int context_calc_reqd) {
        this.context_calc_reqd = context_calc_reqd;
    }

    public int getPos_fixed() {
        return pos_fixed;
    }

    public int getReceny_calc_reqd() {
        return receny_calc_reqd;
    }

    public int getVal_context_fallback() {
        return val_context_fallback;
    }

    public int getParent_card_id() {
        return parent_card_id;
    }

    public int getVal_recency_fallback() {
        return val_recency_fallback;
    }

    public float getCoeff_context() {
        return coeff_context;
    }

    public int getCard_id() {
        return card_id;
    }

    public int getCard_visibility() {
        return card_visibility;
    }

    public String getCard_name() {
        return card_name;
    }

    public int getContext_calc_reqd() {
        return context_calc_reqd;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getExclude_card() {
        return exclude_card;
    }

    public void setExclude_card(String exclude_card) {
        this.exclude_card = exclude_card;
    }
}
