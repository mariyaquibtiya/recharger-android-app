package com.getsmartapp.lib.model;

import java.util.ArrayList;

/**
 * @author Shalakha.Gupta on 02-07-2015.
 */
public class GroupModel {

    String groupName;
    ArrayList<GroupMemberModel> membersList;
    int groupSize;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<GroupMemberModel> getMembersList() {
        return membersList;
    }

    public void setMembersList(ArrayList<GroupMemberModel> membersList) {
        this.membersList = membersList;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }
}
