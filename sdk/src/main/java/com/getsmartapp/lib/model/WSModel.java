package com.getsmartapp.lib.model;

/**
 * @author shalakha.gupta on 12/08/15.
 */
public class WSModel {

    /**
     * body : {"wsHash":"b53b2fec91c4d935161b8a5786c8eb2da68ea0c1921ed64779694e66573bea72a44a11481c1de788791d25485b2389b963ea5dc70e82e4e947d5323b65a47a3c"}
     * header : {"status":"1"}
     */
    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        /**
         * wsHash : b53b2fec91c4d935161b8a5786c8eb2da68ea0c1921ed64779694e66573bea72a44a11481c1de788791d25485b2389b963ea5dc70e82e4e947d5323b65a47a3c
         */
        private String wsHash;

        public void setWsHash(String wsHash) {
            this.wsHash = wsHash;
        }

        public String getWsHash() {
            return wsHash;
        }
    }

    public class HeaderEntity {
        /**
         * status : 1
         */
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
