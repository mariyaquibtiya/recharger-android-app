package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author nitesh.verma on 12/08/15.
 */
public class SpandCircle {


    /**
     * status : 0
     * errors : {"errorList":[{"errorCode":108,"message":"Service Temporarily Down.Please try again after some time."}]}
     */

    private HeaderEntity header;
    /**
     * series : 80071
     * circleId : 13
     * circle : Maharashtra
     * spId : 2
     * spName : Vodafone
     * conType : 0
     * source : series
     * vserv : {"mass":"http://waffiliatestg.vserv.com/review?adspotid=10023&c=Me004fca8b566f761c57b43a164d4ef60"}
     */

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;

        public void setStatus(String status) {
            this.status = status;
        }

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 108
             * message : Service Temporarily Down.Please try again after some time.
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }

    public static class BodyEntity {
        private String series;
        private int circleId;
        private String circle;
        private int spId;
        private String spName;
        private int conType;
        private String source;
        /**
         * mass : http://waffiliatestg.vserv.com/review?adspotid=10023&c=Me004fca8b566f761c57b43a164d4ef60
         */

        private VservEntity vserv;

        public void setSeries(String series) {
            this.series = series;
        }

        public void setCircleId(int circleId) {
            this.circleId = circleId;
        }

        public void setCircle(String circle) {
            this.circle = circle;
        }

        public void setSpId(int spId) {
            this.spId = spId;
        }

        public void setSpName(String spName) {
            this.spName = spName;
        }

        public void setConType(int conType) {
            this.conType = conType;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public void setVserv(VservEntity vserv) {
            this.vserv = vserv;
        }

        public String getSeries() {
            return series;
        }

        public int getCircleId() {
            return circleId;
        }

        public String getCircle() {
            return circle;
        }

        public int getSpId() {
            return spId;
        }

        public String getSpName() {
            return spName;
        }

        public int getConType() {
            return conType;
        }

        public String getSource() {
            return source;
        }

        public VservEntity getVserv() {
            return vserv;
        }

        public static class VservEntity {
            private String mass;

            public void setMass(String mass) {
                this.mass = mass;
            }

            public String getMass() {
                return mass;
            }
        }
    }
}
