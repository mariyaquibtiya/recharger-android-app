package com.getsmartapp.lib.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bishwajeet.kumar on 27/03/16.
 */
public class USSDResponseModel {

    /**
     * status : 1
     */

    private HeaderEntity header;
    /**
     * resultSize : 2
     * operatorId : 1
     * quickHelpDetails : [{"qhdId":1,"qhOperatorId":"1","description":"Find out your prepaid balance","qhRequestSlug":"talktime_balance","title":"Talktime Balance","dialCode":"*123#","dialInstr":"","smsCode":"","smsInstr":"","smsMsg":""},{"qhdId":3,"qhOperatorId":"1","description":"Find out your remaining hi-speed data balance","qhRequestSlug":"3g_balance","title":"3G Balance","dialCode":"*123*11#","dialInstr":"","smsCode":"","smsInstr":"","smsMsg":""}]
     */

    private BodyEntity body;

    public static List<USSDResponseModel> arrayUSSDResponseModelFromData(String str) {

        Type listType = new TypeToken<ArrayList<USSDResponseModel>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public static class HeaderEntity {
        private String status;

        public static List<HeaderEntity> arrayHeaderEntityFromData(String str) {

            Type listType = new TypeToken<ArrayList<HeaderEntity>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public static class BodyEntity {
        private int resultSize;
        private int operatorId;
        /**
         * qhdId : 1
         * qhOperatorId : 1
         * description : Find out your prepaid balance
         * qhRequestSlug : talktime_balance
         * title : Talktime Balance
         * dialCode : *123#
         * dialInstr :
         * smsCode :
         * smsInstr :
         * smsMsg :
         */

        private List<QuickHelpDetailsEntity> quickHelpDetails;

        public static List<BodyEntity> arrayBodyEntityFromData(String str) {

            Type listType = new TypeToken<ArrayList<BodyEntity>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public int getResultSize() {
            return resultSize;
        }

        public void setResultSize(int resultSize) {
            this.resultSize = resultSize;
        }

        public int getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(int operatorId) {
            this.operatorId = operatorId;
        }

        public List<QuickHelpDetailsEntity> getQuickHelpDetails() {
            return quickHelpDetails;
        }

        public void setQuickHelpDetails(List<QuickHelpDetailsEntity> quickHelpDetails) {
            this.quickHelpDetails = quickHelpDetails;
        }

        public static class QuickHelpDetailsEntity {
            private int qhdId;
            private String qhOperatorId;
            private String description;
            private String qhRequestSlug;
            private String title;
            private String dialCode;
            private String dialInstr;
            private String smsCode;
            private String smsInstr;
            private String smsMsg;

            public static List<QuickHelpDetailsEntity> arrayQuickHelpDetailsEntityFromData(String str) {

                Type listType = new TypeToken<ArrayList<QuickHelpDetailsEntity>>() {
                }.getType();

                return new Gson().fromJson(str, listType);
            }

            public int getQhdId() {
                return qhdId;
            }

            public void setQhdId(int qhdId) {
                this.qhdId = qhdId;
            }

            public String getQhOperatorId() {
                return qhOperatorId;
            }

            public void setQhOperatorId(String qhOperatorId) {
                this.qhOperatorId = qhOperatorId;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getQhRequestSlug() {
                return qhRequestSlug;
            }

            public void setQhRequestSlug(String qhRequestSlug) {
                this.qhRequestSlug = qhRequestSlug;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDialCode() {
                return dialCode;
            }

            public void setDialCode(String dialCode) {
                this.dialCode = dialCode;
            }

            public String getDialInstr() {
                return dialInstr;
            }

            public void setDialInstr(String dialInstr) {
                this.dialInstr = dialInstr;
            }

            public String getSmsCode() {
                return smsCode;
            }

            public void setSmsCode(String smsCode) {
                this.smsCode = smsCode;
            }

            public String getSmsInstr() {
                return smsInstr;
            }

            public void setSmsInstr(String smsInstr) {
                this.smsInstr = smsInstr;
            }

            public String getSmsMsg() {
                return smsMsg;
            }

            public void setSmsMsg(String smsMsg) {
                this.smsMsg = smsMsg;
            }
        }
    }
}
