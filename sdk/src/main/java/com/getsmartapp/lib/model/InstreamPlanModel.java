package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author nitesh.verma on 11/4/15.
 */
public class InstreamPlanModel {


    /**
     * status : 1
     */

    private HeaderEntity header;
    /**
     * categoryId : 9
     * plans : [{"planDetail":{"planId":17533,"serviceType":"PREPAID_MOBILE","serviceProviderId":2,"serviceProvider":"Vodafone","circleId":5,"circle":"Delhi NCR","amount":28,"rechargeValue":0,"validity":"28 Days","description":"All Local + STD calls @ 1.2p/sec","categoryId":9,"category":"Special","isExpired":false,"canRecommend":1,"validNumDays":28},"cost":2982.420866775461},{"planDetail":{"planId":17559,"serviceType":"PREPAID_MOBILE","serviceProviderId":2,"serviceProvider":"Vodafone","circleId":5,"circle":"Delhi NCR","amount":88,"rechargeValue":0,"validity":"28 Days","description":"All Local & STD calls @ 40p/min","categoryId":9,"category":"Special","isExpired":false,"canRecommend":1,"validNumDays":28},"cost":2996.194725389429}]
     */

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    public static class BodyEntity {
        private int categoryId;
        /**
         * planDetail : {"planId":17533,"serviceType":"PREPAID_MOBILE","serviceProviderId":2,"serviceProvider":"Vodafone","circleId":5,"circle":"Delhi NCR","amount":28,"rechargeValue":0,"validity":"28 Days","description":"All Local + STD calls @ 1.2p/sec","categoryId":9,"category":"Special","isExpired":false,"canRecommend":1,"validNumDays":28}
         * cost : 2982.420866775461
         */

        private List<PlansEntity> plans;

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public void setPlans(List<PlansEntity> plans) {
            this.plans = plans;
        }

        public int getCategoryId() {
            return categoryId;
        }

        public List<PlansEntity> getPlans() {
            return plans;
        }

        public static class PlansEntity {
            /**
             * planId : 17533
             * serviceType : PREPAID_MOBILE
             * serviceProviderId : 2
             * serviceProvider : Vodafone
             * circleId : 5
             * circle : Delhi NCR
             * amount : 28
             * rechargeValue : 0.0
             * validity : 28 Days
             * description : All Local + STD calls @ 1.2p/sec
             * categoryId : 9
             * category : Special
             * isExpired : false
             * canRecommend : 1
             * validNumDays : 28
             */

            private PlanDetailEntity planDetail;
            private double cost;
            private double savingsAbs;
            private double savingsPercentage;

            public void setPlanDetail(PlanDetailEntity planDetail) {
                this.planDetail = planDetail;
            }

            public void setCost(double cost) {
                this.cost = cost;
            }

            public double getSavingsAbs() {
                return savingsAbs;
            }

            public void setSavingsAbs(double savingsAbs) {
                this.savingsAbs = savingsAbs;
            }

            public double getSavingsPercentage() {
                return savingsPercentage;
            }

            public void setSavingsPercentage(double savingsPercentage) {
                this.savingsPercentage = savingsPercentage;
            }

            public PlanDetailEntity getPlanDetail() {
                return planDetail;
            }

            public double getCost() {
                return cost;
            }

            public static class PlanDetailEntity {
                private int planId;
                private String serviceType;
                private int serviceProviderId;
                private String serviceProvider;
                private int circleId;
                private String circle;
                private int amount;
                private double rechargeValue;
                private String validity;
                private String description;
                private int categoryId;
                private String category;
                private boolean isExpired;
                private int canRecommend;
                private int validNumDays;

                public void setPlanId(int planId) {
                    this.planId = planId;
                }

                public void setServiceType(String serviceType) {
                    this.serviceType = serviceType;
                }

                public void setServiceProviderId(int serviceProviderId) {
                    this.serviceProviderId = serviceProviderId;
                }

                public void setServiceProvider(String serviceProvider) {
                    this.serviceProvider = serviceProvider;
                }

                public void setCircleId(int circleId) {
                    this.circleId = circleId;
                }

                public void setCircle(String circle) {
                    this.circle = circle;
                }

                public void setAmount(int amount) {
                    this.amount = amount;
                }

                public void setRechargeValue(double rechargeValue) {
                    this.rechargeValue = rechargeValue;
                }

                public void setValidity(String validity) {
                    this.validity = validity;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public void setCategoryId(int categoryId) {
                    this.categoryId = categoryId;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public void setIsExpired(boolean isExpired) {
                    this.isExpired = isExpired;
                }

                public void setCanRecommend(int canRecommend) {
                    this.canRecommend = canRecommend;
                }

                public void setValidNumDays(int validNumDays) {
                    this.validNumDays = validNumDays;
                }

                public int getPlanId() {
                    return planId;
                }

                public String getServiceType() {
                    return serviceType;
                }

                public int getServiceProviderId() {
                    return serviceProviderId;
                }

                public String getServiceProvider() {
                    return serviceProvider;
                }

                public int getCircleId() {
                    return circleId;
                }

                public String getCircle() {
                    return circle;
                }

                public int getAmount() {
                    return amount;
                }

                public double getRechargeValue() {
                    return rechargeValue;
                }

                public String getValidity() {
                    return validity;
                }

                public String getDescription() {
                    return description;
                }

                public int getCategoryId() {
                    return categoryId;
                }

                public String getCategory() {
                    return category;
                }

                public boolean isIsExpired() {
                    return isExpired;
                }

                public int getCanRecommend() {
                    return canRecommend;
                }

                public int getValidNumDays() {
                    return validNumDays;
                }
            }
        }
    }
}
