package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 25/04/16.
 */
public class SaveMerchantHashModel {

    private HeaderEntity header;
    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public class HeaderEntity {
        /**
         * errors : {"errorList":[{"errorCode":"102","message":"No Plans Found."}]}
         * status : 1
         */
        private ErrorsEntity errors;
        private String status;

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public String getStatus() {
            return status;
        }

        public class ErrorsEntity {
            /**
             * errorList : [{"errorCode":"102","message":"No Plans Found."}]
             */
            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public class ErrorListEntity {
                /**
                 * errorCode : 102
                 * message : No Plans Found.
                 */
                private String errorCode;
                private String message;

                public void setErrorCode(String errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public String getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }

    public class BodyEntity {
        private String ssoId;

        public String getSsoId() {
            return ssoId;
        }

        public void setSsoId(String ssoId) {
            this.ssoId = ssoId;
        }
    }
}
