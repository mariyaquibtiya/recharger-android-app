package com.getsmartapp.lib.model;

/**
 * @author shalakha.gupta on 05/11/15.
 */
public class DrawerModel {
    private String title;
    private boolean isSelected = false;

    public DrawerModel() {
    }

    public DrawerModel(String title, boolean isSelected) {
        this.title = title;
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}

