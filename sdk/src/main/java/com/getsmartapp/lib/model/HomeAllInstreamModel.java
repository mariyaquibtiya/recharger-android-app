package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 08/09/15.
 */
public class HomeAllInstreamModel {
    /**
     * body : {"inStreamRecommendations":[{"plans":[{"planDetail":{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},"cost":8102.790664074947}],"categoryId":9}],"comboRecommendation":{"comboSets":[{"hasSms":0,"has4g":0,"plans":[{"categoryId":4,"planId":17510,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":250,"rechargeValue":250,"category":"Topup","isExpired":false,"canRecommend":1,"validity":"NA","description":"Talktime of Rs. 250","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":2,"planId":17591,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":397,"rechargeValue":0,"category":"3G","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"1.5 GB @3G speed. Post 1.5 GB you will be charged 4p/10kb","circleId":5,"serviceProvider":"Vodafone"}],"subText":"Plan tailored for your usage","has3g":1,"minValidity":0,"has2g":0,"comboName":"Combo Pack 1","comboId":53949,"comboSubsets":[{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.5,"local":367,"sms":3},"planIds":[17510,17564,17591],"comboCost":{"totalCost":1004.8705882352941,"usageCost":248.8705882352941,"comboCost":756,"saving":41.3755011731425}},{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234475E-4,"local":367,"sms":3},"planIds":[17510,17564],"comboCost":{"totalCost":8352.790664074946,"usageCost":7993.790664074947,"comboCost":359,"saving":51.47756894365351}},{"equivalentUsage":{"nightCall":77,"inNetworkTotalUsage":75,"call":226,"dominatUsageComponentVal":119,"dominatUsageComponentType":"local","std":107,"data":1.5,"local":119,"sms":3},"planIds":[17510,17591],"comboCost":{"totalCost":1386.8258658901732,"usageCost":739.8258658901733,"comboCost":647,"saving":18.622519136024845}},{"equivalentUsage":{"nightCall":4,"inNetworkTotalUsage":5,"call":15,"dominatUsageComponentVal":8,"dominatUsageComponentType":"local","std":7,"data":0.03042786748651226,"local":8,"sms":3},"planIds":[17510],"comboCost":{"totalCost":8734.745941729825,"usageCost":8484.745941729825,"comboCost":250,"saving":-594.7672962883028}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.5,"local":1,"sms":0},"planIds":[17564,17591],"comboCost":{"totalCost":754.8705882352941,"usageCost":248.8705882352941,"comboCost":506,"saving":9.178313262895044}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.0405567426234462E-10,"local":1,"sms":0},"planIds":[17564],"comboCost":{"totalCost":8102.790664074947,"usageCost":7993.790664074947,"comboCost":109,"saving":-1373.3196616070027}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":0,"call":0,"dominatUsageComponentVal":0,"dominatUsageComponentType":"inNetworkTotalUsage","std":0,"data":1.5,"local":0,"sms":3},"planIds":[17591],"comboCost":{"totalCost":1136.8258658901732,"usageCost":739.8258658901733,"comboCost":397,"saving":28.309334117042408}}],"hasData":1,"spId":2,"circleId":5,"maxValidity":28}],"usage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234483,"local":367,"sms":3}}}
     * header : {"status":"1"}
     */
    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        /**
         * inStreamRecommendations : [{"plans":[{"planDetail":{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},"cost":8102.790664074947}],"categoryId":9}]
         * comboRecommendation : {"comboSets":[{"hasSms":0,"has4g":0,"plans":[{"categoryId":4,"planId":17510,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":250,"rechargeValue":250,"category":"Topup","isExpired":false,"canRecommend":1,"validity":"NA","description":"Talktime of Rs. 250","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":2,"planId":17591,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":397,"rechargeValue":0,"category":"3G","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"1.5 GB @3G speed. Post 1.5 GB you will be charged 4p/10kb","circleId":5,"serviceProvider":"Vodafone"}],"subText":"Plan tailored for your usage","has3g":1,"minValidity":0,"has2g":0,"comboName":"Combo Pack 1","comboId":53949,"comboSubsets":[{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.5,"local":367,"sms":3},"planIds":[17510,17564,17591],"comboCost":{"totalCost":1004.8705882352941,"usageCost":248.8705882352941,"comboCost":756,"saving":41.3755011731425}},{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234475E-4,"local":367,"sms":3},"planIds":[17510,17564],"comboCost":{"totalCost":8352.790664074946,"usageCost":7993.790664074947,"comboCost":359,"saving":51.47756894365351}},{"equivalentUsage":{"nightCall":77,"inNetworkTotalUsage":75,"call":226,"dominatUsageComponentVal":119,"dominatUsageComponentType":"local","std":107,"data":1.5,"local":119,"sms":3},"planIds":[17510,17591],"comboCost":{"totalCost":1386.8258658901732,"usageCost":739.8258658901733,"comboCost":647,"saving":18.622519136024845}},{"equivalentUsage":{"nightCall":4,"inNetworkTotalUsage":5,"call":15,"dominatUsageComponentVal":8,"dominatUsageComponentType":"local","std":7,"data":0.03042786748651226,"local":8,"sms":3},"planIds":[17510],"comboCost":{"totalCost":8734.745941729825,"usageCost":8484.745941729825,"comboCost":250,"saving":-594.7672962883028}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.5,"local":1,"sms":0},"planIds":[17564,17591],"comboCost":{"totalCost":754.8705882352941,"usageCost":248.8705882352941,"comboCost":506,"saving":9.178313262895044}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.0405567426234462E-10,"local":1,"sms":0},"planIds":[17564],"comboCost":{"totalCost":8102.790664074947,"usageCost":7993.790664074947,"comboCost":109,"saving":-1373.3196616070027}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":0,"call":0,"dominatUsageComponentVal":0,"dominatUsageComponentType":"inNetworkTotalUsage","std":0,"data":1.5,"local":0,"sms":3},"planIds":[17591],"comboCost":{"totalCost":1136.8258658901732,"usageCost":739.8258658901733,"comboCost":397,"saving":28.309334117042408}}],"hasData":1,"spId":2,"circleId":5,"maxValidity":28}],"usage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234483,"local":367,"sms":3}}
         */
        private List<InStreamRecommendationsEntity> inStreamRecommendations;
        private ComboRecommendationEntity comboRecommendation;

        public void setInStreamRecommendations(List<InStreamRecommendationsEntity> inStreamRecommendations) {
            this.inStreamRecommendations = inStreamRecommendations;
        }

        public void setComboRecommendation(ComboRecommendationEntity comboRecommendation) {
            this.comboRecommendation = comboRecommendation;
        }

        public List<InStreamRecommendationsEntity> getInStreamRecommendations() {
            return inStreamRecommendations;
        }

        public ComboRecommendationEntity getComboRecommendation() {
            return comboRecommendation;
        }

        public class InStreamRecommendationsEntity {
            /**
             * plans : [{"planDetail":{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},"cost":8102.790664074947}]
             * categoryId : 9
             */
            private List<PlansEntity> plans;
            private int categoryId;

            public void setPlans(List<PlansEntity> plans) {
                this.plans = plans;
            }

            public void setCategoryId(int categoryId) {
                this.categoryId = categoryId;
            }

            public List<PlansEntity> getPlans() {
                return plans;
            }

            public int getCategoryId() {
                return categoryId;
            }

            public class PlansEntity {
                /**
                 * planDetail : {"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"}
                 * cost : 8102.790664074947
                 */
                private PlanDetailEntity planDetail;
                private double cost;
                private double savingsAbs;
                private double savingsPercentage;

                public void setPlanDetail(PlanDetailEntity planDetail) {
                    this.planDetail = planDetail;
                }

                public void setCost(double cost) {
                    this.cost = cost;
                }

                public double getSavingsAbs() {
                    return savingsAbs;
                }

                public void setSavingsAbs(double savingsAbs) {
                    this.savingsAbs = savingsAbs;
                }

                public double getSavingsPercentage() {
                    return savingsPercentage;
                }

                public void setSavingsPercentage(double savingsPercentage) {
                    this.savingsPercentage = savingsPercentage;
                }

                public PlanDetailEntity getPlanDetail() {
                    return planDetail;
                }

                public double getCost() {
                    return cost;
                }

                public class PlanDetailEntity {
                    /**
                     * categoryId : 9
                     * planId : 17564
                     * circle : Delhi NCR
                     * serviceType : PREPAID_MOBILE
                     * serviceProviderId : 2
                     * amount : 109
                     * rechargeValue : 0.0
                     * category : Special
                     * isExpired : false
                     * canRecommend : 1
                     * validity : 28 Days
                     * description : All Local & STD calls @ 35p/min
                     * circleId : 5
                     * serviceProvider : Vodafone
                     */
                    private int categoryId;
                    private int planId;
                    private String circle;
                    private String serviceType;
                    private int serviceProviderId;
                    private int amount;
                    private double rechargeValue;
                    private String category;
                    private boolean isExpired;
                    private int canRecommend;
                    private String validity;
                    private String description;
                    private int circleId;
                    private String serviceProvider;

                    public void setCategoryId(int categoryId) {
                        this.categoryId = categoryId;
                    }

                    public void setPlanId(int planId) {
                        this.planId = planId;
                    }

                    public void setCircle(String circle) {
                        this.circle = circle;
                    }

                    public void setServiceType(String serviceType) {
                        this.serviceType = serviceType;
                    }

                    public void setServiceProviderId(int serviceProviderId) {
                        this.serviceProviderId = serviceProviderId;
                    }

                    public void setAmount(int amount) {
                        this.amount = amount;
                    }

                    public void setRechargeValue(double rechargeValue) {
                        this.rechargeValue = rechargeValue;
                    }

                    public void setCategory(String category) {
                        this.category = category;
                    }

                    public void setIsExpired(boolean isExpired) {
                        this.isExpired = isExpired;
                    }

                    public void setCanRecommend(int canRecommend) {
                        this.canRecommend = canRecommend;
                    }

                    public void setValidity(String validity) {
                        this.validity = validity;
                    }

                    public void setDescription(String description) {
                        this.description = description;
                    }

                    public void setCircleId(int circleId) {
                        this.circleId = circleId;
                    }

                    public void setServiceProvider(String serviceProvider) {
                        this.serviceProvider = serviceProvider;
                    }

                    public int getCategoryId() {
                        return categoryId;
                    }

                    public int getPlanId() {
                        return planId;
                    }

                    public String getCircle() {
                        return circle;
                    }

                    public String getServiceType() {
                        return serviceType;
                    }

                    public int getServiceProviderId() {
                        return serviceProviderId;
                    }

                    public int getAmount() {
                        return amount;
                    }

                    public double getRechargeValue() {
                        return rechargeValue;
                    }

                    public String getCategory() {
                        return category;
                    }

                    public boolean isIsExpired() {
                        return isExpired;
                    }

                    public int getCanRecommend() {
                        return canRecommend;
                    }

                    public String getValidity() {
                        return validity;
                    }

                    public String getDescription() {
                        return description;
                    }

                    public int getCircleId() {
                        return circleId;
                    }

                    public String getServiceProvider() {
                        return serviceProvider;
                    }
                }
            }
        }

        public class ComboRecommendationEntity {
            /**
             * comboSets : [{"hasSms":0,"has4g":0,"plans":[{"categoryId":4,"planId":17510,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":250,"rechargeValue":250,"category":"Topup","isExpired":false,"canRecommend":1,"validity":"NA","description":"Talktime of Rs. 250","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":2,"planId":17591,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":397,"rechargeValue":0,"category":"3G","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"1.5 GB @3G speed. Post 1.5 GB you will be charged 4p/10kb","circleId":5,"serviceProvider":"Vodafone"}],"subText":"Plan tailored for your usage","has3g":1,"minValidity":0,"has2g":0,"comboName":"Combo Pack 1","comboId":53949,"comboSubsets":[{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.5,"local":367,"sms":3},"planIds":[17510,17564,17591],"comboCost":{"totalCost":1004.8705882352941,"usageCost":248.8705882352941,"comboCost":756,"saving":41.3755011731425}},{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234475E-4,"local":367,"sms":3},"planIds":[17510,17564],"comboCost":{"totalCost":8352.790664074946,"usageCost":7993.790664074947,"comboCost":359,"saving":51.47756894365351}},{"equivalentUsage":{"nightCall":77,"inNetworkTotalUsage":75,"call":226,"dominatUsageComponentVal":119,"dominatUsageComponentType":"local","std":107,"data":1.5,"local":119,"sms":3},"planIds":[17510,17591],"comboCost":{"totalCost":1386.8258658901732,"usageCost":739.8258658901733,"comboCost":647,"saving":18.622519136024845}},{"equivalentUsage":{"nightCall":4,"inNetworkTotalUsage":5,"call":15,"dominatUsageComponentVal":8,"dominatUsageComponentType":"local","std":7,"data":0.03042786748651226,"local":8,"sms":3},"planIds":[17510],"comboCost":{"totalCost":8734.745941729825,"usageCost":8484.745941729825,"comboCost":250,"saving":-594.7672962883028}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.5,"local":1,"sms":0},"planIds":[17564,17591],"comboCost":{"totalCost":754.8705882352941,"usageCost":248.8705882352941,"comboCost":506,"saving":9.178313262895044}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.0405567426234462E-10,"local":1,"sms":0},"planIds":[17564],"comboCost":{"totalCost":8102.790664074947,"usageCost":7993.790664074947,"comboCost":109,"saving":-1373.3196616070027}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":0,"call":0,"dominatUsageComponentVal":0,"dominatUsageComponentType":"inNetworkTotalUsage","std":0,"data":1.5,"local":0,"sms":3},"planIds":[17591],"comboCost":{"totalCost":1136.8258658901732,"usageCost":739.8258658901733,"comboCost":397,"saving":28.309334117042408}}],"hasData":1,"spId":2,"circleId":5,"maxValidity":28}]
             * usage : {"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234483,"local":367,"sms":3}
             */
            private List<ComboSetsEntity> comboSets;
            private UsageEntity usage;

            public void setComboSets(List<ComboSetsEntity> comboSets) {
                this.comboSets = comboSets;
            }

            public void setUsage(UsageEntity usage) {
                this.usage = usage;
            }

            public List<ComboSetsEntity> getComboSets() {
                return comboSets;
            }

            public UsageEntity getUsage() {
                return usage;
            }

            public class ComboSetsEntity {
                /**
                 * hasSms : 0
                 * has4g : 0
                 * plans : [{"categoryId":4,"planId":17510,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":250,"rechargeValue":250,"category":"Topup","isExpired":false,"canRecommend":1,"validity":"NA","description":"Talktime of Rs. 250","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":9,"planId":17564,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":109,"rechargeValue":0,"category":"Special","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"All Local & STD calls @ 35p/min","circleId":5,"serviceProvider":"Vodafone"},{"categoryId":2,"planId":17591,"circle":"Delhi NCR","serviceType":"PREPAID_MOBILE","serviceProviderId":2,"amount":397,"rechargeValue":0,"category":"3G","isExpired":false,"canRecommend":1,"validity":"28 Days","description":"1.5 GB @3G speed. Post 1.5 GB you will be charged 4p/10kb","circleId":5,"serviceProvider":"Vodafone"}]
                 * subText : Plan tailored for your usage
                 * has3g : 1
                 * minValidity : 0
                 * has2g : 0
                 * comboName : Combo Pack 1
                 * comboId : 53949
                 * comboSubsets : [{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.5,"local":367,"sms":3},"planIds":[17510,17564,17591],"comboCost":{"totalCost":1004.8705882352941,"usageCost":248.8705882352941,"comboCost":756,"saving":41.3755011731425}},{"equivalentUsage":{"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.0405567426234475E-4,"local":367,"sms":3},"planIds":[17510,17564],"comboCost":{"totalCost":8352.790664074946,"usageCost":7993.790664074947,"comboCost":359,"saving":51.47756894365351}},{"equivalentUsage":{"nightCall":77,"inNetworkTotalUsage":75,"call":226,"dominatUsageComponentVal":119,"dominatUsageComponentType":"local","std":107,"data":1.5,"local":119,"sms":3},"planIds":[17510,17591],"comboCost":{"totalCost":1386.8258658901732,"usageCost":739.8258658901733,"comboCost":647,"saving":18.622519136024845}},{"equivalentUsage":{"nightCall":4,"inNetworkTotalUsage":5,"call":15,"dominatUsageComponentVal":8,"dominatUsageComponentType":"local","std":7,"data":0.03042786748651226,"local":8,"sms":3},"planIds":[17510],"comboCost":{"totalCost":8734.745941729825,"usageCost":8484.745941729825,"comboCost":250,"saving":-594.7672962883028}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.5,"local":1,"sms":0},"planIds":[17564,17591],"comboCost":{"totalCost":754.8705882352941,"usageCost":248.8705882352941,"comboCost":506,"saving":9.178313262895044}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":1,"call":2,"dominatUsageComponentVal":1,"dominatUsageComponentType":"inNetworkTotalUsage","std":1,"data":1.0405567426234462E-10,"local":1,"sms":0},"planIds":[17564],"comboCost":{"totalCost":8102.790664074947,"usageCost":7993.790664074947,"comboCost":109,"saving":-1373.3196616070027}},{"equivalentUsage":{"nightCall":0,"inNetworkTotalUsage":0,"call":0,"dominatUsageComponentVal":0,"dominatUsageComponentType":"inNetworkTotalUsage","std":0,"data":1.5,"local":0,"sms":3},"planIds":[17591],"comboCost":{"totalCost":1136.8258658901732,"usageCost":739.8258658901733,"comboCost":397,"saving":28.309334117042408}}]
                 * hasData : 1
                 * spId : 2
                 * circleId : 5
                 * maxValidity : 28
                 */
                private int hasSms;
                private int has4g;
                private List<PlansEntity> plans;
                private String subText;
                private int has3g;
                private int minValidity;
                private int has2g;
                private String comboName;
                private int comboId;
                private List<ComboSubsetsEntity> comboSubsets;
                private int hasData;
                private int spId;
                private int circleId;
                private int maxValidity;

                public void setHasSms(int hasSms) {
                    this.hasSms = hasSms;
                }

                public void setHas4g(int has4g) {
                    this.has4g = has4g;
                }

                public void setPlans(List<PlansEntity> plans) {
                    this.plans = plans;
                }

                public void setSubText(String subText) {
                    this.subText = subText;
                }

                public void setHas3g(int has3g) {
                    this.has3g = has3g;
                }

                public void setMinValidity(int minValidity) {
                    this.minValidity = minValidity;
                }

                public void setHas2g(int has2g) {
                    this.has2g = has2g;
                }

                public void setComboName(String comboName) {
                    this.comboName = comboName;
                }

                public void setComboId(int comboId) {
                    this.comboId = comboId;
                }

                public void setComboSubsets(List<ComboSubsetsEntity> comboSubsets) {
                    this.comboSubsets = comboSubsets;
                }

                public void setHasData(int hasData) {
                    this.hasData = hasData;
                }

                public void setSpId(int spId) {
                    this.spId = spId;
                }

                public void setCircleId(int circleId) {
                    this.circleId = circleId;
                }

                public void setMaxValidity(int maxValidity) {
                    this.maxValidity = maxValidity;
                }

                public int getHasSms() {
                    return hasSms;
                }

                public int getHas4g() {
                    return has4g;
                }

                public List<PlansEntity> getPlans() {
                    return plans;
                }

                public String getSubText() {
                    return subText;
                }

                public int getHas3g() {
                    return has3g;
                }

                public int getMinValidity() {
                    return minValidity;
                }

                public int getHas2g() {
                    return has2g;
                }

                public String getComboName() {
                    return comboName;
                }

                public int getComboId() {
                    return comboId;
                }

                public List<ComboSubsetsEntity> getComboSubsets() {
                    return comboSubsets;
                }

                public int getHasData() {
                    return hasData;
                }

                public int getSpId() {
                    return spId;
                }

                public int getCircleId() {
                    return circleId;
                }

                public int getMaxValidity() {
                    return maxValidity;
                }

                public class PlansEntity {
                    /**
                     * categoryId : 4
                     * planId : 17510
                     * circle : Delhi NCR
                     * serviceType : PREPAID_MOBILE
                     * serviceProviderId : 2
                     * amount : 250
                     * rechargeValue : 250.0
                     * category : Topup
                     * isExpired : false
                     * canRecommend : 1
                     * validity : NA
                     * description : Talktime of Rs. 250
                     * circleId : 5
                     * serviceProvider : Vodafone
                     */
                    private int categoryId;
                    private int planId;
                    private String circle;
                    private String serviceType;
                    private int serviceProviderId;
                    private int amount;
                    private double rechargeValue;
                    private String category;
                    private boolean isExpired;
                    private int canRecommend;
                    private String validity;
                    private String description;
                    private int circleId;
                    private String serviceProvider;

                    public void setCategoryId(int categoryId) {
                        this.categoryId = categoryId;
                    }

                    public void setPlanId(int planId) {
                        this.planId = planId;
                    }

                    public void setCircle(String circle) {
                        this.circle = circle;
                    }

                    public void setServiceType(String serviceType) {
                        this.serviceType = serviceType;
                    }

                    public void setServiceProviderId(int serviceProviderId) {
                        this.serviceProviderId = serviceProviderId;
                    }

                    public void setAmount(int amount) {
                        this.amount = amount;
                    }

                    public void setRechargeValue(double rechargeValue) {
                        this.rechargeValue = rechargeValue;
                    }

                    public void setCategory(String category) {
                        this.category = category;
                    }

                    public void setIsExpired(boolean isExpired) {
                        this.isExpired = isExpired;
                    }

                    public void setCanRecommend(int canRecommend) {
                        this.canRecommend = canRecommend;
                    }

                    public void setValidity(String validity) {
                        this.validity = validity;
                    }

                    public void setDescription(String description) {
                        this.description = description;
                    }

                    public void setCircleId(int circleId) {
                        this.circleId = circleId;
                    }

                    public void setServiceProvider(String serviceProvider) {
                        this.serviceProvider = serviceProvider;
                    }

                    public int getCategoryId() {
                        return categoryId;
                    }

                    public int getPlanId() {
                        return planId;
                    }

                    public String getCircle() {
                        return circle;
                    }

                    public String getServiceType() {
                        return serviceType;
                    }

                    public int getServiceProviderId() {
                        return serviceProviderId;
                    }

                    public int getAmount() {
                        return amount;
                    }

                    public double getRechargeValue() {
                        return rechargeValue;
                    }

                    public String getCategory() {
                        return category;
                    }

                    public boolean isIsExpired() {
                        return isExpired;
                    }

                    public int getCanRecommend() {
                        return canRecommend;
                    }

                    public String getValidity() {
                        return validity;
                    }

                    public String getDescription() {
                        return description;
                    }

                    public int getCircleId() {
                        return circleId;
                    }

                    public String getServiceProvider() {
                        return serviceProvider;
                    }
                }

                public class ComboSubsetsEntity {
                    /**
                     * equivalentUsage : {"nightCall":237,"inNetworkTotalUsage":230,"call":696,"dominatUsageComponentVal":367,"dominatUsageComponentType":"local","std":329,"data":1.5,"local":367,"sms":3}
                     * planIds : [17510,17564,17591]
                     * comboCost : {"totalCost":1004.8705882352941,"usageCost":248.8705882352941,"comboCost":756,"saving":41.3755011731425}
                     */
                    private EquivalentUsageEntity equivalentUsage;
                    private List<Integer> planIds;
                    private ComboCostEntity comboCost;

                    public void setEquivalentUsage(EquivalentUsageEntity equivalentUsage) {
                        this.equivalentUsage = equivalentUsage;
                    }

                    public void setPlanIds(List<Integer> planIds) {
                        this.planIds = planIds;
                    }

                    public void setComboCost(ComboCostEntity comboCost) {
                        this.comboCost = comboCost;
                    }

                    public EquivalentUsageEntity getEquivalentUsage() {
                        return equivalentUsage;
                    }

                    public List<Integer> getPlanIds() {
                        return planIds;
                    }

                    public ComboCostEntity getComboCost() {
                        return comboCost;
                    }

                    public class EquivalentUsageEntity {
                        /**
                         * nightCall : 237
                         * inNetworkTotalUsage : 230
                         * call : 696
                         * dominatUsageComponentVal : 367
                         * dominatUsageComponentType : local
                         * std : 329
                         * data : 1.5
                         * local : 367
                         * sms : 3
                         */
                        private int nightCall;
                        private int inNetworkTotalUsage;
                        private int call;
                        private int dominatUsageComponentVal;
                        private String dominatUsageComponentType;
                        private int std;
                        private double data;
                        private int local;
                        private int sms;

                        public void setNightCall(int nightCall) {
                            this.nightCall = nightCall;
                        }

                        public void setInNetworkTotalUsage(int inNetworkTotalUsage) {
                            this.inNetworkTotalUsage = inNetworkTotalUsage;
                        }

                        public void setCall(int call) {
                            this.call = call;
                        }

                        public void setDominatUsageComponentVal(int dominatUsageComponentVal) {
                            this.dominatUsageComponentVal = dominatUsageComponentVal;
                        }

                        public void setDominatUsageComponentType(String dominatUsageComponentType) {
                            this.dominatUsageComponentType = dominatUsageComponentType;
                        }

                        public void setStd(int std) {
                            this.std = std;
                        }

                        public void setData(double data) {
                            this.data = data;
                        }

                        public void setLocal(int local) {
                            this.local = local;
                        }

                        public void setSms(int sms) {
                            this.sms = sms;
                        }

                        public int getNightCall() {
                            return nightCall;
                        }

                        public int getInNetworkTotalUsage() {
                            return inNetworkTotalUsage;
                        }

                        public int getCall() {
                            return call;
                        }

                        public int getDominatUsageComponentVal() {
                            return dominatUsageComponentVal;
                        }

                        public String getDominatUsageComponentType() {
                            return dominatUsageComponentType;
                        }

                        public int getStd() {
                            return std;
                        }

                        public double getData() {
                            return data;
                        }

                        public int getLocal() {
                            return local;
                        }

                        public int getSms() {
                            return sms;
                        }
                    }

                    public class ComboCostEntity {
                        /**
                         * totalCost : 1004.8705882352941
                         * usageCost : 248.8705882352941
                         * comboCost : 756
                         * saving : 41.3755011731425
                         */
                        private double totalCost;
                        private double usageCost;
                        private int comboCost;
                        private double saving;

                        public void setTotalCost(double totalCost) {
                            this.totalCost = totalCost;
                        }

                        public void setUsageCost(double usageCost) {
                            this.usageCost = usageCost;
                        }

                        public void setComboCost(int comboCost) {
                            this.comboCost = comboCost;
                        }

                        public void setSaving(double saving) {
                            this.saving = saving;
                        }

                        public double getTotalCost() {
                            return totalCost;
                        }

                        public double getUsageCost() {
                            return usageCost;
                        }

                        public int getComboCost() {
                            return comboCost;
                        }

                        public double getSaving() {
                            return saving;
                        }
                    }
                }
            }

            public class UsageEntity {
                /**
                 * nightCall : 237
                 * inNetworkTotalUsage : 230
                 * call : 696
                 * dominatUsageComponentVal : 367
                 * dominatUsageComponentType : local
                 * std : 329
                 * data : 1.0405567426234483
                 * local : 367
                 * sms : 3
                 */
                private int nightCall;
                private int inNetworkTotalUsage;
                private int call;
                private int dominatUsageComponentVal;
                private String dominatUsageComponentType;
                private int std;
                private double data;
                private int local;
                private int sms;

                public void setNightCall(int nightCall) {
                    this.nightCall = nightCall;
                }

                public void setInNetworkTotalUsage(int inNetworkTotalUsage) {
                    this.inNetworkTotalUsage = inNetworkTotalUsage;
                }

                public void setCall(int call) {
                    this.call = call;
                }

                public void setDominatUsageComponentVal(int dominatUsageComponentVal) {
                    this.dominatUsageComponentVal = dominatUsageComponentVal;
                }

                public void setDominatUsageComponentType(String dominatUsageComponentType) {
                    this.dominatUsageComponentType = dominatUsageComponentType;
                }

                public void setStd(int std) {
                    this.std = std;
                }

                public void setData(double data) {
                    this.data = data;
                }

                public void setLocal(int local) {
                    this.local = local;
                }

                public void setSms(int sms) {
                    this.sms = sms;
                }

                public int getNightCall() {
                    return nightCall;
                }

                public int getInNetworkTotalUsage() {
                    return inNetworkTotalUsage;
                }

                public int getCall() {
                    return call;
                }

                public int getDominatUsageComponentVal() {
                    return dominatUsageComponentVal;
                }

                public String getDominatUsageComponentType() {
                    return dominatUsageComponentType;
                }

                public int getStd() {
                    return std;
                }

                public double getData() {
                    return data;
                }

                public int getLocal() {
                    return local;
                }

                public int getSms() {
                    return sms;
                }
            }
        }
    }

    public class HeaderEntity {
        /**
         * status : 1
         */
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
