package com.getsmartapp.lib.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author nitesh.verma on 9/18/15.
 */
public class RecentRechargeServer implements Parcelable{

    /**
     * header : {"status":"1"}
     * body : [{"pgPaymentStatus":"PMNT_SUCCESSFUL","orderId":11503206308,"netAmountPaid":2000,"orderStatus":"ORDER_SUCCESSFUL","message":"Your recharge is successful","orderItemStatusList":[{"amount":2000,"spName":"Airtel","orderId":11503206308,"rechargeStatus":"RECHARGE_SUCCESSFUL","planValidity":"","sTypeId":2,"message":"Your recharge is successful","spId":1,"categoryName":"Topup","rechargeTo":"9971730287","orderItemStatus":"ORDER_SUCCESSFUL","rechargePartnerTxId":"1441291201075115032063080","promoCodeAmount":0,"planId":0,"planDescription":"","circleId":5,"rechargePartner":"MOBIKWIK","orderDate":"03-Sep-15","circleName":"Delhi NCR","seqId":0,"categoryId":4}],"totalAmount":2000,"promoCode":"","payUTxId":"409487636","rechargeType":"single","orderDate":"Sep 3, 2015 10:37:53 AM","paymentStatus":"PMNT_SUCCESSFUL","promoCodeDiscount":0},{"pgPaymentStatus":"PMNT_SUCCESSFUL","orderId":11503206217,"netAmountPaid":10,"orderStatus":"ORDER_SUCCESSFUL","message":"Your recharge is successful","orderItemStatusList":[{"amount":10,"spName":"Airtel","orderId":11503206217,"rechargeStatus":"RECHARGE_SUCCESSFUL","planValidity":"Lifetime","sTypeId":1,"message":"Your recharge is successful","spId":1,"categoryName":"Topup","rechargeTo":"8826044217","orderItemStatus":"ORDER_SUCCESSFUL","rechargePartnerTxId":"1441028590124115032062170","promoCodeAmount":0,"planId":1699,"planDescription":"Talktime of Rs. 7.77","circleId":5,"rechargePartner":"OXIGEN","orderDate":"31-Aug-15","circleName":"Delhi NCR","seqId":0,"categoryId":4}],"totalAmount":10,"promoCode":"","payUTxId":"407761930","rechargeType":"single","orderDate":"Aug 31, 2015 9:42:04 AM","paymentStatus":"PMNT_SUCCESSFUL","promoCodeDiscount":0},{"pgPaymentStatus":"PMNT_SUCCESSFUL","orderId":11503206198,"netAmountPaid":5,"orderStatus":"ORDER_SUCCESSFUL","message":"Your recharge is successful","orderItemStatusList":[{"amount":10,"spName":"Airtel","orderId":11503206198,"rechargeStatus":"RECHARGE_SUCCESSFUL","planValidity":"Lifetime","sTypeId":1,"message":"Your recharge is successful","spId":1,"categoryName":"Topup","rechargeTo":"8826044217","orderItemStatus":"ORDER_SUCCESSFUL","rechargePartnerTxId":"1441013219257115032061980","promoCodeAmount":5,"planId":1699,"planDescription":"Talktime of Rs. 7.77","circleId":5,"rechargePartner":"MOBIKWIK","orderDate":"31-Aug-15","circleName":"Delhi NCR","seqId":0,"categoryId":4}],"gcPaymentStatus":"PMNT_SUCCESSFUL","totalAmount":10,"promoCode":"SR4DUP23K9HIC1T","payUTxId":"407604227","rechargeType":"single","orderDate":"Aug 31, 2015 5:24:56 AM","paymentStatus":"PMNT_SUCCESSFUL","promoCodeDiscount":5}]
     */
    private HeaderEntity header;
    private List<BodyEntity> body;

    protected RecentRechargeServer(Parcel in) {
    }

    public static final Creator<RecentRechargeServer> CREATOR = new Creator<RecentRechargeServer>() {
        @Override
        public RecentRechargeServer createFromParcel(Parcel in) {
            return new RecentRechargeServer(in);
        }

        @Override
        public RecentRechargeServer[] newArray(int size) {
            return new RecentRechargeServer[size];
        }
    };

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(List<BodyEntity> body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public List<BodyEntity> getBody() {
        return body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public class HeaderEntity {
        /**
         * status : 1
         */
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    public class BodyEntity {
        /**
         * pgPaymentStatus : PMNT_SUCCESSFUL
         * orderId : 11503206308
         * netAmountPaid : 2000
         * orderStatus : ORDER_SUCCESSFUL
         * message : Your recharge is successful
         * orderItemStatusList : [{"amount":2000,"spName":"Airtel","orderId":11503206308,"rechargeStatus":"RECHARGE_SUCCESSFUL","planValidity":"","sTypeId":2,"message":"Your recharge is successful","spId":1,"categoryName":"Topup","rechargeTo":"9971730287","orderItemStatus":"ORDER_SUCCESSFUL","rechargePartnerTxId":"1441291201075115032063080","promoCodeAmount":0,"planId":0,"planDescription":"","circleId":5,"rechargePartner":"MOBIKWIK","orderDate":"03-Sep-15","circleName":"Delhi NCR","seqId":0,"categoryId":4}]
         * totalAmount : 2000
         * promoCode :
         * payUTxId : 409487636
         * rechargeType : single
         * orderDate : Sep 3, 2015 10:37:53 AM
         * paymentStatus : PMNT_SUCCESSFUL
         * promoCodeDiscount : 0
         */
        private String pgPaymentStatus;
        private long orderId;
        private int netAmountPaid;
        private String orderStatus;
        private String message;
        private List<OrderItemStatusListEntity> orderItemStatusList;
        private int totalAmount;
        private String promoCode;
        private String payUTxId;
        private String rechargeType;
        private String orderDate;
        private String paymentStatus;
        private int promoCodeDiscount;

        public void setPgPaymentStatus(String pgPaymentStatus) {
            this.pgPaymentStatus = pgPaymentStatus;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public void setNetAmountPaid(int netAmountPaid) {
            this.netAmountPaid = netAmountPaid;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setOrderItemStatusList(List<OrderItemStatusListEntity> orderItemStatusList) {
            this.orderItemStatusList = orderItemStatusList;
        }

        public void setTotalAmount(int totalAmount) {
            this.totalAmount = totalAmount;
        }

        public void setPromoCode(String promoCode) {
            this.promoCode = promoCode;
        }

        public void setPayUTxId(String payUTxId) {
            this.payUTxId = payUTxId;
        }

        public void setRechargeType(String rechargeType) {
            this.rechargeType = rechargeType;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public void setPromoCodeDiscount(int promoCodeDiscount) {
            this.promoCodeDiscount = promoCodeDiscount;
        }

        public String getPgPaymentStatus() {
            return pgPaymentStatus;
        }

        public long getOrderId() {
            return orderId;
        }

        public int getNetAmountPaid() {
            return netAmountPaid;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public String getMessage() {
            return message;
        }

        public List<OrderItemStatusListEntity> getOrderItemStatusList() {
            return orderItemStatusList;
        }

        public int getTotalAmount() {
            return totalAmount;
        }

        public String getPromoCode() {
            return promoCode;
        }

        public String getPayUTxId() {
            return payUTxId;
        }

        public String getRechargeType() {
            return rechargeType;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public int getPromoCodeDiscount() {
            return promoCodeDiscount;
        }

        public class OrderItemStatusListEntity {
            /**
             * amount : 2000
             * spName : Airtel
             * orderId : 11503206308
             * rechargeStatus : RECHARGE_SUCCESSFUL
             * planValidity :
             * sTypeId : 2
             * message : Your recharge is successful
             * spId : 1
             * categoryName : Topup
             * rechargeTo : 9971730287
             * orderItemStatus : ORDER_SUCCESSFUL
             * rechargePartnerTxId : 1441291201075115032063080
             * promoCodeAmount : 0
             * planId : 0
             * planDescription :
             * circleId : 5
             * rechargePartner : MOBIKWIK
             * orderDate : 03-Sep-15
             * circleName : Delhi NCR
             * seqId : 0
             * categoryId : 4
             */
            private int amount;
            private String spName;
            private long orderId;
            private String rechargeStatus;
            private String planValidity;
            private int sTypeId;
            private String message;
            private int spId;
            private String categoryName;
            private String rechargeTo;
            private String orderItemStatus;
            private String rechargePartnerTxId;
            private int promoCodeAmount;
            private int planId;
            private String planDescription;
            private int circleId;
            private String rechargePartner;
            private String orderDate;
            private String circleName;
            private int seqId;
            private int categoryId;

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public void setSpName(String spName) {
                this.spName = spName;
            }

            public void setOrderId(long orderId) {
                this.orderId = orderId;
            }

            public void setRechargeStatus(String rechargeStatus) {
                this.rechargeStatus = rechargeStatus;
            }

            public void setPlanValidity(String planValidity) {
                this.planValidity = planValidity;
            }

            public void setSTypeId(int sTypeId) {
                this.sTypeId = sTypeId;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public void setSpId(int spId) {
                this.spId = spId;
            }

            public void setCategoryName(String categoryName) {
                this.categoryName = categoryName;
            }

            public void setRechargeTo(String rechargeTo) {
                this.rechargeTo = rechargeTo;
            }

            public void setOrderItemStatus(String orderItemStatus) {
                this.orderItemStatus = orderItemStatus;
            }

            public void setRechargePartnerTxId(String rechargePartnerTxId) {
                this.rechargePartnerTxId = rechargePartnerTxId;
            }

            public void setPromoCodeAmount(int promoCodeAmount) {
                this.promoCodeAmount = promoCodeAmount;
            }

            public void setPlanId(int planId) {
                this.planId = planId;
            }

            public void setPlanDescription(String planDescription) {
                this.planDescription = planDescription;
            }

            public void setCircleId(int circleId) {
                this.circleId = circleId;
            }

            public void setRechargePartner(String rechargePartner) {
                this.rechargePartner = rechargePartner;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }

            public void setCircleName(String circleName) {
                this.circleName = circleName;
            }

            public void setSeqId(int seqId) {
                this.seqId = seqId;
            }

            public void setCategoryId(int categoryId) {
                this.categoryId = categoryId;
            }

            public int getAmount() {
                return amount;
            }

            public String getSpName() {
                return spName;
            }

            public long getOrderId() {
                return orderId;
            }

            public String getRechargeStatus() {
                return rechargeStatus;
            }

            public String getPlanValidity() {
                return planValidity;
            }

            public int getSTypeId() {
                return sTypeId;
            }

            public String getMessage() {
                return message;
            }

            public int getSpId() {
                return spId;
            }

            public String getCategoryName() {
                return categoryName;
            }

            public String getRechargeTo() {
                return rechargeTo;
            }

            public String getOrderItemStatus() {
                return orderItemStatus;
            }

            public String getRechargePartnerTxId() {
                return rechargePartnerTxId;
            }

            public int getPromoCodeAmount() {
                return promoCodeAmount;
            }

            public int getPlanId() {
                return planId;
            }

            public String getPlanDescription() {
                return planDescription;
            }

            public int getCircleId() {
                return circleId;
            }

            public String getRechargePartner() {
                return rechargePartner;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public String getCircleName() {
                return circleName;
            }

            public int getSeqId() {
                return seqId;
            }

            public int getCategoryId() {
                return categoryId;
            }
        }
    }
}
