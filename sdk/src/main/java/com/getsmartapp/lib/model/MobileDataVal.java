package com.getsmartapp.lib.model;

/**
 * @author nitesh.verma on 10/1/15.
 */
public class MobileDataVal {

    long mobile_data,wifi_data,total_data;

    public MobileDataVal(long mobile_data, long wifi_data, long total_data) {
        this.mobile_data = mobile_data;
        this.wifi_data = wifi_data;
        this.total_data = total_data;
    }

    public long getMobile_data() {
        return mobile_data;
    }

    public void setMobile_data(long mobile_data) {
        this.mobile_data = mobile_data;
    }

    public long getWifi_data() {
        return wifi_data;
    }

    public void setWifi_data(long wifi_data) {
        this.wifi_data = wifi_data;
    }

    public long getTotal_data() {
        return total_data;
    }

    public void setTotal_data(long total_data) {
        this.total_data = total_data;
    }
}
