package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author bishwajeet.kumar on 19/07/15.
 */
public class ReferralEvents {

    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        private List<ReferralListEntity> referralList;

        public void setReferralList(List<ReferralListEntity> referralList) {
            this.referralList = referralList;
        }

        public List<ReferralListEntity> getReferralList() {
            return referralList;
        }

        public class ReferralListEntity {
            private String message;
            private String inviteeLastName;
            private String inviteeFirstName;
            private String inviteeId;
            private String action;
            private String eventTime;
            private String referrerId;

            public void setMessage(String message) {
                this.message = message;
            }

            public void setInviteeLastName(String inviteeLastName) {
                this.inviteeLastName = inviteeLastName;
            }

            public void setInviteeFirstName(String inviteeFirstName) {
                this.inviteeFirstName = inviteeFirstName;
            }

            public void setInviteeId(String inviteeId) {
                this.inviteeId = inviteeId;
            }

            public void setAction(String action) {
                this.action = action;
            }

            public void setEventTime(String eventTime) {
                this.eventTime = eventTime;
            }

            public void setReferrerId(String referrerId) {
                this.referrerId = referrerId;
            }

            public String getMessage() {
                return message;
            }

            public String getInviteeLastName() {
                return inviteeLastName;
            }

            public String getInviteeFirstName() {
                return inviteeFirstName;
            }

            public String getInviteeId() {
                return inviteeId;
            }

            public String getAction() {
                return action;
            }

            public String getEventTime() {
                return eventTime;
            }

            public String getReferrerId() {
                return referrerId;
            }
        }
    }

    public class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
