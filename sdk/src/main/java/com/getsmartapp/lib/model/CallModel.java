package com.getsmartapp.lib.model;

import java.util.HashMap;

/**
 * @author Jayant.B on 7/23/2015.
 */
public class CallModel {
    private int local_call = 0;
    private int national_call = 0;
    private int international_call = 0;
    private int call_count = 0;
    private String call_type;
    private HashMap<String, Integer> hashMap;
    private long start_call_time;

    public CallModel() {
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public long getStart_call_time() {
        return start_call_time;
    }

    public void setStart_call_time(long start_call_time) {
        this.start_call_time = start_call_time;
    }

    public CallModel(int local, int national, int international, int count, HashMap<String, Integer> hm) {
        this.local_call = local;
        this.national_call = national;
        this.international_call = international;
        this.call_count = count;
        this.hashMap = hm;
    }

    public HashMap<String, Integer> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, Integer> hashMap) {
        this.hashMap = hashMap;
    }

    public int getCall_count() {
        return call_count;
    }

    public void setCall_count(int call_count) {
        this.call_count = call_count;
    }

    public int getLocal_call() {
        return local_call;
    }

    public void setLocal_call(int local_call) {
        this.local_call = local_call;
    }

    public int getNational_call() {
        return national_call;
    }

    public void setNational_call(int national_call) {
        this.national_call = national_call;
    }

    public int getInternational_call() {
        return international_call;
    }

    public void setInternational_call(int international_call) {
        this.international_call = international_call;
    }
}
