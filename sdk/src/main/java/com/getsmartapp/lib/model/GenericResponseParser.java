package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author nitesh.verma on 11/4/15.
 */
public class GenericResponseParser {


    /**
     * status : 0
     * errors : {"errorList":[{"errorCode":106,"message":"fName should only contain alphabets."}]}
     */

    private HeaderEntity header;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;

        public void setStatus(String status) {
            this.status = status;
        }

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 106
             * message : fName should only contain alphabets.
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }
}
