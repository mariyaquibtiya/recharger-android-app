package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author  nitesh.verma on 1/18/16.
 */
public class Geocode {

    /**
     * results : [{"address_components":[{"long_name":"34","short_name":"34","types":["street_number"]},{"long_name":"Via Pasubio","short_name":"Via Pasubio","types":["route"]},{"long_name":"Dronero","short_name":"Dronero","types":["locality","political"]},{"long_name":"Dronero","short_name":"Dronero","types":["administrative_area_level_3","political"]},{"long_name":"Provincia di Cuneo","short_name":"CN","types":["administrative_area_level_2","political"]},{"long_name":"Piemonte","short_name":"Piemonte","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]},{"long_name":"12025","short_name":"12025","types":["postal_code"]}],"formatted_address":"Via Pasubio, 34, 12025 Dronero CN, Italy","geometry":{"location":{"lat":44.46481,"lng":7.355569999999999},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":44.4661589802915,"lng":7.356918980291502},"southwest":{"lat":44.4634610197085,"lng":7.354221019708497}}},"place_id":"ChIJ9X2GwQE-zRIRMqiTuJO0rbQ","types":["street_address"]},{"address_components":[{"long_name":"Dronero","short_name":"Dronero","types":["locality","political"]},{"long_name":"Dronero","short_name":"Dronero","types":["administrative_area_level_3","political"]},{"long_name":"Provincia di Cuneo","short_name":"CN","types":["administrative_area_level_2","political"]},{"long_name":"Piemonte","short_name":"Piemonte","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]},{"long_name":"12025","short_name":"12025","types":["postal_code"]}],"formatted_address":"12025 Dronero CN, Italy","geometry":{"bounds":{"northeast":{"lat":44.4725466,"lng":7.380186699999999},"southwest":{"lat":44.4564415,"lng":7.3389068}},"location":{"lat":44.4658423,"lng":7.3656691},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":44.4725466,"lng":7.380186699999999},"southwest":{"lat":44.4564415,"lng":7.3389068}}},"place_id":"ChIJOxWz-gI-zRIRkPwCPK3Cxnk","types":["locality","political"]},{"address_components":[{"long_name":"Dronero","short_name":"Dronero","types":["administrative_area_level_3","political"]},{"long_name":"Province of Cuneo","short_name":"CN","types":["administrative_area_level_2","political"]},{"long_name":"Piedmont","short_name":"Piedmont","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]}],"formatted_address":"Dronero, Province of Cuneo, Italy","geometry":{"bounds":{"northeast":{"lat":44.4861443,"lng":7.445563599999999},"southwest":{"lat":44.4245393,"lng":7.229214100000001}},"location":{"lat":44.4394719,"lng":7.297661700000001},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":44.4861443,"lng":7.445563599999999},"southwest":{"lat":44.4245393,"lng":7.229214100000001}}},"place_id":"ChIJs9s7eBoWzRIRkKc8R33mBQQ","types":["administrative_area_level_3","political"]},{"address_components":[{"long_name":"12025","short_name":"12025","types":["postal_code"]},{"long_name":"Province of Cuneo","short_name":"CN","types":["administrative_area_level_2","political"]},{"long_name":"Piedmont","short_name":"Piedmont","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]}],"formatted_address":"12025 Province of Cuneo, Italy","geometry":{"bounds":{"northeast":{"lat":44.486172,"lng":7.4455469},"southwest":{"lat":44.417001,"lng":7.229196}},"location":{"lat":44.4540825,"lng":7.362459800000001},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":44.486172,"lng":7.4455469},"southwest":{"lat":44.417001,"lng":7.229196}}},"place_id":"ChIJcSTCDxEWzRIR4DK3k4DmBRw","types":["postal_code"]},{"address_components":[{"long_name":"Province of Cuneo","short_name":"CN","types":["administrative_area_level_2","political"]},{"long_name":"Piedmont","short_name":"Piedmont","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]}],"formatted_address":"Province of Cuneo, Italy","geometry":{"bounds":{"northeast":{"lat":44.8565911,"lng":8.2687425},"southwest":{"lat":44.0600899,"lng":6.8534305}},"location":{"lat":44.5970314,"lng":7.6114217},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":44.8565911,"lng":8.2687425},"southwest":{"lat":44.0600899,"lng":6.8534305}}},"place_id":"ChIJedLPEntCzRIRoH08R33mBQM","types":["administrative_area_level_2","political"]},{"address_components":[{"long_name":"Piedmont","short_name":"Piedmont","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]}],"formatted_address":"Piedmont, Italy","geometry":{"bounds":{"northeast":{"lat":46.4644351,"lng":9.2142641},"southwest":{"lat":44.0600899,"lng":6.6266304}},"location":{"lat":45.0522366,"lng":7.5153885},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":46.4644351,"lng":9.2142641},"southwest":{"lat":44.0600899,"lng":6.6266304}}},"place_id":"ChIJZ7GdATt0h0cR_zsS8u24x7w","types":["administrative_area_level_1","political"]},{"address_components":[{"long_name":"Italy","short_name":"IT","types":["country","political"]}],"formatted_address":"Italy","geometry":{"bounds":{"northeast":{"lat":47.092,"lng":18.5205015},"southwest":{"lat":35.4930165,"lng":6.6267201}},"location":{"lat":41.87194,"lng":12.56738},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":47.092,"lng":18.5205015},"southwest":{"lat":35.4931088,"lng":6.6267201}}},"place_id":"ChIJA9KNRIL-1BIRb15jJFz1LOI","types":["country","political"]}]
     * status : OK
     */

    private String status;
    /**
     * address_components : [{"long_name":"34","short_name":"34","types":["street_number"]},{"long_name":"Via Pasubio","short_name":"Via Pasubio","types":["route"]},{"long_name":"Dronero","short_name":"Dronero","types":["locality","political"]},{"long_name":"Dronero","short_name":"Dronero","types":["administrative_area_level_3","political"]},{"long_name":"Provincia di Cuneo","short_name":"CN","types":["administrative_area_level_2","political"]},{"long_name":"Piemonte","short_name":"Piemonte","types":["administrative_area_level_1","political"]},{"long_name":"Italy","short_name":"IT","types":["country","political"]},{"long_name":"12025","short_name":"12025","types":["postal_code"]}]
     * formatted_address : Via Pasubio, 34, 12025 Dronero CN, Italy
     * geometry : {"location":{"lat":44.46481,"lng":7.355569999999999},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":44.4661589802915,"lng":7.356918980291502},"southwest":{"lat":44.4634610197085,"lng":7.354221019708497}}}
     * place_id : ChIJ9X2GwQE-zRIRMqiTuJO0rbQ
     * types : ["street_address"]
     */

    private List<ResultsEntity> results;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }

    public static class ResultsEntity {
        private String formatted_address;
        /**
         * location : {"lat":44.46481,"lng":7.355569999999999}
         * location_type : ROOFTOP
         * viewport : {"northeast":{"lat":44.4661589802915,"lng":7.356918980291502},"southwest":{"lat":44.4634610197085,"lng":7.354221019708497}}
         */

        private GeometryEntity geometry;
        private String place_id;
        /**
         * long_name : 34
         * short_name : 34
         * types : ["street_number"]
         */

        private List<AddressComponentsEntity> address_components;
        private List<String> types;

        public void setFormatted_address(String formatted_address) {
            this.formatted_address = formatted_address;
        }

        public void setGeometry(GeometryEntity geometry) {
            this.geometry = geometry;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public void setAddress_components(List<AddressComponentsEntity> address_components) {
            this.address_components = address_components;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }

        public String getFormatted_address() {
            return formatted_address;
        }

        public GeometryEntity getGeometry() {
            return geometry;
        }

        public String getPlace_id() {
            return place_id;
        }

        public List<AddressComponentsEntity> getAddress_components() {
            return address_components;
        }

        public List<String> getTypes() {
            return types;
        }

        public static class GeometryEntity {
            /**
             * lat : 44.46481
             * lng : 7.355569999999999
             */

            private LocationEntity location;
            private String location_type;
            /**
             * northeast : {"lat":44.4661589802915,"lng":7.356918980291502}
             * southwest : {"lat":44.4634610197085,"lng":7.354221019708497}
             */

            private ViewportEntity viewport;

            public void setLocation(LocationEntity location) {
                this.location = location;
            }

            public void setLocation_type(String location_type) {
                this.location_type = location_type;
            }

            public void setViewport(ViewportEntity viewport) {
                this.viewport = viewport;
            }

            public LocationEntity getLocation() {
                return location;
            }

            public String getLocation_type() {
                return location_type;
            }

            public ViewportEntity getViewport() {
                return viewport;
            }

            public static class LocationEntity {
                private double lat;
                private double lng;

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }

                public double getLat() {
                    return lat;
                }

                public double getLng() {
                    return lng;
                }
            }

            public static class ViewportEntity {
                /**
                 * lat : 44.4661589802915
                 * lng : 7.356918980291502
                 */

                private NortheastEntity northeast;
                /**
                 * lat : 44.4634610197085
                 * lng : 7.354221019708497
                 */

                private SouthwestEntity southwest;

                public void setNortheast(NortheastEntity northeast) {
                    this.northeast = northeast;
                }

                public void setSouthwest(SouthwestEntity southwest) {
                    this.southwest = southwest;
                }

                public NortheastEntity getNortheast() {
                    return northeast;
                }

                public SouthwestEntity getSouthwest() {
                    return southwest;
                }

                public static class NortheastEntity {
                    private double lat;
                    private double lng;

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }

                    public double getLat() {
                        return lat;
                    }

                    public double getLng() {
                        return lng;
                    }
                }

                public static class SouthwestEntity {
                    private double lat;
                    private double lng;

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }

                    public double getLat() {
                        return lat;
                    }

                    public double getLng() {
                        return lng;
                    }
                }
            }
        }

        public static class AddressComponentsEntity {
            private String long_name;
            private String short_name;
            private List<String> types;

            public void setLong_name(String long_name) {
                this.long_name = long_name;
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public void setTypes(List<String> types) {
                this.types = types;
            }

            public String getLong_name() {
                return long_name;
            }

            public String getShort_name() {
                return short_name;
            }

            public List<String> getTypes() {
                return types;
            }
        }
    }
}
