package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author Jayant.B on 7/23/2015.
 */
public class PlanDetailModel {

    /**
     * header : {"errors":{"errorList":[{"errorCode":"102","message":"No Plans Found."}]},"status":"1"}
     * body : {"count":52,"planList":[{"serviceType":"PREPAID_MOBILE","amount":44,"serviceProviderId":1,"rechargeValue":0,"description":"78 STD mins","serviceProvider":"Airtel","planId":595,"circleId":5,"validity":"5 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9},{"serviceType":"PREPAID_MOBILE","amount":45,"serviceProviderId":1,"rechargeValue":0,"description":"110 Local Airtel mobile minutes","serviceProvider":"Airtel","planId":596,"circleId":5,"validity":"6 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}],"recPlans":[{"cost":5912.276433819253,"planDetail":{"serviceType":"PREPAID_MOBILE","amount":299,"serviceProviderId":1,"rechargeValue":0,"description":"650 Local Minutes","serviceProvider":"Airtel","planId":631,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}},{"cost":5940.776433819252,"planDetail":{"serviceType":"PREPAID_MOBILE","amount":399,"serviceProviderId":1,"rechargeValue":0,"description":"900 Local + STD Minutes","serviceProvider":"Airtel","planId":632,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}}]}
     */
    private HeaderEntity header;
    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public class HeaderEntity {
        /**
         * errors : {"errorList":[{"errorCode":"102","message":"No Plans Found."}]}
         * status : 1
         */
        private ErrorsEntity errors;
        private String status;

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public String getStatus() {
            return status;
        }

        public class ErrorsEntity {
            /**
             * errorList : [{"errorCode":"102","message":"No Plans Found."}]
             */
            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public class ErrorListEntity {
                /**
                 * errorCode : 102
                 * message : No Plans Found.
                 */
                private String errorCode;
                private String message;

                public void setErrorCode(String errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public String getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }

    public class BodyEntity {
        /**
         * count : 52
         * planList : [{"serviceType":"PREPAID_MOBILE","amount":44,"serviceProviderId":1,"rechargeValue":0,"description":"78 STD mins","serviceProvider":"Airtel","planId":595,"circleId":5,"validity":"5 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9},{"serviceType":"PREPAID_MOBILE","amount":45,"serviceProviderId":1,"rechargeValue":0,"description":"110 Local Airtel mobile minutes","serviceProvider":"Airtel","planId":596,"circleId":5,"validity":"6 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}]
         * recPlans : [{"cost":5912.276433819253,"planDetail":{"serviceType":"PREPAID_MOBILE","amount":299,"serviceProviderId":1,"rechargeValue":0,"description":"650 Local Minutes","serviceProvider":"Airtel","planId":631,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}},{"cost":5940.776433819252,"planDetail":{"serviceType":"PREPAID_MOBILE","amount":399,"serviceProviderId":1,"rechargeValue":0,"description":"900 Local + STD Minutes","serviceProvider":"Airtel","planId":632,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"Discounted Tariff Recharge","isExpired":false,"categoryId":9}}]
         */
        private int count;
        private List<PlanListEntity> planList;
        private List<RecPlansEntity> recPlans;

        public void setCount(int count) {
            this.count = count;
        }

        public void setPlanList(List<PlanListEntity> planList) {
            this.planList = planList;
        }

        public void setRecPlans(List<RecPlansEntity> recPlans) {
            this.recPlans = recPlans;
        }

        public int getCount() {
            return count;
        }

        public List<PlanListEntity> getPlanList() {
            return planList;
        }

        public List<RecPlansEntity> getRecPlans() {
            return recPlans;
        }

        public class PlanListEntity {
            /**
             * serviceType : PREPAID_MOBILE
             * amount : 44
             * serviceProviderId : 1
             * rechargeValue : 0
             * description : 78 STD mins
             * serviceProvider : Airtel
             * planId : 595
             * circleId : 5
             * validity : 5 Days
             * circle : Delhi NCR
             * category : Discounted Tariff Recharge
             * isExpired : false
             * categoryId : 9
             */
            private String serviceType;
            private int amount;
            private int serviceProviderId;
            private double rechargeValue;
            private String description;
            private String serviceProvider;
            private int planId;
            private int circleId;
            private String validity;
            private String circle;
            private String category;
            private boolean isExpired;
            private int categoryId;

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public void setServiceProviderId(int serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public void setRechargeValue(double rechargeValue) {
                this.rechargeValue = rechargeValue;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public void setServiceProvider(String serviceProvider) {
                this.serviceProvider = serviceProvider;
            }

            public void setPlanId(int planId) {
                this.planId = planId;
            }

            public void setCircleId(int circleId) {
                this.circleId = circleId;
            }

            public void setValidity(String validity) {
                this.validity = validity;
            }

            public void setCircle(String circle) {
                this.circle = circle;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public void setIsExpired(boolean isExpired) {
                this.isExpired = isExpired;
            }

            public void setCategoryId(int categoryId) {
                this.categoryId = categoryId;
            }

            public String getServiceType() {
                return serviceType;
            }

            public int getAmount() {
                return amount;
            }

            public int getServiceProviderId() {
                return serviceProviderId;
            }

            public double getRechargeValue() {
                return rechargeValue;
            }

            public String getDescription() {
                return description;
            }

            public String getServiceProvider() {
                return serviceProvider;
            }

            public int getPlanId() {
                return planId;
            }

            public int getCircleId() {
                return circleId;
            }

            public String getValidity() {
                return validity;
            }

            public String getCircle() {
                return circle;
            }

            public String getCategory() {
                return category;
            }

            public boolean isIsExpired() {
                return isExpired;
            }

            public int getCategoryId() {
                return categoryId;
            }
        }


    }
}