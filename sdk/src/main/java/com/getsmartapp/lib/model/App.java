package com.getsmartapp.lib.model;

import java.io.Serializable;

/**
 * @author Nitesh.Verma on 18-03-2015.
 */
public class App implements Serializable {
    private String app_name;
    private String icon;
    private float mobile_dataUsed;
    private float wifi_dataUsed;
    private float total_dataUsed;
    private boolean sys_app;
    private int is_system_app;
    private String app_package_name;


    public int getIs_system_app() {
        return is_system_app;
    }

    public void setIs_system_app(int is_system_app) {
        this.is_system_app = is_system_app;
    }

    public App(String app_name, String icon, float mobile_dataUsed, float wifi_dataUsed, int
            is_system_app) {
        this.app_name = app_name;
        this.icon = icon;
        this.mobile_dataUsed = mobile_dataUsed;
        this.wifi_dataUsed = wifi_dataUsed;
        this.is_system_app = is_system_app;
    }

    public App(String app_name, String packageName, float mobile_dataUsed, float wifi_dataUsed) {
        this.app_name = app_name;
        this.mobile_dataUsed = mobile_dataUsed;
        this.wifi_dataUsed = wifi_dataUsed;
        this.total_dataUsed = mobile_dataUsed + wifi_dataUsed;
        this.app_package_name = packageName;
    }

    public App(String app_name, float total_dataUsed, boolean sys_app) {
        this.app_name = app_name;
        this.total_dataUsed = total_dataUsed;
        this.sys_app = sys_app;
    }

    public App(String app_name, float total_dataUsed) {
        this.app_name = app_name;
        this.total_dataUsed = total_dataUsed;
    }

    public String getApp_name() {
        return app_name;
    }

    public String getApp_package_name()
    {
        return app_package_name;
    }
    public String getIcon() {
        return icon;
    }

    public float getmobile_dataUsed() {
        return mobile_dataUsed;
    }

    public float getWifi_dataUsed() {
        return wifi_dataUsed;
    }

    public float getTotal_dataUsed() {
        return total_dataUsed;
    }

    public void setTotal_dataUsed(float total_dataUsed) {
        this.total_dataUsed = total_dataUsed;
    }

    public void setMobile_dataUsed(float mobile_dataUsed) {
        this.mobile_dataUsed = mobile_dataUsed;
    }

    public boolean isSys_app() {
        return sys_app;
    }

    public void setSys_app(boolean sys_app) {
        this.sys_app = sys_app;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);

    }
}
