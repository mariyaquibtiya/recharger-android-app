package com.getsmartapp.lib.model;

public class ReferrerEarnings {

    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        private int amountRedeemable;
        private int amountClaimed;
        private String referrerId;
        private int totalEarnings;

        public void setAmountRedeemable(int amountRedeemable) {
            this.amountRedeemable = amountRedeemable;
        }

        public void setAmountClaimed(int amountClaimed) {
            this.amountClaimed = amountClaimed;
        }

        public void setReferrerId(String referrerId) {
            this.referrerId = referrerId;
        }

        public void setTotalEarnings(int totalEarnings) {
            this.totalEarnings = totalEarnings;
        }

        public int getAmountRedeemable() {
            return amountRedeemable;
        }

        public int getAmountClaimed() {
            return amountClaimed;
        }

        public String getReferrerId() {
            return referrerId;
        }

        public int getTotalEarnings() {
            return totalEarnings;
        }
    }

    public class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
