package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author bishwajeet.kumar on 19/07/15.
 */
public class ReferralActivityDetails {

    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        private int count;
        private int redeemableEarnings;
        private int totalEarnings;
        private List<ReferralListEntity> referralList;

        public void setCount(int count) {
            this.count = count;
        }

        public void setRedeemableEarnings(int redeemableEarnings) {
            this.redeemableEarnings = redeemableEarnings;
        }

        public void setTotalEarnings(int totalEarnings) {
            this.totalEarnings = totalEarnings;
        }

        public void setReferralList(List<ReferralListEntity> referralList) {
            this.referralList = referralList;
        }

        public int getCount() {
            return count;
        }

        public int getRedeemableEarnings() {
            return redeemableEarnings;
        }

        public int getTotalEarnings() {
            return totalEarnings;
        }

        public List<ReferralListEntity> getReferralList() {
            return referralList;
        }

        public class ReferralListEntity {
            /**
             * message : prateek has made first transaction and you earned Rs 5.0. Tap to copy your code SRG1S457ROQMR35
             * couponExpiryDate : Aug 15, 2015
             * referrerGcAmount : 5
             * inviteeLastName : shekhar
             * status : ACTIVE
             * inviteeFirstName : prateek
             * gcType : REFERRAL_REFERRER
             * inviteeId : arx50fhjek6htxxqh87u4kkcr
             * action : FIRST_TRANSACTION
             * eventTime : Jul 16, 2015 9:18:04 PM
             * referrerGcCode : SRG1S457ROQMR35
             * referrerId : 77qg0c8wxr851onzucstu78ge
             */
            private String message;
            private String couponExpiryDate;
            private int referrerGcAmount;
            private String inviteeLastName;
            private String status;
            private String inviteeFirstName;
            private String gcType;
            private String inviteeId;
            private String action;
            private String eventTime;
            private String referrerGcCode;
            private String referrerId;

            public void setMessage(String message) {
                this.message = message;
            }

            public void setCouponExpiryDate(String couponExpiryDate) {
                this.couponExpiryDate = couponExpiryDate;
            }

            public void setReferrerGcAmount(int referrerGcAmount) {
                this.referrerGcAmount = referrerGcAmount;
            }

            public void setInviteeLastName(String inviteeLastName) {
                this.inviteeLastName = inviteeLastName;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public void setInviteeFirstName(String inviteeFirstName) {
                this.inviteeFirstName = inviteeFirstName;
            }

            public void setGcType(String gcType) {
                this.gcType = gcType;
            }

            public void setInviteeId(String inviteeId) {
                this.inviteeId = inviteeId;
            }

            public void setAction(String action) {
                this.action = action;
            }

            public void setEventTime(String eventTime) {
                this.eventTime = eventTime;
            }

            public void setReferrerGcCode(String referrerGcCode) {
                this.referrerGcCode = referrerGcCode;
            }

            public void setReferrerId(String referrerId) {
                this.referrerId = referrerId;
            }

            public String getMessage() {
                return message;
            }

            public String getCouponExpiryDate() {
                return couponExpiryDate;
            }

            public int getReferrerGcAmount() {
                return referrerGcAmount;
            }

            public String getInviteeLastName() {
                return inviteeLastName;
            }

            public String getStatus() {
                return status;
            }

            public String getInviteeFirstName() {
                return inviteeFirstName;
            }

            public String getGcType() {
                return gcType;
            }

            public String getInviteeId() {
                return inviteeId;
            }

            public String getAction() {
                return action;
            }

            public String getEventTime() {
                return eventTime;
            }

            public String getReferrerGcCode() {
                return referrerGcCode;
            }

            public String getReferrerId() {
                return referrerId;
            }
        }
    }

    public class HeaderEntity {
        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
