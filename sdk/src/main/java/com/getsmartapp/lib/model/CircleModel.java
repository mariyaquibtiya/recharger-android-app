package com.getsmartapp.lib.model;

import java.io.Serializable;

/**
 * @author Shalakha.Gupta on 30-05-2015.
 */
public class CircleModel implements Serializable {

    int circleId;
    String circle;

    public CircleModel(int circleId, String circle) {
        this.circleId = circleId;
        this.circle = circle;
    }

    public CircleModel() {
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }
}
