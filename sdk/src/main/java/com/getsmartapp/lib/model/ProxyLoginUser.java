package com.getsmartapp.lib.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nitesh.verma on 11/26/15.
 */
public class ProxyLoginUser {


    /**
     * site : googleplus
     * loginId : jhumbraberjhum@gmail.com
     * lastName : Talaiya
     * transTicket : Xby5EVdXMXYrvsDmbGJQZDWY1gYRBLd_cQecUuAH1iSJJWMztQrnmgHTgCPyl8pd
     * enct : 5SGBf7v7MdH0Zr5UsTwYOXlwBB4iCmjM7QCVXDakcGBgCPGtra_wTg
     * gassoid : gauid-2bMrsKx7y4YXR2IWM2-RtdYKqVh4q_eejJdD-a0Hcs
     * siteReg :
     * password_changed : 2015-09-18 20:10:13
     * code : 200
     * userType : 1
     * expiryTime : 2015-12-26 13:47:52
     * lastLoginType : googleplus
     * dateRegistered : 2015-09-18 20:10:13.0
     * emailId : jhumbraberjhum@gmail.com
     * activeUser : true
     * ticketId : 769e8bb21ae143d3adbdd91e055430a6
     * userId : 3mxoezhi3z5uch4ih05p4np74
     * checkInterval : 1440
     * firstName : Jhumri
     * primaryEmailId : jhumbraberjhum@gmail.com
     * allemailids : [{"email":"jhumbraberjhum@gmail.com","verified":"1","primary":"1"}]
     * createdDt : 2015-09-18 20:10:13.0
     */

    private SoResponseEntity soResponse;
    /**
     * code : 200.0
     * msg : success
     * mobile : 8587035342
     * ssoid : 3mxoezhi3z5uch4ih05p4np74
     */

    private AddMobileApiEntity addMobileApi;
    /**
     * siteId : 25b1e96ac22e8c5be050429f7facf172
     * oauthId : 116118757439513192416
     * accessToken : ya29.OAIhOm5gD421AU4IfcKnxhMtr2wPaKpGTFqz2t1_fOYfwwLlTHVguHvfwZEOL0MAOBrBp10
     * channel : recharger
     * loginType : google
     * phoneNumber : 8587035342
     * isCurrentId : y
     * isReferred : 0
     * referrerId :
     * ticketId : 769e8bb21ae143d3adbdd91e055430a6
     */

    private SignupEntity signup;
    /**
     * header : {"status":"1"}
     * body : {"referralCode":"NITESH","referralLink":"http: //smrtp.com/NITESH","wsHash":"d88385b73f31163b6fff2aa4c00a174f659c23092a8a2ba1ffa684619453197c6cfb73e9a5a8c6bd76a2bda29da0ff738495663028d34056941db969b7d88283","deleteHash":"1eb725ab4dc2dec4d872b555534691e52e1316b2be2a0bd4c4fcf6dbbdd6a86c9d584e1bb68ba90f4fb68e67ff1031f4bf9b46db580ad7fe497c8ccf27584387","storedCardsHash":"5498e8361539cabf92a10d2cb46dd8d67fc0adf78d3d79b8c2e632bf6423f0fa89b319355edb94466d4ebf135198298ca68654a9a804f69be4526164d2847d98","fetchCardsHash":"398cb6e7626a50234d57e23e8850d9c89f3dc562fb37b077e5a8e73ebca6980566fc0cf5151f73a278d8d421aebfe15392eb96e5cc312af0098c8b83eac35a8e","vasCardsHash":"7da0f4fef5bab0e5034f37f9503bdcbede00cc2cd0cf6cbb4e43baa9d57f05680305885199e2b0d38e8cf12895fd06f4d3dd3fb422535feeb555adc58e2cf3cc","isActiveInviteeGC":"0","hasDoneFirstTransaction":"1","isNewDevice":"0","isReferredDevice":0,"preferences":{"pushTransPref":0,"pushPromotionalPref":1,"emailPref":1,"smsPref":0}}
     */

    private CompleteRegistrationResponseEntity completeRegistrationResponse;

    public void setSoResponse(SoResponseEntity soResponse) {
        this.soResponse = soResponse;
    }

    public void setAddMobileApi(AddMobileApiEntity addMobileApi) {
        this.addMobileApi = addMobileApi;
    }

    public void setSignup(SignupEntity signup) {
        this.signup = signup;
    }

    public void setCompleteRegistrationResponse(CompleteRegistrationResponseEntity completeRegistrationResponse) {
        this.completeRegistrationResponse = completeRegistrationResponse;
    }

    public SoResponseEntity getSoResponse() {
        return soResponse;
    }

    public AddMobileApiEntity getAddMobileApi() {
        return addMobileApi;
    }

    public SignupEntity getSignup() {
        return signup;
    }

    public CompleteRegistrationResponseEntity getCompleteRegistrationResponse() {
        return completeRegistrationResponse;
    }

    public static class SoResponseEntity implements Parcelable{

          /*site : googleplus
          loginId : ramrahim057@gmail.com
          lastName : mittle
          transTicket : E356G3uLf0ePOolkhVode8d6SUoSS-3JG4s4UXE5Fmw3IVbNU2frtOFrzomq2H6k
          enct : isqo92PI5QVLkFCEit9DcU-_98TmGuaD-yx7fH-jKl5gCPGtra_wTg
          gassoid : gauidZgOKxmLj0L00Zryd1HWHhUNH6Dd1nTC8a4nZisWQ73E
          siteReg :
          password_changed : 2015-11-16 17:00:43
          code : 200
          userType : 1
          expiryTime : 2015-12-29 02:09:28
          lastLoginType : googleplus
          dateRegistered : 2015-11-16 17:00:43.0
          emailId : ramrahim057@gmail.com
          activeUser : true
          verifiedMobile : 8587035342
          ticketId : 881f9411796c494dbfe46c185612ca23
          userId : 3vv262mfccmvp80wvbjeigim7
          checkInterval : 1440
          firstName : ramrahim
          primaryEmailId : ramrahim057@gmail.com
          allemailids : [{"email":"ramrahim057@gmail.com","verified":"1","primary":"1"}]
          createdDt : 2015-11-16 17:00:43.0*/

        private String site;
        private String loginId;
        private String lastName;
        private String transTicket;
        private String enct;
        private String gassoid;
        private String siteReg;
        private String password_changed;
        private String code;
        private String userType;
        private String expiryTime;
        private String lastLoginType;
        private String dateRegistered;
        private String emailId;
        private String activeUser;
        private String verifiedMobile;
        private String ticketId;
        private String userId;
        private String checkInterval;
        private String firstName;
        private String primaryEmailId;
        private String createdDt;
        /**
         * email : ramrahim057@gmail.com
         * verified : 1
         * primary : 1
         */

        private List<AllemailidsEntity> allemailids;

        protected SoResponseEntity(Parcel in) {
            site = in.readString();
            loginId = in.readString();
            lastName = in.readString();
            transTicket = in.readString();
            enct = in.readString();
            gassoid = in.readString();
            siteReg = in.readString();
            password_changed = in.readString();
            code = in.readString();
            userType = in.readString();
            expiryTime = in.readString();
            lastLoginType = in.readString();
            dateRegistered = in.readString();
            emailId = in.readString();
            activeUser = in.readString();
            verifiedMobile = in.readString();
            ticketId = in.readString();
            userId = in.readString();
            checkInterval = in.readString();
            firstName = in.readString();
            primaryEmailId = in.readString();
            createdDt = in.readString();
        }

        public static final Creator<SoResponseEntity> CREATOR = new Creator<SoResponseEntity>() {
            @Override
            public SoResponseEntity createFromParcel(Parcel in) {
                return new SoResponseEntity(in);
            }

            @Override
            public SoResponseEntity[] newArray(int size) {
                return new SoResponseEntity[size];
            }
        };

        public void setSite(String site) {
            this.site = site;
        }

        public void setLoginId(String loginId) {
            this.loginId = loginId;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setTransTicket(String transTicket) {
            this.transTicket = transTicket;
        }

        public void setEnct(String enct) {
            this.enct = enct;
        }

        public void setGassoid(String gassoid) {
            this.gassoid = gassoid;
        }

        public void setSiteReg(String siteReg) {
            this.siteReg = siteReg;
        }

        public void setPassword_changed(String password_changed) {
            this.password_changed = password_changed;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public void setExpiryTime(String expiryTime) {
            this.expiryTime = expiryTime;
        }

        public void setLastLoginType(String lastLoginType) {
            this.lastLoginType = lastLoginType;
        }

        public void setDateRegistered(String dateRegistered) {
            this.dateRegistered = dateRegistered;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public void setActiveUser(String activeUser) {
            this.activeUser = activeUser;
        }

        public void setVerifiedMobile(String verifiedMobile) {
            this.verifiedMobile = verifiedMobile;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setCheckInterval(String checkInterval) {
            this.checkInterval = checkInterval;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setPrimaryEmailId(String primaryEmailId) {
            this.primaryEmailId = primaryEmailId;
        }

        public void setCreatedDt(String createdDt) {
            this.createdDt = createdDt;
        }

        public void setAllemailids(List<AllemailidsEntity> allemailids) {
            this.allemailids = allemailids;
        }

        public String getSite() {
            return site;
        }

        public String getLoginId() {
            return loginId;
        }

        public String getLastName() {
            return lastName;
        }

        public String getTransTicket() {
            return transTicket;
        }

        public String getEnct() {
            return enct;
        }

        public String getGassoid() {
            return gassoid;
        }

        public String getSiteReg() {
            return siteReg;
        }

        public String getPassword_changed() {
            return password_changed;
        }

        public String getCode() {
            return code;
        }

        public String getUserType() {
            return userType;
        }

        public String getExpiryTime() {
            return expiryTime;
        }

        public String getLastLoginType() {
            return lastLoginType;
        }

        public String getDateRegistered() {
            return dateRegistered;
        }

        public String getEmailId() {
            return emailId;
        }

        public String getActiveUser() {
            return activeUser;
        }

        public String getVerifiedMobile() {
            return verifiedMobile;
        }

        public String getTicketId() {
            return ticketId;
        }

        public String getUserId() {
            return userId;
        }

        public String getCheckInterval() {
            return checkInterval;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getPrimaryEmailId() {
            return primaryEmailId;
        }

        public String getCreatedDt() {
            return createdDt;
        }

        public List<AllemailidsEntity> getAllemailids() {
            return allemailids;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(site);
            dest.writeString(loginId);
            dest.writeString(lastName);
            dest.writeString(transTicket);
            dest.writeString(enct);
            dest.writeString(gassoid);
            dest.writeString(siteReg);
            dest.writeString(password_changed);
            dest.writeString(code);
            dest.writeString(userType);
            dest.writeString(expiryTime);
            dest.writeString(lastLoginType);
            dest.writeString(dateRegistered);
            dest.writeString(emailId);
            dest.writeString(activeUser);
            dest.writeString(verifiedMobile);
            dest.writeString(ticketId);
            dest.writeString(userId);
            dest.writeString(checkInterval);
            dest.writeString(firstName);
            dest.writeString(primaryEmailId);
            dest.writeString(createdDt);
        }

        public static class AllemailidsEntity {
            private String email;
            private String verified;
            private String primary;

            public void setEmail(String email) {
                this.email = email;
            }

            public void setVerified(String verified) {
                this.verified = verified;
            }

            public void setPrimary(String primary) {
                this.primary = primary;
            }

            public String getEmail() {
                return email;
            }

            public String getVerified() {
                return verified;
            }

            public String getPrimary() {
                return primary;
            }
        }
    }

    public static class AddMobileApiEntity implements Parcelable{
        private int code;
        private String msg;
        private String mobile;
        private String ssoid;

        protected AddMobileApiEntity(Parcel in) {
            code = in.readInt();
            msg = in.readString();
            mobile = in.readString();
            ssoid = in.readString();
        }

        public static final Creator<AddMobileApiEntity> CREATOR = new Creator<AddMobileApiEntity>() {
            @Override
            public AddMobileApiEntity createFromParcel(Parcel in) {
                return new AddMobileApiEntity(in);
            }

            @Override
            public AddMobileApiEntity[] newArray(int size) {
                return new AddMobileApiEntity[size];
            }
        };

        public void setCode(int code) {
            this.code = code;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public void setSsoid(String ssoid) {
            this.ssoid = ssoid;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }

        public String getMobile() {
            return mobile;
        }

        public String getSsoid() {
            return ssoid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(code);
            dest.writeString(msg);
            dest.writeString(mobile);
            dest.writeString(ssoid);
        }
    }

    public static class SignupEntity implements Parcelable{
        private String siteId;
        private String oauthId;
        private String accessToken;
        private String channel;
        private String loginType;
        private String phoneNumber;
        private String isCurrentId;
        private String isReferred;
        private String referrerId;
        private String ticketId;
        private String deviceId;
        private String facebookDeviceId;

        private String onboardNumber;
        private String ipAddress;
        private String circleId;
        private String serviceProviderId;
        private String isPrepaid;

        protected SignupEntity(Parcel in) {
            siteId = in.readString();
            oauthId = in.readString();
            accessToken = in.readString();
            channel = in.readString();
            loginType = in.readString();
            phoneNumber = in.readString();
            isCurrentId = in.readString();
            isReferred = in.readString();
            referrerId = in.readString();
            ticketId = in.readString();
            deviceId = in.readString();
            facebookDeviceId = in.readString();

            onboardNumber = in.readString();
            ipAddress = in.readString();
            circleId = in.readString();
            serviceProviderId = in.readString();
            isPrepaid = in.readString();
        }

        public static final Creator<SignupEntity> CREATOR = new Creator<SignupEntity>() {
            @Override
            public SignupEntity createFromParcel(Parcel in) {
                return new SignupEntity(in);
            }

            @Override
            public SignupEntity[] newArray(int size) {
                return new SignupEntity[size];
            }
        };

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public void setSiteId(String siteId) {
            this.siteId = siteId;
        }

        public void setOauthId(String oauthId) {
            this.oauthId = oauthId;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public void setIsCurrentId(String isCurrentId) {
            this.isCurrentId = isCurrentId;
        }

        public void setIsReferred(String isReferred) {
            this.isReferred = isReferred;
        }

        public void setReferrerId(String referrerId) {
            this.referrerId = referrerId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getSiteId() {
            return siteId;
        }

        public String getOauthId() {
            return oauthId;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getChannel() {
            return channel;
        }

        public String getLoginType() {
            return loginType;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getIsCurrentId() {
            return isCurrentId;
        }

        public String getIsReferred() {
            return isReferred;
        }

        public String getReferrerId() {
            return referrerId;
        }

        public String getTicketId() {
            return ticketId;
        }

        public String getFacebookDeviceId() {
            return facebookDeviceId;
        }

        public void setFacebookDeviceId(String facebookDeviceId) {
            this.facebookDeviceId = facebookDeviceId;
        }

        public String getCircleId() {
            return circleId;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public String getIsPrepaid() {
            return isPrepaid;
        }

        public String getOnboardNumber() {
            return onboardNumber;
        }

        public String getServiceProviderId() {
            return serviceProviderId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(siteId);
            dest.writeString(oauthId);
            dest.writeString(accessToken);
            dest.writeString(channel);
            dest.writeString(loginType);
            dest.writeString(phoneNumber);
            dest.writeString(isCurrentId);
            dest.writeString(isReferred);
            dest.writeString(referrerId);
            dest.writeString(ticketId);
            dest.writeString(deviceId);
            dest.writeString(facebookDeviceId);

            dest.writeString(onboardNumber);
            dest.writeString(ipAddress);
            dest.writeString(circleId);
            dest.writeString(serviceProviderId);
            dest.writeString(isPrepaid);

        }
    }

    public static class CompleteRegistrationResponseEntity {
        /**
         * status : 1
         */

        private HeaderEntity header;
        /**
         * referralCode : BISHWAJEET9Z
         * referralLink : http://smrtp.com/bishwajeet9z
         * wsHash : 17479c82b39d66a7602eb60d5b1c7dfd8df6b4bcb9856751774c32e114f5b1e38628b9f83b8e60b33aa2c074e1eb9acbb606b537b1662cdd3c0d61dcd1a4e5c2
         * deleteHash : 4a1c6cbcd7d2d7879020bfd03eda1cd378ac0c9af35332079444edd915362fb9d037705ef4cc9ef83107b2963022b3ad6f0d95c3ee3833686d4e360901d958c0
         * storedCardsHash : 65ebedbac6f383e87b43cde74f4c3acb67fe1caff0ab97582ca46221ffb0a15331826c92742b19358d7ba3c2d8c58e18f0001aca2c099a09ad5df8ebfa6ebff5
         * fetchCardsHash : 8dc9b21554e2ab7c0834645ac88cd8a3f51239944c55a637d333ceb9b1122f96aa4824f84ee3326f9071119cb56e49ff64a00a22855ce06e011663bfe4568440
         * vasCardsHash : df34254cee84493537bf72bb1bd772b4978157aa4f086911569a7510cd4212442d65221bf4b39fe1ed9e8041618373e39ced2b9dacc26679a54d529365f3b5dc
         * isActiveInviteeGC : 0
         * inviteeGC : SBDLKFA34SKJFKDSJ
         * hasDoneFirstTransaction : 1
         * isNewDevice : 0
         * isReferredDevice : 0
         * preferences : {"pushTransPref":1,"pushPromotionalPref":1,"emailPref":1,"smsPref":1}
         * referrerBranchDetails : {"current_branch_user_identity":"ijuddkzsm8nk90ziu80c78v4","current_branch_user_name":"Bishwajeet","current_user_image_url":"","current_user_channel":"ijuddkzsm8nk90ziu80c78v4"}
         */

        private BodyEntity body;

        public static List<SignUpOnSmartAppModel> arraySignUpOnSmartAppModelFromData(String str) {

            Type listType = new TypeToken<ArrayList<SignUpOnSmartAppModel>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public void setHeader(HeaderEntity header) {
            this.header = header;
        }

        public void setBody(BodyEntity body) {
            this.body = body;
        }

        public HeaderEntity getHeader() {
            return header;
        }

        public BodyEntity getBody() {
            return body;
        }

        public static class HeaderEntity {
            private String status;
            private ErrorsEntity errors;

            public static List<HeaderEntity> arrayHeaderEntityFromData(String str) {

                Type listType = new TypeToken<ArrayList<HeaderEntity>>() {
                }.getType();
                return new Gson().fromJson(str, listType);
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public void setErrors(ErrorsEntity errors) {
                this.errors = errors;
            }

            public String getStatus() {
                return status;
            }

            public ErrorsEntity getErrors() {
                return errors;
            }

            public static class ErrorsEntity {
                /**
                 * errorCode : 106
                 * message : fName should only contain alphabets.
                 */

                private List<ErrorListEntity> errorList;

                public static List<ErrorsEntity> arrayErrorsEntityFromData(String str) {

                    Type listType = new TypeToken<ArrayList<ErrorsEntity>>() {
                    }.getType();

                    return new Gson().fromJson(str, listType);
                }

                public void setErrorList(List<ErrorListEntity> errorList) {
                    this.errorList = errorList;
                }

                public List<ErrorListEntity> getErrorList() {
                    return errorList;
                }

                public static class ErrorListEntity {
                    private int errorCode;
                    private String message;

                    public static List<ErrorListEntity> arrayErrorListEntityFromData(String str) {

                        Type listType = new TypeToken<ArrayList<ErrorListEntity>>() {
                        }.getType();

                        return new Gson().fromJson(str, listType);
                    }

                    public void setErrorCode(int errorCode) {
                        this.errorCode = errorCode;
                    }

                    public void setMessage(String message) {
                        this.message = message;
                    }

                    public int getErrorCode() {
                        return errorCode;
                    }

                    public String getMessage() {
                        return message;
                    }
                }
            }


        }

        public static class BodyEntity {
            private String referralCode;
            private String referralLink;
            private String wsHash;
            private String deleteHash;
            private String storedCardsHash;
            private String deleteStoreCardCvvHash;
            private String fetchCardsHash;
            private String vasCardsHash;
            private String isActiveInviteeGC;
            private String inviteeGC;
            private String hasDoneFirstTransaction;
            private String isNewDevice;
            private int isReferredDevice;
            private int isReferredPhNumber;
            /**
             * pushTransPref : 1
             * pushPromotionalPref : 1
             * emailPref : 1
             * smsPref : 1
             */

            private PreferencesEntity preferences;

            private WalletDataEntity walletData;
            /**
             * current_branch_user_identity : ijuddkzsm8nk90ziu80c78v4
             * current_branch_user_name : Bishwajeet
             * current_user_image_url :
             * current_user_channel : ijuddkzsm8nk90ziu80c78v4
             */

            private ReferrerBranchDetailsEntity referrerBranchDetails;

            public static List<BodyEntity> arrayBodyEntityFromData(String str) {

                Type listType = new TypeToken<ArrayList<BodyEntity>>() {
                }.getType();

                return new Gson().fromJson(str, listType);
            }

            public void setReferralCode(String referralCode) {
                this.referralCode = referralCode;
            }

            public void setReferralLink(String referralLink) {
                this.referralLink = referralLink;
            }

            public void setWsHash(String wsHash) {
                this.wsHash = wsHash;
            }

            public void setDeleteHash(String deleteHash) {
                this.deleteHash = deleteHash;
            }

            public void setStoredCardsHash(String storedCardsHash) {
                this.storedCardsHash = storedCardsHash;
            }

            public void setFetchCardsHash(String fetchCardsHash) {
                this.fetchCardsHash = fetchCardsHash;
            }

            public void setVasCardsHash(String vasCardsHash) {
                this.vasCardsHash = vasCardsHash;
            }

            public String getDeleteStoreCardCvvHash() {
                return deleteStoreCardCvvHash;
            }

            public void setDeleteStoreCardCvvHash(String deleteStoreCardCvvHash) {
                this.deleteStoreCardCvvHash = deleteStoreCardCvvHash;
            }

            public void setIsActiveInviteeGC(String isActiveInviteeGC) {
                this.isActiveInviteeGC = isActiveInviteeGC;
            }

            public void setInviteeGC(String inviteeGC) {
                this.inviteeGC = inviteeGC;
            }

            public void setHasDoneFirstTransaction(String hasDoneFirstTransaction) {
                this.hasDoneFirstTransaction = hasDoneFirstTransaction;
            }

            public void setIsNewDevice(String isNewDevice) {
                this.isNewDevice = isNewDevice;
            }

            public void setIsReferredDevice(int isReferredDevice) {
                this.isReferredDevice = isReferredDevice;
            }

            public void setPreferences(PreferencesEntity preferences) {
                this.preferences = preferences;
            }

            public void setReferrerBranchDetails(ReferrerBranchDetailsEntity referrerBranchDetails) {
                this.referrerBranchDetails = referrerBranchDetails;
            }

            public String getReferralCode() {
                return referralCode;
            }

            public String getReferralLink() {
                return referralLink;
            }

            public String getWsHash() {
                return wsHash;
            }

            public String getDeleteHash() {
                return deleteHash;
            }

            public String getStoredCardsHash() {
                return storedCardsHash;
            }

            public String getFetchCardsHash() {
                return fetchCardsHash;
            }

            public String getVasCardsHash() {
                return vasCardsHash;
            }

            public String getIsActiveInviteeGC() {
                return isActiveInviteeGC;
            }

            public String getInviteeGC() {
                return inviteeGC;
            }

            public String getHasDoneFirstTransaction() {
                return hasDoneFirstTransaction;
            }

            public String getIsNewDevice() {
                return isNewDevice;
            }

            public int getIsReferredDevice() {
                return isReferredDevice;
            }

            public int getIsReferredPhNumber() {
                return isReferredPhNumber;
            }

            public void setIsReferredPhNumber(int isReferredPhNumber) {
                this.isReferredPhNumber = isReferredPhNumber;
            }

            public PreferencesEntity getPreferences() {
                return preferences;
            }

            public WalletDataEntity getWalletData() {
                return walletData;
            }

            public void setWalletData(WalletDataEntity walletData) {
                this.walletData = walletData;
            }

            public ReferrerBranchDetailsEntity getReferrerBranchDetails() {
                return referrerBranchDetails;
            }

            public static class PreferencesEntity {
                private int pushTransPref;
                private int pushPromotionalPref;
                private int emailPref;
                private int smsPref;

                public static List<PreferencesEntity> arrayPreferencesEntityFromData(String str) {

                    Type listType = new TypeToken<ArrayList<PreferencesEntity>>() {
                    }.getType();

                    return new Gson().fromJson(str, listType);
                }

                public void setPushTransPref(int pushTransPref) {
                    this.pushTransPref = pushTransPref;
                }

                public void setPushPromotionalPref(int pushPromotionalPref) {
                    this.pushPromotionalPref = pushPromotionalPref;
                }

                public void setEmailPref(int emailPref) {
                    this.emailPref = emailPref;
                }

                public void setSmsPref(int smsPref) {
                    this.smsPref = smsPref;
                }

                public int getPushTransPref() {
                    return pushTransPref;
                }

                public int getPushPromotionalPref() {
                    return pushPromotionalPref;
                }

                public int getEmailPref() {
                    return emailPref;
                }

                public int getSmsPref() {
                    return smsPref;
                }
            }

            public static class WalletDataEntity {
                private int walletId;
                private double balanceAmount;
                private String email;


                public static List<WalletDataEntity> arrayPreferencesEntityFromData(String str) {

                    Type listType = new TypeToken<ArrayList<WalletDataEntity>>() {
                    }.getType();

                    return new Gson().fromJson(str, listType);
                }

                public int getWalletId() {
                    return walletId;
                }

                public void setWalletId(int walletId) {
                    this.walletId = walletId;
                }

                public double getBalanceAmount() {
                    return balanceAmount;
                }

                public void setBalanceAmount(double balanceAmount) {
                    this.balanceAmount = balanceAmount;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }
            }


            public static class ReferrerBranchDetailsEntity {
                private String current_branch_user_identity;
                private String current_branch_user_name;
                private String current_user_image_url;
                private String current_user_channel;

                public static List<ReferrerBranchDetailsEntity> arrayReferrerBranchDetailsEntityFromData(String str) {

                    Type listType = new TypeToken<ArrayList<ReferrerBranchDetailsEntity>>() {
                    }.getType();

                    return new Gson().fromJson(str, listType);
                }

                public void setCurrent_branch_user_identity(String current_branch_user_identity) {
                    this.current_branch_user_identity = current_branch_user_identity;
                }

                public void setCurrent_branch_user_name(String current_branch_user_name) {
                    this.current_branch_user_name = current_branch_user_name;
                }

                public void setCurrent_user_image_url(String current_user_image_url) {
                    this.current_user_image_url = current_user_image_url;
                }

                public void setCurrent_user_channel(String current_user_channel) {
                    this.current_user_channel = current_user_channel;
                }

                public String getCurrent_branch_user_identity() {
                    return current_branch_user_identity;
                }

                public String getCurrent_branch_user_name() {
                    return current_branch_user_name;
                }

                public String getCurrent_user_image_url() {
                    return current_user_image_url;
                }

                public String getCurrent_user_channel() {
                    return current_user_channel;
                }
            }
        }
    }
}
