package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author nitesh.verma on 3/29/16.
 */
public class MNPRequest {
    final List<String> phone;

    public MNPRequest(List<String> phone) {
        this.phone = phone;
    }
}
