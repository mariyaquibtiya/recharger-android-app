package com.getsmartapp.lib.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shalakha.gupta on 29/07/15.
 */
public class OrderHistoryBean {

    /**
     * body : {"count":6,"ordersList":[{"message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","payUTxId":"403993715512850090","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 2:40:48 PM","netAmountPaid":2,"totalAmount":2,"paymentStatus":"PMNT_SUCCESSFUL","orderItemStatusList":[{"rechargeStatus":"RECHARGE_OPERATOR_DOWN","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362089}],"orderStatus":"ORDER_PENDING","promoCode":"","orderId":9750362089},{"message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","payUTxId":"403993715512848064","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 12:16:09 PM","netAmountPaid":2,"totalAmount":2,"paymentStatus":"PMNT_SUCCESSFUL","orderItemStatusList":[{"rechargeStatus":"RECHARGE_OPERATOR_DOWN","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362078}],"orderStatus":"ORDER_PENDING","promoCode":"","orderId":9750362078},{"message":"Your order could not be processed because of payment failure.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 12:15:15 PM","netAmountPaid":2,"totalAmount":2,"paymentStatus":"PMNT_FAILED","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Your order could not be processed because of payment failure.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_FAILED","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362077}],"orderStatus":"ORDER_FAILED","promoCode":"","orderId":9750362077},{"message":"Your order could not be processed because of payment failure.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 10:30:10 AM","netAmountPaid":10,"totalAmount":10,"paymentStatus":"PMNT_FAILED","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9654118361","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Your order could not be processed because of payment failure.","amount":10,"categoryName":"Topup","orderItemStatus":"ORDER_FAILED","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362069}],"orderStatus":"ORDER_FAILED","promoCode":"","orderId":9750362069},{"message":"We are awaiting your payment status.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 28, 2015 6:22:36 PM","netAmountPaid":20,"totalAmount":20,"paymentStatus":"PMNT_INIT","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"28-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"We are awaiting your payment status.","amount":20,"categoryName":"TOPUP","orderItemStatus":"ORDER_INIT","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362061}],"orderStatus":"ORDER_INIT","promoCode":"","orderId":9750362061},{"message":"We are awaiting your payment status.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 28, 2015 6:22:13 PM","netAmountPaid":20,"totalAmount":20,"paymentStatus":"PMNT_INIT","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"28-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"We are awaiting your payment status.","amount":20,"categoryName":"TOPUP","orderItemStatus":"ORDER_INIT","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362060}],"orderStatus":"ORDER_INIT","promoCode":"","orderId":9750362060}]}
     * header : {"errors":{"errorList":[{"message":"No Plans Found.","errorCode":"102"}]},"status":"1"}
     */
    private BodyEntity body;
    private HeaderEntity header;

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public class BodyEntity {
        /**
         * count : 6
         * ordersList : [{"message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","payUTxId":"403993715512850090","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 2:40:48 PM","netAmountPaid":2,"totalAmount":2,"paymentStatus":"PMNT_SUCCESSFUL","orderItemStatusList":[{"rechargeStatus":"RECHARGE_OPERATOR_DOWN","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362089}],"orderStatus":"ORDER_PENDING","promoCode":"","orderId":9750362089},{"message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","payUTxId":"403993715512848064","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 12:16:09 PM","netAmountPaid":2,"totalAmount":2,"paymentStatus":"PMNT_SUCCESSFUL","orderItemStatusList":[{"rechargeStatus":"RECHARGE_OPERATOR_DOWN","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362078}],"orderStatus":"ORDER_PENDING","promoCode":"","orderId":9750362078},{"message":"Your order could not be processed because of payment failure.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 12:15:15 PM","netAmountPaid":2,"totalAmount":2,"paymentStatus":"PMNT_FAILED","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Your order could not be processed because of payment failure.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_FAILED","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362077}],"orderStatus":"ORDER_FAILED","promoCode":"","orderId":9750362077},{"message":"Your order could not be processed because of payment failure.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 29, 2015 10:30:10 AM","netAmountPaid":10,"totalAmount":10,"paymentStatus":"PMNT_FAILED","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9654118361","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Your order could not be processed because of payment failure.","amount":10,"categoryName":"Topup","orderItemStatus":"ORDER_FAILED","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362069}],"orderStatus":"ORDER_FAILED","promoCode":"","orderId":9750362069},{"message":"We are awaiting your payment status.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 28, 2015 6:22:36 PM","netAmountPaid":20,"totalAmount":20,"paymentStatus":"PMNT_INIT","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"28-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"We are awaiting your payment status.","amount":20,"categoryName":"TOPUP","orderItemStatus":"ORDER_INIT","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362061}],"orderStatus":"ORDER_INIT","promoCode":"","orderId":9750362061},{"message":"We are awaiting your payment status.","rechargeType":"single","promoCodeDiscount":0,"orderDate":"Jul 28, 2015 6:22:13 PM","netAmountPaid":20,"totalAmount":20,"paymentStatus":"PMNT_INIT","orderItemStatusList":[{"rechargeStatus":"RECHARGE_INIT","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"28-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"We are awaiting your payment status.","amount":20,"categoryName":"TOPUP","orderItemStatus":"ORDER_INIT","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362060}],"orderStatus":"ORDER_INIT","promoCode":"","orderId":9750362060}]
         */
        private int count;
        private List<OrdersListEntity> ordersList;

        public void setCount(int count) {
            this.count = count;
        }

        public void setOrdersList(List<OrdersListEntity> ordersList) {
            this.ordersList = ordersList;
        }

        public int getCount() {
            return count;
        }

        public List<OrdersListEntity> getOrdersList() {
            return ordersList;
        }

        public class OrdersListEntity {
            /**
             * message : Vodafone's services are currently down. We will update you the transaction status once their services are up and running.
             * payUTxId : 403993715512850090
             * rechargeType : single
             * promoCodeDiscount : 0.0
             * orderDate : Jul 29, 2015 2:40:48 PM
             * netAmountPaid : 2.0
             * totalAmount : 2.0
             * paymentStatus : PMNT_SUCCESSFUL
             * orderItemStatusList : [{"rechargeStatus":"RECHARGE_OPERATOR_DOWN","spName":"Vodafone","rechargeTo":"9811158771","orderDate":"29-Jul-15","categoryId":4,"seqId":0,"circleName":"Delhi NCR","message":"Vodafone's services are currently down. We will update you the transaction status once their services are up and running.","amount":2,"categoryName":"Topup","orderItemStatus":"ORDER_PENDING","sTypeId":1,"spId":2,"circleId":5,"orderId":9750362089}]
             * orderStatus : ORDER_PENDING
             * promoCode :
             * orderId : 9750362089
             */
            private String message;
            private String payUTxId;
            private String rechargeType;
            private double promoCodeDiscount;
            private String orderDate;
            private double netAmountPaid;
            private double totalAmount;
            private String paymentStatus;
            private ArrayList<OrderItemStatusListEntity> orderItemStatusList;
            private String orderStatus;
            private String promoCode;
            private long orderId;

            public void setMessage(String message) {
                this.message = message;
            }

            public void setPayUTxId(String payUTxId) {
                this.payUTxId = payUTxId;
            }

            public void setRechargeType(String rechargeType) {
                this.rechargeType = rechargeType;
            }

            public void setPromoCodeDiscount(double promoCodeDiscount) {
                this.promoCodeDiscount = promoCodeDiscount;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }

            public void setNetAmountPaid(double netAmountPaid) {
                this.netAmountPaid = netAmountPaid;
            }

            public void setTotalAmount(double totalAmount) {
                this.totalAmount = totalAmount;
            }

            public void setPaymentStatus(String paymentStatus) {
                this.paymentStatus = paymentStatus;
            }

            public void setOrderItemStatusList(ArrayList<OrderItemStatusListEntity> orderItemStatusList) {
                this.orderItemStatusList = orderItemStatusList;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public void setPromoCode(String promoCode) {
                this.promoCode = promoCode;
            }

            public void setOrderId(long orderId) {
                this.orderId = orderId;
            }

            public String getMessage() {
                return message;
            }

            public String getPayUTxId() {
                return payUTxId;
            }

            public String getRechargeType() {
                return rechargeType;
            }

            public double getPromoCodeDiscount() {
                return promoCodeDiscount;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public double getNetAmountPaid() {
                return netAmountPaid;
            }

            public double getTotalAmount() {
                return totalAmount;
            }

            public String getPaymentStatus() {
                return paymentStatus;
            }

            public ArrayList<OrderItemStatusListEntity> getOrderItemStatusList() {
                return orderItemStatusList;
            }

            public String getOrderStatus() {
                return orderStatus;
            }

            public String getPromoCode() {
                return promoCode;
            }

            public long getOrderId() {
                return orderId;
            }

            public class OrderItemStatusListEntity implements Serializable {
                private String rechargeStatus;
                private String spName;
                private String rechargeTo;
                private String orderDate;
                private int categoryId;
                private int seqId;
                private String circleName;
                private String message;
                private double amount;
                private String categoryName;
                private String orderItemStatus;
                private int sTypeId;
                private int spId;
                private int circleId;
                private long orderId;
                private int planId;
                private String planValidity;
                private String planDescription;

//                "planValidity": "14 Days",
//                        "planDescription": "150 LA2A mins & 150 (L+N) A2A SMS",

//                protected OrderItemStatusListEntity(Parcel in) {
//                    rechargeStatus = in.readString();
//                    spName = in.readString();
//                    rechargeTo = in.readString();
//                    orderDate = in.readString();
//                    categoryId = in.readInt();
//                    seqId = in.readInt();
//                    circleName = in.readString();
//                    message = in.readString();
//                    amount = in.readDouble();
//                    categoryName = in.readString();
//                    orderItemStatus = in.readString();
//                    sTypeId = in.readInt();
//                    spId = in.readInt();
//                    circleId = in.readInt();
//                    orderId = in.readLong();
//                }
//
//                @Override
//                public int describeContents() {
//                    return 0;
//                }
//                @Override
//                public void writeToParcel(Parcel dest, int flags) {
//                    dest.writeString(rechargeStatus);
//                    dest.writeString(spName);
//                    dest.writeString(rechargeTo);
//                    dest.writeString(orderDate);
//                    dest.writeInt(categoryId);
//                    dest.writeInt(seqId);
//                    dest.writeString(circleName);
//                    dest.writeString(message);
//                    dest.writeDouble(amount);
//                    dest.writeString(categoryName);
//                    dest.writeString(orderItemStatus);
//                    dest.writeInt(sTypeId);
//                    dest.writeInt(spId);
//                    dest.writeInt(circleId);
//                    dest.writeLong(orderId);
//                }

                public int getsTypeId() {
                    return sTypeId;
                }

                public void setsTypeId(int sTypeId) {
                    this.sTypeId = sTypeId;
                }

                public int getPlanId() {
                    return planId;
                }

                public void setPlanId(int planId) {
                    this.planId = planId;
                }

                public String getPlanValidity() {
                    return planValidity;
                }

                public void setPlanValidity(String planValidity) {
                    this.planValidity = planValidity;
                }

                public String getPlanDescription() {
                    return planDescription;
                }

                public void setPlanDescription(String planDescription) {
                    this.planDescription = planDescription;
                }

                /**
                 * rechargeStatus : RECHARGE_OPERATOR_DOWN
                 * spName : Vodafone
                 * rechargeTo : 9811158771
                 * orderDate : 29-Jul-15
                 * categoryId : 4
                 * seqId : 0
                 * circleName : Delhi NCR
                 * message : Vodafone's services are currently down. We will update you the transaction status once their services are up and running.
                 * amount : 2.0
                 * categoryName : Topup
                 * orderItemStatus : ORDER_PENDING
                 * sTypeId : 1
                 * spId : 2
                 * circleId : 5
                 * orderId : 9750362089
                 */


                public void setRechargeStatus(String rechargeStatus) {
                    this.rechargeStatus = rechargeStatus;
                }

                public void setSpName(String spName) {
                    this.spName = spName;
                }

                public void setRechargeTo(String rechargeTo) {
                    this.rechargeTo = rechargeTo;
                }

                public void setOrderDate(String orderDate) {
                    this.orderDate = orderDate;
                }

                public void setCategoryId(int categoryId) {
                    this.categoryId = categoryId;
                }

                public void setSeqId(int seqId) {
                    this.seqId = seqId;
                }

                public void setCircleName(String circleName) {
                    this.circleName = circleName;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public void setAmount(double amount) {
                    this.amount = amount;
                }

                public void setCategoryName(String categoryName) {
                    this.categoryName = categoryName;
                }

                public void setOrderItemStatus(String orderItemStatus) {
                    this.orderItemStatus = orderItemStatus;
                }

//                public void setSTypeId(int sTypeId) {
//                    this.sTypeId = sTypeId;
//                }

                public void setSpId(int spId) {
                    this.spId = spId;
                }

                public void setCircleId(int circleId) {
                    this.circleId = circleId;
                }

                public void setOrderId(long orderId) {
                    this.orderId = orderId;
                }

                public String getRechargeStatus() {
                    return rechargeStatus;
                }

                public String getSpName() {
                    return spName;
                }

                public String getRechargeTo() {
                    return rechargeTo;
                }

                public String getOrderDate() {
                    return orderDate;
                }

                public int getCategoryId() {
                    return categoryId;
                }

                public int getSeqId() {
                    return seqId;
                }

                public String getCircleName() {
                    return circleName;
                }

                public String getMessage() {
                    return message;
                }

                public double getAmount() {
                    return amount;
                }

                public String getCategoryName() {
                    return categoryName;
                }

                public String getOrderItemStatus() {
                    return orderItemStatus;
                }

//                public int getSTypeId() {
//                    return sTypeId;
//                }

                public int getSpId() {
                    return spId;
                }

                public int getCircleId() {
                    return circleId;
                }

                public long getOrderId() {
                    return orderId;
                }
            }
        }
    }

    public class HeaderEntity {
        /**
         * errors : {"errorList":[{"message":"No Plans Found.","errorCode":"102"}]}
         * status : 1
         */
        private ErrorsEntity errors;
        private String status;

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public String getStatus() {
            return status;
        }

        public class ErrorsEntity {
            /**
             * errorList : [{"message":"No Plans Found.","errorCode":"102"}]
             */
            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public class ErrorListEntity {
                /**
                 * message : No Plans Found.
                 * errorCode : 102
                 */
                private String message;
                private String errorCode;

                public void setMessage(String message) {
                    this.message = message;
                }

                public void setErrorCode(String errorCode) {
                    this.errorCode = errorCode;
                }

                public String getMessage() {
                    return message;
                }

                public String getErrorCode() {
                    return errorCode;
                }
            }
        }
    }
}
