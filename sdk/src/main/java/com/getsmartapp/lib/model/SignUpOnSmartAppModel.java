package com.getsmartapp.lib.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jayant.B on 7/14/2015.
 */
public class SignUpOnSmartAppModel {

    /**
     * status : 1
     */

    private HeaderEntity header;
    /**
     * referralCode : BISHWAJEET9Z
     * referralLink : http://smrtp.com/bishwajeet9z
     * wsHash : 17479c82b39d66a7602eb60d5b1c7dfd8df6b4bcb9856751774c32e114f5b1e38628b9f83b8e60b33aa2c074e1eb9acbb606b537b1662cdd3c0d61dcd1a4e5c2
     * deleteHash : 4a1c6cbcd7d2d7879020bfd03eda1cd378ac0c9af35332079444edd915362fb9d037705ef4cc9ef83107b2963022b3ad6f0d95c3ee3833686d4e360901d958c0
     * storedCardsHash : 65ebedbac6f383e87b43cde74f4c3acb67fe1caff0ab97582ca46221ffb0a15331826c92742b19358d7ba3c2d8c58e18f0001aca2c099a09ad5df8ebfa6ebff5
     * fetchCardsHash : 8dc9b21554e2ab7c0834645ac88cd8a3f51239944c55a637d333ceb9b1122f96aa4824f84ee3326f9071119cb56e49ff64a00a22855ce06e011663bfe4568440
     * vasCardsHash : df34254cee84493537bf72bb1bd772b4978157aa4f086911569a7510cd4212442d65221bf4b39fe1ed9e8041618373e39ced2b9dacc26679a54d529365f3b5dc
     * isActiveInviteeGC : 0
     * inviteeGC : SBDLKFA34SKJFKDSJ
     * hasDoneFirstTransaction : 1
     * isNewDevice : 0
     * isReferredDevice : 0
     * preferences : {"pushTransPref":1,"pushPromotionalPref":1,"emailPref":1,"smsPref":1}
     * referrerBranchDetails : {"current_branch_user_identity":"ijuddkzsm8nk90ziu80c78v4","current_branch_user_name":"Bishwajeet","current_user_image_url":"","current_user_channel":"ijuddkzsm8nk90ziu80c78v4"}
     */

    private BodyEntity body;

    public static List<SignUpOnSmartAppModel> arraySignUpOnSmartAppModelFromData(String str) {

        Type listType = new TypeToken<ArrayList<SignUpOnSmartAppModel>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private String status;
        private ErrorsEntity errors;

        public static List<HeaderEntity> arrayHeaderEntityFromData(String str) {

            Type listType = new TypeToken<ArrayList<HeaderEntity>>() {
            }.getType();
                return new Gson().fromJson(str, listType);
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public void setErrors(ErrorsEntity errors) {
                this.errors = errors;
            }

            public String getStatus() {
                return status;
            }

            public ErrorsEntity getErrors() {
                return errors;
            }

            public static class ErrorsEntity {
                /**
                 * errorCode : 106
                 * message : fName should only contain alphabets.
                 */

                private List<ErrorListEntity> errorList;

                public static List<ErrorsEntity> arrayErrorsEntityFromData(String str) {

                    Type listType = new TypeToken<ArrayList<ErrorsEntity>>() {
                    }.getType();

                    return new Gson().fromJson(str, listType);
                }

                public void setErrorList(List<ErrorListEntity> errorList) {
                    this.errorList = errorList;
                }

                public List<ErrorListEntity> getErrorList() {
                    return errorList;
                }

                public static class ErrorListEntity {
                    private int errorCode;
                    private String message;

                    public static List<ErrorListEntity> arrayErrorListEntityFromData(String str) {

                        Type listType = new TypeToken<ArrayList<ErrorListEntity>>() {
                        }.getType();

                        return new Gson().fromJson(str, listType);
                    }

                    public void setErrorCode(int errorCode) {
                        this.errorCode = errorCode;
                    }

                    public void setMessage(String message) {
                        this.message = message;
                    }

                    public int getErrorCode() {
                        return errorCode;
                    }

                    public String getMessage() {
                        return message;
                    }
                }
            }




    }

    public static class BodyEntity {
        private String referralCode;
        private String referralLink;
        private String wsHash;
        private String deleteHash;
        private String storedCardsHash;
        private String fetchCardsHash;
        private String vasCardsHash;
        private String isActiveInviteeGC;
        private String inviteeGC;
        private String hasDoneFirstTransaction;
        private String isNewDevice;
        private int isReferredDevice;
        /**
         * pushTransPref : 1
         * pushPromotionalPref : 1
         * emailPref : 1
         * smsPref : 1
         */

        private PreferencesEntity preferences;
        /**
         * current_branch_user_identity : ijuddkzsm8nk90ziu80c78v4
         * current_branch_user_name : Bishwajeet
         * current_user_image_url :
         * current_user_channel : ijuddkzsm8nk90ziu80c78v4
         */

        private ReferrerBranchDetailsEntity referrerBranchDetails;

        public static List<BodyEntity> arrayBodyEntityFromData(String str) {

            Type listType = new TypeToken<ArrayList<BodyEntity>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }

        public void setReferralLink(String referralLink) {
            this.referralLink = referralLink;
        }

        public void setWsHash(String wsHash) {
            this.wsHash = wsHash;
        }

        public void setDeleteHash(String deleteHash) {
            this.deleteHash = deleteHash;
        }

        public void setStoredCardsHash(String storedCardsHash) {
            this.storedCardsHash = storedCardsHash;
        }

        public void setFetchCardsHash(String fetchCardsHash) {
            this.fetchCardsHash = fetchCardsHash;
        }

        public void setVasCardsHash(String vasCardsHash) {
            this.vasCardsHash = vasCardsHash;
        }

        public void setIsActiveInviteeGC(String isActiveInviteeGC) {
            this.isActiveInviteeGC = isActiveInviteeGC;
        }

        public void setInviteeGC(String inviteeGC) {
            this.inviteeGC = inviteeGC;
        }

        public void setHasDoneFirstTransaction(String hasDoneFirstTransaction) {
            this.hasDoneFirstTransaction = hasDoneFirstTransaction;
        }

        public void setIsNewDevice(String isNewDevice) {
            this.isNewDevice = isNewDevice;
        }

        public void setIsReferredDevice(int isReferredDevice) {
            this.isReferredDevice = isReferredDevice;
        }

        public void setPreferences(PreferencesEntity preferences) {
            this.preferences = preferences;
        }

        public void setReferrerBranchDetails(ReferrerBranchDetailsEntity referrerBranchDetails) {
            this.referrerBranchDetails = referrerBranchDetails;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public String getReferralLink() {
            return referralLink;
        }

        public String getWsHash() {
            return wsHash;
        }

        public String getDeleteHash() {
            return deleteHash;
        }

        public String getStoredCardsHash() {
            return storedCardsHash;
        }

        public String getFetchCardsHash() {
            return fetchCardsHash;
        }

        public String getVasCardsHash() {
            return vasCardsHash;
        }

        public String getIsActiveInviteeGC() {
            return isActiveInviteeGC;
        }

        public String getInviteeGC() {
            return inviteeGC;
        }

        public String getHasDoneFirstTransaction() {
            return hasDoneFirstTransaction;
        }

        public String getIsNewDevice() {
            return isNewDevice;
        }

        public int getIsReferredDevice() {
            return isReferredDevice;
        }

        public PreferencesEntity getPreferences() {
            return preferences;
        }

        public ReferrerBranchDetailsEntity getReferrerBranchDetails() {
            return referrerBranchDetails;
        }

        public static class PreferencesEntity {
            private int pushTransPref;
            private int pushPromotionalPref;
            private int emailPref;
            private int smsPref;

            public static List<PreferencesEntity> arrayPreferencesEntityFromData(String str) {

                Type listType = new TypeToken<ArrayList<PreferencesEntity>>() {
                }.getType();

                return new Gson().fromJson(str, listType);
            }

            public void setPushTransPref(int pushTransPref) {
                this.pushTransPref = pushTransPref;
            }

            public void setPushPromotionalPref(int pushPromotionalPref) {
                this.pushPromotionalPref = pushPromotionalPref;
            }

            public void setEmailPref(int emailPref) {
                this.emailPref = emailPref;
            }

            public void setSmsPref(int smsPref) {
                this.smsPref = smsPref;
            }

            public int getPushTransPref() {
                return pushTransPref;
            }

            public int getPushPromotionalPref() {
                return pushPromotionalPref;
            }

            public int getEmailPref() {
                return emailPref;
            }

            public int getSmsPref() {
                return smsPref;
            }
        }

        public static class ReferrerBranchDetailsEntity {
            private String current_branch_user_identity;
            private String current_branch_user_name;
            private String current_user_image_url;
            private String current_user_channel;

            public static List<ReferrerBranchDetailsEntity> arrayReferrerBranchDetailsEntityFromData(String str) {

                Type listType = new TypeToken<ArrayList<ReferrerBranchDetailsEntity>>() {
                }.getType();

                return new Gson().fromJson(str, listType);
            }

            public void setCurrent_branch_user_identity(String current_branch_user_identity) {
                this.current_branch_user_identity = current_branch_user_identity;
            }

            public void setCurrent_branch_user_name(String current_branch_user_name) {
                this.current_branch_user_name = current_branch_user_name;
            }

            public void setCurrent_user_image_url(String current_user_image_url) {
                this.current_user_image_url = current_user_image_url;
            }

            public void setCurrent_user_channel(String current_user_channel) {
                this.current_user_channel = current_user_channel;
            }

            public String getCurrent_branch_user_identity() {
                return current_branch_user_identity;
            }

            public String getCurrent_branch_user_name() {
                return current_branch_user_name;
            }

            public String getCurrent_user_image_url() {
                return current_user_image_url;
            }

            public String getCurrent_user_channel() {
                return current_user_channel;
            }
        }
    }
}
