package com.getsmartapp.lib.model;

/**
 * @author nitesh.verma on 10/20/15.
 */
public class BackendUserParser {

    /**
     * header : {"status":"1"}
     * body : {"referralCode":"NITESH","referralLink":"http://smrtp.co/NITESH","isActiveInviteeGC":"0","hasDoneFirstTransaction":"1","isNewDevice":"0","preferences":{"pushTransPref":1,"pushPromotionalPref":0,"emailPref":0,"smsPref":1}}
     */

    private HeaderEntity header;
    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        /**
         * status : 1
         */

        private String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    public static class BodyEntity {
        /**
         * referralCode : NITESH
         * referralLink : http://smrtp.co/NITESH
         * isActiveInviteeGC : 0
         * hasDoneFirstTransaction : 1
         * isNewDevice : 0
         * preferences : {"pushTransPref":1,"pushPromotionalPref":0,"emailPref":0,"smsPref":1}
         */

        private String referralCode;
        private String referralLink;
        private String isActiveInviteeGC;
        private String hasDoneFirstTransaction;
        private String isNewDevice;
        private PreferencesEntity preferences;

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }

        public void setReferralLink(String referralLink) {
            this.referralLink = referralLink;
        }

        public void setIsActiveInviteeGC(String isActiveInviteeGC) {
            this.isActiveInviteeGC = isActiveInviteeGC;
        }

        public void setHasDoneFirstTransaction(String hasDoneFirstTransaction) {
            this.hasDoneFirstTransaction = hasDoneFirstTransaction;
        }

        public void setIsNewDevice(String isNewDevice) {
            this.isNewDevice = isNewDevice;
        }

        public void setPreferences(PreferencesEntity preferences) {
            this.preferences = preferences;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public String getReferralLink() {
            return referralLink;
        }

        public String getIsActiveInviteeGC() {
            return isActiveInviteeGC;
        }

        public String getHasDoneFirstTransaction() {
            return hasDoneFirstTransaction;
        }

        public String getIsNewDevice() {
            return isNewDevice;
        }

        public PreferencesEntity getPreferences() {
            return preferences;
        }

        public static class PreferencesEntity {
            /**
             * pushTransPref : 1
             * pushPromotionalPref : 0
             * emailPref : 0
             * smsPref : 1
             */

            private int pushTransPref;
            private int pushPromotionalPref;
            private int emailPref;
            private int smsPref;

            public void setPushTransPref(int pushTransPref) {
                this.pushTransPref = pushTransPref;
            }

            public void setPushPromotionalPref(int pushPromotionalPref) {
                this.pushPromotionalPref = pushPromotionalPref;
            }

            public void setEmailPref(int emailPref) {
                this.emailPref = emailPref;
            }

            public void setSmsPref(int smsPref) {
                this.smsPref = smsPref;
            }

            public int getPushTransPref() {
                return pushTransPref;
            }

            public int getPushPromotionalPref() {
                return pushPromotionalPref;
            }

            public int getEmailPref() {
                return emailPref;
            }

            public int getSmsPref() {
                return smsPref;
            }
        }
    }
}
