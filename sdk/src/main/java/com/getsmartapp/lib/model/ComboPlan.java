package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author Jayant.B on 7/29/2015.
 */
public class ComboPlan {

    /**
     * header : {"status":"1"}
         * body : {"comboSets":[{"comboName":"Combo Pack 1","hasData":1,"has2g":1,"has3g":1,"has4g":0,"subText":"Plan tailored for your usage","spId":1,"minValidity":0,"plans":[{"serviceType":"PREPAID_MOBILE","amount":500,"serviceProviderId":1,"rechargeValue":500,"description":"Talktime of Rs. 500","serviceProvider":"Airtel","planId":1571,"circleId":5,"validity":"Lifetime","circle":"Delhi NCR","category":"Topup","isExpired":false,"categoryId":4},{"serviceType":"PREPAID_MOBILE","amount":199,"serviceProviderId":1,"rechargeValue":0,"description":"1 GB Data","serviceProvider":"Airtel","planId":1663,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"2G","isExpired":false,"categoryId":1},{"serviceType":"PREPAID_MOBILE","amount":1399,"serviceProviderId":1,"rechargeValue":0,"description":"7 GB 3G Data","serviceProvider":"Airtel","planId":1687,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"3G","isExpired":false,"categoryId":2}],"circleId":5,"hasSms":0,"maxValidity":28,"comboSubsets":[{"planIds":[1687,1663,1571],"equivalentUsage":{"call":0,"std":0,"data":8,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":2098,"totalCost":2594.5399890244007}},{"planIds":[1663,1571],"equivalentUsage":{"call":0,"std":0,"data":1,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":699,"totalCost":5444.705003423159}},{"planIds":[1687,1571],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1899,"totalCost":5400.920912714315}},{"planIds":[1571],"equivalentUsage":{"call":0,"std":0,"data":0,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":7751.085927113074,"comboCost":500,"totalCost":8251.085927113074}},{"planIds":[1687,1663],"equivalentUsage":{"call":0,"std":0,"data":8,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":1598,"totalCost":2094.5399890244007}},{"planIds":[1663],"equivalentUsage":{"call":0,"std":0,"data":1,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":199,"totalCost":4944.705003423159}},{"planIds":[1687],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1399,"totalCost":4900.920912714315}}]},{"comboName":"Combo Pack 2","hasData":1,"has2g":1,"has3g":1,"has4g":0,"subText":"Plan tailored for your usage","spId":1,"minValidity":0,"plans":[{"serviceType":"PREPAID_MOBILE","amount":500,"serviceProviderId":1,"rechargeValue":500,"description":"Talktime of Rs. 500","serviceProvider":"Airtel","planId":1571,"circleId":5,"validity":"Lifetime","circle":"Delhi NCR","category":"Topup","isExpired":false,"categoryId":4},{"serviceType":"PREPAID_MOBILE","amount":251,"serviceProviderId":1,"rechargeValue":0,"description":"1.5 GB Data","serviceProvider":"Airtel","planId":1664,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"2G","isExpired":false,"categoryId":1},{"serviceType":"PREPAID_MOBILE","amount":1399,"serviceProviderId":1,"rechargeValue":0,"description":"7 GB 3G Data","serviceProvider":"Airtel","planId":1687,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"3G","isExpired":false,"categoryId":2}],"circleId":5,"hasSms":0,"maxValidity":28,"comboSubsets":[{"planIds":[1687,1664,1571],"equivalentUsage":{"call":0,"std":0,"data":8.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":2150,"totalCost":2646.5399890244007}},{"planIds":[1664,1571],"equivalentUsage":{"call":0,"std":0,"data":1.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":751,"totalCost":5496.705003423159}},{"planIds":[1687,1571],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1899,"totalCost":5400.920912714315}},{"planIds":[1571],"equivalentUsage":{"call":0,"std":0,"data":0,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":7751.085927113074,"comboCost":500,"totalCost":8251.085927113074}},{"planIds":[1687,1664],"equivalentUsage":{"call":0,"std":0,"data":8.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":1650,"totalCost":2146.5399890244007}},{"planIds":[1664],"equivalentUsage":{"call":0,"std":0,"data":1.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":251,"totalCost":4996.705003423159}},{"planIds":[1687],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1399,"totalCost":4900.920912714315}}]}],"usage":{"call":557,"std":442,"data":1.2996953632682562,"dominatUsageComponentType":"std","sms":4,"nightCall":40,"inNetworkTotalUsage":116,"dominatUsageComponentVal":442,"local":115}}
     */
    private HeaderEntity header;
    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        /**
         * status : 0
         * errors : {"errorList":[{"errorCode":801,"message":"Your usage is too small to build a combo. You could try a Topup recharge instead."}]}
         */

        private String status;
        private ErrorsEntity errors;

        public void setStatus(String status) {
            this.status = status;
        }

        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorList : [{"errorCode":801,"message":"Your usage is too small to build a combo. You could try a Topup recharge instead."}]
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                /**
                 * errorCode : 801
                 * message : Your usage is too small to build a combo. You could try a Topup recharge instead.
                 */

                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }
    }

    public class BodyEntity {
        /**
         * comboSets : [{"comboName":"Combo Pack 1","hasData":1,"has2g":1,"has3g":1,"has4g":0,"subText":"Plan tailored for your usage","spId":1,"minValidity":0,"plans":[{"serviceType":"PREPAID_MOBILE","amount":500,"serviceProviderId":1,"rechargeValue":500,"description":"Talktime of Rs. 500","serviceProvider":"Airtel","planId":1571,"circleId":5,"validity":"Lifetime","circle":"Delhi NCR","category":"Topup","isExpired":false,"categoryId":4},{"serviceType":"PREPAID_MOBILE","amount":199,"serviceProviderId":1,"rechargeValue":0,"description":"1 GB Data","serviceProvider":"Airtel","planId":1663,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"2G","isExpired":false,"categoryId":1},{"serviceType":"PREPAID_MOBILE","amount":1399,"serviceProviderId":1,"rechargeValue":0,"description":"7 GB 3G Data","serviceProvider":"Airtel","planId":1687,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"3G","isExpired":false,"categoryId":2}],"circleId":5,"hasSms":0,"maxValidity":28,"comboSubsets":[{"planIds":[1687,1663,1571],"equivalentUsage":{"call":0,"std":0,"data":8,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":2098,"totalCost":2594.5399890244007}},{"planIds":[1663,1571],"equivalentUsage":{"call":0,"std":0,"data":1,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":699,"totalCost":5444.705003423159}},{"planIds":[1687,1571],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1899,"totalCost":5400.920912714315}},{"planIds":[1571],"equivalentUsage":{"call":0,"std":0,"data":0,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":7751.085927113074,"comboCost":500,"totalCost":8251.085927113074}},{"planIds":[1687,1663],"equivalentUsage":{"call":0,"std":0,"data":8,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":1598,"totalCost":2094.5399890244007}},{"planIds":[1663],"equivalentUsage":{"call":0,"std":0,"data":1,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":199,"totalCost":4944.705003423159}},{"planIds":[1687],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1399,"totalCost":4900.920912714315}}]},{"comboName":"Combo Pack 2","hasData":1,"has2g":1,"has3g":1,"has4g":0,"subText":"Plan tailored for your usage","spId":1,"minValidity":0,"plans":[{"serviceType":"PREPAID_MOBILE","amount":500,"serviceProviderId":1,"rechargeValue":500,"description":"Talktime of Rs. 500","serviceProvider":"Airtel","planId":1571,"circleId":5,"validity":"Lifetime","circle":"Delhi NCR","category":"Topup","isExpired":false,"categoryId":4},{"serviceType":"PREPAID_MOBILE","amount":251,"serviceProviderId":1,"rechargeValue":0,"description":"1.5 GB Data","serviceProvider":"Airtel","planId":1664,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"2G","isExpired":false,"categoryId":1},{"serviceType":"PREPAID_MOBILE","amount":1399,"serviceProviderId":1,"rechargeValue":0,"description":"7 GB 3G Data","serviceProvider":"Airtel","planId":1687,"circleId":5,"validity":"28 Days","circle":"Delhi NCR","category":"3G","isExpired":false,"categoryId":2}],"circleId":5,"hasSms":0,"maxValidity":28,"comboSubsets":[{"planIds":[1687,1664,1571],"equivalentUsage":{"call":0,"std":0,"data":8.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":2150,"totalCost":2646.5399890244007}},{"planIds":[1664,1571],"equivalentUsage":{"call":0,"std":0,"data":1.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":751,"totalCost":5496.705003423159}},{"planIds":[1687,1571],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1899,"totalCost":5400.920912714315}},{"planIds":[1571],"equivalentUsage":{"call":0,"std":0,"data":0,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":7751.085927113074,"comboCost":500,"totalCost":8251.085927113074}},{"planIds":[1687,1664],"equivalentUsage":{"call":0,"std":0,"data":8.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":496.5399890244007,"comboCost":1650,"totalCost":2146.5399890244007}},{"planIds":[1664],"equivalentUsage":{"call":0,"std":0,"data":1.5,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":4745.705003423159,"comboCost":251,"totalCost":4996.705003423159}},{"planIds":[1687],"equivalentUsage":{"call":0,"std":0,"data":7,"dominatUsageComponentType":"inNetworkTotalUsage","sms":0,"nightCall":0,"inNetworkTotalUsage":0,"dominatUsageComponentVal":0,"local":0},"comboCost":{"usageCost":3501.9209127143154,"comboCost":1399,"totalCost":4900.920912714315}}]}]
         * usage : {"call":557,"std":442,"data":1.2996953632682562,"dominatUsageComponentType":"std","sms":4,"nightCall":40,"inNetworkTotalUsage":116,"dominatUsageComponentVal":442,"local":115}
         */
        private List<HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity> comboSets;
        private HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.UsageEntity usage;

        public void setComboSets(List<HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity> comboSets) {
            this.comboSets = comboSets;
        }

        public void setUsage(HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.UsageEntity usage) {
            this.usage = usage;
        }

        public List<HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.ComboSetsEntity> getComboSets() {
            return comboSets;
        }

        public HomeAllInstreamModel.BodyEntity.ComboRecommendationEntity.UsageEntity getUsage() {
            return usage;
        }





    }
}
