package com.getsmartapp.lib.model;

/**
 * @author shalakha.gupta on 07/08/15.
 */
public class HomeCardsModel {
    /**
     * cardName : REFFERAL
     * score : 100
     */
    private String cardName;
    private int score;
    private double eCoeff;
    private int isTapped;

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCardName() {
        return cardName;
    }

    public int getScore() {
        return score;
    }

    public double geteCoeff() {
        return eCoeff;
    }

    public void seteCoeff(double eCoeff) {
        this.eCoeff = eCoeff;
    }

    public int getIsTapped() {
        return isTapped;
    }

    public void setIsTapped(int isTapped) {
        this.isTapped = isTapped;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof HomeCardsModel)) {
            return false;
        }
        if (o == null) {
            return false;
        }
        HomeCardsModel model = (HomeCardsModel) o;
        if (model.getCardName().equalsIgnoreCase(this.getCardName())) {
            return true;
        } else {
            return false;
        }
    }
}
