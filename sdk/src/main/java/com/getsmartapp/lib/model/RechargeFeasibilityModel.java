package com.getsmartapp.lib.model;

import java.util.List;

/**
 * @author shalakha.gupta on 09/06/16.
 */
public class RechargeFeasibilityModel {
    private HeaderEntity header;

    private BodyEntity body;

    public void setHeader(HeaderEntity header) {
        this.header = header;
    }

    public void setBody(BodyEntity body) {
        this.body = body;
    }

    public HeaderEntity getHeader() {
        return header;
    }

    public BodyEntity getBody() {
        return body;
    }

    public static class HeaderEntity {
        private int status;
        private ErrorsEntity errors;
        public void setStatus(int status) {
            this.status = status;
        }
        public void setErrors(ErrorsEntity errors) {
            this.errors = errors;
        }

        public int getStatus() {
            return status;
        }

        public ErrorsEntity getErrors() {
            return errors;
        }

        public static class ErrorsEntity {
            /**
             * errorCode : 602
             * message : Error in walletBalanceRequest API request
             */

            private List<ErrorListEntity> errorList;

            public void setErrorList(List<ErrorListEntity> errorList) {
                this.errorList = errorList;
            }

            public List<ErrorListEntity> getErrorList() {
                return errorList;
            }

            public static class ErrorListEntity {
                private int errorCode;
                private String message;

                public void setErrorCode(int errorCode) {
                    this.errorCode = errorCode;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public int getErrorCode() {
                    return errorCode;
                }

                public String getMessage() {
                    return message;
                }
            }
        }

    }

    public static class BodyEntity {
        boolean isFeasible;
        String delayAdvice;
        public boolean isFeasible() {
            return isFeasible;
        }

        public void setFeasible(boolean feasible) {
            isFeasible = feasible;
        }

        public String getDelayAdvice() {
            return delayAdvice;
        }

        public void setDelayAdvice(String delayAdvice) {
            this.delayAdvice = delayAdvice;
        }
    }

}
