package com.getsmartapp.lib.data;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;

import com.getsmartapp.lib.services.SmsStoringService;

/**
 * @author  nitesh on 27/5/15.
 */
public class SmsDataObserver extends ContentObserver {

    private Context mContext;

    public SmsDataObserver(Handler handler, Context context) {
        super(handler);
        mContext = context;
    }


    @Override
    public boolean deliverSelfNotifications() {
        return true;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Intent service = new Intent(mContext, SmsStoringService.class);
        mContext.startService(service);
    }
}

