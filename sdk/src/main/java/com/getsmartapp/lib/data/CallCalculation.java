package com.getsmartapp.lib.data;

import android.content.Context;
import android.database.Cursor;

import com.getsmartapp.lib.database.SdkAppDatabaseHelper;
import com.getsmartapp.lib.constants.DataStorageConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * @author  nitesh on 25/5/15.
 */
public class CallCalculation {
    private Context mContext;
    private String simType, provider, phone_number, circle;
//    private AppDatabaseHelper appDatabaseHelper;
    private SdkAppDatabaseHelper appDatabaseHelper;
    private String night_start_time = "23:00:00";
    private String night_end_time = "06:00:00";
    private ArrayList<String> country_code = new ArrayList<String>();
    private ArrayList<String> country_name = new ArrayList<String>();
    private ArrayList<String> landline_std_codes = new ArrayList<>();
    private ArrayList<String> landline_circle_name = new ArrayList<>();

    public CallCalculation(Context context, String sim_type, String service_provider, String phone_circle) {
        mContext = context;
//        appDatabaseHelper = new AppDatabaseHelper(mContext);
        appDatabaseHelper = new SdkAppDatabaseHelper(mContext);
        simType = sim_type;
        provider = service_provider;
        circle = phone_circle;
    }

    public void setNightStartEndTime() {
        Cursor nightTimeCursor = null;
        try {
            nightTimeCursor = appDatabaseHelper.getNightTimeCursor(simType, provider);
            if (nightTimeCursor.getCount() > 0) {
                night_start_time = nightTimeCursor.getString(nightTimeCursor.getColumnIndex(DataStorageConstants.COLUMN_START_TIME));
                night_end_time = nightTimeCursor.getString(nightTimeCursor.getColumnIndex(DataStorageConstants.COLUMN_END_TIME));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (nightTimeCursor != null) {
                nightTimeCursor.close();
            }
        }
    }

    public void fillLandlineList() {
        Cursor landlineSeriesCursor = null;
        try {
            landlineSeriesCursor = appDatabaseHelper.getLandlineCursor();
            if (landlineSeriesCursor != null && landlineSeriesCursor.moveToFirst()) {
                do {
                    String circle_name = landlineSeriesCursor.getString(landlineSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_CIRCLE_NAME));
                    String std_code = landlineSeriesCursor.getString(landlineSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_STD_CODE));
                    landline_std_codes.add(std_code);
                    landline_circle_name.add(circle_name);
                } while (landlineSeriesCursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (landlineSeriesCursor != null) {
                landlineSeriesCursor.close();
            }
        }
    }

    public void fillCountryList() {
        Cursor isdSeriesCursor = null;
        try {
            isdSeriesCursor = appDatabaseHelper.getIsdNumberCursor();
            if (isdSeriesCursor != null && isdSeriesCursor.moveToFirst()) {
                do {
                    country_code.add(isdSeriesCursor.getString(isdSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_ISD_CODE)));
                    country_name.add(isdSeriesCursor.getString(isdSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_COUNTRY)));
                } while (isdSeriesCursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (isdSeriesCursor != null) {
                isdSeriesCursor.close();
            }
        }
    }

    public void setMobile(String number) {
        Cursor mobileSeriesCursor = null;
        try {
            phone_number = number;
            String formatted_four_digit = formatefiveDigit(phone_number);
            if (formatted_four_digit != null) {
                if (Character.isDigit(formatted_four_digit.charAt(0)) && Character.isDigit(formatted_four_digit.charAt(formatted_four_digit.length() - 1))) {
                    mobileSeriesCursor = getCursorFromNumberSeries(formatted_four_digit, appDatabaseHelper);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (mobileSeriesCursor != null) {
                mobileSeriesCursor.close();
            }
        }
    }

    private String circleFromlandline(String phone_number) {
        String formated_number = formatefiveDigit(phone_number);
        if (formated_number != null) {
            for (int i = 0; i < landline_std_codes.size(); i++) {
                if (formated_number.startsWith(landline_std_codes.get(i))) {
                    return landline_circle_name.get(i);
                }
            }
        }
        return null;
    }

    public String getCallType(int call_int) {
        String ct = DataStorageConstants.MISSED_CALL_TYPE;
        switch (call_int) {
            case 1:
                ct = DataStorageConstants.INCOMING_CALL_TYPE;
                break;
            case 2:
                ct = DataStorageConstants.OUTGOING_CALL_TYPE;
                break;
        }
        return ct;
    }

    public String getSmsType(int sms_int) {
        String mt = DataStorageConstants.OTHER_SMS_TYPE;
        switch (sms_int) {
            case 1:
                mt = DataStorageConstants.INBOX_SMS_TYPE;
                break;
            case 2:
                mt = DataStorageConstants.SENT_SMS_TYPE;
                break;
        }
        return mt;
    }

    public String getCallNetwork(String phone_number) {
        Cursor mobileSeriesCursor = null;
        try {
            String formatted_four_digit = formatefiveDigit(phone_number);
            mobileSeriesCursor = getCursorFromNumberSeries(formatted_four_digit, appDatabaseHelper);
            if (mobileSeriesCursor != null) {
                if (mobileSeriesCursor.getCount() == 0) {
                    return DataStorageConstants.LANDLINE_CALLNETWORK;
                } else if (mobileSeriesCursor.moveToFirst()) {
                    String call_Number_operator = mobileSeriesCursor.getString(mobileSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_SERVICE_PROVIDER));

                    String own_Operator = provider;
                    if (call_Number_operator.equals(own_Operator)) {
                        return DataStorageConstants.SAME_CALLNETWORK;
                    } else
                        return DataStorageConstants.OTHER_CALLNETWORK;
                } else
                    return DataStorageConstants.OTHER_CALLNETWORK;
            } else
                return DataStorageConstants.OTHER_CALLNETWORK;
        } catch (Exception ex) {
            ex.printStackTrace();
            return DataStorageConstants.OTHER_CALLNETWORK;
        } finally {
            if (mobileSeriesCursor != null) {
                mobileSeriesCursor.close();
            }
            return DataStorageConstants.OTHER_CALLNETWORK;
        }
    }

    public int getIsNightValue(long start_time) {
        Date date = new Date(start_time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String dateo = simpleDateFormat.format(date);
        if (dateo.compareTo(night_start_time) > 0 || dateo.compareTo(night_end_time) < 0) {
            return 1;
        }
        return 0;
    }


    public String typeofCallandSms(String phone) {
        String callType = "local";
        String callCircle = null;
        Cursor mobileSeriesCursor = null;
        if (isIndianPhone(phone)) {
            try {
                String formatted_four_digit = formatefiveDigit(phone_number);
                mobileSeriesCursor = getCursorFromNumberSeries(formatted_four_digit, appDatabaseHelper);
                if (mobileSeriesCursor != null && mobileSeriesCursor.moveToFirst()) {
                    callCircle = mobileSeriesCursor.getString(mobileSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_CIRCLE_NAME));
                    callType = callCircle.equals(circle) ? DataStorageConstants.LOCAL_NUMBER : DataStorageConstants.STD_NUMBER;
                } else {
                    // Log.e("landline","detected");
                    callCircle = circleFromlandline(phone);
                    if (callCircle != null)
                        callType = callCircle.equals(circle) ? DataStorageConstants.LOCAL_NUMBER : DataStorageConstants.STD_NUMBER;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (mobileSeriesCursor != null) {
                    mobileSeriesCursor.close();
                }
            }
        } else if (phone.substring(0, 4).equals("1800")) {
            callType = DataStorageConstants.TOLLFREE_NUMBER;
        } else {
            callType = DataStorageConstants.ISD_NUMBER;
        }
        return callType;
    }

    public String numberCountry(String formatted_number) {
        //formatted_number = formatefiveDigit(formatted_number);
        String country = DataStorageConstants.OTHER_COUNTRY;
        if (!isIndianPhone(formatted_number)) {
            String fivedig = formatefiveDigit(formatted_number);
            for (int i = 0; i < country_code.size(); i++) {
                if (fivedig != null) {
                    if (fivedig.startsWith(country_code.get(i))) {
                        country = country_name.get(i);
                        break;
                    }
                }
            }

        }
        return country;
    }

    public boolean isIndianPhone(String phone_number) {
        boolean isInd = true;
        String first_digit = phone_number.substring(0, 1);
        if (first_digit.equals("+")) {
            String nexttwo_digit = phone_number.substring(1, 3);
            if (nexttwo_digit.equals("91"))
                isInd = true;
            else
                isInd = false;
        } else if (first_digit.equals("0")) {
            return true;
        }
        return isInd;
    }

    public int getMinCallDuration(int call_duration_in_sec) {
        double duration_in_min = call_duration_in_sec / 60.0f;
        return (int) Math.ceil(duration_in_min);

    }

    private String formatefiveDigit(String phone_number) {
        final String phon = phone_number;
        String formatted_five_digit = null;
        if (phon.length() > 7) {
            final String first_threeletter = phon.substring(0, 3);
            final String first_letter = phon.substring(0, 1);
            switch (first_letter) {
                case "+":
                    if (first_threeletter.equals("+91")) {
                        formatted_five_digit = phon.substring(3, 8);
                    } else {
                        formatted_five_digit = phon.substring(1, 6);
                    }
                    break;
                case "0":
                    formatted_five_digit = phon.substring(1, 6);
                    break;
                default:
                    formatted_five_digit = phon.substring(0, 5);
            }
        }

        return formatted_five_digit;
    }

    /*private Cursor getCursorFromNumberSeries(String formatted_four_digit, AppDatabaseHelper appDatabaseHelper) {
        formatted_four_digit = formatted_four_digit.replace(" ", "");
        return appDatabaseHelper.getCircleAndOperator(formatted_four_digit);
    }*/

    private Cursor getCursorFromNumberSeries(String formatted_four_digit, SdkAppDatabaseHelper appDatabaseHelper) {
        formatted_four_digit = formatted_four_digit.replace(" ", "");
        return appDatabaseHelper.getCircleAndOperator(formatted_four_digit);
    }
  /*  private Cursor getCursorFromLandlineSeries(AppDatabaseHelper appDatabaseHelper) {
        return appDatabaseHelper.getLandlineCursor();
    }*/

    private Cursor getCursorFromLandlineSeries(SdkAppDatabaseHelper appDatabaseHelper) {
        return appDatabaseHelper.getLandlineCursor();
    }

    public int getCircleId(String admin_area, String sub_admin_area, String locality, String sub_locality) {

        String[][] state_circle = new String[24][];
        state_circle[1] = new String[]{"Andhra", "Telangana"};
        state_circle[2] = new String[]{"Assam"};
        state_circle[3] = new String[]{"Bihar", "Jharkhand"};
        state_circle[5] = new String[]{"Delhi", "National Capital", "NCR"};
        state_circle[6] = new String[]{"Gujarat", "Dadra", "Daman", "Diu", "Nagar Haveli"};
        state_circle[7] = new String[]{"Himachal"};
        state_circle[8] = new String[]{"Jammu", "Kashmir"};
        state_circle[9] = new String[]{"Kerala", "Lakshadweep"};
        state_circle[10] = new String[]{"Karnataka"};
        state_circle[11] = new String[]{"Goa"};
        state_circle[12] = new String[]{"Madhya Pradesh", "M.P.", "Chattisgarh"};
        state_circle[13] = new String[]{"Goa"};
        state_circle[14] = new String[]{"Madhya Pradesh", "M.P.", "Chattisgarh"};
        state_circle[16] = new String[]{"Arunachal", "Meghalaya", "Mizoram", "Nagaland", "Manipur", "Tripura"};
        state_circle[17] = new String[]{"Orissa", "Odisha"};
        state_circle[18] = new String[]{"Punjab", "Chandigarh"};
        state_circle[19] = new String[]{"Rajasthan"};
        state_circle[20] = new String[]{"Pondicherry"};
        state_circle[22] = new String[]{"Uttarakhand", "Uttaranchal"};
        state_circle[23] = new String[]{"Sikkim", "Andaman", "Nicobar"};
        for (int i = 0; i < state_circle.length; i++) {
            if (state_circle[i] != null && state_circle[i].length != 0) {
                for (String state : state_circle[i]) {
                    if (state.matches("(?i).*\\b" + admin_area + "\\b.*")) {
                        return i;
                    } else {
                        if (state.matches("(?i).*\\b" + sub_admin_area + "\\b.*")) {
                            return i;
                        }


                    }
                }
            }
        }


        String[][] city_circle = new String[24][];
        city_circle[4] = new String[]{"Channai", "Madras"};
        city_circle[5] = new String[]{"Noida", "Ghaziabad", "Faridabad", "Gurgaon", "Ballabgarh"};
        city_circle[6] = new String[]{"Silvassa"};
        city_circle[9] = new String[]{"Ladakh"};
        city_circle[10] = new String[]{"Minicoy"};
        city_circle[12] = new String[]{"Kolkata", "Calcutta"};
        city_circle[15] = new String[]{"Mumbai", "Bombay", "Thane", "Kalyan", "Vashi", "Panvel"};
        city_circle[18] = new String[]{"Panchkula"};
        city_circle[22] = new String[]{"Agra", "Aligarh", "Amroha", "Badaun", "Baghpat", "Bareilly", "Bijnor", "Bulandshahr", "Etah", "Firozabad", "Hapur", "Hathras", "Mahamaya", "Mainpuri", "Mathura", "Meerut", "Moradabad", "Muzaffarnagar", "Pilibhit", "Rampur", "Saharanpur", "Sambhal", "Bhimnagar", "Shahjahanpur", "Shamli"};
        for (int j = 0; j < city_circle.length; j++) {
            if (city_circle[j] != null && city_circle[j].length != 0) {
                for (String city : city_circle[j]) {
                    if (city.matches("(?i).*\\b" + admin_area + "\\b.*")) {
                        return j;
                    } else if (city.matches("(?i).*\\b" + sub_admin_area + "\\b.*")) {
                        return j;
                    } else if (city.matches("(?i).*\\b" + locality + "\\b.*")) {
                        return j;
                    } else if (city.matches("(?i).*\\b" + sub_locality + "\\b.*")) {
                        return j;
                    }
                }
            }
        }

        String[][] rem_circle = new String[24][];
        rem_circle[8] = new String[]{"Haryana"};
        rem_circle[13] = new String[]{"Maharastra"};
        rem_circle[20] = new String[]{"Tamil"};
        rem_circle[21] = new String[]{"Uttar", "U.P", "Purvanchal"};
        rem_circle[23] = new String[]{"Bengal"};
        for (int k = 0; k < rem_circle.length; k++) {
            if (rem_circle[k] != null && rem_circle[k].length != 0) {
                for (String loc : rem_circle[k]) {
                    if (loc.matches("(?i).*\\b" + admin_area + "\\b.*"))
                        return k;
                    if (loc.matches("(?i).*\\b" + sub_admin_area + "\\b.*"))
                        return k;
                }
            }
        }
        return 0;
    }


}


