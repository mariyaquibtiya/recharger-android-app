package com.getsmartapp.lib.dataAggregation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.provider.Settings;

import com.getsmartapp.lib.R;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.App;
import com.getsmartapp.lib.model.DataModel;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.lib.utils.SDKUtils;
import com.getsmartapp.lib.utils.SdkLg;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;


/**
 * @author  nitesh on 19/6/15.
 */
public class DataAggregationUtils {
    private String device_id;
    private String phone_number = null;
    private Date start_of_today;
    private Context mContext;
    private SdkDbHelper dbHelper;
    private SQLiteDatabase db;
    private long callsmsbulk_last_sync_timestamp = 0, databulk_last_sync_date = 0, operatorsms_last_sync_timestamp = 0;
    private String ssoId;
    private SharedPrefManager shrd;
    private long fortyfive_days_back = 0;
    public Date last_date = null;

    //Non-Roaming Hashmaps ------>
    HashMap<String, Map<String, Map<String, Integer>>> callHashMap = new LinkedHashMap<>();
    HashMap<String, Map<String, Integer>> smsHashMap = new LinkedHashMap<>();
    HashMap<String, JSONObject> dataHashMap = new LinkedHashMap<>();
    //HashMap<String, JSONObject> operatorHashMap = new LinkedHashMap<>();
    JSONArray operatorSmsArray = new JSONArray();
    //Roaming Hashmaps -------->
    //Roaming Hashmaps -------->
    HashMap<String, Map<String, Float>> roamingDataHashMap = new LinkedHashMap<>();
    HashMap<String, Map<String, Integer>> roamingSmsHashMap = new LinkedHashMap<>();
    HashMap<String, Map<String, Map<String, Integer>>> roamingCallHashMap = new LinkedHashMap<>();
    HashMap<String, Map<String, Map<String, Float>>> roamingBulkDataHashMap = new LinkedHashMap<>();
    HashMap<String, Map<String, Map<String, Integer>>> roamingBulkSmsHashMap = new LinkedHashMap<>();
    HashMap<String, Map<String, Map<String, Map<String, Integer>>>> roamingBulkCallHashMap = new LinkedHashMap<>();
    //App Hashmap ------------>
    HashMap<String, Map<String, Double>> appdataHashmap = new LinkedHashMap<>();


    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'", Locale.getDefault());
    public int call_last_date=0;


    public DataAggregationUtils(Context mContext,int sim_id) {
        this.mContext = mContext;
        dbHelper = SdkDbHelper.getInstance(mContext);//new SdkDbHelper(mContext);
        db = dbHelper.getReadableDatabase();
        shrd = new SharedPrefManager(mContext);
        getPhoneNumber();
        device_id = getDeviceID();
        start_of_today = DateUtil.getStart(new Date(System.currentTimeMillis()));
        setLastSyncTime(sim_id);
//        Log.e("start_time_today", String.valueOf(start_of_today.getTime()));
        if (SDKUtils.isLoggedIn(mContext)) {
            ProxyLoginUser.SoResponseEntity userLoginSSO = new Gson().fromJson(shrd.getStringValue(Constants.USER_DATA), ProxyLoginUser.SoResponseEntity.class);
            ssoId = userLoginSSO.getUserId();
        } else {
            ssoId = "tester0";
        }
        fortyfive_days_back = System.currentTimeMillis() - (45 * 24 * 3600 * 1000L);
    }


    //Non-Roaming Data Usage
    public HashMap<String, JSONObject> setAndGetDataHashMap(int data_type,long databulk_last_sync_date) throws JSONException {

        HashMap<String, JSONObject> dataHashMap = new HashMap<>();

        try{
            String initialDate = "";


            if(databulk_last_sync_date == 0)
                databulk_last_sync_date = Calendar.getInstance().getTimeInMillis();


            float mod_2G_data = 0;
            float mod_3G_data = 0;
            float mod_4G_data = 0;

            long currentTimeinMillis = Calendar.getInstance().getTimeInMillis();
            if((currentTimeinMillis - databulk_last_sync_date) > 30*24*60*60*1000L)
            {
                databulk_last_sync_date = currentTimeinMillis - 30*24*60*60*1000L;
                SdkLg.e("Sync Date Updated: "+DateUtil.getDateFromTimeInMillis(databulk_last_sync_date));

            }else
            SdkLg.e("Sync date not udpated: "+DateUtil.getDateFromTimeInMillis(databulk_last_sync_date));

            SharedPrefManager mSharedPref = new SharedPrefManager(mContext);
            initialDate = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()/*dateMillisArrayList.get(0)*/);

            if(data_type == ApiConstants.DAILY_DATA_TYPE)
            {
                dataHashMap.clear();
                float totalMobileData = InternetDataUsageUtil.getInstance(mContext).getTodaysMobileDataUsed(mContext)/1024f;//dataValuesArrayList.get(0)[0]/1024f;
                float totalData = InternetDataUsageUtil.getInstance(mContext).getTodaysTotalDataUsed()/1024f;
                float wifiData = totalData-totalMobileData;//dataValuesArrayList.get(0)[1]/1024f;


                if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.threeg))) {
                    mod_3G_data = totalMobileData;
                }else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.twog))) {
                    mod_2G_data = totalMobileData;
                } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.fourg))) {
                    mod_4G_data = totalMobileData;
                }else
                    mod_3G_data = totalMobileData;


                JSONObject metaJson = new JSONObject();

                metaJson.put(ApiConstants.last_sync_timestamp, simpleDateFormat.format(new Date(databulk_last_sync_date)));
                metaJson.put(ApiConstants.device_id, device_id);
                metaJson.put(ApiConstants.sso_id, ssoId);
                metaJson.put(ApiConstants.metadate, initialDate);
                metaJson.put(ApiConstants.last_updated_date, simpleDateFormat.format(new Date(databulk_last_sync_date)));
                metaJson.put("onboardMobile", shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                metaJson.put("onboardCircle", shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
                metaJson.put("onboardOperator", shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
                metaJson.put("onboardSimType", shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE));
                metaJson.put("osVersion", Build.VERSION.RELEASE);
                metaJson.put("deviceName", Build.MANUFACTURER + "-" + Build.DEVICE);
                metaJson.put("android_id", Settings.Secure.getString(mContext.getContentResolver
                        (), Settings.Secure.ANDROID_ID));
                JSONObject dataObj = new JSONObject();

                dataObj.put(ApiConstants.metadataObj, metaJson);
                dataObj.put(ApiConstants.total_data, totalData);
                dataObj.put(ApiConstants.two_g_data, mod_2G_data);
                dataObj.put(ApiConstants.three_g_data, mod_3G_data);
                dataObj.put(ApiConstants.four_g_data, mod_4G_data);
                dataObj.put(ApiConstants.wifi_data, wifiData);
                dataObj.put(ApiConstants.total_network, totalMobileData);
                dataHashMap.put(initialDate, dataObj);

            }else if(data_type == ApiConstants.BULK_DATA_TYPE) {
//            for(int i=0;i<dataValuesArrayList.size();i++)
                boolean checkDate = true;
                long timeInMillis = Calendar.getInstance().getTimeInMillis();

                dataHashMap.clear();
                while (checkDate)
                {
//                    if (!initialDate.equals(""))
                    {

//                    initialDate = DateUtil.getDateFromTimeInMillis(dateMillisArrayList.get(i));

                        SdkLg.e("Entry: " +((timeInMillis > databulk_last_sync_date || DateUtil.getDateFromTimeInMillis(timeInMillis).equalsIgnoreCase(DateUtil.getDateFromTimeInMillis(databulk_last_sync_date)))));
                        if(timeInMillis > databulk_last_sync_date || DateUtil.getDateFromTimeInMillis(timeInMillis).equalsIgnoreCase(DateUtil.getDateFromTimeInMillis(databulk_last_sync_date)))
                        {
                            String date = DateUtil.getDateFromTimeInMillis(timeInMillis);
                            timeInMillis = timeInMillis - 24*60*60*1000L;
                            JSONObject jsonObject = InternetDataUsageUtil.getInstance(mContext).getDataUsageForSpecificDate(date);

//                            if(jsonObject.length()==0) {
//                                checkDate = false;
//                            }

//                            if(jsonObject.length() > 0)
                            {
                                SdkLg.e("Data date: " +date);

                                float totalMobileData = 0;
                                float totalData = 0;
                                float wifiData = 0;
                                try {
                                    JSONObject dataJsonObject = jsonObject.getJSONObject(InternetDataUsageUtil.DATA);
                                    totalMobileData = dataJsonObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA) / 1024f;//dataValuesArrayList.get(i)[0] / 1024f;
                                    totalData = dataJsonObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA) / 1024f;//totalMobileData + wifiData;
                                    wifiData = totalData - totalMobileData;//dataValuesArrayList.get(i)[1] / 1024f;
//                        float totalData = totalMobileData + wifiData;
                                }catch (Exception e){}
                                if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.threeg))) {
                                    mod_3G_data = totalMobileData;
                                } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.twog))) {
                                    mod_2G_data = totalMobileData;
                                } else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.fourg))) {
                                    mod_4G_data = totalMobileData;
                                }else
                                    mod_3G_data = totalMobileData;

                                JSONObject metaJson = new JSONObject();
                                metaJson.put(ApiConstants.last_sync_timestamp, simpleDateFormat.format(new Date(databulk_last_sync_date)));
                                metaJson.put(ApiConstants.device_id, device_id);
                                metaJson.put(ApiConstants.sso_id, ssoId);
                                metaJson.put(ApiConstants.metadate, date);
                                metaJson.put(ApiConstants.last_updated_date, simpleDateFormat.format(new Date(databulk_last_sync_date)));
                                metaJson.put("onboardMobile", shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                                metaJson.put("onboardCircle", shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
                                metaJson.put("onboardOperator", shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
                                metaJson.put("onboardSimType", shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE));
                                metaJson.put("osVersion", Build.VERSION.RELEASE);
                                metaJson.put("deviceName", Build.MANUFACTURER + "-" + Build.DEVICE);
                                try {
                                    metaJson.put("android_id", Settings.Secure.getString(mContext.getContentResolver
                                            (), Settings.Secure.ANDROID_ID));
                                }catch (Exception e){
                                    metaJson.put("android_id", "AndroidID_Not_Found");
                                }
                                JSONObject dataObj = new JSONObject();

                                dataObj.put(ApiConstants.metadataObj, metaJson);
                                dataObj.put(ApiConstants.total_data, totalData);
                                dataObj.put(ApiConstants.two_g_data, mod_2G_data);
                                dataObj.put(ApiConstants.three_g_data, mod_3G_data);
                                dataObj.put(ApiConstants.four_g_data, mod_4G_data);
                                dataObj.put(ApiConstants.wifi_data, wifiData);
                                dataObj.put(ApiConstants.total_network, totalMobileData);
                                SdkLg.e(date+" Object: "+dataObj);
                                dataHashMap.put(date, dataObj);
                            }

                        }else {
                            checkDate = false;
                            SdkLg.e("Exiting");
                        }

                    }
                }
                SdkLg.e("Final: "+dataHashMap.toString());


            }
        }catch (Exception e){}
        return dataHashMap;
    }

    //Non-Roaming SMS Usage + Utils
    public HashMap<String, Map<String, Integer>> setAndGetSmsHashMap(int data_type, boolean forpost,int sim_id) {
        Cursor smsCursor = getSmsCursor(data_type, ApiConstants.NON_ROAMING, sim_id);
        if (forpost) {
            smsCursor = forPostSms(ApiConstants.NON_ROAMING);
        }
        String iniDate = "";
        Map<String, Integer> smsMapObj = new LinkedHashMap<String, Integer>();
        try {
            if (smsCursor != null && smsCursor.moveToFirst()) {
                do {
                    String date = smsCursor.getString(smsCursor.getColumnIndex(ApiConstants.metadate));
                    String hash = smsCursor.getString(smsCursor.getColumnIndex(ApiConstants.COLUMN_HASH));
                    int count = smsCursor.getInt(smsCursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
//                    Log.d("SMS-HASH", "Date: " + date + " | Hash: " + hash + " | Count: " + count);
                    if (!date.equals(iniDate) && hash != null) {
                        if (!iniDate.equals("")) {
                            smsHashMap.put(iniDate, smsMapObj);
                        }
                        smsMapObj = processSMS(hash, count);
                        iniDate = date;
                    } else {
                        assert smsMapObj != null;
                        if (hash != null && hash != "") {
                            smsMapObj = processSameDateSMS(hash, count, smsMapObj);
                        }
                    }
                } while (smsCursor.moveToNext());
            }
        } catch (Exception e) {
        } finally {
            if (!iniDate.equals("")) {
                smsHashMap.put(iniDate, smsMapObj);
            }
            if (smsCursor != null) {
                smsCursor.close();
            }
        }
        return smsHashMap;
    }

    public Map<String, Integer> processSMS(String hash, int count) {
        String keys[] = hash.split(",");
        Map<String, Integer> smsMap = new LinkedHashMap<String, Integer>();
        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_DAY_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_NIGHT_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_NIGHT_INCOMING], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_DAY_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_NIGHT_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_NIGHT_INCOMING], count);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_DAY_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_NIGHT_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_NIGHT_INCOMING], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_DAY_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_NIGHT_OUTGOING], count);
                    } else {
                        smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_NIGHT_INCOMING], count);
                    }
                }
            }
        } else {
            if (!keys[1].equals("India")) {
                if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                    smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_ISD_OUTGOING], count);
                } else {
                    smsMap = getUpdatedSmsMap(smsMap, ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_ISD_INCOMING], count);
                }
            }
        }
        return smsMap;
    }

    public Map<String, Integer> processSameDateSMS(String hash, int count, Map<String, Integer> smsMap) {
        String[] keys = hash.split(",");
        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_DAY_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_NIGHT_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_SAME_NIGHT_INCOMING], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_DAY_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_NIGHT_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_LOCAL_OTHER_NIGHT_INCOMING], count);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_DAY_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_NIGHT_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_SAME_NIGHT_INCOMING], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_DAY_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_DAY_INCOMING], count);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_NIGHT_OUTGOING], count);
                    } else {
                        smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_STD_OTHER_NIGHT_INCOMING], count);
                    }
                }
            }
        } else {
            if (!keys[1].equals("India")) {
                if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                    smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_ISD_OUTGOING], count);
                } else {
                    smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_ISD_INCOMING], count);
                }
            }
        }
        return smsMap;
    }

    public Map<String, Integer> getUpdatedSmsMap(Map<String, Integer> smsMap, String smsUsageKey, int count) {
        smsMap.put(smsUsageKey, count);
        return smsMap;
    }

    //Non-Roaming Call Usage + Utils
    public HashMap<String, Map<String, Map<String, Integer>>> setAndGetCallHashMap(int data_type, boolean forpostuse,int sim_id) {
        Cursor callCursor = getCallDataCursor(data_type, ApiConstants.NON_ROAMING, sim_id);
        if (forpostuse) {
            callCursor = forPostCall(ApiConstants.NON_ROAMING);
        }
        Map<String, Map<String, Integer>> callMapObj = new LinkedHashMap<>();
        String intiDate = "";
        int count = 0;
        int mins = 0;
        int secs = 0;
        String circle_Id = null;
        //Log.e("callcount",callCursor.getCount()+"");
        try {
            if (callCursor != null && callCursor.getCount() > 0 && callCursor.moveToFirst()) {
                do {
                    String date = callCursor.getString(callCursor.getColumnIndex(ApiConstants.metadate));
                    count = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    mins = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_MIN));
                    secs = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_SEC));
                    String hash = callCursor.getString(callCursor.getColumnIndex(ApiConstants.COLUMN_HASH));
                    circle_Id = callCursor.getString(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_CIRCLE_ID));
//                    Log.d("CALL-HASH", "Date: " + date + " | Hash: " + hash + " | Count: " + count + " | Mins: " + mins + " | Secs: " + secs);

                    if (!date.equals(intiDate) && hash != null) {
                        if (!intiDate.equals("")) {
                            callHashMap.put(intiDate, callMapObj);
                        }
                        callMapObj = processStdLocal(hash, count, mins, secs);
                        intiDate = date;
                    } else {
                        assert callMapObj != null;
                        if (hash != null && !hash.equalsIgnoreCase("")) {
                            callMapObj = processSameDateStdLocal(hash, count, mins, secs, callMapObj);
                        }
                    }
                } while (callCursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (data_type == ApiConstants.DAILY_DATA_TYPE) {
                if (!intiDate.equals("") && callHashMap.size() < 1) {
                    callHashMap.put(intiDate, callMapObj);
                }
            } else {
                if (!intiDate.equalsIgnoreCase(""))
                    callHashMap.put(intiDate, callMapObj);

            }
            if (callCursor != null) {
                callCursor.close();
            }
        }
        return callHashMap;
    }

    public Map<String, Map<String, Integer>> processStdLocal(String hash, int count, int mins, int secs) {
        String keys[] = hash.split(",");
        Map<String, Map<String, Integer>> callMap = new LinkedHashMap<>();

        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) { //LOCAL
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0")) { //DAY
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_DAY_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_DAY_INCOMING], count, mins, secs);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_NIGHT_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_NIGHT_INCOMING], count, mins, secs);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_DAY_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_DAY_INCOMING], count, mins, secs);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_NIGHT_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_NIGHT_INCOMING], count, mins, secs);
                    }
                }
            } else {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //LANDLINE
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_DAY_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_DAY_INCOMING], count, mins, secs);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_NIGHT_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_NIGHT_INCOMING], count, mins, secs);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) { //STD
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0")) { //DAY
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_DAY_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_DAY_INCOMING], count, mins, secs);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_NIGHT_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_NIGHT_INCOMING], count, mins, secs);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_DAY_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_DAY_INCOMING], count, mins, secs);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_NIGHT_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_NIGHT_INCOMING], count, mins, secs);
                    }
                }
            } else {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //LANDLINE
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_DAY_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_DAY_INCOMING], count, mins, secs);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_NIGHT_OUTGOING], count, mins, secs);
                    } else {
                        callMap = getUpdatedCallMap(callMap, ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_NIGHT_INCOMING], count, mins, secs);
                    }
                }
            }
        } else { //ISD
            String country = keys[1];
            if (!country.equalsIgnoreCase("India")) {
                if (Arrays.asList(keys).contains(DataStorageConstants.INCOMING_CALL_TYPE)) {
                    callMap = getUpdatedCallMap(callMap, "call-isd-" + country + "-incoming", count, mins, secs);
                } else {
                    callMap = getUpdatedCallMap(callMap, "call-isd-" + country + "-outgoing", count, mins, secs);
                }
            }
        }
        return callMap;
    }

    public Map<String, Map<String, Integer>> processSameDateStdLocal(String hash, int count, int mins, int secs, Map<String, Map<String, Integer>> callMap) {
        String[] keys = hash.split(",");
        Map<String, Integer> countMap = new LinkedHashMap<>();
        countMap.put("count", count);
        countMap.put("mins", mins);
        countMap.put("secs", secs);

        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) { //LOCAL
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0")) { //DAY
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_DAY_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_DAY_INCOMING], countMap);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_NIGHT_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_OTHER_NIGHT_INCOMING], countMap);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_DAY_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_DAY_INCOMING], countMap);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_NIGHT_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_SAME_NIGHT_INCOMING], countMap);
                    }
                }
            } else {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //LANDLINE
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_DAY_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_DAY_INCOMING], countMap);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_NIGHT_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_LOCAL_LANDLINE_NIGHT_INCOMING], countMap);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) { //STD
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0")) { //DAY
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_DAY_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_DAY_INCOMING], countMap);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_NIGHT_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_OTHER_NIGHT_INCOMING], countMap);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_DAY_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_DAY_INCOMING], countMap);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_NIGHT_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_SAME_NIGHT_INCOMING], countMap);
                    }
                }
            } else {
                if (Arrays.asList(keys).contains("0")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //LANDLINE
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_DAY_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_DAY_INCOMING], countMap);
                    }
                } else {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_NIGHT_OUTGOING], countMap);
                    } else {
                        callMap.put(ApiConstants.CALL_USAGE_KEYS[ApiConstants.CALL_STD_LANDLINE_NIGHT_INCOMING], countMap);
                    }
                }
            }
        } else { //ISD
            String country = keys[1];
            if (!country.equalsIgnoreCase("India")) {
                if (Arrays.asList(keys).contains(DataStorageConstants.INCOMING_CALL_TYPE)) {
                    callMap.put("call-isd-" + country + "-incoming", countMap);
                } else {
                    callMap.put("call-isd-" + country + "-outgoing", countMap);
                }
            }
        }
        return callMap;
    }

    public Map<String, Map<String, Integer>> getUpdatedCallMap(Map<String, Map<String, Integer>> callMap, String callUsageKey, int count, int mins, int secs) {
        Map<String, Integer> countMap = new LinkedHashMap<>();
        //  Map<String, Integer> emptyMap = new LinkedHashMap<>();
        countMap.put("count", count);
        countMap.put("mins", mins);
        countMap.put("secs", secs);
        callMap.put(callUsageKey, countMap);
        return callMap;
    }

    //Roaming Data Usage
//    public HashMap<String, Map<String, Float>> setAndGetRoamingDataHashMap(int data_type) {
//        Cursor roamingDataCursor = getDataCursor(data_type, ApiConstants.ROAMING);
//        Map<String, Float> roamingDataMap = new LinkedHashMap<>();
//        long _2G = 0;
//        long _3G = 0;
//        long _4G = 0;
//        long total_wifi = 0;
//        String initialCircleId = "";
//        try {
//            if (roamingDataCursor != null && roamingDataCursor.moveToFirst()) {
//                do {
//                    String date = roamingDataCursor.getString(roamingDataCursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_DATE));
//                    String old_type = roamingDataCursor.getString(roamingDataCursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_OLDTYPE));
//                    long session_data = roamingDataCursor.getLong(roamingDataCursor.getColumnIndex(DataStorageConstants.TOTAL_SESSION_BYTE));
//                    String circle_Id = roamingDataCursor.getString(roamingDataCursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_CIRCLE_ID));
//                    if (initialCircleId != null && !initialCircleId.equals(circle_Id)) {
//                        if (!initialCircleId.equals("")) {
//                            float mod_wifi = (total_wifi / 1024f);
//                            float mod_3G_data = (_3G / 1024f);
//                            float mod_2G_data = (_2G / 1024f);
//                            float mod_4G_data = (_4G / 1024f);
//                            float total = mod_3G_data + mod_2G_data + mod_wifi;
//                            float total_network = mod_3G_data + mod_2G_data + mod_4G_data;
//                            roamingDataMap.put(ApiConstants.total_data, total);
//                            roamingDataMap.put(ApiConstants.two_g_data, mod_2G_data);
//                            roamingDataMap.put(ApiConstants.three_g_data, mod_3G_data);
//                            roamingDataMap.put(ApiConstants.four_g_data, mod_4G_data);
//                            roamingDataMap.put(ApiConstants.wifi_data, mod_wifi);
//                            roamingDataMap.put(ApiConstants.total_network, total_network);
//                            roamingDataHashMap.put(initialCircleId, roamingDataMap);
//                            _2G = 0;
//                            _3G = 0;
//                            total_wifi = 0;
//                        }
//                        if (old_type != null && old_type.equals(DataStorageConstants.THREEG_CONN)) {
//                            _3G = session_data;
//                        } else if (old_type != null && old_type.equals(DataStorageConstants.TWOG_CONN)) {
//                            _2G = session_data;
//                        } else {
//                            total_wifi = session_data;
//                        }
//                        initialCircleId = circle_Id;
//                    } else {
//                        if (old_type != null && old_type.equals(DataStorageConstants.THREEG_CONN)) {
//                            _3G += session_data;
//                        } else if (old_type != null && old_type.equals(DataStorageConstants.TWOG_CONN)) {
//                            _2G += session_data;
//                        } else {
//                            total_wifi += session_data;
//                        }
//                    }
//                } while (roamingDataCursor.moveToNext());
//            }
//        } catch (Exception e) {
//        } finally {
//            float mod_wifi = (float) (total_wifi / 1024f);
//            float mod_3G_data = (float) (_3G / 1024f);
//            float mod_2G_data = (float) (_2G / 1024f);
//            float mod_4G_data = (float) (_4G / 1024f);
//            float total = mod_3G_data + mod_2G_data + mod_wifi;
//            float total_network = mod_3G_data + mod_2G_data + mod_4G_data;
//            roamingDataMap.put(ApiConstants.total_data, total);
//            roamingDataMap.put(ApiConstants.two_g_data, mod_2G_data);
//            roamingDataMap.put(ApiConstants.three_g_data, mod_3G_data);
//            roamingDataMap.put(ApiConstants.four_g_data, mod_4G_data);
//            roamingDataMap.put(ApiConstants.wifi_data, mod_wifi);
//            roamingDataMap.put(ApiConstants.total_network, total_network);
//            if (initialCircleId != null && initialCircleId != "") {
//                roamingDataHashMap.put(initialCircleId, roamingDataMap);
//            }
//            if (roamingDataCursor != null) {
//                roamingDataCursor.close();
//            }
//        }
//        return roamingDataHashMap;
//    }

    //Roaming SMS Usage + Utils
    public HashMap<String, Map<String, Integer>> setAndGetRoamingSmsHashMap(int data_type,int sim_id) {
        Cursor smsCursor = getSmsCursor(data_type, ApiConstants.ROAMING,sim_id);
        String initialCircleId = "";
        Map<String, Integer> smsMapObj = new LinkedHashMap<String, Integer>();
        try {
            if (smsCursor != null && smsCursor.moveToFirst()) {
                do {
                    String date = smsCursor.getString(smsCursor.getColumnIndex(ApiConstants.metadate));
                    String hash = smsCursor.getString(smsCursor.getColumnIndex(ApiConstants.COLUMN_HASH));
                    int count = smsCursor.getInt(smsCursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    String circle_Id = smsCursor.getString(smsCursor.getColumnIndex(DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID));
                    if (initialCircleId != null && !initialCircleId.equals(circle_Id) && hash != null) {
                        if (!initialCircleId.equals("")) {
                            roamingSmsHashMap.put(circle_Id, smsMapObj);
                        }
                        smsMapObj = processRoamingSMS(hash, count);
                        initialCircleId = circle_Id;
                    } else {
                        assert smsMapObj != null;
                        if (hash != null && hash != "") {
                            smsMapObj = processSameCircleSMS(hash, count, smsMapObj);
                        }
                    }
                } while (smsCursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (data_type == ApiConstants.DAILY_DATA_TYPE) {
                if (initialCircleId != null && !initialCircleId.equals("")) {
                    roamingSmsHashMap.put(initialCircleId, smsMapObj);
                }
            } else {
                if (!initialCircleId.equalsIgnoreCase("")) {
                    roamingSmsHashMap.put(initialCircleId, smsMapObj);
                }
            }
            if (smsCursor != null) {
                smsCursor.close();
            }
        }
        return roamingSmsHashMap;
    }

    public Map<String, Integer> processRoamingSMS(String hash, int count) {
        String keys[] = hash.split(",");
        Map<String, Integer> smsMap = new LinkedHashMap<String, Integer>();
        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedRoamingSmsMap(smsMap, ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_LOCAL_SAME], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedRoamingSmsMap(smsMap, ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_LOCAL_OTHER], count);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedRoamingSmsMap(smsMap, ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_STD_SAME], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap = getUpdatedRoamingSmsMap(smsMap, ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_STD_OTHER], count);
                    }
                }
            }
        } else {
            if (!keys[1].equals("India")) {
                if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                    smsMap = getUpdatedSmsMap(smsMap, ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_ISD], count);
                }
            }
        }
        return smsMap;
    }

    public Map<String, Integer> processSameCircleSMS(String hash, int count, Map<String, Integer> smsMap) {
        String keys[] = hash.split(",");
        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_LOCAL_SAME], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_LOCAL_OTHER], count);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) {
            if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_LOCAL_SAME], count);
                    }
                }
            }
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                        smsMap.put(ApiConstants.ROAMING_SMS_USAGE_KEYS[ApiConstants.ROAMING_SMS_LOCAL_OTHER], count);
                    }
                }
            }
        } else {
            if (!keys[1].equals("India")) {
                if (Arrays.asList(keys).contains(DataStorageConstants.SENT_SMS_TYPE)) {
                    smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_ISD_OUTGOING], count);
                } else {
                    smsMap.put(ApiConstants.SMS_USAGE_KEYS[ApiConstants.SMS_ISD_INCOMING], count);
                }
            }
        }
        return smsMap;
    }

    public Map<String, Integer> getUpdatedRoamingSmsMap(Map<String, Integer> smsMap, String smsUsageKey, int count) {
        smsMap.put(smsUsageKey, count);
        return smsMap;
    }

    //Roaming Call Usage + Utils
    public HashMap<String, Map<String, Map<String, Integer>>> setAndGetRoamingCallHashMap(int data_type,int sim_id) {
        Cursor callCursor = getCallDataCursor(data_type, ApiConstants.ROAMING, sim_id);
        Map<String, Map<String, Integer>> callMapObj = new LinkedHashMap<>();
        int count = 0;
        int mins = 0;
        int secs = 0;
        String initialCircleId = "";
        try {
            if (callCursor != null && callCursor.getCount() > 0 && callCursor.moveToFirst()) {
                do {
                    String date = callCursor.getString(callCursor.getColumnIndex(ApiConstants.metadate));
                    count = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    mins = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_MIN));
                    secs = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_SEC));
                    String hash = callCursor.getString(callCursor.getColumnIndex(ApiConstants.COLUMN_HASH));
                    String circle_Id = callCursor.getString(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_CIRCLE_ID));
                    if (initialCircleId != null && !initialCircleId.equals(circle_Id) && hash != null) {
                        if (!initialCircleId.equals("")) {
                            roamingCallHashMap.put(initialCircleId, callMapObj);
                        }
                        callMapObj = processRoamingCall(hash, count, mins, secs);
                        circle_Id = circle_Id == null ? "0" : circle_Id;
                        initialCircleId = circle_Id;
                    } else {
                        assert callMapObj != null;
                        if (hash != null && hash != "") {
//                            Log.e("CALL-HASH", hash);
                            callMapObj = processSameCircleCall(hash, count, mins, secs, callMapObj);
                        }
                    }
                } while (callCursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (data_type == ApiConstants.DAILY_DATA_TYPE) {
                if (initialCircleId != null && !initialCircleId.equals("")) {
                    roamingCallHashMap.put(initialCircleId, callMapObj);
                }
            } else {
                if (!initialCircleId.equalsIgnoreCase("")) {
                    roamingCallHashMap.put(initialCircleId, callMapObj);
                }
            }
            if (callCursor != null) {
                callCursor.close();
            }
        }
        return roamingCallHashMap;
    }

    public Map<String, Map<String, Integer>> processRoamingCall(String hash, int count, int mins, int secs) {
        String keys[] = hash.split(",");
        Map<String, Map<String, Integer>> callMap = new LinkedHashMap<>();

        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) { //LOCAL
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_LOCAL_OTHER], count, mins, secs);
                    } else {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], count, mins, secs);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_LOCAL_SAME], count, mins, secs);
                    } else {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], count, mins, secs);
                    }
                }
            } else { //LANDLINE
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_LOCAL_LANDLINE], count, mins, secs);
                    } else {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], count, mins, secs);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) { //STD
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_STD_OTHER], count, mins, secs);
                    } else {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], count, mins, secs);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_STD_SAME], count, mins, secs);
                    } else {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], count, mins, secs);
                    }
                }
            } else {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_STD_LANDLINE], count, mins, secs);
                    } else {
                        callMap = getUpdatedRoamingCallMap(callMap, ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], count, mins, secs);
                    }
                }
            }
        } else {
        }
        return callMap;
    }

    public Map<String, Map<String, Integer>> processSameCircleCall(String hash, int count, int mins, int secs, Map<String, Map<String, Integer>> callMap) {
        String keys[] = hash.split(",");
        Map<String, Integer> countMap = new LinkedHashMap<>();
        countMap.put("count", count);
        countMap.put("mins", mins);
        countMap.put("secs", secs);

        if (Arrays.asList(keys).contains(DataStorageConstants.LOCAL_NUMBER)) { //LOCAL
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_LOCAL_OTHER], countMap);
                    } else {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], countMap);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_LOCAL_SAME], countMap);
                    } else {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], countMap);
                    }
                }
            } else { //LANDLINE
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) {
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_LOCAL_LANDLINE], countMap);
                    } else {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], countMap);
                    }
                }
            }
        } else if (Arrays.asList(keys).contains(DataStorageConstants.STD_NUMBER)) { //STD
            if (Arrays.asList(keys).contains(DataStorageConstants.OTHER_CALLNETWORK)) { //OTHER
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_STD_OTHER], countMap);
                    } else {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], countMap);
                    }
                }
            } else if (Arrays.asList(keys).contains(DataStorageConstants.SAME_CALLNETWORK)) { //SAME
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_STD_SAME], countMap);
                    } else {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], countMap);
                    }
                }
            } else {
                if (Arrays.asList(keys).contains("0") || Arrays.asList(keys).contains("1")) { //DAY or NIGHT
                    if (Arrays.asList(keys).contains(DataStorageConstants.OUTGOING_CALL_TYPE)) { //OUTGOING
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_STD_LANDLINE], countMap);
                    } else {
                        callMap.put(ApiConstants.ROAMING_CALL_USAGE_KEYS[ApiConstants.ROAMING_CALL_INCOMING], countMap);
                    }
                }
            }
        } else {
        }
        return callMap;
    }

    public Map<String, Map<String, Integer>> getUpdatedRoamingCallMap(Map<String, Map<String, Integer>> callMap, String callUsageKey, int count, int mins, int secs) {
        Map<String, Integer> countMap = new LinkedHashMap<>();
        //Map<String, Integer> emptyMap = new LinkedHashMap<>();

        countMap.put("count", count);
        countMap.put("mins", mins);
        countMap.put("secs", secs);
        callMap.put(callUsageKey, countMap);
        return callMap;
    }

    // ---- Getting and setting App data in hashmap
    public Map<String, Map<String, Double>> setAndGetAppData() {
//        Cursor app_cursor = null;
        try {

            List<App> mList = InternetDataUsageUtil.getInstance(mContext).getAppWiseTotalDataUsageForLastNumberOfDays(1, false);
            int size = 20;
            if(mList.size() < size)
                size = mList.size();

            for(int i=0;i<size;i++)
            {
                addToAppDataHashmap(mList.get(i));
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } /*finally {
            if (app_cursor != null)
                app_cursor.close();
        }*/

        return appdataHashmap;
    }

    // ---- Getting and setting App data in hashmap
    public Map<String, Map<String, Double>> setAndGetAppData(String date) {
//        Cursor app_cursor = null;
        try {

            List<App> mList = InternetDataUsageUtil.getInstance(mContext).getAppWiseTotalDataUsageDateWise(date);
            int size = 20;
            if(mList.size() < size)
                size = mList.size();

            for(int i=0;i<size;i++)
            {
                addToAppDataHashmap(mList.get(i));
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } /*finally {
            if (app_cursor != null)
                app_cursor.close();
        }*/

        return appdataHashmap;
    }

//    // ---- Getting and setting App data in hashmap
//    public Map<String, Map<String, Integer>> setAndGetAppData() {
//        Cursor app_cursor = null;
//        try {
//            app_cursor = getAppDataCursor();
//            if (app_cursor != null && app_cursor.moveToFirst()) {
//                do {
//                    addToAppDataHashmap(app_cursor);
//
//                } while (app_cursor.moveToNext());
//
//            }
//            Log.e("hashmap", appdataHashmap.toString());
//        } catch (SQLiteException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//        } finally {
//            if (app_cursor != null)
//                app_cursor.close();
//        }
//
//        return appdataHashmap;
//    }

    private void addToAppDataHashmap(App app_cursor) {
        String iniDate = "";
        Map<String, Double> appmap = new HashMap<>();
        SharedPrefManager mSharedPref = new SharedPrefManager(mContext);
//        Log.e("PeeyushKS","AppName: "+app_cursor.getApp_name()+", Total Data:  "+InternetDataUsageUtil.humanReadableByteCount((long) app_cursor.getTotal_dataUsed())+", Mobile Data:  "+InternetDataUsageUtil.humanReadableByteCount((long)app_cursor.getmobile_dataUsed()));
        String date = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());
        String app_id = app_cursor.getApp_package_name();
        int is_bg = 1;
        long twog_upload = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_TRANSMITTED));
        long twog_download = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_RECIEVED));
        long threeg_upload = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_TRANSMITTED));
        long threeg_download = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_RECIEVED));
        long fourg_upload = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_TRANSMITTED));
        long fourg_download = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_RECIEVED));

        if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).trim().length() == 0 || mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.threeg))) {
            threeg_download = (long)app_cursor.getmobile_dataUsed();
        }
        else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.twog))) {
            twog_download = (long)app_cursor.getmobile_dataUsed();
        }
        else if (mSharedPref.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE).equalsIgnoreCase(mContext.getString(R.string.fourg))) {
            fourg_download = (long)app_cursor.getmobile_dataUsed();;
        }

        long wifi_upload = 0;//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_WIFI_DATA_TRANSMITTED));
        long wifi_download = (long)app_cursor.getWifi_dataUsed();//app_cursor.getLong(app_cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_WIFI_DATA_RECIEVED));

        DecimalFormat df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.CEILING);

        Double kb_twog_upload = Double.parseDouble(df.format(twog_upload / (1024.0)));
        Double kb_twog_download = Double.parseDouble(df.format(twog_download / (1024.0)));
        Double kb_threeg_upload = Double.parseDouble(df.format(threeg_upload / (1024.0)));
        Double kb_threeg_download = Double.parseDouble(df.format(threeg_download / (1024.0)));
        Double kb_fourg_upload = Double.parseDouble(df.format(fourg_upload / (1024.0)));
        Double kb_fourg_download = Double.parseDouble(df.format(fourg_download / 1024.0));
        Double kb_wifi_upload = Double.parseDouble(df.format(wifi_upload / (1024.0)));
        Double kb_wifi_download = Double.parseDouble(df.format(wifi_download / (1024.0)));

        //removing "." from app_name

        app_id = app_id.replace(".", "_");

        // Making key-value pair

        if (is_bg == 1) {
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_BACKGROUND_2G], kb_twog_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_BACKGROUND_3G], kb_threeg_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_BACKGROUND_4G], kb_fourg_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_BACKGROUND_WIFI], kb_wifi_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_BACKGROUND_2G], kb_twog_download);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_BACKGROUND_3G], kb_threeg_download);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_BACKGROUND_4G], kb_fourg_download);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_BACKGROUND_WIFI], kb_wifi_download);
        } else {
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_FOREGROUND_2G], kb_twog_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_FOREGROUND_3G], kb_threeg_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_FOREGROUND_4G], kb_fourg_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.UPLOAD_FOREGROUND_WIFI], kb_wifi_upload);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_FOREGROUND_2G], kb_twog_download);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_FOREGROUND_3G], kb_threeg_download);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_FOREGROUND_4G], kb_fourg_download);
            appmap.put(ApiConstants.APP_USAGE_KEYS[ApiConstants.DOWNLOAD_FOREGROUND_WIFI], kb_wifi_download);
        }
        appdataHashmap.put(app_id, appmap);
    }


    public Cursor getCallDataCursor(int data_type, int is_roaming, int simid) {
        try {
            String query = null;
            String sim_str = "";
            if (simid != 0) {
                sim_str = " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " = " + simid;
            }

            if (data_type == ApiConstants.DAILY_DATA_TYPE) {
                query = "SELECT " +
                        "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                        "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                        "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                        "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", "
                        + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + ", "
                        + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.CallDataEntry.COLUMN_TYPE + " as hash, " +
                        DBContractor.CallDataEntry.COLUMN_CIRCLE_ID +
                        " from " + DBContractor.CallDataEntry.TABLE_NAME +
                        " where " +
                        DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "= " + is_roaming +
                        " and " +
                        DBContractor.CallDataEntry.COLUMN_STARTTIME + "> " + start_of_today.getTime() +
                        sim_str +
                        " group by " +
                        ApiConstants.metadate + ", " +
                        DBContractor.CallDataEntry.COLUMN_IS_NIGHT + ", " +
                        DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + ", " +
                        DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + ", " +
                        DBContractor.CallDataEntry.COLUMN_TYPE +
                        " order by " +
                        ApiConstants.metadate + " DESC";

//            Log.e("daily_query",query);
            } else {
                if (callsmsbulk_last_sync_timestamp == 0) {
                    query = "SELECT " +
                            "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                            "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                            "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                            "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", "
                            + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.CallDataEntry.COLUMN_TYPE + " as hash, "
                            + DBContractor.CallDataEntry.COLUMN_CIRCLE_ID +
                            " from " + DBContractor.CallDataEntry.TABLE_NAME +
                            " where " +
                            DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "= " + is_roaming +
                            " and " +
                            DBContractor.CallDataEntry.COLUMN_STARTTIME + " > " + (Calendar.getInstance().getTimeInMillis() - 30 * 24 * 60 * 60 * 1000L) +
                            sim_str +
                            " group by " + ApiConstants.metadate + ", " +
                            DBContractor.CallDataEntry.COLUMN_IS_NIGHT + ", " +
                            DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + ", " +
                            DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + ", " +
                            DBContractor.CallDataEntry.COLUMN_TYPE +
                            " order by " + ApiConstants.metadate + " DESC";
                } else {
                    query = "SELECT " +
                            "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                            "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                            "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                            "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", "
                            + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.CallDataEntry.COLUMN_TYPE + " as hash, "
                            + DBContractor.CallDataEntry.COLUMN_CIRCLE_ID +
                            " from " + DBContractor.CallDataEntry.TABLE_NAME +
                            " where " +
                            DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "= " + is_roaming +
                            " and " +
                            DBContractor.CallDataEntry.COLUMN_STARTTIME + " > " + callsmsbulk_last_sync_timestamp +
                            sim_str +
                            " group by " + ApiConstants.metadate + ", " +
                            DBContractor.CallDataEntry.COLUMN_IS_NIGHT + ", " +
                            DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + ", " +
                            DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + ", " +
                            DBContractor.CallDataEntry.COLUMN_TYPE +
                            " order by " + ApiConstants.metadate + " DESC";
                }
//            Log.e("Ccall_query", query);
            }

            return db.rawQuery(query, null);
        }catch (Exception e) {
            return null;
        }
    }

    public Cursor getSmsCursor(int data_type, int is_roaming,int sim_id) {
        String query = null;
        String sms_sim_str = "";
        if(sim_id!=0){
            sms_sim_str = " and "+ DBContractor.ReferenceAppEntry.COLUMN_SIMNO+" = "+sim_id;
        }

        if (data_type == ApiConstants.DAILY_DATA_TYPE && !simpleDateFormat.format(new Date(callsmsbulk_last_sync_timestamp)).equals("")) {
            query = "SELECT " +
                    "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                    "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch')                    as " + ApiConstants.metadate + ", " +
                    DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.SmsDataEntry.COLUMN_TYPE + " as hash, " +
                    DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID +
                    " from " + DBContractor.SmsDataEntry.TABLE_NAME +
                    " where " +
                    DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "= " + is_roaming +
                    " and " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + "> " + start_of_today.getTime() +
                    sms_sim_str+
                    " group by " +
                    ApiConstants.metadate + ", " +
                    DBContractor.CallDataEntry.COLUMN_IS_NIGHT + ", " +
                    DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + ", " +
                    DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + ", " +
                    DBContractor.SmsDataEntry.COLUMN_TYPE +
                    " order by " +
                    ApiConstants.metadate + " DESC";
        } else {
            if (callsmsbulk_last_sync_timestamp == 0) {
                query = "SELECT " +
                        "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                        "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", " +
                        DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.SmsDataEntry.COLUMN_TYPE + " as hash, " +
                        DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID +
                        " from " + DBContractor.SmsDataEntry.TABLE_NAME + " where " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=  " + is_roaming +
                        sms_sim_str+
                        " group by " +
                        ApiConstants.metadate + ", " +
                        DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + ", " +
                        DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + ", " +
                        DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + ", " +
                        DBContractor.SmsDataEntry.COLUMN_TYPE +
                        " order by " +
                        ApiConstants.metadate + " DESC";
            } else {
                query = "SELECT " +
                        "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                        "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", " +
                        DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.SmsDataEntry.COLUMN_TYPE + " as hash, " +
                        DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID +
                        " from " + DBContractor.SmsDataEntry.TABLE_NAME + " where " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=  " + is_roaming +
                        " and " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " > " + callsmsbulk_last_sync_timestamp +
                        sms_sim_str+
                        " group by " +
                        ApiConstants.metadate + ", " +
                        DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + ", " +
                        DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + ", " +
                        DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + ", " +
                        DBContractor.SmsDataEntry.COLUMN_TYPE +
                        " order by " +
                        ApiConstants.metadate + " DESC";
            }
        }
        return db.rawQuery(query, null);
    }

    private Cursor forPostCall(int is_roaming) {
        String query = "SELECT " +
                "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", "
                + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.CallDataEntry.COLUMN_TYPE + " as hash, "
                + DBContractor.CallDataEntry.COLUMN_CIRCLE_ID +
                " from " + DBContractor.CallDataEntry.TABLE_NAME +
                " where " +
                DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "= " + is_roaming + " and " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " > " + fortyfive_days_back +
                " group by " +
                ApiConstants.metadate + ", " +
                DBContractor.CallDataEntry.COLUMN_IS_NIGHT + ", " +
                DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + ", " +
                DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + ", " +
                DBContractor.CallDataEntry.COLUMN_TYPE +
                " order by " + ApiConstants.metadate + " DESC";
        return db.rawQuery(query, null);
    }

    private Cursor forPostSms(int is_roaming) {
        String query = "SELECT " +
                "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", " +
                DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME + "|| ','|| " + DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + "||','||" + DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + "||','||" + DBContractor.SmsDataEntry.COLUMN_TYPE + " as hash, " +
                DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID +
                " from " + DBContractor.SmsDataEntry.TABLE_NAME +
                " where " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=  " + is_roaming +
                " and " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " > " + fortyfive_days_back +
                " group by " +
                ApiConstants.metadate + ", " +
                DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + ", " +
                DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + ", " +
                DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + ", " +
                DBContractor.SmsDataEntry.COLUMN_TYPE +
                " order by " +
                ApiConstants.metadate + " DESC";
        return db.rawQuery(query, null);
    }


//    public Cursor getAppDataCursor() {
//        String query = null;
//        query = "SELECT " +
//                DBContractor.AppDataUsageEntry.COLUMN_APP_NAME + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_IS_BG + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_DATE + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_RECIEVED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_TRANSMITTED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_RECIEVED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_TRANSMITTED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_RECIEVED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_TRANSMITTED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_WIFI_DATA_RECIEVED + ", " +
//                DBContractor.AppDataUsageEntry.COLUMN_WIFI_DATA_TRANSMITTED +
//                " FROM " +
//                DBContractor.AppDataUsageEntry.TABLE_NAME +
//                " where " + DBContractor.AppDataUsageEntry.COLUMN_TIMESTAMP + " > " + start_of_today.getTime() + " order by (" +
//                DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_RECIEVED + "+" +
//                DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_TRANSMITTED + "+" +
//                DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_RECIEVED + "+" +
//                DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_TRANSMITTED + "+" +
//                DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_RECIEVED + "+" +
//                DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_TRANSMITTED +
//                ") DESC limit 20";
//        Log.e("app-query", query);
//        return db.rawQuery(query, null);
//    }

    //Other Utils
    public String getDeviceID() {
        String id = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        getPhoneNumber();
        return SDKUtils.getHashedDeviceID(id + phone_number);
//        return AppUtils.getDeviceID(mContext);
    }

    private String getPhoneNumber() {
        phone_number = shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER);
        return phone_number;
    }

    public Metadata getMetaDataObj(String date, long last_sync_date, int slot_id) {
        //setLastSyncTime();
        Metadata metadata = new Metadata();
        device_id = getDeviceID();
        shrd.setStringValue(Constants.DEVICE_ID, device_id);
        if (SDKUtils.isLoggedIn(mContext)) {
            ProxyLoginUser.SoResponseEntity user = new Gson().fromJson(shrd.getStringValue(Constants.USER_DATA), ProxyLoginUser.SoResponseEntity.class);
            metadata.setSso_id(user.getUserId());
        } else {
            metadata.setSso_id("tester0");
        }
        metadata.setAndroid_id(Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID));
        metadata.setDevice_id(device_id);
        if (last_sync_date == 0) {
            metadata.setLast_updated_date(simpleDateFormat.format(new Date(System.currentTimeMillis())));
            metadata.setLast_sync_timestamp(simpleDateFormat.format(new Date(System.currentTimeMillis())));
        } else {
            metadata.setLast_updated_date(simpleDateFormat.format(new Date(last_sync_date)));
            metadata.setLast_sync_timestamp(simpleDateFormat.format(new Date(last_sync_date)));
        }
        metadata.setDate(date);
        metadata.setOnboardMobile(shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
        metadata.setOnboardCircle(shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
        metadata.setOnboardOperator(shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
        metadata.setOnboardSimType(shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE));
        if (slot_id==2){
            metadata.setOnboardMobile(shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER_2));
            metadata.setOnboardCircle(shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID_2));
            metadata.setOnboardOperator(shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID_2));
            metadata.setOnboardSimType(shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE_2));
        }
        metadata.setOsVersion(Build.VERSION.RELEASE);
        metadata.setDeviceName(Build.BRAND + "-" + Build.DEVICE);
        return metadata;
    }

    private void setLastSyncTime(int sim_id) {
        long callsms_last_timestamp = SDKUtils.getLastDataTypeEntry(db, ApiConstants.CALLSMS_DATA_TYPE,sim_id);
        if (callsms_last_timestamp != 0) {
            if (DateUtil.isToday(new Date(callsms_last_timestamp))) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                callsmsbulk_last_sync_timestamp = calendar.getTimeInMillis();
            } else {
                callsmsbulk_last_sync_timestamp = callsms_last_timestamp;
            }
            call_last_date = 1;
            last_date = new Date(callsms_last_timestamp);
        } else {
            call_last_date = 0;
            last_date = new Date(Calendar.getInstance().getTimeInMillis());
        }
        databulk_last_sync_date = SDKUtils.getLastDataTypeEntry(db, ApiConstants.INTERNET_DATA_TYPE,0);
        operatorsms_last_sync_timestamp = SDKUtils.getLastDataTypeEntry(db, ApiConstants.OPSMS_DATA_TYPE,0);
    }


    public void saveSyncDetailInDB(String s, int data_type, int send_type,int sim_id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContractor.SendToServerEntry.COLUMN_DATA_DATE, s);
        contentValues.put(DBContractor.SendToServerEntry.COLUMN_JSON_TYPE, data_type);
        contentValues.put(DBContractor.SendToServerEntry.COLUMN_TIMESTAMP, System.currentTimeMillis());
        contentValues.put(DBContractor.SendToServerEntry.COLUMN_SEND_TYPE, send_type);
        contentValues.put(DBContractor.SendToServerEntry.COLUMN_SIM_ID,sim_id);
        try {
            long id = db.insert(DBContractor.SendToServerEntry.TABLE_NAME, null, contentValues);
//            Log.e("data_inserted"+data_type,id+"");
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
    }

    public String giveTodayDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date d = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(d);
    }

//    public Map<String, Float> getEmptyDataUsageMap() {
//        Map<String, Float> emptyMap = new HashMap<>();
//        emptyMap.put(ApiConstants.total_data, 0f);
//        emptyMap.put(ApiConstants.two_g_data, 0f);
//        emptyMap.put(ApiConstants.three_g_data, 0f);
//        emptyMap.put(ApiConstants.four_g_data, 0f);
//        emptyMap.put(ApiConstants.wifi_data, 0f);
//        emptyMap.put(ApiConstants.total_network, 0f);
//        return emptyMap;
//    }

    public String formNetworkData(String networkpref) throws JSONException {

        JsonObject networkObj = new JsonObject();
        JsonObject metaJson = new JsonObject();
        metaJson.addProperty(ApiConstants.last_sync_timestamp, System.currentTimeMillis());
        metaJson.addProperty(ApiConstants.device_id, device_id);
        metaJson.addProperty(ApiConstants.sso_id, ssoId);
        metaJson.addProperty(ApiConstants.metadate, System.currentTimeMillis());
        metaJson.addProperty(ApiConstants.last_updated_date, simpleDateFormat.format(new Date(databulk_last_sync_date)));
        metaJson.addProperty("onboardMobile", shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
        metaJson.addProperty("onboardCircle", shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
        metaJson.addProperty("onboardOperator", shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
        metaJson.addProperty("onboardSimType", shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE));
        metaJson.addProperty("osVersion", Build.VERSION.RELEASE);
        metaJson.addProperty("deviceName", Build.MANUFACTURER + "-" + Build.DEVICE);
        DataModel dm = SDKUtils.extrapoleteForDays(networkpref);
        if (dm != null) {
            networkObj.add(ApiConstants.metadataObj, metaJson);
//            networkObj.addProperty(ApiConstants.total_data, 0);
            networkObj.addProperty(ApiConstants.total_data, dm.getTotal());
            networkObj.addProperty(ApiConstants.two_g_data, dm.getTwoG());
            networkObj.addProperty(ApiConstants.three_g_data, dm.getThreeG());
            networkObj.addProperty(ApiConstants.four_g_data, dm.getFourG());
            networkObj.addProperty(ApiConstants.total_network, dm.getThreeG() + dm.getTwoG()+dm.getFourG());
            networkObj.addProperty(ApiConstants.dayCount, ApiConstants.EXTRAPOLATION_DAYS);
            return new Gson().toJson(networkObj);
        } else {
            return null;
        }
    }


    public String formCallSmsData(int is_roaming,int sim_id) {
        JsonObject callsmsObj = new JsonObject();
        callHashMap = setAndGetCallHashMap(ApiConstants.NON_ROAMING, true,sim_id);
        smsHashMap = setAndGetSmsHashMap(ApiConstants.NON_ROAMING, true,sim_id);
        int daycount = callHashMap.size();
        Iterator smsit = smsHashMap.entrySet().iterator();
        HashMap<String, Integer> allsms = new HashMap<>();
        Iterator callit = callHashMap.entrySet().iterator();
        HashMap<String, Map<String, Integer>> allcall = new HashMap<>();
        List<Map<String, Map<String, Integer>>> calllist = new ArrayList<>();
        List<Map<String, Integer>> smsList = new ArrayList<>();
        try {
            for (int i = 0; i < daycount && smsit.hasNext(); i++) {
                HashMap.Entry pair = (HashMap.Entry) smsit.next();
                smsList.add(smsHashMap.get(pair.getKey()));
                smsit.remove();
            }
            for (int j = 0; j < daycount && callit.hasNext(); j++) {
                HashMap.Entry pair = (HashMap.Entry) callit.next();
                calllist.add(callHashMap.get(pair.getKey()));
                callit.remove();
            }

            for (Map<String, Integer> map : smsList) {
                for (String x : map.keySet()) {
                    if (allsms.containsKey(x)) {
                        allsms.put(x, allsms.get(x) + map.get(x));
                    } else {
                        allsms.put(x, map.get(x));
                    }
                }
            }
            for (Map<String, Map<String, Integer>> calmap : calllist) {
                for (String y : calmap.keySet()) {
                    if (allcall.containsKey(y)) {
                        Map<String, Integer> k1 = allcall.get(y);
                        Map<String, Integer> k2 = calmap.get(y);
                        Map<String, Integer> k3 = new HashMap<>();
                        k3.put("count", k1.get("count") + k2.get("count"));
                        k3.put("mins", k1.get("mins") + k2.get("mins"));
                        k3.put("secs", k1.get("secs") + k2.get("secs"));
                        allcall.put(y, k3);
                    } else {
                        allcall.put(y, calmap.get(y));
                    }
                }

            }

        } catch (NoSuchElementException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        JsonObject metaJson = new JsonObject();
        metaJson.addProperty(ApiConstants.last_sync_timestamp, System.currentTimeMillis());
        metaJson.addProperty(ApiConstants.device_id, device_id);
        metaJson.addProperty(ApiConstants.sso_id, ssoId);
        metaJson.addProperty(ApiConstants.metadate, System.currentTimeMillis());
        metaJson.addProperty(ApiConstants.last_updated_date, simpleDateFormat.format(new Date(System.currentTimeMillis())));
        metaJson.addProperty("onboardMobile", shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
        metaJson.addProperty("onboardCircle", shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
        metaJson.addProperty("onboardOperator", shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
        metaJson.addProperty("osVersion", Build.VERSION.RELEASE);
        metaJson.addProperty("deviceName", Build.MANUFACTURER + "-" + Build.DEVICE);
        Iterator itcal = allcall.entrySet().iterator();
        Iterator itsms = allsms.entrySet().iterator();
        while (itcal.hasNext()) {
            Map.Entry pair = (Map.Entry) itcal.next();
            String map_value = new GsonBuilder().create().toJson(pair.getValue());
            callsmsObj.add((String) pair.getKey(), new JsonParser().parse(map_value));
            itcal.remove();
        }
        while (itsms.hasNext()) {
            Map.Entry pair = (Map.Entry) itsms.next();
            String map_value = new GsonBuilder().create().toJson(pair.getValue());
            callsmsObj.add((String) pair.getKey(), new JsonParser().parse(map_value));
            itsms.remove();
        }
        callsmsObj.addProperty(ApiConstants.dayCount, daycount);
        callsmsObj.add(ApiConstants.metadataObj, metaJson);
        return new Gson().toJson(callsmsObj);

    }

    //Bulk Roaming Call Usage + Utils
    public HashMap<String, Map<String, Map<String, Map<String, Integer>>>> setAndGetBulkRoamingCallHashMap(int data_type,int sim_id) {
        HashMap<String, Map<String, Map<String, Integer>>> roamingCallHashMap = new LinkedHashMap<>();
        Cursor callCursor = getCallDataCursor(data_type, ApiConstants.ROAMING,sim_id);
        Map<String, Map<String, Integer>> callMapObj = new LinkedHashMap<>();
        int count = 0;
        int mins = 0;
        int secs = 0;
        String initialCircleId = "";
        try {
            if (callCursor != null && callCursor.getCount() > 0 && callCursor.moveToFirst()) {
                do {
                    String date = callCursor.getString(callCursor.getColumnIndex(ApiConstants.metadate));
                    count = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    mins = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_MIN));
                    secs = callCursor.getInt(callCursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_SEC));
                    String hash = callCursor.getString(callCursor.getColumnIndex(ApiConstants.COLUMN_HASH));
                    String circle_Id = callCursor.getString(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_CIRCLE_ID));
                    if (initialCircleId != null && !initialCircleId.equals(circle_Id) && hash != null) {
                        callMapObj = processRoamingCall(hash, count, mins, secs);
                        circle_Id = circle_Id == null ? "0" : circle_Id;
                        initialCircleId = circle_Id;
                        if (!initialCircleId.equals("")) {
                            roamingCallHashMap.put(initialCircleId, callMapObj);
                        }
                    } else {
                        assert callMapObj != null;
                        if (hash != null && hash != "") {
//                            Log.e("CALL-HASH", hash);
                            callMapObj = processSameCircleCall(hash, count, mins, secs, callMapObj);
                        }
                    }
                    roamingBulkCallHashMap.put(date, roamingCallHashMap);
                } while (callCursor.moveToNext());
            }
        }catch (Exception e){}finally {
            if (callCursor != null) {
                callCursor.close();
            }
        }
        return roamingBulkCallHashMap;
    }

    //Bulk Roaming Data Usage
//    public HashMap<String, Map<String, Map<String, Float>>> setAndGetBulkRoamingDataHashMap(int data_type) {
//        HashMap<String, Map<String, Float>> roamingDataHashMap = new LinkedHashMap<>();
//        String currdate = null;
//        Cursor roamingDataCursor = getDataCursor(data_type, ApiConstants.ROAMING);
//        Map<String, Float> roamingDataMap = new LinkedHashMap<>();
//        long _2G = 0;
//        long _3G = 0;
//        long _4G = 0;
//        long total_wifi = 0;
//        String initialCircleId = "";
//        try {
//            if (roamingDataCursor != null && roamingDataCursor.moveToFirst()) {
//                do {
//                    String date = roamingDataCursor.getString(roamingDataCursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_DATE));
//                    String old_type = roamingDataCursor.getString(roamingDataCursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_OLDTYPE));
//                    long session_data = roamingDataCursor.getLong(roamingDataCursor.getColumnIndex(DataStorageConstants.TOTAL_SESSION_BYTE));
//                    String circle_Id = roamingDataCursor.getString(roamingDataCursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_CIRCLE_ID));
//                    if (initialCircleId != null && !initialCircleId.equals(circle_Id)) {
//                        if (!initialCircleId.equals("")) {
//                            float mod_wifi = (total_wifi / 1024f);
//                            float mod_3G_data = (_3G / 1024f);
//                            float mod_2G_data = (_2G / 1024f);
//                            float mod_4G_data = (_4G / 1024f);
//                            float total = mod_3G_data + mod_2G_data + mod_wifi;
//                            float total_network = mod_3G_data + mod_2G_data + mod_4G_data;
//                            roamingDataMap.put(ApiConstants.total_data, total);
//                            roamingDataMap.put(ApiConstants.two_g_data, mod_2G_data);
//                            roamingDataMap.put(ApiConstants.three_g_data, mod_3G_data);
//                            roamingDataMap.put(ApiConstants.four_g_data, mod_4G_data);
//                            roamingDataMap.put(ApiConstants.wifi_data, mod_wifi);
//                            roamingDataMap.put(ApiConstants.total_network, total_network);
//                            roamingDataHashMap.put(initialCircleId, roamingDataMap);
//                            _2G = 0;
//                            _3G = 0;
//                            total_wifi = 0;
//                        } else {
//                            if (old_type != null && old_type.equals(DataStorageConstants.THREEG_CONN)) {
//                                _3G = session_data;
//                            } else if (old_type != null && old_type.equals(DataStorageConstants.TWOG_CONN)) {
//                                _2G = session_data;
//                            } else {
//                                total_wifi = session_data;
//                            }
//                        }
//                        initialCircleId = circle_Id;
//                    } else {
//                        if (old_type != null && old_type.equals(DataStorageConstants.THREEG_CONN)) {
//                            _3G += session_data;
//                        } else if (old_type != null && old_type.equals(DataStorageConstants.TWOG_CONN)) {
//                            _2G += session_data;
//                        } else {
//                            total_wifi += session_data;
//                        }
//                    }
//
//                    if (roamingDataHashMap.size() > 0) {
//                        roamingBulkDataHashMap.put(date, roamingDataHashMap);
//                    }
//                    currdate = date;
//                } while (roamingDataCursor.moveToNext());
//            }
//        } finally {
//            float mod_wifi = (float) (total_wifi / 1024f);
//            float mod_3G_data = (float) (_3G / 1024f);
//            float mod_2G_data = (float) (_2G / 1024f);
//            float mod_4G_data = (float) (_4G / 1024f);
//            float total = mod_3G_data + mod_2G_data + mod_wifi;
//            float total_network = mod_3G_data + mod_2G_data + mod_4G_data;
//            roamingDataMap.put(ApiConstants.total_data, total);
//            roamingDataMap.put(ApiConstants.two_g_data, mod_2G_data);
//            roamingDataMap.put(ApiConstants.three_g_data, mod_3G_data);
//            roamingDataMap.put(ApiConstants.four_g_data, mod_4G_data);
//            roamingDataMap.put(ApiConstants.wifi_data, mod_wifi);
//            roamingDataMap.put(ApiConstants.total_network, total_network);
//            if (initialCircleId != null && initialCircleId != "") {
//                roamingDataHashMap.put(initialCircleId, roamingDataMap);
//            }
//            if (roamingDataCursor != null) {
//                roamingDataCursor.close();
//            }
//            if (roamingDataHashMap.size() > 0) {
//                roamingBulkDataHashMap.put(currdate, roamingDataHashMap);
//            }
//        }
//        return roamingBulkDataHashMap;
//    }

    //Roaming SMS Usage + Utils
    public HashMap<String, Map<String, Map<String, Integer>>> setAndGetBulkRoamingSmsHashMap(int data_type,int sim_id) {
        HashMap<String, Map<String, Integer>> roamingSmsHashMap = new LinkedHashMap<>();
        Cursor smsCursor = getSmsCursor(data_type, ApiConstants.ROAMING,sim_id);
        String initialCircleId = "";
        Map<String, Integer> smsMapObj = new LinkedHashMap<String, Integer>();
        try {
            if (smsCursor != null && smsCursor.moveToFirst()) {
                do {
                    String date = smsCursor.getString(smsCursor.getColumnIndex(ApiConstants.metadate));
                    String hash = smsCursor.getString(smsCursor.getColumnIndex(ApiConstants.COLUMN_HASH));
                    int count = smsCursor.getInt(smsCursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    String circle_Id = smsCursor.getString(smsCursor.getColumnIndex(DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID));
                    if (initialCircleId != null && !initialCircleId.equals(circle_Id) && hash != null) {
                        smsMapObj = processRoamingSMS(hash, count);
                        initialCircleId = circle_Id;
                        if (!initialCircleId.equals("")) {
                            roamingSmsHashMap.put(circle_Id, smsMapObj);
                        }
                    } else {
                        assert smsMapObj != null;
                        if (hash != null && hash != "") {
                            smsMapObj = processSameCircleSMS(hash, count, smsMapObj);
                        }
                    }
                    roamingBulkSmsHashMap.put(date, roamingSmsHashMap);
                } while (smsCursor.moveToNext());
            }
        }catch (Exception e){}finally {
            if (smsCursor != null) {
                smsCursor.close();
            }
        }
        return roamingBulkSmsHashMap;
    }

    public JSONArray setandGetOperatorSms(int data_type) throws JSONException {

        Cursor operatorSms = null;
        try {
            String initialDate = "";
            operatorSms = getOperatorSmsCursor(data_type);
            if (operatorsms_last_sync_timestamp == 0) {
                operatorsms_last_sync_timestamp = Calendar.getInstance().getTimeInMillis();
            }

            if (operatorSms != null && operatorSms.moveToFirst()) {
                do {
                    JSONObject operatorObj = new JSONObject();
                    JSONObject metaJson = new JSONObject();
                    String body = operatorSms.getString(operatorSms.getColumnIndex(DBContractor.SmsInboxEntry.COLUMN_MSG_BODY));
                    String date = operatorSms.getString(operatorSms.getColumnIndex(ApiConstants.metadate));
                    int sim_id = operatorSms.getInt(operatorSms.getColumnIndex(DBContractor.SmsInboxEntry.COLUMN_SIM_ID));
                    long timestamp = operatorSms.getLong(operatorSms.getColumnIndex(DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP));
                    String address = operatorSms.getString(operatorSms.getColumnIndex(DBContractor.SmsInboxEntry.COLUMN_ADDRESS));
                    String number = SDKUtils.getOnboardedNumFromSimId(shrd, sim_id);
                    metaJson.put(ApiConstants.last_sync_timestamp, simpleDateFormat.format(new Date(operatorsms_last_sync_timestamp)));
                    metaJson.put(ApiConstants.device_id, device_id);
                    metaJson.put(ApiConstants.sso_id, ssoId);
                    metaJson.put(ApiConstants.metadate, date);
                    metaJson.put(ApiConstants.last_updated_date, simpleDateFormat.format(new Date(databulk_last_sync_date)));
                    metaJson.put("onboardMobile", shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
                    metaJson.put("onboardCircle", shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
                    metaJson.put("onboardOperator", shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
                    metaJson.put("onboardSimType", shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE));
                    metaJson.put("osVersion", Build.VERSION.RELEASE);
                    metaJson.put("deviceName", Build.MANUFACTURER + "-" + Build.DEVICE);
                    operatorObj.put(ApiConstants.metadataObj, metaJson);
                    operatorObj.put(ApiConstants.smsfrom, address);
                    operatorObj.put(ApiConstants.smsto, number);
                    operatorObj.put(ApiConstants.message, body);
                    operatorObj.put(ApiConstants.type, "sms");
                    operatorObj.put(ApiConstants.timestamp, simpleDateFormat.format(new Date(timestamp)));
                    operatorSmsArray.put(operatorObj);
                    //initialDate = date;
                } while (operatorSms.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }catch (Exception e){}finally {
            if (operatorSms != null) {
                operatorSms.close();
            }
        }
        return operatorSmsArray;
    }

    private Cursor getOperatorSmsCursor(int data_type) {
        String query = null;
        if (data_type == ApiConstants.DAILY_DATA_TYPE) {
            query = "SELECT " +
                    "strftime('%Y-%m-%d', " + DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", " +
                    DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + ", " +
                    DBContractor.SmsInboxEntry.COLUMN_ADDRESS + ", " +
                    DBContractor.SmsInboxEntry.COLUMN_SIM_ID + ", " +
                    DBContractor.SmsInboxEntry.COLUMN_MSG_BODY +
                    " from " + DBContractor.SmsInboxEntry.TABLE_NAME +
                    " where "+ DBContractor.SmsInboxEntry.COLUMN_SYNCED+" <> 1"+
                    " order by " +
                    ApiConstants.metadate + " DESC";
//            Log.e("smsInbox-daily", query);
        } else {
            if (operatorsms_last_sync_timestamp == 0) {
                query = "SELECT " +
                        "strftime('%Y-%m-%d', " + DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " / 1000, 'unixepoch') as " +
                        ApiConstants.metadate + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_ADDRESS + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_SIM_ID + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_MSG_BODY +
                        " from " + DBContractor.SmsInboxEntry.TABLE_NAME +
                        " where "+ DBContractor.SmsInboxEntry.COLUMN_SYNCED+" <> 1"+
                        " order by " +
                        ApiConstants.metadate + " DESC";
            } else {
                query = "SELECT " +
                        "strftime('%Y-%m-%d', " + DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " / 1000, 'unixepoch') as " + ApiConstants.metadate + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_ADDRESS + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_SIM_ID + ", " +
                        DBContractor.SmsInboxEntry.COLUMN_MSG_BODY +
                        " from " + DBContractor.SmsInboxEntry.TABLE_NAME +
                        " where "+ DBContractor.SmsInboxEntry.COLUMN_SYNCED+" <> 1"+
                        " order by " +
                        ApiConstants.metadate + " DESC";
            }
//            Log.e("smsInbox-bulk", query);
        }
        return db.rawQuery(query, null);

    }


//    public HashMap<String, JSONObject> makeLocalDataMap() throws JSONException {
//        JSONObject dataObj = new JSONObject();
//        JSONObject metaJson = new JSONObject();
//        String initialDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date(System.currentTimeMillis()));
//        long boot_time = SystemClock.elapsedRealtime();
//        long _2G = 0, _3G = 0, _4G = 0, total_wifi = 0;
//        long mobile_data = TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes();
//        long day_data = (mobile_data * 24 * 3600 * 1000) / (boot_time);
//        long totoal = TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes();
//        total_wifi = totoal - mobile_data;
//        String pref = shrd.getStringValue(Constants.ON_BOARDING_PREFERRED_TYPE);
//        switch (pref.toLowerCase()) {
//            case "2g":
//                _2G = day_data;
//                break;
//            case "3g":
//                _3G = day_data;
//                break;
//            case "4g":
//                _4G = day_data;
//                break;
//            default:
//                _3G = day_data;
//                break;
//        }
//        float mod_wifi = (float) (total_wifi / 1024f);
//        float mod_3G_data = (float) (_3G / 1024f);
//        float mod_2G_data = (float) (_2G / 1024f);
//        float mod_4G_data = (float) (_4G / 1024f);
//        float total = mod_3G_data + mod_2G_data + mod_wifi;
//        float total_network = mod_3G_data + mod_2G_data + mod_4G_data;
//        metaJson.put(ApiConstants.last_sync_timestamp, last_sync_date);
//        metaJson.put(ApiConstants.device_id, device_id);
//        metaJson.put(ApiConstants.sso_id, ssoId);
//        metaJson.put(ApiConstants.metadate, initialDate);
//        metaJson.put(ApiConstants.last_updated_date, last_sync_date);
//        metaJson.put("onboardMobile", shrd.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER));
//        metaJson.put("onboardCircle", shrd.getIntValue(Constants.ON_BOARDING_CIRCLE_ID));
//        metaJson.put("onboardOperator", shrd.getIntValue(Constants.ON_BOARDING_PROVIDER_ID));
//        metaJson.put("osVersion", Build.VERSION.RELEASE);
//        metaJson.put("deviceName", Build.MANUFACTURER + "-" + Build.DEVICE);
//        dataObj.put(metadataObj, metaJson);
//        dataObj.put(ApiConstants.total_data, total);
//        dataObj.put(ApiConstants.two_g_data, mod_2G_data);
//        dataObj.put(ApiConstants.three_g_data, mod_3G_data);
//        dataObj.put(ApiConstants.four_g_data, mod_4G_data);
//        dataObj.put(ApiConstants.wifi_data, mod_wifi);
//        dataObj.put(ApiConstants.total_network, total_network);
//        dataHashMap.put(initialDate, dataObj);
//        return dataHashMap;
//    }
}