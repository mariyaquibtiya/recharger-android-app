package com.getsmartapp.lib.dataAggregation;

/**
 * @author  nitesh on 17/6/15.
 */
public class Metadata {
    private String last_sync_timestamp;
    private String device_id;
    private String android_id;
    private String sso_id;
    private String date;
    private String last_updated_date;
    private String onboardMobile;
    private Integer onboardCircle;
    private Integer onboardOperator;
    private String onboardSimType;
    private String deviceName;
    private String osVersion;
    private String parent_package_name;

    public Metadata(){

    }

    public void setAndroid_id(String android_id) {
        this.android_id = android_id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getOnboardSimType() {
        return onboardSimType;
    }

    public void setOnboardSimType(String onboardSimType) {
        this.onboardSimType = onboardSimType;
    }

    public void setLast_sync_timestamp(String last_sync_timestamp) {
        this.last_sync_timestamp = last_sync_timestamp;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setSso_id(String sso_id) {
        this.sso_id = sso_id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setLast_updated_date(String last_updated_date) {
        this.last_updated_date = last_updated_date;
    }

    public void setOnboardOperator(Integer onboardOperator) {
        this.onboardOperator = onboardOperator;
    }

    public void setOnboardMobile(String onboardMobile) {
        this.onboardMobile = onboardMobile;
    }

    public void setOnboardCircle(Integer onboardCircle) {
        this.onboardCircle = onboardCircle;
    }

    public String getParent_package_name() {
        return parent_package_name;
    }

    public void setParent_package_name(String parent_package_name) {
        this.parent_package_name = parent_package_name;
    }
}
