package com.getsmartapp.lib.services;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.getsmartapp.lib.dataAggregation.DataAggregationUtils;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.retrofit.DataAggRestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.lib.utils.SDKUtils;
import com.getsmartapp.lib.utils.SdkLg;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author  nitesh on 18/6/15.
 */
public class DailyAggregationService extends IntentService {

    public HashMap<String, JSONObject> dataHashMap = new HashMap<>();
    public JSONArray operatorSmsHashMap = new JSONArray();
    protected HashMap<String, Map<String, Map<String, Integer>>> callHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Integer>> smsHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> commonCallSmsObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> callObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> smsObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> bulkHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Float>> roamingDataHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Map<String, Integer>>> roamingCallHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Integer>> roamingSmsHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> commonRoamingObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Object> roamingBulkHashMap = new LinkedHashMap<>();
    protected Map<String, Integer> tempSmsMap = new LinkedHashMap<>();
    protected Map<String, Map<String, Integer>> tempCallMap = new LinkedHashMap<>();
    protected Map<String, Object> appHashMap = new LinkedHashMap<>();
    private DataAggregationUtils dataAggregationUtils;
    private long dailysend_call_type, dailysend_data_type, dailysend_roaming_type, dailysend_appdata_type, dailysend_operator_type;
    private long lastsendTimeCall = 0, lastsendTimeData = 0, lastsendTimeApp = 0, lastsendTimeRoaming = 0;
    private DualSimManager simManager;
    private SharedPrefManager shrd;
    private int sim1_id = 0, sim2_id = 0;
    private int sendingforsimid = 0;

    public DailyAggregationService() {
        super("DailyAggregationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        dataAggregationUtils = new DataAggregationUtils(this, 0);
        simManager = new DualSimManager(this);
        shrd = new SharedPrefManager(this);
        if (simManager.isHasDualSim()) {
            sim1_id = shrd.getIntValue(Constants.SIM1_serial);
            sim2_id = shrd.getIntValue(Constants.SIM2_serial);
            if (sim1_id != 0) {
                dataAggregationUtils = new DataAggregationUtils(this, sim1_id);
            }
            if (sim2_id != 0) {
                dataAggregationUtils = new DataAggregationUtils(this, sim2_id);
            }
        }
        SQLiteDatabase sqLiteDatabase = SdkDbHelper.getInstance(this).getWritableDatabase();
        try {
            shrd.setBooleanValue(Constants.SENDING_DATA, true);
            if (simManager.isHasDualSim()) {
                if (sim1_id == 0 && sim2_id == 0) {
                    sendSingleSimData(sqLiteDatabase);
                } else {
                    if (sim1_id != 0) {
                        sendingforsimid = sim1_id;
                        dailysend_call_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase,ApiConstants.CALLSMS_DATA_TYPE, sim1_id);
                        dailysend_data_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.INTERNET_DATA_TYPE, sim1_id);
                        dailysend_roaming_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase,  ApiConstants.ROAMING_DATA_TYPE, sim1_id);
                        dailysend_appdata_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.APP_DATA_TYPE, sim1_id);
                        dailysend_operator_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.OPSMS_DATA_TYPE, sim1_id);
                        //Non-Roaming Usage
                        callSmsData(sim1_id, shrd.getIntValue(Constants.SLOT1));
                        roamingData(sim1_id, shrd.getIntValue(Constants.SLOT1));
                    }
                    if (sim2_id != 0) {
                        sendingforsimid = sim2_id;
                        dailysend_call_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.CALLSMS_DATA_TYPE, sim2_id);
                        dailysend_data_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.INTERNET_DATA_TYPE, sim2_id);
                        dailysend_roaming_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.ROAMING_DATA_TYPE, sim2_id);
                        dailysend_appdata_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.APP_DATA_TYPE, sim2_id);
                        dailysend_operator_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.OPSMS_DATA_TYPE, sim2_id);
                        //Non-Roaming Usage
                        callSmsData(sim2_id, shrd.getIntValue(Constants.SLOT2));
                        roamingData(sim2_id, shrd.getIntValue(Constants.SLOT2));
                    }
                }
            } else {
                sendSingleSimData(sqLiteDatabase);
            }
            appData();
            internetData();
            operatorSmsData();
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendSingleSimData(SQLiteDatabase sqLiteDatabase) throws IOException {
        dailysend_call_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.CALLSMS_DATA_TYPE, 0);
        dailysend_data_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.INTERNET_DATA_TYPE, 0);
        dailysend_roaming_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.ROAMING_DATA_TYPE, 0);
        dailysend_appdata_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.APP_DATA_TYPE, 0);
        dailysend_operator_type = SDKUtils.getLastDataTypeEntry(sqLiteDatabase,ApiConstants.OPSMS_DATA_TYPE, 0);
        //Non-Roaming Usage
        callSmsData(0, 1);
        roamingData(0, 1);
    }

    private void operatorSmsData() throws JSONException {
        if (dailysend_operator_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_data_type))) {
                if (DateUtil.sixHourBefore(dailysend_data_type)) {
                    operatorSmsHashMap = dataAggregationUtils.setandGetOperatorSms(ApiConstants.DAILY_DATA_TYPE);
                    // Sending the internet Data to server--------------------------------------------------------------//
                    sendOperatorSms();
                }
            }
        } else {
            operatorSmsHashMap = dataAggregationUtils.setandGetOperatorSms(ApiConstants.DAILY_DATA_TYPE);
            // Sending the internet Data to server--------------------------------------------------------------//
            sendOperatorSms();
        }
    }

    private void sendOperatorSms() {
        if (operatorSmsHashMap.length() > 0) {
            try {
                final JSONObject obj = (JSONObject) operatorSmsHashMap.get(0);
                DataAggRestClient restClient = new DataAggRestClient();
                restClient.getApiService().sendDailyData(String.valueOf(obj), ApiConstants.OPSMS_DATA_TYPE, new Callback<HashMap<String, Object>>() {
                    @Override
                    public void success(HashMap dataAggregationParser, Response response) {
                        if (dataAggregationParser != null) {
                            lastsendTimeData = System.currentTimeMillis();
                            String giveTodayDate = dataAggregationUtils.giveTodayDate();
                            dataAggregationUtils.saveSyncDetailInDB(String.valueOf(giveTodayDate), ApiConstants.INTERNET_DATA_TYPE, ApiConstants.DAILY_DATA_TYPE, 0);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error.getCause() != null)
                            SdkLg.e("retrofit failed : -" + error.getCause().toString());
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        shrd.setBooleanValue(Constants.SENDING_DATA,false);
    }

    private void roamingData(int sim_id, int slot_id) throws IOException {
        if (dailysend_roaming_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_roaming_type))) {
                if (DateUtil.sixHourBefore(dailysend_roaming_type)) {
                    //Roaming Usages
                    //roamingDataHashMap = dataAggregationUtils.setAndGetRoamingDataHashMap(ApiConstants.DAILY_DATA_TYPE);
                    roamingSmsHashMap = dataAggregationUtils.setAndGetRoamingSmsHashMap(ApiConstants.DAILY_DATA_TYPE, sim_id);
                    roamingCallHashMap = dataAggregationUtils.setAndGetRoamingCallHashMap(ApiConstants.DAILY_DATA_TYPE, sim_id);

                    //Extracting common roaming SMS, Call & Data Circle to form one Object--------------------------------------//
                    makeCommonRoamingObjHashMap();

                    iterateRoamingCallandSms();

                    roamingBulkHashMap.put("metadata", dataAggregationUtils.getMetaDataObj(dataAggregationUtils.giveTodayDate(), dailysend_roaming_type, 1));
                    roamingBulkHashMap.put("circle", commonRoamingObjHashMap);

                    //Sending roaming data to server
                    sendRoamingData();
                }
            }
        } else {
            //Roaming Usages
            //roamingDataHashMap = dataAggregationUtils.setAndGetRoamingDataHashMap(ApiConstants.DAILY_DATA_TYPE);
            roamingSmsHashMap = dataAggregationUtils.setAndGetRoamingSmsHashMap(ApiConstants.DAILY_DATA_TYPE, sim_id);
            roamingCallHashMap = dataAggregationUtils.setAndGetRoamingCallHashMap(ApiConstants.DAILY_DATA_TYPE, sim_id);

            //Extracting common roaming SMS, Call & Data Circle to form one Object--------------------------------------//
            makeCommonRoamingObjHashMap();

            iterateRoamingCallandSms();

            roamingBulkHashMap.put("metadata", dataAggregationUtils.getMetaDataObj(dataAggregationUtils.giveTodayDate(), dailysend_roaming_type, slot_id));
            roamingBulkHashMap.put("circle", commonRoamingObjHashMap);

            //Sending roaming data to server
            sendRoamingData();

        }
    }

    private void internetData() throws JSONException, IOException {
        if (dailysend_data_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_data_type))) {
                if (DateUtil.sixHourBefore(dailysend_data_type)) {
                    dataHashMap = dataAggregationUtils.setAndGetDataHashMap(ApiConstants.DAILY_DATA_TYPE,lastsendTimeData);
                    // Sending the internet Data to server--------------------------------------------------------------//
                    sendInternetData();
                }
            }
        } else {
            dataHashMap = dataAggregationUtils.setAndGetDataHashMap(ApiConstants.DAILY_DATA_TYPE,lastsendTimeData);
            // Sending the internet Data to server--------------------------------------------------------------//
            sendInternetData();
        }
    }

    private void appData() throws IOException {
        if (dailysend_appdata_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_appdata_type))) {
                if (DateUtil.sixHourBefore(dailysend_appdata_type)) {
                    appHashMap.put("metadata", dataAggregationUtils.getMetaDataObj(dataAggregationUtils.giveTodayDate(), dailysend_appdata_type, 1));
                    appHashMap.put("app", dataAggregationUtils.setAndGetAppData());
                    //Sending App data to server
                    sendAppData();
                }

            }
        } else {
            appHashMap.put("metadata", dataAggregationUtils.getMetaDataObj(dataAggregationUtils.giveTodayDate(), dailysend_appdata_type, 1));
            appHashMap.put("app", dataAggregationUtils.setAndGetAppData());
            //Sending App data to server
            sendAppData();
        }
    }

    private void callSmsData(int sim_id, int slot_id) throws IOException {
        if (dailysend_call_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_call_type))) {
                if (DateUtil.sixHourBefore(dailysend_call_type)) {
                    smsHashMap.clear();
                    callHashMap.clear();
                    smsHashMap = dataAggregationUtils.setAndGetSmsHashMap(ApiConstants.DAILY_DATA_TYPE, false, sim_id);
                    callHashMap = dataAggregationUtils.setAndGetCallHashMap(ApiConstants.DAILY_DATA_TYPE, false, sim_id);

                    //Extracting common SMS and Call Date to form one Object--------------------------------------//
                    makeCommonCallSmsObjHashMap(slot_id);

                    //Making Call-Usage HashMap
                    makeCallObjHashMap(slot_id);

                    //Making SMS-Usage HashMap
                    makeSmsObjHashMap(slot_id);

                    bulkHashMap.putAll(callObjHashMap);
                    bulkHashMap.putAll(smsObjHashMap);
                    bulkHashMap.putAll(commonCallSmsObjHashMap);

                    //Sending CallSms Usages data to server ----------------------------------------------------------//
                    sendCallSmsData();
                }
            }
        } else {
            smsHashMap.clear();
            callHashMap.clear();
            smsHashMap = dataAggregationUtils.setAndGetSmsHashMap(ApiConstants.DAILY_DATA_TYPE, false, sim_id);
            callHashMap = dataAggregationUtils.setAndGetCallHashMap(ApiConstants.DAILY_DATA_TYPE, false, sim_id);

            //Extracting common SMS and Call Date to form one Object--------------------------------------//
            makeCommonCallSmsObjHashMap(slot_id);

            //Making Call-Usage HashMap
            makeCallObjHashMap(slot_id);

            //Making SMS-Usage HashMap
            makeSmsObjHashMap(slot_id);

            bulkHashMap.putAll(callObjHashMap);
            bulkHashMap.putAll(smsObjHashMap);
            bulkHashMap.putAll(commonCallSmsObjHashMap);

            //Sending CallSms Usages data to server ----------------------------------------------------------//
            sendCallSmsData();
        }
    }

    private boolean isSendingAppData = false;

    private void sendAppData() throws IOException {
        if (lastsendTimeApp != 0 && !isSendingAppData) {
            if (DateUtil.sixHourBefore(lastsendTimeApp))
                sendAppDailyData();
        } else if (!isSendingAppData) {
            sendAppDailyData();
        }

    }

    private void sendAppDailyData() {

        if (appHashMap != null && appHashMap.size() != 0) {
            //List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            final String data = new Gson().toJson(appHashMap);
            DataAggRestClient restClient = new DataAggRestClient();
            isSendingAppData = true;
            restClient.getApiService().sendDailyData(data, ApiConstants.APP_DATA_TYPE, new Callback<HashMap<String, Object>>() {
                @Override
                public void success(HashMap dataAggregationParser, Response response) {
                    if (dataAggregationParser != null) {
                        lastsendTimeApp = System.currentTimeMillis();
                        String giveTodayDate = dataAggregationUtils.giveTodayDate();
                        dataAggregationUtils.saveSyncDetailInDB(giveTodayDate, ApiConstants.APP_DATA_TYPE, ApiConstants.DAILY_DATA_TYPE, 0);
//                        SDKUtils.setDataForTesting(DailyAggregationService.this, "Daily Service - App Data", data, giveTodayDate,
//                                "Success");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getCause() != null) {
                        SdkLg.e("app retrofit error : - " + error.getCause().toString());
//                        SDKUtils.setDataForTesting(DailyAggregationService.this, "Daily Service - App Data", data, "null", "Failure");
                    }
                }
            });
        }
    }

    private boolean isSendingRoamingData = false;

    private void sendRoamingData() throws IOException {
        if (lastsendTimeRoaming != 0 && !isSendingRoamingData) {
            if (DateUtil.sixHourBefore(lastsendTimeRoaming)) {
                sendRoamingDailyData();
            }
        } else {
            if (!isSendingRoamingData)
                sendRoamingDailyData();
        }
    }

    private void sendRoamingDailyData() {
        if (commonRoamingObjHashMap != null && commonRoamingObjHashMap.size() != 0) {
            //List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            final String data = new Gson().toJson(roamingBulkHashMap);
//            Log.e("daily roaming", data);
            isSendingRoamingData = true;
            DataAggRestClient client = new DataAggRestClient();
            client.getApiService().sendDailyData(data, ApiConstants.ROAMING_DATA_TYPE, new Callback<HashMap<String, Object>>() {
                @Override
                public void success(HashMap dataAggregationParser, Response response) {
                    if (dataAggregationParser != null) {
                        lastsendTimeRoaming = System.currentTimeMillis();
                        String giveTodayDate = dataAggregationUtils.giveTodayDate();
                        dataAggregationUtils.saveSyncDetailInDB(giveTodayDate, ApiConstants.ROAMING_DATA_TYPE, ApiConstants.DAILY_DATA_TYPE, sendingforsimid);
//                        SDKUtils.setDataForTesting(DailyAggregationService.this, "Daily Service - Roaming Data", data, giveTodayDate,
//                                "Success");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getCause() != null) {
                        SdkLg.e("retrofit error: - " + error.getCause().toString());
//                        SDKUtils.setDataForTesting(DailyAggregationService.this, "Daily Service - Roaming Data", data, "null",
//                                "Failure");
                    }
                }
            });
        }
    }

    private boolean isSendingInternetData = false;

    private void sendInternetData() throws IOException {
        if (lastsendTimeData != 0 && !isSendingInternetData) {
            if (DateUtil.sixHourBefore(lastsendTimeData))
                sendInternetDailyData();
        } else {
            if (!isSendingInternetData)
                sendInternetDailyData();
        }

    }

    private void sendInternetDailyData() {
        Iterator dataIter = dataHashMap.entrySet().iterator();
        while (dataIter.hasNext()) {
            //List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            final HashMap.Entry pair = (HashMap.Entry) dataIter.next();
//            Log.e("datajson", String.valueOf(pair.getValue()));
            isSendingInternetData = true;
            DataAggRestClient restClient = new DataAggRestClient();
            restClient.getApiService().sendDailyData(String.valueOf(pair.getValue()), ApiConstants.INTERNET_DATA_TYPE, new Callback<HashMap<String, Object>>() {
                @Override
                public void success(HashMap dataAggregationParser, Response response) {
                    if (dataAggregationParser != null) {
                        lastsendTimeData = System.currentTimeMillis();
                        dataAggregationUtils.saveSyncDetailInDB(String.valueOf(pair.getKey()), ApiConstants.INTERNET_DATA_TYPE, ApiConstants.DAILY_DATA_TYPE, 0);
//                        SDKUtils.setDataForTesting(DailyAggregationService.this, "Daily Service -" +
//                                " Internet Data", String
//                                .valueOf(pair.getSdkvalue()), dataAggregationUtils.giveTodayDate(), "Success");

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getCause() != null)
                        SdkLg.e("retrofit failed : -" + error.getCause().toString());
//                    SDKUtils.setDataForTesting(DailyAggregationService.this, "Daily Service - Internet Data", String.valueOf(pair
//                            .getSdkvalue()), dataAggregationUtils.giveTodayDate(), "Failure");
                }
            });
            dataIter.remove();
        }
    }

    private boolean isSendingCallData = false;

    private void sendCallSmsData() throws IOException {
        if (lastsendTimeCall != 0 && !isSendingCallData) {
            if (DateUtil.sixHourBefore(lastsendTimeCall))
                sendCallDailyData();
        } else {
            if (!isSendingCallData)
                sendCallDailyData();
        }
    }

    private void sendCallDailyData() {
        isSendingCallData = true;
        Iterator bulkIterator = bulkHashMap.entrySet().iterator();
        while (bulkIterator.hasNext()) {
            //List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            final HashMap.Entry pair = (HashMap.Entry) bulkIterator.next();
            String data = new Gson().toJson(pair.getValue());
            DataAggRestClient restClient = new DataAggRestClient();
            restClient.getApiService().sendDailyData(data, ApiConstants.CALLSMS_DATA_TYPE, new Callback<HashMap<String, Object>>() {
                @Override
                public void success(HashMap dataAggregationParser, Response response) {
                    if (dataAggregationParser != null) {
                        lastsendTimeCall = System.currentTimeMillis();
                        dataAggregationUtils.saveSyncDetailInDB(String.valueOf(pair.getKey()), ApiConstants.CALLSMS_DATA_TYPE, ApiConstants.DAILY_DATA_TYPE, sendingforsimid);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error != null) {
                        SdkLg.e(error.toString());
                    }
                }
            });

            bulkIterator.remove();
        }
        isSendingCallData = false;
    }

    private void iterateRoamingCallandSms() {
        Iterator smsItr = roamingSmsHashMap.entrySet().iterator();
        try {
            while (smsItr.hasNext()) {
                Map<String, Object> combinedCallSmsMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) smsItr.next();
                if (roamingCallHashMap.containsKey(pair.getKey())) {
                    combinedCallSmsMap.putAll(roamingCallHashMap.get(pair.getKey()));
                    combinedCallSmsMap.putAll(roamingSmsHashMap.get(pair.getKey()));
                    roamingCallHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                } else {
                    combinedCallSmsMap.putAll(dataAggregationUtils.getUpdatedRoamingCallMap(tempCallMap, "", 0, 0, 0));
                    combinedCallSmsMap.putAll(roamingSmsHashMap.get(pair.getKey()));
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                }
                smsItr.remove();// Avoids ConcurrentModificationException
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        Iterator callItr = roamingCallHashMap.entrySet().iterator();
        try {
            while (callItr.hasNext()) {
                Map<String, Object> combinedCallSmsMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) callItr.next();
                combinedCallSmsMap.putAll(roamingCallHashMap.get(pair.getKey()));
                //combinedCallSmsMap.putAll(dataAggregationUtils.getUpdatedRoamingSmsMap(tempSmsMap, "", 0));
                //combinedCallSmsMap.putAll(dataAggregationUtils.getEmptyDataUsageMap());
                commonRoamingObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                callItr.remove();// Avoids ConcurrentModificationException
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private void makeCommonRoamingObjHashMap() {
        Iterator dataItr = roamingDataHashMap.entrySet().iterator();
        try {
            while (dataItr.hasNext()) {
                Map<String, Object> combinedRoamingMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) dataItr.next();
                if (roamingCallHashMap.containsKey(pair.getKey()) && roamingSmsHashMap.containsKey(pair.getKey())) {
                    //combinedRoamingMap.put("metadata", );
                    combinedRoamingMap.putAll(roamingCallHashMap.get(pair.getKey()));
                    combinedRoamingMap.putAll(roamingSmsHashMap.get(pair.getKey()));
                    combinedRoamingMap.putAll(roamingDataHashMap.get(pair.getKey()));
                    roamingCallHashMap.remove(pair.getKey());
                    roamingSmsHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                } else if (roamingCallHashMap.containsKey(pair.getKey())) {
                    combinedRoamingMap.putAll(roamingCallHashMap.get(pair.getKey()));
                    //combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingSmsMap(tempSmsMap, "", 0));
                    combinedRoamingMap.putAll(roamingDataHashMap.get(pair.getKey()));
                    roamingCallHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                } else if (roamingSmsHashMap.containsKey(pair.getKey())) {
                    // combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingCallMap(tempCallMap, "", 0, 0, 0));
                    combinedRoamingMap.putAll(roamingSmsHashMap.get(pair.getKey()));
                    combinedRoamingMap.putAll(roamingDataHashMap.get(pair.getKey()));
                    roamingSmsHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                } else {
//                    combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingCallMap(tempCallMap, "", 0, 0, 0));
//                    combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingSmsMap(tempSmsMap, "", 0));
                    combinedRoamingMap.putAll(roamingDataHashMap.get(pair.getKey()));
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                }
                dataItr.remove();// Avoids ConcurrentModificationException
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private void makeSmsObjHashMap(int slot_id) {
        smsObjHashMap.clear();
        Iterator smsIterator = smsHashMap.entrySet().iterator();
        try {
            while (smsIterator.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry) smsIterator.next();
                Map<String, Object> smsCallMap = new LinkedHashMap<>();
                Map<String, Map<String, Integer>> tempCallMap = new LinkedHashMap<>();
                smsCallMap.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), dailysend_call_type, slot_id));
                //smsCallMap.putAll(dataAggregationUtils.getUpdatedCallMap(tempCallMap, "", 0, 0, 0));
                smsCallMap.putAll(smsHashMap.get(pair.getKey()));
                smsIterator.remove();
                smsObjHashMap.put((String) pair.getKey(), smsCallMap);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private void makeCallObjHashMap(int slot_id) {
        callObjHashMap.clear();
        Iterator iterator = callHashMap.entrySet().iterator();
        try {
            while (iterator.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry) iterator.next();
                Map<String, Object> callSmsMap = new LinkedHashMap<>();
                callSmsMap.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), dailysend_call_type, slot_id));
                callSmsMap.putAll(callHashMap.get(pair.getKey()));
                callObjHashMap.put((String) pair.getKey(), callSmsMap);
                iterator.remove();
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private void makeCommonCallSmsObjHashMap(int slot_id) {
        commonCallSmsObjHashMap.clear();
        Iterator it = smsHashMap.entrySet().iterator();
        try {
            while (it.hasNext()) {
                Map<String, Object> combinedCallSmsMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) it.next();
                if (callHashMap.containsKey(pair.getKey())) {
                    combinedCallSmsMap.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), dailysend_call_type, slot_id));
                    combinedCallSmsMap.putAll(smsHashMap.get(pair.getKey()));
                    combinedCallSmsMap.putAll(callHashMap.get(pair.getKey()));
                    callHashMap.remove(pair.getKey());
                    commonCallSmsObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                    it.remove();// Avoids ConcurrentModificationException
                }
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }
}
