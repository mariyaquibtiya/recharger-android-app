package com.getsmartapp.lib.services;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.getsmartapp.lib.dataAggregation.DataAggregationUtils;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.retrofit.DataAggRestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.lib.utils.SDKUtils;
import com.getsmartapp.lib.utils.SdkLg;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author  nitesh on 15/6/15.
 */
public class BulkAggregateDataService extends IntentService {

    public JSONArray dataArray = new JSONArray();
    public JSONArray operatorArrya = new JSONArray();
    public HashMap<String, JSONObject> dataHashMap = new HashMap<>();
    protected Map<String, Object> appHashMap = new LinkedHashMap<>();
    private JSONArray appData = new JSONArray();

    //    protected HashMap<String, Map<String, Map<String, Integer>>> callHashMap = new LinkedHashMap<>();
//    protected HashMap<String, Map<String, Integer>> smsHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> commonCallSmsObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> callObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> smsObjHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> bulkHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Map<String, Float>>> roamingBulkDataHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Map<String, Map<String, Integer>>>> roamingBulkCallHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Map<String, Integer>>> roamingBulkSmsHashMap = new LinkedHashMap<>();
    protected HashMap<String, Map<String, Object>> commonRoamingObjHashMap = new LinkedHashMap<>();
    protected JSONArray operatorsmsHashMap = new JSONArray();
    protected List<Object> callSmsObjList = new ArrayList<>();
    protected List<Object> roamingObjList = new ArrayList<>();
    protected List<Objects> smsObjectList = new ArrayList<>();
    private DataAggregationUtils dataAggregationUtils;
    private String device_id;
    private SharedPrefManager shrd;
    private SQLiteDatabase mDatabase;
    private long bulklastcallsend = 0;
    private long bulklastdatasend = 0;
    private long bulklastroamingsend = 0;
    private long bulklastopsmssend = 0;
    private long bulklastdataappwisesend = 0;

    private int sim1_id = 0, sim2_id = 0;

    private long bulklastappdatasend = 0;

    public BulkAggregateDataService() {
        super("BulkAggregateDataService");
    }

    private DualSimManager simManager;
    private boolean sendingSimData = false;
    private int sendingForSimId = 0;


    @Override
    protected void onHandleIntent(Intent intent) {
        SdkDbHelper dbHelper = SdkDbHelper.getInstance(this);
        mDatabase = dbHelper.getWritableDatabase();
        dataAggregationUtils = new DataAggregationUtils(this, 0);
        simManager = new DualSimManager(this);
        shrd = new SharedPrefManager(this);
        if (simManager.isHasDualSim()) {
            sim1_id = shrd.getIntValue(Constants.SIM1_serial);
            sim2_id = shrd.getIntValue(Constants.SIM2_serial);
            if (sim1_id != 0) {
                dataAggregationUtils = new DataAggregationUtils(this, sim1_id);
            }
            if (sim2_id != 0) {
                dataAggregationUtils = new DataAggregationUtils(this, sim2_id);
            }
        }
        device_id = dataAggregationUtils.getDeviceID();
        try {
            shrd.setBooleanValue(Constants.SENDING_DATA, true);
            // dual sim case
            if (simManager.isHasDualSim()) {
                if (sim2_id == 0 && sim1_id == 0 && !sendingSimData) { // The call verification is not done so we have fallback
                    sendSingleSimData();
                } else {
                    if (sim1_id != 0 && !sendingSimData) {
                        sendingForSimId = sim1_id;
                        bulklastcallsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.CALLSMS_DATA_TYPE, sim1_id);
                        bulklastroamingsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.ROAMING_DATA_TYPE, sim1_id);
                        bulklastopsmssend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.OPSMS_DATA_TYPE, sim1_id);
                        calculateCallandRoamingData(sim1_id, shrd.getIntValue(Constants.SLOT1));
                    }
                    if (sim2_id != 0 && !sendingSimData) {
                        sendingForSimId = sim2_id;
                        bulklastcallsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.CALLSMS_DATA_TYPE, sim2_id);
                        bulklastroamingsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.ROAMING_DATA_TYPE, sim2_id);
                        bulklastopsmssend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.OPSMS_DATA_TYPE, sim2_id);
                        calculateCallandRoamingData(sim2_id, shrd.getIntValue(Constants.SLOT2));
                    }
                }
            } else {
                sendSingleSimData();
            }
            bulklastdatasend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.INTERNET_DATA_TYPE, 0);
            bulklastdataappwisesend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.APP_DATA_TYPE, 0);

            bulklastappdatasend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.APP_DATA_TYPE, 0);
            makeInternetData();
            makeAppData();
            makeOperatorSmsData();  // operator sms send to server
            sendAllData();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendAllData() {
        sendCallData();
        sendInternetData();
        sendAppData();
        sendRoamingData();
        sendOperatorSmsData();
    }

    private void sendOperatorSmsData() {
        if (operatorsmsHashMap.length() > 0) {
            // Sending Data Usage to Server----------------------------------------------------------------//
            String dataData = String.valueOf(operatorsmsHashMap);
//            Log.e("data Data", dataData);
            try {
                sendDataToServer(dataData, ApiConstants.OPSMS_DATA_TYPE, device_id);
                shrd.setBooleanValue(Constants.SENDING_DATA, false);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        } else {
            shrd.setBooleanValue(Constants.SENDING_DATA, false);
        }
    }

    private void sendAppData() {
        //        sendDataToServer(dataData, ApiConstants.APP_DATA_TYPE, device_id);
        try {
            if (appData.length() > 0) {
                sendDataToServer(appData.toString(), ApiConstants.APP_DATA_TYPE, device_id);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendInternetData() {
        // Sending Data Usage to Server----------------------------------------------------------------//
        if (dataArray.length() > 0) {
            String dataData = String.valueOf(dataArray);
//            Log.e("data Data", dataData);
            try {
                sendDataToServer(dataData, ApiConstants.INTERNET_DATA_TYPE, device_id);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendCallData() {
        // Sending Call-SMS Usage Data to Server ----------------------------------------------------------------//
        try {
            if (callSmsObjList.size() > 0) {
                String obj = new Gson().toJson(callSmsObjList);
//                Log.e("bulk call", obj);
                sendDataToServer(obj, ApiConstants.CALLSMS_DATA_TYPE, device_id);
            }
        } catch (OutOfMemoryError e) {
            e.getStackTrace();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private void makeAppData() {
        try {
            if (bulklastappdatasend != 0) {
                if (!DateUtil.isToday(new Date(bulklastappdatasend))) {
                    makeAppWiseDataBulkData();
                }
            } else {
                makeAppWiseDataBulkData();
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendSingleSimData() throws IOException, JSONException {
        bulklastcallsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.CALLSMS_DATA_TYPE, 0);
        bulklastdatasend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.INTERNET_DATA_TYPE, 0);
        bulklastroamingsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.ROAMING_DATA_TYPE, 0);
        bulklastopsmssend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.OPSMS_DATA_TYPE, 0);
        bulklastdataappwisesend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.APP_DATA_TYPE, 0);
        sendingForSimId = 0;
        calculateCallandRoamingData(0, 1);   //single sim case
    }

    private void calculateCallandRoamingData(int sim_id, int slot_id) throws IOException, JSONException {
        calculateCallData(sim_id, slot_id);
        calculateRoamingData(sim_id, slot_id);
    }

    private void makeOperatorSmsData() {
        if (bulklastopsmssend != 0) {
            if (!DateUtil.isToday(new Date(bulklastopsmssend))) {
                makeOperatorSms();
            } else {
                bulklastopsmssend = 0;
            }
        } else {
            makeOperatorSms();
        }
    }

    private void calculateRoamingData(int sim_id, int slot_id) {
        if (bulklastroamingsend != 0) {
            if (!DateUtil.isToday(new Date(bulklastroamingsend))) {
                makeRoamingData(sim_id, slot_id);
            } else {
                sendingSimData = false;
            }
        } else {
            makeRoamingData(sim_id, slot_id);
        }
    }

    private void makeInternetData() throws JSONException, IOException {
        if (bulklastdatasend != 0) {
            if (!DateUtil.isToday(new Date(bulklastdatasend))) {
                makeDataBulkData();
            }
        } else {
            makeDataBulkData();
        }
    }

    private void calculateCallData(int sim_id, int slot_id) throws IOException, JSONException {
        if (bulklastcallsend != 0) {
            if (!DateUtil.isToday(new Date(bulklastcallsend))) {
                makeCallBulkData(sim_id, slot_id);
            } else {
                // If data of 1st sim is sent and sim2 is onboarded on the same day ...
                // the first sim method will make condition for 2nd sim
                bulklastcallsend = 0;
                sendingSimData = false;
            }
        } else {
            makeCallBulkData(sim_id, slot_id);
        }
    }


    private void makeAppWiseDataBulkData() throws IOException, JSONException {
        SQLiteDatabase sqLiteDatabase = SdkDbHelper.getInstance(this).getWritableDatabase();
//        long dailysend_appdata_type = SDKUtils.getLastSendEntry(sqLiteDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.APP_DATA_TYPE, 0);

//        bulklastcallsend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.CALLSMS_DATA_TYPE, sim1_id);

//        bulklastdatasend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.INTERNET_DATA_TYPE, 0);
        bulklastdataappwisesend = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.BULK_DATA_TYPE, ApiConstants.APP_DATA_TYPE, 0);
        if (bulklastdataappwisesend == 0) {
            bulklastdataappwisesend = Calendar.getInstance().getTimeInMillis();
        }
        long currentTimeMillis = Calendar.getInstance().getTimeInMillis();
        String date = "";
        do {
            date = DateUtil.getDateFromTimeInMillis(currentTimeMillis);
            appHashMap.put("metadata", dataAggregationUtils.getMetaDataObj(date, bulklastdataappwisesend, 0));
            appHashMap.put("app", dataAggregationUtils.setAndGetAppData(date));
            if (appHashMap.size() > 0) {
                String dataData = new Gson().toJson(appHashMap);
                appData.put(new JSONObject(dataData));
            }

            currentTimeMillis = currentTimeMillis - 24 * 60 * 60 * 1000;
        } while (!DateUtil.getDateFromTimeInMillis(bulklastdataappwisesend).equalsIgnoreCase(date));
    }

    private void makeOperatorSms() {
        // Sms data
        try {
            operatorsmsHashMap = dataAggregationUtils.setandGetOperatorSms(ApiConstants.BULK_DATA_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void makeRoamingData(int sim_id, int slot_id) {
        sendingSimData = true;
        commonRoamingObjHashMap.clear();
        roamingObjList.clear();
        //Roaming Usages
        //roamingBulkDataHashMap = dataAggregationUtils.setAndGetBulkRoamingDataHashMap(ApiConstants.BULK_DATA_TYPE);
        roamingBulkSmsHashMap = dataAggregationUtils.setAndGetBulkRoamingSmsHashMap(ApiConstants.BULK_DATA_TYPE, sim_id);
        roamingBulkCallHashMap = dataAggregationUtils.setAndGetBulkRoamingCallHashMap(ApiConstants.BULK_DATA_TYPE, sim_id);

        //makeCommonDateRoamingObjHashMap();

        //Extracting common roaming SMS, Call & Data Circle to form one Object--------------------------------------//
        makeCommonDateRoamingObjHashMap();
        iterateRoamingCallandSms();
        if (commonRoamingObjHashMap.size() > 0)
            // date hashmap has been made here -- Now need to add metadata object alongwith each date data
            formBulkRoamingHashMap(slot_id);

    }

    private void makeDataBulkData() throws JSONException, IOException {
        dataHashMap = dataAggregationUtils.setAndGetDataHashMap(ApiConstants.BULK_DATA_TYPE,bulklastdatasend);
        SdkLg.e("final 2:"+dataHashMap.toString());
        Iterator dataIt = dataHashMap.entrySet().iterator();
        while (dataIt.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) dataIt.next();
            dataArray.put(pair.getValue());
            SdkLg.e("pair_value:"+pair.getValue().toString());
            dataIt.remove();

        }
    }

    private void makeCallBulkData(int sim_id, int slot_id) throws IOException, JSONException {
        sendingSimData = true;
        bulkHashMap.clear();
        commonCallSmsObjHashMap.clear();
        callObjHashMap.clear();
        callSmsObjList.clear();
        smsObjHashMap.clear();
        HashMap<String, Map<String, Integer>> smsHashMap = dataAggregationUtils.setAndGetSmsHashMap(ApiConstants.BULK_DATA_TYPE, false, sim_id);
        HashMap<String, Map<String, Map<String, Integer>>> callHashMap = dataAggregationUtils.setAndGetCallHashMap(ApiConstants.BULK_DATA_TYPE, false, sim_id);
        Iterator it = smsHashMap.entrySet().iterator();
        try {
            while (it.hasNext()) {
                Map<String, Object> combinedCallSmsMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) it.next();
                if (callHashMap.containsKey(pair.getKey())) {
                    combinedCallSmsMap.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), bulklastcallsend, slot_id));
                    combinedCallSmsMap.putAll(smsHashMap.get(pair.getKey()));
                    combinedCallSmsMap.putAll(callHashMap.get(pair.getKey()));
                    callHashMap.remove(pair.getKey());
                    commonCallSmsObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                    it.remove();// Avoids ConcurrentModificationException
                }
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        //Making Call-Usage HashMap
        Iterator iterator = callHashMap.entrySet().iterator();
        try {
            while (iterator.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry) iterator.next();
                Map<String, Object> callSmsMap = new LinkedHashMap<>();
                callSmsMap.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), bulklastcallsend, slot_id));
                callSmsMap.putAll(callHashMap.get(pair.getKey()));
                callObjHashMap.put((String) pair.getKey(), callSmsMap);
                iterator.remove();
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        //Making SMS-Usage HashMap
        Iterator smsIterator = smsHashMap.entrySet().iterator();
        try {
            while (smsIterator.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry) smsIterator.next();
                Map<String, Object> smsCallMap = new LinkedHashMap<>();
                //Map<String, Map<String, Integer>> tempCallMap = new LinkedHashMap<>();
                smsCallMap.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), bulklastcallsend, slot_id));
                //smsCallMap.putAll(dataAggregationUtils.getUpdatedCallMap(tempCallMap, "", 0, 0, 0));
                smsCallMap.putAll(smsHashMap.get(pair.getKey()));
                smsIterator.remove();
                smsObjHashMap.put((String) pair.getKey(), smsCallMap);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        bulkHashMap.putAll(callObjHashMap);
        bulkHashMap.putAll(smsObjHashMap);
        bulkHashMap.putAll(commonCallSmsObjHashMap);

//        Log.e("bulkHashmap_size", bulkHashMap.size() + "");

        // if data not present the date record should be there

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Set<String> keys = bulkHashMap.keySet();
        int sendornot = dataAggregationUtils.call_last_date;
        Calendar datecal = Calendar.getInstance();
        Date today = new Date(System.currentTimeMillis());
        Date lastdate = new Date(System.currentTimeMillis() - 30 * 24 * 60 * 60 * 1000L);
        if (sendornot == 1) {
            lastdate = dataAggregationUtils.last_date;
        }
            do {
                String date = simpleDateFormat.format(lastdate);
                if (!keys.contains(date)) {
                    HashMap<String, Map<String, Object>> callObjHashMap = new LinkedHashMap<>();
                    Map<String, Object> smsCallMap = new LinkedHashMap<>();
                    //String ldate = simpleDateFormat.format(date);
                    smsCallMap.put("metadata", dataAggregationUtils.getMetaDataObj(date, bulklastdatasend, slot_id));
                    //lastdateCal.add(Calendar.DAY_OF_YEAR, 1);
                    callObjHashMap.put(date, smsCallMap);
                    bulkHashMap.putAll(callObjHashMap);
                }
                datecal.setTime(lastdate);
                datecal.add(Calendar.DATE, 1);
                lastdate = datecal.getTime();
            } while (lastdate.before(today));

        Iterator bulkIterator = bulkHashMap.entrySet().iterator();
        while (bulkIterator.hasNext()) {
            try {
                HashMap.Entry pair = (HashMap.Entry) bulkIterator.next();
                callSmsObjList.add(pair.getValue());
                bulkIterator.remove();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void sendDataToServer(final String callData, final int sType, String device_id) throws IOException,
            JSONException {
        DataAggRestClient restClient = new DataAggRestClient();
        sendingSimData = false;
        restClient.getApiService().sendBulkData(callData, sType, device_id, new Callback<HashMap<String, Object>>() {
            @Override
            public void success(HashMap dataAggregationParser, Response response) {

                String dataData = new Gson().toJson(dataAggregationParser);
//                Log.e("PeeyushKS","Data: "+dataData);
                if (dataAggregationParser != null) {
                    if (dataAggregationParser.containsKey("message")) {

                        updateSyncTime(sType);
                        String date = dataAggregationUtils.giveTodayDate();
                        dataAggregationUtils.saveSyncDetailInDB(date, sType, ApiConstants.BULK_DATA_TYPE, sendingForSimId);
                        shrd.setIntValue(Constants.BULK_DATA_SENT_ONCE, 1);
                        //SDKUtils.setLastDataTypeDateEntry(BulkAggregateDataService.this, Calendar.getInstance().getTimeInMillis());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                SdkLg.e("bulk_failed");

            }
        });

    }

    private void updateSyncTime(int sType) {
        if (sType == ApiConstants.OPSMS_DATA_TYPE) {
            // update sync status
            try {
                Cursor cursor = getOpSyncCursor();
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        int id = cursor.getInt(cursor.getColumnIndex(DBContractor.SmsInboxEntry.COLUMN_ID));
                        String q = "update " + DBContractor.SmsInboxEntry.TABLE_NAME + "  SET " + DBContractor.SmsInboxEntry.COLUMN_SYNCED + "='1'," + DBContractor.SmsInboxEntry.COLUMN_SYNC_TIMESTAMP + "=" + System.currentTimeMillis() + "  where " + DBContractor.SmsInboxEntry.COLUMN_ID + "=" + id;
                        //Log.e("smsupdate", q);
                        mDatabase.execSQL(q);
//                                        ContentValues contentValues = new ContentValues();
//                                        contentValues.put();
//                                        mDatabase.update(SmsInboxEntry.TABLE_NAME,contentValues,SmsInboxEntry.COLUMN_ID+"=?",new String[]{id+""});
                    } while (cursor.moveToNext());
                }
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        }
    }

    private Cursor getOpSyncCursor() {
        Cursor cursor = null;
        if (bulklastopsmssend != 0) {
            cursor = mDatabase.query(DBContractor.SmsInboxEntry.TABLE_NAME, null, DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + ">?", new String[]{bulklastopsmssend + ""}, null, null, null);
        } else {
            cursor = mDatabase.query(DBContractor.SmsInboxEntry.TABLE_NAME, null, null, null, null, null, null);
        }
        return cursor;
    }

    private void makeCommonDateRoamingObjHashMap() {
        Iterator dataItr = roamingBulkDataHashMap.entrySet().iterator();
        try {
            while (dataItr.hasNext()) { // data for each date
                Map<String, Object> combinedRoamingMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) dataItr.next();
                if (roamingBulkCallHashMap.containsKey(pair.getKey()) && roamingBulkSmsHashMap.containsKey(pair.getKey())) {
                    //combinedRoamingMap.put("metadata", );
                    combinedRoamingMap.putAll(roamingBulkCallHashMap.get(pair.getKey()));
                    combinedRoamingMap.putAll(roamingBulkSmsHashMap.get(pair.getKey()));
                    combinedRoamingMap.putAll(roamingBulkDataHashMap.get(pair.getKey()));
                    roamingBulkCallHashMap.remove(pair.getKey());
                    roamingBulkSmsHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);

                } else if (roamingBulkCallHashMap.containsKey(pair.getKey())) {
                    combinedRoamingMap.putAll(roamingBulkCallHashMap.get(pair.getKey()));
                    //combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingSmsMap(tempSmsMap, "", 0));
                    combinedRoamingMap.putAll(roamingBulkDataHashMap.get(pair.getKey()));
                    roamingBulkCallHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                } else if (roamingBulkSmsHashMap.containsKey(pair.getKey())) {
                    //combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingCallMap(tempCallMap, "", 0, 0, 0));
                    combinedRoamingMap.putAll(roamingBulkSmsHashMap.get(pair.getKey()));
                    combinedRoamingMap.putAll(roamingBulkDataHashMap.get(pair.getKey()));
                    roamingBulkSmsHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                } else {
//                    combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingCallMap(tempCallMap, "", 0, 0, 0));
//                    combinedRoamingMap.putAll(dataAggregationUtils.getUpdatedRoamingSmsMap(tempSmsMap, "", 0));
                    combinedRoamingMap.putAll(roamingBulkDataHashMap.get(pair.getKey()));
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedRoamingMap);
                }
                dataItr.remove();// Avoids ConcurrentModificationException
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private void iterateRoamingCallandSms() {
        Iterator smsItr = roamingBulkSmsHashMap.entrySet().iterator();
        try {
            while (smsItr.hasNext()) {
                Map<String, Object> combinedCallSmsMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) smsItr.next();
                if (roamingBulkCallHashMap.containsKey(pair.getKey())) {
                    combinedCallSmsMap.putAll(roamingBulkCallHashMap.get(pair.getKey()));
                    combinedCallSmsMap.putAll(roamingBulkSmsHashMap.get(pair.getKey()));
                    roamingBulkCallHashMap.remove(pair.getKey());
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                } else {
                    combinedCallSmsMap.putAll(roamingBulkSmsHashMap.get(pair.getKey()));
                    commonRoamingObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                }
                smsItr.remove();// Avoids ConcurrentModificationException
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        Iterator callItr = roamingBulkCallHashMap.entrySet().iterator();
        try {
            while (callItr.hasNext()) {
                Map<String, Object> combinedCallSmsMap = new LinkedHashMap<>();
                HashMap.Entry pair = (HashMap.Entry) callItr.next();
                combinedCallSmsMap.putAll(roamingBulkCallHashMap.get(pair.getKey()));
                commonRoamingObjHashMap.put((String) pair.getKey(), combinedCallSmsMap);
                callItr.remove();// Avoids ConcurrentModificationException
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private void formBulkRoamingHashMap(int slot_id) {
        Iterator iter = commonRoamingObjHashMap.entrySet().iterator();
        while (iter.hasNext()) {
            HashMap<String, Object> eachDateObj = new LinkedHashMap<>();
            HashMap.Entry pair = (HashMap.Entry) iter.next();
            eachDateObj.put("metadata", dataAggregationUtils.getMetaDataObj((String) pair.getKey(), bulklastroamingsend, slot_id));
            eachDateObj.put("circle", pair.getValue());
            roamingObjList.add(eachDateObj);
            iter.remove();
        }
    }


    private void sendRoamingData() {
        if (roamingObjList.size() > 0) {
            try {
                String obj = new Gson().toJson(roamingObjList);
                sendDataToServer(obj, ApiConstants.ROAMING_DATA_TYPE, device_id);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sendingSimData = false;
    }
}
