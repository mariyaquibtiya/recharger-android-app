package com.getsmartapp.lib.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.SDKUtils;

/**
 * @author  nitesh.verma on 3/7/16.
 */
public class DeleteOldDataTask extends AsyncTask {

    protected int count1, count2;
    protected SQLiteDatabase sqLiteDatabase;
    protected Context context;
    protected SharedPrefManager sharedPrefManager;
    private DualSimManager simManager;

    public DeleteOldDataTask(Context mContext) {
        this.context = mContext;
        sqLiteDatabase = SdkDbHelper.getInstance(context).getWritableDatabase();
        sharedPrefManager = new SharedPrefManager(context);
        simManager = new DualSimManager(context);
    }

    @Override
    protected Object doInBackground(Object[] params) {

        purgeData(0);
        purgeData(1);

        return null;
    }

    private void purgeData(int is_roaming) {
        deleteData(is_roaming);
    }


    private void deleteData(int is_roaming) {
        if (simManager.isHasDualSim()) {
            if (sharedPrefManager.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) == 1) {
                int sim1_id = sharedPrefManager.getIntValue(Constants.SIM1_serial);
                int sim2_id = sharedPrefManager.getIntValue(Constants.SIM2_serial);
                if (sim1_id != 0) {
                    ClearbySimAndRoaming(is_roaming, sim1_id);
                }
                if (sim2_id != 0) {
                    ClearbySimAndRoaming(is_roaming, sim2_id);
                }
            }
        } else {
            ClearbySimAndRoaming(is_roaming, 0);
        }
    }

    private void ClearbySimAndRoaming(int is_roaming, int sim_id) {
        getTableAndClearData(DBContractor.CallDataEntry.TABLE_NAME, is_roaming, sim_id, sqLiteDatabase, Constants.CALL_TABLE, DBContractor.CallDataEntry.COLUMN_STARTTIME);
        getTableAndClearData(DBContractor.SmsDataEntry.TABLE_NAME, is_roaming, sim_id, sqLiteDatabase, Constants.SMS_TABLE, DBContractor.SmsDataEntry.COLUMN_DATE_TIME);
//        getTableAndClearData(DBContractor.InternetSessionEntry.TABLE_NAME,is_roaming, sim_id, sqLiteDatabase, Constants.INTERNET_TABLE, DBContractor.InternetSessionEntry.COLUMN_TIMESTAMP);
//        getTableAndClearData(DBContractor.AppDataUsageEntry.TABLE_NAME,is_roaming, sim_id, sqLiteDatabase, Constants.APP_TABLE, DBContractor.AppDataUsageEntry.COLUMN_TIMESTAMP);
    }

    private void getTableAndClearData(String table_name, int is_roaming, int sim_id, SQLiteDatabase sqLiteDatabase, int table, String timestamp_column) {
        int count1 = SDKUtils.getDateCount(sqLiteDatabase, table, sim_id, is_roaming);
        int duration = is_roaming == 0 ? Constants.NON_ROAMING_DATA_LIMIT : Constants.ROAMING_DATA_LIMIT;
        if (count1 > duration) {
            long endtme = SDKUtils.getLastLimitTimeStamp(sqLiteDatabase, table, sim_id);
            clearDataForTime(table_name, endtme, sim_id, timestamp_column, is_roaming);
        }
    }


    private void clearDataForTime(String table_name, long last_time, int sim_id, String column_name, int is_roaming) {
        String roamingstr = "";
        roamingstr = " and is_roaming = " + is_roaming;
        String q = "delete from " + table_name + " where " + column_name + " <= " + last_time + roamingstr;
        if (sim_id != 0) {
            q = "delete from " + table_name + " where " + column_name + " <=" + last_time + " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "=" + sim_id + roamingstr;
        }
//        Log.e("delete_query****--" + table_name, q);
        sqLiteDatabase.execSQL(q);
    }
}
