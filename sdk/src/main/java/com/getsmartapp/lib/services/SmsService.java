package com.getsmartapp.lib.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;

import com.getsmartapp.lib.data.SmsDataObserver;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.reciever.AggregateDataReciever;
import com.getsmartapp.lib.reciever.CallBatchSaveReciever;
import com.getsmartapp.lib.reciever.ClearDataReciever;
import com.getsmartapp.lib.constants.Constants;

/**
 * @author  nitesh on 21/6/15.
 */
public class SmsService extends Service {


    private static final long BATCH_CALL_SAVE_INTERVAL = 12 * 60 * 60 * 1000L;
    private String sim_type, sp, circle;
    private Context context = this;
    private SmsDataObserver observer;
    private SharedPrefManager sharedPrefManager;
    private long SYNC_TIME_INTERVAL = 6 * 60 * 60 * 1000L;
    private long DATA_CLEAR_TIME_INTERVAL = 24 * 60 * 60 * 1000L;
    private long ONE_WEEK_INTERVAL = 7 * 24 * 60 * 60 * 1000L;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        sharedPrefManager = new SharedPrefManager(this);
//================================== 6hrs Alarm for Sending data service ================================
        Intent reciever = new Intent(this, AggregateDataReciever.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, reciever, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), SYNC_TIME_INTERVAL, pendingIntent);//6 hours

// ***************** Clear data for more than 60 days old *************************
        if (sharedPrefManager.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) == 1) {
            Intent cleardata = new Intent(this, ClearDataReciever.class);
            PendingIntent pendingIntent2 = PendingIntent.getBroadcast(this, 2, cleardata, 0);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), DATA_CLEAR_TIME_INTERVAL, pendingIntent2);

            // ********************** Batch for Saving call *********************** //

            Intent batchintent = new Intent(this, CallBatchSaveReciever.class);
            PendingIntent pendingIntent3 = PendingIntent.getBroadcast(this, 3, batchintent, 0);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), BATCH_CALL_SAVE_INTERVAL, pendingIntent3);

// **************** MNP update for call classicfication ********************** //

            Intent updateMNPIntent = new Intent(this, UpdateMNPCallLog.class);
            PendingIntent pendingIntent1 = PendingIntent.getService(this, 4, updateMNPIntent, 0);
            alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), ONE_WEEK_INTERVAL, pendingIntent1);
        }

        sharedPrefManager.setIntValue(Constants.SMS_SERVICE_LAUNCHED, 1);
        initializeSmsObserver();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initializeSmsObserver() {
        observer = new SmsDataObserver(new Handler(), context);
        context.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, observer);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sharedPrefManager.setIntValue(Constants.SMS_SERVICE_LAUNCHED, 0);
        getContentResolver().unregisterContentObserver(observer);
    }
}
