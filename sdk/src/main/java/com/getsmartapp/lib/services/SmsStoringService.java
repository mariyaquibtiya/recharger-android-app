package com.getsmartapp.lib.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;

import com.getsmartapp.lib.data.CallCalculation;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.LocationUtils;
import com.getsmartapp.lib.utils.SDKUtils;

/**
 * @author  nitesh.verma on 11/20/15.
 */
public class SmsStoringService extends IntentService {

    private String mFeature = null, mLocality = null, mState = null, mCountry = null;
    private CallCalculation mCallcalculation;
    private int circleId;
    private String sim_type, sp, circle;
    private LocationUtils locationUtils;
    private String circle_id;
    private SharedPrefManager sharedPrefManager;
    private DualSimManager simManager;

    public SmsStoringService() {
        super("SmsStoringService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPrefManager = new SharedPrefManager(this);
        sim_type = sharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
        sp = sharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
        circle = sharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE);
        mCallcalculation = new CallCalculation(this, sim_type, sp, circle);
        locationUtils = LocationUtils.getInstance(this);//new LocationUtils(this);
        sharedPrefManager = new SharedPrefManager(this);
        simManager = new DualSimManager(this);
        try {
            Uri smssentUri = Uri.parse("content://sms");

            Cursor smsCursor = getContentResolver().query(smssentUri, null, null, null, "date DESC " +
                    "LIMIT 1");
            SdkDbHelper dbHelper = SdkDbHelper.getInstance(this);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            UpdateSmsDatabase(smsCursor, database, smssentUri);
            //updateSmsInbox(smsCursor, database);
        } catch (Exception e) {
        }
    }

//    private void updateSmsInbox(Cursor smsCursor, SQLiteDatabase database) {
//
//        try {
//            if (smsCursor != null && smsCursor.moveToFirst()) {
//                String address = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.ADDRESS));
//                String[] sms = address.split("[-\\\\s]");
//                for (int i = 0; i < sms.length; i++) {
//                    for (int j = 0; j < Constants.OPERATOR_SMS_ADDRESSES.length; j++) {
//                        if (sms[i].toLowerCase().contains(Constants.OPERATOR_SMS_ADDRESSES[j].toLowerCase())) {
//                            storeSmsInOperatorTable(smsCursor, database, address);
//                        }
//                    }
//                    for (int k = 0; k < Constants.VENDOR_SMS_ADDRESSES.length; k++) {
//                        if (sms[i].toLowerCase().contains(Constants.VENDOR_SMS_ADDRESSES[k].toLowerCase())) {
//                            String body = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.BODY));
//                            if (body.toLowerCase().matches(".*\\\\recharge\\\\b.*")) {
//                                storeSmsInOperatorTable(smsCursor, database, address);
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//        } finally {
//            if (smsCursor != null) {
//                smsCursor.close();
//            }
//        }
//    }

    private void storeSmsInOperatorTable(Cursor smsCursor, SQLiteDatabase database, String address) {
        String body = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.BODY));
        int sim_id = -1;
        try {
            sim_id = smsCursor.getInt(smsCursor.getColumnIndex(SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.SMS_TABLE)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int id = smsCursor.getInt(smsCursor.getColumnIndex(Telephony.Sms._ID));
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_ADDRESS, address);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_ID, id);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_MSG_BODY, body);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_SIM_ID, sim_id);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP, System.currentTimeMillis());
        try {
            long myid = database.insertWithOnConflict(DBContractor.SmsInboxEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
//            Log.e("sms_inbox_id", myid + "");
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    private void getAddress() {
        locationUtils.setAddressComponents();
        mLocality = locationUtils.getLocality();
        mFeature = locationUtils.getFeature();
        mCountry = locationUtils.getCountry();
        mState = locationUtils.getState();
        circleId = locationUtils.getCircleId(mCountry, mState, mLocality, mFeature);
        circle_id = circleId == 0 ? null : String.valueOf(circleId);
    }

    private void UpdateSmsDatabase(Cursor smsCursor, SQLiteDatabase database, Uri smssentUri) {
        String number, sms_type;
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int is_roaming = telephonyManager.isNetworkRoaming() ? 1 : 0;
        try {
            if (smsCursor != null && smsCursor.moveToFirst()) {
                long timestamp = smsCursor.getLong(smsCursor.getColumnIndex(Telephony.Sms.DATE));
                Cursor sameTimestamp = getContentResolver().query(smssentUri, null, Telephony.Sms.DATE + "=?", new String[]{timestamp + ""}, null);
                if (sameTimestamp != null && sameTimestamp.moveToFirst()) {
                    do {
                        processSms(smsCursor, database, is_roaming);
                    } while (sameTimestamp.moveToNext());
                } else {
                    processSms(smsCursor, database, is_roaming);
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processSms(Cursor smsCursor, SQLiteDatabase database, int is_roaming) {
        String sms_type;
        String number;
        long currentMessageID = smsCursor.getLong(smsCursor.getColumnIndex(Telephony.Sms._ID));
        int type = smsCursor.getInt(smsCursor.getColumnIndexOrThrow("type"));
        int mtype = smsCursor.getInt(smsCursor.getColumnIndex(Telephony.Sms.TYPE));
        if (mtype == Telephony.Sms.MESSAGE_TYPE_INBOX || mtype == Telephony.Sms.MESSAGE_TYPE_SENT) {
            sms_type = mCallcalculation.getSmsType(type);
            number = smsCursor.getString(smsCursor.getColumnIndexOrThrow("address"));
            String contact_name = SDKUtils.getContactName(this, number);
            char c = number.charAt(0);
            if ((number != null) && (number.length() > 9) && ((c >= '0' && c <= '9')
                    || number.substring(0, 1).equals("+"))) {
                ContentValues contentValues = getContentValues(number, sms_type, is_roaming, smsCursor, currentMessageID, contact_name);
                try {
                    long id = database.insertWithOnConflict(DBContractor.SmsDataEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
//                    Log.e("observer sms insert id", String.valueOf(id));
                } catch (SQLiteException e) {
                    e.printStackTrace();
                }
            }else {
                String address = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.ADDRESS));
                String[] sms = address.split("[-\\\\s]");
                for (int i = 0; i < sms.length; i++) {
                    for (int j = 0; j < Constants.OPERATOR_SMS_ADDRESSES.length; j++) {
                        if (sms[i].toLowerCase().contains(Constants.OPERATOR_SMS_ADDRESSES[j].toLowerCase())) {
                            storeSmsInOperatorTable(smsCursor, database, address);
                        }
                    }
                    for (int k = 0; k < Constants.VENDOR_SMS_ADDRESSES.length; k++) {
                        if (sms[i].toLowerCase().contains(Constants.VENDOR_SMS_ADDRESSES[k].toLowerCase())) {
                            String body = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.BODY));
                            if (body.toLowerCase().matches(".*\\\\recharge\\\\b.*")) {
                                storeSmsInOperatorTable(smsCursor, database, address);
                            }
                        }
                    }
                }
            }
        }
    }

    @NonNull
    private ContentValues getContentValues(String number, String sms_type, int is_roaming, Cursor smsCursor, long currentMessageID, String contact_name) {
        try {
            long date;
            String smsNetwork;
            int is_night, usable_sim_id = 0;
            String number_type;
            String country_name;
            number = number.replaceAll("\\s+", "");
            number = number.replaceAll("-", "");
            date = smsCursor.getLong(smsCursor.getColumnIndexOrThrow("date"));
            int sim_no = smsCursor.getInt(smsCursor.getColumnIndex(SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.SMS_TABLE)));
            mCallcalculation.setMobile(number);
            mCallcalculation.fillLandlineList();
            mCallcalculation.setNightStartEndTime();
            mCallcalculation.fillCountryList();
            smsNetwork = mCallcalculation.getCallNetwork(number);
            is_night = mCallcalculation.getIsNightValue(date);
            number_type = mCallcalculation.typeofCallandSms(number);
            getAddress();
            if (mCallcalculation.isIndianPhone(number)) {
                country_name = "India";
            } else country_name = mCallcalculation.numberCountry(number);
            if (number.startsWith("+91")) {
                number = number.substring(3);
            } else if (number.startsWith("0")) {
                number = number.substring(1);
            }
            int mysimid1 = sharedPrefManager.getIntValue(Constants.SIM1_serial);
            //int mysimid2 = sharedPrefManager.getIntValue(Constants.SIM2_serial);
            if (sim_no == mysimid1) {
                usable_sim_id = sim_no;
            } else {
                usable_sim_id = Constants.SECONDARY_SIM_ID;
            }
            is_roaming = simManager.isRoaming(SDKUtils.getSlotfromSimId(sharedPrefManager, sim_no)) ? 1 : 0;
//            Log.w("number: ", number + "");
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBContractor.SmsDataEntry.MESSAGE_ID, currentMessageID);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_NUMBER, number);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_NAME, (contact_name == null) ?
                    number : contact_name);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE, number_type);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_IS_NIGHT, is_night);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_DATE_TIME, date);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE, smsNetwork);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME, country_name);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_TYPE, sms_type);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_IS_ROAMING, is_roaming);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID, circle_id);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_LOCATION_COUNTRY, country_name);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_LOCATION_LOCALITY, mLocality);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_LOCATION_FEATURE_NAME, mFeature);
            contentValues.put(DBContractor.SmsDataEntry.COLUMN_LOCATION_STATE, mState);
            contentValues.put(DBContractor.ReferenceAppEntry.COLUMN_SIMNO, sim_no);
            return contentValues;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
