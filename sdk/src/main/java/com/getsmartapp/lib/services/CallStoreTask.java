package com.getsmartapp.lib.services;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import com.getsmartapp.lib.data.CallCalculation;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.utils.LocationUtils;
import com.getsmartapp.lib.utils.SDKUtils;

/**
 * @author nitesh.verma on 2/3/16.
 */
public class CallStoreTask extends AsyncTask<Void, Void, Void> {

    private Context mContext;
    private String circle_id;
    private String locality, feature, c_state, country;
    private SharedPrefManager shrd;
    private LocationUtils locationUtils;
    private int mCircleId = 0;
    private String provider, simType, circle;
    private DualSimManager simManager;

    public CallStoreTask(Context mContext) {
        this.mContext = mContext;
        this.shrd = new SharedPrefManager(mContext);
        this.locationUtils = LocationUtils.getInstance(mContext);//new LocationUtils(context);
        this.simManager = new DualSimManager(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        SharedPreferences sp2 = mContext.getSharedPreferences("Location_Pref", Context.MODE_PRIVATE);
//        Log.e("coordination", String.valueOf(sp2.getFloat("latitude", (float) 0)) + " , " + String.valueOf(sp2.getFloat("longitude", (float) 0)));
        setAddressAndCircleId();
        try {
            saveCalldetailsNow();
        } catch (Exception e) {
        }
        //mContext.getContentResolver().unregisterContentObserver(this);
        return null;
    }

    private void saveCalldetailsNow() {
        int usable_sim_id = 0;
        String country_name;
        int simid = 1;
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        int isRoaming = telephonyManager.isNetworkRoaming() ? 1 : 0;

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Cursor callCursor = mContext.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC LIMIT 1");
        if (callCursor != null && callCursor.moveToFirst()) {
            try {
                String phone = callCursor.getString(callCursor.getColumnIndex(CallLog.Calls.NUMBER));
                int type = callCursor.getInt(callCursor.getColumnIndex(CallLog.Calls.TYPE));
                long date = callCursor.getLong(callCursor.getColumnIndex(CallLog.Calls.DATE));
                String name = callCursor.getString(callCursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                int duration_in_sec = callCursor.getInt(callCursor.getColumnIndex(CallLog.Calls.DURATION));
                if (simManager.isHasDualSim()) {
                    simid = callCursor.getInt(callCursor.getColumnIndex(SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE)));
                }
                SdkDbHelper dbHelper = SdkDbHelper.getInstance(mContext);//new DBHelper(mContext);
                SQLiteDatabase database = dbHelper.getWritableDatabase();

                if (phone.length() >= 9) {
                    simType = shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
                    provider = shrd.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
                    circle = shrd.getStringValue(Constants.ON_BOARDING_CIRCLE);
                    if (simManager.isHasDualSim()){
                        if (simid==shrd.getIntValue(Constants.SIM1_serial)){
                            usable_sim_id=simid;
                            int slot1 = shrd.getIntValue(Constants.SLOT1);
                            if (slot1==2){
                                simType = shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE_2);
                                provider = shrd.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME_2);
                                circle = shrd.getStringValue(Constants.ON_BOARDING_CIRCLE_2);
                            }
                        }else {
                            usable_sim_id = Constants.SECONDARY_SIM_ID;
                            int slot = shrd.getIntValue(Constants.SLOT2);
                            if (slot==2){
                                simType = shrd.getStringValue(Constants.ON_BOARDING_SIM_TYPE_2);
                                provider = shrd.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME_2);
                                circle = shrd.getStringValue(Constants.ON_BOARDING_CIRCLE_2);
                            }
                        }

                        //TODO Uncomment below part after onboarding UI DONE dual sim integration

//                        if(shrd.getIntValue(Constants.NO_OF_SIM_ONBOARDED)>=2) {
//                            isRoaming = simManager.isRoaming(SDKUtils.getSlotfromSimId(shrd, usable_sim_id)) ? 1 : 0;
//                        }else {
//                            int sim1_slot = SDKUtils.getSlotfromSimId(shrd,shrd.getIntValue(Constants.SIM1_serial));
//                            isRoaming = simManager.isRoaming(sim1_slot==0?1:0)?1:0;
//                        }
                        //TODO ==================================================================
                    }
                    if (duration_in_sec > 0) {
                        int id = callCursor.getInt(callCursor.getColumnIndex(CallLog.Calls._ID));
                        CallCalculation callCalculation = new CallCalculation(mContext, simType, provider, circle);
                        int call_duration_in_min = callCalculation.getMinCallDuration(duration_in_sec);
                        phone = phone.replaceAll("\\s", "");
                        callCalculation.setMobile(phone);
                        callCalculation.fillLandlineList();
                        callCalculation.setNightStartEndTime();
                        callCalculation.fillCountryList();
                        String call_type = callCalculation.getCallType(type);
                        int isNight = callCalculation.getIsNightValue(date);
                        String callnetwork = callCalculation.getCallNetwork(phone);
                        String number_type = callCalculation.typeofCallandSms(phone);
                        if (callCalculation.isIndianPhone(phone)) {
                            country_name = "India";
                        } else country_name = callCalculation.numberCountry(phone);
                        if (!callnetwork.equals(DataStorageConstants.LANDLINE_CALLNETWORK)) {
                            if (phone.startsWith("+91")) {
                                phone = phone.substring(3);
                            } else if (phone.startsWith("0")) {
                                phone = phone.substring(1);
                            }
                        }
                        saveInDb(id,country_name, usable_sim_id, isRoaming, phone, date, name, duration_in_sec, database, call_duration_in_min, call_type, isNight, callnetwork, number_type);
                    }
                }
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (Exception e){}
        }
        if(callCursor!=null)
            callCursor.close();
    }

    private void saveInDb(int id, String country_name, int simid, int isRoaming, String phone, long date, String name, int duration_in_sec, SQLiteDatabase database, int call_duration_in_min, String call_type, int isNight, String callnetwork, String number_type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContractor.CallDataEntry.CALL_ID,id);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_NAME, name);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER, phone);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE, number_type);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_STARTTIME, date);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE, callnetwork);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN, call_duration_in_min);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC, duration_in_sec);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_IS_NIGHT, isNight);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_TYPE, call_type);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_LOCATION_COUNTRY, country);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_IS_ROAMING, isRoaming);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_LOCATION_FEATURE_NAME, feature);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_LOCATION_LOCALITY, locality);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_LOCATION_STATE, c_state);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_CIRCLE_ID, circle_id);
        contentValues.put(DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME, country_name);
        contentValues.put(DBContractor.ReferenceAppEntry.COLUMN_SIMNO, simid);
        try {
            long cid = database.insertWithOnConflict(DBContractor.CallDataEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
//            Log.e("history insert", String.valueOf(cid));
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    private void setAddressAndCircleId() {
        locationUtils.setAddressComponents();
        locality = locationUtils.getLocality();
        c_state = locationUtils.getState();
        feature = locationUtils.getFeature();
        country = locationUtils.getCountry();
        mCircleId = locationUtils.getCircleId(country, c_state, locality, feature);
        circle_id = String.valueOf(mCircleId);
    }
}
