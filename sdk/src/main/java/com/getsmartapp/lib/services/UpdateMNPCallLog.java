package com.getsmartapp.lib.services;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.getsmartapp.lib.SmartSDK;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.CallClassificationMNP;
import com.getsmartapp.lib.model.MNPRequest;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.utils.SDKUtils;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author  nitesh.verma on 3/29/16.
 */
public class UpdateMNPCallLog extends IntentService {

    private SharedPrefManager sharedPrefManager;
    private SQLiteDatabase sqLiteDatabase;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public UpdateMNPCallLog() {
        super("UpdateMNPCallLog");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPrefManager = new SharedPrefManager(this);
        sqLiteDatabase = SdkDbHelper.getInstance(this).getWritableDatabase();
        if (SDKUtils.isConnectingToInternet(this)) {
            List obj = SmartSDK.getInstance(this).getCallContactJson(this);
            //Log.e("obj",new Gson().toJson(obj, JSONObject.class));
//            String map = obj.toString();
//            Log.e("map",map.replace("=",":"));
//            String mymap = map.replace("=",":");
//            Log.e("fianl_amp",mymap);
            //Log.e("obj",obj);
            RestClient restClient = new RestClient(ApiConstants.BASE_RECHARGER_ADDRESS, null);
            restClient.getApiService().getMNPCircleOperatorForNumber(new MNPRequest(obj), new Callback<CallClassificationMNP>() {
                @Override
                public void success(CallClassificationMNP callClassificationMNP, Response response) {
                    if (Integer.parseInt(callClassificationMNP.getHeader().getStatus()) == 1) {
//                        Log.e("mnp_response", callClassificationMNP.toString());
                        if (callClassificationMNP.getBody().getPhoneData().size()>0) {
                            updateCallDB(callClassificationMNP);
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
//                    if (error != null) {
//                        Log.e("mnp_failed", error.getMessage());
//                    }
                }
            });
        }
    }

    private void updateCallDB(CallClassificationMNP callClassificationMNP) {
        for (CallClassificationMNP.BodyEntity.PhoneDataEntity dataEntity : callClassificationMNP.getBody().getPhoneData()){
            String num = dataEntity.getPhone();
            Cursor cursor = null;
            try{
             cursor =  sqLiteDatabase.query(DBContractor.CallDataEntry.TABLE_NAME,null,
                     DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER+"=?",new String[]{num},null,null,null);
                if (cursor!=null && cursor.moveToFirst()){
                  do {
                      String circle = dataEntity.getCircleName();
                      String operator = dataEntity.getSpName();
                      String myopr = sharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
                      String mycircle = sharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE);
                      if (SDKUtils.isStringNullEmpty(myopr)){
                          myopr = sharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME_2);
                          mycircle = sharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE_2);
                      }
                      String number_type = DataStorageConstants.LOCAL_NUMBER;
                      String network_type = DataStorageConstants.OTHER_CALLNETWORK;
                      if (!circle.equalsIgnoreCase(mycircle)){ // local call
                          number_type = DataStorageConstants.STD_NUMBER;
                      }
                      if (operator.equalsIgnoreCase(myopr)){
                          network_type = DataStorageConstants.SAME_CALLNETWORK;
                      }

                      sqLiteDatabase.execSQL("update "+ DBContractor.CallDataEntry.TABLE_NAME+" set "+
                              DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE+"="+number_type+", "+ DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE+"="+network_type+" where "+ DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER+" = "+num);

                  }while (cursor.moveToNext());
                }
            }catch (SQLiteException e){
                e.printStackTrace();
            }finally {
                if (cursor!=null)
                cursor.close();
            }
        }

    }
}
