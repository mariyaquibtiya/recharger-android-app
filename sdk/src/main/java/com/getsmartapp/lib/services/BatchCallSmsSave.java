package com.getsmartapp.lib.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.CallLog;

import com.getsmartapp.lib.data.CallCalculation;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.utils.SDKUtils;

/**
 * @author  nitesh.verma on 3/16/16.
 */
public class BatchCallSmsSave extends IntentService {

    SharedPrefManager sharedPrefManager;
    DualSimManager simManager;


    public BatchCallSmsSave() {
        super("BatchCallSmsSave");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPrefManager = new SharedPrefManager(this);
        simManager = new DualSimManager(this);
        saveBulkCalls();
    }

    private void saveBulkCalls() {
        Uri allCalls = CallLog.Calls.CONTENT_URI;
        String phone_number, person_name, network_type, number_type, call_type, country_name;
        int call_duration_in_sec, call_duration_in_min, is_night, is_roaming,mSimNo=0;
        long call_last_time_saved = sharedPrefManager.getLongValue(Constants.LAST_CALL_SAVED_TIME);
        SQLiteDatabase mDatabase = SdkDbHelper.getInstance(this).getWritableDatabase();
        CallCalculation callCalculation = new CallCalculation(this,sharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE),
                sharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME),sharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE));

        Cursor call_cursor = null;
        try {
            call_cursor = getContentResolver().query(allCalls, null, "date" + ">?", new
                    String[]{"" + call_last_time_saved}, null);
            if (simManager.isHasDualSim()) {
                int sim1_id = sharedPrefManager.getIntValue(Constants.SIM1_serial);
                int sim2_id = sharedPrefManager.getIntValue(Constants.SIM2_serial);
                if (sim1_id != 0 && sim2_id == 0) {
                 // singlw sim case or call varification not allowed
                    call_cursor = getContentResolver().query(allCalls, null, "date" + ">? and " + SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE) + " =?", new
                            String[]{"" + call_last_time_saved, sim1_id + ""}, null);
                    mSimNo=sim1_id;
                } else if (sim1_id == 0 && sim2_id != 0) {
                    mSimNo=sim2_id;
                    callCalculation = new CallCalculation(this,sharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE_2),
                            sharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME_2),sharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE_2));
                    call_cursor = getContentResolver().query(allCalls, null, "date" + ">? and " + SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE) + " =?", new
                            String[]{"" + call_last_time_saved, sim2_id + ""}, null);
                }
            }
            if (call_cursor != null && call_cursor.moveToFirst()) {
                do {
                    int id = call_cursor.getInt(call_cursor.getColumnIndex(CallLog.Calls._ID));
                    phone_number = call_cursor.getString(call_cursor.getColumnIndex(CallLog.Calls.NUMBER));
                    if (phone_number.length() > 8) {
                        callCalculation.setMobile(phone_number);
                        int call_int = call_cursor.getInt(call_cursor.getColumnIndex(CallLog.Calls.TYPE));
                        if (call_int != CallLog.Calls.MISSED_TYPE) {
                            call_type = callCalculation.getCallType(call_int);
                            person_name = call_cursor.getString(call_cursor.getColumnIndex(CallLog.Calls
                                    .CACHED_NAME));
                            network_type = callCalculation.getCallNetwork(phone_number);
                            if (!network_type.equals(DataStorageConstants.LANDLINE_CALLNETWORK)) {
                                if (phone_number.startsWith("+91")) {
                                    phone_number = phone_number.substring(3);
                                } else if (phone_number.startsWith("0")) {
                                    phone_number = phone_number.substring(1);
                                }
                            }
                            call_duration_in_sec = call_cursor.getInt(call_cursor.getColumnIndex(CallLog.Calls.DURATION));
                            call_duration_in_min = callCalculation.getMinCallDuration(call_duration_in_sec);
                            if (call_duration_in_min > 0) {
                                long start_time = call_cursor.getLong(call_cursor.getColumnIndex(CallLog.Calls.DATE));
                                //int actual_sim_no = call_cursor.getInt(call_cursor.getColumnIndex(SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE)));
                                is_night = callCalculation.getIsNightValue(start_time);
                                is_roaming = 0;
                                number_type = callCalculation.typeofCallandSms(phone_number);
                                if (callCalculation.isIndianPhone(phone_number)) {
                                    country_name = "India";
                                } else country_name = callCalculation.numberCountry(phone_number);

                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBContractor.CallDataEntry.CALL_ID,id);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER, phone_number);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_NAME, person_name);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_TYPE, call_type);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_IS_NIGHT, is_night);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC, call_duration_in_sec);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN, call_duration_in_min);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE, network_type);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE, number_type);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_IS_ROAMING, is_roaming);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_STARTTIME, start_time);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME, country_name);
                                contentValues.put(DBContractor.ReferenceAppEntry.COLUMN_SIMNO, mSimNo);
                                try {
                                    long cid = mDatabase.insertWithOnConflict(DBContractor.CallDataEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
                                    //Log.e("call_insert id", String.valueOf(cid));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } while (call_cursor.moveToNext());
            }

        } catch (SQLiteException e) {
            e.getLocalizedMessage();
        } finally {
            if (call_cursor != null) {
                call_cursor.close();
            }
        }
        sharedPrefManager.setLongValue(Constants.LAST_CALL_SAVED_TIME,System.currentTimeMillis());
    }
}
