package com.getsmartapp.lib.services;

import android.app.IntentService;
import android.content.Intent;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.utils.SDKUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author  Shalakha.Gupta on 27-05-2015.
 */



public class WelcomeEmailService extends IntentService {

    public WelcomeEmailService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(!SDKUtils.isConnectingToInternet(this))
            return;
        final SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        String email = SDKUtils.getPrimaryEmail(getApplicationContext());
        if (SDKUtils.isStringNullEmpty(email))
            return;

//        Log.w("Primary email", email);
        RestClient restClient = new RestClient(ApiConstants.BASE_RECHARGER_ADDRESS,null);
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        restClient.getApiService().sendWelcomeEmail(params, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                sharedPrefManager.setIntValue("is_email_send", 1);
            }

            @Override
            public void failure(RetrofitError error) {
                sharedPrefManager.setIntValue("is_email_send", 0);
            }
        });
    }


}





