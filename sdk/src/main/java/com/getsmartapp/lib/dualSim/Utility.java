package com.getsmartapp.lib.dualSim;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;

/**
 * @author Peeyush.Singh on 21-12-2015.
 */
public class Utility {

    public static Context context = null;

    public static String getNetworkTypeName(int type) {
        switch (type) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "CDMA - EvDo rev. 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "CDMA - EvDo rev. A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "CDMA - EvDo rev. B";
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "CDMA - 1xRTT";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "CDMA - eHRPD";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "iDEN";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPA+";
            default:
                return "UNKNOWN";
        }
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    public static String getAndroidId(Context mContext) {
        String androidId = "NA";
        try {
            androidId = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return androidId;
    }
    public static String getPhoneType(int phone_type){
        switch (phone_type){
             default:
                return "NONE";
            case 1:
                return "GSM";
            case 2:
                return "CDMA";
            case 3:
                return "SIP";

        }
    }
    public static String getRealSimState(String val){
        if (!val.equalsIgnoreCase("")) {
            int code = Integer.parseInt(val);
            switch (code) {
                default:
                    return "UNKNOWN";
                case 1:
                    return "ABSENT";
                case 2:
                    return "LOCKED - PIN REQUIRED";
                case 3:
                    return "LOCKED - PUK REQUIRED";
                case 4:
                    return "LOCKED - NETWORK PIN REQUIRED";
                case 5:
                    return "READY";
            }
        }else {
            return "";
        }
    }
    public static String getRealDataState(String val){
        if (!val.equalsIgnoreCase("")) {
            int code = Integer.parseInt(val);
            switch (code) {
                case 0:
                    return "DISCONNECTED";
                case 1:
                    return "CONNECTING";
                case 2:
                    return "CONNECTED";
                case 3:
                    return "SUSPENDED";
               default:
                   return "UNKNOWN";
            }
        }else {
            return "";
        }
    }
}