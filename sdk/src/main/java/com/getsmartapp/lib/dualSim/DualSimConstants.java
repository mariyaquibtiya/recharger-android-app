package com.getsmartapp.lib.dualSim;

/**
 * @author Peeyush.Singh on 21-12-2015.
 */
public class DualSimConstants {
    static final String SIM_OPERATOR_1 = "SIM_OPERATOR_1";
    static final String SIM_OPERATOR_2 = "SIM_OPERATOR_2";
    static final String NETWORK_OPERATOR_1 = "NETWORK_OPERATOR_1";
    static final String NETWORK_OPERATOR_2 = "NETWORK_OPERATOR_2";
    static final String SIM_SUPPORTED_COUNT = "SIM_SUPPORTED_COUNT";
    static final String DUAL_SIM_CLASS = "DUAL_SIM_CLASS";
    static final String SIM_VARINT = "SIM_VARINT";
    static final String SIM_SLOT_NAME_1 = "SIM_SLOT_NAME_1";
    static final String SIM_SLOT_NAME_2 = "SIM_SLOT_NAME_2";
    static final String SIM_SLOT_NUMBER_1 = "SIM_SLOT_NUMBER_1";
    static final String SIM_SLOT_NUMBER_2 = "SIM_SLOT_NUMBER_2";




}
