package com.getsmartapp.lib.dualSim;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Peeyush.Singh on 21-12-2015.
 */
public class MeterPreferences {

    private static final String  PREFERENCE_NAME = "PREFERENCE_NAME";
    public static void setStringPreference(Context context, String preference, String value)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preference,value);
        editor.apply();
    }
    public static String getStringPreference(Context context, String preference)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,0);
        return sharedPreferences.getString(preference,"");
    }

    public static int getInt(Context context, String preference)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,0);
        return sharedPreferences.getInt(preference, 0);
    }

    public static String getString(Context context, String preference, String defValue)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,0);
        return sharedPreferences.getString(preference, defValue);
    }

    public static void putInt(Context context, String preference, int value)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(preference,value);
        editor.apply();
    }

}
