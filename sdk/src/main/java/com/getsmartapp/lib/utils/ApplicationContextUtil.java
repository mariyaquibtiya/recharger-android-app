package com.getsmartapp.lib.utils;

import android.content.Context;

/**
 * @author  Peeyush.Singh on 28-03-2016.
 */
public class ApplicationContextUtil {
    private Context mContext;

    private static ApplicationContextUtil mApplicationContextUtil;
    public static synchronized ApplicationContextUtil getInstance()
    {
        if(mApplicationContextUtil==null)
            mApplicationContextUtil = new ApplicationContextUtil();
        return mApplicationContextUtil;
    }

    public void setApplicationContext(Context mContext) {
        this.mContext = mContext;
    }

    public Context getApplicationContext()
    {
        return mContext;
    }
}
