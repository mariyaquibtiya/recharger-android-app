package com.getsmartapp.lib.utils;

/**
 * @author  nitesh on 14/5/15.
 */

import android.content.Context;
import android.content.pm.PackageManager;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {


    private static final long ONE_DAY_MINS = 24 * 60;

    /**
     * <p>Checks if two dates are on the same day ignoring time.</p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     *
     * @param cal1 the first calendar, not altered, not null
     * @param cal2 the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    /**
     * <p>Checks if a date is today.</p>
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    /**
     * <p>Checks if a calendar date is today.</p>
     *
     * @param cal the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
//    public static boolean isToday(Calendar cal) {
//        return isSameDay(cal, Calendar.getInstance());
//    }

    /**
     * <p>Checks if the first date is before the second date ignoring time.</p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is before the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
//    public static boolean isBeforeDay(Date date1, Date date2) {
//        if (date1 == null || date2 == null) {
//            throw new IllegalArgumentException("The dates must not be null");
//        }
//        Calendar cal1 = Calendar.getInstance();
//        cal1.setTime(date1);
//        Calendar cal2 = Calendar.getInstance();
//        cal2.setTime(date2);
//        return isBeforeDay(cal1, cal2);
//    }

    /**
     * <p>Checks if the first calendar date is before the second calendar date ignoring time.</p>
     *
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is before cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
//    public static boolean isBeforeDay(Calendar cal1, Calendar cal2) {
//        if (cal1 == null || cal2 == null) {
//            throw new IllegalArgumentException("The dates must not be null");
//        }
//        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return true;
//        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return false;
//        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return true;
//        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return false;
//        return cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR);
//    }

    /**
     * <p>Checks if the first date is after the second date ignoring time.</p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is after the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
//    public static boolean isAfterDay(Date date1, Date date2) {
//        if (date1 == null || date2 == null) {
//            throw new IllegalArgumentException("The dates must not be null");
//        }
//        Calendar cal1 = Calendar.getInstance();
//        cal1.setTime(date1);
//        Calendar cal2 = Calendar.getInstance();
//        cal2.setTime(date2);
//        return isAfterDay(cal1, cal2);
//    }

    /**
     * <p>Checks if the first calendar date is after the second calendar date ignoring time.</p>
     *
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is after cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
//    public static boolean isAfterDay(Calendar cal1, Calendar cal2) {
//        if (cal1 == null || cal2 == null) {
//            throw new IllegalArgumentException("The dates must not be null");
//        }
//        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return false;
//        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return true;
//        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return false;
//        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return true;
//        return cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR);
//    }

    /**
     * <p>Checks if a date is after today and within a number of days in the future.</p>
     *
     * @param date the date to check, not altered, not null.
     * @param days the number of days.
     * @return true if the date day is after today and within days in the future .
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
//    public static boolean isWithinDaysFuture(Date date, int days) {
//        if (date == null) {
//            throw new IllegalArgumentException("The date must not be null");
//        }
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        return isWithinDaysFuture(cal, days);
//    }

    /**
     * <p>Checks if a calendar date is after today and within a number of days in the future.</p>
     *
     * @param cal  the calendar, not altered, not null
     * @param days the number of days.
     * @return true if the calendar date day is after today and within days in the future .
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
//    public static boolean isWithinDaysFuture(Calendar cal, int days) {
//        if (cal == null) {
//            throw new IllegalArgumentException("The date must not be null");
//        }
//        Calendar today = Calendar.getInstance();
//        Calendar future = Calendar.getInstance();
//        future.add(Calendar.DAY_OF_YEAR, days);
//        return (isAfterDay(cal, today) && !isAfterDay(cal, future));
//    }

//    public static boolean isWithinDaysPast(Date date, int days) {
//        if (date == null) {
//            throw new IllegalArgumentException("The date must not be null");
//        }
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        return isWithinDaysPast(cal, days);
//    }

//    public static boolean isWithinDaysPast(Calendar cal, int days) {
//        if (cal == null) {
//            throw new IllegalArgumentException("The date must not be null");
//        }
//        Calendar today = Calendar.getInstance();
//        Calendar past = Calendar.getInstance();
//        past.add(Calendar.DAY_OF_YEAR, -days);
//        return (isBeforeDay(cal, today) && isAfterDay(cal, past));
//    }


    /**
     * Returns the given date with the time set to the start of the day.
     */
    public static Date getStart(Date date) {
        return clearTime(date);
    }

    public static long dateToTimestamp(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.getTimeInMillis();
    }

    /**
     * Returns the given date with the time values cleared.
     */
    public static Date clearTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /** Determines whether or not a date has any time values (hour, minute,
     * seconds or millisecondsReturns the given date with the time values cleared. */

    /**
     * Determines whether or not a date has any time values.
     *
     * @param date The date.
     * @return true iff the date is not null and any of the date's hour, minute,
     * seconds or millisecond values are greater than zero.
     */
//    public static boolean hasTime(Date date) {
//        if (date == null) {
//            return false;
//        }
//        Calendar c = Calendar.getInstance();
//        c.setTime(date);
//        if (c.get(Calendar.HOUR_OF_DAY) > 0) {
//            return true;
//        }
//        if (c.get(Calendar.MINUTE) > 0) {
//            return true;
//        }
//        if (c.get(Calendar.SECOND) > 0) {
//            return true;
//        }
//        return c.get(Calendar.MILLISECOND) > 0;
//    }

    /**
     * Returns the given date with time set to the end of the day
     */
//    public static Date getEnd(Date date) {
//        if (date == null) {
//            return null;
//        }
//        Calendar c = Calendar.getInstance();
//        c.setTime(date);
//        c.set(Calendar.HOUR_OF_DAY, 23);
//        c.set(Calendar.MINUTE, 59);
//        c.set(Calendar.SECOND, 59);
//        c.set(Calendar.MILLISECOND, 999);
//        return c.getTime();
//    }

    /**
     * Returns the maximum of two dates. A null date is treated as being less
     * than any non-null date.
     */
    public static Date max(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.after(d2)) ? d1 : d2;
    }

    /**
     * Returns the minimum of two dates. A null date is treated as being greater
     * than any non-null date.
     */
    public static Date min(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.before(d2)) ? d1 : d2;
    }

    /**
     * The maximum date possible.
     */
//    public static Date MAX_DATE = new Date(Long.MAX_VALUE);
//
//
//    public static Date formateTimeStampToDate(long timestamp) {
//        return new Date(timestamp * 1000);
//    }
    public static String getDateInDersiredFormat(String format, Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
//        Date d = new Date(milliseconds);
        return simpleDateFormat.format(date);
    }

    public static String getDateInDersiredFormat(String format, String dateInString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        try {

            Date date = simpleDateFormat.parse(dateInString);
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

//    public static String getMonthDateFromTimestamp(long timestamp) {
//        SimpleDateFormat sdf = new SimpleDateFormat("dd", Locale.getDefault());
//        return sdf.format(new Date(timestamp));
//    }
//
//    public static String getDayHourFromTimes(long timestamp) {
//        SimpleDateFormat sdf = new SimpleDateFormat("HH", Locale.getDefault());
//        return sdf.format(new Date(timestamp));
//    }

    public static String getWeekDayFromTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return String.valueOf(calendar.get(Calendar.DAY_OF_WEEK));
    }

    public static String getWeekFromTimestamp(long timestamp) {
        if (timestamp > System.currentTimeMillis() - (6L * 24 * 3600 * 1000
                                                              + Constants.GAP_TIMESTAMP)) {
            return "4";
        } else if (timestamp > System.currentTimeMillis() - (13L * 24 * 3600 * 1000
                                                                     + Constants.GAP_TIMESTAMP)) {
            return "3";
        } else if (timestamp > System.currentTimeMillis() - (20L * 24 * 3600 * 1000
                                                                     + Constants.GAP_TIMESTAMP)) {
            return "2";
        } else if (timestamp > System.currentTimeMillis() - (27L * 24 * 3600 * 1000
                                                                     + Constants.GAP_TIMESTAMP)) {
            return "1";
        }
        return "1";
    }


    public static String getWeekFromTimestampPostPaid(Context context, long timestamp) {
        SharedPrefManager mSharedPrefManager = new SharedPrefManager(context);

        long postpaidTimeStamp = mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_START_DATE_TIME_MILLIS);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(postpaidTimeStamp);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND, 1);


        if((calendar.getTimeInMillis()<= timestamp)&& (timestamp <= calendar.getTimeInMillis()+7L*24*3600*1000L))
        {
            return "1";
        }
        else if((calendar.getTimeInMillis()<= timestamp)&& (timestamp <= calendar.getTimeInMillis()+14L*24*3600*1000L))
        {
            return "2";
        }
        else if((calendar.getTimeInMillis()<= timestamp)&& (timestamp <= calendar.getTimeInMillis()+21L*24*3600*1000L))
        {
            return "3";
        }
        else if((calendar.getTimeInMillis()<= timestamp)&& (timestamp <= calendar.getTimeInMillis()+28L*24*3600*1000L))
        {
            return "4";
        }

        if (timestamp > System.currentTimeMillis() - (6L * 24 * 3600 * 1000
                + Constants.GAP_TIMESTAMP)) {
            return "4";
        } else if (timestamp > System.currentTimeMillis() - (13L * 24 * 3600 * 1000
                + Constants.GAP_TIMESTAMP)) {
            return "3";
        } else if (timestamp > System.currentTimeMillis() - (20L * 24 * 3600 * 1000
                + Constants.GAP_TIMESTAMP)) {
            return "2";
        } else if (timestamp > System.currentTimeMillis() - (27L * 24 * 3600 * 1000
                + Constants.GAP_TIMESTAMP)) {
            return "1";
        }
        return "1";
    }

    public static String getHoursFromTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        int hour_diff = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        if ((hour_diff % 6) == 0) {
            return ((hour_diff / 6) == 0) ? "1" : (hour_diff / 6) + "";
        } else
            return ((hour_diff / 6) == 4) ? "4" : ((hour_diff / 6) + 1) + "";
    }

    public static boolean sixHourBefore(long timestamp) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(timestamp);
        cal2.setTimeInMillis(System.currentTimeMillis());
        long diff = System.currentTimeMillis() - timestamp;
        long minute = (diff / (60 * 1000));
//        int hour1 = cal1.get(Calendar.HOUR_OF_DAY);
//        int hour2 = cal2.get(Calendar.HOUR_OF_DAY);
        if (minute >= 360) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean twelweHourBefore(long timestamp) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(timestamp);
        cal2.setTimeInMillis(System.currentTimeMillis());
        long diff = System.currentTimeMillis() - timestamp;
        long minute = (diff / (60 * 1000));
        if (minute >= 720) {
            return true;
        } else {
            return false;
        }
    }

//    public static boolean fifMinBefore(long timestamp) {
//        Calendar cal1 = Calendar.getInstance();
//        Calendar cal2 = Calendar.getInstance();
//        cal1.setTimeInMillis(timestamp);
//        cal2.setTimeInMillis(System.currentTimeMillis());
//        int hour1 = cal1.get(Calendar.HOUR_OF_DAY);
//        int hour2 = cal2.get(Calendar.HOUR_OF_DAY);
//        int min1 = cal1.get(Calendar.MINUTE);
//        int min2 = cal2.get(Calendar.MINUTE);
//
//        int totalMins1 = hour1 * 60 + min1;
//        int totalMins2 = hour2 * 60 + min2;
//        if (totalMins2 - totalMins1 >= 1) {
//            return true;
//        } else {
//            return false;
//        }
//
//    }

    public static String getMonthFromInt(int i) {
        switch (i) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sept";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";

        }
        return "";
    }

    public static boolean nDaysGoneSinceOnBoard(Context context, int days) throws PackageManager.NameNotFoundException {

        SharedPrefManager shrd = new SharedPrefManager(context);
        long install_time = shrd.getLongValue(Constants.ON_BOARDING_TIME);
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(install_time);
        cal2.setTimeInMillis(System.currentTimeMillis());
        if (cal2.get(Calendar.DAY_OF_YEAR) - cal1.get(Calendar.DAY_OF_YEAR) > days) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean dataSentBeforeSevenDays(Date sentdate) {

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTimeInMillis(System.currentTimeMillis());
        c2.setTime(sentdate);
        if (c1.get(Calendar.DAY_OF_YEAR) - c2.get(Calendar.DAY_OF_YEAR) > 7) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean twoDaysBefore(long timestamp) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(timestamp);
        cal2.setTimeInMillis(System.currentTimeMillis());
        long diff = System.currentTimeMillis() - timestamp;
        long minute = (diff / (60 * 1000));
//        int hour1 = cal1.get(Calendar.HOUR_OF_DAY);
//        int hour2 = cal2.get(Calendar.HOUR_OF_DAY);
        if (minute >= 2880) {
            return true;
        } else {
            return false;
        }
    }


    public static String getDateInDersiredFormat(String actualFormat,String desiredFormat, String dateInString) {

//        Feb 6, 2016 2:53:35 PM
        SimpleDateFormat dateFormat = new SimpleDateFormat(actualFormat, Locale.getDefault());
        SimpleDateFormat targetFormat = new SimpleDateFormat(desiredFormat, Locale.getDefault());
        String formattedDate = null;
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateInString);
            formattedDate = targetFormat.format(convertedDate);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateFromTimeInMillis(long timeInMillis)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
//        2016-03-10;
//        String date = Calendar.getInstance().get(Calendar.DATE)+"-"+(Calendar.getInstance().get(Calendar.MONTH)+1)+"-"+Calendar.getInstance().get(Calendar.YEAR);

        String month = (calendar.get(Calendar.MONTH)+1)+"";

        if(month.trim().length()==1)
            month="0"+month;
        String date1 = calendar.get(Calendar.DATE)+"";
        if(date1.trim().length()==1)
            date1 = "0"+date1;

        String date = calendar.get(Calendar.YEAR)+"-"+month+"-"+date1;
        return date;
    }

    public static boolean nDaysBefore(long timestamp, int days) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(timestamp);
        cal2.setTimeInMillis(System.currentTimeMillis());
        long diff = System.currentTimeMillis() - timestamp;
        long minute = (diff / (60 * 1000));
//        int hour1 = cal1.get(Calendar.HOUR_OF_DAY);
//        int hour2 = cal2.get(Calendar.HOUR_OF_DAY);
        if (minute >= days * ONE_DAY_MINS) {
            return true;
        } else {
            return false;
        }
    }

    public static long dateToYesterdayTimestamp() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE,1);
        return c.getTimeInMillis();
    }

    public static String getPreviousDayDate()
    {
        long previousDateTime = Calendar.getInstance().getTimeInMillis() - 24*60*60*1000;
        return getDateFromTimeInMillis(previousDateTime);
    }
}


