package com.getsmartapp.lib.utils;

import android.util.Log;

/**
 * @author  nitesh.verma on 1/12/16.
 */
public class SdkLg {

    public static void e(String message){
        Log.e("SmartSDK",message);
    }

    public static void d(String message){
        Log.d("SmartSDK",message);
    }
    public static void w(String message){
        Log.w("SmartSDK",message);
    }

}
