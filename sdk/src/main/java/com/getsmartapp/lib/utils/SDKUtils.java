package com.getsmartapp.lib.utils;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.getsmartapp.lib.R;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.DataModel;
import com.getsmartapp.lib.model.MobileDataVal;
import com.getsmartapp.lib.observer.SinchCallObserver;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.services.BulkAggregateDataService;
import com.getsmartapp.lib.services.DailyAggregationService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static android.provider.CallLog.Calls.CONTENT_URI;

/**
 * @author nitesh.verma on 12/14/15.
 */
public class SDKUtils {

    public static HashMap<String, Integer> simsSubIds(Context context, int table) {
        Cursor cursor = null;
        int mysub_id = -1;
        int no_of_sim = 0;
        HashMap<String, Integer> sims_subid = new HashMap<>();
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                return sims_subid;
            }
            cursor = context.getContentResolver().query(CONTENT_URI, new String[]{getSimIdColumnName(getPhoneManufacturer(), table)}, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int sub_id = cursor.getInt(cursor.getColumnIndex(getSimIdColumnName(getPhoneManufacturer(), table)));
                    if (sub_id > 0) {
                        if (mysub_id != sub_id && !sims_subid.containsValue(sub_id)) {
                            no_of_sim++;
                            sims_subid.put("sim" + no_of_sim + " id", sub_id);
                            mysub_id = sub_id;
                        }
                    }
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return sims_subid;
    }

    public static int getSubIdFromSimSerial(Context context, String serial_id) {
        int sim_id = -1;
        if (getPhoneManufacturer() == PhoneModal.MOTOROLA) {
            Cursor cursor = null;
            try {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                    return sim_id;
                }
                cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "sim_sn=?", new String[]{serial_id}, null);
                if (cursor != null && cursor.moveToFirst()) {
                    sim_id = cursor.getInt(cursor.getColumnIndex(getSimIdColumnName(getPhoneManufacturer(), Constants.CALL_TABLE)));
                }

            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

        }
        return sim_id;
    }

    public static int getSlotfromSimId(SharedPrefManager sharedPrefManager, int sim_id) {
        return sharedPrefManager.getIntValue("sim" + sim_id + "_slot");
    }

    public static void setSlotForSimId(SharedPrefManager sharedPrefManager, int slot, int sim_id) {
        sharedPrefManager.setIntValue("sim" + sim_id + "_slot", slot);
    }

    public static String getOnboardedNumFromSimId(SharedPrefManager sharedPrefManager, int sim_id) {
        int sim_onboarder = sharedPrefManager.getIntValue(Constants.NO_OF_SIM_ONBOARDED);
        if (sim_onboarder == 1)
            return sharedPrefManager.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER);
        else if (sim_onboarder > 1) {
            return sharedPrefManager.getStringValue("simid" + sim_id + "number", null);
        }
        return null;
    }

    public static int getRoamingState(Constants.Roaming is_roaming) {
        switch (is_roaming) {
            case ALL:
                return 2;
            case YES:
                return 1;
            case NO:
                return 0;
        }
        return 2;
    }

    public static String getConnectionState(Constants.ConnType connType) {
        switch (connType) {
            case ALL:
                return null;
            case STD:
                return Constants.STD_NUMBER;
            case ISD:
                return Constants.ISD_NUMBER;
            case LOCAL:
                return Constants.LOCAL_NUMBER;
        }
        return null;
    }

    public static String getNetworkState(Constants.NetworkType networkType) {
        switch (networkType) {
            case ALL:
                return null;
            case SAME:
                return Constants.SAME_CALLNETWORK;
            case OTHER:
                return Constants.OTHER_CALLNETWORK;
            case LANDLINE:
                return Constants.LANDLINE_CALLNETWORK;
        }
        return null;
    }

    public static int getDayNight(Constants.DayNight is_night) {
        switch (is_night) {
            case ALL:
                return 2;
            case DAY:
                return 0;
            case NIGHT:
                return 1;
        }
        return 2;
    }

    public static String getService(Constants.Service service) {
        switch (service) {
            case ALL:
                return null;
            case INCOMING:
                return Constants.INCOMING_CALL_TYPE;
            case OUTGOING:
                return Constants.OUTGOING_CALL_TYPE;
        }
        return null;
    }

    public static String getSmsService(Constants.Service service) {
        switch (service) {
            case ALL:
                return null;
            case INCOMING:
                return Constants.INBOX_SMS_TYPE;
            case OUTGOING:
                return Constants.SENT_SMS_TYPE;
        }
        return null;
    }

    public static int getDateCount(SQLiteDatabase sqLiteDatabase, int table, int sim_id, int is_roaming) {
        int count = 0;
        Cursor cursor = getDataCountCursor(sqLiteDatabase, table, sim_id, is_roaming);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static Cursor getDataCountCursor(SQLiteDatabase sqLiteDatabase, int table, int sim_id, int is_roaming) {
        String q = "";
        switch (table) {
            case Constants.CALL_TABLE:
                if (sim_id != 0) {
                    if (sim_id > 0) {
                        q = "select count(*) from (select strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "=" + is_roaming + " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "= " + sim_id + " group by " + ApiConstants.metadate + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " desc)";
                    } else {
                        q = "select count(*) from (select strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "=" + is_roaming + " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "<> " + sim_id + " group by " + ApiConstants.metadate + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " desc)";
                    }
                } else {
                    q = "select count(*) from (select count(*),strftime('%Y-%m-%d'," + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "=" + is_roaming + " group by " + ApiConstants.metadate + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " desc)";
                }
                break;
            case Constants.SMS_TABLE:
                if (sim_id != 0) {
                    if (sim_id > 0) {
                        q = "select count(*) from (select strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.SmsDataEntry.TABLE_NAME + " where " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=" + is_roaming + " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "= " + sim_id + " group by " + ApiConstants.metadate + " order by " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " desc)";
                    } else {
                        q = "select count(*) from (select strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.SmsDataEntry.TABLE_NAME + " where " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=" + is_roaming + " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "<> " + sim_id + " group by " + ApiConstants.metadate + " order by " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " desc)";
                    }
                } else {
                    q = "select count(*) from (select count(*),strftime('%Y-%m-%d'," + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.SmsDataEntry.TABLE_NAME + " where " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=" + is_roaming + " group by " + ApiConstants.metadate + " order by " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " desc)";
                }
                break;

        }
        return sqLiteDatabase.rawQuery(q, null);
    }

    public static long getLastLimitTimeStamp(SQLiteDatabase sqLiteDatabase, int table, int sim_id) {
        String q = "";
        String simstr = "";
        if (sim_id != 0) {
            simstr = " where " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " = " + sim_id;
        }
        Cursor cursor = null;
        switch (table) {
            case Constants.CALL_TABLE:
                q = "select " + DBContractor.CallDataEntry.COLUMN_STARTTIME + ",strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.CallDataEntry.TABLE_NAME + simstr + " group by " + ApiConstants.metadate + " order by " + ApiConstants.metadate + " desc limit 1 offset " + Constants.NON_ROAMING_DATA_LIMIT;
                cursor = sqLiteDatabase.rawQuery(q, null);
                if (cursor != null && cursor.moveToFirst()) {
                    return cursor.getLong(cursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_STARTTIME));
                }
                break;
            case Constants.SMS_TABLE:
                q = "select " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + ",strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " + DBContractor.SmsDataEntry.TABLE_NAME + simstr + " group by " + ApiConstants.metadate + " order by " + ApiConstants.metadate + " desc limit 1 offset " + Constants.NON_ROAMING_DATA_LIMIT;
                cursor = sqLiteDatabase.rawQuery(q, null);
                if (cursor != null && cursor.moveToFirst()) {
                    return cursor.getLong(cursor.getColumnIndex(DBContractor.SmsDataEntry.COLUMN_DATE_TIME));
                }
                break;
//            case Constants.INTERNET_TABLE:
//                q = "select " + DBContractor.InternetSessionEntry.COLUMN_TIMESTAMP + " from " + DBContractor.InternetSessionEntry.TABLE_NAME + " group by " + DBContractor.InternetSessionEntry.COLUMN_DATE + " order by " + DBContractor.InternetSessionEntry.COLUMN_DATE + " desc limit 1 offset " + Constants.NON_ROAMING_DATA_LIMIT;
//                cursor = sqLiteDatabase.rawQuery(q, null);
//                if (cursor != null && cursor.moveToFirst()) {
//                    return cursor.getLong(cursor.getColumnIndex(DBContractor.InternetSessionEntry.COLUMN_TIMESTAMP));
//                }
//                break;
//            case Constants.APP_TABLE:
//                q = "select " + DBContractor.AppDataUsageEntry.COLUMN_TIMESTAMP + " from " + DBContractor.AppDataUsageEntry.TABLE_NAME + " group by " + DBContractor.AppDataUsageEntry.COLUMN_DATE + " order by " + DBContractor.AppDataUsageEntry.COLUMN_DATE + " desc limit 1 offset " + Constants.NON_ROAMING_DATA_LIMIT;
//                cursor = sqLiteDatabase.rawQuery(q, null);
//                if (cursor != null && cursor.moveToFirst()) {
//                    return cursor.getLong(cursor.getColumnIndex(DBContractor.AppDataUsageEntry.COLUMN_TIMESTAMP));
//                }
//                break;
        }
        return 0;
    }

    public enum PhoneModal {
        MICROMAX, LENOVO, SAMSUNG, XIAOMI, ONEPLUS, MOTOROLA, YUREKA, HTC
    }


    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();


    public static void getTotal() {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.appendWhere("");
        String q = queryBuilder.buildQuery(null, null, null, null, null, null);
    }

    public static long getLastDataTypeEntry(SQLiteDatabase db, int data_type, int sim_id) {
        Cursor cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "= ?", new String[]{String.valueOf(data_type)}, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        if (sim_id != 0) {
            cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "= ? and " + DBContractor.SendToServerEntry.COLUMN_SIM_ID + " = ?", new String[]{String.valueOf(data_type), sim_id + ""}, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        }
        if (cursor != null && cursor.moveToFirst()) {
            long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_TIMESTAMP));
            return timestamp;
        } else {
            return 0;
        }
    }

    public static String getHashedDeviceID(String deviceID) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
//            SdkLg.e("DEVICEID-HASHING", e.getMessage());
            e.printStackTrace();
        }
        messageDigest.reset();
        messageDigest.update(deviceID.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        return new String(bytesToHex(resultByte)).toLowerCase();
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static DataModel extrapoleteForDays(String dataPref) {

        long time_since_boot = SystemClock.elapsedRealtime();
        MobileDataVal mobileDataVal = giveMobileDataVal();
        float data_in_kb = mobileDataVal.getMobile_data() / 1024f;
        float wifi_in_kb = mobileDataVal.getWifi_data() / 1024f;
        float data_mob = (ApiConstants.EXTRAPOLATION_DAYS * data_in_kb * 3600 * 24 * 1000) / time_since_boot;
        float data_wifi = (ApiConstants.EXTRAPOLATION_DAYS * wifi_in_kb * 3600 * 24 * 1000) / time_since_boot;
        switch (dataPref.toLowerCase()) {
            case "2g":
                return new DataModel(0, 0, data_mob, data_wifi, "", "kilobytes", "KB");
            case "3g":
                return new DataModel(0, data_mob, 0, data_wifi, "", "kilobytes", "KB");
            case "4g":
                return new DataModel(data_mob, 0, 0, data_wifi, "", "kilobytes", "KB");
        }
        return null;
    }

    public static MobileDataVal giveMobileDataVal() {
        long mob_rxbyte = TrafficStats.getMobileRxBytes();
        long mob_txbyte = TrafficStats.getMobileTxBytes();
        long mob_total = mob_rxbyte + mob_txbyte;
        long total_rxbyte = TrafficStats.getTotalRxBytes();
        long total_txbyte = TrafficStats.getTotalTxBytes();
        long total_totalData = total_rxbyte + total_txbyte;
        long total_wifi = total_totalData - mob_total;
        return new MobileDataVal(mob_total, total_totalData, total_wifi);
    }

    public static boolean isLoggedIn(Context context) {
        String value = getLoggedInUser(context);
        return value != null;
    }

    public static String getLoggedInUser(Context context) {
        SharedPrefManager mSharedPref = new SharedPrefManager(context);
        String value = mSharedPref.getStringValue(Constants.USER_DATA);
        if (value != null && !value.equals("")) {
            return value;
        } else {
            return null;
        }
    }


    public static long getLastSendEntry(SQLiteDatabase db, int send_type, int data_type, int sim_id) {
        Cursor cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "=? and " + DBContractor.SendToServerEntry.COLUMN_SEND_TYPE + "=? ", new String[]{data_type + "", send_type + ""}, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        if (sim_id != 0) {
            cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "=?  and " + DBContractor.SendToServerEntry.COLUMN_SEND_TYPE + "= ? and " + DBContractor.SendToServerEntry.COLUMN_SIM_ID + " = ?", new String[]{data_type + "", send_type + "", sim_id + ""}, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        }
        if (cursor != null && cursor.moveToFirst()) {
            long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_TIMESTAMP));
            return timestamp;
        } else {
            return 0;
        }
    }


    public static void checkandSendDataToServer(Context context) {

        SdkDbHelper dbHelper = SdkDbHelper.getInstance(context);//new DBHelper(context);
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        SharedPrefManager shrd = new SharedPrefManager(context);
        DualSimManager simManager = new DualSimManager(context);
        Intent serviceIntent = null;
        if (!shrd.getBooleanValue(Constants.SENDING_DATA)) {
            if (shrd.getIntValue(Constants.BULK_DATA_SENT_ONCE) == 1) {
                // ============ For Dual sim case ===========
                if (simManager.isHasDualSim() && shrd.getIntValue(Constants.NO_OF_SIM_ONBOARDED) > 1) {
                    int sim1 = shrd.getIntValue(Constants.SIM1_serial);
                    int sim2 = shrd.getIntValue(Constants.SIM2_serial);
                    long lastcalltime_1 = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.CALLSMS_DATA_TYPE, sim1);
                    long lastdatatime_1 = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.INTERNET_DATA_TYPE, 0);
                    long lastcalltime_2 = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.CALLSMS_DATA_TYPE, sim2);

                    if (lastcalltime_1 != 0 && lastdatatime_1 != 0 && lastcalltime_2 != 0) {
                        if (DateUtil.isToday(new Date(lastcalltime_1)) && DateUtil.isToday(new Date(lastdatatime_1))
                                && DateUtil.isToday(new Date(lastcalltime_2))) {
                            if (DateUtil.sixHourBefore(lastcalltime_1) && DateUtil.sixHourBefore(lastdatatime_1) && DateUtil.sixHourBefore(lastcalltime_2)) {
                                serviceIntent = new Intent(context, DailyAggregationService.class);
                                context.startService(serviceIntent);
                            }
                        }
                    } else {
                        serviceIntent = new Intent(context, BulkAggregateDataService.class);
                        context.startService(serviceIntent);
                    }
                }
                // ========== For single sim case ==========
                else {
                    long lastcalltime_1 = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.CALLSMS_DATA_TYPE, 0);
                    long lastdatatime_1 = SDKUtils.getLastDataTypeEntry(sqLiteDatabase, ApiConstants.INTERNET_DATA_TYPE, 0);
                    if (lastcalltime_1 != 0 && lastdatatime_1 != 0) {
                        if (DateUtil.isToday(new Date(lastcalltime_1)) && DateUtil.isToday(new Date(lastdatatime_1))) {
                            if (DateUtil.sixHourBefore(lastcalltime_1) && DateUtil.sixHourBefore(lastdatatime_1)) {
                                serviceIntent = new Intent(context, DailyAggregationService.class);
                                context.startService(serviceIntent);
                            }
                        } else {
                            serviceIntent = new Intent(context, BulkAggregateDataService.class);
                            context.startService(serviceIntent);
                        }
                    } else {
                        serviceIntent = new Intent(context, BulkAggregateDataService.class);
                        context.startService(serviceIntent);
                    }
                }
            } else {
                serviceIntent = new Intent(context, BulkAggregateDataService.class);
                context.startService(serviceIntent);
            }
        }

    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    // Checking for all possible internet providers
/*    public static boolean isConnectingToInternet(Context ctx) {

        try {
            ConnectivityManager connectivity =
                    (ConnectivityManager) ctx.getSystemService(
                            Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }*/


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getPrimaryEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        if (accountManager != null) {
            return getAccount(accountManager);
        } else {
            return null;
        }
    }

    private static String getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccounts();
        if (accounts != null && accounts.length > 0) {
            for (Account account : accounts) {
                if (account.type.equalsIgnoreCase("com.google")) {
                    return account.name;
                }
            }
        }
        return null;
    }

    public static boolean isStringNullEmpty(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("null") || value.trim().length() == 0)
            return true;
        else
            return false;
    }


    public static void setDataForTesting(Context context, String type, String data, String date,
                                         String status) {
        SharedPrefManager mSharedPreference = new SharedPrefManager(context);
        String jsonData = mSharedPreference.getStringValue(Constants.BACKGROUNDSERVICE);
        try {
            JSONObject newJson = new JSONObject();
            newJson.put("Service Type", type);
            newJson.put("Service Data", data);
            DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
            newJson.put("Service Time", df.format(Calendar.getInstance().getTime()));
            newJson.put("Service Status", status);
            if (!jsonData.equals("")) {
                JSONObject jsonObject = new JSONObject(jsonData);
                JSONArray jsonArray = jsonObject.optJSONArray("data");
                jsonArray.put(newJson);
                jsonObject.put("data", jsonArray);
                mSharedPreference.setStringValue(Constants.BACKGROUNDSERVICE, jsonObject.toString());
            } else {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(newJson);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("data", jsonArray);
                mSharedPreference.setStringValue(Constants.BACKGROUNDSERVICE, jsonObject.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void hide_keyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int dp2px(Context context, float dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return Math.round(px);
    }

    public static boolean isOnBoarded(Context context) {
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = SdkDbHelper.getInstance(context).getReadableDatabase();
        try {
            cursor = sqLiteDatabase.query(DBContractor.CallDataEntry.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                return true;
            } else return false;
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return false;
    }

    public static String getSimIdColumnName(PhoneModal phonemodal, int table) {
        switch (phonemodal) {
            case LENOVO:
                if (table == Constants.CALL_TABLE)
                    return "simid";
                if (table == Constants.SMS_TABLE)
                    return "sub_id";
            case MICROMAX:
                if (table == Constants.CALL_TABLE)
                    return "simid";
                if (table == Constants.SMS_TABLE)
                    return "sim_id";
            case SAMSUNG:
                if (table == Constants.CALL_TABLE)
                    return "subscription_id";
                if (table == Constants.SMS_TABLE)
                    return "sub_id";
            case MOTOROLA:
                if (table == Constants.CALL_TABLE)
                    return "subscription_id";
                if (table == Constants.SMS_TABLE)
                    return "sub_id";
            case XIAOMI:
                if (table == Constants.CALL_TABLE)
                    return "simid";
                if (table == Constants.SMS_TABLE)
                    return "sim_id";
            case ONEPLUS:
                if (table == Constants.CALL_TABLE)
                    return "subscription_id";
                if (table == Constants.SMS_TABLE)
                    return "sub_id";
                break;
            case HTC:
                if (table == Constants.CALL_TABLE)
                    return "simid";
                if (table == Constants.SMS_TABLE)
                    return "sim_id";
                break;
            default:
                return "subscription_id";

        }
        return "subscription_id";
    }

    public static PhoneModal getPhoneManufacturer() {
        String manu = Build.MANUFACTURER;
        if (manu.equalsIgnoreCase("lenovo")) {
            return PhoneModal.LENOVO;
        } else if (manu.equalsIgnoreCase("micromax")) {
            return PhoneModal.MICROMAX;
        } else if (manu.equalsIgnoreCase("samsung")) {
            return PhoneModal.SAMSUNG;
        } else if (manu.equalsIgnoreCase("motorola")) {
            return PhoneModal.MOTOROLA;
        } else if (manu.equalsIgnoreCase("oneplus")) {
            return PhoneModal.ONEPLUS;
        } else if (manu.equalsIgnoreCase("xiaomi")) {
            return PhoneModal.XIAOMI;
        } else if (manu.equalsIgnoreCase("Yu")) {
            return PhoneModal.YUREKA;
        } else if (manu.equalsIgnoreCase("HTC")) {
            return PhoneModal.HTC;
        } else {
            return PhoneModal.SAMSUNG;
        }
    }

    public static boolean checkIfLastSendSixHoursBefore(SQLiteDatabase mDatabase) {
        long dailysend_call_type = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.CALLSMS_DATA_TYPE, 0);
        long dailysend_data_type = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.INTERNET_DATA_TYPE, 0);
        long dailysend_app_type = SDKUtils.getLastSendEntry(mDatabase, ApiConstants.DAILY_DATA_TYPE, ApiConstants.APP_DATA_TYPE, 0);
        if (dailysend_call_type != 0 && dailysend_data_type != 0 && dailysend_app_type != 0) {
            if (DateUtil.isToday(new Date(dailysend_call_type)) && DateUtil.isToday(new Date(dailysend_app_type)) && DateUtil.isToday(new Date(dailysend_data_type))) {
                if (DateUtil.sixHourBefore(dailysend_app_type) && DateUtil.sixHourBefore(dailysend_call_type) && DateUtil.sixHourBefore(dailysend_data_type)) {
                    return true;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public static int getLastCallSimId(Context context, DualSimManager simManager) {
        Cursor cursor = null;
        int mysub_id = -1;
        int no_of_sim = 0;
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                return mysub_id;
            }
            if (simManager.isHasDualSim()) {
                cursor = context.getContentResolver().query(CONTENT_URI, new String[]{getSimIdColumnName(getPhoneManufacturer(), Constants.CALL_TABLE)}, CallLog.Calls.NUMBER + "=?", new String[]{SinchCallObserver.getInstance().getmNumber()}, CallLog.Calls.DATE + " DESC LIMIT 1");
            } else {
                cursor = context.getContentResolver().query(CONTENT_URI, new String[]{getSimIdColumnName(getPhoneManufacturer(), Constants.CALL_TABLE)}, null, null, CallLog.Calls.DATE + " DESC LIMIT 1");
            }
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int sub_id = cursor.getInt(cursor.getColumnIndex(getSimIdColumnName(getPhoneManufacturer(), Constants.CALL_TABLE)));
                    mysub_id = sub_id;
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return mysub_id;
    }

    public static void startDataCapture(Context context){
        try {
//            InternetDataDBWrapper.getInstance(this);
            InternetDataUsageUtil.getInstance(context).setAppName(context.getString(R.string.app_name));
            InternetDataUsageUtil.getInstance(context).initialiseDataCapture(context);
            InternetDataUsageUtil.getInstance(context).updateData(context);

        }catch (Exception e){}
    }

    public static void setLastDataTypeDateEntry(Context context, long date)
    {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        sharedPrefManager.setLongValue(Constants.LAST_INTERNET_DATA_SYNC_TIME_BULK, date);
    }

    public static long getLastDataTypeDateEntry(Context context) {

        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);

        return sharedPrefManager.getLongValue(Constants.LAST_INTERNET_DATA_SYNC_TIME_BULK);

       /* Cursor cursor = db.query(DBContractor.SendToServerEntry.TABLE_NAME, null, DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + "= ?", new String[]{String.valueOf(data_type)}, null, null, DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " DESC ", "1");
        if (cursor != null && cursor.moveToFirst()) {
            long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_TIMESTAMP));
            return timestamp;
        } else {
            return 0;
        }*/
    }
}
