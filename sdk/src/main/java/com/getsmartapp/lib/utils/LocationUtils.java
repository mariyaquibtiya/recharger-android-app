package com.getsmartapp.lib.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.Geocode;
import com.getsmartapp.lib.retrofit.GoogleMapClient;
import com.getsmartapp.lib.constants.Constants;

import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * @author  nitesh.verma on 1/18/16.
 */

public class LocationUtils {

    private String locality,state,country,feature;
    private Context context;
    private static long initTime;
    private static LocationUtils mLocationUtils;
    private SharedPrefManager mSharedPrefManager;

    public static LocationUtils getInstance(Context context)
    {
        if(mLocationUtils==null)
            mLocationUtils = new LocationUtils(context);
        return mLocationUtils;
    }

    private LocationUtils(Context context) {
        this.context = context;
        mSharedPrefManager = new SharedPrefManager(context);
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public void setAddressComponents() {

        if(initTime!=0&&(Calendar.getInstance().getTimeInMillis()-initTime)>20*60*1000)
            Constants.findlocation = true;

        if (Constants.findlocation) {

            Constants.findlocation = false;
            initTime = Calendar.getInstance().getTimeInMillis();


            SharedPreferences sp2 = context.getSharedPreferences("Location_Pref", Context.MODE_PRIVATE);
            float lat = sp2.getFloat("latitude", 0);
            float lng = sp2.getFloat("longitude", 0);
            //String latlng = lat + "," + lng;
            if (SDKUtils.isConnectingToInternet(context)) {
                /*Geocoder geocoder = new Geocoder(context);
                try {
                    List<Address> addresses =  geocoder.getFromLocation(lat, lng, 1);
                    if (addresses!=null && addresses.size()>0) {
                        Address myaddress = addresses.get(0);
                        locality = myaddress.getLocality();
                        state = myaddress.getAdminArea();
                        country = myaddress.getCountryName();
                        feature = myaddress.getFeatureName();
//                        Log.e("address", locality + "-" + state + "-" + country);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

                String latlng = lat+","+lng;

                GoogleMapClient googleMapClient = new GoogleMapClient();
                googleMapClient.getApiService().getAddressComponentsFromLatLong(latlng,"AIzaSyAICO1J7boo-PfIWlkBskZR9EF4n-gza9w", new Callback<Geocode>() {

                    @Override
                    public void success(Geocode geocode, retrofit.client.Response response) {
                        if (geocode.getStatus().equalsIgnoreCase("OK")) {
                            if (geocode.getResults().size() > 0) {
                                locality="";state="";country = "";feature = "";
                                for(int x=0;x<geocode.getResults().size();x++) {
                                    List<Geocode.ResultsEntity.AddressComponentsEntity> list = geocode.getResults().get(x).getAddress_components();
                                    boolean localityUpdated = false, stateUpdated = false, countryUpdated = false, featureUpdated = false;
                                    for (Geocode.ResultsEntity.AddressComponentsEntity entity : list) {

                                        if(!TextUtils.isEmpty(locality) && !TextUtils.isEmpty(state) && !TextUtils.isEmpty(country) && !TextUtils.isEmpty(feature))
                                            break;

                                        for (int i = 0; i < entity.getTypes().size(); i++) {
                                            if (TextUtils.isEmpty(locality)&&entity.getTypes().get(i).equalsIgnoreCase("locality") && !localityUpdated) {
                                                locality = (!TextUtils.isEmpty(entity.getLong_name()) ? entity.getLong_name(): "");
                                                localityUpdated = true;
                                            }
                                            if (TextUtils.isEmpty(state)&&entity.getTypes().get(i).equalsIgnoreCase("administrative_area_level_1") && !stateUpdated) {
                                                state = (!TextUtils.isEmpty(entity.getLong_name()) ? entity.getLong_name(): "");
                                                stateUpdated = true;
                                            }
                                            if (TextUtils.isEmpty(country)&&entity.getTypes().get(i).equalsIgnoreCase("country") && !countryUpdated) {
                                                country = (!TextUtils.isEmpty(entity.getLong_name()) ? entity.getLong_name(): "");
                                                countryUpdated = true;
                                            }
                                            if (TextUtils.isEmpty(feature)&&entity.getTypes().get(i).equalsIgnoreCase("sublocality_level_1") && !featureUpdated) {
                                                feature = (!TextUtils.isEmpty(entity.getLong_name()) ? entity.getLong_name(): "");
                                                featureUpdated = true;
                                            }
                                        }

                                        if(!TextUtils.isEmpty(locality)) {
                                            mSharedPrefManager.setStringValue(Constants.LAST_KNOWN_LOCALITY, locality);
                                            mSharedPrefManager.setStringValue(Constants.LAST_KNOWN_ADMIN_AREA, state);
                                            mSharedPrefManager.setStringValue(Constants.LAST_KNOWN_COUNTRY, country);
                                            mSharedPrefManager.setStringValue(Constants.LAST_KNOWN_FEATURE_NAME, feature);
                                        }
                                    }
                                }

                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("PeeyushKS","error: "+error);                    }
                });
            }
        }
    }

    public int getCircleId(String admin_area, String sub_admin_area, String locality, String sub_locality) {

        String[][] state_circle = new String[24][];
        state_circle[1] = new String[]{"Andhra", "Telangana"};
        state_circle[2] = new String[]{"Assam"};
        state_circle[3] = new String[]{"Bihar", "Jharkhand"};
        state_circle[5] = new String[]{"Delhi", "National Capital", "NCR"};
        state_circle[6] = new String[]{"Gujarat", "Dadra", "Daman", "Diu", "Nagar Haveli"};
        state_circle[7] = new String[]{"Himachal"};
        state_circle[8] = new String[]{"Jammu", "Kashmir"};
        state_circle[9] = new String[]{"Kerala", "Lakshadweep"};
        state_circle[10] = new String[]{"Karnataka"};
        state_circle[11] = new String[]{"Goa"};
        state_circle[12] = new String[]{"Madhya Pradesh", "M.P.", "Chattisgarh"};
        state_circle[13] = new String[]{"Goa"};
        state_circle[14] = new String[]{"Madhya Pradesh", "M.P.", "Chattisgarh"};
        state_circle[16] = new String[]{"Arunachal", "Meghalaya", "Mizoram", "Nagaland", "Manipur", "Tripura"};
        state_circle[17] = new String[]{"Orissa", "Odisha"};
        state_circle[18] = new String[]{"Punjab", "Chandigarh"};
        state_circle[19] = new String[]{"Rajasthan"};
        state_circle[20] = new String[]{"Pondicherry"};
        state_circle[22] = new String[]{"Uttarakhand", "Uttaranchal"};
        state_circle[23] = new String[]{"Sikkim", "Andaman", "Nicobar"};
        for (int i = 0; i < state_circle.length; i++) {
            if (state_circle[i] != null && state_circle[i].length != 0) {
                for (String state : state_circle[i]) {
                    if (state.matches("(?i).*\\b" + admin_area + "\\b.*")) {
                        return i;
                    } else {
                        if (state.matches("(?i).*\\b" + sub_admin_area + "\\b.*")) {
                            return i;
                        }


                    }
                }
            }
        }


        String[][] city_circle = new String[24][];
        city_circle[4] = new String[]{"Channai", "Madras"};
        city_circle[5] = new String[]{"Noida", "Ghaziabad", "Faridabad", "Gurgaon", "Ballabgarh"};
        city_circle[6] = new String[]{"Silvassa"};
        city_circle[9] = new String[]{"Ladakh"};
        city_circle[10] = new String[]{"Minicoy"};
        city_circle[12] = new String[]{"Kolkata", "Calcutta"};
        city_circle[15] = new String[]{"Mumbai", "Bombay", "Thane", "Kalyan", "Vashi", "Panvel"};
        city_circle[18] = new String[]{"Panchkula"};
        city_circle[22] = new String[]{"Agra", "Aligarh", "Amroha", "Badaun", "Baghpat", "Bareilly", "Bijnor", "Bulandshahr", "Etah", "Firozabad", "Hapur", "Hathras", "Mahamaya", "Mainpuri", "Mathura", "Meerut", "Moradabad", "Muzaffarnagar", "Pilibhit", "Rampur", "Saharanpur", "Sambhal", "Bhimnagar", "Shahjahanpur", "Shamli"};
        for (int j = 0; j < city_circle.length; j++) {
            if (city_circle[j] != null && city_circle[j].length != 0) {
                for (String city : city_circle[j]) {
                    if (city.matches("(?i).*\\b" + admin_area + "\\b.*")) {
                        return j;
                    } else if (city.matches("(?i).*\\b" + sub_admin_area + "\\b.*")) {
                        return j;
                    } else if (city.matches("(?i).*\\b" + locality + "\\b.*")) {
                        return j;
                    } else if (city.matches("(?i).*\\b" + sub_locality + "\\b.*")) {
                        return j;
                    }
                }
            }
        }

        String[][] rem_circle = new String[24][];
        rem_circle[8] = new String[]{"Haryana"};
        rem_circle[13] = new String[]{"Maharastra"};
        rem_circle[20] = new String[]{"Tamil"};
        rem_circle[21] = new String[]{"Uttar", "U.P", "Purvanchal"};
        rem_circle[23] = new String[]{"Bengal"};
        for (int k = 0; k < rem_circle.length; k++) {
            if (rem_circle[k] != null && rem_circle[k].length != 0) {
                for (String loc : rem_circle[k]) {
                    if (loc.matches("(?i).*\\b" + admin_area + "\\b.*"))
                        return k;
                    if (loc.matches("(?i).*\\b" + sub_admin_area + "\\b.*"))
                        return k;
                }
            }
        }
        return 0;
    }

}
