package com.getsmartapp.lib.internetData;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.utils.DateUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * @author Peeyush.Singh on 19-02-2016.
 */

class InternetDataDBWrapper {

    private static InternetDataDBWrapper mInternetDataDBWrapper;
    private Context mContext;
    public static final String INTERNET_DATA_DIRECTORY = "internetdata";
    public static final String INTERNET_WIFI_DATA_DIRECTORY = "internetdata_wifi";

    private final String INSTALLED_APPS_LIST = "InstalledAppsList";
    private SharedPrefManager mSharedPrefManager;
    public static final String LAST_SAVED_CONNECTION_IS_MOBILE = "LAST_SAVED_CONNECTION_IS_MOBILE";

    public static InternetDataDBWrapper getInstance(Context context)
    {
        if(mInternetDataDBWrapper==null)
            mInternetDataDBWrapper = new InternetDataDBWrapper(context);
        return  mInternetDataDBWrapper;
    }

    private InternetDataDBWrapper(Context context)
    {
        mContext = context;
        mSharedPrefManager = new SharedPrefManager(context);

        File installedAppListFile = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), INSTALLED_APPS_LIST);
        if(!installedAppListFile.exists()) {
            writeToTempFile(getTempFileObject());
        }

    }


    void updateMainAppsMobileDataTableForTotalData(Context context, boolean updateMobileData) {

        if(InternetDataUsageUtil.isNetworkConnected(context.getApplicationContext()))
        {
            JSONObject tempTableJSONObject1 = readTempFile();//getTempFileObject();
            String date = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());
            JSONObject mainTableJSONObject;// = readFromFileName(date);
            if (!dateDataFileExist(date) || (readFromFileName(date).length() == 0)) {
                if ((readFromFileName(date).length() == 0))
                    deleteExistFile(date);
                JSONObject jsonObject = createNewDateJsonObject();
                writeToFile(jsonObject, date);
            }
            mainTableJSONObject = readFromFileName(date);

            JSONArray mainTableJSONObjectJSONArray = null;
            try {
                mainTableJSONObjectJSONArray = mainTableJSONObject.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);
            } catch (JSONException e) {
//                e.printStackTrace();
                mainTableJSONObjectJSONArray = new JSONArray();
            }

            JSONArray tempTableJSONArray = null;//getInstalledAppList(context);
            try {
                tempTableJSONArray = tempTableJSONObject1.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);
            } catch (JSONException e) {
//                e.printStackTrace();
                tempTableJSONArray = new JSONArray();
            }

            if((mainTableJSONObjectJSONArray.length() > tempTableJSONArray.length()))
            {
//                Log.e("PeeyushKS"," UPDATE TEMP TABLE");
//                File installedAppListFile = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), INSTALLED_APPS_LIST);
//                installedAppListFile.delete();
                deleteExistFile(INSTALLED_APPS_LIST);
                writeToTempFile(getTempFileObject());

            }else
            {
//                Log.e("PeeyushKS","NO NEED TO UPDATE TEMP TABLE");
            }
//            Log.e("PeeyushKS","Temp Table Apps Count: "+tempTableJSONArray.length());
//            Log.e("PeeyushKS","Main Table Apps Count: "+mainTableJSONObjectJSONArray.length());


            try {

                long totalRxBytes = TrafficStats.getTotalRxBytes();
                long totalTxBytes = TrafficStats.getTotalTxBytes();
                JSONObject temTableDataJsonObject = tempTableJSONObject1.getJSONObject(InternetDataUsageUtil.DATA);
                JSONObject mainTableDataJsonObject = mainTableJSONObject.getJSONObject(InternetDataUsageUtil.DATA);

                if (totalRxBytes < 0)
                    totalRxBytes = 0;
                if (totalTxBytes < 0)
                    totalTxBytes = 0;

                long overallDataToInsert = totalRxBytes + totalTxBytes - temTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA);

                if(overallDataToInsert < 0)
                    overallDataToInsert = 0;

                if (updateMobileData ) {
                    /*mainTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA) + mobileDataToInsert);
                    temTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, mobileRxBytes + mobileTxBytes);*/
                    mainTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA) + overallDataToInsert);
                    temTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, totalRxBytes + totalTxBytes);
                }else if(InternetDataUsageUtil.isWifiNetworkConnected(context))
                {
                    WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo currentWifi = mainWifi.getConnectionInfo();
//                    updateWifiDataSSDWise(context, currentWifi, overallDataToInsert);
                }
//                InternetDataUsageUtil.getInstance(mContext).updateNotification(context, mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA));

//                Log.e("PeeyushKS", "Mobile Data: " + InternetDataUsageUtil.humanReadableByteCount(mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA)));
                mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_TODAY_MOBILE_DATA_USED, mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA));

                if (overallDataToInsert >= 0) {
                    mainTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA, mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA) + overallDataToInsert);
                    temTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA, totalRxBytes + totalTxBytes);
                } /*else
                    return;*/
//                Log.e("PeeyushKS", "Overall Data: " + InternetDataUsageUtil.humanReadableByteCount(mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA)));
                mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_TODAY_TOTAL_DATA_USED, mainTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA));


            } catch (JSONException e) {
//                e.printStackTrace();
            }


            for (int i = 0; i < tempTableJSONArray.length(); i++) {
                try {
                    JSONObject tempTableJSONObject = tempTableJSONArray.getJSONObject(i);
                    int uid = tempTableJSONObject.getInt(InternetDataUsageUtil.APP_UID);//jsonObject.put(APP_UID, applicationInfo.uid);

                    long currentReceivedTrafficStatsData = TrafficStats.getUidRxBytes(uid);
                    long currentTransferredTrafficStatsData = TrafficStats.getUidTxBytes(uid);
                    if (currentReceivedTrafficStatsData < 0)
                        currentReceivedTrafficStatsData = 0;
                    if (currentTransferredTrafficStatsData < 0)
                        currentTransferredTrafficStatsData = 0;

                    long dataToInsert = ((currentReceivedTrafficStatsData + currentTransferredTrafficStatsData) - tempTableJSONObject.getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE));

                    if (dataToInsert < 0)
                        dataToInsert = 0;

                    if (dataToInsert > 0) {
//                        Log.e("PeeyushKS", "AN: " + tempTableJSONObject.getString(InternetDataUsageUtil.APP_NAME) + "- " + InternetDataUsageUtil.humanReadableByteCount(dataToInsert));
                    }

                    if (mainTableJSONObject.get(InternetDataUsageUtil.DATE).toString().equalsIgnoreCase(date)) {

                        for (int xyz = 0; xyz < mainTableJSONObjectJSONArray.length(); xyz++) {
                            JSONObject jsonObject2 = mainTableJSONObjectJSONArray.getJSONObject(xyz);

                            if (tempTableJSONObject.getString(InternetDataUsageUtil.APP_NAME).equalsIgnoreCase(jsonObject2.getString(InternetDataUsageUtil.APP_NAME))) {
                                if (updateMobileData) {
                                    jsonObject2.put(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE, jsonObject2.getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE) + dataToInsert);
                                }

                                jsonObject2.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, jsonObject2.getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) + dataToInsert);
                            }
                        }
                    }

                    tempTableJSONObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, currentTransferredTrafficStatsData + currentReceivedTrafficStatsData);

                } catch (JSONException e) {
//                    e.printStackTrace();

                }
            }


            writeToFile(mainTableJSONObject, date);
            writeToTempFile(tempTableJSONObject1);

        }
    }

    void updateWifiDataSSDWise(Context context, WifiInfo wifiInfo, long data)
    {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        String date = DateUtil.getDateFromTimeInMillis(currentTime);
        JSONArray jsonArray = readWifiDataFromFileName(date);
        JSONObject tempJSONObject = readWifiDataTempFile();

        mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_LAST_WIFI_CONNECTED_TIME, Calendar.getInstance().getTimeInMillis());

        tempJSONObject.length();
        JSONObject jsonObject = null;

//        Log.e("PeeyushKS","Total Wifi in list: "+jsonArray.length());
        try {
            for(int i=0;i<jsonArray.length();i++)
            {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                if(jsonObject1.get(InternetDataUsageUtil.WIFI_SSID).toString().equalsIgnoreCase(wifiInfo.getSSID().replace("\"", "")))
                {
                    // Check temp ssid. If same update wifi date. else update temp object with current info.
                    jsonObject = jsonObject1;
//                    Log.e("PeeyushKS","SSID Exist: "+wifiInfo.getSSID().replace("\"", ""));
                    try {
                        if (tempJSONObject.length() > 0 && tempJSONObject.getString(InternetDataUsageUtil.WIFI_SSID).toString().equalsIgnoreCase(wifiInfo.getSSID().replace("\"", ""))) {
                            /// Update both data...temptable with current and data table with data
                            jsonObject1.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, jsonObject1.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED)+data);
                            mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_DATA, jsonObject1.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED)+data);
//                            Log.e("PeeyushKS","Wifi Data: "+jsonObject1);
                            jsonObject = jsonObject1;
                        }
                    }catch (Exception e)
                    {
                    }
                }
            }

            if(jsonObject == null) {
                jsonObject = new JSONObject();
                jsonObject.put(InternetDataUsageUtil.WIFI_SSID, wifiInfo.getSSID().replace("\"", ""));
                jsonObject.put(InternetDataUsageUtil.WIFI_MAC_ADDRESS, wifiInfo.getMacAddress());
                jsonObject.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, 0);
                jsonObject.put(InternetDataUsageUtil.WIFI_CONNECTED_TIME, 0);
                jsonObject.put(InternetDataUsageUtil.WIFI_DATA_LOCATION, "");
//                jsonObject.put(InternetDataUsageUtil.WIFI_DATE,Calendar.getInstance().getTimeInMillis());

                jsonArray.put(jsonObject);
            }

            tempJSONObject.put(InternetDataUsageUtil.WIFI_SSID, wifiInfo.getSSID().replace("\"", ""));
            tempJSONObject.put(InternetDataUsageUtil.WIFI_MAC_ADDRESS, wifiInfo.getMacAddress());
            tempJSONObject.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, TrafficStats.getTotalTxBytes()+TrafficStats.getTotalRxBytes());
            tempJSONObject.put(InternetDataUsageUtil.WIFI_CONNECTED_TIME, Calendar.getInstance().getTimeInMillis());
            tempJSONObject.put(InternetDataUsageUtil.WIFI_DATA_LOCATION, "");

            writeToWifiFie(jsonArray, DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()));
            writeToWifiTempFile(tempJSONObject);
        } catch (JSONException e) {
//            e.printStackTrace();
        }

    }



    JSONArray getWifiDayWiseRecord(String ssid)
    {
        if(hashMap1.size()>0)
        {
            try{
                return hashMap1.get(ssid);
            }catch (Exception e){
                return new JSONArray();
            }
        }
        return new JSONArray();
    }

    HashMap<String, JSONArray> hashMap1 = new HashMap<>();

    ArrayList<JSONObject> readWifiListForDays(int days)
    {
        long timeInMillis = Calendar.getInstance().getTimeInMillis();

        HashMap<String, JSONObject> hashMap = new HashMap<>();

        for(int i=0;i<days;i++)
        {
            JSONArray jsonArray1 = readWifiDataFromFileName(DateUtil.getDateFromTimeInMillis(timeInMillis));

            for(int j =0; j < jsonArray1.length();j++)
            {
                try{
                    JSONObject jsonObject = jsonArray1.getJSONObject(j);

                        try {
                            JSONObject jsonObject1 = hashMap.get(jsonObject.getString(InternetDataUsageUtil.WIFI_SSID));
                            JSONArray jsonArray3 = hashMap1.get(jsonObject.getString(InternetDataUsageUtil.WIFI_SSID));
                            try {
                                jsonObject.put(InternetDataUsageUtil.DATE_IN_MILLIS, timeInMillis);
                                jsonObject.put(InternetDataUsageUtil.DATE, DateUtil.getDateFromTimeInMillis(timeInMillis));
                                jsonArray3.put(jsonObject);
                                jsonObject1.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, jsonObject1.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED) + jsonObject.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED));
                            }catch (Exception e)
                            {
                                jsonObject1.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, jsonObject1.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED) );
                            }
                        }catch (Exception e)
                        {
                            hashMap.put(jsonObject.getString(InternetDataUsageUtil.WIFI_SSID), jsonObject);
                            JSONArray jsonArray2 = new JSONArray();
                            jsonObject.put(InternetDataUsageUtil.DATE_IN_MILLIS, timeInMillis);
                            jsonObject.put(InternetDataUsageUtil.DATE, DateUtil.getDateFromTimeInMillis(timeInMillis));

                            jsonArray2.put(jsonObject);
                            hashMap1.put(jsonObject.getString(InternetDataUsageUtil.WIFI_SSID),jsonArray2);
                        }

                }catch (Exception e){

                }
            }
            timeInMillis = timeInMillis -(24*60*60*1000);
        }

        Log.e("PeeyushKS","Hashmap1: "+hashMap1);
        Log.e("PeeyushKS","Hashmap: "+hashMap);

        ArrayList<JSONObject> keys = new ArrayList<>(hashMap.values());

        Collections.sort(keys, new Comparator<JSONObject>() {
            //You can change "Name" with "ID" if you want to sort by ID
//                private static final String KEY_NAME = "Name";

            @Override
            public int compare(JSONObject a, JSONObject b) {

                Long valA = 0L;//new Long();
                Long valB = 0L;
                try {
                    valA = (Long) a.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED);
                    valB = (Long) b.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED);
                } catch (JSONException e) {
//                        do something
                }
                return -valA.compareTo(valB);
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });

        return keys;
    }


    JSONArray readWifiDataFromFileName(String fileName)
    {
        File file = new File(mContext.getDir(INTERNET_WIFI_DATA_DIRECTORY, Context.MODE_PRIVATE), fileName+"_wifi");

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            String objectString = (String) objectInputStream.readObject();

            JSONArray jsonArray = new JSONArray(objectString);
            return jsonArray;
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } catch (OptionalDataException e) {
//            e.printStackTrace();
        } catch (StreamCorruptedException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } catch (JSONException e) {
//            e.printStackTrace();
        }

        return  new JSONArray();
    }

    public JSONObject readWifiDataTempFile()
    {
        File file = new File(mContext.getDir(INTERNET_WIFI_DATA_DIRECTORY, Context.MODE_PRIVATE), "temp"+"_wifi");

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            String objectString = (String) objectInputStream.readObject();

            JSONObject jsonObject = new JSONObject(objectString);
            return jsonObject;
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } catch (OptionalDataException e) {
//            e.printStackTrace();
        } catch (StreamCorruptedException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } catch (JSONException e) {
//            e.printStackTrace();
        }

        return  new JSONObject();
    }


    public void shutDownEvent()
    {
        JSONObject tempTableJSONObject1 = readTempFile();//getTempFileObject();
        try {
            JSONObject temTableDataJsonObject = tempTableJSONObject1.getJSONObject(InternetDataUsageUtil.DATA);
            temTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, 0);
            temTableDataJsonObject.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA, 0);

            JSONArray tempTableJSONArray = tempTableJSONObject1.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);
            for (int i = 0; i < tempTableJSONArray.length(); i++){

                JSONObject tempTableJSONObject = tempTableJSONArray.getJSONObject(i);
                tempTableJSONObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, 0);
            }
        } catch (JSONException e) {
//            e.printStackTrace();
        }
        writeToTempFile(tempTableJSONObject1);

    }


    public void updateAppList(boolean newAppInstalled, int UID, Context context)
    {

        String date = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());//

        JSONObject mainTableJSONObject = readFromFileName(date);
        JSONObject tempTableJSONObject1 = readTempFile();//getTempFileObject();

        if(newAppInstalled)
        {
            //update Today's appList
            //update Temp List
            try {
                JSONArray tempTableJSONArray = tempTableJSONObject1.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);//getInstalledAppList(context);

                String packageName = context.getPackageManager().getNameForUid(UID);
                String appName = (String) context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA));

                if(context.getPackageManager().checkPermission(Manifest.permission.INTERNET, packageName) == PackageManager.PERMISSION_GRANTED)
                {
                    boolean appAlreadyExist = false;

                    long transferredData = TrafficStats.getUidTxBytes(UID) ;
                    long receivedData = TrafficStats.getUidRxBytes(UID);

                    if(transferredData<0)
                        transferredData = 0;
                    if(receivedData<0)
                        receivedData = 0;

                    for(int i=0; i< tempTableJSONArray.length();i++)
                    {
                        JSONObject jsonObject = tempTableJSONArray.getJSONObject(i);
                        if(jsonObject.getString(InternetDataUsageUtil.APP_PACKAGE_NAME).equalsIgnoreCase(packageName))
                        {
                            jsonObject.put(InternetDataUsageUtil.APP_INSTALLED_STATE, true);
                            jsonObject.put(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE, transferredData+receivedData);
                            jsonObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, transferredData+receivedData);
                            jsonObject.put(InternetDataUsageUtil.APP_UID, UID);

                            appAlreadyExist = true;

//                            Log.e("PeeyushKS","Yeah! Already in temp File");
                            break;
                        }
                    }

                    if(!appAlreadyExist) {
//                        Log.e("PeeyushKS","Nah! New added in temp File");

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(InternetDataUsageUtil.APP_PACKAGE_NAME, packageName);
                        jsonObject.put(InternetDataUsageUtil.APP_UID, UID);
                        jsonObject.put(InternetDataUsageUtil.APP_NAME, appName);
                        jsonObject.put(InternetDataUsageUtil.APP_INSTALLED_STATE, true);
                        jsonObject.put(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE, transferredData + receivedData);
                        jsonObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, transferredData + receivedData);

                        tempTableJSONArray.put(jsonObject);
                    }
                }

                try {
                    JSONArray mainTableJSONObjectJSONArray = mainTableJSONObject.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);
                    if(context.getPackageManager().checkPermission(Manifest.permission.INTERNET, packageName) == PackageManager.PERMISSION_GRANTED)
                    {
                        boolean appAlreadyExist = false;
                        for(int i=0; i< mainTableJSONObjectJSONArray.length();i++)
                        {
                            JSONObject jsonObject = mainTableJSONObjectJSONArray.getJSONObject(i);
                            if(jsonObject.getString(InternetDataUsageUtil.APP_PACKAGE_NAME).equalsIgnoreCase(packageName))
                            {
                                jsonObject.put(InternetDataUsageUtil.APP_INSTALLED_STATE, true);
                                appAlreadyExist = true;
//                                Log.e("PeeyushKS","Yeah! Already in Main File");

                                break;
                            }
                        }

                        if(!appAlreadyExist) {
//                            Log.e("PeeyushKS","Nah! New added in Main File");

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(InternetDataUsageUtil.APP_PACKAGE_NAME, packageName);
                            jsonObject.put(InternetDataUsageUtil.APP_UID, UID);
                            jsonObject.put(InternetDataUsageUtil.APP_NAME, appName);
                            jsonObject.put(InternetDataUsageUtil.APP_INSTALLED_STATE, true);
                            jsonObject.put(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE, 0);
                            jsonObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, 0);

                            mainTableJSONObjectJSONArray.put(jsonObject);
                        }
                    }
                } catch (JSONException e) {
//                    e.printStackTrace();
                }

            } catch (JSONException e) {
//                e.printStackTrace();
            } catch (PackageManager.NameNotFoundException e) {
//                e.printStackTrace();
            }

            writeToFile(mainTableJSONObject, date);
//            writeInstalledAppList(context, tempTableJSONArray);
            writeToTempFile(tempTableJSONObject1);

        }else
        {
            try {
                JSONArray tempTableJSONArray = tempTableJSONObject1.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);//getInstalledAppList(context);
                for(int i = 0; i < tempTableJSONArray.length(); i++)
                {
                    try {
                        JSONObject tempTableJSONObject = tempTableJSONArray.getJSONObject(i);
                        int uid = tempTableJSONObject.getInt(InternetDataUsageUtil.APP_UID);//jsonObject.put(APP_UID, applicationInfo.uid);
                        if(uid==UID)
                        {
                            tempTableJSONObject.put(InternetDataUsageUtil.APP_INSTALLED_STATE, false);
                        }

                    } catch (JSONException e) {
//                        e.printStackTrace();
                    }
                }

                JSONArray mainTableJSONObjectJSONArray = mainTableJSONObject.getJSONArray(InternetDataUsageUtil.APP_WISE_DATA);
                for (int xyz = 0; xyz < mainTableJSONObjectJSONArray.length(); xyz++) {

                    JSONObject jsonObject2 = mainTableJSONObjectJSONArray.getJSONObject(xyz);
                    int uid = jsonObject2.getInt(InternetDataUsageUtil.APP_UID);//jsonObject.put(APP_UID, applicationInfo.uid);
                    if(uid==UID)
                    {
                        jsonObject2.put(InternetDataUsageUtil.APP_INSTALLED_STATE, false);
                    }
                }
            } catch (JSONException e) {
//                e.printStackTrace();
            }
            writeToFile(mainTableJSONObject, date);
            writeToTempFile(tempTableJSONObject1);
        }
    }


    private boolean deleteExistFile(String date)
    {
        File file = new File(mContext.getDir(InternetDataDBWrapper.INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), date);
        return file.delete();
    }

    boolean dateDataFileExist(String dateFileName)
    {
        File file = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), dateFileName);
        return file.exists();
    }

    private JSONObject createNewDateJsonObject()
    {
        JSONObject jsonObject = new JSONObject();

        String date = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());
        try {
            jsonObject.put(InternetDataUsageUtil.DATE, date);

            jsonObject.put(InternetDataUsageUtil.DATE_IN_MILLIS, Calendar.getInstance().getTimeInMillis());

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(InternetDataUsageUtil.MOBILE_DATA_RECEIVED, 0);
            jsonObject1.put(InternetDataUsageUtil.MOBILE_DATA_TRANSFERRED, 0);

            jsonObject1.put(InternetDataUsageUtil.TOTAL_DATA_RECEIVED, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_DATA_TRANSFERRED, 0);

            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, 0 );
            jsonObject1.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA, 0);

            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_2G_DATA, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_3G_DATA, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_4G_DATA, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA_BACKGROUND, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA_FOREGROUND, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA_BACKGROUND, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA_FOREGROUND, 0);


            jsonObject.put(InternetDataUsageUtil.DATA, jsonObject1);
            jsonObject.put(InternetDataUsageUtil.APP_WISE_DATA, getApplicationPackageNameWithLauncher(mContext, false));
            return jsonObject;
        } catch (JSONException e) {
//            e.printStackTrace();
        }
        return new JSONObject();
    }

    private JSONObject getTempFileObject()
    {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(InternetDataUsageUtil.DATE, "");
            jsonObject.put(InternetDataUsageUtil.DATE_IN_MILLIS, Calendar.getInstance().getTimeInMillis());

            JSONObject jsonObject1 = new JSONObject();

            long totalTxBytes = TrafficStats.getTotalTxBytes();
            long totalRxBytes = TrafficStats.getTotalRxBytes();

            long mobileTxBytes = TrafficStats.getMobileTxBytes();
            long mobileRxBytes = TrafficStats.getMobileRxBytes();


            if(totalRxBytes < 0)
                totalRxBytes = 0;
            if(totalTxBytes < 0)
                totalTxBytes = 0;
            if(mobileRxBytes < 0)
                mobileRxBytes = 0;
            if(mobileTxBytes < 0)
                mobileTxBytes = 0;

            jsonObject1.put(InternetDataUsageUtil.MOBILE_DATA_RECEIVED, mobileRxBytes);
            jsonObject1.put(InternetDataUsageUtil.MOBILE_DATA_TRANSFERRED, mobileTxBytes);

            jsonObject1.put(InternetDataUsageUtil.TOTAL_DATA_RECEIVED, totalRxBytes);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_DATA_TRANSFERRED, totalTxBytes);

            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA, mobileRxBytes + mobileTxBytes);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA, totalRxBytes + totalTxBytes);

            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_2G_DATA, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_3G_DATA, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_4G_DATA, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA_BACKGROUND, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_OVERALL_DATA_FOREGROUND, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA_BACKGROUND, 0);
            jsonObject1.put(InternetDataUsageUtil.TOTAL_MOBILE_DATA_FOREGROUND, 0);


            jsonObject.put(InternetDataUsageUtil.DATA, jsonObject1);

            jsonObject.put(InternetDataUsageUtil.APP_WISE_DATA, getApplicationPackageNameWithLauncher(mContext, true));
            return  jsonObject;
        } catch (JSONException e) {
//            e.printStackTrace();
        }

        return new JSONObject();

    }
    private JSONArray getApplicationPackageNameWithLauncher(Context context, boolean isTempFile) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities
                (intent, 0);
        JSONArray jsonArray = new JSONArray();

        ArrayList<String> jsonObjectArrayList = new ArrayList<>();
        for (ResolveInfo info : resolveInfoList) {
            ApplicationInfo applicationInfo = info.activityInfo.applicationInfo;
            JSONObject jsonObject = new JSONObject();

            String packageName = applicationInfo.packageName;
            try {

                jsonObject.put(InternetDataUsageUtil.APP_PACKAGE_NAME, applicationInfo.packageName);
                jsonObject.put(InternetDataUsageUtil.APP_UID, applicationInfo.uid);

                long transferredData = TrafficStats.getUidTxBytes(applicationInfo.uid) ;
                long receivedData = TrafficStats.getUidRxBytes(applicationInfo.uid);

                if(transferredData < 0)
                    transferredData = 0;
                if(receivedData < 0)
                    receivedData = 0;

                if(context.getPackageManager().checkPermission(Manifest.permission.INTERNET, applicationInfo.packageName) == PackageManager.PERMISSION_GRANTED)
                {
                    jsonObject.put(InternetDataUsageUtil.APP_NAME, applicationInfo.loadLabel(context.getPackageManager()));
                    jsonObject.put(InternetDataUsageUtil.APP_INSTALLED_STATE, true);
                    if(!isTempFile) {
                        jsonObject.put(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE, 0);
                        jsonObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, 0);
                    }else
                    {
                        jsonObject.put(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE, transferredData + receivedData);
                        jsonObject.put(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE, transferredData + receivedData);
                    }

                    jsonObject.put(InternetDataUsageUtil.APP_NETWORK_WIFI_USAGE,0);
                    jsonObject.put(InternetDataUsageUtil.APP_NETWORK_3G_USAGE, 0);
                    jsonObject.put(InternetDataUsageUtil.APP_NETWORK_4G_USAGE, 0);
                    jsonObject.put(InternetDataUsageUtil.APP_NETWORK_2G_USAGE, 0);
                    jsonObject.put(InternetDataUsageUtil.APP_NETWORK_FOREGROUND_USAGE, 0);
                    jsonObject.put(InternetDataUsageUtil.APP_NETWORK_BACKGROUND_USAGE, 0);
                    mSharedPrefManager.setBooleanValue(LAST_SAVED_CONNECTION_IS_MOBILE, InternetDataUsageUtil.isMobileNetworkConnected(context));
                    if(!jsonObjectArrayList.contains(applicationInfo.packageName)) {
                        jsonObjectArrayList.add(applicationInfo.packageName);
                        jsonArray.put(jsonObject);
                    }
                }
            } catch (JSONException e) {
//                e.printStackTrace();
            }/* catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }*/
        }
        return jsonArray;
    }

    private void writeToTempFile(JSONObject jsonObject)
    {
        writeToFile(jsonObject, INSTALLED_APPS_LIST);
    }

    public JSONObject readTempFile()
    {
        return readFromFileName(INSTALLED_APPS_LIST);
    }

    private void writeToFile(JSONObject jsonObject, String fileName)
    {
        try {
            File file = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), fileName);

            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(jsonObject.toString());
            objectOutputStream.close();
        }catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    private void writeToWifiTempFile(JSONObject jsonObject)
    {
        try {
            File file = new File(mContext.getDir(INTERNET_WIFI_DATA_DIRECTORY, Context.MODE_PRIVATE), "temp"+"_wifi");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(jsonObject.toString());
            Log.e("PeeyushKS","JSON: "+jsonObject);
            objectOutputStream.close();
        }catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    private void writeToWifiFie(JSONArray jsonArray, String fileName)
    {
        try {
            File file = new File(mContext.getDir(INTERNET_WIFI_DATA_DIRECTORY, Context.MODE_PRIVATE), fileName+"_wifi");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(jsonArray.toString());
            objectOutputStream.close();
        }catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    public JSONObject readFromFile(File file)
    {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            String objectString = (String) objectInputStream.readObject();

            JSONObject jsonObject = new JSONObject(objectString);
            return jsonObject;
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } catch (OptionalDataException e) {
//            e.printStackTrace();
        } catch (StreamCorruptedException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } catch (JSONException e) {
//            e.printStackTrace();
        }

        return  new JSONObject();
    }

    JSONObject readFromFileName(String fileName)
    {
        File file = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), fileName);

        try {
            if (file.exists()) {
                FileInputStream fileInputStream = new FileInputStream(file);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                String objectString = (String) objectInputStream.readObject();

                JSONObject jsonObject = new JSONObject(objectString);
                return jsonObject;
            }
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } catch (OptionalDataException e) {
//            e.printStackTrace();
        } catch (StreamCorruptedException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } catch (JSONException e) {
//            e.printStackTrace();
        }

        return  new JSONObject();
    }

    int getDirectorySize()
    {
        File file = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE),"");
        File[] files = file.listFiles();
        return files.length;
    }

    JSONArray getAllData()
    {
        JSONArray jsonArray = new JSONArray();
//        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();

        File file = new File(mContext.getDir(INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE),"");

        File[] files = file.listFiles();
//        Log.e("PeeyushKS","Total files: "+file.listFiles().length);

       /* for(int i =0; i < 30; i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            JSONObject jsonObject = readFromFileName(currentDate);
            if(jsonObject.length() >0 ) {
                jsonArray.put(jsonObject);
            }
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;
        }*/

        for(int i =0; i < files.length; i++)
        {
//            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            JSONObject jsonObject = readFromFile(files[i]);///readFromFileName(currentDate);
            try {
                if(jsonObject.length() > 0 && jsonObject.getString(InternetDataUsageUtil.DATE).trim().length()>0) {
                    jsonArray.put(jsonObject);
                }
            } catch (JSONException e) {
//                e.printStackTrace();
            }
//            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;
        }
        return jsonArray;
    }

}
