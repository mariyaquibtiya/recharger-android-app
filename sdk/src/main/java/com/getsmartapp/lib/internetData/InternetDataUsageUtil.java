package com.getsmartapp.lib.internetData;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.getsmartapp.lib.managers.RealmDataManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.App;
import com.getsmartapp.lib.model.WifiHotSpotModel;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.realmObjects.AppObject;
import com.getsmartapp.lib.realmObjects.InternetDataObject;
import com.getsmartapp.lib.utils.DateUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * @author Peeyush.Singh on 29-02-2016.
 */



public class InternetDataUsageUtil {

    public static final String SHARED_PREFERENCE_TODAY_MOBILE_DATA_USED = "SHARED_PREFERENCE_TODAY_MOBILE_DATA_USED";
    public static final String SHARED_PREFERENCE_TODAY_TOTAL_DATA_USED = "SHARED_PREFERENCE_TODAY_TOTAL_DATA_USED";

    public static final String SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED = "SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED";
    public static final String SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED = "SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED";

    public static final String SHARED_PREFERENCE_LAST_DAY_DATE = "SHARED_PREFERENCE_LAST_DAY_DATE";


    public static final String SHARED_PREFERENCE_SEVEN_DAYS_MOBILE_DATA_USED = "SHARED_PREFERENCE_SEVEN_DAYS_MOBILE_DATA_USED";
    public static final String SHARED_PREFERENCE_SEVEN_DAYS_TOTAL_DATA_USED = "SHARED_PREFERENCE_SEVEN_DAYS_TOTAL_DATA_USED";

    public static final String SHARED_PREFERENCE_FOURTEEN_DAYS_MOBILE_DATA_USED = "SHARED_PREFERENCE_FOURTEEN_DAYS_MOBILE_DATA_USED";
    public static final String SHARED_PREFERENCE_FOURTEEN_DAYS_TOTAL_DATA_USED = "SHARED_PREFERENCE_FOURTEEN_DAYS_TOTAL_DATA_USED";


    public static final String SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_MOBILE_DATA_USED = "SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_MOBILE_DATA_USED";
    public static final String SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_TOTAL_DATA_USED = "SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_TOTAL_DATA_USED";


    public static final String SHARED_PREFERENCE_APP_DATA_DAY = "SHARED_PREFERENCE_APP_DATA_DAY";
    public static final String SHARED_PREFERENCE_LAST_WIFI_CONNECTION = "SHARED_PREFERENCE_LAST_WIFI_CONNECTION";
    public static final String SHARED_PREFERENCE_LAST_WIFI_CONNECTED_TIME = "SHARED_PREFERENCE_LAST_WIFI_CONNECTED_TIME";

    private static InternetDataUsageUtil mInternetDataUsageUtil;
    private static Context mContext;
    public static final String SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE = "SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE";
    public static final String SHARED_PREFERENCE_CURRENT_CONNECTION_DATA = "SHARED_PREFERENCE_CURRENT_CONNECTION_DATA";

    public static final String MOBILE_DATA_RECEIVED = "rMobileData";
    public static final String MOBILE_DATA_TRANSFERRED = "tMobileData";

    public static final String TOTAL_DATA_RECEIVED = "rTotalData";
    public static final String TOTAL_DATA_TRANSFERRED = "tTotalData";

    public static final String DATE = "date";
    public static final String DATE_IN_MILLIS = "dateInMillis";

    public static final String DATA = "data";
    public static final String TOTAL_MOBILE_DATA = "totalMobileData";
    public static final String TOTAL_OVERALL_DATA = "totalOverallData";


    public static final String TOTAL_MOBILE_2G_DATA = "totalMobile2GData";
    public static final String TOTAL_MOBILE_3G_DATA = "totalMobile3GData";
    public static final String TOTAL_MOBILE_4G_DATA = "totalMobile4GData";
    public static final String TOTAL_OVERALL_DATA_BACKGROUND = "totalOverallDataBackground";
    public static final String TOTAL_OVERALL_DATA_FOREGROUND = "totalOverallDataForeground";
    public static final String TOTAL_MOBILE_DATA_BACKGROUND = "totalMobileDataBackground";
    public static final String TOTAL_MOBILE_DATA_FOREGROUND = "totalMobileDataForeground";

    public static final String APP_WISE_DATA = "appWiseData";

    public static final String APP_PACKAGE_NAME = "appPackageName";
    public static final String APP_UID = "appUid";
    public static final String APP_NAME = "appName";
    public static final String APP_MOBILE_DATA_USAGE = "appMobileDataUsage";
    public static final String APP_WIFI_DATA_USAGE = "appWifiDataUsage";
    public static final String APP_OVERALL_DATA_USAGE = "appOverallDataUsage";
    public static final String APP_INSTALLED_STATE = "appInstalledState";
    public static final String APP_NETWORK_WIFI_USAGE = "appNetworkWifiUsage";
    public static final String APP_NETWORK_3G_USAGE = "appNetwork3gUsage";
    public static final String APP_NETWORK_2G_USAGE = "appNetwork2gUsage";
    public static final String APP_NETWORK_4G_USAGE = "appNetwork4gUsage";

    public static final String APP_NETWORK_FOREGROUND_USAGE = "appNetworkForegroundUsage";
    public static final String APP_NETWORK_BACKGROUND_USAGE = "appNetworkBackgroundUsage";


    public static final String WIFI_SSID = "wifiSSID";
    public static final String WIFI_MAC_ADDRESS = "WifiMacAddress";
    public static final String WIFI_CONNECTED_TIME = "WifiConnectedTime";
    public static final String WIFI_DATA_CONSUMED = "WifiDataConsumed";
    public static final String WIFI_DATA_LOCATION = "WifiLocation";
    public static final String WIFI_DATE= "WifiDate";




    private SharedPrefManager mSharedPrefManager;

    private InternetDataUsageUtil(Context context)
    {
        mContext = context;
        mSharedPrefManager = new SharedPrefManager(context);
    }

    public static synchronized InternetDataUsageUtil getInstance(Context context)
    {
        if(mInternetDataUsageUtil==null)
            mInternetDataUsageUtil = new InternetDataUsageUtil(context);
        return mInternetDataUsageUtil;
    }


    public static boolean isMobileNetworkConnected(Context context)
    {
        mContext = context;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
        if(activeNetworkInfo!=null && activeNetworkInfo.isConnected())
        {
            if(activeNetworkInfo.getType()==ConnectivityManager.TYPE_MOBILE)
                return true;
        }
        return false;
    }

    public static boolean isWifiNetworkConnected(Context context)
    {
        mContext = context;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
        if(activeNetworkInfo!=null && activeNetworkInfo.isConnected())
        {
            if(activeNetworkInfo.getType()==ConnectivityManager.TYPE_WIFI||activeNetworkInfo.getType()==ConnectivityManager.TYPE_WIMAX)
                return true;
        }
        return false;
    }

    public static boolean isNetworkConnected(Context context) {
        mContext = context;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void updateMainAppsMobileDataTableForTotalData(Context context, boolean updateMobileData)
    {
        mContext = context;
        InternetDataDBWrapper.getInstance(context).updateMainAppsMobileDataTableForTotalData(context, updateMobileData);
    }


    boolean needUpdateOfflineNewDateData = true;
    public void updateData(Context context)
    {
        mContext = context;

        if(isNetworkConnected(mContext))
        {
            if(InternetDataDBWrapper.getInstance(mContext).readFromFileName(DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis())).length()==0)
            {
                File file = new File(mContext.getDir(InternetDataDBWrapper.INTERNET_DATA_DIRECTORY, Context.MODE_PRIVATE), DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()));
                if(file.delete()) {
//                    Log.e("PeeyushKS","FILE DELETED");
                }
//                else
//                    Log.e("PeeyushKS","UNABLE TO DELETE FILE");
            }

            int connectionType = -1;
            needUpdateOfflineNewDateData = true;

            if(isMobileNetworkConnected(context))
            {
                connectionType = InternetDataUsageUtil.NETWORK_CONNECTION.MOBILE_NETWORK.ordinal();
                if(mSharedPrefManager.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE,-1)==connectionType)
                {
//                    Log.e("PeeyushKS", "Mobile Connection Resumed");
                    InternetDataUsageUtil.getInstance(mContext).updateMainAppsMobileDataTableForTotalData(mContext, true);
                }
            }else
            {
                connectionType = InternetDataUsageUtil.NETWORK_CONNECTION.WIFI_NETWORK.ordinal();
                if(mSharedPrefManager.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE,-1)==connectionType)
                {
//                    Log.e("PeeyushKS", "Wifi Connection Resumed");
                    InternetDataUsageUtil.getInstance(mContext).updateMainAppsMobileDataTableForTotalData(mContext, false);
                }
            }
            updateAppWiseTotalDataUsage();
        }else
        {
            String todaysDate = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());
            if(!InternetDataDBWrapper.getInstance(mContext).dateDataFileExist(todaysDate) && needUpdateOfflineNewDateData)
            {
                needUpdateOfflineNewDateData = false;
                mSharedPrefManager.setLongValue(SHARED_PREFERENCE_TODAY_MOBILE_DATA_USED, 0);
                mSharedPrefManager.setLongValue(SHARED_PREFERENCE_TODAY_TOTAL_DATA_USED, 0);

                String prevDate = DateUtil.getDateFromTimeInMillis((Calendar.getInstance().getTimeInMillis()- 24*60*60*1000));

                try {
                    JSONObject jsonObject = getDateSpecificData(prevDate);

                    JSONObject dataJSONObject = jsonObject.getJSONObject(DATA);
                    mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED, dataJSONObject.getLong(TOTAL_MOBILE_DATA));
                    mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED, dataJSONObject.getLong(TOTAL_OVERALL_DATA));

                }catch (Exception e)
                {
                    mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED, 0);
                    mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED, 0);
                }
            }

        }

        Intent intent = new Intent("update_location");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        if(updateNotificationListener!=null)
            updateNotificationListener.updateDataNotification(context);
    }

    public void notifyNotification(Context context)
    {
        if(updateNotificationListener!=null)
            updateNotificationListener.updateDataNotification(context);
    }

    public static String humanReadableByteCount(long bytes) {
        boolean si = true;
        int unit = /*si ? 1000 :*/ 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");

        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
    public static String humanReadableDecimalByteCount(long bytes) {
        boolean si = true;
        int unit = /*si ? 1000 :*/ 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");

        return String.format("%d %sB", (int)Math.round(bytes / Math.pow(unit, exp)), pre);
    }

    public void handleBroadCastAction(Context context, Intent intent)
    {
        String action = intent.getAction();
        switch (action)
        {
            case "android.intent.action.PACKAGE_FULLY_REMOVED":
                Bundle bundle = intent.getExtras();
                InternetDataDBWrapper.getInstance(context).updateAppList(false, bundle.getInt("android.intent.extra.UID") ,context);
                break;
            case "android.intent.action.PACKAGE_ADDED":
                Bundle bundle2 = intent.getExtras();
                InternetDataDBWrapper.getInstance(context).updateAppList(true, bundle2.getInt("android.intent.extra.UID"), context);
                break;

            case "android.intent.action.ACTION_SHUTDOWN":
                InternetDataDBWrapper.getInstance(context).shutDownEvent();

                break;
            case "android.intent.action.BOOT_COMPLETED":
                initialiseDataCapture(context);
                break;
        }
    }

    public int getDbSize()
    {
        return InternetDataDBWrapper.getInstance(mContext).getDirectorySize();
    }


    public JSONArray getAllData()
    {
        return InternetDataDBWrapper.getInstance(mContext).getAllData();
    }

    public JSONObject getDateSpecificData(String date)
    {
        return InternetDataDBWrapper.getInstance(mContext).readFromFileName(date);
    }

    private UpdateNotificationListener updateNotificationListener;
    public interface UpdateNotificationListener {
        public void updateDataNotification(Context context);
    }

    public void setUpdateNotificationListener(UpdateNotificationListener updateNotificationListener) {
        this.updateNotificationListener = updateNotificationListener;
    }

    public enum NETWORK_CONNECTION
    {
        MOBILE_NETWORK,
        WIFI_NETWORK,
    }

    public void initialiseDataCapture(Context context)
    {
        Calendar cal = Calendar.getInstance();
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, InternetDataCollectionService.class);
        PendingIntent pintent = PendingIntent.getService(context, 0, intent, 0);
// schedule for every 120 seconds
        alarm.setRepeating(AlarmManager.RTC, cal.getTimeInMillis(), 2 * 60 * 1000, pintent);
//        alarm.setRepeating(AlarmManager.RTC, cal.getTimeInMillis(), 5*1000, pintent);
//        Log.e("PeeyushKS","Initialized");
    }

    public long totalDataUsageForLastNumberOfDays;

    public long getTotalMobileDataUsageForLastNumberOfDays(int days)
    {
        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
        totalDataUsageForLastNumberOfDays = 0;
        for(int i=0; i< days; i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;

            try {
                JSONObject dayJsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
                JSONObject dataJsonObject = dayJsonObject.getJSONObject(DATA);
                totalDataUsageForLastNumberOfDays += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
            } catch (JSONException e) {
//                e.printStackTrace();
            }
        }

        return totalDataUsageForLastNumberOfDays;
    }

    public long getTotalOverallDataUsageForLastNumberOfDays(int days)
    {
        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
        totalDataUsageForLastNumberOfDays = 0;
        for(int i=0; i< days; i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;

            try {
                JSONObject dayJsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
                JSONObject dataJsonObject = dayJsonObject.getJSONObject(DATA);
                totalDataUsageForLastNumberOfDays += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
            } catch (JSONException e) {
//                e.printStackTrace();
            }
        }

        return totalDataUsageForLastNumberOfDays;
    }

    private void updateAppWiseTotalDataUsage()
    {
        int days  = mSharedPrefManager.getIntValue(Constants.DATAUSAGE);
        if(mSharedPrefManager.getIntValue(SHARED_PREFERENCE_APP_DATA_DAY,0 )==0)
            mSharedPrefManager.setIntValue(SHARED_PREFERENCE_APP_DATA_DAY, mSharedPrefManager.getIntValue(Constants.DATAUSAGE));

        int savedDays = mSharedPrefManager.getIntValue(SHARED_PREFERENCE_APP_DATA_DAY);

        if(days==-1)
        {
            //calculate day diff and cache the data
            int initDay = Integer.parseInt(mSharedPrefManager.getStringValue(Constants.ON_BOARDING_BILL_DATE));
            int currentDay = Calendar.getInstance().get(Calendar.DATE);
            int dayDiff = currentDay - initDay + 1;
            if(mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS) > 0)
            {
                dayDiff = (int) mSharedPrefManager.getLongValue(Constants.ON_BOARDING_BILL_DAYS);
            }
            getAppWiseTotalDataUsageForLastNumberOfDays(dayDiff, false);

        }else
        {
            getAppWiseTotalDataUsageForLastNumberOfDays(days, false);
            //cache data for number of days.
        }

    }

    public ArrayList<Long> getWifiDayWiseRecord(Context context, String ssid, int days)
    {
        ArrayList<Long> arrayList = new ArrayList<>();
        JSONArray jsonArray = InternetDataDBWrapper.getInstance(context).getWifiDayWiseRecord(ssid);

        long currentTime = Calendar.getInstance().getTimeInMillis();
        Map<Long, Long> dateAndDataHashMap = new HashMap<>();

        for(int i = 0; i < 7; i++)
        {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

//                if(date.equals(jsonObject.getString(InternetDataUsageUtil.DATE)))
//                    arrayList.add(jsonObject.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED));
                    dateAndDataHashMap.put(currentTime, jsonObject.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED));
            }catch (Exception e){
//                arrayList.add(0L);
                dateAndDataHashMap.put(currentTime, 0L);
            }
            currentTime = currentTime - 24*60*60*1000;
        }
        dateAndDataHashMap = new TreeMap<Long, Long>(dateAndDataHashMap);
        arrayList.addAll(dateAndDataHashMap.values());


//        ArrayList<Long> datekeys = new ArrayList<>(dateAndDataHashMap.keySet());
//
//        Collections.sort(datekeys, Collections.<Long>reverseOrder());
//
//        ArrayList<Long> dataValues = new ArrayList<>();
//
//        for(int i =0;i<datekeys.size();i++)
//            dataValues.

        return arrayList;
//        return InternetDataDBWrapper.getInstance(context).getWifiDayWiseRecord(ssid);
    }

    public ArrayList<JSONObject> getWifiListForDays(Context context, int days)
    {
        return  InternetDataDBWrapper.getInstance(context).readWifiListForDays(days);
    }

    public JSONArray readWifiDataFromFileName(Context context, String date)
    {
        return  InternetDataDBWrapper.getInstance(context).readWifiDataFromFileName(date);
    }

    private void writeArrayList(ArrayList<App> apps, String dataType)
    {
        try {
//            File file = new File(mContext.getDir("DATA", Context.MODE_PRIVATE), "DATA");
            FileOutputStream fileOutputStream = mContext.openFileOutput(/*"DATA"*/dataType, Context.MODE_PRIVATE);//new FileOutputStream(file);
            ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(apps);
            objectOutputStream.close();

        }catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }

    }


    private ArrayList<App> readArrayList(String dataType)
    {
        try {
//            File file = new File(mContext.getDir("DATA", Context.MODE_PRIVATE), "DATA");
            FileInputStream fileInputStream = mContext.openFileInput(dataType/*"DATA"*/);//new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            ArrayList<App> appArrayList = (ArrayList<App>) objectInputStream.readObject();
            return appArrayList;
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } catch (OptionalDataException e) {
//            e.printStackTrace();
        } catch (StreamCorruptedException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }
        return new ArrayList<App>();
    }

    public ArrayList<WifiHotSpotModel> getWifiListForLastNumberofDays(Context context, int days)
    {
        ArrayList<WifiHotSpotModel> mListWifis = new ArrayList<>();

        JSONArray todaysJsonArray = InternetDataUsageUtil.getInstance(context).readWifiDataFromFileName(context, DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()));

        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        for (int i = 0; i < todaysJsonArray.length(); i++) {
            try {
                jsonValues.add(todaysJsonArray.getJSONObject(i));
            } catch (JSONException e) {
//                e.printStackTrace();
            }
        }
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            //You can change "Name" with "ID" if you want to sort by ID
//                private static final String KEY_NAME = "Name";

            @Override
            public int compare(JSONObject a, JSONObject b) {

                Long valA = 0L;//new Long();
                Long valB = 0L;
                try {
                    valA = (Long) a.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED);
                    valB = (Long) b.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED);
                } catch (JSONException e) {
//                        do something
                }
                return -valA.compareTo(valB);
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });

        for(int i=0;i<jsonValues.size();i++)
        {
            try {
                mListWifis.add(new WifiHotSpotModel(jsonValues.get(i).getString(WIFI_SSID), jsonValues.get(i).getLong(WIFI_DATA_CONSUMED)));
            } catch (JSONException e) {
//                e.printStackTrace();
            }
        }


        return mListWifis;
    }

    public ArrayList<App> getAppWiseTotalDataUsageForLastNumberOfDays(int days, boolean useCachedData )
    {

        if(mSharedPrefManager.getIntValue(SHARED_PREFERENCE_APP_DATA_DAY,0 )==0)
            mSharedPrefManager.setIntValue(SHARED_PREFERENCE_APP_DATA_DAY, mSharedPrefManager.getIntValue(Constants.DATAUSAGE));

        if(useCachedData)
        {
            if(mSharedPrefManager.getIntValue(SHARED_PREFERENCE_APP_DATA_DAY) == mSharedPrefManager.getIntValue(Constants.DATAUSAGE))
            {
//                return readArrayList("MOBILE_DATA");
                ArrayList<App> appList = readArrayList("MOBILE_DATA");
                if(appList.size() > 2)
                    return appList;
            }else
                mSharedPrefManager.setIntValue(SHARED_PREFERENCE_APP_DATA_DAY, mSharedPrefManager.getIntValue(Constants.DATAUSAGE));
        }

        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
        ArrayList<App> mListApps = new ArrayList<>();

        HashMap<String, long[]> hashMap = new HashMap<>();
        float normalTotalMobileDataUsed = 0;
        float appwiseTotalMobileDataUsed = 0;

        long initTime = Calendar.getInstance().getTimeInMillis();

        long past7DaysMobileData = 0;
        long past7DaysTotalData = 0;

        long past14DaysMobileData = 0;
        long past14DaysTotalData = 0;

        long past28DaysMobileData = 0;
        long past28DaysTotalData = 0;
        totalDataUsageForLastNumberOfDays = 0;


        for(int i=0; i< days; i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;

            try {

                JSONObject dayJsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
                JSONObject dataJsonObject = dayJsonObject.getJSONObject(DATA);

                JSONArray jsonArray = dayJsonObject.getJSONArray(APP_WISE_DATA);;

                normalTotalMobileDataUsed += dataJsonObject.getLong(TOTAL_MOBILE_DATA);

                if(i < 7) {
                    past7DaysMobileData += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
                    past7DaysTotalData += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                }
                if(i < 14) {
                    past14DaysMobileData += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
                    past14DaysTotalData += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                }
                if(i < 28) {
                    past28DaysMobileData += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
                    past28DaysTotalData += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                }
//                Log.e("PeeyushKS",(i+1)+" day mobile data: "+humanReadableByteCount(dataJsonObject.getLong(TOTAL_OVERALL_DATA)) );
                totalDataUsageForLastNumberOfDays += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                for(int j=0; j < jsonArray.length();j++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(j);
                    try
                    {
                        appwiseTotalMobileDataUsed += jsonObject.getLong(APP_MOBILE_DATA_USAGE);

                        long[] appdata = new long[2];
                        appdata[0] = hashMap.get(jsonObject.getString(APP_PACKAGE_NAME))[0]+jsonObject.getLong(APP_MOBILE_DATA_USAGE);
                        appdata[1] = hashMap.get(jsonObject.getString(APP_PACKAGE_NAME))[1]+jsonObject.getLong(APP_OVERALL_DATA_USAGE);

                        hashMap.put(jsonObject.getString(APP_PACKAGE_NAME),appdata);
                    }catch (Exception e)
                    {
                        long[] appdata = new long[2];
                        appdata[0] = jsonObject.getLong(APP_MOBILE_DATA_USAGE);
                        appdata[1] = jsonObject.getLong(APP_OVERALL_DATA_USAGE);

                        hashMap.put(jsonObject.getString(APP_PACKAGE_NAME), appdata);

                    }
                }

            } catch (JSONException e) {
//                e.printStackTrace();
            }
        }

//        Log.e("PeeyushKS", "7 Days Mobile Data"+humanReadableByteCount(past7DaysMobileData));
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_SEVEN_DAYS_MOBILE_DATA_USED, past7DaysMobileData);
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_SEVEN_DAYS_TOTAL_DATA_USED, past7DaysTotalData);
//        Log.e("PeeyushKS", "7 Days Total Data"+humanReadableByteCount(past7DaysTotalData));

        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_FOURTEEN_DAYS_MOBILE_DATA_USED, past14DaysMobileData);
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_FOURTEEN_DAYS_TOTAL_DATA_USED, past14DaysTotalData);

        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_MOBILE_DATA_USED, past28DaysMobileData);
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_TOTAL_DATA_USED, past28DaysTotalData);


        JSONArray newJsonArray = new JSONArray();
        ArrayList<String> packageNameArrayList = new ArrayList<>();
        packageNameArrayList.addAll(hashMap.keySet());

        try {
            for (int i = 0; i < packageNameArrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
//                    jsonObject.put(APP_NAME, packageNameArrayList.get(i)[0]);
                    jsonObject.put(APP_PACKAGE_NAME, packageNameArrayList.get(i));
                    jsonObject.put(APP_MOBILE_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[0]);
                    jsonObject.put(APP_OVERALL_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[1]);
                    newJsonArray.put(jsonObject);
                } catch (JSONException e) {
//                    e.printStackTrace();
                }
            }

            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i < newJsonArray.length(); i++) {
                jsonValues.add(newJsonArray.getJSONObject(i));
            }

            Collections.sort(jsonValues, new Comparator<JSONObject>() {
                //You can change "Name" with "ID" if you want to sort by ID
//                private static final String KEY_NAME = "Name";

                @Override
                public int compare(JSONObject a, JSONObject b) {

                    Long valA = 0L;//new Long();
                    Long valB = 0L;
                    try {
                        valA = (Long) a.getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE);
                        valB = (Long) b.getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE);
                    } catch (JSONException e) {
//                        do something
                    }
                    return -valA.compareTo(valB);
                    //if you want to change the sort order, simply use the following:
                    //return -valA.compareTo(valB);
                }
            });

            float factor = normalTotalMobileDataUsed/appwiseTotalMobileDataUsed;

            for(int i=0;i<jsonValues.size();i++)
            {
                PackageManager packageManager= mContext.getPackageManager();
                String appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), PackageManager.GET_META_DATA));

                if(!jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME).equalsIgnoreCase(mContext.getPackageName())) {
                    if (factor < 1) {
                        mListApps.add(new App(appName, jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), factor * jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) - jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE)));
                    } else
                        mListApps.add(new App(appName, jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) - jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE)));
                }
            }
//            return mListApps;
        }catch (Exception e)
        { }

        writeArrayList(mListApps, "MOBILE_DATA");
        return  mListApps;
    }


    public ArrayList<App> getAppWiseWifiDataUsageForLastNumberOfDays(int days, boolean useCachedData )
    {

        if(useCachedData)
        {
            if(mSharedPrefManager.getIntValue(SHARED_PREFERENCE_APP_DATA_DAY) == mSharedPrefManager.getIntValue(Constants.WIFIUSAGE))
            {
//                return readArrayList("WIFI_DATA");
                ArrayList<App> appList = readArrayList("WIFI_DATA");
                if(appList.size() > 2)
                    return appList;
            }else
                mSharedPrefManager.setIntValue(SHARED_PREFERENCE_APP_DATA_DAY, mSharedPrefManager.getIntValue(Constants.WIFIUSAGE));
        }

        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
        ArrayList<App> mListApps = new ArrayList<>();

        HashMap<String, long[]> hashMap = new HashMap<>();
        float normalTotalMobileDataUsed = 0;
        float appwiseTotalMobileDataUsed = 0;

        long initTime = Calendar.getInstance().getTimeInMillis();

        long past7DaysMobileData = 0;
        long past7DaysTotalData = 0;

        long past14DaysMobileData = 0;
        long past14DaysTotalData = 0;

        long past28DaysMobileData = 0;
        long past28DaysTotalData = 0;
        totalDataUsageForLastNumberOfDays = 0;


        for(int i=0; i< days; i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;

            try {

                JSONObject dayJsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
                JSONObject dataJsonObject = dayJsonObject.getJSONObject(DATA);

                JSONArray jsonArray = dayJsonObject.getJSONArray(APP_WISE_DATA);;
                normalTotalMobileDataUsed += dataJsonObject.getLong(TOTAL_MOBILE_DATA);

                if(i < 7) {
                    past7DaysMobileData += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
                    past7DaysTotalData += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                }
                if(i < 14) {
                    past14DaysMobileData += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
                    past14DaysTotalData += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                }
                if(i < 28) {
                    past28DaysMobileData += dataJsonObject.getLong(TOTAL_MOBILE_DATA);
                    past28DaysTotalData += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                }

                totalDataUsageForLastNumberOfDays += dataJsonObject.getLong(TOTAL_OVERALL_DATA);
                for(int j=0; j < jsonArray.length();j++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(j);
                    try
                    {
                        appwiseTotalMobileDataUsed += jsonObject.getLong(APP_MOBILE_DATA_USAGE);

                        long[] appdata = new long[2];
                        appdata[0] = hashMap.get(jsonObject.getString(APP_PACKAGE_NAME))[0]+jsonObject.getLong(APP_MOBILE_DATA_USAGE);
                        appdata[1] = hashMap.get(jsonObject.getString(APP_PACKAGE_NAME))[1]+jsonObject.getLong(APP_OVERALL_DATA_USAGE);

                        hashMap.put(jsonObject.getString(APP_PACKAGE_NAME),appdata);
                    }catch (Exception e)
                    {
                        long[] appdata = new long[2];
                        appdata[0] = jsonObject.getLong(APP_MOBILE_DATA_USAGE);
                        appdata[1] = jsonObject.getLong(APP_OVERALL_DATA_USAGE);

                        hashMap.put(jsonObject.getString(APP_PACKAGE_NAME), appdata);

                    }
                }

            } catch (JSONException e) {
//                e.printStackTrace();
            }
        }

//        Log.e("PeeyushKS", "7 Days Mobile Data "+humanReadableByteCount(past7DaysMobileData));
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_SEVEN_DAYS_MOBILE_DATA_USED, past7DaysMobileData);
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_SEVEN_DAYS_TOTAL_DATA_USED, past7DaysTotalData);
//        Log.e("PeeyushKS", "7 Days Total Data "+humanReadableByteCount(past7DaysTotalData));

        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_FOURTEEN_DAYS_MOBILE_DATA_USED, past14DaysMobileData);
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_FOURTEEN_DAYS_TOTAL_DATA_USED, past14DaysTotalData);

        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_MOBILE_DATA_USED, past28DaysMobileData);
        mSharedPrefManager.setLongValue(SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_TOTAL_DATA_USED, past28DaysTotalData);


        JSONArray newJsonArray = new JSONArray();
        ArrayList<String> packageNameArrayList = new ArrayList<>();
        packageNameArrayList.addAll(hashMap.keySet());

        try {
            for (int i = 0; i < packageNameArrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
//                    jsonObject.put(APP_NAME, packageNameArrayList.get(i)[0]);
                    jsonObject.put(APP_PACKAGE_NAME, packageNameArrayList.get(i));
                    jsonObject.put(APP_MOBILE_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[0]);
                    jsonObject.put(APP_OVERALL_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[1]);
                    jsonObject.put(APP_WIFI_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[1] - hashMap.get(packageNameArrayList.get(i))[0]);
                    newJsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i < newJsonArray.length(); i++) {
                jsonValues.add(newJsonArray.getJSONObject(i));
            }

            Collections.sort(jsonValues, new Comparator<JSONObject>() {
                //You can change "Name" with "ID" if you want to sort by ID
//                private static final String KEY_NAME = "Name";

                @Override
                public int compare(JSONObject a, JSONObject b) {

                    Long valA = 0L;//new Long();
                    Long valB = 0L;
                    try {
                        valA = (Long) a.getLong(InternetDataUsageUtil.APP_WIFI_DATA_USAGE);
                        valB = (Long) b.getLong(InternetDataUsageUtil.APP_WIFI_DATA_USAGE);
                    } catch (JSONException e) {
//                        do something
                    }
                    return -valA.compareTo(valB);
                    //if you want to change the sort order, simply use the following:
                    //return -valA.compareTo(valB);
                }
            });

            float factor = normalTotalMobileDataUsed/appwiseTotalMobileDataUsed;

            for(int i=0;i<jsonValues.size();i++)
            {
                PackageManager packageManager= mContext.getPackageManager();
                String appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), PackageManager.GET_META_DATA));

                if(!jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME).equalsIgnoreCase(mContext.getPackageName())) {
                    if (factor < 1) {
                        mListApps.add(new App(appName, jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), factor * jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) - jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE)));
                    } else
                        mListApps.add(new App(appName, jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) - jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE)));
                }
            }
//            return mListApps;
        }catch (Exception e)
        {
            Log.e("PeeyushKS","Exception Exception Exception");
        }

        writeArrayList(mListApps, "WIFI_DATA");
        return  mListApps;
    }

    public ArrayList<App> getAppWiseTotalDataUsageDateWise(String date)
    {
        ArrayList<App> mListApps = new ArrayList<>();

        HashMap<String, long[]> hashMap = new HashMap<>();
        float normalTotalMobileDataUsed = 0;
        float appwiseTotalMobileDataUsed = 0;

        long initTime = Calendar.getInstance().getTimeInMillis();

        try {

            JSONObject dayJsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(date);
            JSONObject dataJsonObject = dayJsonObject.getJSONObject(DATA);

            JSONArray jsonArray = dayJsonObject.getJSONArray(APP_WISE_DATA);;

            normalTotalMobileDataUsed += dataJsonObject.getLong(TOTAL_MOBILE_DATA);

            for(int j=0; j < jsonArray.length();j++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(j);
                try
                {
                    appwiseTotalMobileDataUsed += jsonObject.getLong(APP_MOBILE_DATA_USAGE);

                    long[] appdata = new long[2];
                    appdata[0] = hashMap.get(jsonObject.getString(APP_PACKAGE_NAME))[0]+jsonObject.getLong(APP_MOBILE_DATA_USAGE);
                    appdata[1] = hashMap.get(jsonObject.getString(APP_PACKAGE_NAME))[1]+jsonObject.getLong(APP_OVERALL_DATA_USAGE);

                    hashMap.put(jsonObject.getString(APP_PACKAGE_NAME),appdata);
                }catch (Exception e)
                {
                    long[] appdata = new long[2];
                    appdata[0] = jsonObject.getLong(APP_MOBILE_DATA_USAGE);
                    appdata[1] = jsonObject.getLong(APP_OVERALL_DATA_USAGE);


                    hashMap.put(jsonObject.getString(APP_PACKAGE_NAME), appdata);

                }
            }

        } catch (JSONException e) {
//            e.printStackTrace();
        }

        JSONArray newJsonArray = new JSONArray();
        ArrayList<String> packageNameArrayList = new ArrayList<>();
        packageNameArrayList.addAll(hashMap.keySet());

        try {
            for (int i = 0; i < packageNameArrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
//                    jsonObject.put(APP_NAME, packageNameArrayList.get(i)[0]);
                    jsonObject.put(APP_PACKAGE_NAME, packageNameArrayList.get(i));
                    jsonObject.put(APP_MOBILE_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[0]);
                    jsonObject.put(APP_OVERALL_DATA_USAGE, hashMap.get(packageNameArrayList.get(i))[1]);
                    newJsonArray.put(jsonObject);
                } catch (JSONException e) {
//                    e.printStackTrace();
                }
            }

            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i < newJsonArray.length(); i++) {
                jsonValues.add(newJsonArray.getJSONObject(i));
            }

            Collections.sort(jsonValues, new Comparator<JSONObject>() {
                //You can change "Name" with "ID" if you want to sort by ID
//                private static final String KEY_NAME = "Name";

                @Override
                public int compare(JSONObject a, JSONObject b) {

                    Long valA = 0L;//new Long();
                    Long valB = 0L;
                    try {
                        valA = (Long) a.getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE);
                        valB = (Long) b.getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE);
                    } catch (JSONException e) {
//                        do something
                    }
                    return -valA.compareTo(valB);
                    //if you want to change the sort order, simply use the following:
                    //return -valA.compareTo(valB);
                }
            });

            float factor = normalTotalMobileDataUsed/appwiseTotalMobileDataUsed;

            for(int i=0;i<jsonValues.size();i++)
            {
                PackageManager packageManager= mContext.getPackageManager();
                String appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), PackageManager.GET_META_DATA));

                if(!jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME).equalsIgnoreCase(mContext.getPackageName())) {
                    if (factor < 1) {
                        mListApps.add(new App(appName, jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), factor * jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) - jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE)));
                    } else
                        mListApps.add(new App(appName, jsonValues.get(i).getString(InternetDataUsageUtil.APP_PACKAGE_NAME), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE), jsonValues.get(i).getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE) - jsonValues.get(i).getLong(InternetDataUsageUtil.APP_MOBILE_DATA_USAGE)));
                }
            }
            return mListApps;
        }catch (Exception e)
        { }

        return  mListApps;
    }

    public void setAppName(String appName)
    {
        SharedPrefManager sharedPrefManager = new SharedPrefManager(mContext);
        sharedPrefManager.setStringValue("OUR_APP_NAME",appName);
    }
    public JSONObject getDataUsageForSpecificDate(String date)
    {
        return InternetDataDBWrapper.getInstance(mContext).readFromFileName(date);
    }
    public long getTodaysMobileDataUsed(Context context)
    {
        if(mSharedPrefManager == null)
            mSharedPrefManager = new SharedPrefManager(context);
        try {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_TODAY_MOBILE_DATA_USED);
        }catch (Exception e)
        {
            return 0;
        }
    }

    public long getTodaysTotalDataUsed()
    {
        try {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_TODAY_TOTAL_DATA_USED);
        }catch (Exception e)
        {
            return 0;
        }
    }




    public long getLast7DaysMobileDataUsed()
    {
        try
        {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_SEVEN_DAYS_MOBILE_DATA_USED);
        }catch (Exception e)
        {
            return 0;
        }
    }

    public long getLast7DaysTotalDataUsed()
    {
        try
        {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_SEVEN_DAYS_TOTAL_DATA_USED);
        }catch (Exception e)
        {
            return 0;
        }
    }


    public long getLast14DaysMobileDataUsed()
    {
        try
        {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_FOURTEEN_DAYS_MOBILE_DATA_USED);
        }catch (Exception  e)
        {
            return 0;
        }
    }

    public long getLast14DaysTotalDataUsed()
    {
        try
        {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_FOURTEEN_DAYS_TOTAL_DATA_USED);
        }catch (Exception  e)
        {
            return 0;
        }
    }


    public long getLast28DaysMobileDataUsed()
    {
        try
        {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_MOBILE_DATA_USED);
        }catch (Exception  e)
        {
            return 0;
        }
    }

    public long getLast28DaysTotalDataUsed()
    {
        try
        {
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_TWENTY_EIGHT_DAYS_TOTAL_DATA_USED);
        }catch (Exception  e)
        {
            return 0;
        }

    }

    public long getPreviousDayMobileDataUsed()
    {
        try {
            updatePreviousDayData();
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED);
        }catch (Exception e)
        {
            return 0;
        }
    }

    public long getPreviousDayTotalDataUsed()
    {
        try {
            updatePreviousDayData();
            return mSharedPrefManager.getLongValue(SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED);
        }catch (Exception e)
        {
            return 0;
        }
    }

    void updatePreviousDayData()
    {
        String prevDate = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()- 24*60*60*1000L) ;

        if(!mSharedPrefManager.getStringValue(SHARED_PREFERENCE_LAST_DAY_DATE, "").equalsIgnoreCase(prevDate))
        {
            long prevDataLong = Calendar.getInstance().getTimeInMillis() - 24 * 60 * 60 * 1000;
            String prevData = DateUtil.getDateFromTimeInMillis(prevDataLong);
            try {
                JSONObject jsonObject = getDateSpecificData(prevData);
                JSONObject dataJSONObject = jsonObject.getJSONObject(DATA);
                mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED, dataJSONObject.getLong(InternetDataUsageUtil.TOTAL_MOBILE_DATA));
                mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED, dataJSONObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA));

            } catch (Exception e) {
                mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_MOBILE_DATA_USED, 0);
                mSharedPrefManager.setLongValue(SHARED_PREFERENCE_LAST_DAY_TOTAL_DATA_USED, 0);
            }
            mSharedPrefManager.setStringValue(SHARED_PREFERENCE_LAST_DAY_DATE, prevDate);
        }
    }

    public ArrayList<Long> updateMapData(int days)
    {
        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
//        JSONObject jsonObject = InternetDataDBWrapper.getInstance(getContext()).readFromFile(DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis));

        ArrayList<Long> mobileDataList = new ArrayList<>();
        for(int i=0;i<days;i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;
            JSONObject jsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
            try {
                if(jsonObject.length()>0) {
                    JSONObject dataJsonObject = jsonObject.getJSONObject(DATA);
                    mobileDataList.add(dataJsonObject.getLong(TOTAL_MOBILE_DATA));
                }else
                    mobileDataList.add(0L);

            } catch (JSONException e) {
//                e.printStackTrace();
                mobileDataList.add(0L);
            }
        }
        return mobileDataList;
    }

    public ArrayList<Long> updateWifiMapData(int days)
    {
        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();

        ArrayList<Long> mobileDataList = new ArrayList<>();
        for(int i=0;i<days;i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;
            JSONObject jsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
            try {
                if(jsonObject.length()>0) {
                    JSONObject dataJsonObject = jsonObject.getJSONObject(DATA);
                    mobileDataList.add(dataJsonObject.getLong(TOTAL_OVERALL_DATA) - dataJsonObject.getLong(TOTAL_MOBILE_DATA));
                }else
                    mobileDataList.add(0L);

            } catch (JSONException e) {
//                e.printStackTrace();
                mobileDataList.add(0L);
            }
        }
        return mobileDataList;
    }

    public HashMap<String, JSONObject> getDataUsageForLastNumberOfDays(int days)
    {
        long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
        HashMap<String, JSONObject> hashMap = new HashMap<>();
        hashMap.clear();
        for(int i =0; i < days; i++)
        {
            String currentDate = DateUtil.getDateFromTimeInMillis(currentDateTimeInMillis);
            JSONObject jsonObject = InternetDataDBWrapper.getInstance(mContext).readFromFileName(currentDate);
            hashMap.put(currentDate, jsonObject);
            currentDateTimeInMillis = currentDateTimeInMillis - 24*60*60*1000;
        }
        return hashMap;
    }

    public RealmList<AppObject> getAppWiseTotalDataUsageForLastNumberOfDays1(Context context, int days)
    {
        long startTime = startTime(Calendar.getInstance().getTimeInMillis()-((days - 1 )*24*3600*1000L));
        long endTime = endTime(Calendar.getInstance().getTimeInMillis());
        Realm realm = RealmDataManager.getInstance(context).getRealm();

        RealmResults<AppObject> appObjects1 = realm.where(AppObject.class).equalTo("is_temp_object" , false).greaterThan("date_in_millis", startTime).lessThan("date_in_millis", endTime).findAll();

        RealmResults<AppObject> appObjects2 = realm.where(AppObject.class).equalTo("is_temp_object" , false).greaterThan("date_in_millis", startTime).lessThan("date_in_millis", endTime).distinct("app_package_name");
        List<AppObject> appObjects = new RealmList<>();


        for(int i=0;i<appObjects2.size();i++ )
        {
            AppObject appObject1  = appObjects2.get(i);
            AppObject appObject = new AppObject();
            appObject.setApp_package_name(appObject1.getApp_package_name());
            appObject.setApp_name(appObject1.getApp_name());
            appObject.setApp_uid(appObject1.getApp_uid());
            long mobiledata = appObjects1.where().equalTo("app_package_name", appObject1.getApp_package_name()).sum("app_mobile_data_usage").longValue();
            long totaldata = appObjects1.where().equalTo("app_package_name", appObject1.getApp_package_name()).sum("app_total_data_usage").longValue();
            appObject.setApp_mobile_data_usage(mobiledata);
            appObject.setApp_total_data_usage(totaldata);
            appObject.setApp_wifi_data_usage(totaldata - mobiledata);
            appObjects.add(appObject);

        }
        sort((RealmList<AppObject>)appObjects, true);

        return (RealmList<AppObject>) appObjects;
    }

    private void sort(RealmList<AppObject> appObjects, final boolean byMobileData)
    {
        Collections.sort(appObjects, new Comparator<AppObject>() {
            //You can change "Name" with "ID" if you want to sort by ID
//                private static final String KEY_NAME = "Name";

            @Override
            public int compare(AppObject a, AppObject b) {

                Long valA = 0L;//new Long();
                Long valB = 0L;
                if(byMobileData) {
                    valA = a.getApp_mobile_data_usage();
                    valB = b.getApp_mobile_data_usage();
                }else
                {
                    valA = a.getApp_total_data_usage();
                    valB = b.getApp_total_data_usage();
                }
                return -valA.compareTo(valB);
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });
    }

    private long startTime(long timeInMillis)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        calendar.set(Calendar.HOUR,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTimeInMillis();
    }

    private long endTime(long timeInMillis)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        calendar.set(Calendar.HOUR,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        calendar.set(Calendar.MILLISECOND,999);
        return calendar.getTimeInMillis();
    }

    public long getTotalMobileDataUsageForLastNumberOfDays1(Context context, int days)
    {
        try {
            Realm realm = RealmDataManager.getInstance(context).getRealm();

            long startTime = startTime(Calendar.getInstance().getTimeInMillis() - ((days - 1) * 24 * 3600 * 1000L));
            long endTime = endTime(Calendar.getInstance().getTimeInMillis());

            RealmResults<InternetDataObject> internetDataObjects = realm.where(InternetDataObject.class).notEqualTo("file_name", "temp_object").greaterThan("date_in_millis", startTime).lessThan("date_in_millis", endTime).findAllSorted("date_in_millis", Sort.DESCENDING);
            return internetDataObjects.where().sum("total_mobile_data").longValue();
        }catch (Exception e)
        {
            return 0L;
        }
    }

}
