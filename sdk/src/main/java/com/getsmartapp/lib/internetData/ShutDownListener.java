package com.getsmartapp.lib.internetData;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author  Peeyush.Singh on 19-02-2016.
 */
public class ShutDownListener extends BroadcastReceiver {

//    private SharedPreferences mSharedPreferences;
    @Override
    public void onReceive(Context context, Intent intent) {
        InternetDataUsageUtil.getInstance(context).handleBroadCastAction(context, intent);
    }
}
