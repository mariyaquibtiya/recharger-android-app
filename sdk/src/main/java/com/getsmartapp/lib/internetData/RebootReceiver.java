package com.getsmartapp.lib.internetData;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author  Peeyush.Singh on 23-02-2016.
 */
public class RebootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        InternetDataUsageUtil.getInstance(context).handleBroadCastAction(context, intent);
    }
}
