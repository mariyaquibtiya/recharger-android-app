package com.getsmartapp.lib.internetData;

import android.app.IntentService;
import android.content.Intent;

/**
 * @author Peeyush.Singh on 29-02-2016.
 */
public class InternetDataCollectionService extends IntentService {


    public InternetDataCollectionService()
    {
        super("InternetDataCollectionService");
    }
    public InternetDataCollectionService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InternetDataUsageUtil.getInstance(this).updateData(getApplicationContext());
    }
}
