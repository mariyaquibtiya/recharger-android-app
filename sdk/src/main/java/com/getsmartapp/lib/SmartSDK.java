package com.getsmartapp.lib;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CallLog;
import android.provider.Settings;
import android.provider.Telephony;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.getsmartapp.lib.data.CallCalculation;
import com.getsmartapp.lib.dataAggregation.DataAggregationUtils;
import com.getsmartapp.lib.database.DBContractor;
import com.getsmartapp.lib.database.SdkAppDatabaseHelper;
import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.interfaces.OnBeginListener;
import com.getsmartapp.lib.interfaces.ProgressDialogListener;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.model.CallModel;
import com.getsmartapp.lib.model.Operator;
import com.getsmartapp.lib.model.QueryResult;
import com.getsmartapp.lib.model.SpandCircle;
import com.getsmartapp.lib.retrofit.DataAggRestClient;
import com.getsmartapp.lib.retrofit.RestClient;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.BundleConstants;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.services.BulkAggregateDataService;
import com.getsmartapp.lib.services.SmsService;
import com.getsmartapp.lib.utils.DateUtil;
import com.getsmartapp.lib.utils.SDKUtils;
import com.getsmartapp.lib.utils.SdkLg;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

//import com.getsmartapp.lib.services.StoreDatainDbService;

/**
 * @author  nitesh.verma on 12/15/15.
 */

public class SmartSDK {

    private static SmartSDK mSmartSDK = null;
    private Context context;
    private String onboardednum = null;
    private SharedPrefManager sharedPrefManager;
    private SQLiteDatabase mDatabase;
    private HashMap<String, String> mUserHashMap;
    private Class mynextActivity;
    private static int mSimNo = 1;
    private int simOnboarded = 0;
    private DualSimManager simManager;
    private OnBeginListener mOnBeginListener;
    private ProgressDialogListener mProgressDialogListener;


    public static SmartSDK getInstance(Context context) {
        if (mSmartSDK == null)
            mSmartSDK = new SmartSDK(context);
        return mSmartSDK;
    }

    private SmartSDK(Context context) {
        this.context = context;
        sharedPrefManager = new SharedPrefManager(context);
        onboardednum = sharedPrefManager.getStringValue(Constants.ON_BOARDING_MOBILE_NUMBER);
        SdkDbHelper sdkDbHelper = SdkDbHelper.getInstance(context);
        mDatabase = sdkDbHelper.getWritableDatabase();
        simManager = new DualSimManager(context);
    }

    private static String mSimType = "prepaid";
    public static final String SIM_TYPE_PREPAID = "prepaid";
    public static final String SIM_TYPE_POSTPAID = "postpaid";

    public static void setSimType(String simtype) {
        mSimType = simtype;
    }

    public String getSimType() {
        return mSimType;
    }

    public static void setSimNum(int simno) {
        mSimNo = simno;
    }

    public void setCallingSim(int simno) {
        sharedPrefManager.setIntValue(Constants.CALLING_SIM, simno);
    }

    public void setDataSim(int simno) {
        sharedPrefManager.setIntValue(Constants.DATA_SIM, simno);
    }

    public void setCallingAllowed(Boolean isallowed) {
        sharedPrefManager.setBooleanValue(Constants.CALLING_ALLOWED, isallowed);
    }

    public Boolean callAllowed() {
        return sharedPrefManager.getBooleanValue(Constants.CALLING_ALLOWED);
    }

    public int getSimNum() {
        return mSimNo;
    }

    public ArrayList<Operator> getOperatorList() {
        SdkAppDatabaseHelper mAppDatabaseHelper = new SdkAppDatabaseHelper(context);
        ArrayList mOperatorBeanArrayList = new ArrayList<>();
        Cursor providerC = null;
        try {
            providerC = mAppDatabaseHelper.getServiceProvider();
            mOperatorBeanArrayList.add(new Operator(1, "Airtel"));
            if (providerC != null && providerC.moveToFirst()) {
                do {
                    String provider = providerC.getString(providerC.getColumnIndex(SdkAppDatabaseHelper.COLUMNS.COLUMN_SERVICE_PROVIDER_NAME));
                    int providerId = providerC.getInt(providerC.getColumnIndex(SdkAppDatabaseHelper.COLUMNS.COLUMN_SERVICE_PROVIDER_ID));
                    mOperatorBeanArrayList.add(new Operator(providerId, provider));
                } while (providerC.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (providerC != null) {
                providerC.close();
            }
        }
        return mOperatorBeanArrayList;
    }

    public ArrayList<Operator> getCircleList() {
        Cursor circleC = null;
        SdkAppDatabaseHelper mAppDatabaseHelper = new SdkAppDatabaseHelper(context);
        ArrayList mCircleBeanArrayList = new ArrayList<>();
        try {
            circleC = mAppDatabaseHelper.getAllCircle();
            mCircleBeanArrayList = new ArrayList<>();
            mCircleBeanArrayList.add(new Operator(5, "New Delhi"));
            if (circleC != null && circleC.moveToFirst()) {
                do {
                    String circle = circleC.getString(circleC.getColumnIndex(SdkAppDatabaseHelper.COLUMNS.COLUMN_CIRCLE_NAME));
                    int circleId = circleC.getInt(circleC.getColumnIndex(SdkAppDatabaseHelper.COLUMNS
                            .COLUMN_CIRCLE_ID));
                    mCircleBeanArrayList.add(new Operator(circleId, circle));
                } while (circleC.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (circleC != null) {
                circleC.close();
            }
        }
        return mCircleBeanArrayList;
    }

    public void onClickBegin(String number, String circleName, int operatorId, String operatorName, int circleID, String datalimit, String billdate, String pref_network) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.CIRCLE_NAME, circleName);
        map.put(Constants.PROVIDER_ID, operatorId + "");
        map.put(Constants.PROVIDER_NAME, operatorName);
        map.put(Constants.SIMTYPE, mSimType);
        map.put(Constants.CIRCLE_ID, circleID + "");
        map.put(Constants.MOBILE, number);
        if (onboardednum != null && !number.equalsIgnoreCase(onboardednum)) {
            map.put(Constants.NUM_CHANGED, "1");
        } else {
            map.put(Constants.NUM_CHANGED, "0");
        }
        if (mSimType.equals(SIM_TYPE_POSTPAID)) {
            map.put(Constants.BILL_CYCLE, billdate);
            map.put(Constants.DATA_LIMIT, datalimit);
            map.put(Constants.PREFERRED_NETWORK_TYPE, pref_network);
            mOnBeginListener.onPostPaid(map);
        } else {
            mOnBeginListener.OnPrePaid(map);
        }
    }

    public void setOnBeginListener(OnBeginListener onBeginListener) {
        mOnBeginListener = onBeginListener;
    }

    public void setDialogListener(ProgressDialogListener progressDialogListener) {
        mProgressDialogListener = progressDialogListener;
    }


    public void saveIntoDatabase(int mSimNo, String mobiledata, Context context, HashMap<String, String> mUserValueHashMap, Class nextActivity) {
        mUserHashMap = mUserValueHashMap;
        mynextActivity = nextActivity;
        if (mSimNo == 1) {
            sharedPrefManager.setIntValue(Constants.ON_BOARDING_PROVIDER_ID, Integer.parseInt(mUserValueHashMap.get
                    (BundleConstants.PROVIDER_ID)));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_PREFERRED_TYPE, mobiledata);
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_SIM_TYPE, mUserValueHashMap.get
                    (BundleConstants.SIMTYPE));
            sharedPrefManager.setIntValue(Constants.ON_BOARDING_CIRCLE_ID, Integer.parseInt(mUserValueHashMap.get
                    (BundleConstants.CIRCLE_ID)));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_MOBILE_NUMBER, mUserValueHashMap.get
                    (BundleConstants.MOBILE));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_OPERATOR_NAME, mUserValueHashMap.get
                    (BundleConstants.PROVIDER_NAME));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_DATA_LIMIT, mUserValueHashMap.get(BundleConstants.DATA_LIMIT));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_BILL_DATE, mUserValueHashMap.get(BundleConstants.BILL_CYCLE));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_CIRCLE, mUserValueHashMap.get(BundleConstants.CIRCLE_NAME));
            String id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure
                    .ANDROID_ID);
            sharedPrefManager.setStringValue(Constants.DEVICE_ID, SDKUtils.getHashedDeviceID(id + mUserValueHashMap.get
                    (BundleConstants.MOBILE)));
        } else {
            sharedPrefManager.setIntValue(Constants.ON_BOARDING_PROVIDER_ID_2, Integer.parseInt(mUserValueHashMap.get
                    (BundleConstants.PROVIDER_ID)));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_PREFERRED_TYPE_2, mobiledata);
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_SIM_TYPE_2, mUserValueHashMap.get
                    (BundleConstants.SIMTYPE));
            sharedPrefManager.setIntValue(Constants.ON_BOARDING_CIRCLE_ID_2, Integer.parseInt(mUserValueHashMap.get
                    (BundleConstants.CIRCLE_ID)));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_MOBILE_NUMBER_2, mUserValueHashMap.get
                    (BundleConstants.MOBILE));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_OPERATOR_NAME_2, mUserValueHashMap.get
                    (BundleConstants.PROVIDER_NAME));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_DATA_LIMIT_2, mUserValueHashMap.get(BundleConstants.DATA_LIMIT));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_BILL_DATE_2, mUserValueHashMap.get(BundleConstants.BILL_CYCLE));
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_CIRCLE_2, mUserValueHashMap.get(BundleConstants.CIRCLE_NAME));
            String id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure
                    .ANDROID_ID);
            sharedPrefManager.setStringValue(Constants.DEVICE_ID, SDKUtils.getHashedDeviceID(id + mUserValueHashMap.get
                    (BundleConstants.MOBILE)));
        }
        String num_chng = sharedPrefManager.getStringValue(Constants.ON_BOARDING_NUMBER_CHANGED);
        if ((num_chng.equalsIgnoreCase("1") || num_chng.equalsIgnoreCase("")) || (sharedPrefManager.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) != 1)) {
            clearAndSaveData(context);
        } else {
            Intent intent = new Intent();
            intent.putExtra("pos", 0);
            intent.putExtra("amount", "");
            intent.putExtra("from", "save_my_money");
            ((Activity) context).setResult(Activity.RESULT_OK, intent);
            ((Activity) context).finish();
        }
    }

    private void clearAndSaveData(Context context) {
        try {
            if (sharedPrefManager.getIntValue(Constants.NO_OF_SIM_ONBOARDED) >= 2) {
                mDatabase.delete(DBContractor.CallDataEntry.TABLE_NAME, null, null);
                mDatabase.delete(DBContractor.SmsDataEntry.TABLE_NAME, null, null);
                mDatabase.delete(DBContractor.SendToServerEntry.TABLE_NAME, null, null);
            }
            SaveCallandSmsLog saveCallLog = new SaveCallandSmsLog(context);
            saveCallLog.execute(mDatabase);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    private class SaveCallandSmsLog extends AsyncTask<SQLiteDatabase, Void, Void> {

        private Context mContext;

        public SaveCallandSmsLog(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog.showProgress(null, false);
            mProgressDialogListener.showProgressDialog();
        }

        @Override
        protected Void doInBackground(SQLiteDatabase... sqLiteDatabases) {
            SQLiteDatabase mDatabase = sqLiteDatabases[0];
            sharedPrefManager.setLongValue(Constants.ON_BOARDING_TIME, System.currentTimeMillis());
            CallCalculation callCalculation = new CallCalculation(mContext,
                    mUserHashMap.get(BundleConstants.SIMTYPE), mUserHashMap.get
                    (BundleConstants.PROVIDER_NAME),
                    mUserHashMap.get(BundleConstants.CIRCLE_NAME));
            callCalculation.fillCountryList();
            callCalculation.setNightStartEndTime();
            callCalculation.fillLandlineList();
            saveCallLogData(mDatabase, callCalculation);
            saveSmsLogData(mDatabase, callCalculation);
            //mDatabase.close();
            Intent smsservice = new Intent(mContext, SmsService.class);
            smsservice.putExtra("circle", mUserHashMap.get
                    (BundleConstants.CIRCLE_NAME));
            smsservice.putExtra("provider", mUserHashMap.get
                    (BundleConstants.PROVIDER_NAME));
            smsservice.putExtra("simType", mUserHashMap.get
                    (BundleConstants.SIMTYPE));
            mContext.startService(smsservice);

            Intent bulkService = new Intent(mContext,
                    BulkAggregateDataService.class);
            mContext.startService(bulkService);
            sharedPrefManager.setStringValue(Constants.ON_BOARDING_NUMBER_CHANGED, "");
            sharedPrefManager.setLongValue(Constants.ON_BOARDING_TIME, System.currentTimeMillis());
            sharedPrefManager.setLongValue(Constants.LAST_RECO_CALL_TIMESTAMP, System.currentTimeMillis());
            if (mSimType.equalsIgnoreCase(SIM_TYPE_POSTPAID)) {
                updateDB();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            String dataexitsflag = sharedPrefManager.getStringValue(Constants.DATA_EXISTS_FLAG,
                    ApiConstants.DATA_PENDING);
            if (dataexitsflag.equalsIgnoreCase(ApiConstants.DATA_PENDING)) {
                checkifDataExists();
            }


            // If not from settings or number update =============================================
            sharedPrefManager.setIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY, 1);
            if (simManager.isHasDualSim()) {
                sharedPrefManager.setIntValue(Constants.NO_OF_SIM_ONBOARDED, sharedPrefManager.getIntValue(Constants.NO_OF_SIM_ONBOARDED) + 1);
                if (sharedPrefManager.getIntValue(Constants.NO_OF_SIM_ONBOARDED) >= 2) {
                    sharedPrefManager.setIntValue(Constants.SLOT2, sharedPrefManager.getIntValue(Constants.SLOTNO));
                    sharedPrefManager.setIntValue(Constants.SIM2_serial, Constants.SECONDARY_SIM_ID);
                    SDKUtils.setSlotForSimId(sharedPrefManager, 1, Constants.SECONDARY_SIM_ID);
                } else {
                    sharedPrefManager.setIntValue(Constants.SLOT1, sharedPrefManager.getIntValue(Constants.SLOTNO));
                    SDKUtils.setSlotForSimId(sharedPrefManager, 0, sharedPrefManager.getIntValue(Constants.SIM1_serial));
                }
            }
//            progressDialog.dismissProgressDialog();
            mProgressDialogListener.dismissProgressDialog();
            if (mynextActivity != null) {
                Intent intent = new Intent(mContext, mynextActivity);
                intent.putExtra("pos", 0);
                intent.putExtra("amount", "");
                intent.putExtra("from", "save_my_money");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            } else {
                Intent intent = new Intent();
                intent.putExtra("pos", 0);
                intent.putExtra("amount", "");
                intent.putExtra("from", "save_my_money");
                ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
                ((Activity) mContext).finish();
            }

        }
    }

    private void updateDB() {
        if (!TextUtils.isEmpty(mUserHashMap.get(BundleConstants.BILL_CYCLE)) && Integer.parseInt
                (mUserHashMap.get(BundleConstants.BILL_CYCLE)) < 30) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBContractor.UserPhoneEntry.COLUMN_BILL_CYCLE_START_DAY, mUserHashMap.get(BundleConstants.BILL_CYCLE));
            contentValues.put(DBContractor.UserPhoneEntry.COLUMN_DATA_LIMIT,
                    mUserHashMap.get(BundleConstants.DATA_LIMIT));
            try {
                mDatabase.update(DBContractor.UserPhoneEntry.TABLE_NAME, contentValues, null, null);
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, "Please enter your cycle date", Toast.LENGTH_LONG).show();
        }
    }

    private void checkifDataExists() {
        DataAggregationUtils dau = new DataAggregationUtils(context, 0);
        HashMap<String, String> map = new HashMap<>();
        if (SDKUtils.isConnectingToInternet(context)) {
            map.put(ApiConstants.DEVICE_ID, dau.getDeviceID());
            map.put(ApiConstants.NOOFDAYS, "30");
            map.put(ApiConstants.JSON_DATA_TYPE, "4");
            DataAggRestClient restClient = new DataAggRestClient();
            restClient.getApiService().getPreviousAggregatedData(map, new Callback<Response>() {
                @Override
                public void success(Response result, Response response) {
                    if (result != null) {
                        //Try to get response body
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONArray arr = new JSONArray(res);
                            JSONObject obj = arr.getJSONObject(0);

                            Date last_updated = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a").parse(obj.getString("last_updated_date"));
                            int no_of_days = obj.getInt("daysCount");
                            if (!DateUtil.dataSentBeforeSevenDays(last_updated) && no_of_days > 3) {
                                sharedPrefManager.setStringValue(Constants.DATA_EXISTS_FLAG, ApiConstants.DATA_AVAILABLE);
                            } else {
                                sharedPrefManager.setStringValue(Constants.DATA_EXISTS_FLAG, ApiConstants.DATA_NOT_AVAILABLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    private void saveCallLogData(SQLiteDatabase mDatabase, CallCalculation callCalculation) {
        Uri allCalls = CallLog.Calls.CONTENT_URI;
        String phone_number, person_name, network_type, number_type, call_type, country_name;
        int call_duration_in_sec, call_duration_in_min, is_night, is_roaming;
        try {
            long date = new Date(System.currentTimeMillis() - 30L * 24 * 3600 * 1000).getTime();
            Cursor cursor = context.getContentResolver().query(allCalls, null, "date" + ">?", new
                    String[]{"" + date}, null);
            if (simManager.isHasDualSim()) {
                if (mSimNo != Constants.SECONDARY_SIM_ID) {
                    cursor = context.getContentResolver().query(allCalls, null, "date" + ">? and " + SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE) + " =?", new
                            String[]{"" + date, mSimNo + ""}, null);
                } else {
                    if (callAllowed()) {
                        int sim1 = sharedPrefManager.getIntValue(Constants.SIM1_serial);
                        cursor = context.getContentResolver().query(allCalls, null, "date" + ">? and " + SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE) + " <>?", new
                                String[]{"" + date, sim1 + ""}, null);
                        callCalculation = new CallCalculation(context, sharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE_2), sharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME_2), sharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE_2));
                    }
                }
            }
//            Log.e("call_count", cursor.getCount() + "");
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(CallLog.Calls._ID));
                    phone_number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                    if (phone_number.length() > 8) {
                        callCalculation.setMobile(phone_number);
                        int call_int = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
                        if (call_int != CallLog.Calls.MISSED_TYPE) {
                            call_type = callCalculation.getCallType(call_int);
                            person_name = cursor.getString(cursor.getColumnIndex(CallLog.Calls
                                    .CACHED_NAME));
                            network_type = callCalculation.getCallNetwork(phone_number);
                            if (!network_type.equals(DataStorageConstants.LANDLINE_CALLNETWORK)) {
                                if (phone_number.startsWith("+91")) {
                                    phone_number = phone_number.substring(3);
                                } else if (phone_number.startsWith("0")) {
                                    phone_number = phone_number.substring(1);
                                }
                            }
                            call_duration_in_sec = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.DURATION));
                            call_duration_in_min = callCalculation.getMinCallDuration(call_duration_in_sec);
                            if (call_duration_in_min > 0) {
                                long start_time = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
                                //int actual_sim_no = cursor.getInt(cursor.getColumnIndex(SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.CALL_TABLE)));
                                is_night = callCalculation.getIsNightValue(start_time);
                                is_roaming = 0;
                                number_type = callCalculation.typeofCallandSms(phone_number);
                                if (callCalculation.isIndianPhone(phone_number)) {
                                    country_name = "India";
                                } else country_name = callCalculation.numberCountry(phone_number);

                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBContractor.CallDataEntry.CALL_ID, id);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER, phone_number);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_NAME, person_name);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_TYPE, call_type);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_IS_NIGHT, is_night);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC, call_duration_in_sec);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN, call_duration_in_min);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE, network_type);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE, number_type);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_IS_ROAMING, is_roaming);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_STARTTIME, start_time);
                                contentValues.put(DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME, country_name);
                                contentValues.put(DBContractor.ReferenceAppEntry.COLUMN_SIMNO, mSimNo);
                                try {
                                    long cid = mDatabase.insertWithOnConflict(DBContractor.CallDataEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
                                    Log.e("call_insert id", String.valueOf(cid));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            sharedPrefManager.setLongValue(Constants.LAST_CALL_SAVED_TIME, System.currentTimeMillis());
        }
    }

    private void saveSmsLogData(SQLiteDatabase mDatabase, CallCalculation callCalculation) {

        String country_name, number, smsNetwork, number_type, sms_type;
        int is_night;
        Uri smssentUri = Uri.parse("content://sms");
        try {
            long date = new Date(System.currentTimeMillis() - 30L * 24 * 3600 * 1000).getTime();
            Cursor smsCursor = context.getContentResolver().query(smssentUri, null, "date" + ">?", new String[]{"" + date}, "date DESC");
            if (simManager.isHasDualSim()) {
                if (mSimNo != Constants.SECONDARY_SIM_ID) {
                    smsCursor = context.getContentResolver().query(smssentUri, null, "date" + ">? and " + SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.SMS_TABLE) + " =?", new String[]{"" + date, mSimNo + ""}, " date DESC");
                } else {
                    int sim1 = sharedPrefManager.getIntValue(Constants.SIM1_serial);
                    smsCursor = context.getContentResolver().query(smssentUri, null, "date" + ">? and " + SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.SMS_TABLE) + " <>?", new String[]{"" + date, sim1 + ""}, " date DESC");
                }
            }

            if (smsCursor != null && smsCursor.moveToFirst()) {
                do {
                    int id = smsCursor.getInt(smsCursor.getColumnIndex(Telephony.Sms._ID));
                    number = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.Sent.ADDRESS));
                    String contact_name = SDKUtils.getContactName(context, number);
                    if (number != null) {
                        if (number.matches(".*\\d.*")) {
                            number = number.replaceAll("\\s+", "");
                            number = number.replaceAll("-", "");
                            if (number.length() > 9) {
                                //sim_no =
                                int type = smsCursor.getInt(smsCursor.getColumnIndex(Telephony.Sms.TYPE));
                                sms_type = callCalculation.getSmsType(type);
                                date = smsCursor.getLong(smsCursor.getColumnIndex(Telephony.Sms.Sent.DATE));
                                callCalculation.setMobile(number);
                                smsNetwork = callCalculation.getCallNetwork(number);
                                is_night = callCalculation.getIsNightValue(date);
                                number_type = callCalculation.typeofCallandSms(number);
                                if (callCalculation.isIndianPhone(number)) {
                                    country_name = "India";
                                } else country_name = callCalculation.numberCountry(number);
                                if (number.startsWith("+91")) {
                                    number = number.substring(3);
                                } else if (number.startsWith("0")) {
                                    number = number.substring(1);
                                }
                                //Log.w("number: ", number + "");
                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DBContractor.SmsDataEntry.MESSAGE_ID, id);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_NUMBER, number);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_NAME, (contact_name ==
                                        null) ?
                                        number : contact_name);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE, number_type);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_IS_NIGHT, is_night);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_DATE_TIME, date);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE, smsNetwork);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME, country_name);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_TYPE, sms_type);
                                contentValues.put(DBContractor.SmsDataEntry.COLUMN_IS_ROAMING, 0);
                                contentValues.put(DBContractor.ReferenceAppEntry.COLUMN_SIMNO, mSimNo);
                                try {
                                    long mid = mDatabase.insertWithOnConflict(DBContractor.SmsDataEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
//                                    Log.e("sms insert id", String.valueOf(mid));
                                } catch (SQLiteException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                // ===== The sms might have operator number
                                for (int i = 0; i < Constants.OPERATOR_SMS_ADDRESSES.length; i++) {
                                    if (number.contains(Constants.OPERATOR_SMS_ADDRESSES[i].toLowerCase())) {
                                        storeOperatorSms(mDatabase, number, smsCursor);
                                    }
                                }
                            }
                        } else {
                            // ======== The sms contains only characters (Can be operator sms)========
                            String[] sms = number.split("[-\\\\s]");
                            for (int i = 0; i < sms.length; i++) {
                                for (int j = 0; j < Constants.OPERATOR_SMS_ADDRESSES.length; j++) {
                                    if (sms[i].toLowerCase().contains(Constants.OPERATOR_SMS_ADDRESSES[j].toLowerCase())) {
                                        storeOperatorSms(mDatabase, number, smsCursor);
                                    }
                                }
                                for (int k = 0; k < Constants.VENDOR_SMS_ADDRESSES.length; k++) {
                                    if (sms[i].toLowerCase().contains(Constants.VENDOR_SMS_ADDRESSES[k].toLowerCase())) {
                                        String body = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.BODY));
                                        if (body.toLowerCase().matches(".*\\\\recharge\\\\b.*")) {
                                            storeOperatorSms(mDatabase, number, smsCursor);
                                        }
                                    }
                                }
                            }

                        }
                    }
                } while (smsCursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sharedPrefManager.setLongValue(Constants.LAST_SMS_SAVE_TIME, System.currentTimeMillis());
        }
    }

    private void storeOperatorSms(SQLiteDatabase mDatabase, String number, Cursor smsCursor) {
        String body = smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.BODY));
        //int sim_id = smsCursor.getInt(smsCursor.getColumnIndex(SDKUtils.getSimIdColumnName(SDKUtils.getPhoneManufacturer(), Constants.SMS_TABLE)));
        int id = smsCursor.getInt(smsCursor.getColumnIndex(Telephony.Sms._ID));
        long timestamp = smsCursor.getLong(smsCursor.getColumnIndex(Telephony.Sms.DATE));
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_ADDRESS, number);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_ID, id);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_MSG_BODY, body);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_SIM_ID, mSimNo);
        contentValues.put(DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP, timestamp);
        try {
            long myid = mDatabase.insertWithOnConflict(DBContractor.SmsInboxEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
//            Log.e("sms_inbox_id", myid + "");
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getCircleAndOperatorForNumber(String number, Context context) {
        HashMap<String, String> map = new HashMap<>();
        String mCircleName = null, mOperatorName = null;
        int mOperatorId = 0, mCircleId = 0;
        String formatnum = number.substring(0, 5);
        SdkAppDatabaseHelper sdkDatabase = new SdkAppDatabaseHelper(context);
        Cursor c = sdkDatabase.getCircleAndOperator(formatnum);
        if (c.moveToFirst()) {
            mCircleName = c.getString(c.getColumnIndex("circle_name"));
            mOperatorName = c.getString(c.getColumnIndex("service_provider_name"));
            mOperatorId = c.getInt(c.getColumnIndex("service_provider_id"));
            mCircleId = c.getInt(c.getColumnIndex("circle_id"));
        }
        c.close();
        map.put(BundleConstants.CIRCLE_NAME, mCircleName);
        map.put(BundleConstants.CIRCLE_ID, mCircleId + "");
        map.put(BundleConstants.PROVIDER_ID, mOperatorId + "");
        map.put(BundleConstants.PROVIDER_NAME, mOperatorName);
        return map;
    }

    public HashMap<String, String> fetchCircleAndOperatorFromServer(String number, Context context) {
        HashMap<String, String> map = null;
        if (number.length() == 10) {
            if (SDKUtils.isConnectingToInternet(context)) {
                RestClient restClient = new RestClient(ApiConstants.BASE_RECHARGER_ADDRESS, null);
                restClient.getApiService().fetchCircleOperator(number, new Callback<SpandCircle>() {
                    @Override
                    public void success(SpandCircle spandCircle, Response response) {
                        if (spandCircle != null) {
                            if (spandCircle.getHeader().getStatus().equals("1")) {
                                final HashMap<String, String> map = new HashMap<>();
                                SpandCircle.BodyEntity obj = spandCircle.getBody();
                                map.put(BundleConstants.CIRCLE_NAME, obj.getCircle());
                                map.put(BundleConstants.CIRCLE_ID, obj.getCircleId() + "");
                                map.put(BundleConstants.PROVIDER_NAME, obj.getSpName());
                                map.put(BundleConstants.PROVIDER_ID, obj.getSpId() + "");
                            } else if (spandCircle.getHeader().getStatus().equals("0")) {
                                //SdkLg.w("error: ", spandCircle.getHeader().getErrors().getErrorList().get(0).getMessage());
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error != null) {
                            // if (error.getCause() != null)
                            // SdkLg.e("retrofit failed", error.getCause().toString());
                        }

                    }
                });
            }

        } else {
            SdkLg.e("number not correct");
        }
        return map;
    }

    public static void startAllServices(Context context) {
        /*if (SDKUtils.isOnBoarded(context))*/ {

            Intent intent2 = new Intent(context, SmsService.class);
            context.startService(intent2);
        }
    }


    // =================== Total Data functions =============================//

    public HashMap<String, Integer> getTotaldata(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.Action action, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.Service service, Constants.DayNight is_night, String start_time, String end_time, String limit) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
        HashMap<String, Integer> outputmap = new HashMap<>();
        try {
            Date st_date = simpleDateFormat.parse(start_time);
            Date en_date = simpleDateFormat2.parse(end_time + " 23:59");  // for the whole end date inclusive
            long s_time = st_date.getTime();
            long e_time = en_date.getTime();
            switch (action) {
                case CALL:
                    outputmap = getCalltotalData(sqLiteDatabase, sim_id, connType, networkType, is_roaming, is_night, service, s_time, e_time, null, limit);
                    break;
                case SMS:
                    outputmap = getSmsTotalData(sqLiteDatabase, sim_id, connType, networkType, is_roaming, is_night, service, s_time, e_time, null, limit);
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputmap;
    }

    public HashMap<String, Integer> getSmsTotalData(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long s_time, long e_time, String seachphone, String limit) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        Cursor cursor = gettotalSmsCursor(sqLiteDatabase, sim_id, connType, networkType, is_night, is_roaming, service, s_time, e_time, seachphone, limit);
        if (cursor != null && cursor.moveToFirst()) {
            int count = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
            hashMap.put(DataStorageConstants.COUNT, count);
        }
        return hashMap;
    }

    private Cursor gettotalSmsCursor(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.DayNight is_night, Constants.Roaming is_roaming, Constants.Service service, long s_time, long e_time, String searchphone, String limit) {
        int roaming = is_roaming != null ? SDKUtils.getRoamingState(is_roaming) : 2;
        String conn = connType != null ? SDKUtils.getConnectionState(connType) : null;
        String network = networkType != null ? SDKUtils.getNetworkState(networkType) : null;
        int daynight = is_night != null ? SDKUtils.getDayNight(is_night) : 2;
        String mservice = service != null ? SDKUtils.getSmsService(service) : null;
        String romingstr = "", connstr = "", networkstr = "", daystr = "", servicestr = "", s_timestr = "", e_timestr = "", simstr = "", limitstr = "", searchstr = "";

        if (roaming != 2) {
            romingstr = " and " + DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + "=" + roaming + " ";
        }
        if (conn != null) {
            connstr = " and " + DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + " = '" + conn + "' ";
        }
        if (network != null) {
            networkstr = " and " + DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + " = '" + network + "' ";
        }
        if (daynight != 2) {
            daystr = " and " + DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + " = '" + daynight + "' ";
        }
        if (mservice != null) {
            servicestr = " and " + DBContractor.SmsDataEntry.COLUMN_TYPE + " = '" + mservice + "' ";
        }
        if (s_time != 0) {
            s_timestr = " where " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + "> " + s_time + " ";
        }
        if (e_time != 0) {
            e_timestr = " and " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + "< " + e_time + " ";
        }
        if (sim_id != 0) {
            simstr = " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "=" + sim_id + " ";
        }
        if (limit != null) {
            limitstr = " limit " + limit;
        }
        if (searchphone != null) {
            searchstr = " and " + DBContractor.SmsDataEntry.COLUMN_NUMBER + " = " + searchphone;
        }
        String query = "SELECT " +
                "count(*) as " + ApiConstants.COLUMN_COUNT +
                " from " + DBContractor.SmsDataEntry.TABLE_NAME +
                s_timestr + e_timestr +
                romingstr + connstr + networkstr + daystr + servicestr + simstr + searchstr + limitstr;

//        Log.e("sms_total_query", query);
        return sqLiteDatabase.rawQuery(query, null);
    }

    public HashMap<String, Integer> getCalltotalData(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long s_time, long e_time, String searchphone, String limit) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        Cursor cursor = getTotalCallCursor(sqLiteDatabase, sim_id, connType, networkType, is_night, is_roaming, service, s_time, e_time, searchphone, limit);
        if (cursor != null && cursor.moveToFirst()) {
            int count = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
            int min = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_MIN));
            int secs = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_SEC));
            hashMap.put(DataStorageConstants.COUNT, count);
            hashMap.put(DataStorageConstants.MINS, min);
            hashMap.put(DataStorageConstants.SECS, secs);
        }
        return hashMap;
    }

/*    // Method to be used only in our app
    public CallModel getCallDataForUsagesScreenInSeconds(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long start, long end, DataStorageConstants.SHOW_CATEGORY_TYPE category_type) {
        int localCallCount = 0;
        int stdCallCount = 0;
        int isdCallCount = 0;
        int totalCount = 0;
        Cursor callCursor = getScreenCallCursor(start, end, sqLiteDatabase, sim_id, connType, networkType, is_roaming, is_night, service);
        LinkedHashMap<String, Integer> hashMap = new LinkedHashMap<>();
        try {
            if (callCursor != null && callCursor.moveToFirst()) {
                do {
                    String number_type = callCursor.getString(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE));
                    long timestamp = callCursor.getLong(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_STARTTIME));
                    int bill_cycle_count = 1;
                    int call_in_min = callCursor.getInt(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC));
                    if (call_in_min >= 1) {
                        totalCount = totalCount + 1;
                        switch (category_type) {
                            case TODAY_TYPE:
                                String current_HoursNo = DateUtil.getHoursFromTimestamp(timestamp);
                                if (hashMap.containsKey(current_HoursNo)) {
                                    hashMap.put(current_HoursNo, hashMap.get(current_HoursNo) + call_in_min);
                                } else {
                                    hashMap.put(current_HoursNo, call_in_min);
                                }
                                break;
                            case THIS_MONTH_TYPE:
                                String current_weekNo = DateUtil.getWeekFromTimestamp(timestamp);
                                if (hashMap.containsKey(current_weekNo)) {
                                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + call_in_min);
                                } else {
                                    hashMap.put(current_weekNo, call_in_min);
                                }
                                break;
                            case THIS_WEEK_TYPE:
                                String current_dayNo = DateUtil.getWeekDayFromTimestamp(timestamp);
                                if (hashMap.containsKey(current_dayNo)) {
                                    hashMap.put(current_dayNo, hashMap.get(current_dayNo) + call_in_min);
                                } else {
                                    hashMap.put(current_dayNo, call_in_min);
                                }
                                break;
                            case LAST_WEEK_TYPE:
                                String last_dayNo = DateUtil.getWeekDayFromTimestamp(timestamp);
                                if (hashMap.containsKey(last_dayNo)) {
                                    hashMap.put(last_dayNo, hashMap.get(last_dayNo) + call_in_min);
                                } else {
                                    hashMap.put(last_dayNo, call_in_min);
                                }
                                break;
                            case BILL_CYCLE_TYPE:
                                String current_weekNo1 = bill_cycle_count+"";//DateUtil.getWeekFromTimestamp(Calendar.getInstance().getTimeInMillis());
                                if (hashMap.containsKey(current_weekNo1)) {
                                    hashMap.put(current_weekNo1, hashMap.get(current_weekNo1) + call_in_min);
                                } else {
                                    if(!hashMap.containsKey(current_weekNo1))
                                        bill_cycle_count++;
                                    hashMap.put(current_weekNo1, call_in_min);
                                }
                                break;
                        }
                        if (number_type != null) {
                            if (number_type.equals(DataStorageConstants.LOCAL_NUMBER)) {
                                localCallCount += call_in_min;
                            } else if (number_type.equals(DataStorageConstants.STD_NUMBER)) {
                                stdCallCount += call_in_min;
                            } else if (number_type.equals(DataStorageConstants.ISD_NUMBER)) {
                                isdCallCount += call_in_min;
                            }
                        }

                    }
                } while (callCursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (callCursor != null) {
                callCursor.close();
            }
        }
        return new CallModel(localCallCount, stdCallCount, isdCallCount, totalCount, hashMap);
    }*/

    // Method to be used only in our app
    public CallModel getCallDataForUsagesScreen(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long start, long end, DataStorageConstants.SHOW_CATEGORY_TYPE category_type) {
        int localCallCount = 0;
        int stdCallCount = 0;
        int isdCallCount = 0;
        int totalCount = 0;

        Cursor callCursor = getScreenCallCursor(start, end, sqLiteDatabase, sim_id, connType, networkType, is_roaming, is_night, service);
        LinkedHashMap<String, Integer> hashMap = new LinkedHashMap<>();
        try {
            if (callCursor != null && callCursor.moveToFirst()) {
                do {
                    String number_type = callCursor.getString(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE));
                    long timestamp = callCursor.getLong(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_STARTTIME));
                    int call_in_min = callCursor.getInt(callCursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN));
                    if (call_in_min >= 1) {
                        totalCount = totalCount + 1;
                        switch (category_type) {
                            case TODAY_TYPE:
                                String current_HoursNo = DateUtil.getHoursFromTimestamp(timestamp);
                                if (hashMap.containsKey(current_HoursNo)) {
                                    hashMap.put(current_HoursNo, hashMap.get(current_HoursNo) + call_in_min);
                                } else {
                                    hashMap.put(current_HoursNo, call_in_min);
                                }
                                break;
                            case THIS_MONTH_TYPE:
                                String current_weekNo = DateUtil.getWeekFromTimestamp(timestamp);
                                if (hashMap.containsKey(current_weekNo)) {
                                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + call_in_min);
                                } else {
                                    hashMap.put(current_weekNo, call_in_min);
                                }
                                break;
                            case THIS_WEEK_TYPE:
                                String current_dayNo = DateUtil.getWeekDayFromTimestamp(timestamp);
                                if (hashMap.containsKey(current_dayNo)) {
                                    hashMap.put(current_dayNo, hashMap.get(current_dayNo) + call_in_min);
                                } else {
                                    hashMap.put(current_dayNo, call_in_min);
                                }
                                break;
                            case LAST_WEEK_TYPE:
                                String last_dayNo = DateUtil.getWeekDayFromTimestamp(timestamp);
                                if (hashMap.containsKey(last_dayNo)) {
                                    hashMap.put(last_dayNo, hashMap.get(last_dayNo) + call_in_min);
                                } else {
                                    hashMap.put(last_dayNo, call_in_min);
                                }
                                break;
                            case BILL_CYCLE_TYPE:
                                String current_weekNo1 = DateUtil.getWeekFromTimestampPostPaid(context, timestamp);

                                if (hashMap.containsKey(current_weekNo1)) {
                                    hashMap.put(current_weekNo1, hashMap.get(current_weekNo1) + call_in_min);
                                } else {
                                    hashMap.put(current_weekNo1, call_in_min);
                                }
                                break;
                        }
                        if (number_type != null) {
                            if (number_type.equals(DataStorageConstants.LOCAL_NUMBER)) {
                                localCallCount += call_in_min;
                            } else if (number_type.equals(DataStorageConstants.STD_NUMBER)) {
                                stdCallCount += call_in_min;
                            } else if (number_type.equals(DataStorageConstants.ISD_NUMBER)) {
                                isdCallCount += call_in_min;
                            }
                        }

                    }
                } while (callCursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (callCursor != null) {
                callCursor.close();
            }
        }
        return new CallModel(localCallCount, stdCallCount, isdCallCount, totalCount, hashMap);
    }

    private Cursor getScreenCallCursor(long start, long end, SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service) {
        int roaming = is_roaming != null ? SDKUtils.getRoamingState(is_roaming) : 2;
        String conn = connType != null ? SDKUtils.getConnectionState(connType) : null;
        String network = networkType != null ? SDKUtils.getNetworkState(networkType) : null;
        int daynight = is_night != null ? SDKUtils.getDayNight(is_night) : 2;
        String mservice = service != null ? SDKUtils.getService(service) : null;
        String romingstr = "", connstr = "", networkstr = "", daystr = "", servicestr = "", s_timestr = "", e_timestr = "", simstr = "", limitstr = "", searchstr = "";
        if (roaming != 2) {
            romingstr = " and " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "=" + roaming + " ";
        }
        if (conn != null) {
            connstr = " and " + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + " = '" + conn + "' ";
        }
        if (network != null) {
            networkstr = " and " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + " = '" + network + "' ";
        }
        if (daynight != 2) {
            daystr = " and " + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + " = '" + daynight + "' ";
        }
        if (mservice != null) {
            servicestr = " and " + DBContractor.CallDataEntry.COLUMN_TYPE + " = '" + mservice + "' ";
        }
        if (end != 0) {
            e_timestr = " and " + DBContractor.CallDataEntry.COLUMN_STARTTIME + "< " + end + " ";
        }
        if (sim_id != 0) {
            simstr = " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "=" + sim_id + " ";
        }
        String query = "select * from " + DBContractor.CallDataEntry.TABLE_NAME + " where " + DBContractor.CallDataEntry.COLUMN_STARTTIME + "> " + start
                + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr;
        return sqLiteDatabase.rawQuery(query, null);
    }


    private Cursor getTotalCallCursor(SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.DayNight is_night, Constants.Roaming is_roaming, Constants.Service service, long s_time, long e_time, String searchphone, String limit) {
        int roaming = is_roaming != null ? SDKUtils.getRoamingState(is_roaming) : 2;
        String conn = connType != null ? SDKUtils.getConnectionState(connType) : null;
        String network = networkType != null ? SDKUtils.getNetworkState(networkType) : null;
        int daynight = is_night != null ? SDKUtils.getDayNight(is_night) : 2;
        String mservice = service != null ? SDKUtils.getService(service) : null;
        String romingstr = "", connstr = "", networkstr = "", daystr = "", servicestr = "", s_timestr = "", e_timestr = "", simstr = "", limitstr = "", searchstr = "";
        if (roaming != 2) {
            romingstr = " and " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "=" + roaming + " ";
        }
        if (conn != null) {
            connstr = " and " + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + " = '" + conn + "' ";
        }
        if (network != null) {
            networkstr = " and " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + " = '" + network + "' ";
        }
        if (daynight != 2) {
            daystr = " and " + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + " = '" + daynight + "' ";
        }
        if (mservice != null) {
            servicestr = " and " + DBContractor.CallDataEntry.COLUMN_TYPE + " = '" + mservice + "' ";
        }
        if (s_time != 0) {
            s_timestr = " where " + DBContractor.CallDataEntry.COLUMN_STARTTIME + "> " + s_time + " ";
        }
        if (e_time != 0) {
            e_timestr = " and " + DBContractor.CallDataEntry.COLUMN_STARTTIME + "< " + e_time + " ";
        }
        if (sim_id != 0) {
            simstr = " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "=" + sim_id + " ";
        }
        if (limit != null) {
            limitstr = " limit " + limit;
        }
        if (searchphone != null) {
            searchstr = " and " + DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + "=" + searchphone;
        }
        String query = "SELECT " +
                "count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                DBContractor.CallDataEntry.COLUMN_STARTTIME + ", " +
                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN +
                " from " + DBContractor.CallDataEntry.TABLE_NAME +
                s_timestr + e_timestr +
                romingstr + connstr + networkstr + daystr + servicestr + simstr + searchstr + limitstr;

        Log.e("call_total_query", query);
        return sqLiteDatabase.rawQuery(query, null);
    }


    // ====================== Granular Data functions =========================//

    public ArrayList<QueryResult> getListData(Constants.DataType dataType, SQLiteDatabase sqLiteDatabase, int sim_id, Constants.Action action, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.Service service, Constants.DayNight is_night, String start_date, String end_date, String limit) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
        ArrayList<QueryResult> outputmap = new ArrayList<>();
        try {
            Date st_date = simpleDateFormat.parse(start_date);
            Date en_date = simpleDateFormat1.parse(end_date + " 23:59");
            long s_time = st_date.getTime();
            long e_time = en_date.getTime();
            outputmap = getRequiredListData(dataType, action, sqLiteDatabase, sim_id, connType, networkType, is_roaming, is_night, service, s_time, e_time, limit);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputmap;
    }

    public ArrayList<QueryResult> getRequiredListData(Constants.DataType dataType, Constants.Action action, SQLiteDatabase sqLiteDatabase, int simid, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long s_time, long e_time, String limit) {
        switch (dataType) {
            case GRANULATED:
                return getGranulatedData(action, sqLiteDatabase, simid, connType, networkType, is_roaming, is_night, service, s_time, e_time, limit);
            case TOP_ENTITIES:
                return getTopData(action, sqLiteDatabase, simid, connType, networkType, is_roaming, is_night, service, s_time, e_time, null, limit, Constants.DATA_FOR.TEST_APP);
            case RECENT_ENTITIES:
                return getRecentData(action, sqLiteDatabase, simid, connType, networkType, is_roaming, is_night, service, s_time, e_time, null, limit, Constants.DATA_FOR.TEST_APP);
        }
        return null;
    }

    public ArrayList<QueryResult> getRecentData(Constants.Action action, SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long s_time, long e_time, String searchphone, String limit, Constants.DATA_FOR data_for) {
        Cursor cursor = getDataCursor(Constants.DataType.RECENT_ENTITIES, sim_id, sqLiteDatabase, action, connType, networkType, is_night, is_roaming, service, s_time, e_time, searchphone, limit,data_for);
        switch (action) {
            case CALL:
                return formCallTopRecentList(cursor,data_for);
            case SMS:
                return formSmsTopRecentList(cursor,data_for);
        }
        return null;
    }

    public ArrayList<QueryResult> getTopData(Constants.Action action, SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long s_time, long e_time, String searchphone, String limit, Constants.DATA_FOR data_for) {
        Cursor cursor = getDataCursor(Constants.DataType.TOP_ENTITIES, sim_id, sqLiteDatabase, action, connType, networkType, is_night, is_roaming, service, s_time, e_time, searchphone, limit,data_for);
        if (cursor!=null && cursor.getCount()>0) {
            switch (action) {
                case CALL:
                    return formCallTopRecentList(cursor, data_for);
                case SMS:
                    return formSmsTopRecentList(cursor, data_for);
            }
        }
        return null;
    }

    public ArrayList<QueryResult> formSmsTopRecentList(Cursor cursor, Constants.DATA_FOR data_for) {
        ArrayList<QueryResult> ls = new ArrayList<>();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    HashMap<String, Integer> map = new HashMap<>();
                    String phone = cursor.getString(cursor.getColumnIndex(DBContractor.SmsDataEntry.COLUMN_NUMBER));
                    int count = 1;
                    if (data_for== Constants.DATA_FOR.TEST_APP) {
                        count = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    }
                    long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SmsDataEntry.COLUMN_DATE_TIME));
                    map.put(DataStorageConstants.COUNT, count);
                    ls.add(new QueryResult(phone, count + "", phone, phone, timestamp, map));
                } while (cursor.moveToNext());
            }
            return ls;

        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public ArrayList<QueryResult> formCallTopRecentList(Cursor cursor, Constants.DATA_FOR data_for) {
        ArrayList<QueryResult> list = new ArrayList<>();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    HashMap<String, Integer> map = new HashMap<>();
                    String phone = cursor.getString(cursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER));
                    String name = cursor.getString(cursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_NAME));
                    int mins = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_MIN));
                    int sec = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_SEC));
                    long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_STARTTIME));
                    String namenum = SDKUtils.isStringNullEmpty(name) ? phone : phone + "-" + name;
                    map.put(DataStorageConstants.MINS, mins);
                    map.put(DataStorageConstants.SECS, sec);
                    int count=1;
                    if (data_for== Constants.DATA_FOR.TEST_APP) {
                        count = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                        map.put(DataStorageConstants.COUNT, count);
                    }else {
                        map.put(DataStorageConstants.COUNT,count);
                    }
                    list.add(new QueryResult(namenum, count + " count, " + mins + " mins, " + sec + " secs", phone, name, timestamp, map));
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }

    public ArrayList<QueryResult> getGranulatedData(Constants.Action action, SQLiteDatabase sqLiteDatabase, int sim_id, Constants.ConnType connType, Constants.NetworkType networkType, Constants.Roaming is_roaming, Constants.DayNight is_night, Constants.Service service, long s_time, long e_time, String limit) {
        ArrayList<QueryResult> results = null;
        Cursor cursor = getDataCursor(Constants.DataType.GRANULATED, sim_id, sqLiteDatabase, action, connType, networkType, is_night, is_roaming, service, s_time, e_time, null, limit, Constants.DATA_FOR.TEST_APP);
        switch (action) {
            case CALL:
                results = formCallGranList(cursor);
                break;
            case SMS:
                results = formSmsGranList(cursor);
                break;
        }
        return results;
    }

    private ArrayList<QueryResult> formSmsGranList(Cursor cursor) {
        ArrayList<QueryResult> mylist = new ArrayList<>();
        HashMap<String, Integer> map = new HashMap<>();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int count = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    String date = cursor.getString(cursor.getColumnIndex(ApiConstants.metadate));
                    long timestamp = cursor.getLong(cursor.getColumnIndex(DBContractor.SmsDataEntry.COLUMN_DATE_TIME));
                    map.put(DataStorageConstants.COUNT, count);
                    mylist.add(new QueryResult(date, count + " count", null, null, timestamp, map));
                } while (cursor.moveToNext());
            }
            return mylist;
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private ArrayList<QueryResult> formCallGranList(Cursor cursor) {
        ArrayList<QueryResult> mylist = new ArrayList<>();
        HashMap<String, Integer> map = new HashMap<>();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int count = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_COUNT));
                    int t_sec = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_SEC));
                    int t_min = cursor.getInt(cursor.getColumnIndex(ApiConstants.COLUMN_TOTAL_MIN));
                    String date = cursor.getString(cursor.getColumnIndex(ApiConstants.metadate));
                    long timstamp = cursor.getLong(cursor.getColumnIndex(DBContractor.CallDataEntry.COLUMN_STARTTIME));
                    map.put(DataStorageConstants.COUNT, count);
                    map.put(DataStorageConstants.MINS, t_min);
                    map.put(DataStorageConstants.SECS, t_sec);
                    mylist.add(new QueryResult(date, count + " count, " + t_sec + " secs, " + t_min + " mins", null, null, timstamp, map));
                } while (cursor.moveToNext());
            }
            return mylist;

        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private Cursor getDataCursor(Constants.DataType dataType, int sim_id, SQLiteDatabase sqLiteDatabase, Constants.Action action, Constants.ConnType connType, Constants.NetworkType networkType, Constants.DayNight is_night, Constants.Roaming is_roaming, Constants.Service service, long s_time, long e_time, String searchphone, String limit, Constants.DATA_FOR datafor) {
        String q = null;
        String mservice = null;
        String romingstr = "", connstr = "", networkstr = "", daystr = "", servicestr = "", s_timestr = "", e_timestr = "", simstr = "", limitstr = "", searchstr = "";
        int roaming = is_roaming != null ? SDKUtils.getRoamingState(is_roaming) : 2;
        String conn = connType != null ? SDKUtils.getConnectionState(connType) : null;
        String network = networkType != null ? SDKUtils.getNetworkState(networkType) : null;
        int daynight = is_night != null ? SDKUtils.getDayNight(is_night) : 2;
        if (searchphone != null) {
            if (action == Constants.Action.CALL) {
                searchstr = " and " + DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + "='" + searchphone + "'";
            } else {
                searchstr = " and " + DBContractor.SmsDataEntry.COLUMN_NUMBER + "='" + searchphone + "'";
            }
        }
        if (action == Constants.Action.CALL) {
            mservice = service != null ? SDKUtils.getService(service) :null;
        } else {
            mservice = service != null ? SDKUtils.getSmsService(service) : null;
        }
        if (roaming != 2) {
            romingstr = " and " + DBContractor.CallDataEntry.COLUMN_IS_ROAMING + "=" + roaming + " ";
        }
        if (conn != null) {
            connstr = " and " + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + " = '" + conn + "' ";
        }
        if (network != null) {
            networkstr = " and " + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + " = '" + network + "' ";
        }
        if (daynight != 2) {
            daystr = " and " + DBContractor.CallDataEntry.COLUMN_IS_NIGHT + " = '" + daynight + "' ";
        }
        if (mservice != null) {
            if (action == Constants.Action.CALL) {
                servicestr = " and " + DBContractor.CallDataEntry.COLUMN_TYPE + " = '" + mservice + "' ";
            } else {
                servicestr = " and " + DBContractor.SmsDataEntry.COLUMN_TYPE + " = '" + mservice + "' ";
            }
        }else {
            if (action== Constants.Action.CALL)
            servicestr = " and "+ DBContractor.CallDataEntry.COLUMN_TYPE+"<> '"+DataStorageConstants.MISSED_CALL_TYPE+"'";
        }
        if (s_time != 0) {
            if (action == Constants.Action.CALL) {
                s_timestr = " where " + DBContractor.CallDataEntry.COLUMN_STARTTIME + "> " + s_time + " ";
            } else {
                s_timestr = " where " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + "> " + s_time + " ";
            }
        }
        if (e_time != 0) {
            if (action == Constants.Action.CALL) {
                e_timestr = " and " + DBContractor.CallDataEntry.COLUMN_STARTTIME + "< " + e_time + " ";
            } else {
                e_timestr = " and " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + "< " + e_time + " ";
            }
        }
        if (sim_id != 0) {
            simstr = " and " + DBContractor.ReferenceAppEntry.COLUMN_SIMNO + "=" + sim_id + " ";
        }
        if (limit != null) {
            limitstr = " limit " + limit;
        }

        switch (dataType) {
            case GRANULATED:
                switch (action) {
                    case CALL:
                        q = "SELECT count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                                DBContractor.CallDataEntry.COLUMN_STARTTIME + ", " +
                                "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " +
                                DBContractor.CallDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " and " + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ">= 1" + " group by " + ApiConstants.metadate + " order by " + ApiConstants.metadate + " desc" + limitstr;
                        break;
                    case SMS:
                        q = "SELECT count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                                DBContractor.SmsDataEntry.COLUMN_DATE_TIME + ", " +
                                "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " +
                                DBContractor.SmsDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " group by " + ApiConstants.metadate + " order by " + ApiConstants.metadate + " desc" + limitstr;
                        break;
                }
                break;
            case TOP_ENTITIES:
                switch (action) {
                    case CALL:
                        q = "SELECT count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                                DBContractor.CallDataEntry.COLUMN_STARTTIME + ", " +
                                DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + ", " + DBContractor.CallDataEntry.COLUMN_NAME + ", " +
                                "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " +
                                DBContractor.CallDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " and " + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ">= 1" + " group by " + DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + " order by " + ApiConstants.COLUMN_TOTAL_MIN + " desc" + limitstr;
                        if (datafor== Constants.DATA_FOR.LIVE_APP){
                            q = "select "+ DBContractor.CallDataEntry.COLUMN_STARTTIME+", "+ DBContractor.CallDataEntry.COLUMN_NAME+", "+ DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER+", sum("+ DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN+") as "+ApiConstants.COLUMN_TOTAL_MIN+", sum("+ DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC+") as "+ApiConstants.COLUMN_TOTAL_SEC+"  from "+ DBContractor.CallDataEntry.TABLE_NAME+s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " and " + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ">= 1" +" group by "+ DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER+ " order by " + ApiConstants.COLUMN_TOTAL_MIN + " desc" + limitstr;
                        }
                        break;
                    case SMS:
                        q = "SELECT count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                                DBContractor.SmsDataEntry.COLUMN_NAME + ", " +
                                DBContractor.SmsDataEntry.COLUMN_DATE_TIME + ", " +
                                "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " +
                                DBContractor.SmsDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " group by " + DBContractor.SmsDataEntry.COLUMN_NAME + " order by " + ApiConstants.COLUMN_COUNT + " desc" + limitstr;
                        if (datafor== Constants.DATA_FOR.LIVE_APP){
                            q = "select *,count(*) as "+ApiConstants.COLUMN_COUNT+" from "+ DBContractor.SmsDataEntry.TABLE_NAME+s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr +" group by "+ DBContractor.SmsDataEntry.COLUMN_NUMBER+ " order by " + ApiConstants.COLUMN_COUNT + " desc" + limitstr;
                        }
                        break;
                }
                break;
            case RECENT_ENTITIES:
                switch (action) {
                    case CALL:
                        q = "SELECT count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ") as " + ApiConstants.COLUMN_TOTAL_SEC + ", " +
                                "sum(" + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ") as " + ApiConstants.COLUMN_TOTAL_MIN + ", " +
                                DBContractor.CallDataEntry.COLUMN_STARTTIME + ", " +
                                DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + ", " + DBContractor.CallDataEntry.COLUMN_NAME + ", " +
                                "strftime('%Y-%m-%d', " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " +
                                DBContractor.CallDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " and " + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ">= 1" + " group by " + DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " desc" + limitstr;
                        if (datafor == Constants.DATA_FOR.LIVE_APP) {
                            q = "select "+ DBContractor.CallDataEntry.COLUMN_STARTTIME+", "+ DBContractor.CallDataEntry.COLUMN_NAME+", "+ DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER+", "+ DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN+" as "+ApiConstants.COLUMN_TOTAL_MIN+", "+ DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC+" as "+ApiConstants.COLUMN_TOTAL_SEC+" from " + DBContractor.CallDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " and " + DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + ">= 1" + " order by " + DBContractor.CallDataEntry.COLUMN_STARTTIME + " desc " + limitstr;
                        }
                        break;
                    case SMS:
                        q = "SELECT count(*) as " + ApiConstants.COLUMN_COUNT + ", " +
                                DBContractor.SmsDataEntry.COLUMN_NAME + ", " +
                                DBContractor.SmsDataEntry.COLUMN_DATE_TIME + ", " +
                                "strftime('%Y-%m-%d', " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " / 1000, 'unixepoch') as " + ApiConstants.metadate + " from " +
                                DBContractor.SmsDataEntry.TABLE_NAME + s_timestr + e_timestr + servicestr + daystr + networkstr + connstr + romingstr + simstr + searchstr + " group by " + DBContractor.SmsDataEntry.COLUMN_NAME + " order by " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " desc" + limitstr;
                        if (datafor== Constants.DATA_FOR.LIVE_APP){
                            q = "select *, count(*) as "+ApiConstants.COLUMN_COUNT+" from "+ DBContractor.SmsDataEntry.TABLE_NAME+s_timestr+e_timestr+servicestr+ daystr + networkstr + connstr + romingstr + simstr + searchstr +" order by " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " desc" + limitstr;
                        }
                        break;
                }
                break;
        }
//        Log.e("query_executed", q);
        return sqLiteDatabase.rawQuery(q, null);
    }

    public String getSimInfoForLogin(Context context) {
        SdkAppDatabaseHelper databaseHelper = new SdkAppDatabaseHelper(context);
        String sim_details = null;
        HashMap<String, String> sim1map = new HashMap<>();
        HashMap<String, String> sim2map = new HashMap<>();
        if (simManager.isDualSIMSupported()) {
//            Log.e("sim_serial1-->",simManager.getSerialNumber(0)+"-- sim_serial2-->"+simManager.getSerialNumber(1));
            if (!SDKUtils.isStringNullEmpty(simManager.getSerialNumber(0))) {
                sim1map.put(Constants.SUBSCRIPTION_ID, simManager.getIMSI(0));
                sim1map.put(Constants.SERIAL_NUMBER, simManager.getSerialNumber(0));
                sim1map.put(Constants.SIM_TYPE, simManager.getPhoneType(0));
                sim1map.put(Constants.NETWORK_TYPE, simManager.getNETWORK_TYPE(0));
                sim1map.put(Constants.ROAMING_STATUS, simManager.getRoamingState(0) + "");
                sim1map.put(Constants.OPERATOR_NAME, simManager.getmSimOperatorName(0));
                sim1map.put(Constants.CIR_NAME, databaseHelper.getCircleFromSerialOperator(simManager.getSerialNumber(0), simManager.getmSimOperatorName(0)));
            }
            if (!SDKUtils.isStringNullEmpty(simManager.getSerialNumber(1))) {
                sim2map.put(Constants.SUBSCRIPTION_ID, simManager.getIMSI(1));
                sim2map.put(Constants.SERIAL_NUMBER, simManager.getSerialNumber(1));
                sim2map.put(Constants.SIM_TYPE, simManager.getPhoneType(1));
                sim2map.put(Constants.NETWORK_TYPE, simManager.getNETWORK_TYPE(1));
                sim2map.put(Constants.ROAMING_STATUS, simManager.getRoamingState(1) + "");
                sim2map.put(Constants.OPERATOR_NAME, simManager.getmSimOperatorName(1));
                sim2map.put(Constants.CIR_NAME, databaseHelper.getCircleFromSerialOperator(simManager.getSerialNumber(1), simManager.getmSimOperatorName(1)));
            }
            List<HashMap<String, String>> list = new ArrayList<>();
            list.add(sim1map);
            list.add(sim2map);
            sim_details = new Gson().toJson(list);
        }
        return sim_details;
    }

    public List getCallContactJson(Context context) {
        long date = new Date(System.currentTimeMillis() - 30L * 24 * 3600 * 1000).getTime();
        List<String> list = new ArrayList<>();
        try {
            Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "date" + ">?", new
                    String[]{"" + date}, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                    if (!list.contains(number) && number.length() >= 9 && list.size() < 90) {
                        list.add("\"" + number + "\"");
                    }
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return list;
    }

}
