package com.getsmartapp.lib.database;

import android.provider.BaseColumns;

/**
 * @author  Nitesh.Verma on 05-05-2015.
 */
public class DBContractor {


    public static final class ReferenceAppEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "app_data_ref";
        public static final String COLUMN_UID = "uid";
        public static final String COLUMN_APP_NAME="app_name";
        public static final String COLUMN_PACKAGE_NAME = "package_name";
        public static final String COLUMN_SIMNO = "sim_no";
        public static final String COLUMN_RECIEVED_DATA = "recieved_data";
        public static final String COLUMN_TRASNMITTED_DATA = "transmitted_data";
        public static final String COLUMN_SHARED_USER_ID ="sharedUserId";
        public static final String COLUMN_CURRENT_NETWORK = "current_network";
        public static final String COLUMN_LAST_UPDATED="last_updated";
        public static final String COLUMN_IS_SYSTEM_APP="is_system_app";
    }

    public static final class CallDataEntry implements BaseColumns
    {
        public static final String TABLE_NAME ="call_data_ref";
        public static final String CALL_ID = "call_id";
        public static final String COLUMN_PHONE_NUMBER ="phone_number";
        public static final String COLUMN_NAME = "person_name";
        public static final String COLUMN_STARTTIME = "start_time";
        public static final String COLUMN_DURATION_IN_SEC = "call_duration_in_sec";
        public static final String COLUMN_DURATION_IN_MIN = "call_duration_in_min";
        public static final String COLUMN_TYPE="call_type";
        public static final String COLUMN_IS_ROAMING="is_roaming";
        public static final String COLUMN_NUMBER_TYPE="number_type";
        public static final String COLUMN_IS_NIGHT="is_night";
        public static final String COLUMN_NETWORK_TYPE = "network_type";
        public static final String COLUMN_CIRCLE_ID = "circle_id";
        public static final String COLUMN_COUNTRY_NAME = "country_name";
        public static final String COLUMN_IS_SYNCED="is_synced";
        public static final String COLUMN_SYNC_TIME="sync_time";
        public static final String COLUMN_LOCATION_LOCALITY = "call_location_locality";
        public static final String COLUMN_LOCATION_STATE = "call_location_state";
        public static final String COLUMN_LOCATION_COUNTRY= "call_location_country";
        public static final String COLUMN_LOCATION_FEATURE_NAME="call_location_feature";
    }

    public static final class UserPhoneEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "user_details";
        public static final String COLUMN_MOBILE_NUMBER = "mobile_number";
        public static final String COLUMN_CIRCLE = "circle";
        public static final String COLUMN_PROVIDER = "provider";
        public static final String COLUMN_SIM_TYPE = "sim_type";
        public static final String COLUMN_BILL_CYCLE_START_DAY = "bill_start_day";
        public static final String COLUMN_MONTLY_BILL = "monthly_bill";
        public static final String COLUMN_CIRCLE_ID = "circle_id";
        public static final String COLUMN_PRE_DATA = "preffered_network";
        public static final String COLUMN_DATA_LIMIT = "data_limit";
    }
    public static final class SmsDataEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "sms_data_ref";
        public static final String MESSAGE_ID = "message_id";
        public static final String COLUMN_NUMBER = "sender_number";
        public static final String COLUMN_NAME= "person_name";
        public static final String COLUMN_DATE_TIME = "timestamp";
        public static final String COLUMN_TYPE="sms_type";
        public static final String COLUMN_IS_ROAMING="is_roaming";
        public static final String COLUMN_NUMBER_TYPE="number_type";
        public static final String COLUMN_IS_NIGHT="is_night";
        public static final String COLUMN_NETWORK_TYPE = "network_type";
        public static final String COLUMN_CIRCLE_ID = "circle_id";
        public static final String COLUMN_COUNTRY_NAME = "country_name";
        public static final String COLUMN_IS_SYNCED="is_synced";
        public static final String COLUMN_SYNC_TIME="sync_time";
        public static final String COLUMN_LOCATION_LOCALITY = "sms_location_locality";
        public static final String COLUMN_LOCATION_STATE = "sms_location_state";
        public static final String COLUMN_LOCATION_COUNTRY= "sms_location_country";
        public static final String COLUMN_LOCATION_FEATURE_NAME="sms_location_feature";
    }


    public static final class CartItemsEntry implements BaseColumns {
        public static final String TABLE_NAME = "cart_items";
        public static final String COLUMN_MOBILE = "mobile_number";
        public static final String COLUMN_OPERATOR = "operator";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_DESC = "plan_desc";
        public static final String COLUMN_PLAN_ID = "plan_id";
        public static final String COLUMN_CIRCLE_ID = "circle_id";
        public static final String COLUMN_SP_ID = "service_provider_id";
        public static final String COLUMN_CAT_ID = "category_id";

    }
    public static final class RecentRechargeEntry implements BaseColumns{
        public static final String TABLE_NAME = "recent_recharge";
        public static final String COLUMN_OPERATOR = "operator";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_NUMBER = "number";
        public static final String COLUMN_IS_POSTPAID = "is_postpaid";
        public static final String COLUMN_TIMESTAMP = "timestamp";
        public static final String COLUMN_OPERATOR_ID = "operator_id";
        public static final String COLUMN_PLAN_ID = "plan_id";
        public static final String COLUMN_CIRCLE_ID = "circle_id";
        public static final String COLUMN_CATAGORY_ID = "category_id";
        public static final String COLUMN_VALIDITY = "validity";
        public static final String COLUMN_DESC = "desc";
        //public static final String COLUMN_PLAN_SUBTITLE = "plan_subtitle";
    }
    public static final class SendToServerEntry implements BaseColumns {
        public static final String TABLE_NAME = "server_synced";
        public static final String COLUMN_DATA_DATE = "data_date";
        public static final String COLUMN_JSON_TYPE = "json_type";
        public static final String COLUMN_TIMESTAMP = "timestamp";
        public static final String COLUMN_SEND_TYPE = "send_type";
        public static final String COLUMN_SIM_ID = "sim_id";
    }
    public static final class UserGroupsEntry implements BaseColumns {
        public static final String TABLE_NAME = "user_group_ref";
        public static final String COLUMN_GROUP_NAME = "group_name";
        public static final String COLUMN_MEMBER_NAME = "member_name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_NUMBER = "number";
    }

    public static final class CardDataEntry implements BaseColumns {
        public static final String TABLE_NAME = "card_data";
        public static final String VIEW_NAME = "view_card";
        public static final String TABLE_ALIAS = "CA";
        public static final String COLUMN_CARD_ID = "card_id";
        public static final String COLUMN_CARD_NAME = "card_name";
        public static final String COLUMN_VAL_RECENCY_FALLBACK = "val_recency_fallback";
        public static final String COLUMN_VAL_CONTEXT_FALLBACK = "val_context_fallback";
        public static final String COLUMN_COEFF_CONTEXT = "coeff_context";
        public static final String COLUMN_POSITION_FIXED = "pos_fixed";
        public static final String COLUMN_PARENT_CARD_ID = "parent_card_id";
        public static final String COLUMN_CARD_VISIBILITY = "card_visibility";
        public static final String COLUMN_UPDATE_TIMESTAMP = "update_timestamp";
        public static final String COLUMN_PRIORITY_FALLBACK = "priority_fallback";
        public static final String COLUMN_SCORE = "score";
        public static final String COLUMN_CARDS_TO_EXCLUDE = "exclude_card";
    }

    public static final class CardCategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "card_category_data";
        public static final String TABLE_ALIAS = "CC";
        public static final String COLUMN_CATEGORY_ID = "category_id";
        public static final String COLUMN_CATEGORY_NAME = "category_name";
    }

    public static final class CardCategoryMapping implements BaseColumns {
        public static final String TABLE_NAME = "card_category_mapping";
        public static final String TABLE_ALIAS = "CCM";
        public static final String COLUMN_CARD_ID = "card_id";
        public static final String COLUMN_CATEGORY_ID = "category_id";
    }
    public static final class SmsInboxEntry implements BaseColumns {
        public static final String TABLE_NAME = "sms_inbox";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_ADDRESS = "sender_address";
        public static final String COLUMN_MSG_BODY = "msg_body";
        public static final String COLUMN_TIMESTAMP = "timestamp";
        public static final String COLUMN_SYNCED = "is_synced";
        public static final String COLUMN_SIM_ID = "sim_id";
        public static final String COLUMN_SYNC_TIMESTAMP = "sync_timestamp";
    }

    public static final class USSDCodeDetails implements BaseColumns {
        public static final String TABLE_NAME = "ussd_code_details";
        public static final String COLUMN_TITLE = "title_text";
        public static final String COLUMN_DESCRIPTION = "description_text";
        public static final String COLUMN_CODE_TYPE = "code_type";
        public static final String COLUMN_DEFAULT_DIAL_CODE = "default_dial_code";
        public static final String COLUMN_UPDATED_DIAL_CODE = "updated_dial_code";
        public static final String COLUMN_DIAL_INSTRUCTION = "dial_instructions";
        public static final String COLUMN_DEFAULT_SMS_CODE = "default_sms_code";
        public static final String COLUMN_UPDATED_SMS_CODE = "updated_sms_code";
        public static final String COLUMN_SMS_INSTRUCTION = "sms_instructions";
        public static final String COLUMN_SMS_TEXT = "sms_text";
    }
}
