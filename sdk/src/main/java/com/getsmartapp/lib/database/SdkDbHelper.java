package com.getsmartapp.lib.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.Connectivity;

public class SdkDbHelper extends SQLiteOpenHelper {
    //    private static final String DB_NAME = "rechargerDBSDK";
    private static final String DB_NAME = "rechargerDB";
    //    private static final int DB_VERSION = 5;
    private static final int DB_VERSION = 14;
    public Context mContext;
    public static SdkDbHelper instance;


    public static synchronized SdkDbHelper getInstance(Context context) {
        if (instance == null)
            instance = new SdkDbHelper(context.getApplicationContext());
        return instance;
    }

    public SdkDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
//        Log.e("created","sdndfs");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String currentNetwork = Connectivity.getNetworkType(mContext);
//        final String CREATE_TABLE_APP_DATA_REF = "CREATE TABLE " + DBContractor.ReferenceAppEntry.TABLE_NAME + " (" +
//                DBContractor.ReferenceAppEntry.COLUMN_UID + " INTEGER NOT NULL, " +
//                DBContractor.ReferenceAppEntry.COLUMN_APP_NAME + " TEXT NOT NULL, " +
//                DBContractor.ReferenceAppEntry.COLUMN_PACKAGE_NAME + " TEXT NOT NULL, " +
//                DBContractor.ReferenceAppEntry.COLUMN_RECIEVED_DATA + " INTEGER DEFAULT 0, " +
//                DBContractor.ReferenceAppEntry.COLUMN_TRASNMITTED_DATA + " INTEGER DEFAULT 0, " +
//                DBContractor.ReferenceAppEntry.COLUMN_CURRENT_NETWORK + " '" + currentNetwork + "', " +
//                DBContractor.ReferenceAppEntry.COLUMN_IS_SYSTEM_APP + " INTEGER DEFAULT 1, " +
//                DBContractor.ReferenceAppEntry.COLUMN_SHARED_USER_ID + " INTEGER, " +
//                DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " INTEGER NOT NULL DEFAULT 1, " +
//                DBContractor.ReferenceAppEntry.COLUMN_LAST_UPDATED + " TEXT DEFAULT " + System.currentTimeMillis() + " , " +
//                "UNIQUE (" + DBContractor.ReferenceAppEntry.COLUMN_APP_NAME + ", " + DBContractor.ReferenceAppEntry.COLUMN_UID + ") ON CONFLICT REPLACE);";
//
//        final String CREATE_TABLE_APP_DATA_USAGE = "CREATE TABLE " + DBContractor.AppDataUsageEntry.TABLE_NAME + " (" +
//                DBContractor.AppDataUsageEntry.COLUMN_APP_NAME + " TEXT NOT NULL, " +
//                DBContractor.AppDataUsageEntry.COLUMN_DATE + " TEXT NOT NULL, " +
//                DBContractor.AppDataUsageEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
//                DBContractor.AppDataUsageEntry.COLUMN_IS_BG + " INTEGER NOT NULL, " +
//                DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_RECIEVED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_2G_DATA_TRANSMITTED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_RECIEVED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_3G_DATA_TRANSMITTED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_RECIEVED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_4G_DATA_TRANSMITTED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_WIFI_DATA_RECIEVED + " INTEGER NOT NULL DEFAULT 0, " +
//                DBContractor.AppDataUsageEntry.COLUMN_WIFI_DATA_TRANSMITTED + " INTEGER NOT NULL DEFAULT 0, " +
//                "UNIQUE (" + DBContractor.AppDataUsageEntry.COLUMN_DATE + ", " + DBContractor.AppDataUsageEntry.COLUMN_APP_NAME + " )" +
//                ")";


        final String CREATE_TABLE_CALL_DATA = "CREATE TABLE " + DBContractor.CallDataEntry.TABLE_NAME + " (" +
                DBContractor.CallDataEntry.CALL_ID + " INTEGER PRIMARY KEY , " +
                DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER + " TEXT NOT NULL, " +
                DBContractor.CallDataEntry.COLUMN_NAME + " TEXT, " +
                DBContractor.CallDataEntry.COLUMN_STARTTIME + " INTEGER NOT NULL, " +
                DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CallDataEntry.COLUMN_DURATION_IN_MIN + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CallDataEntry.COLUMN_IS_NIGHT + " INTEGER DEFAULT 0, " +
                DBContractor.CallDataEntry.COLUMN_IS_ROAMING + " INTEGER DEFAULT 0, " +
                DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + " TEXT NOT NULL, " +
                DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + " TEXT DEFAULT" + Constants.STD_NUMBER + ", " +
                DBContractor.CallDataEntry.COLUMN_LOCATION_FEATURE_NAME + " TEXT, " +
                DBContractor.CallDataEntry.COLUMN_LOCATION_LOCALITY + " TEXT, " +
                DBContractor.CallDataEntry.COLUMN_LOCATION_STATE + " TEXT, " +
                DBContractor.CallDataEntry.COLUMN_LOCATION_COUNTRY + " TEXT, " +
                DBContractor.CallDataEntry.COLUMN_TYPE + " TEXT NOT NULL, " +
                DBContractor.CallDataEntry.COLUMN_CIRCLE_ID + " INTEGER, " +
                DBContractor.CallDataEntry.COLUMN_COUNTRY_NAME + " TEXT , " +
                DBContractor.CallDataEntry.COLUMN_IS_SYNCED + " INTEGER DEFAULT 0, " +
                DBContractor.CallDataEntry.COLUMN_SYNC_TIME + " TEXT, " +
                DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " INTEGER NOT NULL DEFAULT 1, " +
                "UNIQUE (" + DBContractor.CallDataEntry.COLUMN_NETWORK_TYPE + ", " +
                DBContractor.CallDataEntry.COLUMN_DURATION_IN_SEC + ", " + DBContractor.CallDataEntry.COLUMN_NUMBER_TYPE + ", " + DBContractor.CallDataEntry.COLUMN_PHONE_NUMBER +
                ", " + DBContractor.CallDataEntry.COLUMN_STARTTIME + ")" +
                ")";

        final String CREATE_TABLE_USER_PHONE_DATA = "CREATE TABLE " + DBContractor.UserPhoneEntry.TABLE_NAME + " (" +
                DBContractor.UserPhoneEntry.COLUMN_MOBILE_NUMBER + " TEXT NOT NULL, " +
                DBContractor.UserPhoneEntry.COLUMN_CIRCLE + " TEXT NOT NULL, " +
                DBContractor.UserPhoneEntry.COLUMN_PROVIDER + " TEXT NOT NULL, " +
                DBContractor.UserPhoneEntry.COLUMN_SIM_TYPE + " TEXT NOT NULL, " +
                DBContractor.UserPhoneEntry.COLUMN_BILL_CYCLE_START_DAY + " INTEGER, " +
                DBContractor.UserPhoneEntry.COLUMN_MONTLY_BILL + " INTEGER, " +
                DBContractor.UserPhoneEntry.COLUMN_CIRCLE_ID + " INTEGER, " +
                DBContractor.UserPhoneEntry.COLUMN_PRE_DATA + " TEXT NOT NULL, " +
                DBContractor.UserPhoneEntry.COLUMN_DATA_LIMIT + " TEXT, " +
                DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " INTEGER NOT NULL DEFAULT 1, " +
                "UNIQUE (" + DBContractor.UserPhoneEntry.COLUMN_MOBILE_NUMBER + ", " + DBContractor.UserPhoneEntry.COLUMN_SIM_TYPE + ", " + DBContractor.UserPhoneEntry.COLUMN_PROVIDER + ", " + DBContractor.UserPhoneEntry.COLUMN_CIRCLE + ") ON CONFLICT REPLACE" +
                ");";

        final String CREATE_TABLE_USER_SMS_DATA = "CREATE TABLE " + DBContractor.SmsDataEntry.TABLE_NAME + " (" +
                DBContractor.SmsDataEntry.MESSAGE_ID + " INTEGER PRIMARY KEY , " +
                DBContractor.SmsDataEntry.COLUMN_NUMBER + " TEXT NOT NULL, " +
                DBContractor.SmsDataEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                DBContractor.SmsDataEntry.COLUMN_DATE_TIME + " INTEGER NOT NULL, " +
                DBContractor.SmsDataEntry.COLUMN_TYPE + " TEXT NOT NULL, " +
                DBContractor.SmsDataEntry.COLUMN_NUMBER_TYPE + " TEXT DEFAULT" + Constants.STD_NUMBER + ", " +
                DBContractor.SmsDataEntry.COLUMN_NETWORK_TYPE + " TEXT NOT NULL, " +
                DBContractor.SmsDataEntry.COLUMN_IS_NIGHT + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.SmsDataEntry.COLUMN_IS_ROAMING + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.SmsDataEntry.COLUMN_LOCATION_FEATURE_NAME + " TEXT, " +
                DBContractor.SmsDataEntry.COLUMN_LOCATION_LOCALITY + " TEXT, " +
                DBContractor.SmsDataEntry.COLUMN_LOCATION_STATE + " TEXT, " +
                DBContractor.SmsDataEntry.COLUMN_LOCATION_COUNTRY + " TEXT, " +
                DBContractor.SmsDataEntry.COLUMN_CIRCLE_ID + " INTEGER, " +
                DBContractor.SmsDataEntry.COLUMN_COUNTRY_NAME + " TEXT, " +
                DBContractor.SmsDataEntry.COLUMN_IS_SYNCED + " INTEGER DEFAULT 0, " +
                DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.SmsDataEntry.COLUMN_SYNC_TIME + " INTEGER, " +
                "UNIQUE (" + DBContractor.SmsDataEntry.COLUMN_NUMBER + ", " + DBContractor.SmsDataEntry.COLUMN_DATE_TIME + ")" +
                ")";

//        final String CREATE_TABLE_INTERNET_SESSION = "CREATE TABLE " + DBContractor.InternetSessionEntry.TABLE_NAME + " (" +
//                DBContractor.InternetSessionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                DBContractor.InternetSessionEntry.COLUMN_DATE + " TEXT NOT NULL, " +
//                DBContractor.InternetSessionEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
//                DBContractor.InternetSessionEntry.COLUMN_OLDTYPE + " TEXT , " +
//                DBContractor.InternetSessionEntry.COLUMN_NEWTYPE + " TEXT , " +
//                DBContractor.InternetSessionEntry.COLUMN_DELTA_RXDATA + " INTEGER DEFAULT 0, " +
//                DBContractor.InternetSessionEntry.COLUMN_DELTA_TXDATA + " INTEGER DEFAULT 0, " +
//                DBContractor.InternetSessionEntry.COLUMN_TOTAL_RXDATA + " INTEGER DEFAULT 0, " +
//                DBContractor.InternetSessionEntry.COLUMN_TOTAL_TXDATA + " INTEGER DEFAULT 0, " +
//                DBContractor.InternetSessionEntry.COLUMN_IS_ROAMING + " INTEGER DEFAULT 0, " +
//                DBContractor.InternetSessionEntry.COLUMN_LOCATION_FEATURE_NAME + " TEXT, " +
//                DBContractor.InternetSessionEntry.COLUMN_LOCATION_LOCALITY + " TEXT, " +
//                DBContractor.InternetSessionEntry.COLUMN_LOCATION_STATE + " TEXT, " +
//                DBContractor.InternetSessionEntry.COLUMN_LOCATION_COUNTRY + " TEXT, " +
//                DBContractor.InternetSessionEntry.COLUMN_CIRCLE_ID + " INTEGER, " +
//                DBContractor.ReferenceAppEntry.COLUMN_SIMNO + " INTEGER NOT NULL DEFAULT 1, " +
//                "UNIQUE (" + DBContractor.InternetSessionEntry.COLUMN_TIMESTAMP + ", " + DBContractor.InternetSessionEntry.COLUMN_NEWTYPE +
//                ", " + DBContractor.InternetSessionEntry.COLUMN_DELTA_TXDATA + ", " + DBContractor.InternetSessionEntry.COLUMN_DELTA_RXDATA +
//                ", " + DBContractor.InternetSessionEntry.COLUMN_TOTAL_TXDATA + ", " + DBContractor.InternetSessionEntry.COLUMN_TOTAL_RXDATA + ")" +
//                ");";

        final String CREATE_TABLE_SYNCED = "CREATE TABLE " + DBContractor.SendToServerEntry.TABLE_NAME + " (" +
                DBContractor.SendToServerEntry.COLUMN_DATA_DATE + " TEXT NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + " INTEGER NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_SEND_TYPE + " INTEGER NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_SIM_ID+" INTEGER NOT NULL "+
                ")";

        final String CREATE_SMS_INBOX_TABLE = "CREATE TABLE " + DBContractor.SmsInboxEntry.TABLE_NAME + " (" +
                DBContractor.SmsInboxEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBContractor.SmsInboxEntry.COLUMN_ADDRESS + " TEXT NOT NULL, " +
                DBContractor.SmsInboxEntry.COLUMN_MSG_BODY + " TEXT NOT NULL, " +
                DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL DEFAULT " + System.currentTimeMillis() + " , " +
                DBContractor.SmsInboxEntry.COLUMN_SYNCED + " INTEGER NOT NULL DEFAULT 0," +
                DBContractor.SmsInboxEntry.COLUMN_SIM_ID + " INTEGER NOT NULL, " +
                DBContractor.SmsInboxEntry.COLUMN_SYNC_TIMESTAMP+" INTEGER DEFAULT 0, "+
                "UNIQUE (" + DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " ," + DBContractor.SmsInboxEntry.COLUMN_ADDRESS + " )" +
                ")";

//        Log.e("smsInbox", CREATE_SMS_INBOX_TABLE);

//        if (mContext.getPackageName().equalsIgnoreCase("com.getsmartapp"))
//            smartAppTables(db);

        try {
//            Log.e("mesg_table", CREATE_TABLE_USER_SMS_DATA);
//            db.execSQL(CREATE_TABLE_APP_DATA_REF);
//            db.execSQL(CREATE_TABLE_APP_DATA_USAGE);
            db.execSQL(CREATE_TABLE_CALL_DATA);
            db.execSQL(CREATE_TABLE_USER_PHONE_DATA);
            db.execSQL(CREATE_TABLE_USER_SMS_DATA);
//            db.execSQL(CREATE_TABLE_INTERNET_SESSION);
            db.execSQL(CREATE_TABLE_SYNCED);
            db.execSQL(CREATE_SMS_INBOX_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void smartAppTables(SQLiteDatabase db) {
        final String CREATE_TABLE_CART_ITEMS = "CREATE TABLE " + DBContractor.CartItemsEntry.TABLE_NAME + " (" +
                DBContractor.CartItemsEntry.COLUMN_OPERATOR + " TEXT NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_AMOUNT + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_PLAN_ID + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_CIRCLE_ID + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_SP_ID + " INTEGER NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_DESC + " TEXT NOT NULL, " +
                DBContractor.CartItemsEntry.COLUMN_MOBILE + " TEXT , " +
                DBContractor.CartItemsEntry.COLUMN_CAT_ID + " INTEGER NOT NULL, " +
                "UNIQUE (" + DBContractor.CartItemsEntry.COLUMN_AMOUNT + ", " +
                DBContractor.CartItemsEntry.COLUMN_OPERATOR + ")" +
                ");";

        final String CREATE_TABLE_RECENT_RECHARGE = "CREATE TABLE " + DBContractor.RecentRechargeEntry.TABLE_NAME + " (" +
                DBContractor.RecentRechargeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBContractor.RecentRechargeEntry.COLUMN_NUMBER + " TEXT NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_OPERATOR + " TEXT NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_AMOUNT + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_IS_POSTPAID + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.RecentRechargeEntry.COLUMN_OPERATOR_ID + " INTEGER DEFAULT 0, " +
                DBContractor.RecentRechargeEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_PLAN_ID + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_CIRCLE_ID + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_CATAGORY_ID + " INTEGER NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_DESC + " TEXT NOT NULL, " +
                DBContractor.RecentRechargeEntry.COLUMN_VALIDITY + " TEXT NOT NULL, " +
                "UNIQUE (" + DBContractor.RecentRechargeEntry.COLUMN_NUMBER + ", " +
                DBContractor.RecentRechargeEntry.COLUMN_AMOUNT + ") ON CONFLICT IGNORE" +
                ")";

        final String CREATE_GROUP_RECHARGE_TABLE = "CREATE TABLE " + DBContractor.UserGroupsEntry.TABLE_NAME + " (" +
                DBContractor.UserGroupsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DBContractor.UserGroupsEntry.COLUMN_MEMBER_NAME + " TEXT NOT NULL, " +
                DBContractor.UserGroupsEntry.COLUMN_GROUP_NAME + " TEXT NOT NULL, " +
                "UNIQUE (" + DBContractor.UserGroupsEntry.COLUMN_MEMBER_NAME + ", " + DBContractor.UserGroupsEntry.COLUMN_GROUP_NAME + ")" +
                ")";

        /*final String CREATE_CARD_DATA_TABLE = "CREATE TABLE " + DBContractor.CardDataEntry.TABLE_NAME + " (" +
                DBContractor.CardDataEntry.COLUMN_CARD_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardDataEntry.COLUMN_CARD_NAME + " TEXT NOT NULL UNIQUE, " +
                DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_PRIORITY_FALLBACK + " REAL NOT NULL)";

        final String CREATE_CARD_CATEGORY_TABLE = "CREATE TABLE " + DBContractor.CardCategoryEntry.TABLE_NAME + " (" +
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + " TEXT)";

        final String CREATE_CARD_CATEGORY_MAPPING = "CREATE TABLE " + DBContractor.CardCategoryMapping.TABLE_NAME + " (" +
                DBContractor.CardCategoryMapping.COLUMN_CARD_ID + " INTEGER NOT NULL, " +
                DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " INTEGER NOT NULL)";

        final String CREATE_CARD_DATA_VIEW = "CREATE VIEW " + DBContractor.CardDataEntry.VIEW_NAME + " AS SELECT " +
                DataStorageConstants.ALIAS + ".* FROM (SELECT " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_NAME + ", " +
                DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + ", " +
                "MAX((SELECT (0.3*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ")+(0.7*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + "*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + "))) AS " + DBContractor.CardDataEntry.COLUMN_SCORE +
                " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " " + DBContractor.CardDataEntry.TABLE_ALIAS +
                " JOIN " + DBContractor.CardCategoryMapping.TABLE_NAME + " " + DBContractor.CardCategoryMapping.TABLE_ALIAS +
                " ON " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CARD_ID +
                " JOIN " + DBContractor.CardCategoryEntry.TABLE_NAME + " " + DBContractor.CardCategoryEntry.TABLE_ALIAS +
                " ON " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " = " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " WHERE " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " = 1" +
                " GROUP BY " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME +
                " HAVING " + DBContractor.CardDataEntry.COLUMN_SCORE + " > 0" +
                " ORDER BY " + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC" +
                ") " + DataStorageConstants.ALIAS + " GROUP BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID +
                " ORDER BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC" +
                " LIMIT 9";*/

        try {
            db.execSQL(CREATE_TABLE_CART_ITEMS);
            db.execSQL(CREATE_TABLE_RECENT_RECHARGE);
            db.execSQL(CREATE_GROUP_RECHARGE_TABLE);
            /*db.execSQL(CREATE_CARD_DATA_TABLE);
            db.execSQL(CREATE_CARD_CATEGORY_TABLE);
            db.execSQL(CREATE_CARD_CATEGORY_MAPPING);*/

//            insertCardData(db, Constants.INSERT);
//            insertCardCategoryData(db);
//            insertCardCategoryMapping(db);

            //db.execSQL(CREATE_CARD_DATA_VIEW);
//            SdkLg.e("DBHelper", "View Created");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.e("PeeyushKS","ON UPGRADE: SDK_HELPER");
        final String DROP_TABLE_ORDER_TABLE = "DROP TABLE IF EXISTS order_summary";
        final String DROP_TABLE_ADD_CARD = "DROP TABLE IF EXISTS add_card";
        final String DROP_TABLE_REGISTRATION = "DROP TABLE IF EXISTS registration";
        final String DROP_TABLE_GROUP = "DROP TABLE IF EXISTS add_group";

        /*final String DROP_TABLE_CARD_DATA = "DROP TABLE IF EXISTS " + DBContractor.CardDataEntry.TABLE_NAME;

        final String DROP_TABLE_CARD_CATEGORY_MAPPING = "DROP TABLE IF EXISTS " + DBContractor.CardCategoryMapping.TABLE_NAME;

        final String DROP_VIEW_CARD = "DROP VIEW IF EXISTS " + DBContractor.CardDataEntry.VIEW_NAME;

        final String CREATE_CARD_DATA_TABLE = "CREATE TABLE " + DBContractor.CardDataEntry.TABLE_NAME + " (" +
                DBContractor.CardDataEntry.COLUMN_CARD_ID + " INTEGER PRIMARY KEY, " +
                DBContractor.CardDataEntry.COLUMN_CARD_NAME + " TEXT NOT NULL UNIQUE, " +
                DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + " REAL NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + " INTEGER NOT NULL DEFAULT 0, " +
                DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_UPDATE_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.CardDataEntry.COLUMN_PRIORITY_FALLBACK + " REAL NOT NULL)";

        final String CREATE_CARD_CATEGORY_MAPPING = "CREATE TABLE " + DBContractor.CardCategoryMapping.TABLE_NAME + " (" +
                DBContractor.CardCategoryMapping.COLUMN_CARD_ID + " INTEGER NOT NULL, " +
                DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " INTEGER NOT NULL)";

        final String CREATE_CARD_DATA_VIEW = "CREATE VIEW " + DBContractor.CardDataEntry.VIEW_NAME + " AS SELECT " +
                DataStorageConstants.ALIAS + ".* FROM (SELECT " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_NAME + ", " +
                DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_PARENT_CARD_ID + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_POSITION_FIXED + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + ", " +
                DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + ", " +
                "MAX((SELECT (0.3*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_RECENCY_FALLBACK + ")+(0.7*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_VAL_CONTEXT_FALLBACK + "*" + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_COEFF_CONTEXT + "))) AS " + DBContractor.CardDataEntry.COLUMN_SCORE +
                " FROM " + DBContractor.CardDataEntry.TABLE_NAME + " " + DBContractor.CardDataEntry.TABLE_ALIAS +
                " JOIN " + DBContractor.CardCategoryMapping.TABLE_NAME + " " + DBContractor.CardCategoryMapping.TABLE_ALIAS +
                " ON " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID + " = " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CARD_ID +
                " JOIN " + DBContractor.CardCategoryEntry.TABLE_NAME + " " + DBContractor.CardCategoryEntry.TABLE_ALIAS +
                " ON " + DBContractor.CardCategoryMapping.TABLE_ALIAS + "." + DBContractor.CardCategoryMapping.COLUMN_CATEGORY_ID + " = " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID +
                " WHERE " + DBContractor.CardDataEntry.TABLE_ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_VISIBILITY + " = 1" +
                " GROUP BY " + DBContractor.CardCategoryEntry.TABLE_ALIAS + "." + DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME +
                " HAVING " + DBContractor.CardDataEntry.COLUMN_SCORE + " > 0" +
                " ORDER BY " + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC" +
                ") " + DataStorageConstants.ALIAS + " GROUP BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_CARD_ID +
                " ORDER BY " + DataStorageConstants.ALIAS + "." + DBContractor.CardDataEntry.COLUMN_SCORE + " DESC" +
                " LIMIT 9";*/

//        final String DROP_SYNC_TABLE = "DROP TABLE IF EXISTS "+ DBContractor.SendToServerEntry.TABLE_NAME;

/*        String CREATE_SMS_INBOX = "CREATE TABLE IF NOT EXISTS " + DBContractor.SmsInboxEntry.TABLE_NAME + " (" +
                DBContractor.SmsInboxEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBContractor.SmsInboxEntry.COLUMN_ADDRESS + " TEXT NOT NULL, " +
                DBContractor.SmsInboxEntry.COLUMN_MSG_BODY + " TEXT NOT NULL, " +
                DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL DEFAULT " + System.currentTimeMillis() + " , " +
                DBContractor.SmsInboxEntry.COLUMN_SYNCED + " INTEGER NOT NULL DEFAULT 0," +
                DBContractor.SmsInboxEntry.COLUMN_SIM_ID + " INTEGER NOT NULL, " +
                DBContractor.SmsInboxEntry.COLUMN_SYNC_TIMESTAMP+" INTEGER DEFAULT 0, "+
                "UNIQUE (" + DBContractor.SmsInboxEntry.COLUMN_TIMESTAMP + " ," + DBContractor.SmsInboxEntry.COLUMN_ADDRESS + " )" +
                ")";*/

        String ALTER_TABLE_SMS_INBOX = "ALTER TABLE "+ DBContractor.SmsInboxEntry.TABLE_NAME+" ADD COLUMN "+ DBContractor.SmsInboxEntry.COLUMN_SYNC_TIMESTAMP+" INTEGER DEFAULT 0";
     /*   final String CREATE_TABLE_SYNCED = "CREATE TABLE IF NOT EXISTS " + DBContractor.SendToServerEntry.TABLE_NAME + " (" +
                DBContractor.SendToServerEntry.COLUMN_DATA_DATE + " TEXT NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + " INTEGER NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_SEND_TYPE + " INTEGER NOT NULL, " +
                DBContractor.SendToServerEntry.COLUMN_SIM_ID+" INTEGER NOT NULL "+
                ")";*/
        String ALTER_TABLE_SYNCED = "ALTER TABLE " + DBContractor.SendToServerEntry.TABLE_NAME + " ADD COLUMN "+ DBContractor.SendToServerEntry.COLUMN_SIM_ID+" INTEGER DEFAULT 0";// +" (" +

        String ALTER_TABLE_SYNCED2 = "ALTER TABLE " + DBContractor.SendToServerEntry.TABLE_NAME + " ADD COLUMN "+ DBContractor.SendToServerEntry.COLUMN_SEND_TYPE+" INTEGER DEFAULT 0";// +" (" +

//                DBContractor.SendToServerEntry.COLUMN_DATA_DATE + " TEXT NOT NULL, " +
//                DBContractor.SendToServerEntry.COLUMN_JSON_TYPE + " INTEGER NOT NULL, " +
//                DBContractor.SendToServerEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL, " +
//                DBContractor.SendToServerEntry.COLUMN_SEND_TYPE + " INTEGER NOT NULL, " +
//                DBContractor.SendToServerEntry.COLUMN_SIM_ID+" INTEGER NOT NULL "+
//                ")";

        try {
//            Log.e("PeeyushKS", "SDKDbHelper 1");
            if (newVersion>oldVersion){
//                Log.e("PeeyushKS", "SDKDbHelper 2");  DBContractor.SmsInboxEntry.TABLE_NAME
                Cursor cursor1 = db.rawQuery("SELECT * FROM "+ DBContractor.SmsInboxEntry.TABLE_NAME, null);
                if(cursor1.getColumnIndex(DBContractor.SmsInboxEntry.COLUMN_SYNC_TIMESTAMP)<0) {
                    db.execSQL(ALTER_TABLE_SMS_INBOX);
                }

                Cursor cursor = db.rawQuery("SELECT * FROM "+ DBContractor.SendToServerEntry.TABLE_NAME, null); // grab cursor for all data

                if(cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_SIM_ID)<0) {
                    db.execSQL(ALTER_TABLE_SYNCED);
                }

                int deleteStateColumnIndex = cursor.getColumnIndex(DBContractor.SendToServerEntry.COLUMN_SEND_TYPE);

                if(deleteStateColumnIndex < 0)
                {
                    db.execSQL(ALTER_TABLE_SYNCED2);
                }

            }
//            Log.e("PeeyushKS", "SDKDbHelper 3");


            /*db.execSQL(DROP_VIEW_CARD);
            db.execSQL(DROP_TABLE_CARD_DATA);
            db.execSQL(DROP_TABLE_CARD_CATEGORY_MAPPING);
            db.execSQL(CREATE_CARD_DATA_TABLE);*/
//            db.execSQL(CREATE_CARD_CATEGORY_MAPPING);
            db.execSQL(DROP_TABLE_ORDER_TABLE);
//            Log.e("PeeyushKS", "SDKDbHelper 4");
            db.execSQL(DROP_TABLE_ADD_CARD);
//            Log.e("PeeyushKS", "SDKDbHelper 5");
            db.execSQL(DROP_TABLE_REGISTRATION);
//            Log.e("PeeyushKS", "SDKDbHelper 6");
            db.execSQL(DROP_TABLE_GROUP);
//            Log.e("PeeyushKS", "SDKDbHelper 7");
            //db.execSQL(CREATE_CARD_DATA_VIEW);
//            db.execSQL(DROP_SYNC_TABLE);
//            Log.e("PeeyushKS", "SDKDbHelper 8");
//            Log.e("sdkhelper","yo");
        } catch (SQLException e) {
            e.printStackTrace();
//            Log.e("PeeyushKS", "SDKDbHelper 9");
        }
    }
}
