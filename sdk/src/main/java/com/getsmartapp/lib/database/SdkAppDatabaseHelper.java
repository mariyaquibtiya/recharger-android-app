package com.getsmartapp.lib.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.utils.SDKUtils;
import com.getsmartapp.lib.utils.SdkLg;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * @author  nitesh.verma on 12/16/15.
 */
public class SdkAppDatabaseHelper extends SQLiteAssetHelper {

    private static final int DATABASE_VERSION = 3; //Existing Version in Main Code is 3
    private static final String DATABASE_NAME = "recharger.sqlite";
    private static final String TAG = SQLiteAssetHelper.class.getSimpleName();


    public SdkAppDatabaseHelper(Context context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    public interface TABLES {

        String ISD_SERIES_TABLE = "isd_series_master";
        String MOBILE_SERIES_TABLE = "mobile_series_master";
        String SERVICE_PROVIDER_TABLE = "service_provider_master";
        String NIGHT_CALL_TABLE = "night_call_master";
        String LANDLINE_SERIES_TABLE = "landline_series_master";
        String CIRCLE_TABLE = "circle_master";
        String NUM_SERIES_TABLE = "num_range_series";
        String SIM_OPERATOR_CIRCLE = "sim_operator_circle";
        String USSD_TABLE = "ussd_table";
    }

    public interface COLUMNS {
        String COLUMN_CIRCLE_NAME = "circle_name";
        String COLUMN_CIRCLE_ID = "circle_id";
        String COLUMN_SERVICE_PROVIDER_NAME = "service_provider_name";
        String COLUMN_SERVICE_PROVIDER_ID = "service_provider_id";
        String COLUMN_START_TIME = "start_time";
        String COLUMN_END_TIME = "end_time";
        String COLUMN_SERIES = "series";
        String COLUMN_TYPE = "type";
        String COLUMN_SERIES_START = "start";
        String COLUMN_SERIES_END = "end";
        String COLUMN_MCC = "mcc";
        String COLUMN_MNC = "mnc";
        String COLUMN_CIRCLE = "circle";
        String COLUMN_OPERATOR = "operator";
    }

    public Cursor getCircleAndOperator(String num) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select c." + COLUMNS.COLUMN_CIRCLE_NAME + ", c." + COLUMNS.COLUMN_CIRCLE_ID + ", " +
                "p." + COLUMNS.COLUMN_SERVICE_PROVIDER_NAME + ", " +
                "p." + COLUMNS.COLUMN_SERVICE_PROVIDER_ID + "" +
                " from " + TABLES.CIRCLE_TABLE + " c " +
                "inner join " + TABLES.NUM_SERIES_TABLE +
                " m on m." + COLUMNS.COLUMN_CIRCLE_ID + "=c." + COLUMNS.COLUMN_CIRCLE_ID +
                " inner join " + TABLES.SERVICE_PROVIDER_TABLE +
                " p on m." + COLUMNS.COLUMN_SERVICE_PROVIDER_ID + " = p." + COLUMNS.COLUMN_SERVICE_PROVIDER_ID +
                " where m." + COLUMNS.COLUMN_SERIES_START + "<=" + num + " and m." + COLUMNS.COLUMN_SERIES_END + ">=" + num + ";";

        return db.rawQuery(query, null);
    }

    public Cursor getNightTimeCursor(String conn_type, String provider) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select " + COLUMNS.COLUMN_START_TIME + "," + COLUMNS.COLUMN_END_TIME + " from " + TABLES.NIGHT_CALL_TABLE + " n INNER join " +
                TABLES.SERVICE_PROVIDER_TABLE + " s ON n." + COLUMNS.COLUMN_SERVICE_PROVIDER_ID + "=s." + COLUMNS.COLUMN_SERVICE_PROVIDER_ID +
                " where s." + COLUMNS.COLUMN_SERVICE_PROVIDER_NAME + "='" + provider + "' and n." + COLUMNS.COLUMN_TYPE + "='" + conn_type.toUpperCase() + "';";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor getAllCircle() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("Select " + COLUMNS.COLUMN_CIRCLE_NAME + ", " + COLUMNS.COLUMN_CIRCLE_ID + " from " + TABLES.CIRCLE_TABLE, null);
    }

    public Cursor getServiceProvider() {
        SQLiteDatabase db = getReadableDatabase();
        return db.query(TABLES.SERVICE_PROVIDER_TABLE, new String[]{COLUMNS.COLUMN_SERVICE_PROVIDER_ID, COLUMNS.COLUMN_SERVICE_PROVIDER_NAME}, null, null, null, null, null);
    }

    public Cursor getIsdNumberCursor() {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select isd_code,country from isd_series_master";
        Cursor cursor = db.rawQuery(query, null);
        return cursor;

    }

    public Cursor getLandlineCursor() {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select c." + Constants.COLUMN_CIRCLE_NAME + ",l." + Constants.COLUMN_STD_CODE + " from " + TABLES.CIRCLE_TABLE + " c inner join " + TABLES.LANDLINE_SERIES_TABLE + " l on c." + Constants.COLUMN_CIRCLE_ID + "=l." + Constants.COLUMN_CIRCLE_ID + " order by " + Constants.COLUMN_STD_CODE + " ASC";
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }

    public Cursor getAllOperators() {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select p." + Constants.COLUMN_SERVICE_ROVIDER_ID + ", p." + Constants.COLUMN_SERVICE_PROVIDER + " from " + TABLES.SERVICE_PROVIDER_TABLE + " p ";
        return db.rawQuery(query, null);
    }

    public String getCircleFromSerialOperator(String serial, String operator) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String opeere = operator;
        if (!SDKUtils.isStringNullEmpty(opeere) && !SDKUtils.isStringNullEmpty(serial)) {
            String ope = Character.toUpperCase(opeere.charAt(0)) + opeere.substring(1).toLowerCase();
            String twosubs = serial.substring(4, 6);
            String threesubs = serial.substring(4, 7);
            if (ope.contains(" ")) {
                String[] oper = operator.split(" ");
                ope = oper[0];
            }

            String query = "select * from " + TABLES.SIM_OPERATOR_CIRCLE + " where " + COLUMNS.COLUMN_OPERATOR + " = '" + ope + "' and " + COLUMNS.COLUMN_MNC + " like '" + twosubs + "%'";
            SdkLg.e(query);
            Cursor cursor = null;
            try {
                cursor = sqLiteDatabase.rawQuery(query, null);
                if (cursor.getCount()<=0){
                    String singlesub = serial.substring(5, 6);
                    query = "select * from " + TABLES.SIM_OPERATOR_CIRCLE + " where " + COLUMNS.COLUMN_OPERATOR + " = '" + ope + "' and " + COLUMNS.COLUMN_MNC + " = '" + singlesub + "'";
                    cursor = sqLiteDatabase.rawQuery(query,null);
                    SdkLg.e(query);
                }
//                Log.e("cursor_count",cursor.getCount()+"");
                if (cursor != null && cursor.moveToFirst()) {
                    if (cursor.getCount() == 1) {
                        return cursor.getString(cursor.getColumnIndex(COLUMNS.COLUMN_CIRCLE));
                    } else {
                        do {
                            String mnc = cursor.getString(cursor.getColumnIndex(COLUMNS.COLUMN_MNC));
                            mnc = mnc.replaceAll("\u00A0", "");
                            if (mnc.equalsIgnoreCase(threesubs)) {
                                return cursor.getString(cursor.getColumnIndex(COLUMNS.COLUMN_CIRCLE));
                            }
                        } while (cursor.moveToNext());
                    }
                }

            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return null;
    }
}
