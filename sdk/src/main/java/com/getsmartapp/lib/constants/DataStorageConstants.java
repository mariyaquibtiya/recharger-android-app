package com.getsmartapp.lib.constants;

/**
 * @author nitesh on 2/6/15.
 */
public class DataStorageConstants {

    public static final String LOCAL_NUMBER = "local";
    public static final String STD_NUMBER = "std";
    public static final String ISD_NUMBER = "isd";
    public static final String TOLLFREE_NUMBER = "tollfree";


    public static final String SAME_CALLNETWORK = "same";
    public static final String OTHER_CALLNETWORK = "other";
    public static final String LANDLINE_CALLNETWORK = "landline";

    public static final String COLUMN_CIRCLE_NAME = "circle_name";
    public static final String COLUMN_SERVICE_PROVIDER = "service_provider_name";
    public static final String COLUMN_SERVICE_ROVIDER_ID = "service_provider_id";

    public static final String OTHER_SMS_TYPE = "other";
    public static final String INBOX_SMS_TYPE = "inbox";
    public static final String SENT_SMS_TYPE = "sent";

    public static final String MISSED_CALL_TYPE = "missed";
    public static final String OUTGOING_CALL_TYPE = "outgoing";
    public static final String INCOMING_CALL_TYPE = "incoming";

    public static final String COLUMN_START_TIME = "start_time";
    public static final String COLUMN_END_TIME = "end_time";

    public static final String COLUMN_ISD_CODE = "isd_code";
    public static final String COLUMN_COUNTRY = "country";
    public static final String OTHER_COUNTRY = "other";

    public static final String THREEG_CONN = "3G";
    public static final String TWOG_CONN = "2G";
    public static final String FOURG_CONN = "4G";
    public static final String WIFI_CONN = "wifi";


    public static final String COLUMN_STD_CODE = "std_code";
    public static final String COLUMN_CIRCLE_ID = "circle_id";

    public static final String TOTAL_SESSION_BYTE = "total_session_byte";
    public static final String GROUP_NAME = "group_name";
    public static final String SCALL = "scall";

    /*public static final long GAP_TIMESTAMP = System.currentTimeMillis() - DateUtil.dateToTimestamp(DateUtil.getStart(new Date
            (System.currentTimeMillis())));
    public static final long THIS_WEEK_START = System.currentTimeMillis() - (6L * 24 * 3600 * 1000
            + GAP_TIMESTAMP);
    public static final long THIS_MONTH_START = System.currentTimeMillis() - (27L * 24 * 3600 *
            1000 + GAP_TIMESTAMP);
    public static final long LAST_WEEK_START = System.currentTimeMillis() - (13L * 24 * 3600 *
            1000 + GAP_TIMESTAMP);
    public static final long LAST_MONTH_START = System.currentTimeMillis() - (57L * 24 * 3600 * 1000 + GAP_TIMESTAMP);
    public static final long LAST_WEEK_END = THIS_WEEK_START;
    public static final long TODAY_START = DateUtil.dateToTimestamp(DateUtil.getStart(new Date(System.currentTimeMillis())));
    */public static final String COUNT = "count";
    public static final String MINS = "mins";
    public static final String SECS = "secs";


    public enum SHOW_CATEGORY_TYPE {
        THIS_WEEK_TYPE, LAST_WEEK_TYPE, THIS_MONTH_TYPE, LAST_MONTH_TYPE, TODAY_TYPE,YESTERDAY_TYPE, BILL_CYCLE_TYPE
    }


    public static final String CARD_RECHARGE = "recharge";
    public static final String CARD_RECOMMEND_SPL = "recommend_special";
    public static final String CARD_RECOMMEND_COMBO = "recommend_combo";
    public static final String CARD_ORDER_STATUS = "order_status";
    public static final String CARD_REFERRAL = "referral";
    public static final String CARD_REFER_EARN = "refer_earn";
    public static final String CARD_REFERRAL_DISCOUNT = "referral_discount";
    public static final String CARD_GET_REFERRED = "get_referred";
    public static final String CARD_FEEDBACK = "feedback";
    public static final String CARD_LAUNCH_OFFER = "launch_offer";
    public static final String CARD_STD_GRAPH = "std_graph";
    public static final String CARD_STD_PLAN_VALIDITY = "std_plan_validity";
    public static final String CARD_STD_TOP_CONTACTS = "std_top_contacts";
    public static final String CARD_CALLING_SUMMARY = "calling_summary";
    public static final String CARD_CALLING_CONTACTS = "calling_contacts";
    public static final String CARD_DATA_PLAN_VALIDITY = "data_plan_validity";
    public static final String CARD_DATA_TOP_APPS = "data_top_apps";
    public static final String CARD_RECENT_CONTACT_CALLS = "recent_contact_calls";
    public static final String CARD_APP_SHARE = "app_share";
    public static final String CARD_STD_SUMMARY = "std_summary";
    public static final String CARD_DATA_TODAY = "data_today";
    public static final String CARD_DATA_GRAPH = "data_graph";
    public static final String CARD_RECENT_CONTACT_SMS = "recent_contact_sms";
    public static final String CARD_ROAMING = "roaming";
    public static final String CARD_ROAMING_PLANS = "roaming_plans";
    public static final String CARD_ROAMING_ALERT = "roaming_alert";
    public static final String CARD_DATA_PLAN_UPDATE = "data_plan_update";
    public static final String CARD_NUMBER_UPDATE = "number_update";
    public static final String CARD_COMING_SOON = "coming_soon";
    public static final String CARD_RECOMMEND_DATA = "recommend_data";
    public static final String CARD_TODAY_USAGE = "today_usage";
    public static final String CARD_SPL_PLANS_VSERV = "spl_plans_vserv";
    public static final String CARD_WIFI_USAGE = "usage_wifi";

    public static final Integer SHOW_CARD = 1;
    public static final Integer HIDE_CARD = 0;

    public static final String ALIAS = "CARD";

    public static final String[] CARDS_ARRAY = {CARD_RECHARGE, CARD_RECOMMEND_SPL, CARD_RECOMMEND_COMBO, CARD_ORDER_STATUS, CARD_REFERRAL, CARD_REFER_EARN,
            CARD_REFERRAL_DISCOUNT, CARD_GET_REFERRED, CARD_FEEDBACK, CARD_LAUNCH_OFFER, CARD_STD_GRAPH, CARD_STD_PLAN_VALIDITY, CARD_STD_TOP_CONTACTS,
            CARD_CALLING_SUMMARY, CARD_CALLING_CONTACTS, CARD_DATA_PLAN_VALIDITY, CARD_DATA_TOP_APPS, CARD_RECENT_CONTACT_CALLS, CARD_APP_SHARE, CARD_STD_SUMMARY,
            CARD_DATA_TODAY, CARD_DATA_GRAPH, CARD_RECENT_CONTACT_SMS, CARD_ROAMING, CARD_ROAMING_PLANS, CARD_ROAMING_ALERT, CARD_DATA_PLAN_UPDATE,
            CARD_NUMBER_UPDATE, CARD_COMING_SOON, CARD_RECOMMEND_DATA, CARD_TODAY_USAGE, CARD_SPL_PLANS_VSERV, CARD_WIFI_USAGE};

}
