package com.getsmartapp.lib.constants;


import java.util.Arrays;
import java.util.List;

/**
 * @author  Peeyush.Singh on 28-12-2015.
 */
public class ApiConstants {

    //Update Check
    public static final String APP_UPDATE_URL = "https://getsmartapp.com/v.json";
    //SSO IPs
    public static final String SSO_BASE_IP = ApiEndpoints.SSO_BASE_IP;
    public static final String SOCIAL_BASE_IP = ApiEndpoints.SOCIAL_BASE_IP;

    //Recharger and Plan Apis
    public static final String BASE_IP = ApiEndpoints.BASE_IP;
    public static final String BASE_RECHARGER_ADDRESS = ApiEndpoints.BASE_RECHARGER_ADDRESS;
    public static final String BASE_RECOMMENDATION_ADDRESS = ApiEndpoints.BASE_RECOMMENDATION_ADDRESS;
    public static final String BASE_REGISTRATION_ADDRESS = ApiEndpoints.BASE_REGISTRATION_ADDRESS;

    public static final String HOME_URL = BASE_RECOMMENDATION_ADDRESS + "/recommend/all/plans";
    public static final String HOME_DEVICE_URL = BASE_RECOMMENDATION_ADDRESS + "/recommend/device/all/plans";
    public static final String GC_BASE_IP = ApiEndpoints.GC_BASE_IP;
    public static final String PayU_SURL = BASE_RECHARGER_ADDRESS + "/recharge/payUSuccess";
    public static final String PayU_FURL = BASE_RECHARGER_ADDRESS + "/recharge/payUFailure";


    public static final String PayU_CURL = BASE_RECHARGER_ADDRESS + "/recharge/payUCancel";
    public static final String PayU_WALLET_SURL = BASE_RECHARGER_ADDRESS + "/wallet/payUSuccess";
    public static final String PayU_WALLET_FURL = BASE_RECHARGER_ADDRESS + "/wallet/payUFailure";
    public static final String PayU_WALLET_CURL = BASE_RECHARGER_ADDRESS + "/wallet/payUCancel";
    public static final String segId = "seqId";
    public static final String planId = "planId";
    public static final String spId = "spId";
    public static final String cirId = "cirId";
    public static final String sTypeId = "sTypeId";
    public static final String qty = "qty";
    public static final String rechargeTo = "rechargeTo";
    public static final String isSpecialRecharge = "isSpecialRecharge";
    public static final String itemAmount = "itemAmount";
    public static final String itemDiscount = "itemDiscount";
    public static final String itemPayableAmount = "itemPayableAmount";
    public static final String fName = "fName";
    public static final String email = "email";
    public static final String phNo = "phNo";
    public static final String chCode = "chCode";
    public static final String ssoToken = "ssoToken";
    public static final String totalAmount = "totalAmount";
    public static final String isGC = "isGC";
    public static final String gcCode = "gcCode";
    public static final String gcAmount = "gcAmount";
    public static final String isPG = "isPG";
    public static final String pgAmount = "pgAmount";
    public static final String isWallet = "isWallet";
    public static final String walletAmount = "walletAmount";
    public static final String orderItems = "orderItems";
    public static final String user = "user";
    public static final String ssoid = "ssoId";
    public static final String rechargeType = "rechargeType";
    public static final String categoryId = "categoryId";
    public static final String amount = "amount";
    public static final String circle_id = "circleid";
    public static final String operator_id = "operatorid";
    public static final String paymentMode = "paymentMode";
    public static final String bankName = "bankName";
    public static final String cardPlatformName = "cardBrand";
    public static final String maskedCardNo = "maskedCardNo";
    public static final String limit = "limit";
    public static final String beforeId = "beforeId";
    public static final String ssoId = "ssoId";

    public static final int BULK_DATA_TYPE = 1;
    public static final int DAILY_DATA_TYPE = 2;
    public static final int CALLSMS_DATA_TYPE = 1;
    public static final int ROAMING_DATA_TYPE = 2;
    public static final int APP_DATA_TYPE = 3;
    public static final int INTERNET_DATA_TYPE = 4;
    public static final int OPSMS_DATA_TYPE = 5;


        /* ----------------------------Data aggregation API constants ---------------------------*/

    public static final String metadataObj = "metadata";
    public static final String last_sync_timestamp = "last_sync_timestamp";
    public static final String device_id = "device_id";
    public static final String deviceId = "deviceId";
    public static final String sso_id = "sso_id";
    public static final String metadate = "date";
    public static final String last_updated_date = "last_updated_date";
    public static final String dataObj = "data";
    public static final String total_data = "data-total";
    public static final String two_g_data = "data-2G";
    public static final String three_g_data = "data-3G";
    public static final String four_g_data = "data-4G";
    public static final String wifi_data = "data-wifi";
    public static final String total_network = "data-total_network";
    public static final String dayCount = "dayscount";
    public static final String outgoing = "outgoing";
    public static final String mins = "mins";

    //-------------------Branch + Referral SharedPreference Keys-------------------//
    public static final String CURRENT_BRANCH_USER_NAME = "current_branch_user_name";
    public static final String REFERRAL_BRANCH_USER_NAME = "referral_branch_user_name";
    public static final String CURRENT_BRANCH_USER_IDENTITY = "current_branch_user_identity";
    public static final String REFERRAL_BRANCH_USER_IDENTITY = "referral_branch_user_identity";
    public static final String CURRENT_USER_REFERRAL_LINK = "current_user_ref_link";
    public static final String CURRENT_USER_REFERRAL_CODE = "current_user_ref_code";
    public static final String CURRENT_USER_GC_CODE = "current_user_gc_code";
    public static final String IS_ACTIVE_INVITEE_GC = "is_Active_Invitee_GC";
    public static final String CURRENT_USER_IMAGE_URL = "current_user_image_url";
    public static final String REFERRAL_USER_IMAGE_URL = "referral_user_image_url";
    public static final String IS_REFERRED_USER = "is_referred_user";
    public static final String CURRENT_DEVICE_ID = "current_device_id";
    public static final String REFERRAL_DEVICE_ID = "referral_device_id";
    public static final String REFERRAL_CHANNEL = "referral_channel";
    public static final String CURRENT_USER_CHANNEL = "current_user_channel";
    public static final String REFERRAL_EARNING_AMT = "referral_earning_amount";
    public static final String REFERRAL_REDEEMABLE_AMT = "referral_redeemable_amount";
    public static final String REFERRER_ID = "referrerid";
    public static final String START = "start";
    public static final String LIMIT = "limit";
    public static final String INVITEE_SSO_ID = "inviteeSsoId";
    public static final String HAS_MADE_FIRST_TRANSACTION = "hasDoneFirstTransaction";
    public static final String SHOW_WELCOME_MSG = "showWelcomeMsg";
    public static final String T_PUSH_STATE = "pushTransPref";
    public static final String P_PUSH_STATE = "pushPromotionalPref";
    public static final String EMAIL_STATE = "emailPref";
    public static final String SMS_STATE = "smsPref";
    public static final String IS_NEW_DEVICE = "isNewDevice";
    public static final String IS_REFERRED_DEVICE = "isReferredDevice";
    public static final String IS_REFERRED_NUMBER = "isReferredNumber";
    public static final String BRANCH_EMAIL_CTA = "cta";

    //-------------------Roaming CALL Usage Keys-------------------//
    public static final String[] ROAMING_CALL_USAGE_KEYS = {"call-local-same_network", "call-local-other_network", "call-local-landline", "call-std-same_network", "call-std-other_network", "call-std-landline", "call-incoming"};

    public static final Integer ROAMING_CALL_LOCAL_SAME = 0;
    public static final Integer ROAMING_CALL_LOCAL_OTHER = 1;
    public static final Integer ROAMING_CALL_LOCAL_LANDLINE = 2;
    public static final Integer ROAMING_CALL_STD_SAME = 3;
    public static final Integer ROAMING_CALL_STD_OTHER = 4;
    public static final Integer ROAMING_CALL_STD_LANDLINE = 5;
    public static final Integer ROAMING_CALL_INCOMING = 6;
    public static final String AGGREGATION_BASE_URL = ApiEndpoints.AGGREGATION_BASE_URL;
    public static final String JSON_DATA_TYPE = "jsonDataType";
    public static final String DATA_JSON = "jsonData";
    public static final String JSON_DEVICEID = "deviceId";


    //-----------------------------App Usages Keys -----------------------------------//

    public static final String[] APP_USAGE_KEYS = {
            "upload-foreground-2g", "upload-foreground-3g", "upload-foreground-4g", "upload-foreground-wifi", "upload-background-2g", "upload-background-3g", "upload-background-4g", "upload-background-wifi",
            "download-foreground-2g", "download-foreground-3g", "download-foreground-4g", "download-foreground-wifi", "download-background-2g", "download-background-3g", "download-background-4g", "download-background-wifi"
    };

    public static final Integer UPLOAD_FOREGROUND_2G = 0;
    public static final Integer UPLOAD_FOREGROUND_3G = 1;
    public static final Integer UPLOAD_FOREGROUND_4G = 2;
    public static final Integer UPLOAD_FOREGROUND_WIFI = 3;
    public static final Integer UPLOAD_BACKGROUND_2G = 4;
    public static final Integer UPLOAD_BACKGROUND_3G = 5;
    public static final Integer UPLOAD_BACKGROUND_4G = 6;
    public static final Integer UPLOAD_BACKGROUND_WIFI = 7;
    public static final Integer DOWNLOAD_FOREGROUND_2G = 8;
    public static final Integer DOWNLOAD_FOREGROUND_3G = 9;
    public static final Integer DOWNLOAD_FOREGROUND_4G = 10;
    public static final Integer DOWNLOAD_FOREGROUND_WIFI = 11;
    public static final Integer DOWNLOAD_BACKGROUND_2G = 12;
    public static final Integer DOWNLOAD_BACKGROUND_3G = 13;
    public static final Integer DOWNLOAD_BACKGROUND_4G = 14;
    public static final Integer DOWNLOAD_BACKGROUND_WIFI = 15;


    // SSO change password
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";

    // For getting SSO profile pic
    public static final String SSOIDS = "ssoids";

    //For getting Facebook Profile pic image size 120px*120px
    public static final String FB_PROFILE_PIC_URL = "https://graph.facebook.com/v2.4/me/picture?width=120&height=120&access_token=";

    //For getting Google Profile pic image size 120*120
    public static final String GP_PROFILE_BASE_URL = "https://www.googleapis.com/plus/v1/people";

    public static final String TYPE = "type";
    public static final String CIRCLEID = "circleid";
    public static final String CIRCLEIDCOMBO = "circleId";
    public static final String CATEGORYID = "categoryid";
    public static final String OPERATORID = "operatorid";
    public static final String STARTINDEX = "startIndex";
    public static final int LIMIT_VALUE = 15;
    public static final String RECOMMEND = "recommend";
    public static final String RECCOUNT = "recCount";
    public static final int RECCOUNT_VALUE = 2;
    public static final int NO_OF_DAYS_VALUE = 28;
    public static final String NO_OF_DAYS = "numDays";
    public static final String EXCLUDEID = "excludeIds";
    public static final String SPID = "spId";
    public static final String TOPUPBALANCE = "topUpBalance";
    public static final String DATABALANCE = "dataBalance";
    public static final String DATAPREFERENCES = "dataPreference";
    public static final String SERIES = "series";

    public static final String GP_ACCESS_TOKEN = "access_token";

    public static final String NETWORKUSAGES = "networkUsage";
    public static final String CALLINGUSAGES = "callUsage";
    public static final String CATEGORYIDs = "categoryIds";
    public static final int EXTRAPOLATION_DAYS = 45;

    public static final String NOOFDAYS = "numberOfDays";
    public static final String DATA_NOT_AVAILABLE = "data_not_available";
    public static final String DATA_PENDING = "pending";
    public static final String DATA_AVAILABLE = "data_available";
    public static final String CATEGORY_ID = "categoryId";
    public static final String FILTER = "filter";
    public static final String CALL_STD = "call_std";
    public static final String CALL_LOCAL = "call_local";
    public static final String keyword = "keyword";
    public static final String field = "field";
    public static final String LOGIN_TYPE = "loginType";
    public static final String FACEBOOK_DEVICE_ID = "facebookDeviceId";
    public static final String phoneNumber = "phoneNumber";
    public static final String SIGNUP = "signup";
    public static final String STD_PLAN_RECOMMEND_VALUE = "1";

    //-------------------Non Roaming SMS Usage Keys-------------------//
    public static final Integer NON_ROAMING = 0;

    public static final String[] SMS_USAGE_KEYS = {"sms-local-same_network-day-incoming", "sms-local-same_network-day-outgoing", "sms-local-same_network-night-incoming", "sms-local-same_network-night-outgoing",
            "sms-local-other_network-day-incoming", "sms-local-other_network-day-outgoing", "sms-local-other_network-night-incoming", "sms-local-other_network-night-outgoing",
            "sms-std-same_network-day-incoming", "sms-std-same_network-day-outgoing", "sms-std-same_network-night-incoming", "sms-std-same_network-night-outgoing",
            "sms-std-other_network-day-incoming", "sms-std-other_network-day-outgoing", "sms-std-other_network-night-incoming", "sms-std-other_network-night-outgoing",
            "sms-isd-incoming", "sms-isd-outgoing"};

    public static final Integer SMS_LOCAL_SAME_DAY_INCOMING = 0;
    public static final Integer SMS_LOCAL_SAME_DAY_OUTGOING = 1;
    public static final Integer SMS_LOCAL_SAME_NIGHT_INCOMING = 2;
    public static final Integer SMS_LOCAL_SAME_NIGHT_OUTGOING = 3;
    public static final Integer SMS_LOCAL_OTHER_DAY_INCOMING = 4;
    public static final Integer SMS_LOCAL_OTHER_DAY_OUTGOING = 5;
    public static final Integer SMS_LOCAL_OTHER_NIGHT_INCOMING = 6;
    public static final Integer SMS_LOCAL_OTHER_NIGHT_OUTGOING = 7;
    public static final Integer SMS_STD_SAME_DAY_INCOMING = 8;
    public static final Integer SMS_STD_SAME_DAY_OUTGOING = 9;
    public static final Integer SMS_STD_SAME_NIGHT_INCOMING = 10;
    public static final Integer SMS_STD_SAME_NIGHT_OUTGOING = 11;
    public static final Integer SMS_STD_OTHER_DAY_INCOMING = 12;
    public static final Integer SMS_STD_OTHER_DAY_OUTGOING = 13;
    public static final Integer SMS_STD_OTHER_NIGHT_INCOMING = 14;
    public static final Integer SMS_STD_OTHER_NIGHT_OUTGOING = 15;
    public static final Integer SMS_ISD_INCOMING = 16;
    public static final Integer SMS_ISD_OUTGOING = 17;

    // ----------------Data constant for the database retrievel --------------

    public static final String COLUMN_COUNT = "count";
    public static final String COLUMN_TOTAL_SEC = "total_sec";
    public static final String COLUMN_TOTAL_MIN = "total_min";
    public static final String COLUMN_HASH = "hash";


    //-------------------Non Roaming CALL Usage Keys-------------------//
    public static final String[] CALL_USAGE_KEYS = {"call-local-same_network-day-incoming", "call-local-same_network-day-outgoing", "call-local-same_network-night-incoming", "call-local-same_network-night-outgoing",
            "call-local-other_network-day-incoming", "call-local-other_network-day-outgoing", "call-local-other_network-night-incoming", "call-local-other_network-night-outgoing",
            "call-local-landline-day-incoming", "call-local-landline-day-outgoing", "call-local-landline-night-incoming", "call-local-landline-night-outgoing",
            "call-std-same_network-day-incoming", "call-std-same_network-day-outgoing", "call-std-same_network-night-incoming", "call-std-same_network-night-outgoing",
            "call-std-other_network-day-incoming", "call-std-other_network-day-outgoing", "call-std-other_network-night-incoming", "call-std-other_network-night-outgoing",
            "call-std-landline-day-incoming", "call-std-landline-day-outgoing", "call-std-landline-night-incoming", "call-std-landline-night-outgoing"};

    public static final Integer CALL_LOCAL_SAME_DAY_INCOMING = 0;
    public static final Integer CALL_LOCAL_SAME_DAY_OUTGOING = 1;
    public static final Integer CALL_LOCAL_SAME_NIGHT_INCOMING = 2;
    public static final Integer CALL_LOCAL_SAME_NIGHT_OUTGOING = 3;
    public static final Integer CALL_LOCAL_OTHER_DAY_INCOMING = 4;
    public static final Integer CALL_LOCAL_OTHER_DAY_OUTGOING = 5;
    public static final Integer CALL_LOCAL_OTHER_NIGHT_INCOMING = 6;
    public static final Integer CALL_LOCAL_OTHER_NIGHT_OUTGOING = 7;
    public static final Integer CALL_LOCAL_LANDLINE_DAY_INCOMING = 8;
    public static final Integer CALL_LOCAL_LANDLINE_DAY_OUTGOING = 9;
    public static final Integer CALL_LOCAL_LANDLINE_NIGHT_INCOMING = 10;
    public static final Integer CALL_LOCAL_LANDLINE_NIGHT_OUTGOING = 11;
    public static final Integer CALL_STD_SAME_DAY_INCOMING = 12;
    public static final Integer CALL_STD_SAME_DAY_OUTGOING = 13;
    public static final Integer CALL_STD_SAME_NIGHT_INCOMING = 14;
    public static final Integer CALL_STD_SAME_NIGHT_OUTGOING = 15;
    public static final Integer CALL_STD_OTHER_DAY_INCOMING = 16;
    public static final Integer CALL_STD_OTHER_DAY_OUTGOING = 17;
    public static final Integer CALL_STD_OTHER_NIGHT_INCOMING = 18;
    public static final Integer CALL_STD_OTHER_NIGHT_OUTGOING = 19;
    public static final Integer CALL_STD_LANDLINE_DAY_INCOMING = 20;
    public static final Integer CALL_STD_LANDLINE_DAY_OUTGOING = 21;
    public static final Integer CALL_STD_LANDLINE_NIGHT_INCOMING = 22;
    public static final Integer CALL_STD_LANDLINE_NIGHT_OUTGOING = 23;


    //-------------------Roaming SMS Usage Keys-------------------//
    public static final Integer ROAMING = 1;

    public static final String[] ROAMING_SMS_USAGE_KEYS = {"sms-local-same_network", "sms-local-other_network", "sms-std-same_network", "sms-std-other_network", "sms-isd"};

    public static final Integer ROAMING_SMS_LOCAL_SAME = 0;
    public static final Integer ROAMING_SMS_LOCAL_OTHER = 1;
    public static final Integer ROAMING_SMS_STD_SAME = 2;
    public static final Integer ROAMING_SMS_STD_OTHER = 3;
    public static final Integer ROAMING_SMS_ISD = 4;

    public static final String parent_package_name = "parent_package_name";

    public static final String GOOGLEMAP_BASE_ADDRESS = "https://maps.googleapis.com/";


    //    /*  API PARAMS */
    public static final String MOBILE = "mobile";
    public static final String MOBILECODE = "mobilecode";
    public static final String SITE_ID = "siteid";
    public static final String CHANNEL = "channel";
    public static final String EMAILID = "emailid";
    public static final String SSOID = "ssoid";
    public static final String FORCE = "force";
    public static final String GETTICKET = "getticket";
    public static final String SOCIAL_SITE_ID = "siteId";
    public static final String OAUTH_ID = "oauthId";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String TICKETID = "ticketid";
    public static final String FNAME = "fName";
    public static final String LNAME = "lName";
    public static final String EMAIL = "email";
    public static final String PHONE = "phNo";
    public static final String ISCURRENTID = "isCurrentId";
    public static final String ISREFERRED = "isReferred";
    public static final String REFERREDID = "referrerId";
    public static final String COMPLETEREGISTRATION = "completeregistration";
    public static final String DEVICE_ID = "deviceId";
    public static final String IMAGEURL = "imgUrl";
    public static final String NOIMAGEFOUND = "no_image_found";
    public static final String IP_ADDRESS = "ipAddress";
    public static final String ONBOARD_NUMBER = "onboardNumber";
    public static final String SERVICE_PROVIDER_ID = "serviceProviderId";
    public static final String IS_PREPAID = "isPrepaid";
    public static final String TARGET_LOCAL_USAGE = "targetUsageLocalCalls";
    public static final String TARGET_STD_USAGE = "targetUsageStdCalls";
    public static final String TARGET_DATA_USAGE = "targetUsageData";
    public static final String TARGET_SMS_USAGE = "targetUsageSms";


    //    //-------------------Push Notification Channels-------------------//
    public static final String TAG_SIGNUP_NOT_TRANSACTED = "signNotTransacted";
    public static final String TAG_FIRST_REFERRAL_PENDING = "firstReferralPending";
    public static final String TAG_LAZY_REFERRER = "lazyReferrer";

    public static final List<String> QUICK_HELP_USSD_KEYS = Arrays.asList("talktime_balance", "3g_balance", "2g_balance", "sms_balance", "customer_care", "my_mobile_number");
    public static final List<String> QUICK_HELP_SERVICE_KEYS = Arrays.asList("activate_vas", "deactivate_vas", "activate_dnd", "deactivate_dnd", "get_gprs_settings", "balance_transfer", "upgrade_to_4g", "apply_for_mnp");


    public static final String smsfrom = "from";
    public static final String smsto = "to";
    public static final String message = "message";
    public static final String type = "type";
    public static final String timestamp = "timestamp";
    public static final String SIM_DETAILS = "sim_details";
    public static final String gcReason = "gcType";

    public static final String backend_version = "1.4.1";


    public static final String HOTSPOT_API = ApiEndpoints.BASE_IP+"/summarizer/wifihotspots/saveHotSpots";
    public static final String HOTSPOT_RATING_API = ApiEndpoints.BASE_IP+"/summarizer/wifihotspots/rateHotSpot";

}


