package com.getsmartapp.lib.constants;

import com.getsmartapp.lib.utils.DateUtil;

import java.util.Date;

/**
 * @author nitesh.verma on 12/14/15.
 */
public class Constants {

    public static final String APP_STARTED_FIRST_TIME = "APP_STARTED_FIRST_TIME";

    public static final String APP_UDPATE_CHECK_TIME = "APP_UDPATE_CHECK_TIME";
    public static final String APP_UPDATE_URL_RESPONSE = "APP_UPDATE_URL_RESPONSE";
    public static final String APP_UPDATE_SOFT_UPDATE_DIALOG_SHOWN_LAST_TIME = "APP_UPDATE_SOFT_UPDATE_DIALOG_SHOWN_LAST_TIME";


    public static final String ISBOARDINGTUTORIALSHOWN = "isboardingtutorialshown";



    public static final int DEFAULT_TIMEOUT_IN_SECONDS = 20; //
    public static final int DEFAULT_TIMEOUT_IN_MILLISECONDS = 20000; //

    public static final String BACKGROUNDSERVICE = "backgroundservice";

    public static final String LOCAL_NUMBER = "local";
    public static final String STD_NUMBER = "std";
    public static final String ISD_NUMBER = "isd";
    public static final String TOLLFREE_NUMBER = "tollfree";
//    public static final String LANDLINE_NUMBER = "landline";

    public static final String SAME_CALLNETWORK = "same";
    public static final String OTHER_CALLNETWORK = "other";
    public static final String LANDLINE_CALLNETWORK = "landline";

    public static final String OTHER_SMS_TYPE = "other";
    public static final String INBOX_SMS_TYPE = "inbox";
    public static final String SENT_SMS_TYPE = "sent";

    public static final String MISSED_CALL_TYPE = "missed";
    public static final String OUTGOING_CALL_TYPE = "outgoing";
    public static final String INCOMING_CALL_TYPE = "incoming";

    public static final String COLUMN_START_TIME = "start_time";
    public static final String COLUMN_END_TIME = "end_time";

    public static final String COLUMN_ISD_CODE = "isd_code";
    public static final String COLUMN_COUNTRY = "country";
    public static final String OTHER_COUNTRY = "other";

    public static final String THREEG_CONN = "3G";
    public static final String TWOG_CONN = "2G";
    public static final String FOURG_CONN = "4G";
    public static final String WIFI_CONN = "wifi";

    //public static final String ON_BOARDING_MOBILE_NUMBER = "onboarding_number";
    public static final String ON_BOARDED_SIM1_ID = "onboarded_sim1_id";
    public static final String ON_BOARDING_SIM2_ID = "onboarding_sim2_id";

    public static final String NO_OF_SIM_ONBOARDED = "no_of_sim_onboarded";

    public static final String COLUMN_CIRCLE_NAME = "circle_name";
    public static final String COLUMN_SERVICE_PROVIDER = "service_provider_name";
    public static final String COLUMN_SERVICE_ROVIDER_ID = "service_provider_id";

    public static final String COLUMN_STD_CODE = "std_code";
    public static final String COLUMN_CIRCLE_ID = "circle_id";

    public static final String MOBILE = "mobile";
    public static final String LOGIN_TYPE = "login_type";
    public static final String SIGNIN = "signin";
    public static final String SIGNUP = "signup";
    public static final String FROM = "from";
    public static final String PHONE = "phone";
    public static final String SIGNUP_OBJ = "signup_obj";
    public static final String SIMNO = "simno";
    public static final String SLOT2 = "slot2";
    public static final String SLOT1 = "slot1";
    public static final String CALLING_SIM = "calling_sim";
    public static final String DATA_SIM = "data_sim";
    public static final int SIM1_FAKE_NO = 111;
    public static final int SIM2_FAKE_NO = 222;
    public static final String CALLING_ALLOWED = "calling_allowed";
    public static final String LAST_CALL_SAVED_TIME = "last_call_saved_time";
    public static final String LAST_SMS_SAVE_TIME = "last_sms_saved_time";
    public static final String SUBSCRIPTION_ID = "subscription_id";
    public static final String SERIAL_NUMBER = "serial_number";
    public static final String SIM_TYPE = "sim_type";
    public static final String NETWORK_TYPE = "number_type";
    public static final String ROAMING_STATUS = "roaming_status";
    public static final String OPERATOR_NAME = "operator_name";
    public static final int TOTAL_DATA = 0;
    public static final String SHARED_PREFERENCE_SSO_NUMBER = "_SHARED_PREFERENCE_SSO_NUMBER";
    public static final String LAST_PERSISTENT_EVENT = "last_persistent_event";
    public static String BEAN = "bean";
    public static String AUTHTOKEN = "authtoken";
    public static String CODE = "code";
    public static String TICKETID = "ticketid";
    public static String PASSWORD = "password";
    public static String COMBOPLAN = "comboplan";
    public static String EMAIL = "email";
    public static String USER_SSO = "user_sso";

    public static String POS = "pos";
    public static String SIMTYPE = "simtype";
    public static String ORDER_ID = "OrderId";
    public static String ORDER_ITEM_LIST = "OrderItemList";
    public static String ORDER_STATUS = "orderStatus";
    public static String PROMO_DISCOUNT = "promoCodeDiscount";
    public static String PROMO_DISCOUNT_CODE = "pcCode";
    public static String NET_AMOUNT = "netAmountPaid";
    public static String MESSAGE = "message";
    public static String DATE = "date";
    public static String PROVIDER_ID = "providerid";
    public static String PROVIDER_NAME = "providername";
    public static String CIRCLE_ID = "circleid";
    public static String ISRECOMMENDED = "isrecommended";
    public static String TYPEID = "typeid";
    public static String SLASH_N_DASH = "slash_dash";
    public static String ROAMING = "roaming";
    public static String LANDING = "landing";
    public static String TOO_MUCH_DATA = "too_much_data";
    public static String CARD_NAME = "card_name";
    public static final String PREFERRED_NETWORK_TYPE = "network_type";
    public static final String BILL_CYCLE = "bill_cycle";
    public static final String DATA_LIMIT = "data_limit";
    public static String CIRCLE_NAME = "circlename";
    public static String CONTACT_NAME = "contactname";
    public static final String NUM_CHANGED = "num_changed";
    public static final String CIR_NAME = "circle_name";


    public static final String ALIAS = "CARD";


    public static final long GAP_TIMESTAMP = System.currentTimeMillis() - DateUtil.dateToTimestamp(DateUtil.getStart(new Date
            (System.currentTimeMillis())));
    public static final long THIS_WEEK_START = System.currentTimeMillis() - (6L * 24 * 3600 * 1000
            + GAP_TIMESTAMP);
    public static final long THIS_MONTH_START = System.currentTimeMillis() - (27L * 24 * 3600 *
            1000 + GAP_TIMESTAMP);
    public static final long LAST_WEEK_START = System.currentTimeMillis() - (13L * 24 * 3600 *
            1000 + GAP_TIMESTAMP);
    public static final long LAST_WEEK_END = THIS_WEEK_START;
    public static final long TODAY_START = DateUtil.dateToTimestamp(DateUtil.getStart(new Date(System.currentTimeMillis())));
    public static final long LAST_MONTH_START = System.currentTimeMillis() - (57L * 24 * 3600 * 1000 + GAP_TIMESTAMP);

    public static final String INDIA_CODE = "91";

    public static final String FB_ACCESS_TOKEN = "fb_access_token";
    public static final String GP_ACCESS_TOKEN = "gp_access_token";
    
    public static final String BULK_DATA_SENT_ONCE = "bulk_data_sent_once";
    public static final String GP_PIC_URL = "gp_profile_url";


    public static final int LOGIN_REQUEST_CODE = 1;
    public static final int FRAG_BACK_REQUEST = 2;
    public static final String DEVICE_ID = "device_id";
    public static final int OTPSCREEN_REQUEST_CODE = 9;
    public static final int VERIFYMOBILE_REQUEST_CODE = 10;
    public static final int ACCOUNT_PICKER_CODE = 11;


    public static final String LAST_TRANSACTION_ID = "last_trans_id";
    public static final String LAST_TRANSACTION_STATUS = "last_trans_status";
    public static final String ORDER_INIT = "ORDER_INIT";
    public static final String ORDER_SUCCESSFUL = "ORDER_SUCCESSFUL";
    public static final String ORDER_PENDING = "ORDER_PENDING";
    public static final String ORDER_PARTIALLY_SUCCESSFUL = "ORDER_PARTIALLY_SUCCESSFUL";
    public static final String ORDER_FAILED = "ORDER_FAILED";

    public static final String ORDER_PAYMENT_REFUNDED = "ORDER_PAYMENT_REFUNDED";


    public static final String SMS_SERVICE_LAUNCHED = "sms_service_launched";
    public static final String ON_BOARDING_NUMBER_CHANGED = "on_boarding_number_changed";
    public static final String ON_BOARDING_TIME = "onboarding_time";
    public static final String USER_ON_BOARDED_SUCCESSFULLY = "onboarding_successfully";
    public static final String FETCH_FRESH_RECOMMENDATION = "fetch_fresh_recommendation";
    public static final String LAST_RECO_CALL_TIMESTAMP = "last_reco_call_timestamp";
    public static final String DATA_EXISTS_FLAG = "previous_data_exists";


    public static final String LOCATION_SERVICE_RUNNING = "location_service_running";
    public static final int CALL_TABLE = 1;
    public static final int SMS_TABLE = 2;
    public static final int INTERNET_TABLE = 3;
    public static final int APP_TABLE = 4;


    public static String WS_HASH = "ws_hash";
    public static String DELETE_CARD_HASH = "delete_hash";
    public static String SAVE_CARD_HASH = "save_hash";
    public static String FETCH_CARD_HASH = "fetch_hash";
    public static String VAS_CARD_HASH = "vas_hash";
    public static String DELETE_STORED_CVV_HASH = "delete_stored_cvv";

    public static boolean findlocation = true;

    public static final String KEY = "key";
    public static final String LATLONG = "latlng";

    public static final String[] OPERATOR_SMS_ADDRESSES  = {"VFCARE","140","191","Vodafone","AIRMTA","AIRBBU", "AIROAM", "Airtel", "AIR", "121","Idea","52811","Aircel","Uninor"};
    public static final String[] VENDOR_SMS_ADDRESSES = {"FCHRGE","PAYTM","MOBIKW","SMARTP"};
    public static final char space = ' ';

    public static final String PRODUCT_INFO = "RECHARGER";
    public static final String USER_CREDENTIALS = "ra:ra";
    public static final String SSO_TOKEN = "dsfkgbdifsidsffisfdsfds";
    public static final String PLANSUGGESTION = "planSuggestion";


    public static final String emailPref = "emailPref";
    public static final String smsPref = "smsPref";

    public static final int QUERY_SAVED_CARDS_FROM_DB = 1;
    public static final String USER_DATA = "user_data";
    public static final String CART_DATA = "cart_data";

//    public static final String SSO_MOBILE = "sso_mobile";
    public static final String BACKEND_MOBILE = "backend_mobile";
    public static final String SUCCESSFUL_RECHARGE = "RECHARGE_SUCCESSFUL";
    public static final String RECHARGE_OPERATOR_SIDE = "ORDER_INIT";

    public static final String INSTREAM_DATA = "user_transacted_3g_instream";
    public static final String INSTREAM_RC = "user_transacted_ratecutter_instream";
    public static final String NUMBER_OF_DAYS = "number_of_days";
    public static final String NUMBER = "recharge_number";
    public static final String CATEGORYID = "categoryId";

    public static final int PICK_CONTACT = 1234;

    public static final String GET_PLAN_API_DETAILS = "get_plan_api_details";
    public static String prefUpdated = "prefUpdated";
    public static final String pushTransPref = "pushTransPref";
    public static final String pushWidgetPref = "pushWidgetPref";

    public static final String pushPromotionalPref = "pushPromPref";

    public static final String pushWifiPref = "pushWifiPref";


    public static final int PHONE_CHANGE_REQUEST_CODE = 50;

    public static final String CHECK_HIJACK = "check_hijack";
    public static final int INSERT = 0;
    public static final int UPDATE = 1;

    public static final String COACH_MARK_1 = "coach_1";
    public static final String COACH_MARK_2 = "coach_2";
    public static final String CUSTOM_RECO_VIEW = "custom_reco_view";

    public static final String CARD_JSON_FOR_TAPPED = "CARD_JSON_FOR_TAPPED";


    public static final String DESCRIPTION = "description";
    public static final String VALIDITY = "validity";
    public static final String AMOUNT = "amount";
    public static final String PLANID = "planId";
    public static final String CALLINGUSAGE = "calling_usage";
    public static final String SMARTCOMBO = "user_transacted_smartcombo";
    public static final String DATAUSAGE = "data_usage";
    public static final String WIFIUSAGE = "wifi_usage";
    public static final String STDUSAGE = "std_usage";

    // For 1st sim
    public static final String ON_BOARDING_MOBILE_NUMBER = "onboarding_number";
    public static final String ON_BOARDING_OPERATOR_NAME = "onboarding_operator_name";
    public static final String ON_BOARDING_PROVIDER_ID = "onboarding_provider_id";
    public static final String ON_BOARDING_PREFERRED_TYPE = "onboarding_preferred_type";
    public static final String ON_BOARDING_SIM_TYPE = "onboarding_sim_type";
    public static final String ON_BOARDING_CIRCLE_ID = "onboarding_circle_id";
    public static final String ON_BOARDING_DATA_LIMIT = "onboarding_data_limit";
    public static final String ON_BOARDING_BILL_DATE = "onboarding_bill_date";
    public static final String ON_BOARDING_CIRCLE = "onboarding_circle";
    public static final String ON_BOARDING_BILL_START_DATE_TIME_MILLIS = "onboarding_bill_start_date_time_millis";
    public static final String ON_BOARDING_BILL_END_DATE_TIME_MILLIS = "onboarding_bill_end_date_time_millis";
    public static final String ON_BOARDING_BILL_DAYS = "onboarding_bill_days";


    // For 2nd sim
    public static final String ON_BOARDING_MOBILE_NUMBER_2 = "onboarding_number_2";
    public static final String ON_BOARDING_OPERATOR_NAME_2 = "onboarding_operator_name_2";
    public static final String ON_BOARDING_PROVIDER_ID_2 = "onboarding_provider_id_2";
    public static final String ON_BOARDING_PREFERRED_TYPE_2 = "onboarding_preferred_type_2";
    public static final String ON_BOARDING_SIM_TYPE_2 = "onboarding_sim_type_2";
    public static final String ON_BOARDING_CIRCLE_ID_2 = "onboarding_circle_id_2";
    public static final String ON_BOARDING_DATA_LIMIT_2 = "onboarding_data_limit_2";
    public static final String ON_BOARDING_BILL_DATE_2 = "onboarding_bill_date_2";
    public static final String ON_BOARDING_CIRCLE_2 = "onboarding_circle_2";


    public static final String LAST_ROAMING_ALERT_TIMESTAMP = "last_roaming_alert_timestamp";
    public static final String CARD_RANK_JSON = "card_rank_json";
    public static final String APP_OPEN_COUNT = "app_open_count";
    public static final String SUCCESSFUL_TRANSACTION_COUNT = "successful_transaction_count";
    public static final String OLD_APP_VERSION = "old_app_version";
    public static final String LAST_CALL_SUMMARY_SMS = "last_call_summary_sms";
    public static final String LAST_DATA_SUMMARY_SMS = "last_data_summary_sms";
    public static final String LAST_RECOMMENDED_PLAN_SMS = "last_recommended_plan_sms";
    public static final String LAST_NOT_REGISTERED_SMS = "last_not_registered_sms";
    public static final String RECOMMENDATION_SMS_COUNT = "recommendation_sms_count";
    public static final String NOT_REGISTERED_SMS_COUNT = "not_registered_sms_count";
    public static final String LAST_CALL_SUMMARY_PUSH = "last_call_summary_push";
    public static final String LAST_DATA_SUMMARY_PUSH = "last_data_summary_push";
    public static final String LAST_WIFI_SUMMARY_PUSH = "last_wifi_summary_push";
    public static final String CALL_SUMMARY_PUSH_TYPE_1 = "call_summary_push_type_1";
    public static final String CALL_SUMMARY_PUSH_TYPE_2 = "call_summary_push_type_2";
    public static final String CALL_SUMMARY_PUSH_TYPE_3 = "call_summary_push_type_3";
    public static final String CALL_SUMMARY_PUSH_TYPE_4 = "call_summary_push_type_4";
    public static final String DATA_SUMMARY_PUSH_TYPE_1 = "data_summary_push_type_1";
    public static final String DATA_SUMMARY_PUSH_TYPE_2 = "data_summary_push_type_2";
    public static final String DATA_SUMMARY_PUSH_TYPE_3 = "data_summary_push_type_3";
    public static final String WIFI_SUMMARY_PUSH_TYPE_1 = "wifi_summary_push_type_1";
    public static final String WIFI_SUMMARY_PUSH_TYPE_2 = "wifi_summary_push_type_2";
    public static final String IS_QUICKHELP_FETCHED = "is_quickhelp_fetched";

    public static final String SIM1_serial = "sim1_serial";
    public static final String SIM2_serial = "sim2_serial";
    public static final String SIM_CHANGED = "sim_changed";
    public static final String MOBILENUMBER_ACTIVITY_NUM = "mobilenumber_activity_num";
    public static final String APPLICATION_KEY = "eaed5177-3cfe-4bd7-ab3a-c9b7c4fdf682";
    public static final String SLOTNO = "slotno";
    public static final String SIM1_ONBOARDED = "sim1_onboarded";
    public static final String SIM2_ONBOARDED = "sim2_onboarded";
    public static final String SIM_SAVED = "sim_saved";
    public static final String SENDING_DATA = "sending_data";
    public static final String IS_REFERRAL_IMAGE_SAVED = "is_referral_image_saved";
    public static final int NON_ROAMING_DATA_LIMIT = 60;
    public static final int ROAMING_DATA_LIMIT = 120;
    public static final int SECONDARY_SIM_ID = 100;
    public static final int CALLING_SIM_ID = 200;
    public static final int DATA_SIM_ID = 300;
    public static final String SIM_USAGES_DIALOG_SHOWN = "sim_usages_dialog_shown";
    public static final int WIFI_DATA = 1;
    public static final int MOBILE_DATA = 2;

    public static final String PROMO_WEBPROMOTIONAL = "WEBPROMOTION";
    public static final String PROMO_CASHBACK = "CashBack";

    public static final String WALLET_BALANCE = "walletBalance";
    public static final String WALLET_MONTHLY_CREDIT = "monthlyCredit";
    public static final String WALLET_MONTHLY_DEBIT = "monthlyDebit";
    public static final String WALLET_ON = "isWalletOn";

    public static final String LAST_KNOWN_LOCALITY = "LAST_KNOWN_LOCALITY";
    public static final String LAST_KNOWN_ADMIN_AREA = "LAST_KNOWN_ADMIN_AREA";
    public static final String LAST_KNOWN_COUNTRY = "LAST_KNOWN_COUNTRY";
    public static final String LAST_KNOWN_FEATURE_NAME = "LAST_KNOWN_FEATURE_NAME";

    public static final String LAST_INTERNET_DATA_SYNC_TIME_BULK = "LAST_INTERNET_DATA_SYNC_TIME_BULK";

    public static final int USSD_TYPE = 1;
    public static final int SERVICES_TYPE = 2;
    public static final int CALL_TYPE = 1;
    public static final int SMS_TYPE = 2;

    public static final String ONBOARDED_DROPDOWN_CLICKED = "onboarded_dropdown_clicked";

    public static final String CUSTOM_DEEPLINK = "customdeeplink";
    public static final String DEEPLINK_CALLING_USAGE = "http://smrtp.co/callingusage";
    public static final String DEEPLINK_DATA_USAGE = "http://smrtp.co/datausage";
    public static final String DEEPLINK_BROWSE_PLANS_SPL = "http://smrtp.co/bpspecial";
    public static final String DEEPLINK_BROWSE_PLANS_DATA = "http://smrtp.co/bpdata";
    public static final String DEEPLINK_REFERRAL = "http://smrtp.co/referral";


    public static final String SHARED_PREFERENCE_SHOW_NOTIFICATION = "SHARED_PREFERENCE_SHOW_NOTIFICATION";
    public static final String REFERRAL_IMAGE_NAME = "smartapp_ref_image.jpg";

    public static enum Action{
        CALL,SMS,DATA
    }
    public static enum ConnType {
        ALL,LOCAL,STD,ISD
    }
    public static enum NetworkType {
        ALL,SAME,OTHER,LANDLINE
    }
    public static enum Roaming {
        ALL,YES,NO
    }
    public static enum Service {
        ALL,OUTGOING,INCOMING
    }
    public static enum DayNight {
        ALL,DAY,NIGHT
    }
    public static enum DataType {
        GRANULATED,TOP_ENTITIES,RECENT_ENTITIES
    }
    public enum SHOW_CATEGORY_TYPE {
        THIS_WEEK_TYPE, LAST_WEEK_TYPE, THIS_MONTH_TYPE, TODAY_TYPE
    }
    public enum DATA_FOR {
        LIVE_APP,TEST_APP
    }

    public static String encyptionSalt;


    public static final String CARD_TOKEN = "card_token";
    public static final String MERCHANT_HASH = "merchant_hash";
//    public static final String MERCHANT_SSO = "_merchant_sso";
}
