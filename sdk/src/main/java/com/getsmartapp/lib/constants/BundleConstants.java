package com.getsmartapp.lib.constants;

/**
 * @author Shalakha.Gupta on 14-05-2015.
 */
public class BundleConstants {
    public static final String MOBILE = "mobile";
    public static final String LOGIN_TYPE = "login_type";
    public static final String SIGNIN = "signin";
    public static final String SIGNUP = "signup";
    public static final String FROM = "from";
    public static final String PHONE = "phone";
    public static final String SIGNUP_OBJ = "signup_obj";
    public static final String SIMNO = "sim_no";
    public static String BEAN = "bean";
    public static String AUTHTOKEN = "authtoken";
    public static String CODE = "code";
    public static String TICKETID = "ticketid";
    public static String PASSWORD = "password";
    public static String COMBOPLAN = "comboplan";
    public static String EMAIL = "email";
    public static String USER_SSO = "user_sso";

    public static String POS = "pos";
    public static String SIMTYPE = "simtype";
    public static String ORDER_ID = "OrderId";
    public static String ORDER_ITEM_LIST = "OrderItemList";
    public static String ORDER_STATUS = "orderStatus";
    public static String PROMO_DISCOUNT = "promoCodeDiscount";
    public static String PROMO_DISCOUNT_CODE = "pcCode";
    public static String NET_AMOUNT = "netAmountPaid";
    public static String MESSAGE = "message";
    public static String DATE = "date";
    public static String PROVIDER_ID = "providerid";
    public static String PROVIDER_NAME = "providername";
    public static String CIRCLE_ID = "circleid";
    public static String ISRECOMMENDED = "isrecommended";
    public static String TYPEID = "typeid";
    public static String SLASH_N_DASH = "slash_dash";
    public static String ROAMING = "roaming";
    public static String LANDING = "landing";
    public static String TOO_MUCH_DATA = "too_much_data";
    public static String CARD_NAME = "card_name";
    public static final String PREFERRED_NETWORK_TYPE = "network_type";
    public static final String BILL_CYCLE = "bill_cycle";
    public static final String DATA_LIMIT = "data_limit";
    public static String CIRCLE_NAME = "circlename";
    public static String CONTACT_NAME = "contactname";
    public static final String NUM_CHANGED = "num_changed";
}