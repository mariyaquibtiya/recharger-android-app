package com.getsmartapp.lib.constants;

/**
 * @author  bishwajeet.kumar on 21/10/15.
 */
public class ApiEndpoints {

    public static final String SSO_BASE_IP = "http://jssostg.indiatimes.com/sso/app";
    public static final String SOCIAL_BASE_IP = "http://testsocialappsintegrator.indiatimes.com/msocialsite";

    public static final String BASE_IP = "https://stg.getsmartapp.com";
    public static final String BASE_RECHARGER_ADDRESS = BASE_IP + "/recharger-api";
    public static final String BASE_RECOMMENDATION_ADDRESS = BASE_IP + "/recharger-api";
    public static final String BASE_REGISTRATION_ADDRESS = BASE_IP + "/recharger-api";

    public static final String GC_BASE_IP = BASE_IP;

    public static final String AGGREGATION_BASE_URL = BASE_IP+"/summarizer/summary";

}
