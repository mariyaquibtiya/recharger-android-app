package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.getsmartapp.lib.constants.BundleConstants;


/**
 * @author  Shalakha.Gupta on 25-05-2015.
 */
public class IncomingSms extends BroadcastReceiver {

//    private IncomingSmsListner mIncomingSmsListner;
    private final String ACTION_SMS_RECEIVED = "OTP_RECEIVED";
//    public void setIncomingSmsListeners(IncomingSmsListner resultListener) {
//        mIncomingSmsListner = resultListener;
//    }

    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();
//                    Log.i("SmsReceiver", "senderNum: " + phoneNumber + " message: " + message);
                    if ((phoneNumber.contains("SMARTP"))/* && (mIncomingSmsListner != null)*/) {
//                        mIncomingSmsListner.onSmsReceived(phoneNumber, message);
                        Intent myIntent = new Intent(ACTION_SMS_RECEIVED);
                        myIntent.putExtra(BundleConstants.MESSAGE, message);
                        context.sendBroadcast(myIntent);
                    }
                } // end for loop
            } // bundle is null
        } catch (Exception e) {
//            Log.e("SmsReceiver", "Exception smsReceiver" + e);
        }
    }
}
