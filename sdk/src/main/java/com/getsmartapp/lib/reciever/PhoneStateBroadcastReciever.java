package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.observer.SinchCallObserver;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.services.CallStoreTask;

import java.util.Iterator;
import java.util.Set;

/**
 * @author  nitesh on 23/5/15.
 */
public class PhoneStateBroadcastReciever extends BroadcastReceiver {

    private static final String TAG = "PhoneStateBroadCastReciever";
    private SharedPrefManager sharedPrefManager;

    //private SinchCallObserver mBroadcastObserver;
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        sharedPrefManager = new SharedPrefManager(context);
        if (bundle == null)
            return;

        SharedPreferences sp = context.getSharedPreferences("Recharger", Context.MODE_PRIVATE);
        String s = bundle.getString(TelephonyManager.EXTRA_STATE);
        //Log.e("state",s);
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            sp.edit().putString("number", number).commit();
            sp.edit().putString("state", s).commit();
        } else if (s.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            String number = bundle.getString("incoming_number");
            sp.edit().putString("number", number).commit();
            sp.edit().putString("state", s).commit();
            if (SinchCallObserver.getInstance().getSinchInitializedState())
                SinchCallObserver.getInstance().setNumber(number);
        } else if (s.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            sp.edit().putString("state", s).commit();
        } else if (s.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            String state = sp.getString("state", null);

            if (state != null && !state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                sp.edit().putString("state", null).commit();
                if (Build.VERSION.SDK_INT>20) {
                    if (!sharedPrefManager.getBooleanValue(Constants.SIM_SAVED)) {
                        if (sharedPrefManager.getIntValue(Constants.SIM1_serial)==0){
                            sharedPrefManager.setIntValue(Constants.SIM1_serial,Integer.parseInt(bundle.get(subscriptionStr(bundle))+""));
                        }
//                        if (sharedPrefManager.getIntValue(Constants.SLOTNO) == 1) {
//                            Log.e("set1_id", bundle.get(subscriptionStr(bundle)) + "");
//                            sharedPrefManager.setIntValue(Constants.SIM1_serial, Integer.parseInt(bundle.get(subscriptionStr(bundle)) + ""));
//                        } else {
//                            Log.e("set2_id", bundle.get(subscriptionStr(bundle)) + "");
//                            sharedPrefManager.setIntValue(Constants.SIM2_serial, Integer.parseInt(bundle.get(subscriptionStr(bundle)) + ""));
//                        }
//                        Log.e("sim_details_stored","--PhoneStatereciever");
                        sharedPrefManager.setBooleanValue(Constants.SIM_SAVED,true);
                        //sharedPrefManager.setBooleanValue(Constants.VERIFYING_MOBILE, false);
                    }
                }
                if(sharedPrefManager.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY)==1) {
                    CallStoreTask callStoreTask = new CallStoreTask(context);
                    callStoreTask.execute();
                }
//                Log.e("intent extr", intent.getExtras() + "");
//                Log.e("intent extr", intent.getDataString() + "");
                sp.edit().putString("state", s).commit();

            }
        }

        InternetDataUsageUtil.getInstance(context).notifyNotification(context);
    }

    private String subscriptionStr(Bundle bundle) {
        Set<String> keySet = bundle.keySet();
        Iterator iter = keySet.iterator();
        while (iter.hasNext()) {
            String str = (String) iter.next();
//            Log.e("str",str+"--"+bundle.get(str));
            if (str.contains("subscription")) {
                return str;
            }
            iter.remove();
        }
        return "subscription";
    }
}
