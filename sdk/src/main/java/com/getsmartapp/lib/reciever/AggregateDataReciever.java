package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.getsmartapp.lib.utils.SDKUtils;

/**
 * @author  Nitesh.Verma on 06-05-2015.
 */
public class AggregateDataReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
            SDKUtils.checkandSendDataToServer(context);
    }
}
