package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.services.SmsService;

/**
 * @author nitesh.verma on 9/17/15.
 */
public class BootCompleteReciever extends BroadcastReceiver{

    private DualSimManager simManager;
    private SharedPrefManager sharedPrefManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        simManager = new DualSimManager(context);
        sharedPrefManager = new SharedPrefManager(context);
        Intent i = new Intent(context, SmsService.class);
        context.startService(i);

        // =================== CHECK IF SIM CHANGED ==========================


//        if (simManager.isHasDualSim()) {
//            int sim1_serial = sharedPrefManager.getIntValue(Constants.SIM1_serial);
//            int sim2_serial = sharedPrefManager.getIntValue(Constants.SIM2_serial);
//            String s1 = simManager.getSerialNumber(0);
//            String s2 = simManager.getSerialNumber(1);
//            if (sim1_serial.equalsIgnoreCase(s1) && sim2_serial.equalsIgnoreCase(s2)) {
//                sharedPrefManager.setBooleanValue(Constants.SIM_CHANGED, true);
//                Log.e("sim_changed", "yes");
//            }
//        }else {
//            String sim1_serial = sharedPrefManager.getStringValue(Constants.SIM1_serial);
//            String s1 = simManager.getSerialNumber(0);
//            Log.e("sim_1_serial",s1);
//            if (!sim1_serial.equalsIgnoreCase(s1)){
//                sharedPrefManager.setBooleanValue(Constants.SIM_CHANGED, true);
//                Log.e("sim_changed", "yes");
//            }
//        }

    }
}
