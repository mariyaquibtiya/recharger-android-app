package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.managers.SharedPrefManager;

import java.util.Calendar;

/**
 * @author Nitesh.Verma on 06-05-2015.
 */
public class ConnectionChangedListener extends BroadcastReceiver {

    private static final String TAG = "ConnectionChnagedListener";
    private int connectionType = -1;

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPrefManager mSharedPrefManager = new SharedPrefManager(context.getApplicationContext());

        if(InternetDataUsageUtil.isNetworkConnected(context.getApplicationContext())) {

            if(InternetDataUsageUtil.isMobileNetworkConnected(context.getApplicationContext())) {
                connectionType = InternetDataUsageUtil.NETWORK_CONNECTION.MOBILE_NETWORK.ordinal();
                if(mSharedPrefManager.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE,-1)!=connectionType) {
                    mSharedPrefManager.setIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE, connectionType);
                    InternetDataUsageUtil.getInstance(context).updateMainAppsMobileDataTableForTotalData(context.getApplicationContext(), false);
                }
            } else {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                mSharedPrefManager.setStringValue(InternetDataUsageUtil.SHARED_PREFERENCE_LAST_WIFI_CONNECTION, wifiManager.getConnectionInfo().getSSID().replace("\"", ""));
                connectionType = InternetDataUsageUtil.NETWORK_CONNECTION.WIFI_NETWORK.ordinal();
                if(mSharedPrefManager.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE,-1)!=connectionType) {
                    mSharedPrefManager.setIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE, connectionType);
                    InternetDataUsageUtil.getInstance(context).updateMainAppsMobileDataTableForTotalData(context.getApplicationContext(), true);
                }

            }

        } else {
            Log.e("PeeyushKS","Network disconnected: "+mSharedPrefManager.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE));
            if(mSharedPrefManager.getIntValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_TYPE, -1)== InternetDataUsageUtil.NETWORK_CONNECTION.WIFI_NETWORK.ordinal()) {
                mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_LAST_WIFI_CONNECTED_TIME, Calendar.getInstance().getTimeInMillis());
            }
        }
    }
}
