package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.services.BatchCallSmsSave;
import com.getsmartapp.lib.utils.DateUtil;

public class CallBatchSaveReciever extends BroadcastReceiver {

    private SharedPrefManager sharedPrefManager ;


    public CallBatchSaveReciever() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPrefManager = new SharedPrefManager(context);

       // Log.e("call_batch","yes");

        long call_store = sharedPrefManager.getLongValue(Constants.LAST_CALL_SAVED_TIME);
        //long sms_store = sharedPrefManager.getLongValue(Constants.LAST_SMS_SAVE_TIME);
        if (DateUtil.twelweHourBefore(call_store)){
            Intent i = new Intent(context,BatchCallSmsSave.class);
            context.startService(i);
        }
    }
}
