package com.getsmartapp.lib.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.getsmartapp.lib.database.SdkDbHelper;
import com.getsmartapp.lib.dualSim.DualSimManager;
import com.getsmartapp.lib.managers.SharedPrefManager;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.services.DeleteOldDataTask;
import com.getsmartapp.lib.utils.SDKUtils;

/**
 * @author  nitesh.verma on 3/4/16.
 */
public class ClearDataReciever extends BroadcastReceiver {

    private DualSimManager simManager;
    private SharedPrefManager shredpref;
    private SQLiteDatabase sqLiteDatabase;
    private int count = 0;
    private int callcount1 = 0, callcount2 = 0,smscount1 = 0,smscount2 = 0;


    @Override
    public void onReceive(Context context, Intent intent) {
        simManager = new DualSimManager(context);
        shredpref = new SharedPrefManager(context);
        sqLiteDatabase = SdkDbHelper.getInstance(context).getWritableDatabase();
        checkAndStartClearTask(context,0);
        checkAndStartClearTask(context,1);
    }

    private void checkAndStartClearTask(Context context,int is_roaming) {
        if (simManager.isHasDualSim()) {
            // Need to delete Call and Sms data for both sim seperatly

            if (shredpref.getIntValue(Constants.USER_ON_BOARDED_SUCCESSFULLY) == 1) {
                if (shredpref.getIntValue(Constants.NO_OF_SIM_ONBOARDED) == 1) {
                    int sim_id = shredpref.getIntValue(Constants.SIM1_serial);
                    callcount1 = SDKUtils.getDateCount(sqLiteDatabase, Constants.CALL_TABLE, sim_id, is_roaming);
                    callcount2 = SDKUtils.getDateCount(sqLiteDatabase, Constants.CALL_TABLE, -1,is_roaming);
                    smscount1 = SDKUtils.getDateCount(sqLiteDatabase,Constants.SMS_TABLE,sim_id,is_roaming);
                    smscount2 = SDKUtils.getDateCount(sqLiteDatabase,Constants.SMS_TABLE,-1,is_roaming);
                } else {
                    int sim1_id = shredpref.getIntValue(Constants.SIM1_serial);
                    int sim2_id = shredpref.getIntValue(Constants.SIM2_serial);
                    callcount1 = SDKUtils.getDateCount(sqLiteDatabase, Constants.CALL_TABLE, sim1_id,is_roaming);
                    callcount2 = SDKUtils.getDateCount(sqLiteDatabase, Constants.CALL_TABLE, sim2_id,is_roaming);
                    smscount1 = SDKUtils.getDateCount(sqLiteDatabase,Constants.SMS_TABLE,sim1_id,is_roaming);
                    smscount2 = SDKUtils.getDateCount(sqLiteDatabase,Constants.SMS_TABLE,sim2_id,is_roaming);
                }
            }
        } else {
            callcount1 = SDKUtils.getDateCount(sqLiteDatabase, Constants.CALL_TABLE, 0,is_roaming);
            smscount1 = SDKUtils.getDateCount(sqLiteDatabase,Constants.SMS_TABLE,0,is_roaming);
        }
        // delete internet and app data table
        int netcount = SDKUtils.getDateCount(sqLiteDatabase, Constants.INTERNET_TABLE, 0,is_roaming);
        int appcount = SDKUtils.getDateCount(sqLiteDatabase, Constants.APP_TABLE, 0,is_roaming);
        int allowed_no_of_rows = is_roaming==1?Constants.ROAMING_DATA_LIMIT:Constants.NON_ROAMING_DATA_LIMIT;
        if (callcount1 > allowed_no_of_rows || callcount2 > allowed_no_of_rows || smscount1>allowed_no_of_rows ||smscount2>allowed_no_of_rows|| netcount>allowed_no_of_rows || appcount>allowed_no_of_rows) {
            // Start task to delete
            DeleteOldDataTask deleteOldDataTask = new DeleteOldDataTask(context);
            deleteOldDataTask.execute();
        }
    }
}
