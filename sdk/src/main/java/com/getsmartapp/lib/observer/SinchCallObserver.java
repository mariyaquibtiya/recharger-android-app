package com.getsmartapp.lib.observer;

import android.text.TextUtils;

/**
 * @author  nitesh.verma on 2/11/16.
 */
public class SinchCallObserver {

    private static SinchCallObserver mSinchCallObserver;

    public static SinchCallObserver getInstance()
    {
        if(mSinchCallObserver ==null)
            mSinchCallObserver = new SinchCallObserver();
        return mSinchCallObserver;
    }

    private String mNumber;
    private boolean state = false;
//    private void triggerObservers() {
//        setChanged();
//        notifyObservers();
//    }

    public void setNumber(String number) {
        if(TextUtils.isEmpty(mNumber))
            mNumber = number;
       // triggerObservers();
    }

    public String getmNumber() {
        return mNumber;
    }

    public void setSinchInitializedState(boolean state)
    {
        this.state = state;
    }

    public boolean getSinchInitializedState()
    {
        return state;
    }
}
