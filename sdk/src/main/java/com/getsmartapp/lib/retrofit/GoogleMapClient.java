package com.getsmartapp.lib.retrofit;

import com.getsmartapp.lib.interfaces.ApiServices;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * @author  nitesh.verma on 1/18/16.
 */


public class GoogleMapClient {
    private ApiServices apiService;

    public GoogleMapClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
//        client.setConnectTimeout(60, TimeUnit.SECONDS);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ApiConstants.GOOGLEMAP_BASE_ADDRESS)
                .setErrorHandler(new RestErrorHandler())
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(client))
                .setRequestInterceptor(new HeaderRequestInterceptor())
                .build();

        apiService = restAdapter.create(ApiServices.class);
    }

    public ApiServices getApiService() {
        return apiService;
    }
}
