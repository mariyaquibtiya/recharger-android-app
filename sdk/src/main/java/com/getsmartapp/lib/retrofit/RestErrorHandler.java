package com.getsmartapp.lib.retrofit;


import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author  jayant on 5/11/15.
 */
public class RestErrorHandler implements ErrorHandler {

    @Override
    public Throwable handleError(RetrofitError cause) {
        Response response = cause.getResponse();
        return cause;
    }
}
