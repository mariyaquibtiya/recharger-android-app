package com.getsmartapp.lib.retrofit;

import android.text.TextUtils;

import com.getsmartapp.lib.constants.ApiConstants;

import retrofit.RequestInterceptor;

/**
 * @author  jayant on 5/11/15.
 */
public class HeaderRequestInterceptor implements RequestInterceptor {
    String authStr;
    public HeaderRequestInterceptor(String authStr) {
        this.authStr = authStr;
    }
    public HeaderRequestInterceptor() {
    }
    @Override
    public void intercept(RequestFacade request) {
        if(!TextUtils.isEmpty(authStr)) {
            request.addHeader("transactionHash",authStr);
            request.addHeader("Content-type","application/json");
        }
        else {
            request.addHeader("User-Agent", "Recharge-App");
            request.addHeader("version", ApiConstants.backend_version);
        }
    }
}