package com.getsmartapp.lib.retrofit;

import com.getsmartapp.lib.interfaces.ApiServices;
import com.getsmartapp.lib.constants.ApiConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * @author  nitesh.verma on 9/2/15.
 */
public class RestClientGPlus {
    private ApiServices apiService;

    public RestClientGPlus(){

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();
        OkHttpClient client = new OkHttpClient();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setEndpoint(ApiConstants.GP_PROFILE_BASE_URL)
                .setErrorHandler(new RestErrorHandler())
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(client))
                .setRequestInterceptor(new HeaderRequestInterceptor())
                .build();

        apiService = restAdapter.create(ApiServices.class);


    }
    public ApiServices getApiService() {
        return apiService;
    }
}
