package com.getsmartapp.lib.retrofit;

import com.getsmartapp.lib.interfaces.ApiServices;
import com.getsmartapp.lib.constants.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * @author  jayant on 5/11/15.
 */
public class RestClient {
    private ApiServices apiService;

    public RestClient(final String base_address, final String auth_str) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
        client.setConnectTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
        client.setWriteTimeout(Constants.DEFAULT_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(base_address)
                .setErrorHandler(new RestErrorHandler())
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(client))
                .setRequestInterceptor(new HeaderRequestInterceptor(auth_str))
                .build();

        apiService = restAdapter.create(ApiServices.class);
    }

    public ApiServices getApiService() {
        return apiService;
    }
}
