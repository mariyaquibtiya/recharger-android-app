package com.getsmartapp.lib.retrofit;

import com.getsmartapp.lib.interfaces.ApiServices;
import com.getsmartapp.lib.constants.ApiConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * @author  shalakha.gupta on 03/09/15.
 */
public class RestClientBaseIp {
    private ApiServices apiService;

    public RestClientBaseIp() {
        Gson gson = new GsonBuilder()
                /*.registerTypeAdapterFactory(new ItemTypeAdapterFactory())*/
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();
        OkHttpClient client = new OkHttpClient();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ApiConstants.BASE_IP)
                .setErrorHandler(new RestErrorHandler())
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(client))
                .setRequestInterceptor(new HeaderRequestInterceptor())
                .build();

        apiService = restAdapter.create(ApiServices.class);
    }

    public ApiServices getApiService() {
        return apiService;
    }
}
