package com.getsmartapp.lib.managers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.getsmartapp.lib.data.CallCalculation;
import com.getsmartapp.lib.database.SdkAppDatabaseHelper;
import com.getsmartapp.lib.realmObjects.CallObject;
import com.getsmartapp.lib.realmObjects.CardDataRealmObject;
import com.getsmartapp.lib.realmObjects.SMSObject;
import com.getsmartapp.lib.realmObjects.ServerSyncInfoObject;
import com.getsmartapp.lib.constants.Constants;
import com.getsmartapp.lib.constants.DataStorageConstants;
import com.getsmartapp.lib.utils.DateUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * @author Peeyush.Singh on 22-05-2016.
 */
public class RealmDBManager {

    private static RealmDBManager mRealmDBManager;
    private Context mContext;
    private Realm realm;

    public static RealmDBManager getInstance(Context context) {
//        if(mCallRealmManager == null)
        mRealmDBManager = new RealmDBManager(context);
        return mRealmDBManager;
    }


    RealmDBManager(Context context) {
        mContext = context;
        realm = Realm.getInstance(
                new RealmConfiguration.Builder(context.getApplicationContext())
                        .name("call_test.realm").deleteRealmIfMigrationNeeded()/*.schemaVersion(1)*/
                        .build());

    }

    public void updateCallSMSRecord(Context context) {
        updateCallRealm(context);
        updateSMSRealm(context);
    }


    public Realm getRealm() {
        return realm;
    }

    public long getEndTimeOfToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    public long getStartTimeOfToday() {
        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        calendar.setTimeInMillis(calendar.getTimeInMillis()-24*60*60*1000L);
//        calendar.setTimeInMillis(calendar.getTimeInMillis()+(24*60*60*1000));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public long getEndTimeOfToday(int numOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(calendar.getTimeInMillis() - ((numOfDays - 1) * 24 * 60 * 60 * 1000L));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    public long getStartTimeOfDay(long timeInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public long getEndTimeOfDay(long timeInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    public long getStartTimeOfToday(int numOfDays) {
        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
        calendar.setTimeInMillis(calendar.getTimeInMillis() - ((numOfDays - 1) * 24 * 60 * 60 * 1000L));
//        calendar.setTimeInMillis(calendar.getTimeInMillis()+(24*60*60*1000));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }


    private void updateSMSRealm(Context context) {
        Uri contentURI = Uri.parse("content://sms/");
        Cursor managedCursor = context.getContentResolver().query(contentURI,
                null,
                Telephony.Sms.DATE + " BETWEEN ? AND ? ",
                new String[]{String.valueOf(getStartTimeOfToday()), String.valueOf(getEndTimeOfToday())}, null);

        int index_Body = managedCursor.getColumnIndex(Telephony.Sms.BODY);
        int index_Date = managedCursor.getColumnIndex(Telephony.Sms.DATE);
        int index_Address = managedCursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int index_Type = managedCursor.getColumnIndex(Telephony.Sms.TYPE);
        mSharedPrefManager = new SharedPrefManager(context);


        String simType = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
        String provider = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
        String circle = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE);


        Log.e("PeeyushKS", "SMS Cursor Size: " + managedCursor.getCount());

        while (managedCursor.moveToNext()) {
            int sms_type = managedCursor.getInt(index_Type);

            if ((sms_type == Telephony.TextBasedSmsColumns.MESSAGE_TYPE_INBOX) || (sms_type == Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT)) {
                String sms_body = managedCursor.getString(index_Body);
                long sms_time = managedCursor.getLong(index_Date);
                String sms_address = managedCursor.getString(index_Address);
                RealmResults<SMSObject> smsObjects = realm.where(SMSObject.class).equalTo("sms_time", Long.valueOf(sms_time)).findAll();
                Log.e("PeeyushKS", "SMS REALM DB Size: " + smsObjects.size());

                if (smsObjects.size() == 0) {
                    String[] sms = sms_address.split("[-\\\\s]");
                    SMSObject smsObject = new SMSObject();
                    smsObject.setOperatorSMS(false);
                    for (int i = 0; i < sms.length; i++) {
                        for (int j = 0; j < Constants.OPERATOR_SMS_ADDRESSES.length; j++) {
                            if (sms[i].toLowerCase().contains(Constants.OPERATOR_SMS_ADDRESSES[j].toLowerCase())) {
//                            storeOperatorSms(mDatabase, number, smsCursor);
                                smsObject.setOperatorSMS(true);
                            }
                        }
                        for (int k = 0; k < Constants.VENDOR_SMS_ADDRESSES.length; k++) {
                            if (sms[i].toLowerCase().contains(Constants.VENDOR_SMS_ADDRESSES[k].toLowerCase())) {
                                String body = sms_body;//smsCursor.getString(smsCursor.getColumnIndex(Telephony.Sms.BODY));
                                if (body.toLowerCase().matches(".*\\\\recharge\\\\b.*")) {
//                                storeOperatorSms(mDatabase, number, smsCursor);
                                    smsObject.setOperatorSMS(true);
                                }
                            }
                        }
                    }

                    Log.e("PeeyushKS", new Date(sms_time) + " -- " + sms_address);

                    smsObject.setSms_time(sms_time);
                    smsObject.setSms_body(sms_body);
                    if (sms_type == Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT)
                        smsObject.setSms_type(SMSObject.Constants.SMS_TYPE_SENT);
                    else
                        smsObject.setSms_type(SMSObject.Constants.SMS_TYPE_RECEIVED);

                    CallCalculation callCalculation = new CallCalculation(context, simType, provider, circle);

                    String call_type = callCalculation.getSmsType(sms_type);
                    int isNight = callCalculation.getIsNightValue(sms_time);
                    String callnetwork = callCalculation.getCallNetwork(sms_address);
                    String number_type = callCalculation.typeofCallandSms(sms_address);
                    String country_name = "India";
                    if (callCalculation.isIndianPhone(sms_address)) {
                        country_name = "India";
                    } else country_name = callCalculation.numberCountry(sms_address);


                    if (sms_address.startsWith("+91")) {
                        sms_address = sms_address.substring(3);
                    } else if (sms_address.startsWith("0")) {
                        sms_address = sms_address.substring(1);
                    }
                    smsObject.setSms_address(sms_address);

                    smsObject.setHash(number_type + "," + country_name + "," + callnetwork + "," + isNight + "," + call_type);

                    realm.beginTransaction();
                    realm.copyToRealm(smsObject);
                    realm.commitTransaction();
                }
            }
        }
    }

    public void updateCallRealm(Context context) {

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Cursor managedCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null,
                CallLog.Calls.DATE + " BETWEEN ? AND ? ",
                new String[]{String.valueOf(getStartTimeOfToday()), String.valueOf(getEndTimeOfToday())}, null);
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);


        mSharedPrefManager = new SharedPrefManager(context);
        String simType = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_SIM_TYPE);
        String provider = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_OPERATOR_NAME);
        String circle = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE);

        while (managedCursor.moveToNext()) {

            String phName = managedCursor.getString(name);
            String phNumber = managedCursor.getString(number);

            int callType = managedCursor.getInt(type);
            long callDate = managedCursor.getLong(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            int callSeconds = managedCursor.getInt(duration);
            if(callSeconds > 0) {

                RealmResults<CallObject> callObjects = realm.where(CallObject.class).equalTo("call_time", Long.valueOf(callDate)).findAll();

                if (callObjects.size() == 0) {

                    CallCalculation callCalculation = new CallCalculation(mContext, simType, provider, circle);
                    CallObject callObject = new CallObject();

                    String phone = phNumber.replaceAll("\\s", "");
                    callCalculation.setMobile(phone);
                    callCalculation.fillLandlineList();
                    callCalculation.setNightStartEndTime();
                    callCalculation.fillCountryList();

                    String call_type = callCalculation.getCallType(callType);
                    int isNight = callCalculation.getIsNightValue(callDate);
                    String callnetwork = callCalculation.getCallNetwork(phone);
                    String number_type = callCalculation.typeofCallandSms(phone);

                    callObject.setContact_number(phNumber);
                    callObject.setContact_name(phName);
                    callObject.setCall_duration_in_sec(callSeconds);
                    callObject.setCall_duration_in_min((int) Math.ceil(callSeconds / 60.0f));
                    callObject.setCall_time(Long.valueOf(callDate));
                    callObject.setCall_type(call_type);
                    callObject.setIs_night(isNight);
                    callObject.setNumber_type(number_type);
                    callObject.setNetwork_type(callnetwork);

                    if (telephonyManager.isNetworkRoaming())
                        callObject.setIs_roaming(1);
                    else
                        callObject.setIs_roaming(0);

                    String country_name= "";
                    if (callCalculation.isIndianPhone(phone)) {
                        country_name = "India";
                    } else country_name = callCalculation.numberCountry(phone);

                    callObject.setHash(number_type+","+country_name+","+callnetwork+","+isNight+","+call_type);

                    realm.beginTransaction();
                    realm.copyToRealm(callObject);
                    realm.commitTransaction();
                    Log.e("PeeyushKS","Sending Broadcast to update Notification and Home Screen");
                    Intent intent = new Intent("update_calls");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }
            }
        }

        managedCursor.close();
    }



    public int getTotalSTDOutgoingMins(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type" , DataStorageConstants.STD_NUMBER).sum("call_duration_in_min").intValue();
    }

    public int getTotalSTDIncomingMins(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).sum("call_duration_in_min").intValue();

    }

    public long getTotalSTDOutgoingCount(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).greaterThan("call_duration_in_min", 0).count();
    }

    public int getTotalOutgoingTodayInMins()
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday()).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).sum("call_duration_in_min").intValue();
    }

    public int getTotalIncomingTodayinMins()
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday()).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).sum("call_duration_in_min").intValue();
    }

    public int getTotalOutgoingMins(String phoneNumber, int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE/*DataStorageConstants.OUTGOING_CALL_TYPE*/).equalTo("contact_number", phoneNumber).sum("call_duration_in_min").intValue();
    }

    public RealmResults<CallObject> getLastCalls(String phoneNumber, int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("contact_number", phoneNumber).greaterThan("call_duration_in_min",0).findAllSorted("call_time", Sort.DESCENDING);
    }

    public int getTotalOutgoingMins(int numOfDays)
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).sum("call_duration_in_min").intValue();
        }catch (Exception e){
            return 0;
        }
    }

    public int getTotalOutgoingMinsByRoamingValue(int numOfDays, int roaming_state)
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("is_roaming", roaming_state).sum("call_duration_in_min").intValue();
        }catch (Exception e){
            return 0;
        }
    }

    public RealmResults<CallObject> getTotalOutgoingCalls(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).greaterThan("call_duration_in_min", 0).findAllSorted("call_time", Sort.DESCENDING);
    }

    public long getTotalOutgoingCount(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).greaterThan("call_duration_in_min", 0).count();
    }

    public long getTotalOutgoingCountInRoaming(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("is_roaming", 1).greaterThan("call_duration_in_min", 0).count();
    }

    public int getTotalOutgoingCallDurationInRoaming(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("is_roaming", 1).greaterThan("call_duration_in_min", 0).sum("call_duration_in_min").intValue();
    }

    public long getTotalOutgoingSTDCount(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).greaterThan("call_duration_in_min", 0).count();
    }

    public long getTotalIncomingCount(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).greaterThan("call_duration_in_min", 0).count();
    }

    public long getTotalIncomingCountInRoaming(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("is_roaming", 1).greaterThan("call_duration_in_min", 0).count();
    }

    public int getTotalIncomingCallDurationInRoaming(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("is_roaming", 1).greaterThan("call_duration_in_min", 0).sum("call_duration_in_min").intValue();
    }

    public long getTotalIncomingSTDCount(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).greaterThan("call_duration_in_min", 0).count();
    }

    public int getTotalIncomingMins(String phoneNumber, int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("contact_number", phoneNumber).sum("call_duration_in_min").intValue();
    }

    public int getTotalIncomingMins(int numOfDays)
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).sum("call_duration_in_min").intValue();
        }catch (Exception e){
            return 0;
        }
    }

    public int getTotalIncomingMinsByRoamingValue(int numOfDays, int roaming_state)
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("is_roaming", roaming_state).sum("call_duration_in_min").intValue();
        }catch (Exception e){
            return 0;
        }
    }

    public double getTotalOutgoingAverageMins(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).average("call_duration_in_min");
    }

    public double getTotalOutgoingAverageSeconds(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).average("call_duration_in_sec");
    }



    public int getMaxOutgoingMins(int numOfDays)
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).max("call_duration_in_min").intValue();
        }catch (Exception e){return 0;}
    }

    public int getMaxIncomingMins(int numOfDays)
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).max("call_duration_in_min").intValue();
        }catch (Exception e){return 0;}
    }

    public List<Map.Entry<CallObject, Integer>> getTopContacts(int numOfDays)
    {
        HashMap<CallObject, Integer> hashMap = new HashMap<>();
        RealmResults<CallObject> callList = getCallList(numOfDays);

        for(int i=0; i<callList.size();i++) {
            CallObject callObject = callList.get(i);
            hashMap.put(callObject,getTotalCallsByNumber(numOfDays, callObject.getContact_number()));
//            Log.e("PeeyushKS", i + " " + callObject.getContact_name() + " (" + callObject.getContact_number()+") - "+getTotalOutgoingCallByNumber(numOfDays, callObject.getContact_number()));
        }

        Set<Map.Entry<CallObject, Integer>> set = hashMap.entrySet();
        List<Map.Entry<CallObject, Integer>> list = new ArrayList<Map.Entry<CallObject, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<CallObject, Integer>>()
        {
            public int compare( Map.Entry<CallObject, Integer> o1, Map.Entry<CallObject, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
    }

    public List<Map.Entry<String, Integer>> getTopOutgoingCalls(int numOfDays)
    {
        HashMap<String, Integer> hashMap = new HashMap<>();
        RealmResults<CallObject> outgoingCallList = getOutgoingCallList(numOfDays);

        for(int i=0; i<outgoingCallList.size();i++) {
            CallObject callObject = outgoingCallList.get(i);
            hashMap.put((!TextUtils.isEmpty(callObject.getContact_name())? callObject.getContact_name(): callObject.getContact_number()),getTotalOutgoingCallByNumber(numOfDays, callObject.getContact_number()));
//            Log.e("PeeyushKS", i + " " + callObject.getContact_name() + " (" + callObject.getContact_number()+") - "+getTotalOutgoingCallByNumber(numOfDays, callObject.getContact_number()));
        }

        Set<Map.Entry<String, Integer>> set = hashMap.entrySet();
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
    }

    public List<Map.Entry<String, Integer>> getTopSTDOutgoingCalls(int numOfDays)
    {
        HashMap<String, Integer> hashMap = new HashMap<>();
        RealmResults<CallObject> outgoingCallList = getSTDOutgoingCallList(numOfDays);

        for(int i=0; i<outgoingCallList.size();i++) {
            CallObject callObject = outgoingCallList.get(i);
            hashMap.put((!TextUtils.isEmpty(callObject.getContact_name())? callObject.getContact_name(): callObject.getContact_number()),getTotalOutgoingCallByNumber(numOfDays, callObject.getContact_number()));
        }

        Set<Map.Entry<String, Integer>> set = hashMap.entrySet();
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
    }
    public List<Map.Entry<CallObject, Integer>> getTopSTDOutgoingCallsByObject(int numOfDays)
    {
        HashMap<CallObject, Integer> hashMap = new HashMap<>();
        RealmResults<CallObject> outgoingCallList = getSTDOutgoingCallList(numOfDays);

        for(int i=0; i<outgoingCallList.size();i++) {
            CallObject callObject = outgoingCallList.get(i);
            hashMap.put(callObject, getTotalOutgoingCallByNumber(numOfDays, callObject.getContact_number()));
        }

        Set<Map.Entry<CallObject, Integer>> set = hashMap.entrySet();
        List<Map.Entry<CallObject, Integer>> list = new ArrayList<Map.Entry<CallObject, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<CallObject, Integer>>()
        {
            public int compare( Map.Entry<CallObject, Integer> o1, Map.Entry<CallObject, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
    }

 /*   public List<Map.Entry<String, Integer>> getTopSTDOutgoingCallsTimeStamp(int numOfDays)
    {
        HashMap<String, Integer> hashMap = new HashMap<>();
        RealmResults<CallObject> outgoingCallList = getSTDOutgoingCallList(numOfDays);

        for(int i=0; i<outgoingCallList.size();i++) {
            CallObject callObject = outgoingCallList.get(i);
            hashMap.put((!TextUtils.isEmpty(callObject.getContact_name())? callObject.getContact_name(): callObject.getContact_number()),getTotalOutgoingCallByNumber(numOfDays, callObject.getContact_number()));
        }

        Set<Map.Entry<String, Integer>> set = hashMap.entrySet();
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
    }*/


    public long getLastOutogingCallTimeByNumber(String number)
    {
        return  realm.where(CallObject.class).equalTo("contact_name", number).or().equalTo("contact_number",number).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).greaterThan("call_duration_in_min",0).findAllSorted("call_time", Sort.DESCENDING).first().getCall_time();
    }

    public long getLastSTDCallTimeByNumber(String number)
    {
        try {
            return realm.where(CallObject.class).equalTo("contact_name", number).or().equalTo("contact_number", number).equalTo("number_type", DataStorageConstants.STD_NUMBER).greaterThan("call_duration_in_min", 0).findAllSorted("call_time", Sort.DESCENDING).first().getCall_time();
        }catch (Exception e)
        {
            return 0;
        }
    }

    public RealmResults<CallObject> getCallList(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).distinct("contact_number");//.findAll();
    }

    public RealmResults<CallObject> getCallList(int numOfDays, int is_roaming)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("is_roaming", is_roaming).distinct("contact_number");//.findAll();
    }

    public RealmResults<CallObject> getCallListByHash(long dayTime, int roaming_state)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfDay(dayTime)).lessThan("call_time", getEndTimeOfDay(dayTime)).equalTo("is_roaming", roaming_state).distinct("hash");//.findAll();
    }

    public long getLastRoamingLocationTime()
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfDay(30)).lessThan("call_time", getEndTimeOfToday()).equalTo("is_roaming", 1).findAll().sort("call_time", Sort.DESCENDING).first().getCall_time();//.findAll();
        }catch (Exception  e){

            return 0L;
        }
    }

    public RealmResults<CallObject> getLastRoamingCallsUniqueLocality()
    {
        return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfDay(30)).lessThan("call_time", getEndTimeOfToday()).equalTo("is_roaming", 1).findAll().sort("call_time", Sort.DESCENDING).distinct("call_location_locality");
    }

    public int callsMinsOnRoamingToday()
    {
        try {
            return realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday()).lessThan("call_time", getEndTimeOfToday()).equalTo("is_roaming", 1).findAll().sum("call_duration_in_min").intValue();
        }catch (Exception  e){

            return 0;
        }
    }

    public RealmResults<CallObject> getCallListSortedbyTime(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).findAllSorted("call_time", Sort.DESCENDING);//.findAll();

    }
    public RealmResults<CallObject> getCallList(long startTimeInMillis)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfDay(startTimeInMillis)).lessThan("call_time", getEndTimeOfToday()).distinct("contact_number");//.findAll();
    }

    public RealmResults<CallObject> getOutgoingCallList(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).distinct("contact_number");//.findAll();
    }

    public RealmResults<CallObject> getTotalSTDCallList(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).greaterThan("call_duration_in_min",0).findAllSorted("call_time", Sort.DESCENDING);//.findAll();
    }


    public RealmResults<CallObject> getSTDOutgoingCallList(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).distinct("contact_number");//.findAll();
    }

    public RealmResults<CallObject> getLastCall(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).greaterThan("call_duration_in_min", 0).findAllSorted("call_time", Sort.DESCENDING);

    }

    public RealmResults<CallObject> getLastSTDCalls(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).equalTo("number_type", DataStorageConstants.STD_NUMBER).lessThan("call_time", getEndTimeOfToday()).greaterThan("call_duration_in_min", 0).findAllSorted("call_time", Sort.DESCENDING);

    }

    public RealmResults<CallObject> getLastSTDOutgoingCallList(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).greaterThan("call_duration_in_min", 0).findAllSorted("call_time", Sort.DESCENDING);//.findAll();
    }


    public RealmResults<CallObject> getTotalSTDOutgoingCallList(int numOfDays)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).distinct("contact_number");//.findAll();
    }

    public int getTotalOutgoingCallByNumber(int numOfDays, String phNumber)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("contact_number", phNumber).sum("call_duration_in_min").intValue();
    }
    public int getTotalCallsByNumberDays(int numOfDays, String phNumber)
    {
        try {
            return (int)realm.where(CallObject.class).greaterThan("call_time", getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).greaterThan("call_duration_in_min", 0).equalTo("contact_number", phNumber).count();
        }catch (Exception e)
        {
            return 0;
        }
    }

    public int getTotalIncomingCallByNumber(int numOfDays, String phNumber)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).equalTo("contact_number", phNumber).sum("call_duration_in_min").intValue();
    }

    public int getTotalCallDurationByNumber(int numOfDays, String phNumber)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("contact_number", phNumber).sum("call_duration_in_min").intValue();
    }

    public RealmResults<CallObject> getAllCallsByNumber(String phNumber)
    {
        return realm.where(CallObject.class).equalTo("contact_number", phNumber).findAllSorted("call_time", Sort.DESCENDING);
    }

    public long getTotalCallByHashandDay(String hash, long callTime)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfDay(callTime)).lessThan("call_time", getEndTimeOfDay(callTime)).equalTo("hash", hash).count();
    }



    public int getTotalCallMinsByHashAndDay(String hash, long callTime)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfDay(callTime)).lessThan("call_time", getEndTimeOfDay(callTime)).equalTo("hash", hash).sum("call_duration_in_min").intValue();
    }

    public int getTotalCallSecsByHashAndDay(String hash, long callTime)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfDay(callTime)).lessThan("call_time", getEndTimeOfDay(callTime)).equalTo("hash", hash).sum("call_duration_in_sec").intValue();
    }

    public int getTotalCallsByNumber(int numOfDays, String phNumber)
    {
        return realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("contact_number", phNumber).sum("call_duration_in_min").intValue();
    }

    public HashMap<String, Integer> getIncomingHashMap(int numOfDays)
    {
        LinkedHashMap<String, Integer> hashMap = new LinkedHashMap<>();
        RealmResults<CallObject> realmResults = realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.INCOMING_CALL_TYPE).findAll();

        if(numOfDays == 7)
        {
            for(CallObject callObject: realmResults) {
                String last_dayNo = DateUtil.getWeekDayFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(last_dayNo)) {
                    hashMap.put(last_dayNo, hashMap.get(last_dayNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(last_dayNo, callObject.getCall_duration_in_min());
                }
            }
        }else if(numOfDays == 30)
        {
            for(CallObject callObject: realmResults) {
                String current_weekNo = DateUtil.getWeekFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(current_weekNo)) {
                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_weekNo, callObject.getCall_duration_in_min());
                }
            }
        }else if(numOfDays == 1)
        {
            for(CallObject callObject: realmResults) {
//                String current_HoursNo = DateUtil.getHoursFromTimestamp(timestamp);
                String current_HoursNo = DateUtil.getHoursFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(current_HoursNo)) {
                    hashMap.put(current_HoursNo, hashMap.get(current_HoursNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_HoursNo, /*call_in_min*/callObject.getCall_duration_in_min());
                }
            }
        }else // Postpaid Case
        {
            for(CallObject callObject: realmResults) {

                String current_weekNo = DateUtil.getWeekFromTimestampPostPaid(mContext, callObject.getCall_time());
                if (hashMap.containsKey(current_weekNo)) {
                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_weekNo, callObject.getCall_duration_in_min());
                }
            }
        }

        return hashMap;
    }

    public HashMap<String, Integer> getOutgoingHashMap(int numOfDays)
    {
        LinkedHashMap<String, Integer> hashMap = new LinkedHashMap<>();
        RealmResults<CallObject> realmResults = realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).findAll();

        if(numOfDays == 7)
        {
            for(CallObject callObject: realmResults) {
                String last_dayNo = DateUtil.getWeekDayFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(last_dayNo)) {
                    hashMap.put(last_dayNo, hashMap.get(last_dayNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(last_dayNo, callObject.getCall_duration_in_min());
                }
            }
        }else if(numOfDays == 30)
        {
            for(CallObject callObject: realmResults) {
                String current_weekNo = DateUtil.getWeekFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(current_weekNo)) {
                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_weekNo, callObject.getCall_duration_in_min());
                }
            }
        }else if(numOfDays == 1)
        {
            for(CallObject callObject: realmResults) {
//                String current_HoursNo = DateUtil.getHoursFromTimestamp(timestamp);
                String current_HoursNo = DateUtil.getHoursFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(current_HoursNo)) {
                    hashMap.put(current_HoursNo, hashMap.get(current_HoursNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_HoursNo, /*call_in_min*/callObject.getCall_duration_in_min());
                }
            }
        }else // Postpaid Case
        {
            for(CallObject callObject: realmResults)
            {
                String current_weekNo = DateUtil.getWeekFromTimestampPostPaid(mContext, callObject.getCall_time());
                if (hashMap.containsKey(current_weekNo)) {
                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_weekNo, callObject.getCall_duration_in_min());
                }
            }

        }

        return hashMap;
    }


    public HashMap<String, Integer> getSTDOutgoingHashMap(int numOfDays)
    {
        LinkedHashMap<String, Integer> hashMap = new LinkedHashMap<>();
        RealmResults<CallObject> realmResults = realm.where(CallObject.class).greaterThan("call_time",getStartTimeOfToday(numOfDays)).lessThan("call_time", getEndTimeOfToday()).equalTo("call_type", DataStorageConstants.OUTGOING_CALL_TYPE).equalTo("number_type", DataStorageConstants.STD_NUMBER).findAll();

        if(numOfDays == 7)
        {
            for(CallObject callObject: realmResults) {
                String last_dayNo = DateUtil.getWeekDayFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(last_dayNo)) {
                    hashMap.put(last_dayNo, hashMap.get(last_dayNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(last_dayNo, callObject.getCall_duration_in_min());
                }
            }
        }else if(numOfDays == 30)
        {
            for(CallObject callObject: realmResults) {
                String current_weekNo = DateUtil.getWeekFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(current_weekNo)) {
                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_weekNo, callObject.getCall_duration_in_min());
                }
            }
        }else if(numOfDays == 1)
        {
            for(CallObject callObject: realmResults) {
//                String current_HoursNo = DateUtil.getHoursFromTimestamp(timestamp);
                String current_HoursNo = DateUtil.getHoursFromTimestamp(callObject.getCall_time());
                if (hashMap.containsKey(current_HoursNo)) {
                    hashMap.put(current_HoursNo, hashMap.get(current_HoursNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_HoursNo, /*call_in_min*/callObject.getCall_duration_in_min());
                }
            }
        }else // Postpaid Case
        {
            for(CallObject callObject: realmResults)
            {
                String current_weekNo = DateUtil.getWeekFromTimestampPostPaid(mContext, callObject.getCall_time());
                if (hashMap.containsKey(current_weekNo)) {
                    hashMap.put(current_weekNo, hashMap.get(current_weekNo) + callObject.getCall_duration_in_min());
                } else {
                    hashMap.put(current_weekNo, callObject.getCall_duration_in_min());
                }
            }

        }

        return hashMap;
    }

    private Cursor getCursorFromNumberSeries(String formatted_four_digit, SdkAppDatabaseHelper appDatabaseHelper) {
        formatted_four_digit = formatted_four_digit.replace(" ", "");
        return appDatabaseHelper.getCircleAndOperator(formatted_four_digit);
    }


    SdkAppDatabaseHelper appDatabaseHelper;
    SharedPrefManager mSharedPrefManager;

/*    public String typeOfCallAndSMS(String phNumber)
    {
        if(isISDCall(phNumber))
            return CallObject.Constants.ISD_NUMBER;
        else if(isTollFree(phNumber))
            return CallObject.Constants.TOLLFREE_NUMBER;
        else
        {
            mSharedPrefManager = new SharedPrefManager(mContext);
            appDatabaseHelper = new SdkAppDatabaseHelper(mContext);

            String circle = mSharedPrefManager.getStringValue(Constants.ON_BOARDING_CIRCLE);
            String formatted_four_digit = formatefiveDigit(phNumber);
            try {
                Cursor mobileSeriesCursor = getCursorFromNumberSeries(formatted_four_digit, appDatabaseHelper);

                if (mobileSeriesCursor != null && mobileSeriesCursor.moveToFirst()) {
                    String callCircle = mobileSeriesCursor.getString(mobileSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_CIRCLE_NAME));
                    if (!TextUtils.isEmpty(callCircle))
                        return callCircle.equalsIgnoreCase(circle) ? CallObject.Constants.LOCAL_NUMBER : DataStorageConstants.STD_NUMBER;
                } else {
                    fillLandlineList();
                    String callCircle = circleFromlandline(phNumber);
                    if (!TextUtils.isEmpty(callCircle))
                        return callCircle.equals(circle) ? CallObject.Constants.LOCAL_NUMBER : DataStorageConstants.STD_NUMBER;
                }
            }catch (Exception e)
            {
                return CallObject.Constants.LOCAL_NUMBER;
            }

        }
        return CallObject.Constants.LOCAL_NUMBER;
    }*/

    public boolean isISDCall(String phNumber)
    {
        if(phNumber.startsWith("+")&& !phNumber.startsWith("+91"))
            return true;
        else if(phNumber.startsWith("00"))
            return true;
        else
            return false;
    }

    public boolean isTollFree(String phNumber)
    {
        if (phNumber.substring(0, 4).equals("1800"))
            return true;
        else
            return false;
    }
    private ArrayList<String> landline_std_codes = new ArrayList<>();
    private ArrayList<String> landline_circle_name = new ArrayList<>();

    private String circleFromlandline(String phone_number) {
        String formated_number = formatefiveDigit(phone_number);
        if (formated_number != null) {
            for (int i = 0; i < landline_std_codes.size(); i++) {
                if (formated_number.startsWith(landline_std_codes.get(i))) {
                    return landline_circle_name.get(i);
                }
            }
        }
        return null;
    }

    public void fillLandlineList() {
        Cursor landlineSeriesCursor = null;
        landline_std_codes.clear();
        landline_circle_name.clear();
        try {
            landlineSeriesCursor = appDatabaseHelper.getLandlineCursor();
            if (landlineSeriesCursor != null && landlineSeriesCursor.moveToFirst()) {
                do {
                    String circle_name = landlineSeriesCursor.getString(landlineSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_CIRCLE_NAME));
                    String std_code = landlineSeriesCursor.getString(landlineSeriesCursor.getColumnIndex(DataStorageConstants.COLUMN_STD_CODE));
                    if(!landline_std_codes.contains(std_code)) {
                        landline_std_codes.add(std_code);
                        landline_circle_name.add(circle_name);
                    }
                } while (landlineSeriesCursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (landlineSeriesCursor != null) {
                landlineSeriesCursor.close();
            }
        }
    }

    private String formatefiveDigit(String phone_number) {
        final String phon = phone_number;
        String formatted_five_digit = null;
        if (phon.length() > 7) {
            final String first_threeletter = phon.substring(0, 3);
            final String first_letter = phon.substring(0, 1);
            switch (first_letter) {
                case "+":
                    if (first_threeletter.equals("+91")) {
                        formatted_five_digit = phon.substring(3, 8);
                    } else {
                        formatted_five_digit = phon.substring(1, 6);
                    }
                    break;
                case "0":
                    formatted_five_digit = phon.substring(1, 6);
                    break;
                default:
                    formatted_five_digit = phon.substring(0, 5);
            }
        }

        return formatted_five_digit;
    }






    /// SMS related methods

    public long getTotalSMSReceived(String phNumber, int numOfDays)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("sms_address", phNumber).equalTo("sms_type", SMSObject.Constants.SMS_TYPE_RECEIVED).count();
    }

    public long getTotalSMSSent(String phNumber, int numOfDays)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("sms_address", phNumber).equalTo("sms_type", SMSObject.Constants.SMS_TYPE_SENT).count();

    }

    public long getTotalSMS(int numOfDays)
    {
        try {
            return realm.where(SMSObject.class).greaterThan("sms_time", getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).count();
        }catch (Exception e){
            return 0;
        }
    }

    public RealmResults<SMSObject> getAllSMSWithRoamingState(int numOfDays, int is_roaming)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time", getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("is_roaming", is_roaming).findAllSorted("sms_time", Sort.DESCENDING);
    }
    public RealmResults<SMSObject> getAllSMSWithRoamStateTimestamp(long time_stamp, int is_roaming)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time", getStartTimeOfDay(time_stamp)).lessThan("sms_time", getEndTimeOfToday()).equalTo("is_roaming", is_roaming).findAllSorted("sms_time", Sort.DESCENDING);
    }

    public RealmResults<SMSObject> getOperatorSMSListByHash(long dayTime, int roaming_state)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfDay(dayTime)).lessThan("sms_time", getEndTimeOfDay(dayTime)).equalTo("is_roaming", roaming_state).equalTo("isOperatorSMS", true).distinct("hash");//.findAll();
    }

/*    public RealmResults<SMSObject> getSMSList(int numOfDays, int roaming_state)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("is_roaming", roaming_state)*//*.distinct("hash");/*//*//*.findAll();
    }*/

    public RealmResults<SMSObject> getSMSList(int numOfDays)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).findAllSorted("sms_time", Sort.DESCENDING);
    }

    public RealmResults<SMSObject> getOperatorSMSsReceivedBRoamingStateByDays(int numOfDays, int roaming_state)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("is_roaming", roaming_state).equalTo("isOperatorSMS", true).equalTo("sms_type", SMSObject.Constants.SMS_TYPE_RECEIVED).distinct("hash");
    }
    public RealmResults<SMSObject> getSMSsSent(int numOfDays)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("sms_type", SMSObject.Constants.SMS_TYPE_SENT).findAllSorted("sms_time", Sort.DESCENDING);
    }

    public RealmResults<SMSObject> getSMSsSentInRoaming(int numOfDays)
    {
        return realm.where(SMSObject.class).greaterThan("sms_time",getStartTimeOfToday(numOfDays)).lessThan("sms_time", getEndTimeOfToday()).equalTo("sms_type", SMSObject.Constants.SMS_TYPE_SENT).equalTo("is_roaming", 1).findAllSorted("sms_time", Sort.DESCENDING);
    }

    public long getTotalSMSsHashAndDay(String hash, long smsTime)
    {
        return realm.where(CallObject.class).greaterThan("sms_time",getStartTimeOfDay(smsTime)).lessThan("sms_time", getEndTimeOfDay(smsTime)).equalTo("hash", hash).count();
    }


    public RealmResults<SMSObject> getSMSListByTimeAndAddess(long time, String sender)
    {
        return realm.where(SMSObject.class).equalTo("sms_time", time).equalTo("sms_address", sender).findAll();

    }

    public long getLastDataTypeEntry(int data_type, int sim_id)
    {
        try {
            return realm.where(ServerSyncInfoObject.class).equalTo("json_type", data_type).equalTo("sim_id", sim_id).findFirst().getTimestamp();
        }catch (Exception e)
        {
            return 0L;
        }
    }

    ///
    public RealmResults<CardDataRealmObject> getCardList(long startTime, long endTime)
    {
        return realm.where(CardDataRealmObject.class).equalTo("card_visibility", 1).greaterThan("update_timestamp",getStartTimeOfDay(startTime)).lessThan("update_timestamp", getEndTimeOfDay(endTime)).findAllSorted("update_timestamp", Sort.DESCENDING);
    }

    //// CARD DATA
    public void insertCardCategoryData(Context context) {
//        ContentValues values = new ContentValues();

       /* Realm realm = getRealm();

        Iterator itrCardCategoryList = CardUtils.getCardCategoryList(mContext).entrySet().iterator();
        while (itrCardCategoryList.hasNext()) {
            HashMap.Entry cardEntry = (HashMap.Entry) itrCardCategoryList.next();
            values.put(DBContractor.CardCategoryEntry.COLUMN_CATEGORY_ID, (Integer) cardEntry.getKey());
            values.put(DBContractor.CardCategoryEntry.COLUMN_CATEGORY_NAME, (String) cardEntry.getValue());

            long newRowID = db.insert(DBContractor.CardCategoryEntry.TABLE_NAME, null, values);
            //Log.e("INSERTSUCCESS", Long.toString(newRowID));
        }*/
    }
}
