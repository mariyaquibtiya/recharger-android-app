package com.getsmartapp.lib.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.getsmartapp.lib.R;

/**
 * @author  Peeyush.Singh on 16-12-2015.
 */
public class SharedPrefManager {

    private SharedPreferences prefs;
    private Context mContext;

    public SharedPrefManager(Context context) {
        mContext = context;
        if (mContext == null)
            return;

        prefs = context.getSharedPreferences(context.getResources().getString(R.string.prefrences),
                Context.MODE_PRIVATE);
    }



    public void setBooleanValue(String key, boolean value)
    {
        if(mContext!=null) {
            if (prefs == null) {
                prefs = mContext.getSharedPreferences(mContext.getResources().getString(R.string.prefrences),
                        Context.MODE_PRIVATE);
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(key, value);
            editor.apply();
        }
    }

    public boolean getBooleanValue(String key)
    {

        boolean result = true;
        if(prefs!=null)
            result = prefs.getBoolean(key, true);
        return result;
    }

    public boolean getBooleanValue(String key, boolean defValue)
    {

        boolean result = defValue;
        if(prefs!=null)
            result = prefs.getBoolean(key, defValue);
        return result;
    }

    public void setStringValue(String key, String value) {
        if(mContext!=null) {
            if (prefs == null) {
                prefs = mContext.getSharedPreferences(mContext.getResources().getString(R.string.prefrences),
                        Context.MODE_PRIVATE);
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public String getStringValue(String key) {
        String result ="";
        if(prefs!=null)
            result = prefs.getString(key, "");
        return result;
    }
    public String getStringValue(String key, String defaultValue){
        String result = defaultValue;
        if(prefs!=null)
            result = prefs.getString(key, defaultValue);
        return result;
    }

    public void setIntValue(String key, int num) {
        if(mContext!=null) {
            if (prefs == null) {
                prefs = mContext.getSharedPreferences(
                        mContext.getResources().getString(R.string.prefrences),
                        Context.MODE_PRIVATE);
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(key, num);
            editor.apply();
        }
    }

    public long getLongValue(String key) {
        long result = 0;
        if(prefs!=null)
            result = prefs.getLong(key, 0);
        return result;
    }

    public void setLongValue(String key, long num) {
        if(mContext!=null) {
            if (prefs == null) {
                prefs = mContext.getSharedPreferences(
                        mContext.getResources().getString(R.string.prefrences),
                        Context.MODE_PRIVATE);
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong(key, num);
            editor.apply();
        }
    }

    public int getIntValue(String key) {
        int result = 0;
        if(prefs!=null)
            result = prefs.getInt(key, 0);
        return result;
    }

    public int getIntValue(String key, int defaultValue) {
        int result = defaultValue;
        if(prefs!=null)
            result = prefs.getInt(key, defaultValue);
        return result;
    }

    public void deleteStringKeyVal(String key) {
        if(prefs!=null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(key);
            editor.apply();
        }
    }
}
