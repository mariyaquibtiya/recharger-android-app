package com.getsmartapp.lib.managers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.getsmartapp.lib.internetData.InternetDataUsageUtil;
import com.getsmartapp.lib.realmObjects.AppObject;
import com.getsmartapp.lib.realmObjects.InternetDataObject;
import com.getsmartapp.lib.realmObjects.WifiHotSpot;
import com.getsmartapp.lib.utils.DateUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * @author Peeyush.Singh on 03-06-2016.
 */
public class RealmDataManager {

    private Context mContext;
    private Realm mRealm;
    public static RealmDataManager getInstance(Context context) {
        return new RealmDataManager(context);
    }

    RealmDataManager(Context context)
    {
        mContext = context;
//        mRealm = Realm.getInstance(
//                new RealmConfiguration.Builder(context.getApplicationContext())
//                        .name("internet_test.realm").deleteRealmIfMigrationNeeded()/*.schemaVersion(1)*/
//                        .build());
        mRealm = RealmDBManager.getInstance(context).getRealm();
    }


    public Realm getRealm()
    {
        return mRealm;
    }

    public InternetDataObject createNewRealmDayObject()
    {
        InternetDataObject internetDataObject = new InternetDataObject();
        String date = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());

        internetDataObject.setDate(date);
        internetDataObject.setFile_name(date);
        internetDataObject.setDate_in_millis(Calendar.getInstance().getTimeInMillis());
        internetDataObject.setMobile_data_received(0L);
        internetDataObject.setMobile_data_transferred(0L);
        internetDataObject.setTotal_mobile_data(0L);
        internetDataObject.setWifi_data_received(0L);
        internetDataObject.setWifi_data_transferred(0L);
        internetDataObject.setTotal_wifi_data(0L);
        internetDataObject.setTotal_data(0L);
        internetDataObject.setTotal_data_received(0L);
        internetDataObject.setTotal_data_transferred(0L);
        internetDataObject.setAppObjectRealmList(getApplicationPackageNameWithLauncher(mContext, false));
        internetDataObject.setWifiHotSpotRealmList(new RealmList<WifiHotSpot>());


        mRealm.beginTransaction();
        mRealm.copyToRealm(internetDataObject);
        mRealm.commitTransaction();

        return internetDataObject;

    }

    public InternetDataObject createTempObject(Context context)
    {
        WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo currentWifi = mainWifi.getConnectionInfo();
        InternetDataObject internetDataObject = new InternetDataObject();
        String date = "temp_object";//DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());

        internetDataObject.setDate(date);
        internetDataObject.setFile_name(date);
        internetDataObject.setDate_in_millis(Calendar.getInstance().getTimeInMillis());


        long totalTxBytes = TrafficStats.getTotalTxBytes();
        long totalRxBytes = TrafficStats.getTotalRxBytes();

        long mobileTxBytes = TrafficStats.getMobileTxBytes();
        long mobileRxBytes = TrafficStats.getMobileRxBytes();


        if(totalRxBytes < 0)
            totalRxBytes = 0;
        if(totalTxBytes < 0)
            totalTxBytes = 0;
        if(mobileRxBytes < 0)
            mobileRxBytes = 0;
        if(mobileTxBytes < 0)
            mobileTxBytes = 0;


        internetDataObject.setMobile_data_received(mobileRxBytes);
        internetDataObject.setMobile_data_transferred(mobileTxBytes);

        internetDataObject.setTotal_data_received(totalRxBytes);
        internetDataObject.setTotal_data_transferred(totalTxBytes);

        internetDataObject.setTotal_mobile_data(mobileRxBytes + mobileTxBytes);
        internetDataObject.setTotal_data(totalRxBytes + totalTxBytes);
        internetDataObject.setAppObjectRealmList(getApplicationPackageNameWithLauncher(mContext, true));

        mRealm.beginTransaction();
        mRealm.copyToRealm(internetDataObject);
        mRealm.commitTransaction();
        return internetDataObject;
    }

    private RealmList<AppObject> getApplicationPackageNameWithLauncher(Context context, boolean isTempFile) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities
                (intent, 0);

        List<AppObject> realmList = new RealmList<>();

        ArrayList<String> jsonObjectArrayList = new ArrayList<>();
        for (ResolveInfo info : resolveInfoList) {
            ApplicationInfo applicationInfo = info.activityInfo.applicationInfo;
            AppObject appObject = new AppObject();

            String packageName = applicationInfo.packageName;
            appObject.setApp_package_name(packageName);
            appObject.setApp_uid(applicationInfo.uid);
            appObject.setDate(DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()));
            appObject.setDate_in_millis(Calendar.getInstance().getTimeInMillis());
            appObject.setTempObject(isTempFile);
            long transferredData = TrafficStats.getUidTxBytes(applicationInfo.uid) ;
            long receivedData = TrafficStats.getUidRxBytes(applicationInfo.uid);

            if(transferredData < 0)
                transferredData = 0;
            if(receivedData < 0)
                receivedData = 0;

            if(context.getPackageManager().checkPermission(Manifest.permission.INTERNET, applicationInfo.packageName) == PackageManager.PERMISSION_GRANTED)
            {
                appObject.setApp_name(applicationInfo.loadLabel(context.getPackageManager()).toString());
                appObject.setInstalled(true);

                if(!isTempFile) {
                    appObject.setApp_mobile_data_usage(0L);
                    appObject.setApp_wifi_data_usage(0L);
                    appObject.setApp_total_data_usage(0L);
                }else
                {
                    appObject.setApp_mobile_data_usage(transferredData + receivedData);
                    appObject.setApp_total_data_usage(transferredData + receivedData);
                }

                appObject.setApp_wifi_data_usage(0L);
                appObject.setApp_2g_usage(0L);
                appObject.setApp_3g_usage(0L);
                appObject.setApp_4g_usage(0L);
                appObject.setApp_5g_usage(0L);
                appObject.setApp_6g_usage(0L);

//                    mSharedPrefManager.setBooleanValue(LAST_SAVED_CONNECTION_IS_MOBILE, InternetDataUsageUtil.isMobileNetworkConnected(context));
                if(!jsonObjectArrayList.contains(applicationInfo.packageName)) {
                    jsonObjectArrayList.add(applicationInfo.packageName);
//                        jsonArray.put(jsonObject);
                    realmList.add(appObject);
                }
            }
        }
        return (RealmList<AppObject>) realmList;
    }


   public void updateMainAppsMobileDataTableForTotalData(Context context, boolean updateMobileData) {

        if(InternetDataUsageUtil.isNetworkConnected(context.getApplicationContext()))
        {

            RealmResults<InternetDataObject> tempRealmResults = mRealm.where(InternetDataObject.class).equalTo("file_name", "temp_object").findAll();
            InternetDataObject tempObject = null;
            if(tempRealmResults.size() == 0)
                tempObject = createTempObject(context);
            else
                tempObject = tempRealmResults.get(0);


//            String date = DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis());
//            JSONObject mainTableJSONObject;// = readFromFileName(date);
            RealmResults<InternetDataObject> realmResults = mRealm.where(InternetDataObject.class).equalTo("file_name", DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis())).findAll();
            InternetDataObject internetDataObject = null;
            if(realmResults.size()==0) {
                internetDataObject = createNewRealmDayObject();
            }else
                internetDataObject = realmResults.get(0);

            RealmList<AppObject> appObjects = internetDataObject.getAppObjectRealmList();
            RealmList<AppObject> tempAppObjects = tempObject.getAppObjectRealmList();
            mRealm.beginTransaction();

            long totalRxBytes = TrafficStats.getTotalRxBytes();
            long totalTxBytes = TrafficStats.getTotalTxBytes();

            if (totalRxBytes < 0)
                totalRxBytes = 0;
            if (totalTxBytes < 0)
                totalTxBytes = 0;

            long overallDataToInsert = totalRxBytes + totalTxBytes - tempObject.getTotal_data();//temTableDataJsonObject.getLong(InternetDataUsageUtil.TOTAL_OVERALL_DATA);

            if(overallDataToInsert < 0)
                overallDataToInsert = 0;

            if (updateMobileData ) {
//                mRealm.beginTransaction();
                internetDataObject.setTotal_mobile_data(internetDataObject.getTotal_mobile_data()+ overallDataToInsert);
                tempObject.setTotal_mobile_data(totalRxBytes+totalTxBytes);
//                mRealm.commitTransaction();
            }else if(InternetDataUsageUtil.isWifiNetworkConnected(context))
            {
                WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo currentWifi = mainWifi.getConnectionInfo();
//                updateWifiDataSSDWise(context, currentWifi, internetDataObject, overallDataToInsert);
            }

            if (overallDataToInsert >= 0) {
//                mRealm.beginTransaction();
                internetDataObject.setTotal_data(internetDataObject.getTotal_data()+overallDataToInsert);
                tempObject.setTotal_data(totalRxBytes+totalTxBytes);
//                mRealm.commitTransaction();
            } /*elsecurrentWifi
                    return;*/

            for (AppObject tempAppObject: tempAppObjects
                    ) {

                int uid = tempAppObject.getApp_uid();
                long currentReceivedTrafficStatsData = TrafficStats.getUidRxBytes(uid);
                long currentTransferredTrafficStatsData = TrafficStats.getUidTxBytes(uid);
                if (currentReceivedTrafficStatsData < 0)
                    currentReceivedTrafficStatsData = 0;
                if (currentTransferredTrafficStatsData < 0)
                    currentTransferredTrafficStatsData = 0;

                long dataToInsert = ((currentReceivedTrafficStatsData + currentTransferredTrafficStatsData) - tempAppObject.getApp_total_data_usage());//tempTableJSONObject.getLong(InternetDataUsageUtil.APP_OVERALL_DATA_USAGE));

                if (dataToInsert < 0)
                    dataToInsert = 0;

//                if (dataToInsert > 0) {
//                        Log.e("PeeyushKS", "Realm AN: " + tempAppObject.getApp_name() + "- " + InternetDataUsageUtil.humanReadableByteCount(dataToInsert));
//                }

                AppObject appObject = appObjects.where().equalTo("app_package_name", tempAppObject.getApp_package_name()).findFirst();
                if(updateMobileData)
                    appObject.setApp_mobile_data_usage(appObject.getApp_mobile_data_usage()+dataToInsert);
                appObject.setApp_total_data_usage(appObject.getApp_total_data_usage()+dataToInsert);

                tempAppObject.setApp_total_data_usage(currentTransferredTrafficStatsData+currentReceivedTrafficStatsData);
//                tempAppObject.setTotal_data(currentTransferredTrafficStatsData+currentReceivedTrafficStatsData);

            }

            mRealm.commitTransaction();

        }

    }

/*    void updateWifiDataSSDWise(Context context, WifiInfo wifiInfo, InternetDataObject internetDataObject, long data)
    {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        String date = DateUtil.getDateFromTimeInMillis(currentTime);
//        JSONArray jsonArray = readWifiDataFromFileName(date);
        InternetDataObject internetDataObject1 = internetDataObject;
        RealmList<WifiHotSpot> wifiHotSpotRealmList = internetDataObject1.getWifiHotSpotRealmList();
        JSONObject tempJSONObject = readWifiDataTempFile();

        mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_LAST_WIFI_CONNECTED_TIME, Calendar.getInstance().getTimeInMillis());

        tempJSONObject.length();
        JSONObject jsonObject = null;

        try {

            RealmResults<WifiHotSpot> wifiHotSpots = wifiHotSpotRealmList.where().equalTo("wifi_ssid", wifiInfo.getSSID().replace("\"", "")).findAll();

            if(wifiHotSpots.size() > 0)
            {

            }else
            {
                WifiHotSpot wifiHotSpot = new WifiHotSpot();
                wifiHotSpot.setWifi_ssid(wifiInfo.getSSID().replace("\"", ""));
                wifiHotSpot.setWifi_mac_address(wifiInfo.getMacAddress());
                wifiHotSpot.setWifi_data_consumed(0L);
                wifiHotSpot.setWifi_connected_time(0L);
                wifiHotSpot.setWifi_locations("");

            }

//            for(int i=0;i<jsonArray.length();i++)
            for(int i=0;i<wifiHotSpotRealmList.size();i++)
            {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                if(jsonObject1.get(InternetDataUsageUtil.WIFI_SSID).toString().equalsIgnoreCase(wifiInfo.getSSID().replace("\"", "")))
                {
                    // Check temp ssid. If same update wifi date. else update temp object with current info.
                    jsonObject = jsonObject1;
//                    Log.e("PeeyushKS","SSID Exist: "+wifiInfo.getSSID().replace("\"", ""));
                    try {
                        if (tempJSONObject.length() > 0 && tempJSONObject.getString(InternetDataUsageUtil.WIFI_SSID).toString().equalsIgnoreCase(wifiInfo.getSSID().replace("\"", ""))) {
                            /// Update both data...temptable with current and data table with data
                            jsonObject1.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, jsonObject1.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED)+data);
                            mSharedPrefManager.setLongValue(InternetDataUsageUtil.SHARED_PREFERENCE_CURRENT_CONNECTION_DATA, jsonObject1.getLong(InternetDataUsageUtil.WIFI_DATA_CONSUMED)+data);
//                            Log.e("PeeyushKS","Wifi Data: "+jsonObject1);
                            jsonObject = jsonObject1;
                        }
                    }catch (Exception e)
                    {
                    }
                }
            }

            if(jsonObject == null) {
                jsonObject = new JSONObject();
                jsonObject.put(InternetDataUsageUtil.WIFI_SSID, wifiInfo.getSSID().replace("\"", ""));
                jsonObject.put(InternetDataUsageUtil.WIFI_MAC_ADDRESS, wifiInfo.getMacAddress());
                jsonObject.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, 0);
                jsonObject.put(InternetDataUsageUtil.WIFI_CONNECTED_TIME, 0);
                jsonObject.put(InternetDataUsageUtil.WIFI_DATA_LOCATION, "");
//                jsonObject.put(InternetDataUsageUtil.WIFI_DATE,Calendar.getInstance().getTimeInMillis());

                jsonArray.put(jsonObject);
            }

            tempJSONObject.put(InternetDataUsageUtil.WIFI_SSID, wifiInfo.getSSID().replace("\"", ""));
            tempJSONObject.put(InternetDataUsageUtil.WIFI_MAC_ADDRESS, wifiInfo.getMacAddress());
            tempJSONObject.put(InternetDataUsageUtil.WIFI_DATA_CONSUMED, TrafficStats.getTotalTxBytes()+TrafficStats.getTotalRxBytes());
            tempJSONObject.put(InternetDataUsageUtil.WIFI_CONNECTED_TIME, Calendar.getInstance().getTimeInMillis());
            tempJSONObject.put(InternetDataUsageUtil.WIFI_DATA_LOCATION, "");

            writeToWifiFie(jsonArray, DateUtil.getDateFromTimeInMillis(Calendar.getInstance().getTimeInMillis()));
            writeToWifiTempFile(tempJSONObject);
        } catch (JSONException e) {
//            e.printStackTrace();
        }

    }*/

    public RealmResults<InternetDataObject> getInternetDataObjectsWeekly(int weekNumber) {
        return mRealm.where(InternetDataObject.class).greaterThan("date_in_millis", getStartTimeOfWeek(weekNumber)).lessThan("date_in_millis", getEndTimeOfWeek(weekNumber)).notEqualTo("file_name", "temp_object").findAll();

    }

    public RealmResults<InternetDataObject> getInternetDataObject(String date) {
        return mRealm.where(InternetDataObject.class).equalTo("date", date).notEqualTo("file_name", "temp_object").findAll();

    }

    public long getStartTimeOfWeek(int weekNumber) {
        int numOfDays = 7*(weekNumber+1);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(calendar.getTimeInMillis()-((numOfDays -1)*24*60*60*1000L));

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }
    public long getEndTimeOfWeek(int weekNumber) {


        int numOfDays = 7*(weekNumber);

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(calendar.getTimeInMillis()-((numOfDays)*24*60*60*1000L));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    public long getStartTimeOfToday(int numOfDays) {
        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
        calendar.setTimeInMillis(calendar.getTimeInMillis()-((numOfDays - 1)*24*60*60*1000L));
//        calendar.setTimeInMillis(calendar.getTimeInMillis()+(24*60*60*1000));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public long getEndTimeOfToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }
}
