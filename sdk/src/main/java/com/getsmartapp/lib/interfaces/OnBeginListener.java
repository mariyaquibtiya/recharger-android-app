package com.getsmartapp.lib.interfaces;

import java.util.HashMap;

/**
 * @author  nitesh.verma on 2/4/16.
 */
public interface OnBeginListener {
    void OnPrePaid(HashMap<String, String> hashMap);
    void onPostPaid(HashMap<String, String> map);
}
