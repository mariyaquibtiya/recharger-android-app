package com.getsmartapp.lib.interfaces;

/**
 * @author nitesh.verma on 2/4/16.
 */
public interface ProgressDialogListener {
    void showProgressDialog();
    void dismissProgressDialog();
}
