package com.getsmartapp.lib.interfaces;

import com.getsmartapp.lib.model.BackendUserParser;
import com.getsmartapp.lib.model.CallClassificationMNP;
import com.getsmartapp.lib.model.ComboPlan;
import com.getsmartapp.lib.model.GenerateReferralCode;
import com.getsmartapp.lib.model.GenericParserSSO;
import com.getsmartapp.lib.model.GenericResponseParser;
import com.getsmartapp.lib.model.Geocode;
import com.getsmartapp.lib.model.GetReferredDetails;
import com.getsmartapp.lib.model.GoogleImgModal;
import com.getsmartapp.lib.model.HijeckNumberParser;
import com.getsmartapp.lib.model.HomeAllInstreamModel;
import com.getsmartapp.lib.model.InitRechargeModel;
import com.getsmartapp.lib.model.InstreamPlanModel;
import com.getsmartapp.lib.model.MNPRequest;
import com.getsmartapp.lib.model.MerchantHashModel;
import com.getsmartapp.lib.model.OrderHistoryBean;
import com.getsmartapp.lib.model.OrderSummaryBean;
import com.getsmartapp.lib.model.PlanDetailModel;
import com.getsmartapp.lib.model.PromoCodeModel1;
import com.getsmartapp.lib.model.ProxyLoginUser;
import com.getsmartapp.lib.model.RecentRechargeServer;
import com.getsmartapp.lib.model.RechargeFeasibilityModel;
import com.getsmartapp.lib.model.ReferralActivityDetails;
import com.getsmartapp.lib.model.ReferralEvents;
import com.getsmartapp.lib.model.ReferrerBranchDetails;
import com.getsmartapp.lib.model.ReferrerEarnings;
import com.getsmartapp.lib.model.SaveMerchantHashModel;
import com.getsmartapp.lib.model.SpandCircle;
import com.getsmartapp.lib.model.USSDResponseModel;
import com.getsmartapp.lib.model.UserDetailFromServer;
import com.getsmartapp.lib.model.WSModel;
import com.getsmartapp.lib.constants.ApiConstants;
import com.getsmartapp.lib.constants.Constants;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * @author  jayant on 5/11/15.
 */
public interface ApiServices {

    /*   Login Web Service   */
    @GET("/ActivateMobileNo")
    void activateMobileNumber(@QueryMap Map<String, String> params, Callback<ProxyLoginUser.SoResponseEntity>
            callback);

    /*   Add Mobile Number   */
    @GET("/AddMobileNo")
    void addMobileNumber(@QueryMap Map<String, String> params, Callback<ProxyLoginUser.AddMobileApiEntity>
            callback);

    /* User Registration */
    @FormUrlEncoded
    @POST("/user/completeregistration")
    void userRegistrationOnSmartAPP(@Field(ApiConstants.COMPLETEREGISTRATION) String params, Callback<ProxyLoginUser.CompleteRegistrationResponseEntity> callback);

    /* Get Referred through Referral Code */
    @FormUrlEncoded
    @POST("/referral/getReferred")
    void getReferred(@Field("data") String params, Callback<GetReferredDetails> callback);

    /* Get Referral Activity Details */
    @GET("/referral/getreferrals")
    void getReferralActivityDetails(@Query(ApiConstants.REFERRER_ID) String referrerId, @Query(ApiConstants.START) Integer start,
                                    @Query(ApiConstants.LIMIT) Integer limit, Callback<ReferralActivityDetails> callback);

    /* Get Referral Events Stream*/
    @GET("/referral/getreferralevents")
    void getReferralActivityLog(@Query(ApiConstants.REFERRER_ID) String referrerId, @Query(ApiConstants.START) Integer start,
                                @Query(ApiConstants.LIMIT) Integer limit, Callback<ReferralEvents> callback);

    /* Get Referrer Earnings */
    @GET("/referral/getreferrerearnings")
    void getReferrerEarnings(@Query(ApiConstants.REFERRER_ID) String referrerId, Callback<ReferrerEarnings> callback);

    /* Get Referral Data before Referral Screen */
    @GET("/referral/generateReferralCode")
    void generateReferralCode(@Query(ApiConstants.ssoid) String ssoId, @Query(ApiConstants.DEVICE_ID) String deviceId, @Query(ApiConstants.fName) String fName,
                              @Query(ApiConstants.IMAGEURL) String imgUrl, Callback<GenerateReferralCode> callback);

    // Update Profile
    @GET("/UpdateIntegraUserProfile")
    void userRequestUpdateProfile(@QueryMap Map<String, String> params, Callback<GenericParserSSO>
            callback);

    //Amount to Plan API
    @GET("/plan/getplanid")
    void getPlanIdFromAmount(@QueryMap Map<String, String> params, Callback<PlanDetailModel>
            callback);

    /*Order History */
    @GET("/order/getOrderHistory")
    void getOrderHistory(@QueryMap Map<String, String> params, Callback<OrderHistoryBean>
            callback);

    /*Order Summary */
    @GET("/order/getOrdStatus")
    void getOrderSummary(@QueryMap Map<String, String> params, Callback<OrderSummaryBean>
            callback);

    /* GetPlan Details */
    @GET("/plan/getplandetails")
    void getPlanAPIDetails(@QueryMap Map<String, String> params,
                           Callback<PlanDetailModel>
                                   callback);

    /* GetPlan Details */
    @GET("/recommend/all/plans")
    void getRecommendedPlanAPIDetails(@QueryMap Map<String, String> params,
                                      Callback<HomeAllInstreamModel>
                                              callback);

    @GET("/recommend/instream/plans")
    void getInstreamPlansDetails(@QueryMap Map<String, String> params, Callback<InstreamPlanModel> callback);

    @FormUrlEncoded
    @POST("/recommend/device/instream/plans/")
    void postInstreamPlansDetails(@FieldMap Map<String, String> params, Callback<InstreamPlanModel>
            callback);

    /* PostPlan Details */
    @FormUrlEncoded
    @POST("/recommend/device/all/plans")
    void postRecommendedPlanAPIDetails(@FieldMap Map<String, String> params, Callback<HomeAllInstreamModel> callback);

    /* STD Plan API */
    @GET("/recommend/instream/filtered-plans")
    void getSTDPlansDetails(@QueryMap Map<String, String> params, Callback<InstreamPlanModel> callback);

    /* Get Referrer Details */
    @GET("/referral/getReferrerBranchDetails")
    void getReferrerBranchDetails(@Query(ApiConstants.INVITEE_SSO_ID) String inviteeSSOID, Callback<ReferrerBranchDetails> callback);

    //Sending Daily data
    @FormUrlEncoded
    @POST("/insertDailyData")
    void sendDailyData(@Field(ApiConstants.DATA_JSON) String jsonData, @Field(ApiConstants.JSON_DATA_TYPE) int jsonDataType, Callback<HashMap<String, Object>> callback);

    //Sending Bulk data
    @FormUrlEncoded
    @POST("/insertBulkData")
    void sendBulkData(@Field(ApiConstants.DATA_JSON) String jsonData, @Field(ApiConstants.JSON_DATA_TYPE) int jsonDataType, @Field(ApiConstants.DEVICE_ID) String deviceId, Callback<HashMap<String, Object>> callback);

    /* GetPlan Details */
    @GET("/recommend/combo/plans")
    void getComboAPIDetails(@QueryMap Map<String, String> params, Callback<ComboPlan> callback);

    /* GetPlan Details */
    @FormUrlEncoded
    @POST("/recommend/device/combo/plans")
    void postComboAPIDetails(@FieldMap Map<String, String> params, Callback<ComboPlan> callback);

    @GET("/recharge/getPayUWSHash")
    void getWsHash(@QueryMap Map<String, String> params, Callback<WSModel> callback);

//    // Getting Service Provider and Circle from Server
//    @GET("/sp/getSpAndCir")
//    void fetchCircleOperator(@Query(ApiConstants.SERIES) String s, Callback<SpandCircle> callback);

    // Getting Service Provider and Circle from Server
    @GET("/sp/getSpAndCirVServe")
    void fetchCircleOperator(@Query(ApiConstants.SERIES) String s, Callback<SpandCircle> callback);

    //Setting User Notification Preferences
    @GET("/user/saveUserPreferences")
    void saveUserPreferences(@Query(ApiConstants.ssoId) String ssoId, @Query(ApiConstants.T_PUSH_STATE) int tFlag, @Query(ApiConstants.P_PUSH_STATE) int pFlag,
                             @Query(ApiConstants.EMAIL_STATE) int eFlag, @Query(ApiConstants.SMS_STATE) int sFlag, Callback<GenericResponseParser> callback);

    /* Post Feedback */
    @FormUrlEncoded
    @POST("/email/sendFeedback")
    void sendFeedBacK(@Field("feedback") String params,
                      @Field("ssoId") String ssoId,
                      @Field("deviceId") String deviceId,
                      @Field("email") String email,
                      @Field("phoneNo") String phoneNo,
                      Callback<String> callback);

    // User profile update
    @FormUrlEncoded
    @POST("/user/update")
    void updateUserInSmartAppdb(@Field(ApiConstants.FNAME) String fname,
                                @Field(ApiConstants.LNAME) String lname, @Field(ApiConstants.PHONE) String phone,
                                @Field(ApiConstants.ssoId) String ssoId, @Field(ApiConstants.email) String email,
                                @Field(ApiConstants.IMAGEURL) String imgUrl, Callback<BackendUserParser> callback);

    //mobile hijecked Backend API
    @GET("/user/isPhoneNumberHijacked")
    void checknumberHijecked(@Query(ApiConstants.ssoId) String ssoId, Callback<HijeckNumberParser> callback);

    /* Post INIT RECHARGE */
    @FormUrlEncoded
    @POST("/recharge/initRecharge")
    void postInitRecharge(@Field("order") String params, Callback<InitRechargeModel> callback);

    /* Post INIT RECHARGE */
    @FormUrlEncoded
    @POST("/recharge/quickRecharge")
    void quickRecharge(@Field("order") String params, Callback<OrderSummaryBean> callback);

    //    @GET("/Demeter/gc/shop/validate4")
    @GET("/gc/validate")
    void verifyPromocode(@Query("userId") String userId, @Query("gcId") String gcId, @Query("orderAmount") String orderAmount,
                         @Query("merchant") String merchant, @Query("product") String product,
                         @Query("orderTime") String orderTime, @Query("subAmount") String subAmount, @Query("catalog") String catalog,
                         @Query("category") String category, @Query("altId") String altId, @Query("deviceId") String deviceId,
                         @Query("doGCValidation") boolean doGCValidationOnServer, @Query("csHash") String csHash,
                         Callback<PromoCodeModel1> callback);

    @GET("/plan/prepaidmobile/getcircles")
    void getCircle(@QueryMap Map<String, String> params, Callback<String> callback);

    @GET("/me")
    void getGplusPicUrl(@Query(ApiConstants.GP_ACCESS_TOKEN) String acc_token, Callback<GoogleImgModal> callback);

    @GET("/order/getRecentRecharges")
    void getRecentRecharge(@QueryMap Map<String, String> params, Callback<RecentRechargeServer> callback);

    @GET("/get-latest-date-and-count")
    void getPreviousAggregatedData(@QueryMap Map<String, String> params, Callback<Response> callback);

    @GET("/user/get")
    void getUserDetails(@Query(ApiConstants.keyword) String keyword, @Query(ApiConstants.field) String field, Callback<UserDetailFromServer> callback);

    @GET("/email/sendWelcomeEmail")
    void sendWelcomeEmail(@QueryMap Map<String, String> params, Callback<Response> callback);

    @GET("/signup")
    void completeRegisterationProxy(@QueryMap Map<String, String> params, Callback<ProxyLoginUser> callback);

    @GET("/activate/mobile")
    void activateMobile(@QueryMap Map<String, String> params, Callback<ProxyLoginUser> callback);

/*    @GET("/maps/api/geocode/json")
    void getAddressComponentsFromLatLong(@Query(Constants.LATLONG)String latlong,Callback<Geocode> callback);*/
@GET("/maps/api/geocode/json")
void getAddressComponentsFromLatLong(@Query(Constants.LATLONG) String latlong, @Query(Constants.KEY) String key, Callback<Geocode> callback);

    @POST("/sp/mnpstatus")
    void getMNPCircleOperatorForNumber(@Body MNPRequest body, Callback<CallClassificationMNP> callback);

    @GET("/qh/get_qhd")
    void getQuickHelpData(@Query("operatorId") int operator, Callback<USSDResponseModel> callback);

    @POST("/sms/sendSMS")
    void sendSMS(@Body HashMap<String, String> map, Callback<String> callback);

    @GET("/user/saveUserCard")
    void saveMerchantHashTokens(@Query("ssoId") String ssoId, @Query("pgSlug") String pgSlug,
                                @Query("cardToken") String cardToken, @Query("merchantHash") String merchantHash,
                                Callback<SaveMerchantHashModel> callback);

    @GET("/user/getUserSavedCardList")
    void getMerchantHashTokens(@Query("ssoId") String ssoId, Callback<MerchantHashModel> callback);
    @GET("/user/deleteUserSavedCard")
    void deleteMerchantHashTokens(@Query("ssoId") String ssoId, @Query("cardToken") String cardToken, Callback<MerchantHashModel> callback);


    //Amount to Plan API
    @GET("/recharge/getRechargeFeasibilityCombo")
    void getRechargeFeasibility(@Query("rechargeTo") String rechargeTo,
                                @Query("serviceProviderId") int serviceProviderId,
                                @Query("comboAmtsStr") String rechargeAmt, Callback<RechargeFeasibilityModel> callback);
}